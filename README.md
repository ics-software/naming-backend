# Naming

The purpose of Naming is to handle Naming of ESS wide physical and logical devices according to ESS Naming Convention.

## Naming backend

Naming backend is a web application implemented as a REST style web service backed by a relational database. The web service is implemented as a Spring Boot application and the database is available as PostgreSQL.

See `docs` and sub folders to learn how ESS Naming Convention is implemented in Naming backend.

### Environment

Tools

* Java 17 - openjdk
* Maven 3.8+
* PostgreSQL - client lib 9.6
* Docker - engine 18.06.0+ or later, compose 2.21.0 or later, compose file version 3.7 to be supported
* Git

More on configuration and setting of environment variables is available in [Configuration](CONFIGURATION.md).

### Start application

Start the application and database via Docker. For first time users, the recommended way is to use demo content. See Note for more on configuration.

* build and run
    * `mvn clean install`
    * `docker-compose up --build`
    * `docker-compose -f docker-compose-demo.yml up --build`
*  browse to
    * `http://localhost:8080/swagger-ui.html`

More on how to start the application is available in [Start application](START_APPLICATION.md).

### Integration tests with Docker containers

Purpose of integration tests is to ensure REST API works as expected for parts and as a whole by running tests against running instances of application and database.

See `src/test/java` and package `org.openepics.names.docker`.

Integration tests start docker containers for Naming backend and Postgresql database and run http requests (GET) and curl commands (POST, PUT, PATCH, DELETE) towards the application to test behavior (CRUD - create, read, update, delete) and replies are received and checked if content is as expected.

There are tests for names and structures separately and in combination.

Integration tests can be run in IDE and via Maven.

```
mvn failsafe:integration-test -DskipITs=false
```

See
* [How to run Integration test with Docker](src/test/resources/INTEGRATIONTEST_DOCKER_RUN.md)
* [Tutorial for Integration test with Docker](src/test/resources/INTEGRATIONTEST_DOCKER_TUTORIAL.md)

### Note

* Configuration
    * Application and database may be started separately, with or without Docker, in which case configuration set by environment variables, docker compose files and `application.properties` must reflect this. An example of this is hostname/ip address/port for database that is to be available for application. Configuration must not be in conflict with environment in which application and database run.
* Pre-populated database to be available if data to be available at start of application. Otherwise database may be populated through `curl` at command line or Swagger UI.
    * See `src/main/resources` and `src/test/resources` and sub folders for database scripts. This includes scripts for database schema, migration and example content.
    * If no prior database is available, see integration tests for examples on how to populate database. E.g. tests may be debugged and database content may be examined at any debug point. It's possible to backup database at such debug point and later restore it outside test. In such way, database may be set to state of choice.

 ```
   postgres:
     volumes:
       - ./src/test/resources/data/db/dump-discs_names_namesit.sql:/docker-entrypoint-initdb.d/dump-discs_names_namesit.sql
 ```

* Test
    * Integration tests and code coverage for unit and integration tests are disabled by default and can be enabled separately and together.
    * Code coverage for unit tests is enabled by `-Djacoco.skip=false`. Result is available in `target/site/jacoco` folder and includes code coverage execution data and reports.

### Recommendation

* Have local docker-compose files - `.gitignore` contains `docker-compose-local*`
    * for application and database together that may contain local configuration
    * for database alone if there is need to backup or restore database

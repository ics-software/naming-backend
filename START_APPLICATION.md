### Start application

For first time users, the recommended way to start the application and database is with Docker and demo content.

##### With Docker

To start the application with an existing database.

```
docker-compose up --build
```

To start the application with an empty database.

```
docker-compose -f docker-compose-integrationtest.yml up --build
```

To start the application with a database with demo content.

```
docker-compose -f docker-compose-demo.yml up --build
```

Browse to `http://localhost:8080/swagger-ui.html`.

##### With `java -jar`

Make sure the database is available as in `src/main/resources/application.properties`.

To start the application.

```
java -jar target/naming-backend-*.jar
java -jar -Dspring.datasource.url=jdbc:postgresql://localhost:5432/discs_names target/naming-backend-*.jar
```

Browse to `http://localhost:8080/swagger-ui.html`.

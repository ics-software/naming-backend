-- ----------------------------------------------------------------------------------------------------
-- About
--     transformation script
--     postgresql 9.6.7
-- Content
--     data structures
--         set most recent content (without latest) to deleted = true
-- Note
--     to have most recent in line of uuid without latest as deleted in order to be possible to query and find data when status attribute is removed as query parameter
--         may affect entries with statuses PENDING, CANCELLED, REJECTED
--         will not affect entries considered history
--     strong (!) recommendation to make sure that no items with status pending exist prior to running this script (any such to be approved, cancelled, rejected)
--     order of items is important
--     notice prefix "wip_" meaning "work in progress" as tables are temporary
-- ----------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------
-- structures
--     systemgroup
--     system
--     subsystem
--     discipline
--     devicegroup
--     devicetype
-- ----------------------------------------------------------------------------------------------------

update wip_systemgroup s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

update wip_system s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

update wip_subsystem s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

update wip_discipline s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

update wip_devicegroup s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

update wip_devicetype s set latest = true, deleted = true where (
                 not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false)
);

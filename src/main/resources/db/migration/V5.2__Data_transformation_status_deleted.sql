-- --------------------------------------------------------------------------------
-- About
--     transformation script
--     postgresql 9.6.7
-- Content
--     data structures
--         set most recent content (without latest) to deleted = true
-- Note
--     delete line of line of uuid without latest
--         may affect entries with statuses PENDING, CANCELLED, REJECTED
--         will not affect entries considered history
--     reason is that such entries have never been active and while removing them will delete some part of history,
--         the benefit is unambiguous content in database. Alternative is deleted entries with statuses
--         pending, cancelled, rejected which may be ambiguous.
--     strong (!) requirement to make sure that no items with status pending exist prior to running this script (any such to be approved, cancelled, rejected)
--     order of items is important
-- --------------------------------------------------------------------------------

-- --------------------------------------------------------------------------------
-- structures
--     systemgroup
--     system
--     subsystem
--     discipline
--     devicegroup
--     devicetype
-- --------------------------------------------------------------------------------

delete from wip_systemgroup where uuid in (
    select s.uuid from wip_systemgroup s where (
                 not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

delete from wip_system where uuid in (
    select s.uuid from wip_system s where (
                 not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

delete from wip_subsystem where uuid in (
    select s.uuid from wip_subsystem s where (
                 not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

delete from wip_discipline where uuid in (
    select s.uuid from wip_discipline s where (
                 not exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

delete from wip_devicegroup where uuid in (
    select s.uuid from wip_devicegroup s where (
                 not exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

delete from wip_devicetype s0 where s0."uuid" in (
    select s.uuid from wip_devicetype s where (
                 not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id = (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false)
    )
);

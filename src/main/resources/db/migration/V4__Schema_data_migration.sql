-- ----------------------------------------------------------------------------------------------------
-- About
--     migration script
--     postgresql 9.6.7
-- Content
--     structures
--     names
--     data structures level 1
--     data structures level 2
--     data structures level 3
--     data names level 1
--     data names level 2
--     data names level 3
--     index
--     latest
--     index
--     sequence
--     primary key
--     foreign key
--     function
-- Note
--     order of items is important
--     name and description are concatenated for structures
--     notice prefix "wip_" meaning "work in progress" as tables are temporary
-- ----------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------
-- structures
-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS wip_systemgroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS wip_system (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS wip_subsystem (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS wip_discipline (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS wip_devicegroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS wip_devicetype (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
-- names
-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS wip_name (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    systemgroup_uuid text,
    system_uuid text,
    subsystem_uuid text,
    devicetype_uuid text,
    instance_index text,
    convention_name text,
    convention_name_equivalence text,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
-- data structures level 1
--     from namepartrevision, level 1, SECTION     - no parent
--     from namepartrevision, level 1, DEVICE_TYPE - no parent
-- ----------------------------------------------------------------------------------------------------
insert into wip_systemgroup (
    id,
    version,
    uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr1.id, npr1."version", np1.uuid, npr1.mnemonic, npr1.mnemoniceqclass, null, nullif(concat_ws('. ', npr1."name", npr1.description), ''), npr1.status, false, npr1.deleted,
npr1.requestdate, ua_r.username as requestedBy, npr1.requestercomment, npr1.processdate, ua_p.username as processedBy, npr1.processorcomment
from namepartrevision npr1
inner join namepart np1 on npr1.namepart_id = np1.id
left join useraccount ua_r on npr1.requestedby_id = ua_r.id
left join useraccount ua_p on npr1.processedby_id = ua_p.id
where np1.nameparttype = 'SECTION' and npr1.parent_id is null;

insert into wip_discipline (
    id,
    version,
    uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr1.id, npr1."version", np1.uuid, npr1.mnemonic, npr1.mnemoniceqclass, null, nullif(concat_ws('. ', npr1."name", npr1.description), ''), npr1.status, false, npr1.deleted,
npr1.requestdate, ua_r.username as requestedBy, npr1.requestercomment, npr1.processdate, ua_p.username as processedBy, npr1.processorcomment
from namepartrevision npr1
inner join namepart np1 on npr1.namepart_id = np1.id
left join useraccount ua_r on npr1.requestedby_id = ua_r.id
left join useraccount ua_p on npr1.processedby_id = ua_p.id
where np1.nameparttype = 'DEVICE_TYPE' and npr1.parent_id is null;

-- ----------------------------------------------------------------------------------------------------
-- data structures level 2
--     from namepartrevision, level 2, SECTION     - 1 parent level
--     from namepartrevision, level 2, DEVICE_TYPE - 1 parent level
-- ----------------------------------------------------------------------------------------------------
insert into wip_system (
    id,
    version,
    uuid,
    parent_uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr2.id, npr2."version", np2.uuid, np22.uuid,
npr2.mnemonic, npr2.mnemoniceqclass, null, nullif(concat_ws('. ', npr2."name", npr2.description), ''),
npr2.status, false, npr2.deleted, npr2.requestdate, ua_r.username as requestedBy, npr2.requestercomment, npr2.processdate, ua_p.username as processedBy, npr2.processorcomment
from namepartrevision npr2
inner join namepart np2  on npr2.namepart_id = np2.id
inner join namepart np22 on npr2.parent_id   = np22.id
left join useraccount ua_r on npr2.requestedby_id = ua_r.id
left join useraccount ua_p on npr2.processedby_id = ua_p.id
where np2.nameparttype = 'SECTION' and npr2.parent_id in
(select npr1.namepart_id from namepartrevision npr1, namepart np1 where npr1.namepart_id = np1.id and np1.nameparttype = 'SECTION' and npr1.parent_id is null);

insert into wip_devicegroup (
    id,
    version,
    uuid,
    parent_uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr2.id, npr2."version", np2.uuid, np22.uuid,
npr2.mnemonic, npr2.mnemoniceqclass, null, nullif(concat_ws('. ', npr2."name", npr2.description), ''),
npr2.status, false, npr2.deleted, npr2.requestdate, ua_r.username as requestedBy, npr2.requestercomment, npr2.processdate, ua_p.username as processedBy, npr2.processorcomment
from namepartrevision npr2
inner join namepart np2  on npr2.namepart_id = np2.id
inner join namepart np22 on npr2.parent_id   = np22.id
left join useraccount ua_r on npr2.requestedby_id = ua_r.id
left join useraccount ua_p on npr2.processedby_id = ua_p.id
where np2.nameparttype = 'DEVICE_TYPE' and npr2.parent_id in
(select npr1.namepart_id from namepartrevision npr1, namepart np1 where npr1.namepart_id = np1.id and np1.nameparttype = 'DEVICE_TYPE' and npr1.parent_id is null);

-- ----------------------------------------------------------------------------------------------------
-- data structures level 3
--     from namepartrevision, level 3, SECTION     - 2 parent levels
--     from namepartrevision, level 3, DEVICE_TYPE - 2 parent levels
-- ----------------------------------------------------------------------------------------------------
insert into wip_subsystem (
    id,
    version,
    uuid,
    parent_uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr3.id, npr3."version", np3.uuid, np32.uuid,
npr3.mnemonic, npr3.mnemoniceqclass, null, nullif(concat_ws('. ', npr3."name", npr3.description), ''),
npr3.status, false, npr3.deleted, npr3.requestdate, ua_r.username as requestedBy, npr3.requestercomment, npr3.processdate, ua_p.username as processedBy, npr3.processorcomment
from namepartrevision npr3
inner join namepart np3  on npr3.namepart_id = np3.id
inner join namepart np32 on npr3.parent_id   = np32.id
left join useraccount ua_r on npr3.requestedby_id = ua_r.id
left join useraccount ua_p on npr3.processedby_id = ua_p.id
where np3.nameparttype = 'SECTION' and npr3.parent_id in
(
select npr2.namepart_id from namepartrevision npr2, namepart np2 where npr2.namepart_id = np2.id and np2.nameparttype = 'SECTION' and npr2.parent_id in
(select npr1.namepart_id from namepartrevision npr1, namepart np1 where npr1.namepart_id = np1.id and np1.nameparttype = 'SECTION' and npr1.parent_id is null)
);

insert into wip_devicetype (
    id,
    version,
    uuid,
    parent_uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select npr3.id, npr3."version", np3.uuid, np32.uuid,
npr3.mnemonic, npr3.mnemoniceqclass, null, nullif(concat_ws('. ', npr3."name", npr3.description), ''),
npr3.status, false, npr3.deleted, npr3.requestdate, ua_r.username as requestedBy, npr3.requestercomment, npr3.processdate, ua_p.username as processedBy, npr3.processorcomment
from namepartrevision npr3
inner join namepart np3  on npr3.namepart_id = np3.id
inner join namepart np32 on npr3.parent_id   = np32.id
left join useraccount ua_r on npr3.requestedby_id = ua_r.id
left join useraccount ua_p on npr3.processedby_id = ua_p.id
where np3.nameparttype = 'DEVICE_TYPE' and npr3.parent_id in
(
select npr2.namepart_id from namepartrevision npr2, namepart np2 where npr2.namepart_id = np2.id and np2.nameparttype = 'DEVICE_TYPE' and npr2.parent_id in
(select npr1.namepart_id from namepartrevision npr1, namepart np1 where npr1.namepart_id = np1.id and np1.nameparttype = 'DEVICE_TYPE' and npr1.parent_id is null)
);

-- ----------------------------------------------------------------------------------------------------
-- data names level 1
--     from devicerevision
--         referring to namepartrevision, level 1, SECTION
--         referring to namepartrevision, level 3, DEVICE_TYPE
-- ----------------------------------------------------------------------------------------------------
insert into wip_name (
    id,
    version,
    uuid,
    systemgroup_uuid,
    system_uuid,
    subsystem_uuid,
    devicetype_uuid,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select dr.id, dr.version, d.uuid,
np_s.uuid,
null,
null,
np_d.uuid,
dr.instanceindex, dr.conventionname, dr.conventionnameeqclass, dr.additionalinfo, null, false, dr.deleted,
dr.requestdate, ua_r.username as requestedBy, dr.processorComment, null, null, null
from devicerevision dr
inner join device d on dr.device_id = d.id
inner join namepart np_s on dr.section_id    = np_s.id
left outer join namepart np_d on dr.devicetype_id = np_d.id
left join useraccount ua_r on dr.requestedby_id = ua_r.id
where dr.section_id in
(
select np.id from namepart np, namepartrevision npr where np.id = npr.namepart_id and npr.parent_id is null
);
-- ----------------------------------------------------------------------------------------------------
-- data names level 2
--     from devicerevision
--         referring to namepartrevision, level 2, SECTION
--         referring to namepartrevision, level 3, DEVICE_TYPE
-- ----------------------------------------------------------------------------------------------------
insert into wip_name (
    id,
    version,
    uuid,
    systemgroup_uuid,
    system_uuid,
    subsystem_uuid,
    devicetype_uuid,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select dr.id, dr.version, d.uuid,
null,
np_s.uuid,
null,
np_d.uuid,
dr.instanceindex, dr.conventionname, dr.conventionnameeqclass, dr.additionalinfo, null, false, dr.deleted,
dr.requestdate, ua_r.username as requestedBy, dr.processorComment, null, null, null
from devicerevision dr
inner join device d on dr.device_id = d.id
inner join namepart np_s on dr.section_id    = np_s.id
left outer join namepart np_d on dr.devicetype_id = np_d.id
left join useraccount ua_r on dr.requestedby_id = ua_r.id
where dr.section_id in
(
select np.id from namepart np, namepartrevision npr where np.id = npr.namepart_id and npr.parent_id in
    (
    select np2.id from namepart np2, namepartrevision npr2 where np2.id = npr2.namepart_id and npr2.parent_id is null
    )
);

-- ----------------------------------------------------------------------------------------------------
-- data names level 3
--     from devicerevision
--         referring to namepartrevision, level 3, SECTION
--         referring to namepartrevision, level 3, DEVICE_TYPE
-- ----------------------------------------------------------------------------------------------------
insert into wip_name (
    id,
    version,
    uuid,
    systemgroup_uuid,
    system_uuid,
    subsystem_uuid,
    devicetype_uuid,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select dr.id, dr.version, d.uuid,
null,
null,
np_s.uuid,
np_d.uuid,
dr.instanceindex, dr.conventionname, dr.conventionnameeqclass, dr.additionalinfo, null, false, dr.deleted,
dr.requestdate, ua_r.username as requestedBy, dr.processorComment, null, null, null
from devicerevision dr
inner join device d on dr.device_id = d.id
inner join namepart np_s on dr.section_id    = np_s.id
left outer join namepart np_d on dr.devicetype_id = np_d.id
left join useraccount ua_r on dr.requestedby_id = ua_r.id
where dr.section_id in
(
select np.id from namepart np, namepartrevision npr where np.id = npr.namepart_id and npr.parent_id in
    (
    select np2.id from namepart np2, namepartrevision npr2 where np2.id = npr2.namepart_id and npr2.parent_id in
        (
        select np3.id from namepart np3, namepartrevision npr3 where np3.id = npr3.namepart_id and npr3.parent_id is null
        )
    )
);

-- ----------------------------------------------------------------------------------------------------
-- index
-- ----------------------------------------------------------------------------------------------------
CREATE INDEX wip_systemgroup_id_idx ON wip_systemgroup (id);
CREATE INDEX wip_systemgroup_uuid_idx ON wip_systemgroup (uuid);
CREATE INDEX wip_systemgroup_mnemonic_idx ON wip_systemgroup (mnemonic);
CREATE INDEX wip_systemgroup_status_idx ON wip_systemgroup (status);
CREATE INDEX wip_systemgroup_deleted_idx ON wip_systemgroup (deleted);

CREATE INDEX wip_system_id_idx ON wip_system (id);
CREATE INDEX wip_system_uuid_idx ON wip_system (uuid);
CREATE INDEX wip_system_parent_uuid_idx ON wip_system (parent_uuid);
CREATE INDEX wip_system_mnemonic_idx ON wip_system (mnemonic);
CREATE INDEX wip_system_status_idx ON wip_system (status);
CREATE INDEX wip_system_deleted_idx ON wip_system (deleted);

CREATE INDEX wip_subsystem_id_idx ON wip_subsystem (id);
CREATE INDEX wip_subsystem_uuid_idx ON wip_subsystem (uuid);
CREATE INDEX wip_subsystem_parent_uuid_idx ON wip_subsystem (parent_uuid);
CREATE INDEX wip_subsystem_mnemonic_idx ON wip_subsystem (mnemonic);
CREATE INDEX wip_subsystem_status_idx ON wip_subsystem (status);
CREATE INDEX wip_subsystem_deleted_idx ON wip_subsystem (deleted);

CREATE INDEX wip_discipline_id_idx ON wip_discipline (id);
CREATE INDEX wip_discipline_uuid_idx ON wip_discipline (uuid);
CREATE INDEX wip_discipline_mnemonic_idx ON wip_discipline (mnemonic);
CREATE INDEX wip_discipline_status_idx ON wip_discipline (status);
CREATE INDEX wip_discipline_deleted_idx ON wip_discipline (deleted);

CREATE INDEX wip_devicegroup_id_idx ON wip_devicegroup (id);
CREATE INDEX wip_devicegroup_uuid_idx ON wip_devicegroup (uuid);
CREATE INDEX wip_devicegroup_parent_uuid_idx ON wip_devicegroup (parent_uuid);
CREATE INDEX wip_devicegroup_mnemonic_idx ON wip_devicegroup (mnemonic);
CREATE INDEX wip_devicegroup_status_idx ON wip_devicegroup (status);
CREATE INDEX wip_devicegroup_deleted_idx ON wip_devicegroup (deleted);

CREATE INDEX wip_devicetype_id_idx ON wip_devicetype (id);
CREATE INDEX wip_devicetype_uuid_idx ON wip_devicetype (uuid);
CREATE INDEX wip_devicetype_parent_uuid_idx ON wip_devicetype (parent_uuid);
CREATE INDEX wip_devicetype_mnemonic_idx ON wip_devicetype (mnemonic);
CREATE INDEX wip_devicetype_status_idx ON wip_devicetype (status);
CREATE INDEX wip_devicetype_deleted_idx ON wip_devicetype (deleted);

CREATE INDEX wip_name_uuid_idx ON wip_name (uuid);

-- ----------------------------------------------------------------------------------------------------
-- latest
-- ----------------------------------------------------------------------------------------------------
update wip_systemgroup sg set latest = true where sg.id = (
  select max(sg2.id) from wip_systemgroup sg2 where sg2.uuid = sg.uuid and sg2.status = 'APPROVED'
);
update wip_system sys set latest = true where sys.id = (
  select max(sys2.id) from wip_system sys2 where sys2.uuid = sys.uuid and sys2.status = 'APPROVED'
);
update wip_subsystem sub set latest = true where sub.id = (
  select max(sub2.id) from wip_subsystem sub2 where sub2.uuid = sub.uuid and sub2.status = 'APPROVED'
);

update wip_discipline di set latest = true where di.id = (
  select max(di2.id) from wip_discipline di2 where di2.uuid = di.uuid and di2.status = 'APPROVED'
);
update wip_devicegroup dg set latest = true where dg.id = (
  select max(dg2.id) from wip_devicegroup dg2 where dg2.uuid = dg.uuid and dg2.status = 'APPROVED'
);
update wip_devicetype dt set latest = true where dt.id = (
  select max(dt2.id) from wip_devicetype dt2 where dt2.uuid = dt.uuid and dt2.status = 'APPROVED'
);

update wip_name en set latest = true where en.id = (
  select max(en2.id) from wip_name en2 where en2.uuid = en.uuid
);

-- ----------------------------------------------------------------------------------------------------
-- index
-- ----------------------------------------------------------------------------------------------------
CREATE INDEX wip_name_id_idx ON wip_name (id);
CREATE INDEX wip_name_systemgroup_uuid_idx ON wip_name (systemgroup_uuid);
CREATE INDEX wip_name_system_uuid_idx ON wip_name (system_uuid);
CREATE INDEX wip_name_subsystem_uuid_idx ON wip_name (subsystem_uuid);
CREATE INDEX wip_name_devicetype_uuid_idx ON wip_name (devicetype_uuid);
CREATE INDEX wip_name_convention_name_idx ON wip_name (convention_name);
CREATE INDEX wip_name_status_idx ON wip_name (status);
CREATE INDEX wip_name_deleted_idx ON wip_name (deleted);

CREATE INDEX wip_systemgroup_latest_idx ON wip_systemgroup (latest);
CREATE INDEX wip_system_latest_idx ON wip_system (latest);
CREATE INDEX wip_subsystem_latest_idx ON wip_subsystem (latest);
CREATE INDEX wip_discipline_latest_idx ON wip_discipline (latest);
CREATE INDEX wip_devicegroup_latest_idx ON wip_devicegroup (latest);
CREATE INDEX wip_devicetype_latest_idx ON wip_devicetype (latest);
CREATE INDEX wip_name_latest_idx ON wip_name (latest);

-- ----------------------------------------------------------------------------------------------------
-- sequence
-- ----------------------------------------------------------------------------------------------------
CREATE SEQUENCE wip_systemgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_subsystem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_discipline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_devicegroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_devicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE wip_name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SELECT setval('wip_systemgroup_id_seq', (select max(id) from wip_systemgroup));
SELECT setval('wip_system_id_seq', (select max(id) from wip_system));
SELECT setval('wip_subsystem_id_seq', (select max(id) from wip_subsystem));
SELECT setval('wip_discipline_id_seq', (select max(id) from wip_discipline));
SELECT setval('wip_devicegroup_id_seq', (select max(id) from wip_devicegroup));
SELECT setval('wip_devicetype_id_seq', (select max(id) from wip_devicetype));
SELECT setval('wip_name_id_seq', (select max(id) from wip_name));

ALTER SEQUENCE wip_systemgroup_id_seq OWNED BY wip_systemgroup.id;
ALTER SEQUENCE wip_system_id_seq OWNED BY wip_system.id;
ALTER SEQUENCE wip_subsystem_id_seq OWNED BY wip_subsystem.id;
ALTER SEQUENCE wip_discipline_id_seq OWNED BY wip_discipline.id;
ALTER SEQUENCE wip_devicegroup_id_seq OWNED BY wip_devicegroup.id;
ALTER SEQUENCE wip_devicetype_id_seq OWNED BY wip_devicetype.id;
ALTER SEQUENCE wip_name_id_seq OWNED BY wip_name.id;

ALTER TABLE ONLY wip_systemgroup ALTER COLUMN id SET DEFAULT nextval('wip_systemgroup_id_seq'::regclass);
ALTER TABLE ONLY wip_system ALTER COLUMN id SET DEFAULT nextval('wip_system_id_seq'::regclass);
ALTER TABLE ONLY wip_subsystem ALTER COLUMN id SET DEFAULT nextval('wip_subsystem_id_seq'::regclass);
ALTER TABLE ONLY wip_discipline ALTER COLUMN id SET DEFAULT nextval('wip_discipline_id_seq'::regclass);
ALTER TABLE ONLY wip_devicegroup ALTER COLUMN id SET DEFAULT nextval('wip_devicegroup_id_seq'::regclass);
ALTER TABLE ONLY wip_devicetype ALTER COLUMN id SET DEFAULT nextval('wip_devicetype_id_seq'::regclass);
ALTER TABLE ONLY wip_name ALTER COLUMN id SET DEFAULT nextval('wip_name_id_seq'::regclass);

-- ----------------------------------------------------------------------------------------------------
-- primary key
-- ----------------------------------------------------------------------------------------------------
ALTER TABLE wip_systemgroup ADD CONSTRAINT wip_systemgroup_pk PRIMARY KEY (id);
ALTER TABLE wip_system ADD CONSTRAINT wip_system_pk PRIMARY KEY (id);
ALTER TABLE wip_subsystem ADD CONSTRAINT wip_subsystem_pk PRIMARY KEY (id);
ALTER TABLE wip_discipline ADD CONSTRAINT wip_discipline_pk PRIMARY KEY (id);
ALTER TABLE wip_devicegroup ADD CONSTRAINT wip_devicegroup_pk PRIMARY KEY (id);
ALTER TABLE wip_devicetype ADD CONSTRAINT wip_devicetype_pk PRIMARY KEY (id);
ALTER TABLE wip_name ADD CONSTRAINT wip_name_pk PRIMARY KEY (id);

-- ----------------------------------------------------------------------------------------------------
-- foreign key
-- ----------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------
-- function
-- ----------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_system_structure(convention_name text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    pos int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        RETURN substr(convention_name, 1, pos-1);
    END IF;
    RETURN convention_name;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_device_structure(convention_name text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            mnemonic_path = substr(mnemonic_path, strpos(mnemonic_path, '-')+1);
            RETURN reverse(mnemonic_path);
        ELSIF nbr_delimiters = 1 then
            return mnemonic_path;
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_instance_index(convention_name text)
 RETURNS text
 LANGUAGE plpgsql
AS
$$
DECLARE
    len int;
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            len = length(mnemonic_path);
            pos = strpos(mnemonic_path, '-');
            mnemonic_path = reverse(mnemonic_path);
            RETURN substr(mnemonic_path, len - pos + 2);
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_system(system_uuid text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    system_mnemonic text;
BEGIN
    select mnemonic into system_mnemonic from wip_system where uuid = system_uuid and latest = true;

    if system_mnemonic is not null then
        return system_mnemonic;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_subsystem(subsystem_uuid text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    subsystem_mnemonic text;
    system_uuid text;
    system_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into system_uuid, subsystem_mnemonic from wip_subsystem where uuid = subsystem_uuid and latest = true;
    select mnemonic into system_mnemonic from wip_system where uuid = system_uuid and latest = true;

    if system_mnemonic is not null and subsystem_mnemonic is not null then
        return concat(system_mnemonic, '-', subsystem_mnemonic);
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_devicegroup(devicegroup_uuid text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid into discipline_uuid from wip_devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from wip_discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null then
        return discipline_mnemonic;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION wip_get_mnemonic_path_devicetype(devicetype_uuid text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    devicetype_mnemonic text;
    devicegroup_uuid text;
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into devicegroup_uuid, devicetype_mnemonic from wip_devicetype where uuid = devicetype_uuid and latest = true;
    select parent_uuid, mnemonic into discipline_uuid from wip_devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from wip_discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null and devicetype_mnemonic is not null then
        return concat(discipline_mnemonic, '-', devicetype_mnemonic);
    elsif devicetype_mnemonic is not null then
        return devicetype_mnemonic;
    else
        return null;
    end if;
END;
$$;

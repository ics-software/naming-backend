-- ----------------------------------------------------------------------------------------------------
-- About
--     migration script
--     postgresql 9.6.7
-- Content
--     structures
--     names
--     data structures level 1
--         systemgroup    +    audit
--         discipline     +    audit
--     data structures level 2
--         system         +    audit
--         devicegroup    +    audit
--     data structures level 3
--         subsystem      +    audit
--         devicetype     +    audit
--     data names
--         systemgroup_id    and    not devicetype_id    +    audit
--         system_id         and    not devicetype_id    +    audit
--         subsystem_id      and    not devicetype_id    +    audit
--         systemgroup_id    and    devicetype_id        +    audit
--         system_id         and    devicetype_id        +    audit
--         subsystem_id      and    devicetype_id        +    audit
--     index
--     sequence
--     primary key
--     foreign key
--     function
-- Note
--     about audit tables and data
--         data comes from "wip" tables
--     order of items is important
--     notice prefix "wip_" meaning "work in progress" as tables are temporary
-- ----------------------------------------------------------------------------------------------------
--     data structures (parent_id)
--     data names
--         selection of proper id to refer to - systemgroup, system, subsystem, devicetype
--                     exclude content (with latest) before latest
--                     exclude content (with latest) after  latest (cancelled, rejected)
--                     keep most recent content (without latest)   (to have most recent in line of uuid without latest)
--             <-->
--                     select * from structure s
--                     where (
--                                 not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true))
--                             and not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
--                             and not (not exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from structure s2 where s2."uuid" = s."uuid" and s2.latest=false))
--                     )
-- ----------------------------------------------------------------------------------------------------
--     retrieve parent id for audit structure
--         1 or 2 or 3
--                     1.  get id from parent that is approved and was processed prior to child or within 10 seconds
--                     2.  get id from parent that is approved and was requested prior to child or within 10 minutes
--                     3.  get id from parent that was requested prior to child or within 10 minutes
--             <-->
--                     coalesce (
--                     (select max(s3.id) from parent s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.processed < (s.processed + interval '10 seconds')),
--                     (select max(s3.id) from parent s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.requested < (s.processed + interval '10 minutes')),
--                     (select max(s3.id) from parent s3 where s3."uuid" = s.parent_uuid and s3.requested < (s.processed + interval '10 minutes'))
--                     )
-- ----------------------------------------------------------------------------------------------------
--     non-audit tables contain valid
--     audit tables contain obsolete     <---
-- ----------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------
-- structures
-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS systemgroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS system (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_id bigint NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS subsystem (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_id bigint NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS discipline (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS devicegroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_id bigint NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS devicetype (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_id bigint NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
-- name
-- ----------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS name (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    systemgroup_id bigint,
    system_id bigint,
    subsystem_id bigint,
    devicetype_id bigint,
    instance_index text,
    convention_name text,
    convention_name_equivalence text,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
-- audit
-- ----------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS audit_structure (
    audit_id bigint NOT NULL,
    audit_version integer,
    audit_table text NOT NULL,
    audit_operation text,
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_id bigint,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

CREATE TABLE IF NOT EXISTS audit_name (
    audit_id bigint NOT NULL,
    audit_version integer,
    audit_table text NOT NULL,
    audit_operation text,
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    systemgroup_id bigint,
    system_id bigint,
    subsystem_id bigint,
    devicetype_id bigint,
    instance_index text,
    convention_name text,
    convention_name_equivalence text,
    description text,
    status text,
    -- latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);

-- ----------------------------------------------------------------------------------------------------
-- data structures level 1
--     systemgroup    +    audit
--     discipline     +    audit
-- ----------------------------------------------------------------------------------------------------
--     from wip_systemgroup
--     from wip_discipline
-- ----------------------------------------------------------------------------------------------------
insert into systemgroup (
    id,
    version,
    uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid, s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_systemgroup s
where (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'systemgroup', null,
s.id, s.version, s.uuid, null, s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_systemgroup s
where not (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
insert into discipline (
    id,
    version,
    uuid,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid, s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_discipline s
where (
            not (exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'discipline', null,
s.id, s.version, s.uuid, null, s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_discipline s
where not (
            not (exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_discipline s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
-- data structures level 2
--     system         +    audit
--     devicegroup    +    audit
-- ----------------------------------------------------------------------------------------------------
--     from wip_system
--     from wip_devicegroup
-- ----------------------------------------------------------------------------------------------------
insert into system (
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid,
(select sg.id from wip_systemgroup sg
where sg.uuid = s.parent_uuid and (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=true) and sg.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=true) and sg.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=true) and sg.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=true) and sg.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = sg."uuid" and s2.latest=false))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_system s
where (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'system', null,
s.id, s.version, s.uuid,
(coalesce (
(select max(s3.id) from wip_systemgroup s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.processed < (s.processed + interval '10 seconds')),
(select max(s3.id) from wip_systemgroup s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.requested < (s.processed + interval '10 minutes')),
(select max(s3.id) from wip_systemgroup s3 where s3."uuid" = s.parent_uuid and s3.requested < (s.processed + interval '10 minutes'))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_system s
where not (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
insert into devicegroup (
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid,
(select di.id from wip_discipline di
where di.uuid = s.parent_uuid and (
            not (exists (select s2.id from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=true) and di.id < (select s2.id from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=true) and di.id > (select s2.id from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=true) and di.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=true) and di.id < (select max(s2.id) from wip_discipline s2 where s2."uuid" = di."uuid" and s2.latest=false))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_devicegroup s
where (
            not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'devicegroup', null,
s.id, s.version, s.uuid,
(coalesce (
(select max(s3.id) from wip_discipline s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.processed < (s.processed + interval '10 seconds')),
(select max(s3.id) from wip_discipline s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.requested < (s.processed + interval '10 minutes')),
(select max(s3.id) from wip_discipline s3 where s3."uuid" = s.parent_uuid and s3.requested < (s.processed + interval '10 minutes'))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_devicegroup s
where not (
            not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicegroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
-- data structures level 3
--     subsystem      +    audit
--     devicetype     +    audit
-- ----------------------------------------------------------------------------------------------------
--     from wip_subsystem
--     from wip_devicetype
-- ----------------------------------------------------------------------------------------------------
insert into subsystem (
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid,
(select sys.id from wip_system sys
where sys.uuid = s.parent_uuid and (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=true) and sys.id < (select s2.id from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=true) and sys.id > (select s2.id from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=true) and sys.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=true) and sys.id < (select max(s2.id) from wip_system s2 where s2."uuid" = sys."uuid" and s2.latest=false))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_subsystem s
where (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'subsystem', null,
s.id, s.version, s.uuid,
(coalesce (
(select max(s3.id) from wip_system s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.processed < (s.processed + interval '10 seconds')),
(select max(s3.id) from wip_system s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.requested < (s.processed + interval '10 minutes')),
(select max(s3.id) from wip_system s3 where s3."uuid" = s.parent_uuid and s3.requested < (s.processed + interval '10 minutes'))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_subsystem s
where not (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
insert into devicetype (
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select
s.id, s."version", s.uuid,
(select dg.id from wip_devicegroup dg
where dg.uuid = s.parent_uuid and (
            not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=true) and dg.id < (select s2.id from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=true) and dg.id > (select s2.id from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=true) and dg.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=true) and dg.id < (select max(s2.id) from wip_devicegroup s2 where s2."uuid" = dg."uuid" and s2.latest=false))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_devicetype s
where (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

insert into audit_structure (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    parent_id,
    mnemonic,
    mnemonic_equivalence,
    ordering,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select s.id, s."version", 'devicetype', null,
s.id, s.version, s.uuid,
(coalesce (
(select max(s3.id) from wip_devicegroup s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.processed < (s.processed + interval '10 seconds')),
(select max(s3.id) from wip_devicegroup s3 where s3."uuid" = s.parent_uuid and s3.status = 'APPROVED' and s3.requested < (s.processed + interval '10 minutes')),
(select max(s3.id) from wip_devicegroup s3 where s3."uuid" = s.parent_uuid and s3.requested < (s.processed + interval '10 minutes'))
)),
s.mnemonic, s.mnemonic_equivalence, s.ordering, s.description, s.status, s.deleted,
s.requested, s.requested_by, s.requested_comment, s.processed, s.processed_by, s.processed_comment from wip_devicetype s
where not (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
);

-- ----------------------------------------------------------------------------------------------------
-- data names
--     1) systemgroup_id    and    not devicetype_id    +    audit
--     2) system_id         and    not devicetype_id    +    audit
--     3) subsystem_id      and    not devicetype_id    +    audit
--     4) systemgroup_id    and    devicetype_id        +    audit
--     5) system_id         and    devicetype_id        +    audit
--     6) subsystem_id      and    devicetype_id        +    audit
-- ----------------------------------------------------------------------------------------------------
--     from wip_systemgroup
--     from wip_systemg
--     from wip_subsystem
--     from wip_systemgroup    +    wip_devicetype
--     from wip_systemg        +    wip_devicetype
--     from wip_subsystem      +    wip_devicetype
-- ----------------------------------------------------------------------------------------------------
-- 1)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
(select s.id from wip_systemgroup s
where s.uuid = n.systemgroup_uuid and (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is not null and n.system_uuid is null and n.subsystem_uuid is null and n.devicetype_uuid is null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
(select s.id from wip_systemgroup s
where s.uuid = n.systemgroup_uuid and (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is not null and n.system_uuid is null and n.subsystem_uuid is null and n.devicetype_uuid is null;

-- ----------------------------------------------------------------------------------------------------
-- 2)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
null,
(select s.id from wip_system s
where s.uuid = n.system_uuid and (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is null and n.system_uuid is not null and n.subsystem_uuid is null and n.devicetype_uuid is null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
null,
(select s.id from wip_system s
where s.uuid = n.system_uuid and (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is null and n.system_uuid is not null and n.subsystem_uuid is null and n.devicetype_uuid is null;

-- ----------------------------------------------------------------------------------------------------
-- 3)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
null,
null,
(select s.id from wip_subsystem s
where s.uuid = n.subsystem_uuid and (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is null and n.system_uuid is null and n.subsystem_uuid is not null and n.devicetype_uuid is null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
null,
null,
(select s.id from wip_subsystem s
where s.uuid = n.subsystem_uuid and (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is null and n.system_uuid is null and n.subsystem_uuid is not null and n.devicetype_uuid is null;

-- ----------------------------------------------------------------------------------------------------
-- 4)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
(select s.id from wip_systemgroup s
where s.uuid = n.systemgroup_uuid and (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
(select s.id from wip_devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is not null and n.system_uuid is null and n.subsystem_uuid is null and n.devicetype_uuid is not null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
(select s.id from wip_systemgroup s
where s.uuid = n.systemgroup_uuid and (
            not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_systemgroup s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
null,
(select s.id from devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is not null and n.system_uuid is null and n.subsystem_uuid is null and n.devicetype_uuid is not null;

-- ----------------------------------------------------------------------------------------------------
-- 5)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
null,
(select s.id from wip_system s
where s.uuid = n.system_uuid and (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
(select s.id from wip_devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is null and n.system_uuid is not null and n.subsystem_uuid is null and n.devicetype_uuid is not null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
null,
(select s.id from wip_system s
where s.uuid = n.system_uuid and (
            not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_system s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
null,
(select s.id from wip_devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is null and n.system_uuid is not null and n.subsystem_uuid is null and n.devicetype_uuid is not null;

-- ----------------------------------------------------------------------------------------------------
-- 6)
insert into name (
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", n.uuid,
null,
null,
(select s.id from wip_subsystem s
where s.uuid = n.subsystem_uuid and (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
(select s.id from wip_devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where not (n.latest = false)
and n.systemgroup_uuid is null and n.system_uuid is null and n.subsystem_uuid is not null and n.devicetype_uuid is not null;

insert into audit_name (
    audit_id,
    audit_version,
    audit_table,
    audit_operation,
    id,
    version,
    uuid,
    systemgroup_id,
    system_id,
    subsystem_id,
    devicetype_id,
    instance_index,
    convention_name,
    convention_name_equivalence,
    description,
    status,
    -- latest,
    deleted,
    requested,
    requested_by,
    requested_comment,
    processed,
    processed_by,
    processed_comment
)
select n.id, n."version", 'name', null,
n.id, n."version", n.uuid,
null,
null,
(select s.id from wip_subsystem s
where s.uuid = n.subsystem_uuid and (
            not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_subsystem s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
(select s.id from wip_devicetype s
where s.uuid = n.devicetype_uuid and (
            not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true))
        and not (exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
        and not (not exists (select s2.id from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from wip_devicetype s2 where s2."uuid" = s."uuid" and s2.latest=false))
)),
n.instance_index, n.convention_name, n.convention_name_equivalence, n.description, n.status, n.deleted,
n.requested, n.requested_by, n.requested_comment, n.processed, n.processed_by, n.processed_comment from wip_name n
where
(n.latest = false) and
n.systemgroup_uuid is null and n.system_uuid is null and n.subsystem_uuid is not null and n.devicetype_uuid is not null;

-- ----------------------------------------------------------------------------------------------------
-- index
-- ----------------------------------------------------------------------------------------------------
CREATE INDEX systemgroup_id_idx ON systemgroup (id);
CREATE INDEX systemgroup_uuid_idx ON systemgroup (uuid);
CREATE INDEX systemgroup_mnemonic_idx ON systemgroup (mnemonic);
CREATE INDEX systemgroup_status_idx ON systemgroup (status);
CREATE INDEX systemgroup_deleted_idx ON systemgroup (deleted);

CREATE INDEX system_id_idx ON system (id);
CREATE INDEX system_uuid_idx ON system (uuid);
CREATE INDEX system_parent_id_idx ON system (parent_id);
CREATE INDEX system_mnemonic_idx ON system (mnemonic);
CREATE INDEX system_status_idx ON system (status);
CREATE INDEX system_deleted_idx ON system (deleted);

CREATE INDEX subsystem_id_idx ON subsystem (id);
CREATE INDEX subsystem_uuid_idx ON subsystem (uuid);
CREATE INDEX subsystem_parent_id_idx ON subsystem (parent_id);
CREATE INDEX subsystem_mnemonic_idx ON subsystem (mnemonic);
CREATE INDEX subsystem_status_idx ON subsystem (status);
CREATE INDEX subsystem_deleted_idx ON subsystem (deleted);

CREATE INDEX discipline_id_idx ON discipline (id);
CREATE INDEX discipline_uuid_idx ON discipline (uuid);
CREATE INDEX discipline_mnemonic_idx ON discipline (mnemonic);
CREATE INDEX discipline_status_idx ON discipline (status);
CREATE INDEX discipline_deleted_idx ON discipline (deleted);

CREATE INDEX devicegroup_id_idx ON devicegroup (id);
CREATE INDEX devicegroup_uuid_idx ON devicegroup (uuid);
CREATE INDEX devicegroup_parent_id_idx ON devicegroup (parent_id);
CREATE INDEX devicegroup_mnemonic_idx ON devicegroup (mnemonic);
CREATE INDEX devicegroup_status_idx ON devicegroup (status);
CREATE INDEX devicegroup_deleted_idx ON devicegroup (deleted);

CREATE INDEX devicetype_id_idx ON devicetype (id);
CREATE INDEX devicetype_uuid_idx ON devicetype (uuid);
CREATE INDEX devicetype_parent_id_idx ON devicetype (parent_id);
CREATE INDEX devicetype_mnemonic_idx ON devicetype (mnemonic);
CREATE INDEX devicetype_status_idx ON devicetype (status);
CREATE INDEX devicetype_deleted_idx ON devicetype (deleted);

CREATE INDEX name_id_idx ON name (id);
CREATE INDEX name_uuid_idx ON name (uuid);
CREATE INDEX name_systemgroup_id_idx ON name (systemgroup_id);
CREATE INDEX name_system_id_idx ON name (system_id);
CREATE INDEX name_subsystem_id_idx ON name (subsystem_id);
CREATE INDEX name_devicetype_id_idx ON name (devicetype_id);
CREATE INDEX name_convention_name_idx ON name (convention_name);
CREATE INDEX name_status_idx ON name (status);
CREATE INDEX name_deleted_idx ON name (deleted);

CREATE INDEX audit_structure_audit_id_idx ON audit_structure (audit_id);
CREATE INDEX audit_structure_audit_table_idx ON audit_structure (audit_table);
CREATE INDEX audit_structure_audit_operation_idx ON audit_structure (audit_operation);
CREATE INDEX audit_structure_id_idx ON audit_structure (id);
CREATE INDEX audit_structure_uuid_idx ON audit_structure (uuid);
CREATE INDEX audit_structure_parent_id_idx ON audit_structure (parent_id);
CREATE INDEX audit_structure_mnemonic_idx ON audit_structure (mnemonic);
CREATE INDEX audit_structure_status_idx ON audit_structure (status);
CREATE INDEX audit_structure_deleted_idx ON audit_structure (deleted);

CREATE INDEX audit_name_audit_id_idx ON audit_name (audit_id);
CREATE INDEX audit_name_audit_table_idx ON audit_name (audit_table);
CREATE INDEX audit_name_audit_operation_idx ON audit_name (audit_operation);
CREATE INDEX audit_name_id_idx ON audit_name (id);
CREATE INDEX audit_name_uuid_idx ON audit_name (uuid);
CREATE INDEX audit_name_systemgroup_id_idx ON audit_name (systemgroup_id);
CREATE INDEX audit_name_system_id_idx on audit_name (system_id);
CREATE INDEX audit_name_subsystem_id_idx ON audit_name (subsystem_id);
CREATE INDEX audit_name_devicetype_id_idx ON audit_name (devicetype_id);
CREATE INDEX audit_name_convention_name_idx ON audit_name (convention_name);
CREATE INDEX audit_name_status_idx ON audit_name (status);
CREATE INDEX audit_name_deleted_idx ON audit_name (deleted);

-- ----------------------------------------------------------------------------------------------------
-- sequence
-- ----------------------------------------------------------------------------------------------------
CREATE SEQUENCE systemgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE subsystem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE discipline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE devicegroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE devicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE audit_structure_audit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE SEQUENCE audit_name_audit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

SELECT setval('systemgroup_id_seq', (select max(id) from systemgroup));
SELECT setval('system_id_seq', (select max(id) from system));
SELECT setval('subsystem_id_seq', (select max(id) from subsystem));
SELECT setval('discipline_id_seq', (select max(id) from discipline));
SELECT setval('devicegroup_id_seq', (select max(id) from devicegroup));
SELECT setval('devicetype_id_seq', (select max(id) from devicetype));
SELECT setval('name_id_seq', (select max(id) from name));
SELECT setval('audit_structure_audit_id_seq', (select max(audit_id) from audit_structure));
SELECT setval('audit_name_audit_id_seq', (select max(audit_id) from audit_name));

ALTER SEQUENCE systemgroup_id_seq OWNED BY systemgroup.id;
ALTER SEQUENCE system_id_seq OWNED BY system.id;
ALTER SEQUENCE subsystem_id_seq OWNED BY subsystem.id;
ALTER SEQUENCE discipline_id_seq OWNED BY discipline.id;
ALTER SEQUENCE devicegroup_id_seq OWNED BY devicegroup.id;
ALTER SEQUENCE devicetype_id_seq OWNED BY devicetype.id;
ALTER SEQUENCE name_id_seq OWNED BY name.id;
ALTER SEQUENCE audit_structure_audit_id_seq OWNED BY audit_structure.audit_id;
ALTER SEQUENCE audit_name_audit_id_seq OWNED BY audit_name.audit_id;

ALTER TABLE ONLY systemgroup ALTER COLUMN id SET DEFAULT nextval('systemgroup_id_seq'::regclass);
ALTER TABLE ONLY system ALTER COLUMN id SET DEFAULT nextval('system_id_seq'::regclass);
ALTER TABLE ONLY subsystem ALTER COLUMN id SET DEFAULT nextval('subsystem_id_seq'::regclass);
ALTER TABLE ONLY discipline ALTER COLUMN id SET DEFAULT nextval('discipline_id_seq'::regclass);
ALTER TABLE ONLY devicegroup ALTER COLUMN id SET DEFAULT nextval('devicegroup_id_seq'::regclass);
ALTER TABLE ONLY devicetype ALTER COLUMN id SET DEFAULT nextval('devicetype_id_seq'::regclass);
ALTER TABLE ONLY name ALTER COLUMN id SET DEFAULT nextval('name_id_seq'::regclass);
ALTER TABLE ONLY audit_structure ALTER COLUMN audit_id SET DEFAULT nextval('audit_structure_audit_id_seq'::regclass);
ALTER TABLE ONLY audit_name ALTER COLUMN audit_id SET DEFAULT nextval('audit_name_audit_id_seq'::regclass);

-- ----------------------------------------------------------------------------------------------------
-- primary key
-- ----------------------------------------------------------------------------------------------------
ALTER TABLE systemgroup ADD CONSTRAINT systemgroup_pk PRIMARY KEY (id);
ALTER TABLE system ADD CONSTRAINT system_pk PRIMARY KEY (id);
ALTER TABLE subsystem ADD CONSTRAINT subsystem_pk PRIMARY KEY (id);
ALTER TABLE discipline ADD CONSTRAINT discipline_pk PRIMARY KEY (id);
ALTER TABLE devicegroup ADD CONSTRAINT devicegroup_pk PRIMARY KEY (id);
ALTER TABLE devicetype ADD CONSTRAINT devicetype_pk PRIMARY KEY (id);
ALTER TABLE name ADD CONSTRAINT name_pk PRIMARY KEY (id);
ALTER TABLE audit_structure ADD CONSTRAINT audit_structure_pk PRIMARY KEY (audit_id);
ALTER TABLE audit_name ADD CONSTRAINT audit_name_pk PRIMARY KEY (audit_id);

-- ----------------------------------------------------------------------------------------------------
-- foreign key
-- ----------------------------------------------------------------------------------------------------
ALTER TABLE ONLY system ADD CONSTRAINT fk_system_systemgroup FOREIGN KEY (parent_id) REFERENCES systemgroup(id);
ALTER TABLE ONLY subsystem ADD CONSTRAINT fk_subsystem_system FOREIGN KEY (parent_id) REFERENCES system(id);
ALTER TABLE ONLY devicegroup ADD CONSTRAINT fk_devicegroup_discipline FOREIGN KEY (parent_id) REFERENCES discipline(id);
ALTER TABLE ONLY devicetype ADD CONSTRAINT fk_devicetype_devicegroup FOREIGN KEY (parent_id) REFERENCES devicegroup(id);
ALTER TABLE ONLY name ADD CONSTRAINT fk_name_systemgroup FOREIGN KEY (systemgroup_id) REFERENCES systemgroup(id);
ALTER TABLE ONLY name ADD CONSTRAINT fk_name_system FOREIGN KEY (system_id) REFERENCES system(id);
ALTER TABLE ONLY name ADD CONSTRAINT fk_name_subsystem FOREIGN KEY (subsystem_id) REFERENCES subsystem(id);
ALTER TABLE ONLY name ADD CONSTRAINT fk_name_devicetype FOREIGN KEY (devicetype_id) REFERENCES devicetype(id);

-- ----------------------------------------------------------------------------------------------------
-- function
-- ----------------------------------------------------------------------------------------------------
-- same as V4
--     get_mnemonic_path_system_structure(convention_name text)
--     get_mnemonic_path_device_structure(convention_name text)
--     get_instance_index(convention_name text)

CREATE OR REPLACE FUNCTION get_mnemonic_path_system_structure(convention_name text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    pos int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        RETURN substr(convention_name, 1, pos-1);
    END IF;
    RETURN convention_name;
END;
$$;

CREATE OR REPLACE FUNCTION get_mnemonic_path_device_structure(convention_name text)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            mnemonic_path = substr(mnemonic_path, strpos(mnemonic_path, '-')+1);
            RETURN reverse(mnemonic_path);
        ELSIF nbr_delimiters = 1 then
            return mnemonic_path;
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;

CREATE OR REPLACE FUNCTION get_instance_index(convention_name text)
 RETURNS text
 LANGUAGE plpgsql
AS
$$
DECLARE
    len int;
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            len = length(mnemonic_path);
            pos = strpos(mnemonic_path, '-');
            mnemonic_path = reverse(mnemonic_path);
            RETURN substr(mnemonic_path, len - pos + 2);
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;

CREATE OR REPLACE FUNCTION get_parent_uuid_system(system_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    parent_uuid text;
BEGIN
    select uuid into parent_uuid from systemgroup where id = (select parent_id from system where id = system_id);

    if parent_uuid is not null then
        return parent_uuid;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_parent_uuid_subsystem(subsystem_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    parent_uuid text;
BEGIN
    select uuid into parent_uuid from system where id = (select parent_id from subsystem where id = subsystem_id);

    if parent_uuid is not null then
        return parent_uuid;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_parent_uuid_devicegroup(devicegroup_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    parent_uuid text;
BEGIN
    select uuid into parent_uuid from discipline where id = (select parent_id from devicegroup where id = devicegroup_id);

    if parent_uuid is not null then
        return parent_uuid;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_parent_uuid_devicetype(devicetype_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    parent_uuid text;
BEGIN
    select uuid into parent_uuid from devicegroup where id = (select parent_id from devicetype where id = devicetype_id);

    if parent_uuid is not null then
        return parent_uuid;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_mnemonic_path_system(system_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    system_mnemonic text;
BEGIN
    select mnemonic into system_mnemonic from system where id = system_id;

    if system_mnemonic is not null then
        return system_mnemonic;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_mnemonic_path_subsystem(subsystem_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    subsystem_mnemonic text;
    system_id bigint;
    system_mnemonic text;
BEGIN
    select parent_id, mnemonic into system_id, subsystem_mnemonic from subsystem where id = subsystem_id;
    select mnemonic into system_mnemonic from system where id = system_id;

    if system_mnemonic is not null and subsystem_mnemonic is not null then
        return concat(system_mnemonic, '-', subsystem_mnemonic);
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_mnemonic_path_devicegroup(devicegroup_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    discipline_id bigint;
    discipline_mnemonic text;
BEGIN
    select parent_id into discipline_id from devicegroup where id = devicegroup_id;
    select mnemonic into discipline_mnemonic from discipline where id = discipline_id;

    if discipline_mnemonic is not null then
        return discipline_mnemonic;
    else
        return null;
    end if;
END;
$$;

CREATE OR REPLACE FUNCTION get_mnemonic_path_devicetype(devicetype_id bigint)
    RETURNS text
    LANGUAGE plpgsql
AS
$$
DECLARE
    devicetype_mnemonic text;
    devicegroup_id bigint;
    discipline_id bigint;
    discipline_mnemonic text;
BEGIN
    select parent_id, mnemonic into devicegroup_id, devicetype_mnemonic from devicetype where id = devicetype_id;
    select parent_id, mnemonic into discipline_id from devicegroup where id = devicegroup_id;
    select mnemonic into discipline_mnemonic from discipline where id = discipline_id;

    if discipline_mnemonic is not null and devicetype_mnemonic is not null then
        return concat(discipline_mnemonic, '-', devicetype_mnemonic);
    elsif devicetype_mnemonic is not null then
        return devicetype_mnemonic;
    else
        return null;
    end if;
END;
$$;

-- ----------------------------------------------------------------------------------------------------
-- About
--     migration script
--     postgresql 9.6.7
-- Content
--     user notification table
-- Note
--     remove table as it is no longer used
--     users to which to send notifications to be handled by configuration
-- ----------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------
-- user notification
-- ----------------------------------------------------------------------------------------------------
ALTER TABLE ONLY user_notification
   DROP CONSTRAINT user_notification_pk;

DROP TABLE user_notification;

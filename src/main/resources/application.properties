# ------------------------------------------------------------------------------
# Naming
# ------------------------------------------------------------------------------

# configuration
spring.config.import=optional:file:.env[.properties]

# logging
#     includes - comma-separated list of strings to filter stack trace for inclusion (ref. whitelist)
#     excludes - comma-separated list of strings to filter stack trace for exclusion (ref. blacklist)
#     stack trace length - max number of stack trace elements to log, negative value will log entire stack trace
naming.logging.filter.includes=${NAMING_LOGGING_FILTER_INCLUDES:org.openepics.names}
naming.logging.filter.excludes=${NAMING_LOGGING_FILTER_EXCLUDES:org.openepics.names.util.ExceptionUtil}
naming.logging.stacktrace.length=${NAMING_LOGGING_STACKTRACE_LENGTH:20}
logging.level.org.openepics.names=${NAMING_LOGGING_LEVEL:INFO}
logging.level.org.springframework.web=INFO
logging.level.org.hibernate.SQL=INFO
spring.http.log-request-details=true

# security
#     enable
#         disabled by default - allow run of application without association to particular way of authentication & authorization
#     encryption
#     token
#     rbac
naming.security.enabled=${NAMING_SECURITY_ENABLED:false}
aes.key=${AES_KEY:aes_secret_key}
jwt.secret=${JWT_SECRET:secret_password_to_hash_token}
jwt.expire.in.minutes=${JWT_EXP_MIN:240}
rbac.server.address=${RBAC_SERVER_ADDRESS}

# mail, notification
#     administrator mail - comma-separated list of email addresses
#     notification  mail - comma-separated list of email addresses
#     notification job scheduled at 5 am every day by default
naming.notify.schedule=${NAMING_NOTIFY_SCHEDULE:0 0 5 * * *}
naming.mail.administrator=${NAMING_MAIL_ADMINISTRATOR:}
naming.mail.notification=${NAMING_MAIL_NOTIFICATION:false}
naming.mail.notification.names=${NAMING_MAIL_NOTIFICATION_NAMES:}
naming.mail.notification.structures=${NAMING_MAIL_NOTIFICATION_STRUCTURES:}
naming.mail.from=${NAMING_MAIL_FROM:jboss@ess.eu}
naming.smtp.host=${NAMING_SMTP_HOST:mail.esss.lu.se}
naming.smtp.port=${NAMING_SMTP_PORT:587}
naming.smtp.username=${NAMING_SMTP_USERNAME:}
naming.smtp.password=${NAMING_SMTP_PASSWORD:}
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
spring.mail.properties.mail.smtp.connectiontimeout=5000
spring.mail.properties.mail.smtp.timeout=3000
spring.mail.properties.mail.smtp.writetimeout=5000
spring.mail.debug=false

# data
#     datasource
#     jpa
spring.datasource.url=${NAMING_DATABASE_URL:jdbc:postgresql://postgres:5432/discs_names}
spring.datasource.username=${NAMING_DATABASE_USERNAME:discs_names}
spring.datasource.password=${NAMING_DATABASE_PASSWORD:discs_names}
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.hibernate.ddl-auto=none
spring.jpa.hibernate.show-sql=true

# open api
#     doc
app.version=0.0.1-SNAPSHOT
openapi.externaldocs.description=ESS Naming Convention
openapi.externaldocs.url=https://chess.esss.lu.se/enovia/link/ESS-0000757/21308.51166.45568.45993/valid
openapi.info.contact.email=Icsscontrolsystemsupport@ess.eu
openapi.info.contact.name=Support
openapi.info.contact.url=https://jira.esss.lu.se/projects/NT/issues
openapi.info.description=\
  Handle Naming of ESS wide physical and logical devices according to ESS Naming Convention \n\n \
  ![Naming data and access](/images/naming_backend_data_access.png "Naming data and access") \n\n \
  Here are some useful links \n \
  - [Naming](https://confluence.esss.lu.se/display/SW/Naming+Tool%2C+Cable+DB%2C+CCDB%2C+IOC+Factory%2C+RBAC) in ICS Software toolchain \n \
  - [Introduction to Naming REST API](/pdfs/naming_rest_api_brief_introduction.pdf) + [Lifecycle](/images/naming_backend_lifecycle.png) + [Cheat Sheet](/pdfs/naming_rest_api_cheat_sheet.pdf) \n \
  - [How to use](/pdfs/naming_rest_api_excel_guide.pdf) Excel templates for [names](/templates/NameElementCommand.xlsx) + [structures](/templates/StructureElementCommand.xlsx) \n \
  - [Migration of Naming REST API](/pdfs/naming_rest_api_migration.pdf) \n\n \
  Note \n \
  - Observe which fields to use for operations client to server. \n \
  - Obsolete values are not shown unless history is requested. \n \
  - Regular expressions are not supported for searches. Regex-like behavior is available with _ underscore, 0 or 1 occurrences of any character, % percent, any number of any character.
openapi.info.license.name=Copyright (C) 2022 European Spallation Source ERIC.
openapi.info.title=Naming REST API

# springdoc
#     api
#     swagger
#         url used for notification
springdoc.api-docs.path=${API_DOCS_PATH:/api-docs}
springdoc.swagger-ui.path=${SWAGGER_UI_PATH:/swagger-ui.html}
springdoc.swagger-ui.tagsSorter=alpha
springdoc.swagger-ui.operationsSorter=method
naming.swagger.url=${NAMING_SWAGGER_URL:http://localhost:8080/swagger-ui.html}

# metrics
#     actuator
management.endpoints.web.exposure.include=prometheus

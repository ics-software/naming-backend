/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

/**
 * Starting point of SpringBoot application.
 *
 * @author Lars Johansson
 */
@SpringBootApplication
public class NamingApplication {

    public static void main(String[] args) {
        SpringApplication.run(NamingApplication.class, args);
    }

    @Value("${openapi.externaldocs.description}") String openapiExternaldocsDescription;
    @Value("${openapi.externaldocs.url}") String openapiExternaldocsUrl;
    @Value("${openapi.info.contact.email}") String openapiInfoContactEmail;
    @Value("${openapi.info.contact.name}") String openapiInfoContactName;
    @Value("${openapi.info.contact.url}") String openapiInfoContactUrl;
    @Value("${openapi.info.description}") String openapiInfoDescription;
    @Value("${openapi.info.license.name}") String openapiInfoLicenseName;
    @Value("${openapi.info.title}") String openapiInfoTitle;

    @Bean
    public OpenAPI customOpenAPI(@Value("${app.version}") String appVersion) {
        return new OpenAPI()
                .externalDocs(new ExternalDocumentation()
                        .description(openapiExternaldocsDescription)
                        .url(openapiExternaldocsUrl))
                .info(new Info()
                        .contact(new Contact()
                                .email(openapiInfoContactEmail)
                                .name(openapiInfoContactName)
                                .url(openapiInfoContactUrl))
                        .description(openapiInfoDescription)
                        .license(new License().name(openapiInfoLicenseName))
                        .title(openapiInfoTitle)
                        .version(appVersion));
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                .addMapping("/**")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE");
            }
        };
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.exception;

/**
 * Exception class to assist in handling of service layer exceptions.
 *
 * @author Lars Johansson
 */
public class ServiceException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -5521814109184738596L;

    private final String details;
    private final String field;

    /**
     * Public constructor.
     *
     * @param message message
     */
    public ServiceException(String message) {
        super(message);
        this.details = null;
        this.field = null;
    }

    /**
     * Public constructor.
     *
     * @param message message
     * @param details details
     * @param field field
     * @param cause cause
     */
    public ServiceException(String message, String details, String field, Throwable cause) {
        super(message, cause);
        this.details = details;
        this.field = field;
    }

    /**
     * Returns details of exception.
     *
     * @return details
     */
    public String getDetails() {
        return details;
    }

    /**
     * Return field of exception.
     *
     * @return field
     */
    public String getField() {
        return field;
    }

}

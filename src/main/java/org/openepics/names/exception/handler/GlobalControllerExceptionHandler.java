/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.exception.handler;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.exception.DataConflictException;
import org.openepics.names.exception.DataDeletedException;
import org.openepics.names.exception.DataExistException;
import org.openepics.names.exception.DataNotAvailableException;
import org.openepics.names.exception.DataNotCorrectException;
import org.openepics.names.exception.DataNotFoundException;
import org.openepics.names.exception.DataNotValidException;
import org.openepics.names.exception.InputNotAvailableException;
import org.openepics.names.exception.InputNotCorrectException;
import org.openepics.names.exception.InputNotEmptyException;
import org.openepics.names.exception.InputNotValidException;
import org.openepics.names.exception.ServiceException;
import org.openepics.names.exception.security.AuthenticationException;
import org.openepics.names.exception.security.EntityNotFoundException;
import org.openepics.names.exception.security.RemoteException;
import org.openepics.names.exception.security.UnauthorizedException;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.util.TextUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Global exception handler to ensure exceptions are communicated to request origin in a uniform way.
  * <br/><br/>
 * Note
 * <ul>
 * <li>400 {@link HttpStatus#BAD_REQUEST}</li>
 * <li>401 {@link HttpStatus#UNAUTHORIZED}</li>
 * <li>403 {@link HttpStatus#FORBIDDEN}</li>
 * <li>404 {@link HttpStatus#NOT_FOUND}</li>
 * <li>409 {@link HttpStatus#CONFLICT}</li>
 * <li>422 {@link HttpStatus#UNPROCESSABLE_ENTITY}</li>
 * <li>500 {@link HttpStatus#INTERNAL_SERVER_ERROR}</li>
 * <li>503 {@link HttpStatus#SERVICE_UNAVAILABLE}</li>
 * </ul>
*
 * @author Lars Johansson
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Response> handleConflict(RuntimeException ex, WebRequest request) {
        Response response = new Response("", "", "");
        response.setMessage(TextUtil.OPERATION_COULD_NOT_BE_PERFORMED);

        HttpStatus resultStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (ex instanceof ServiceException se) {
            response.setMessage(StringUtils.trimToEmpty(ex.getMessage()));
            response.setDetails(StringUtils.trimToEmpty(se.getDetails()));
            response.setField(StringUtils.trimToEmpty(se.getField()));

            // HttpStatus.BAD_REQUEST handled by Spring

            if (ex instanceof AuthenticationException) {
                resultStatus = HttpStatus.UNAUTHORIZED;
            }

            if (ex instanceof UnauthorizedException) {
                resultStatus = HttpStatus.FORBIDDEN;
            }

            if (ex instanceof DataNotAvailableException
                    || ex instanceof DataNotFoundException
                    || ex instanceof EntityNotFoundException) {
                resultStatus = HttpStatus.NOT_FOUND;
            }

            if (ex instanceof DataConflictException
                    || ex instanceof DataDeletedException
                    || ex instanceof DataExistException
                    || ex instanceof DataNotValidException) {
                 resultStatus = HttpStatus.CONFLICT;
            }

            if (ex instanceof InputNotAvailableException
                    || ex instanceof InputNotCorrectException
                    || ex instanceof InputNotEmptyException
                    || ex instanceof InputNotValidException
                    || ex instanceof DataNotCorrectException) {
                resultStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            }

            if (ex instanceof RemoteException) {
                resultStatus = HttpStatus.SERVICE_UNAVAILABLE;
            }
        }

        return new ResponseEntity<>(response, Response.getHeaderJson(), resultStatus);
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.exception.security;

import org.openepics.names.exception.ServiceException;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
public class AuthenticationException extends ServiceException {

    /**
     *
     */
    private static final long serialVersionUID = -5434486411943794281L;

    private static final String ERROR = "Authentication error";

    public AuthenticationException(String description) {
        super(ERROR, description, null, null);
    }

}

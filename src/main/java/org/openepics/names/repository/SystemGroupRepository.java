/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.NameStructure;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle system group information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class SystemGroupRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * Count system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return count of system groups
     */
    public Long countSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        // where
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<SystemGroup> from = cq.from(SystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, cq, from, deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of system groups
     */
    public List<SystemGroup> readSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        return readSystemGroups(deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                null, null, null, null);
    }

    /**
     * Find system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of system groups
     */
    public List<SystemGroup> readSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // where
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SystemGroup> cq = cb.createQuery(SystemGroup.class);
        Root<SystemGroup> from = cq.from(SystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, cq, from, deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        TypedQuery<SystemGroup> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for system groups.
     *
     * @param cb criteria builder
     * @param cq criteria query
     * @param from criteria query root
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesSystemGroups(CriteriaBuilder cb, CriteriaQuery cq, Root<SystemGroup> from, Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        List<Predicate> predicates = new ArrayList<>();

        if (deleted != null) {
            predicates.add(cb.equal(from.get(NameStructure.FIELD_DELETED), deleted));
        }

        // prepare pattern
        //     jpa query characters % and _
        //     remove excess % characters

        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(NameStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }
        if (!StringUtils.isEmpty(mnemonic)) {
            predicates.add(cb.and(cb.like(from.get(Structure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonic))));
        }
        if (!StringUtils.isEmpty(mnemonicEquivalence)) {
            predicates.add(cb.and(cb.like(from.get(Structure.FIELD_MNEMONIC_EQUIVALENCE), RepositoryUtil.preparePattern(mnemonicEquivalence))));
        }
        if (!StringUtils.isEmpty(mnemonicPath)) {
            predicates.add(cb.and(cb.like(from.get(Structure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonicPath))));
        }
        if (!StringUtils.isEmpty(description)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_DESCRIPTION), RepositoryUtil.preparePattern(description))));
        }
        if (!StringUtils.isEmpty(who)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_PROCESSED_BY), RepositoryUtil.preparePattern(who))));
        }

        return predicates;
    }

    /**
     * Prepare order by query expression.
     *
     * @param root root type in the from clause
     * @param orderBy field on which to order by
     * @return order by query expression
     */
    private Expression<String> orderBy(Root<SystemGroup> root, FieldStructure orderBy) {
        // no parent for system group

        if (FieldStructure.UUID.equals(orderBy)) {
            return root.get(NameStructure.FIELD_UUID);
        } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
            return root.get(Structure.FIELD_MNEMONIC);
        } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
            return root.get(Structure.FIELD_MNEMONIC);
        } else if (FieldStructure.ORDERING.equals(orderBy)) {
            return root.get(Structure.FIELD_ORDERING);
        } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
            return root.get(NameStructure.FIELD_DESCRIPTION);
        } else if (FieldStructure.WHEN.equals(orderBy)) {
            return root.get(NameStructure.FIELD_PROCESSED);
        } else {
            return root.get(Structure.FIELD_MNEMONIC);
        }
    }

    /**
     * Persist system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void createSystemGroup(SystemGroup systemGroup) {
        em.persist(systemGroup);
    }

    /**
     * Merge system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void updateSystemGroup(SystemGroup systemGroup) {
        em.merge(systemGroup);
    }

}

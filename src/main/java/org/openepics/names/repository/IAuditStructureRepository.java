/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import org.openepics.names.repository.model.AuditStructure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find audit structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IAuditStructureRepository extends JpaRepository<AuditStructure, Long> {

    @Query("SELECT aus FROM AuditStructure aus "
            + "WHERE aus.auditTable = ?1 "
            + "AND aus.uuid = ?2 "
            + "AND aus.status = 'APPROVED' "
            + "AND aus.auditId = (select max(aus2.auditId) from AuditStructure aus2 where aus2.uuid = aus.uuid and aus2.auditId < ?3)")
    AuditStructure findPreviousByAuditTableAndUuidAndAuditId(String auditTable, String uuid, Long id);

}

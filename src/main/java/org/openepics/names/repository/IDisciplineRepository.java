/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find discipline structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IDisciplineRepository extends JpaRepository<Discipline, Long> {

    @Query("FROM Discipline di WHERE di.uuid = ?1")
    Discipline findByUuid(String uuid);

    @Query("FROM Discipline di WHERE di.deleted = false AND di.mnemonic = ?1")
    Discipline findNotDeletedByMnemonic(String mnemonic);

    @Query("FROM Discipline di WHERE di.deleted = false")
    List<Discipline> findNotDeleted();

}

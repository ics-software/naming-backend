/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.Date;
import java.util.List;

import org.openepics.names.repository.model.AuditName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find audit name (device) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IAuditNameRepository extends JpaRepository<AuditName, Long> {

    @Query("SELECT aun FROM AuditName aun "
            + "WHERE aun.uuid = ?1 "
            + "AND aun.status = 'APPROVED' "
            + "AND aun.auditId = (select max(aun2.auditId) from AuditName aun2 where aun2.uuid = aun.uuid and aun2.auditId < ?2)")
    AuditName findPreviousByUuidAndAuditId(String uuid, Long id);

    @Query("FROM AuditName aun WHERE aun.requested >= ?1 AND aun.requested <= ?2")
    List<AuditName> findByRequestedBetween(Date from, Date to);

    @Query("FROM AuditName aun WHERE aun.uuid = ?1")
    List<AuditName> findByUuid(String uuid);

}

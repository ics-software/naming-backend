/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle audit structure information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class AuditStructureRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * Count audit structures.
     *
     * @param type type
     * @param uuid uuid
     * @return count of audit structures
     */
    public Long countAuditStructures(Type type, String uuid) {
        // where
        //     type
        //     uuid

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<AuditStructure> from = cq.from(AuditStructure.class);

        cq.where(cb.and(preparePredicatesAuditStructures(cb, cq, from,
                type, uuid).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find audit structures.
     *
     * @param type type
     * @param uuid uuid
     * @return list of structures
     */
    public List<AuditStructure> readAuditStructures(Type type, String uuid,
            Integer offset, Integer limit) {

        // where
        //     type
        //     uuid
        // order
        //     processed asc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AuditStructure> cq = cb.createQuery(AuditStructure.class);
        Root<AuditStructure> from = cq.from(AuditStructure.class);

        cq.where(cb.and(preparePredicatesAuditStructures(cb, cq, from,
                type, uuid).toArray(new Predicate[0])));
        cq.select(from);

        cq.orderBy(cb.asc(from.get(AuditStructure.FIELD_PROCESSED)));

        TypedQuery<AuditStructure> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for audit structures.
     *
     * @param cb criteria builder
     * @param cq criteria query
     * @param from criteria query root
     * @param type type
     * @param uuid uuid
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesAuditStructures(CriteriaBuilder cb, CriteriaQuery cq, Root<AuditStructure> from,
            Type type, String uuid) {

        List<Predicate> predicates = new ArrayList<>();

        if (type != null) {
            predicates.add(cb.and(cb.equal(from.get(AuditStructure.FIELD_AUDIT_TABLE), type.name().toLowerCase())));
        }
        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(AuditStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }

        return predicates;
    }

    /**
     * Persist audit structure into persistence context.
     *
     * @param auditStructure audit structure
     */
    public void createAuditStructure(AuditStructure auditStructure) {
        em.persist(auditStructure);
    }

}

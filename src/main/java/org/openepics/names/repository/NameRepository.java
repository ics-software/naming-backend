/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.NameStructure;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle name information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class NameRepository {

    /**
     * Used to decide how to traverse structure references for names.
     */
    public enum NameByStructure {
        NAME_BY_SYSTEMGROUP,
        NAME_BY_SYSTEMGROUP_THROUGH_SYSTEM,
        NAME_BY_SYSTEMGROUP_THROUGH_SUBSYSTEM,
        NAME_BY_SYSTEM,
        NAME_BY_SYSTEM_THROUGH_SUBSYSTEM,
        NAME_BY_SUBSYSTEM,
        NAME_BY_DISCIPLINE_THROUGH_DEVICETYPE,
        NAME_BY_DEVICEGROUP_THROUGH_DEVICETYPE,
        NAME_BY_DEVICETYPE;
    }

    @PersistenceContext
    private EntityManager em;

    /**
     * Count names.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param name name
     * @param nameEquivalence name equivalence
     * @param systemStructure system structure mnemonic
     * @param deviceStructure device structure mnemonic
     * @param index index
     * @param description description
     * @param who who
     * @return count of names
     */
    public Long countNames(Boolean deleted,
            String uuid, String name, String nameEquivalence, String systemStructure, String deviceStructure, String index, String description, String who) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Name> from = cq.from(Name.class);

        cq.where(cb.and(preparePredicatesNames(cb, from, deleted,
                uuid, name, nameEquivalence, systemStructure, deviceStructure, index, description, who).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find names.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param name name
     * @param nameEquivalence name equivalence
     * @param systemStructure system structure mnemonic
     * @param deviceStructure device structure mnemonic
     * @param index index
     * @param description description
     * @param who who
     * @return list of names
     */
    public List<Name> readNames(Boolean deleted,
            String uuid, String name, String nameEquivalence, String systemStructure, String deviceStructure, String index, String description, String who) {

        return readNames(deleted,
                uuid, name, nameEquivalence, systemStructure, deviceStructure, index, description, who,
                null, null, null, null);
    }

    /**
     * Find names.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param name name
     * @param nameEquivalence name equivalence
     * @param systemStructure system structure mnemonic
     * @param deviceStructure device structure mnemonic
     * @param index index
     * @param description description
     * @param who who
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of names
     */
    public List<Name> readNames(Boolean deleted,
            String uuid, String name, String nameEquivalence, String systemStructure, String deviceStructure, String index, String description, String who,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Name> cq = cb.createQuery(Name.class);
        Root<Name> from = cq.from(Name.class);

        cq.where(cb.and(preparePredicatesNames(cb, from, deleted,
                uuid, name, nameEquivalence, systemStructure, deviceStructure, index, description, who).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(cb, from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        TypedQuery<Name> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for names.
     *
     * @param cb criteria builder
     * @param from criteria query root
     * @param deleted deleted
     * @param uuid uuid
     * @param name name
     * @param nameEquivalence name equivalence
     * @param systemStructure system structure mnemonic
     * @param deviceStructure device structure mnemonic
     * @param index index
     * @param description description
     * @param who who
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesNames(CriteriaBuilder cb, Root<Name> from, Boolean deleted,
            String uuid, String name, String nameEquivalence, String systemStructure, String deviceStructure, String index, String description, String who) {

        List<Predicate> predicates = new ArrayList<>();

        if (deleted != null) {
            predicates.add(cb.equal(from.get(NameStructure.FIELD_DELETED), deleted));
        }

        // prepare pattern
        //     jpa query characters % and _
        //     remove excess % characters

        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(NameStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }
        if (!StringUtils.isEmpty(name)) {
            predicates.add(cb.and(cb.like(from.get(Name.FIELD_CONVENTION_NAME), RepositoryUtil.preparePattern(name))));
        }
        if (!StringUtils.isEmpty(nameEquivalence)) {
            predicates.add(cb.and(cb.like(from.get(Name.FIELD_CONVENTION_NAME_EQUIVALENCE), RepositoryUtil.preparePattern(nameEquivalence))));
        }
        if (!StringUtils.isEmpty(systemStructure)) {
            predicates.add(cb.and(cb.like(cb.function(NameStructure.FUNCTION_GET_MNEMONIC_PATH_SYSTEM_STRUCTURE, String.class, from.get(Name.FIELD_CONVENTION_NAME)), RepositoryUtil.preparePattern(systemStructure))));
        }
        if (!StringUtils.isEmpty(deviceStructure)) {
            predicates.add(cb.and(cb.like(cb.function(NameStructure.FUNCTION_GET_MNEMONIC_PATH_DEVICE_STRUCTURE, String.class, from.get(Name.FIELD_CONVENTION_NAME)), RepositoryUtil.preparePattern(deviceStructure))));
        }
        if (!StringUtils.isEmpty(index)) {
            predicates.add(cb.and(cb.like(cb.function(NameStructure.FUNCTION_GET_INSTANCE_INDEX, String.class, from.get(Name.FIELD_CONVENTION_NAME)), RepositoryUtil.preparePattern(index))));
        }
        if (!StringUtils.isEmpty(description)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_DESCRIPTION), RepositoryUtil.preparePattern(description))));
        }
        if (!StringUtils.isEmpty(who)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_REQUESTED_BY), RepositoryUtil.preparePattern(who))));
        }

        return predicates;
    }

    /**
     * Prepare order by query expression.
     *
     * @param cb criteria builder
     * @param root root type in the from clause
     * @param orderBy field on which to order by
     * @return order by query expression
     */
    private Expression<String> orderBy(CriteriaBuilder cb, Root<Name> root, FieldName orderBy) {
        if (FieldName.UUID.equals(orderBy)) {
            return root.get(NameStructure.FIELD_UUID);
        } else if (FieldName.SYSTEMSTRUCTURE.equals(orderBy)) {
            return cb.function(NameStructure.FUNCTION_GET_MNEMONIC_PATH_SYSTEM_STRUCTURE, String.class, root.get(Name.FIELD_CONVENTION_NAME));
        } else if (FieldName.DEVICESTRUCTURE.equals(orderBy)) {
            return cb.function(NameStructure.FUNCTION_GET_MNEMONIC_PATH_DEVICE_STRUCTURE, String.class, root.get(Name.FIELD_CONVENTION_NAME));
        } else if (FieldName.INDEX.equals(orderBy)) {
            return cb.function(NameStructure.FUNCTION_GET_INSTANCE_INDEX, String.class, root.get(Name.FIELD_CONVENTION_NAME));
        } else if (FieldName.DESCRIPTION.equals(orderBy)) {
            return root.get(NameStructure.FIELD_DESCRIPTION);
        } else if (FieldName.WHEN.equals(orderBy)) {
            return root.get(NameStructure.FIELD_REQUESTED);
        } else {
            return root.get(Name.FIELD_CONVENTION_NAME);
        }
    }

    /**
     * Find names by structure uuid.
     *
     * @param nameByStructure kind of read operation, used to decide how to traverse structure references for names
     * @param uuid structure uuid
     * @param deleted deleted
     * @param orderBy order by
     * @param isAsc is ascending
     * @return list of names
     */
    @SuppressWarnings("unchecked")
    public List<Name> readNamesByStructure(NameByStructure nameByStructure, String uuid, Boolean deleted,
            FieldName orderBy, Boolean isAsc) {

        // must have NameByStructure
        if (nameByStructure == null || StringUtils.isEmpty(uuid)) {
            return Lists.newArrayList();
        }

        StringBuilder sql = new StringBuilder();
        switch (nameByStructure) {
        case NAME_BY_SYSTEMGROUP:
            sql.append("SELECT n FROM Name n, SystemGroup sg "
                    + "WHERE sg.id = n.systemGroupId "
                    + "  AND sg.uuid = :sUuid");
            break;
        case NAME_BY_SYSTEMGROUP_THROUGH_SYSTEM:
            sql.append("SELECT n FROM Name n, System sys, SystemGroup sg "
                    + "WHERE sys.id = n.systemId "
                    + "  AND sg.id = sys.parentId  "
                    + "  AND sg.uuid = :sUuid");
            break;
        case NAME_BY_SYSTEMGROUP_THROUGH_SUBSYSTEM:
            sql.append("SELECT n FROM Name n, Subsystem sub, System sys, SystemGroup sg "
                    + "WHERE sub.id = n.subsystemId "
                    + "  AND sys.id = sub.parentId  "
                    + "  AND sg.id = sys.parentId  "
                    + "  AND sg.uuid = :sUuid");
            break;
        case NAME_BY_SYSTEM:
            sql.append("SELECT n FROM Name n, System sys "
                    + "WHERE sys.id = n.systemId "
                    + "  AND sys.uuid = :sUuid");
            break;
        case NAME_BY_SYSTEM_THROUGH_SUBSYSTEM:
            sql.append("SELECT n FROM Name n, Subsystem sub, System sys "
                    + "WHERE sub.id = n.subsystemId "
                    + "  AND sys.id = sub.parentId  "
                    + "  AND sys.uuid = :sUuid");
            break;
        case NAME_BY_SUBSYSTEM:
            sql.append("SELECT n FROM Name n, Subsystem sub "
                    + "WHERE sub.id = n.subsystemId "
                    + "  AND sub.uuid = :sUuid");
            break;
        case NAME_BY_DISCIPLINE_THROUGH_DEVICETYPE:
            sql.append("SELECT n FROM Name n, DeviceType dt, DeviceGroup dg, Discipline di "
                    + "WHERE dt.id = n.deviceTypeId "
                    + "  AND dg.id = dt.parentId "
                    + "  AND di.id = dg.parentId "
                    + "  AND di.uuid = :sUuid");
            break;
        case NAME_BY_DEVICEGROUP_THROUGH_DEVICETYPE:
            sql.append("SELECT n FROM Name n, DeviceType dt, DeviceGroup dg "
                    + "WHERE dt.id = n.deviceTypeId "
                    + "  AND dg.id = dt.parentId "
                    + "  AND dg.uuid = :sUuid");
            break;
        case NAME_BY_DEVICETYPE:
            sql.append("SELECT n FROM Name n, DeviceType dt "
                    + "WHERE dt.id = n.deviceTypeId "
                    + "  AND dt.uuid = :sUuid");
            break;
        default: return Lists.newArrayList();
        }
        if (deleted != null) {
            sql.append(" and n.deleted = :nDeleted");
        }

        StringBuilder sqlOrderBy = new StringBuilder();
        if (orderBy != null) {
            sqlOrderBy.append(" order by ");

            if (FieldName.UUID.equals(orderBy)) {
                sqlOrderBy.append("n.");
                sqlOrderBy.append(NameStructure.FIELD_UUID);
            } else if (FieldName.SYSTEMSTRUCTURE.equals(orderBy)) {
                sqlOrderBy.append(NameStructure.FUNCTION_GET_MNEMONIC_PATH_SYSTEM_STRUCTURE);
                sqlOrderBy.append("(n.conventionName)");
            } else if (FieldName.DEVICESTRUCTURE.equals(orderBy)) {
                sqlOrderBy.append(NameStructure.FUNCTION_GET_MNEMONIC_PATH_DEVICE_STRUCTURE);
                sqlOrderBy.append("(n.conventionName)");
            } else if (FieldName.INDEX.equals(orderBy)) {
                sqlOrderBy.append(NameStructure.FUNCTION_GET_INSTANCE_INDEX);
                sqlOrderBy.append("(n.conventionName)");
            } else if (FieldName.DESCRIPTION.equals(orderBy)) {
                sqlOrderBy.append("n.");
                sqlOrderBy.append(NameStructure.FIELD_DESCRIPTION);
            } else if (FieldName.WHEN.equals(orderBy)) {
                sqlOrderBy.append("n.");
                sqlOrderBy.append(NameStructure.FIELD_REQUESTED);
            } else {
                sqlOrderBy.append("n.");
                sqlOrderBy.append(Name.FIELD_CONVENTION_NAME);
            }

            if (BooleanUtils.toBoolean(isAsc)) {
                sqlOrderBy.append(" asc");
            } else {
                sqlOrderBy.append(" desc");
            }
        }
        if (orderBy != null) {
            sql.append(sqlOrderBy.toString());
        }

        Query query = em.createQuery(sql.toString(), Name.class).setParameter("sUuid", uuid);
        if (deleted != null) {
            query.setParameter("nDeleted", deleted);
        }
        return query.getResultList();
    }

    /**
     * Persist name into persistence context.
     *
     * @param name name
     */
    public void createName(Name name) {
        em.persist(name);
    }

    /**
     * Merge name into persistence context.
     *
     * @param name name
     */
    public void updateName(Name name) {
        em.merge(name);
    }

}

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.openepics.names.rest.beans.Status;
import org.openepics.names.util.TextUtil;

/**
 * This entity represents an audit name entry.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "audit_name")
public class AuditName {

    public static final String FIELD_AUDIT_ID                    = "audit_id";
    public static final String FIELD_AUDIT_VERSION               = "audit_version";
    public static final String FIELD_AUDIT_TABLE                 = "audit_table";
    public static final String FIELD_AUDIT_OPERATION             = "audit_operation";

    public static final String FIELD_ID                          = "id";
    public static final String FIELD_VERSION                     = "version";
    public static final String FIELD_UUID                        = "uuid";
    public static final String FIELD_SYSTEM_GROUP_ID             = "systemgroupId";
    public static final String FIELD_SYSTEM_ID                   = "systemId";
    public static final String FIELD_SUBSYSTEM_ID                = "subsystemId";
    public static final String FIELD_DEVICETYPE_ID               = "devicetypeId";
    public static final String FIELD_INSTANCE_INDEX              = "instanceIndex";
    public static final String FIELD_CONVENTION_NAME             = "conventionName";
    public static final String FIELD_CONVENTION_NAME_EQUIVALENCE = "conventionNameEquivalence";
    public static final String FIELD_DESCRIPTION                 = "description";
    public static final String FIELD_STATUS                      = "status";
    public static final String FIELD_DELETED                     = "deleted";
    public static final String FIELD_REQUESTED                   = "requested";
    public static final String FIELD_REQUESTED_BY                = "requestedBy";
    public static final String FIELD_REQUESTED_COMMENT           = "requestedComment";
    public static final String FIELD_PROCESSED                   = "processed";
    public static final String FIELD_PROCESSED_BY                = "processedBy";
    public static final String FIELD_PROCESSED_COMMENT           = "processedComment";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "audit_id")
    protected @Nullable Long auditId;
    @Version
    @Column(name = "audit_version")
    private @Nullable Integer auditVersion;
    @Column(name = "audit_table")
    private String auditTable;
    @Column(name = "audit_operation")
    private String auditOperation;

    private Long id;
    private Integer version;
    private String uuid;
    @Column(name = "systemgroup_id")
    private Long systemGroupId;
    @Column(name = "system_id")
    private Long systemId;
    @Column(name = "subsystem_id")
    private Long subsystemId;
    @Column(name = "devicetype_id")
    private Long deviceTypeId;
    @Column(name = "instance_index")
    private String instanceIndex;
    @Column(name = "convention_name")
    private String conventionName;
    @Column(name = "convention_name_equivalence")
    private String conventionNameEquivalence;
    private String description;
    @Enumerated(EnumType.STRING)
    private Status status;
    private Boolean deleted;
    private Date requested;
    @Column(name = "requested_by")
    private String requestedBy;
    @Column(name = "requested_comment")
    private String requestedComment;
    private Date processed;
    @Column(name = "processed_by")
    private String processedBy;
    @Column(name = "processed_comment")
    private String processedComment;

    /**
     * Constructor for audit name.
     */
    public AuditName() {
    }

    /**
     * Constructor for audit name.
     *
     * @param auditOperation audit operation
     * @param name name
     */
    public AuditName(String auditOperation, Name name) {
        this(TextUtil.NAME, auditOperation,
                name.getId(), name.getVersion(),
                name.getUuid(), name.getSystemGroupId(), name.getSystemId(), name.getSubsystemId(), name.getDeviceTypeId(),
                name.getInstanceIndex(), name.getConventionName(), name.getConventionNameEquivalence(), name.getDescription(),
                name.getStatus(), name.isDeleted(),
                name.getRequested(), name.getRequestedBy(), name.getRequestedComment(), null, null, null);
    }

    /**
     * Constructor for audit name.
     *
     * @param auditTable audit table
     * @param auditOperation audit operation
     * @param id id
     * @param version version
     * @param uuid uuid
     * @param systemGroupId system group id
     * @param systemId system id
     * @param subsystemId subsystem id
     * @param deviceTypeId device type id
     * @param index index
     * @param conventionName convention name
     * @param conventionNameEquivalence convention name equivalence
     * @param description description
     * @param status status
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     * @param processed processed
     * @param processedBy processedBy
     * @param processedComment processedComment
     */
    public AuditName(String auditTable, String auditOperation,
            Long id, Integer version,
            UUID uuid, Long systemGroupId, Long systemId, Long subsystemId, Long deviceTypeId,
            String instanceIndex, String conventionName, String conventionNameEquivalence, String description,
            Status status, Boolean deleted,
            Date requested, String requestedBy, String requestedComment, Date processed, String processedBy, String processedComment) {
        super();
        setAuditTable(auditTable);
        setAuditOperation(auditOperation);
        setId(id);
        setVersion(version);
        setUuid(uuid);
        setSystemGroupId(systemGroupId);
        setSystemId(systemId);
        setSubsystemId(subsystemId);
        setDeviceTypeId(deviceTypeId);
        setInstanceIndex(instanceIndex);
        setConventionName(conventionName);
        setConventionNameEquivalence(conventionNameEquivalence);
        setDescription(description);
        setStatus(status);
        setDeleted(deleted);
        setRequested(requested);
        setRequestedBy(requestedBy);
        setRequestedComment(requestedComment);
        setProcessed(processed);
        setProcessedBy(processedBy);
        setProcessedComment(processedComment);
    }

    public Long getAuditId() {
        return auditId;
    }
    public Integer getAuditVersion() {
        return auditVersion;
    }

    public String getAuditTable() {
        return auditTable;
    }
    public void setAuditTable(String auditTable) {
        this.auditTable = auditTable;
    }
    public String getAuditOperation() {
        return auditOperation;
    }
    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getVersion() {
        return version;
    }
    public void setVersion(Integer version) {
        this.version = version;
    }
    public UUID getUuid() {
        return uuid != null ? UUID.fromString(uuid) : null;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid != null ? uuid.toString() : null;
    }
    public Long getSystemGroupId() {
        return systemGroupId;
    }
    public void setSystemGroupId(Long systemGroupId) {
        this.systemGroupId = systemGroupId;
    }
    public Long getSystemId() {
        return systemId;
    }
    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }
    public Long getSubsystemId() {
        return subsystemId;
    }
    public void setSubsystemId(Long subsystemId) {
        this.subsystemId = subsystemId;
    }
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }
    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }
    public String getInstanceIndex() {
        return instanceIndex;
    }
    public void setInstanceIndex(String instanceIndex) {
        this.instanceIndex = instanceIndex;
    }
    public String getConventionName() {
        return conventionName;
    }
    public void setConventionName(String conventionName) {
        this.conventionName = conventionName;
    }
    public String getConventionNameEquivalence() {
        return conventionNameEquivalence;
    }
    public void setConventionNameEquivalence(String conventionNameEquivalence) {
        this.conventionNameEquivalence = conventionNameEquivalence;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getRequested() {
        return requested;
    }
    public void setRequested(Date requested) {
        this.requested = requested;
    }
    public String getRequestedBy() {
        return requestedBy;
    }
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }
    public String getRequestedComment() {
        return requestedComment;
    }
    public void setRequestedComment(String requestedComment) {
        this.requestedComment = requestedComment;
    }
    public Date getProcessed() {
        return processed;
    }
    public void setProcessed(Date processed) {
        this.processed = processed;
    }
    public String getProcessedBy() {
        return processedBy;
    }
    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }
    public String getProcessedComment() {
        return processedComment;
    }
    public void setProcessedComment(String processedComment) {
        this.processedComment = processedComment;
    }

    /**
     * Return name given audit name.
     *
     * @return name
     */
    public Name getNonAuditName() {
        return new Name(getUuid(), getSystemGroupId(), getSystemId(), getSubsystemId(), getDeviceTypeId(),
                getInstanceIndex(), getConventionName(), getConventionNameEquivalence(), getDescription(),
                getStatus(), isDeleted(),
                getRequested(), getRequestedBy(), getRequestedComment());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((AuditName) obj);
    }

    public boolean equals(AuditName other) {
        if (other == null)
            return false;

        if (getAuditId() == null) {
            if (other.getAuditId() != null)
                return false;
        } else if (!getAuditId().equals(other.getAuditId()))
            return false;
        if (getAuditVersion() == null) {
            if (other.getAuditVersion() != null)
                return false;
        } else if (!getAuditVersion().equals(other.getAuditVersion()))
            return false;
        if (getAuditTable() == null) {
            if (other.getAuditTable() != null)
                return false;
        } else if (!getAuditTable().equals(other.getAuditTable()))
            return false;
        if (getAuditOperation() == null) {
            if (other.getAuditOperation() != null)
                return false;
        } else if (!getAuditOperation().equals(other.getAuditOperation()))
            return false;

        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        if (getVersion() == null) {
            if (other.getVersion() != null)
                return false;
        } else if (!getVersion().equals(other.getVersion()))
            return false;
        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getSystemGroupId() == null) {
            if (other.getSystemGroupId() != null)
                return false;
        } else if (!getSystemGroupId().equals(other.getSystemGroupId()))
            return false;
        if (getSystemId() == null) {
            if (other.getSystemId() != null)
                return false;
        } else if (!getSystemId().equals(other.getSystemId()))
            return false;
        if (getSubsystemId() == null) {
            if (other.getSubsystemId() != null)
                return false;
        } else if (!getSubsystemId().equals(other.getSubsystemId()))
            return false;
        if (getDeviceTypeId() == null) {
            if (other.getDeviceTypeId() != null)
                return false;
        } else if (!getDeviceTypeId().equals(other.getDeviceTypeId()))
            return false;
        if (getInstanceIndex() == null) {
            if (other.getInstanceIndex() != null)
                return false;
        } else if (!getInstanceIndex().equals(other.getInstanceIndex()))
            return false;
        if (getConventionName() == null) {
            if (other.getConventionName() != null)
                return false;
        } else if (!getConventionName().equals(other.getConventionName()))
            return false;
        if (getConventionNameEquivalence() == null) {
            if (other.getConventionNameEquivalence() != null)
                return false;
        } else if (!getConventionNameEquivalence().equals(other.getConventionNameEquivalence()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getRequested() == null) {
            if (other.getRequested() != null)
                return false;
        } else if (!getRequested().equals(other.getRequested()))
            return false;
        if (getRequestedBy() == null) {
            if (other.getRequestedBy() != null)
                return false;
        } else if (!getRequestedBy().equals(other.getRequestedBy()))
            return false;
        if (getRequestedComment() == null) {
            if (other.getRequestedComment() != null)
                return false;
        } else if (!getRequestedComment().equals(other.getRequestedComment()))
            return false;
        if (getProcessed() == null) {
            if (other.getProcessed() != null)
                return false;
        } else if (!getProcessed().equals(other.getProcessed()))
            return false;
        if (getProcessedBy() == null) {
            if (other.getProcessedBy() != null)
                return false;
        } else if (!getProcessedBy().equals(other.getProcessedBy()))
            return false;
        if (getProcessedComment() == null) {
            if (other.getProcessedComment() != null)
                return false;
        } else if (!getProcessedComment().equals(other.getProcessedComment()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuditId(), getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"audit_id\": "                      + getAuditId());
        sb.append(", \"audit_version\": "               + getAuditVersion());
        sb.append(", \"audit_table\": "                 + getAuditTable());
        sb.append(", \"audit_operation\": "             + getAuditOperation());
        sb.append(", \"id\": "                          + getId());
        sb.append(", \"version\": "                     + getVersion());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"systemgroup_id\": "              + getSystemGroupId());
        sb.append(", \"system_id\": "                   + getSystemId());
        sb.append(", \"subsystem_id\": "                + getSubsystemId());
        sb.append(", \"devicetype_id\": "               + getDeviceTypeId());
        sb.append(", \"instance_index\": "              + getInstanceIndex());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"convention_name_equivalence\": " + getConventionNameEquivalence());
        sb.append(", \"description\": "                 + getDescription());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append(", \"requested\": "                   + getRequested());
        sb.append(", \"requested_by\": "                + getRequestedBy());
        sb.append(", \"requested_comment\": "           + getRequestedComment());
        sb.append(", \"processed\": "                   + getProcessed());
        sb.append(", \"processed_by\": "                + getProcessedBy());
        sb.append(", \"processed_comment\": "           + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

}

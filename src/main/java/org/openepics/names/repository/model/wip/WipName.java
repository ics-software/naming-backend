/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model.wip;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.openepics.names.rest.beans.Status;

/**
 * This entity represents a name.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "wip_name")
public class WipName extends WipNameStructure {

    public static final String FIELD_SYSTEM_GROUP_UUID           = "systemgroupUuid";
    public static final String FIELD_SYSTEM_UUID                 = "systemUuid";
    public static final String FIELD_SUBSYSTEM_UUID              = "subsystemUuid";
    public static final String FIELD_DEVICETYPE_UUID             = "devicetypeUuid";
    public static final String FIELD_INSTANCE_INDEX              = "instanceIndex";
    public static final String FIELD_CONVENTION_NAME             = "conventionName";
    public static final String FIELD_CONVENTION_NAME_EQUIVALENCE = "conventionNameEquivalence";

    @Column(name = "systemgroup_uuid")
    private String systemGroupUuid;
    @Column(name = "system_uuid")
    private String systemUuid;
    @Column(name = "subsystem_uuid")
    private String subsystemUuid;
    @Column(name = "devicetype_uuid")
    private String deviceTypeUuid;
    @Column(name = "instance_index")
    private String instanceIndex;
    @Column(name = "convention_name")
    private String conventionName;
    @Column(name = "convention_name_equivalence")
    private String conventionNameEquivalence;

    /**
     * Constructor for Name.
     */
    public WipName() {
    }

    /**
     * Constructor for Name.
     *
     * @param uuid uuid
     * @param systemGroupUuid system group uuid
     * @param systemUuid system uuid
     * @param subsystemUuid subsystem uuid
     * @param deviceTypeUuid device type uuid
     * @param index index
     * @param conventionName convention name
     * @param conventionNameEquivalence convention name equivalence
     * @param description description
     * @param status status
     * @param latest latest
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     */
    public WipName(UUID uuid, UUID systemGroupUuid, UUID systemUuid, UUID subsystemUuid, UUID deviceTypeUuid,
            String index, String conventionName, String conventionNameEquivalence, String description,
            Status status, Boolean latest, Boolean deleted,
            Date requested, String requestedBy, String requestedComment) {
        setUuid(uuid);
        setSystemGroupUuid(systemGroupUuid);
        setSystemUuid(systemUuid);
        setSubsystemUuid(subsystemUuid);
        setDeviceTypeUuid(deviceTypeUuid);
        setInstanceIndex(index);
        setConventionName(conventionName);
        setConventionNameEquivalence(conventionNameEquivalence);
        setDescription(description);
        setStatus(status);
        setLatest(latest);
        setDeleted(deleted);
        setRequested(requested);
        setRequestedBy(requestedBy);
        setRequestedComment(requestedComment);
    }

    public UUID getSystemGroupUuid() {
        return systemGroupUuid != null ? UUID.fromString(systemGroupUuid) : null;
    }
    public void setSystemGroupUuid(UUID systemGroupUuid) {
        this.systemGroupUuid = systemGroupUuid != null ? systemGroupUuid.toString() : null;
    }
    public UUID getSystemUuid() {
        return systemUuid != null ? UUID.fromString(systemUuid) : null;
    }
    public void setSystemUuid(UUID systemUuid) {
        this.systemUuid = systemUuid != null ? systemUuid.toString() : null;
    }
    public UUID getSubsystemUuid() {
        return subsystemUuid != null ? UUID.fromString(subsystemUuid) : null;
    }
    public void setSubsystemUuid(UUID subsystemUuid) {
        this.subsystemUuid = subsystemUuid != null ? subsystemUuid.toString() : null;
    }
    public UUID getDeviceTypeUuid() {
        return deviceTypeUuid != null ? UUID.fromString(deviceTypeUuid) : null;
    }
    public void setDeviceTypeUuid(UUID deviceTypeUuid) {
        this.deviceTypeUuid = deviceTypeUuid != null ? deviceTypeUuid.toString() : null;
    }
    public String getInstanceIndex() {
        return instanceIndex;
    }
    public void setInstanceIndex(String instanceIndex) {
        this.instanceIndex = instanceIndex;
    }
    public String getConventionName() {
        return conventionName;
    }
    public void setConventionName(String conventionName) {
        this.conventionName = conventionName;
    }
    public String getConventionNameEquivalence() {
        return conventionNameEquivalence;
    }
    public void setConventionNameEquivalence(String conventionNameEquivalence) {
        this.conventionNameEquivalence = conventionNameEquivalence;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((WipName) obj);
    }

    public boolean equals(WipName other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getSystemGroupUuid() == null) {
            if (other.getSystemGroupUuid() != null)
                return false;
        } else if (!getSystemGroupUuid().equals(other.getSystemGroupUuid()))
            return false;
        if (getSystemUuid() == null) {
            if (other.getSystemUuid() != null)
                return false;
        } else if (!getSystemUuid().equals(other.getSystemUuid()))
            return false;
        if (getSubsystemUuid() == null) {
            if (other.getSubsystemUuid() != null)
                return false;
        } else if (!getSubsystemUuid().equals(other.getSubsystemUuid()))
            return false;
        if (getDeviceTypeUuid() == null) {
            if (other.getDeviceTypeUuid() != null)
                return false;
        } else if (!getDeviceTypeUuid().equals(other.getDeviceTypeUuid()))
            return false;
        if (getInstanceIndex() == null) {
            if (other.getInstanceIndex() != null)
                return false;
        } else if (!getInstanceIndex().equals(other.getInstanceIndex()))
            return false;
        if (getConventionName() == null) {
            if (other.getConventionName() != null)
                return false;
        } else if (!getConventionName().equals(other.getConventionName()))
            return false;
        if (getConventionNameEquivalence() == null) {
            if (other.getConventionNameEquivalence() != null)
                return false;
        } else if (!getConventionNameEquivalence().equals(other.getConventionNameEquivalence()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"version\": "                     + getVersion());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"systemgroup_uuid\": "            + getSystemGroupUuid());
        sb.append(", \"system_uuid\": "                 + getSystemUuid());
        sb.append(", \"subsystem_uuid\": "              + getSubsystemUuid());
        sb.append(", \"devicetype_uuid\": "             + getDeviceTypeUuid());
        sb.append(", \"instance_index\": "              + getInstanceIndex());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"convention_name_equivalence\": " + getConventionNameEquivalence());
        sb.append(", \"description\": "                 + getDescription());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"latest\": "                      + isLatest());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append(", \"requested\": "                   + getRequested());
        sb.append(", \"requested_by\": "                + getRequestedBy());
        sb.append(", \"requested_comment\": "           + getRequestedComment());
        sb.append(", \"processed\": "                   + getProcessed());
        sb.append(", \"processed_by\": "                + getProcessedBy());
        sb.append(", \"processed_comment\": "           + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"latest\": "                      + isLatest());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.openepics.names.rest.beans.Status;
import org.openepics.names.util.TextUtil;

/**
 * This entity represents an audit structure entry.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "audit_structure")
public class AuditStructure {

    public static final String FIELD_AUDIT_ID             = "auditId";
    public static final String FIELD_AUDIT_VERSION        = "auditVersion";
    public static final String FIELD_AUDIT_TABLE          = "auditTable";
    public static final String FIELD_AUDIT_OPERATION      = "auditOperation";

    public static final String FIELD_ID                   = "id";
    public static final String FIELD_VERSION              = "version";
    public static final String FIELD_UUID                 = "uuid";
    public static final String FIELD_PARENT_ID            = "parentId";
    public static final String FIELD_MNEMONIC             = "mnemonic";
    public static final String FIELD_MNEMONIC_EQUIVALENCE = "mnemonicEquivalence";
    public static final String FIELD_ORDERING             = "ordering";
    public static final String FIELD_DESCRIPTION          = "description";
    public static final String FIELD_STATUS               = "status";
    public static final String FIELD_DELETED              = "deleted";
    public static final String FIELD_REQUESTED            = "requested";
    public static final String FIELD_REQUESTED_BY         = "requestedBy";
    public static final String FIELD_REQUESTED_COMMENT    = "requestedComment";
    public static final String FIELD_PROCESSED            = "processed";
    public static final String FIELD_PROCESSED_BY         = "processedBy";
    public static final String FIELD_PROCESSED_COMMENT    = "processedComment";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "audit_id")
    protected @Nullable Long auditId;
    @Version
    @Column(name = "audit_version")
    private @Nullable Integer auditVersion;
    @Column(name = "audit_table")
    private String auditTable;
    @Column(name = "audit_operation")
    private String auditOperation;

    private Long id;
    private Integer version;
    private String uuid;
    @Column(name = "parent_id")
    private Long parentId;
    private String mnemonic;
    @Column(name = "mnemonic_equivalence")
    private String mnemonicEquivalence;
    private Integer ordering;
    private String description;
    @Enumerated(EnumType.STRING)
    private Status status;
    private Boolean deleted;
    private Date requested;
    @Column(name = "requested_by")
    private String requestedBy;
    @Column(name = "requested_comment")
    private String requestedComment;
    private Date processed;
    @Column(name = "processed_by")
    private String processedBy;
    @Column(name = "processed_comment")
    private String processedComment;

    /**
     * Constructor for audit structure.
     */
    public AuditStructure() {
    }

    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, SystemGroup structure) {
            this(TextUtil.SYSTEMGROUP, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), null, structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }
    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, System structure) {
        this(TextUtil.SYSTEM, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), structure.getParentId(), structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }
    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, Subsystem structure) {
        this(TextUtil.SUBSYSTEM, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), structure.getParentId(), structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }
    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, Discipline structure) {
        this(TextUtil.DISCIPLINE, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), null, structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }
    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, DeviceGroup structure) {
        this(TextUtil.DEVICEGROUP, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), structure.getParentId(), structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }
    /**
     * Constructor for audit structure.
     *
     * @param auditOperation audit operation
     * @param structure structure
     */
    public AuditStructure(String auditOperation, DeviceType structure) {
        this(TextUtil.DEVICETYPE, auditOperation,
                structure.getId(), structure.getVersion(),
                structure.getUuid(), structure.getParentId(), structure.getMnemonic(), structure.getMnemonicEquivalence(), structure.getOrdering(), structure.getDescription(),
                structure.getStatus(), structure.isDeleted(),
                null, null, null, structure.getProcessed(), structure.getProcessedBy(), structure.getProcessedComment());
    }

    /**
     * Constructor for audit structure.
     *
     * @param auditTable audit table
     * @param auditOperation audit operation
     * @param id id
     * @param version version
     * @param uuid uuid
     * @param parentId parent id
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param ordering
     * @param description description
     * @param status status
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     * @param processed processed
     * @param processedBy processedBy
     * @param processedComment processedComment
     */
    public AuditStructure(String auditTable, String auditOperation,
            Long id, Integer version,
            UUID uuid, Long parentId, String mnemonic, String mnemonicEquivalence, Integer ordering, String description,
            Status status, Boolean deleted,
            Date requested, String requestedBy, String requestedComment, Date processed, String processedBy, String processedComment) {
        super();
        setAuditTable(auditTable);
        setAuditOperation(auditOperation);
        setId(id);
        setVersion(version);
        setUuid(uuid);
        setParentId(parentId);
        setMnemonic(mnemonic);
        setMnemonicEquivalence(mnemonicEquivalence);
        setOrdering(ordering);
        setDescription(description);
        setStatus(status);
        setDeleted(deleted);
        setRequested(requested);
        setRequestedBy(requestedBy);
        setRequestedComment(requestedComment);
        setProcessed(processed);
        setProcessedBy(processedBy);
        setProcessedComment(processedComment);
    }

    public Long getAuditId() {
        return auditId;
    }
    public Integer getAuditVersion() {
        return auditVersion;
    }

    public String getAuditTable() {
        return auditTable;
    }
    public void setAuditTable(String auditTable) {
        this.auditTable = auditTable;
    }
    public String getAuditOperation() {
        return auditOperation;
    }
    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getVersion() {
        return version;
    }
    public void setVersion(Integer version) {
        this.version = version;
    }
    public UUID getUuid() {
        return uuid != null ? UUID.fromString(uuid) : null;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid != null ? uuid.toString() : null;
    }
    public Long getParentId() {
        return parentId;
    }
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    public String getMnemonic() {
        return mnemonic;
    }
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    public String getMnemonicEquivalence() {
        return mnemonicEquivalence;
    }
    public void setMnemonicEquivalence(String mnemonicEquivalence) {
        this.mnemonicEquivalence = mnemonicEquivalence;
    }
    public Integer getOrdering() {
        return ordering;
    }
    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getRequested() {
        return requested;
    }
    public void setRequested(Date requested) {
        this.requested = requested;
    }
    public String getRequestedBy() {
        return requestedBy;
    }
    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }
    public String getRequestedComment() {
        return requestedComment;
    }
    public void setRequestedComment(String requestedComment) {
        this.requestedComment = requestedComment;
    }
    public Date getProcessed() {
        return processed;
    }
    public void setProcessed(Date processed) {
        this.processed = processed;
    }
    public String getProcessedBy() {
        return processedBy;
    }
    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }
    public String getProcessedComment() {
        return processedComment;
    }
    public void setProcessedComment(String processedComment) {
        this.processedComment = processedComment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((AuditStructure) obj);
    }

    public boolean equals(AuditStructure other) {
        if (other == null)
            return false;

        if (getAuditId() == null) {
            if (other.getAuditId() != null)
                return false;
        } else if (!getAuditId().equals(other.getAuditId()))
            return false;
        if (getAuditVersion() == null) {
            if (other.getAuditVersion() != null)
                return false;
        } else if (!getAuditVersion().equals(other.getAuditVersion()))
            return false;
        if (getAuditTable() == null) {
            if (other.getAuditTable() != null)
                return false;
        } else if (!getAuditTable().equals(other.getAuditTable()))
            return false;
        if (getAuditOperation() == null) {
            if (other.getAuditOperation() != null)
                return false;
        } else if (!getAuditOperation().equals(other.getAuditOperation()))
            return false;

        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        if (getVersion() == null) {
            if (other.getVersion() != null)
                return false;
        } else if (!getVersion().equals(other.getVersion()))
            return false;
        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getParentId() == null) {
            if (other.getParentId() != null)
                return false;
        } else if (!getParentId().equals(other.getParentId()))
            return false;
        if (getMnemonic() == null) {
            if (other.getMnemonic() != null)
                return false;
        } else if (!getMnemonic().equals(other.getMnemonic()))
            return false;
        if (getMnemonicEquivalence() == null) {
            if (other.getMnemonicEquivalence() != null)
                return false;
        } else if (!getMnemonicEquivalence().equals(other.getMnemonicEquivalence()))
            return false;
        if (getOrdering() == null) {
            if (other.getOrdering() != null)
                return false;
        } else if (!getOrdering().equals(other.getOrdering()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getRequested() == null) {
            if (other.getRequested() != null)
                return false;
        } else if (!getRequested().equals(other.getRequested()))
            return false;
        if (getRequestedBy() == null) {
            if (other.getRequestedBy() != null)
                return false;
        } else if (!getRequestedBy().equals(other.getRequestedBy()))
            return false;
        if (getRequestedComment() == null) {
            if (other.getRequestedComment() != null)
                return false;
        } else if (!getRequestedComment().equals(other.getRequestedComment()))
            return false;
        if (getProcessed() == null) {
            if (other.getProcessed() != null)
                return false;
        } else if (!getProcessed().equals(other.getProcessed()))
            return false;
        if (getProcessedBy() == null) {
            if (other.getProcessedBy() != null)
                return false;
        } else if (!getProcessedBy().equals(other.getProcessedBy()))
            return false;
        if (getProcessedComment() == null) {
            if (other.getProcessedComment() != null)
                return false;
        } else if (!getProcessedComment().equals(other.getProcessedComment()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuditId(), getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"audit_id\": "               + getAuditId());
        sb.append(", \"audit_version\": "        + getAuditVersion());
        sb.append(", \"audit_table\": "          + getAuditTable());
        sb.append(", \"audit_operation\": "      + getAuditOperation());
        sb.append(", \"id\": "                   + getId());
        sb.append(", \"version\": "              + getVersion());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"parent_id\": "            + getParentId());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"mnemonic_equivalence\": " + getMnemonicEquivalence());
        sb.append(", \"ordering\": "             + getOrdering());
        sb.append(", \"description\": "          + getDescription());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append(", \"requested\": "            + getRequested());
        sb.append(", \"requested_by\": "         + getRequestedBy());
        sb.append(", \"requested_comment\": "    + getRequestedComment());
        sb.append(", \"processed\": "            + getProcessed());
        sb.append(", \"processed_by\": "         + getProcessedBy());
        sb.append(", \"processed_comment\": "    + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

}

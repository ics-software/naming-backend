/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.openepics.names.rest.beans.Status;

/**
 * This entity represents a name.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "name")
public class Name extends NameStructure {

    public static final String FIELD_SYSTEM_GROUP_ID             = "systemgroupId";
    public static final String FIELD_SYSTEM_ID                   = "systemId";
    public static final String FIELD_SUBSYSTEM_ID                = "subsystemId";
    public static final String FIELD_DEVICETYPE_ID               = "devicetypeId";
    public static final String FIELD_INSTANCE_INDEX              = "instanceIndex";
    public static final String FIELD_CONVENTION_NAME             = "conventionName";
    public static final String FIELD_CONVENTION_NAME_EQUIVALENCE = "conventionNameEquivalence";

    @Column(name = "systemgroup_id")
    private Long systemGroupId;
    @Column(name = "system_id")
    private Long systemId;
    @Column(name = "subsystem_id")
    private Long subsystemId;
    @Column(name = "devicetype_id")
    private Long deviceTypeId;
    @Column(name = "instance_index")
    private String instanceIndex;
    @Column(name = "convention_name")
    private String conventionName;
    @Column(name = "convention_name_equivalence")
    private String conventionNameEquivalence;

    /**
     * Constructor for name.
     */
    public Name() {
    }

    /**
     * Constructor for name.
     *
     * @param uuid uuid
     * @param systemGroupId system group id
     * @param systemId system id
     * @param subsystemId subsystem id
     * @param deviceTypeId device type id
     * @param index index
     * @param conventionName convention name
     * @param conventionNameEquivalence convention name equivalence
     * @param description description
     * @param status status
     * @param deleted deleted
     * @param requested requested
     * @param requestedBy requested by
     * @param requestedComment requested comment
     */
    public Name(UUID uuid, Long systemGroupId, Long systemId, Long subsystemId, Long deviceTypeId,
            String index, String conventionName, String conventionNameEquivalence, String description,
            Status status, Boolean deleted,
            Date requested, String requestedBy, String requestedComment) {
        setUuid(uuid);
        setSystemGroupId(systemGroupId);
        setSystemId(systemId);
        setSubsystemId(subsystemId);
        setDeviceTypeId(deviceTypeId);
        setInstanceIndex(index);
        setConventionName(conventionName);
        setConventionNameEquivalence(conventionNameEquivalence);
        setDescription(description);
        setStatus(status);
        setDeleted(deleted);
        setRequested(requested);
        setRequestedBy(requestedBy);
        setRequestedComment(requestedComment);
    }

    public Long getSystemGroupId() {
        return systemGroupId;
    }
    public void setSystemGroupId(Long systemGroupId) {
        this.systemGroupId = systemGroupId;
    }
    public Long getSystemId() {
        return systemId;
    }
    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }
    public Long getSubsystemId() {
        return subsystemId;
    }
    public void setSubsystemId(Long subsystemId) {
        this.subsystemId = subsystemId;
    }
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }
    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }
    public String getInstanceIndex() {
        return instanceIndex;
    }
    public void setInstanceIndex(String instanceIndex) {
        this.instanceIndex = instanceIndex;
    }
    public String getConventionName() {
        return conventionName;
    }
    public void setConventionName(String conventionName) {
        this.conventionName = conventionName;
    }
    public String getConventionNameEquivalence() {
        return conventionNameEquivalence;
    }
    public void setConventionNameEquivalence(String conventionNameEquivalence) {
        this.conventionNameEquivalence = conventionNameEquivalence;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((Name) obj);
    }

    public boolean equals(Name other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getSystemGroupId() == null) {
            if (other.getSystemGroupId() != null)
                return false;
        } else if (!getSystemGroupId().equals(other.getSystemGroupId()))
            return false;
        if (getSystemId() == null) {
            if (other.getSystemId() != null)
                return false;
        } else if (!getSystemId().equals(other.getSystemId()))
            return false;
        if (getSubsystemId() == null) {
            if (other.getSubsystemId() != null)
                return false;
        } else if (!getSubsystemId().equals(other.getSubsystemId()))
            return false;
        if (getDeviceTypeId() == null) {
            if (other.getDeviceTypeId() != null)
                return false;
        } else if (!getDeviceTypeId().equals(other.getDeviceTypeId()))
            return false;
        if (getInstanceIndex() == null) {
            if (other.getInstanceIndex() != null)
                return false;
        } else if (!getInstanceIndex().equals(other.getInstanceIndex()))
            return false;
        if (getConventionName() == null) {
            if (other.getConventionName() != null)
                return false;
        } else if (!getConventionName().equals(other.getConventionName()))
            return false;
        if (getConventionNameEquivalence() == null) {
            if (other.getConventionNameEquivalence() != null)
                return false;
        } else if (!getConventionNameEquivalence().equals(other.getConventionNameEquivalence()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"version\": "                     + getVersion());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"systemgroup_id\": "              + getSystemGroupId());
        sb.append(", \"system_id\": "                   + getSystemId());
        sb.append(", \"subsystem_id\": "                + getSubsystemId());
        sb.append(", \"devicetype_id\": "               + getDeviceTypeId());
        sb.append(", \"instance_index\": "              + getInstanceIndex());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"convention_name_equivalence\": " + getConventionNameEquivalence());
        sb.append(", \"description\": "                 + getDescription());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append(", \"requested\": "                   + getRequested());
        sb.append(", \"requested_by\": "                + getRequestedBy());
        sb.append(", \"requested_comment\": "           + getRequestedComment());
        sb.append(", \"processed\": "                   + getProcessed());
        sb.append(", \"processed_by\": "                + getProcessedBy());
        sb.append(", \"processed_comment\": "           + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                            + getId());
        sb.append(", \"uuid\": "                        + getUuid());
        sb.append(", \"convention_name\": "             + getConventionName());
        sb.append(", \"status\": "                      + getStatus());
        sb.append(", \"deleted\": "                     + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

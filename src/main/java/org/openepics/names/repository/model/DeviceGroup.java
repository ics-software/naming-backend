/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.openepics.names.rest.beans.Status;

/**
 * This entity represents a device group name part.
 *
 * @author Lars Johansson
 */
@Entity
@Table(name = "devicegroup")
public class DeviceGroup extends Structure {

    public static final String FIELD_PARENT_ID = "parentId";

    public static final String FUNCTION_GET_PARENT_UUID_DEVICEGROUP   = "get_parent_uuid_devicegroup";
    public static final String FUNCTION_GET_MNEMONIC_PATH_DEVICEGROUP = "get_mnemonic_path_devicegroup";

    @Column(name = "parent_id")
    private Long parentId;

    /**
     * Constructor for device group.
     */
    public DeviceGroup() {
    }

    /**
     * Constructor for device group.
     *
     * @param uuid uuid
     * @param parentId parent id
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param ordering ordering
     * @param description description
     * @param status status
     * @param deleted deleted
     * @param processed processed
     * @param processedBy processed by
     * @param processedComment processed comment
     */
    public DeviceGroup(UUID uuid, Long parentId,
            String mnemonic, String mnemonicEquivalence, Integer ordering, String description,
            Status status, Boolean deleted,
            Date processed, String processedBy, String processedComment) {
        super(uuid, mnemonic, mnemonicEquivalence, ordering,
                description, status, deleted,
                processed, processedBy, processedComment);
        setParentId(parentId);
    }

    /**
     * Constructor for device group.
     *
     * @param auditStructure audit structure
     */
    public DeviceGroup(AuditStructure auditStructure) {
        super(auditStructure.getUuid(),
                auditStructure.getMnemonic(), auditStructure.getMnemonicEquivalence(), auditStructure.getOrdering(), auditStructure.getDescription(),
                auditStructure.getStatus(), auditStructure.isDeleted(),
                auditStructure.getProcessed(), auditStructure.getProcessedBy(), auditStructure.getProcessedComment());
        setParentId(auditStructure.getParentId());
        this.id = auditStructure.getId();
    }

    public Long getParentId() {
        return parentId;
    }
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((DeviceGroup) obj);
    }

    public boolean equals(DeviceGroup other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getParentId() == null) {
            if (other.getParentId() != null)
                return false;
        } else if (!getParentId().equals(other.getParentId()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                     + getId());
        sb.append(", \"version\": "              + getVersion());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"parent_id\": "            + getParentId());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"mnemonic_equivalence\": " + getMnemonicEquivalence());
        sb.append(", \"ordering\": "             + getOrdering());
        sb.append(", \"description\": "          + getDescription());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append(", \"requested\": "            + getRequested());
        sb.append(", \"requested_by\": "         + getRequestedBy());
        sb.append(", \"requested_comment\": "    + getRequestedComment());
        sb.append(", \"processed\": "            + getProcessed());
        sb.append(", \"processed_by\": "         + getProcessedBy());
        sb.append(", \"processed_comment\": "    + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": " + getId());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

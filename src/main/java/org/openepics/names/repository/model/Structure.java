/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.openepics.names.rest.beans.Status;

/**
 * A superclass implementing properties required by JPA. It should be extended by
 * System structure and Device structure classes that need to be persisted to the database.
 *
 * @author Lars Johansson
 */
@MappedSuperclass
public class Structure extends NameStructure {

    public static final String FIELD_MNEMONIC             = "mnemonic";
    public static final String FIELD_MNEMONIC_EQUIVALENCE = "mnemonicEquivalence";
    public static final String FIELD_ORDERING             = "ordering";

    private String mnemonic;
    @Column(name = "mnemonic_equivalence")
    private String mnemonicEquivalence;
    private Integer ordering;

    /**
     * Constructor for structure.
     */
    public Structure() {
    }

    /**
     * Constructor for structure.
     *
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param ordering ordering
     * @param description description
     * @param status status
     * @param deleted deleted
     * @param processed processed
     * @param processedBy processed by
     * @param processedComment processed comment
     */
    public Structure(UUID uuid,
            String mnemonic, String mnemonicEquivalence, Integer ordering, String description,
            Status status, Boolean deleted,
            Date processed, String processedBy, String processedComment) {
        setUuid(uuid);
        setMnemonic(mnemonic);
        setMnemonicEquivalence(mnemonicEquivalence);
        setOrdering(ordering);
        setDescription(description);
        setStatus(status);
        setDeleted(deleted);
        setProcessed(processed);
        setProcessedBy(processedBy);
        setProcessedComment(processedComment);
    }

    public String getMnemonic() {
        return mnemonic;
    }
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    public String getMnemonicEquivalence() {
        return mnemonicEquivalence;
    }
    public void setMnemonicEquivalence(String mnemonicEquivalence) {
        this.mnemonicEquivalence = mnemonicEquivalence;
    }
    public Integer getOrdering() {
        return ordering;
    }
    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals((Structure) obj);
    }

    public boolean equals(Structure other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getMnemonic() == null) {
            if (other.getMnemonic() != null)
                return false;
        } else if (!getMnemonic().equals(other.getMnemonic()))
            return false;
        if (getMnemonicEquivalence() == null) {
            if (other.getMnemonicEquivalence() != null)
                return false;
        } else if (!getMnemonicEquivalence().equals(other.getMnemonicEquivalence()))
            return false;
        if (getOrdering() == null) {
            if (other.getOrdering() != null)
                return false;
        } else if (!getOrdering().equals(other.getOrdering()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                     + getId());
        sb.append(", \"version\": "              + getVersion());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"mnemonic_equivalence\": " + getMnemonicEquivalence());
        sb.append(", \"ordering\": "             + getOrdering());
        sb.append(", \"description\": "          + getDescription());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append(", \"requested\": "            + getRequested());
        sb.append(", \"requested_by\": "         + getRequestedBy());
        sb.append(", \"requested_comment\": "    + getRequestedComment());
        sb.append(", \"processed\": "            + getProcessed());
        sb.append(", \"processed_by\": "         + getProcessedBy());
        sb.append(", \"processed_comment\": "    + getProcessedComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"id\": "                     + getId());
        sb.append(", \"uuid\": "                 + getUuid());
        sb.append(", \"mnemonic\": "             + getMnemonic());
        sb.append(", \"status\": "               + getStatus());
        sb.append(", \"deleted\": "              + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

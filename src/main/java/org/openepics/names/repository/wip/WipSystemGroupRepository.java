/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.Persistable;
import org.openepics.names.repository.model.wip.WipNameStructure;
import org.openepics.names.repository.model.wip.WipStructure;
import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle system group information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class WipSystemGroupRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * Count system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @return count of system groups
     */
    public Long countSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory) {

        // where
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<WipSystemGroup> from = cq.from(WipSystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, cq, from, deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                includeHistory).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of system groups
     */
    public List<WipSystemGroup> readSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        return readSystemGroups(deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.FALSE, null, null, null, null);
    }

    /**
     * Find system groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of system groups
     */
    public List<WipSystemGroup> readSystemGroups(Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory, FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // where
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WipSystemGroup> cq = cb.createQuery(WipSystemGroup.class);
        Root<WipSystemGroup> from = cq.from(WipSystemGroup.class);

        cq.where(cb.and(preparePredicatesSystemGroups(cb, cq, from, deleted,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                includeHistory).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        TypedQuery<WipSystemGroup> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for system groups.
     *
     * @param cb criteria builder
     * @param cq criteria query
     * @param from criteria query root
     * @param deleted deleted
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesSystemGroups(CriteriaBuilder cb, CriteriaQuery cq, Root<WipSystemGroup> from, Boolean deleted,
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory) {

        List<Predicate> predicates = new ArrayList<>();

        if (!Boolean.TRUE.equals(includeHistory)) {
            // purpose of Naming to show valid entries
            //     therefore
            //         exclude obsolete values unless history requested
            //         make sure to not exclude present and future values (latest approved and pending)
            //
            // condition(s)
            //     exclude content (with latest) before latest
            //     exclude content (with latest) after  latest (cancelled, rejected)
            //     keep most recent content (without latest)   (to have most recent in line of uuid without latest)
            //         <-->
            //         select * from structure s
            //         where (
            //                     not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true))
            //                 and not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
            //                 and not (not exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from structure s2 where s2."uuid" = s."uuid" and s2.latest=false))
            //         )
            //         <-->
            //                     not (exists (subquery id) and s.id < (subquery id))
            //                 and not (exists (subquery id) and s.id > (subquery id) and s.status in ('CANCELLED', 'REJECTED'))
            //                 and not (not exists (subquery id) and s.id < (subquery 2))
            //
            // note
            //     persistence libraries may optimize query
            //         e.g. change (!a and !b) to !(a or b)

            Subquery<Long> sub = cq.subquery(Long.class);
            Root<WipSystemGroup> fromSub = sub.from(WipSystemGroup.class);
            sub.where(cb.and(
                    cb.equal(fromSub.get(WipNameStructure.FIELD_UUID),   from.get(WipNameStructure.FIELD_UUID)),
                    cb.equal(fromSub.get(WipNameStructure.FIELD_LATEST), Boolean.TRUE)
                    ));
            sub.select(fromSub.get(Persistable.FIELD_ID));

            Subquery<Long> sub2 = cq.subquery(Long.class);
            Root<WipSystemGroup> fromSub2 = sub2.from(WipSystemGroup.class);
            sub2.where(cb.and(
                    cb.equal(fromSub2.get(WipNameStructure.FIELD_UUID),   from.get(WipNameStructure.FIELD_UUID)),
                    cb.equal(fromSub2.get(WipNameStructure.FIELD_LATEST), Boolean.FALSE)
                    ));
            sub2.select(cb.max(fromSub2.get(Persistable.FIELD_ID)));

            Predicate predicateExclude =
                    cb.and(
                            cb.not(cb.and(
                                    cb.exists(sub),
                                    cb.lessThan(from.get(Persistable.FIELD_ID).as(Long.class), sub))),
                            cb.not(cb.and(
                                    cb.exists(sub),
                                    cb.greaterThan(from.get(Persistable.FIELD_ID).as(Long.class), sub),
                                    cb.or(
                                            cb.equal(from.get(WipNameStructure.FIELD_STATUS), Status.CANCELLED),
                                            cb.equal(from.get(WipNameStructure.FIELD_STATUS), Status.REJECTED)))),
                            cb.not(cb.and(
                                    cb.not(cb.exists(sub)),
                                    cb.lessThan(from.get(Persistable.FIELD_ID).as(Long.class), sub2)))
                            );
            predicates.add(predicateExclude);
        }

        if (deleted != null) {
            predicates.add(cb.equal(from.get(WipNameStructure.FIELD_DELETED), deleted));
        }

        // prepare pattern
        //     jpa query characters % and _
        //     remove excess % characters

        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(WipNameStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }
        if (!StringUtils.isEmpty(mnemonic)) {
            predicates.add(cb.and(cb.like(from.get(WipStructure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonic))));
        }
        if (!StringUtils.isEmpty(mnemonicEquivalence)) {
            predicates.add(cb.and(cb.like(from.get(WipStructure.FIELD_MNEMONIC_EQUIVALENCE), RepositoryUtil.preparePattern(mnemonicEquivalence))));
        }
        if (!StringUtils.isEmpty(mnemonicPath)) {
            predicates.add(cb.and(cb.like(from.get(WipStructure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonicPath))));
        }
        if (!StringUtils.isEmpty(description)) {
            predicates.add(cb.and(cb.like(from.get(WipNameStructure.FIELD_DESCRIPTION), RepositoryUtil.preparePattern(description))));
        }
        if (!StringUtils.isEmpty(who)) {
            predicates.add(cb.and(cb.like(from.get(WipNameStructure.FIELD_PROCESSED_BY), RepositoryUtil.preparePattern(who))));
        }

        return predicates;
    }

    /**
     * Prepare order by query expression.
     *
     * @param root root type in the from clause
     * @param orderBy field on which to order by
     * @return order by query expression
     */
    private Expression<String> orderBy(Root<WipSystemGroup> root, FieldStructure orderBy) {
        // no parent for system group

        if (FieldStructure.UUID.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_UUID);
        } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
            return root.get(WipStructure.FIELD_MNEMONIC);
        } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
            return root.get(WipStructure.FIELD_MNEMONIC);
        } else if (FieldStructure.ORDERING.equals(orderBy)) {
            return root.get(WipStructure.FIELD_ORDERING);
        } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_DESCRIPTION);
        } else if (FieldStructure.WHEN.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_PROCESSED);
        } else {
            return root.get(WipStructure.FIELD_MNEMONIC);
        }
    }

    /**
     * Count system groups history.
     *
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return count of system groups
     */
    public Long countSystemGroupsHistory(
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        // note
        //     deleted - null
        //     includeHistory - true
        // where
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<WipSystemGroup> from = cq.from(WipSystemGroup.class);

        Subquery<String> sub = cq.subquery(String.class);
        Root<WipSystemGroup> fromSub = sub.from(WipSystemGroup.class);
        sub.where(cb.and(preparePredicatesSystemGroups(cb, cq, fromSub, null,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.TRUE).toArray(new Predicate[0])));
        sub.select(fromSub.get(WipNameStructure.FIELD_UUID));

        cq.where(cb.and(cb.in(from.get(WipNameStructure.FIELD_UUID)).value(sub)));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find system groups history.
     *
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param orderBy order by
     * @param isAsc is ascending
     * @return list of system groups
     */
    public List<WipSystemGroup> readSystemGroupsHistory(
            String uuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc) {

        // note
        //     deleted - null
        //     includeHistory - true
        // where
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     outside of method

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WipSystemGroup> cq = cb.createQuery(WipSystemGroup.class);
        Root<WipSystemGroup> from = cq.from(WipSystemGroup.class);

        Subquery<String> sub = cq.subquery(String.class);
        Root<WipSystemGroup> fromSub = sub.from(WipSystemGroup.class);
        sub.where(cb.and(preparePredicatesSystemGroups(cb, cq, fromSub, null,
                uuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.TRUE).toArray(new Predicate[0])));
        sub.select(fromSub.get(WipNameStructure.FIELD_UUID));

        cq.where(cb.and(cb.in(from.get(WipNameStructure.FIELD_UUID)).value(sub)));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        return em.createQuery(cq).getResultList();
    }

    /**
     * Persist system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void createSystemGroup(WipSystemGroup systemGroup) {
        em.persist(systemGroup);
    }

    /**
     * Merge system group into persistence context.
     *
     * @param systemGroup system group
     */
    public void updateSystemGroup(WipSystemGroup systemGroup) {
        em.merge(systemGroup);
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find device group structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipDeviceGroupRepository extends JpaRepository<WipDeviceGroup, Long> {

    // old + verification

    @Query("FROM WipDeviceGroup dg WHERE dg.latest = true AND dg.uuid = ?1")
    WipDeviceGroup findLatestByUuid(String uuid);

    @Query("FROM WipDeviceGroup dg WHERE dg.uuid = ?1")
    List<WipDeviceGroup> findByUuid(String uuid);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipDeviceGroup dg WHERE dg.latest = true AND dg.deleted = false AND dg.uuid = ?1")
    WipDeviceGroup findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipDeviceGroup dg WHERE dg.latest = true")
    List<WipDeviceGroup> findLatest();

    @Query("FROM WipDeviceGroup dg WHERE dg.latest = true AND dg.deleted = false")
    List<WipDeviceGroup> findLatestNotDeleted();

    @Query("FROM WipDeviceGroup dg WHERE dg.latest = true AND dg.deleted = false AND dg.parentUuid = ?1")
    List<WipDeviceGroup> findLatestNotDeletedByParent(String uuid);

    @Query("FROM WipDeviceGroup dg WHERE dg.uuid = ?1 AND dg.id < ?2 ORDER BY dg.id DESC")
    List<WipDeviceGroup> findPreviousByUuidAndId(String uuid, Long id);

}

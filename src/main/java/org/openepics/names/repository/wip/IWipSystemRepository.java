/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find system structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipSystemRepository extends JpaRepository<WipSystem, Long> {

    // old + verification

    @Query("FROM WipSystem sys WHERE sys.uuid = ?1")
    List<WipSystem> findByUuid(String uuid);

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.mnemonic = ?1")
    List<WipSystem> findLatestByMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.uuid = ?1")
    WipSystem findLatestByUuid(String uuid);

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.deleted = false AND sys.mnemonic = ?1")
    WipSystem findLatestNotDeletedByMnemonic(String mnemonic);

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.deleted = false AND sys.uuid = ?1")
    WipSystem findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipSystem sys WHERE sys.latest = true")
    List<WipSystem> findLatest();

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.deleted = false")
    List<WipSystem> findLatestNotDeleted();

    @Query("FROM WipSystem sys WHERE sys.latest = true AND sys.deleted = false AND sys.parentUuid = ?1")
    List<WipSystem> findLatestNotDeletedByParent(String uuid);

    @Query("FROM WipSystem sys WHERE sys.uuid = ?1 AND sys.id < ?2 ORDER BY sys.id DESC")
    List<WipSystem> findPreviousByUuidAndId(String uuid, Long id);

}

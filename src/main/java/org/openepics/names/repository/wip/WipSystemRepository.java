/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.Persistable;
import org.openepics.names.repository.model.wip.WipNameStructure;
import org.openepics.names.repository.model.wip.WipStructure;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle system information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class WipSystemRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * Count systems.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @return count of systems
     */
    public Long countSystems(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<WipSystem> from = cq.from(WipSystem.class);

        cq.where(cb.and(preparePredicatesSystems(cb, cq, from, deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                includeHistory).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find systems.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of systems
     */
    public List<WipSystem> readSystems(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        return readSystems(deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.FALSE, null, null, null, null);
    }

    /**
     * Find systems.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of systems
     */
    public List<WipSystem> readSystems(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory, FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WipSystem> cq = cb.createQuery(WipSystem.class);
        Root<WipSystem> from = cq.from(WipSystem.class);

        cq.where(cb.and(preparePredicatesSystems(cb, cq, from, deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                includeHistory).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        TypedQuery<WipSystem> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for systems.
     *
     * @param cb criteria builder
     * @param cq criteria query
     * @param from criteria query root
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param includeHistory include history
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesSystems(CriteriaBuilder cb, CriteriaQuery cq, Root<WipSystem> from, Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            Boolean includeHistory) {

        List<Predicate> predicates = new ArrayList<>();

        if (!Boolean.TRUE.equals(includeHistory)) {
            // purpose of Naming to show valid entries
            //     therefore
            //         exclude some values unless history requested
            //         make sure to not exclude present and future values (latest approved and pending)
            //
            // condition(s)
            //     exclude content (with latest) before latest
            //     exclude content (with latest) after  latest (cancelled, rejected)
            //     keep most recent content (without latest)   (to have most recent in line of uuid without latest)
            //         <-->
            //         select * from structure s
            //         where (
            //                     not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true))
            //                 and not (exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id > (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.status in ('CANCELLED', 'REJECTED'))
            //                 and not (not exists (select s2.id from structure s2 where s2."uuid" = s."uuid" and s2.latest=true) and s.id < (select max(s2.id) from structure s2 where s2."uuid" = s."uuid" and s2.latest=false))
            //         )
            //         <-->
            //                     not (exists (subquery id) and s.id < (subquery id))
            //                 and not (exists (subquery id) and s.id > (subquery id) and s.status in ('CANCELLED', 'REJECTED'))
            //                 and not (not exists (subquery id) and s.id < (subquery 2))
            //
            // note
            //     persistence libraries may optimize query
            //         e.g. change (!a and !b) to !(a or b)

            Subquery<Long> sub = cq.subquery(Long.class);
            Root<WipSystem> fromSub = sub.from(WipSystem.class);
            sub.where(cb.and(
                    cb.equal(fromSub.get(WipNameStructure.FIELD_UUID),   from.get(WipNameStructure.FIELD_UUID)),
                    cb.equal(fromSub.get(WipNameStructure.FIELD_LATEST), Boolean.TRUE)
                    ));
            sub.select(fromSub.get(Persistable.FIELD_ID));

            Subquery<Long> sub2 = cq.subquery(Long.class);
            Root<WipSystem> fromSub2 = sub2.from(WipSystem.class);
            sub2.where(cb.and(
                    cb.equal(fromSub2.get(WipNameStructure.FIELD_UUID),   from.get(WipNameStructure.FIELD_UUID)),
                    cb.equal(fromSub2.get(WipNameStructure.FIELD_LATEST), Boolean.FALSE)
                    ));
            sub2.select(cb.max(fromSub2.get(Persistable.FIELD_ID)));

            Predicate predicateExclude =
                    cb.and(
                            cb.not(cb.and(
                                    cb.exists(sub),
                                    cb.lessThan(from.get(Persistable.FIELD_ID).as(Long.class), sub))),
                            cb.not(cb.and(
                                    cb.exists(sub),
                                    cb.greaterThan(from.get(Persistable.FIELD_ID).as(Long.class), sub),
                                    cb.or(
                                            cb.equal(from.get(WipNameStructure.FIELD_STATUS), Status.CANCELLED),
                                            cb.equal(from.get(WipNameStructure.FIELD_STATUS), Status.REJECTED)))),
                            cb.not(cb.and(
                                    cb.not(cb.exists(sub)),
                                    cb.lessThan(from.get(Persistable.FIELD_ID).as(Long.class), sub2)))
                            );
            predicates.add(predicateExclude);
        }

        if (deleted != null) {
            predicates.add(cb.equal(from.get(WipNameStructure.FIELD_DELETED), deleted));
        }

        // prepare pattern
        //     jpa query characters % and _
        //     remove excess % characters

        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(WipNameStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }
        if (!StringUtils.isEmpty(parentUuid)) {
            predicates.add(cb.and(cb.equal(from.get(WipSystem.FIELD_PARENT_UUID), RepositoryUtil.preparePattern(parentUuid))));
        }
        if (!StringUtils.isEmpty(mnemonic)) {
            predicates.add(cb.and(cb.like(from.get(WipStructure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonic))));
        }
        if (!StringUtils.isEmpty(mnemonicEquivalence)) {
            predicates.add(cb.and(cb.like(from.get(WipStructure.FIELD_MNEMONIC_EQUIVALENCE), RepositoryUtil.preparePattern(mnemonicEquivalence))));
        }
        if (!StringUtils.isEmpty(mnemonicPath)) {
            predicates.add(cb.and(cb.like(cb.function(WipSystem.FUNCTION_GET_MNEMONIC_PATH_SYSTEM, String.class, from.get(WipNameStructure.FIELD_UUID)), RepositoryUtil.preparePattern(mnemonicPath))));
        }
        if (!StringUtils.isEmpty(description)) {
            predicates.add(cb.and(cb.like(from.get(WipNameStructure.FIELD_DESCRIPTION), RepositoryUtil.preparePattern(description))));
        }
        if (!StringUtils.isEmpty(who)) {
            predicates.add(cb.and(cb.like(from.get(WipNameStructure.FIELD_PROCESSED_BY), RepositoryUtil.preparePattern(who))));
        }

        return predicates;
    }

    /**
     * Prepare order by query expression.
     *
     * @param root root type in the from clause
     * @param orderBy field on which to order by
     * @return order by query expression
     */
    private Expression<String> orderBy(Root<WipSystem> root, FieldStructure orderBy) {
        if (FieldStructure.UUID.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_UUID);
        } else if (FieldStructure.PARENT.equals(orderBy)) {
            return root.get(WipSystem.FIELD_PARENT_UUID);
        } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
            return root.get(WipStructure.FIELD_MNEMONIC);
        } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
            return root.get(WipStructure.FIELD_MNEMONIC);
        } else if (FieldStructure.ORDERING.equals(orderBy)) {
            return root.get(WipStructure.FIELD_ORDERING);
        } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_DESCRIPTION);
        } else if (FieldStructure.WHEN.equals(orderBy)) {
            return root.get(WipNameStructure.FIELD_PROCESSED);
        } else {
            return root.get(WipStructure.FIELD_MNEMONIC);
        }
    }

    /**
     * Count systems history.
     *
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return count of systems
     */
    public Long countSystemsHistory(
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        // note
        //     use of function for mnemonic path
        //     deleted - null
        //     includeHistory - true
        // where
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<WipSystem> from = cq.from(WipSystem.class);

        Subquery<String> sub = cq.subquery(String.class);
        Root<WipSystem> fromSub = sub.from(WipSystem.class);
        sub.where(cb.and(preparePredicatesSystems(cb, cq, fromSub, null,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.TRUE).toArray(new Predicate[0])));
        sub.select(fromSub.get(WipNameStructure.FIELD_UUID));

        cq.where(cb.and(cb.in(from.get(WipNameStructure.FIELD_UUID)).value(sub)));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find systems history.
     *
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param orderBy order by
     * @param isAsc is ascending
     * @return list of systems
     */
    public List<WipSystem> readSystemsHistory(
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc) {

        // note
        //     use of function for mnemonic path
        //     deleted - null
        //     includeHistory - true
        // where
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     outside of method

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<WipSystem> cq = cb.createQuery(WipSystem.class);
        Root<WipSystem> from = cq.from(WipSystem.class);

        Subquery<String> sub = cq.subquery(String.class);
        Root<WipSystem> fromSub = sub.from(WipSystem.class);
        sub.where(cb.and(preparePredicatesSystems(cb, cq, fromSub, null,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                Boolean.TRUE).toArray(new Predicate[0])));
        sub.select(fromSub.get(WipNameStructure.FIELD_UUID));

        cq.where(cb.and(cb.in(from.get(WipNameStructure.FIELD_UUID)).value(sub)));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        return em.createQuery(cq).getResultList();
    }

    /**
     * Persist system into persistence context.
     *
     * @param system system
     */
    public void createSystem(WipSystem system) {
        em.persist(system);
    }

    /**
     * Merge system into persistence context.
     *
     * @param system system
     */
    public void updateSystem(WipSystem system) {
        em.merge(system);
    }

}

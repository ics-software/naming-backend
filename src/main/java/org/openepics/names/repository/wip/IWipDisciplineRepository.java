/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipDiscipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find discipline structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipDisciplineRepository extends JpaRepository<WipDiscipline, Long> {

    // old + verification

    @Query("FROM WipDiscipline di WHERE di.latest = true AND di.uuid = ?1")
    WipDiscipline findLatestByUuid(String uuid);

    @Query("FROM WipDiscipline di WHERE di.uuid = ?1")
    List<WipDiscipline> findByUuid(String uuid);

    @Query("FROM WipDiscipline di WHERE di.latest = true AND di.mnemonic = ?1")
    List<WipDiscipline> findLatestByMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipDiscipline di WHERE di.latest = true AND di.deleted = false AND di.mnemonic = ?1")
    WipDiscipline findLatestNotDeletedByMnemonic(String mnemonic);

    @Query("FROM WipDiscipline di WHERE di.latest = true AND di.deleted = false AND di.uuid = ?1")
    WipDiscipline findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipDiscipline di WHERE di.latest = true")
    List<WipDiscipline> findLatest();

    @Query("FROM WipDiscipline di WHERE di.latest = true AND di.deleted = false")
    List<WipDiscipline> findLatestNotDeleted();

    @Query("FROM WipDiscipline di WHERE di.uuid = ?1 AND di.id < ?2 ORDER BY di.id DESC")
    List<WipDiscipline> findPreviousByUuidAndId(String uuid, Long id);

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipDeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find device type structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipDeviceTypeRepository extends JpaRepository<WipDeviceType, Long> {

    // old + verification

    @Query("FROM WipDeviceType dt WHERE dt.uuid = ?1")
    List<WipDeviceType> findByUuid(String uuid);

    @Query("FROM WipDeviceType dt WHERE dt.latest = true AND dt.mnemonic = ?1")
    List<WipDeviceType> findLatestByMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipDeviceType dt WHERE dt.latest = true AND dt.uuid = ?1")
    WipDeviceType findLatestByUuid(String uuid);

    @Query("FROM WipDeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.uuid = ?1")
    WipDeviceType findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipDeviceType dt WHERE dt.latest = true")
    List<WipDeviceType> findLatest();

    @Query("FROM WipDeviceType dt WHERE dt.latest = true AND dt.deleted = false")
    List<WipDeviceType> findLatestNotDeleted();

    @Query("FROM WipDeviceType dt WHERE dt.latest = true AND dt.deleted = false AND dt.parentUuid = ?1")
    List<WipDeviceType> findLatestNotDeletedByParent(String uuid);

    @Query("FROM WipDeviceType dt WHERE dt.uuid = ?1 AND dt.id < ?2 ORDER BY dt.id DESC")
    List<WipDeviceType> findPreviousByUuidAndId(String uuid, Long id);

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.Date;
import java.util.List;

import org.openepics.names.repository.model.wip.WipName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find name information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipNameRepository extends JpaRepository<WipName, Long> {

    // old + verification

    @Query("FROM WipName n WHERE n.latest = true AND n.conventionName = ?1")
    WipName findLatestByConventionName(String conventionName);

    @Query("FROM WipName n WHERE n.latest = true")
    List<WipName> findLatest();

    @Query("FROM WipName n WHERE n.latest = true AND n.deleted = false")
    List<WipName> findLatestNotDeleted();

    @Query("SELECT n FROM WipName n, WipSystemGroup sg "
            + "WHERE n.latest = true "
            + "AND sg.uuid = n.systemGroupUuid "
            + "AND sg.latest = true "
            + "AND sg.mnemonic = ?1")
    List<WipName> findLatestBySystemGroupMnemonic(String mnemonic);

    @Query("SELECT n FROM WipName n, WipSystem sys "
            + "WHERE n.latest = true "
            + "AND sys.uuid = n.systemUuid "
            + "AND sys.latest = true "
            + "AND sys.mnemonic = ?1")
    List<WipName> findLatestBySystemMnemonic(String mnemonic);

    @Query("SELECT n FROM WipName n, WipSubsystem sub, WipSystem sys "
            + "WHERE n.latest = true "
            + "AND sub.uuid = n.subsystemUuid "
            + "AND sub.latest = true "
            + "AND sys.uuid = sub.parentUuid  "
            + "AND sys.latest = true "
            + "AND sys.mnemonic = ?1")
    List<WipName> findLatestBySystemMnemonicThroughSubsystem(String mnemonic);

    @Query("SELECT n FROM WipName n, WipSubsystem sub "
            + "WHERE n.latest = true "
            + "AND sub.uuid = n.subsystemUuid "
            + "AND sub.latest = true "
            + "AND sub.mnemonic = ?1")
    List<WipName> findLatestBySubsystemMnemonic(String mnemonic);

    @Query("SELECT n FROM WipName n, WipDeviceType dt, WipDeviceGroup dg, WipDiscipline di "
            + "WHERE n.latest = true "
            + "AND dt.uuid = n.deviceTypeUuid "
            + "AND dt.latest = true "
            + "AND dg.uuid = dt.parentUuid "
            + "AND dg.latest = true "
            + "AND di.uuid = dg.parentUuid "
            + "AND di.latest = true "
            + "AND di.mnemonic = ?1")
    List<WipName> findLatestByDisciplineMnemonicThroughDeviceType(String mnemonic);

    @Query("SELECT n FROM WipName n, WipDeviceType dt "
            + "WHERE n.latest = true "
            + "AND dt.uuid = n.deviceTypeUuid "
            + "AND dt.latest = true "
            + "AND dt.mnemonic = ?1")
    List<WipName> findLatestByDeviceTypeMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipName n WHERE n.latest = true AND n.uuid = ?1")
    WipName findLatestByUuid(String uuid);

    @Query("FROM WipName n WHERE n.uuid = ?1")
    List<WipName> findByUuid(String uuid);

    // ----------------------------------------------------------------------------------------------------

    @Query("SELECT n FROM WipName n, WipSystemGroup sg "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND sg.uuid = n.systemGroupUuid "
            + "AND sg.latest = true "
            + "AND sg.uuid = ?1")
    List<WipName> findLatestNotDeletedBySystemGroupUuid(String uuid);

    @Query("SELECT n FROM WipName n, WipSystem sys "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND sys.uuid = n.systemUuid "
            + "AND sys.latest = true "
            + "AND sys.uuid = ?1")
    List<WipName> findLatestNotDeletedBySystemUuid(String uuid);

    @Query("SELECT n FROM WipName n, WipSubsystem sub, WipSystem sys "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND sub.uuid = n.subsystemUuid "
            + "AND sub.latest = true "
            + "AND sys.uuid = sub.parentUuid  "
            + "AND sys.latest = true "
            + "AND sys.uuid = ?1")
    List<WipName> findLatestNotDeletedBySystemUuidThroughSubsystem(String uuid);

    @Query("SELECT n FROM WipName n, WipSubsystem sub "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND sub.uuid = n.subsystemUuid "
            + "AND sub.latest = true "
            + "AND sub.uuid = ?1")
    List<WipName> findLatestNotDeletedBySubsystemUuid(String uuid);

    @Query("SELECT n FROM WipName n, WipDeviceType dt, WipDeviceGroup dg, WipDiscipline di "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND dt.uuid = n.deviceTypeUuid "
            + "AND dt.latest = true "
            + "AND dg.uuid = dt.parentUuid "
            + "AND dg.latest = true "
            + "AND di.uuid = dg.parentUuid "
            + "AND di.latest = true "
            + "AND di.uuid = ?1")
    List<WipName> findLatestNotDeletedByDisciplineUuidThroughDeviceType(String uuid);

    @Query("SELECT n FROM WipName n, WipDeviceType dt "
            + "WHERE n.latest = true "
            + "AND n.deleted = false "
            + "AND dt.uuid = n.deviceTypeUuid "
            + "AND dt.latest = true "
            + "AND dt.uuid = ?1")
    List<WipName> findLatestNotDeletedByDeviceTypeUuid(String uuid);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipName n WHERE n.requested >= ?1 AND n.requested <= ?2")
    List<WipName> findByRequestedBetween(Date from, Date to);

}

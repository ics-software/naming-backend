/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find system group structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipSystemGroupRepository extends JpaRepository<WipSystemGroup, Long> {

    // old + verification

    @Query("FROM WipSystemGroup sg WHERE sg.uuid = ?1")
    List<WipSystemGroup> findByUuid(String uuid);

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true AND sg.mnemonic = ?1")
    List<WipSystemGroup> findLatestByMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true AND sg.uuid = ?1")
    WipSystemGroup findLatestByUuid(String uuid);

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true AND sg.deleted = false AND sg.mnemonic = ?1")
    WipSystemGroup findLatestNotDeletedByMnemonic(String mnemonic);

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true AND sg.deleted = false AND sg.uuid = ?1")
    WipSystemGroup findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true")
    List<WipSystemGroup> findLatest();

    @Query("FROM WipSystemGroup sg WHERE sg.latest = true AND sg.deleted = false")
    List<WipSystemGroup> findLatestNotDeleted();

    @Query("FROM WipSystemGroup sg WHERE sg.uuid = ?1 AND sg.id < ?2 ORDER BY sg.id DESC")
    List<WipSystemGroup> findPreviousByUuidAndId(String uuid, Long id);

}

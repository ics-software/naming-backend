/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.wip;

import java.util.List;

import org.openepics.names.repository.model.wip.WipSubsystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find subsystem structure (name part)information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IWipSubsystemRepository extends JpaRepository<WipSubsystem, Long> {

    // old + verification

    @Query("FROM WipSubsystem sub WHERE sub.uuid = ?1")
    List<WipSubsystem> findByUuid(String uuid);

    @Query("FROM WipSubsystem sub WHERE sub.latest = true AND sub.mnemonic = ?1")
    List<WipSubsystem> findLatestByMnemonic(String mnemonic);

    // ----------------------------------------------------------------------------------------------------

    @Query("FROM WipSubsystem sub WHERE sub.latest = true AND sub.uuid = ?1")
    WipSubsystem findLatestByUuid(String uuid);

    @Query("FROM WipSubsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.uuid = ?1")
    WipSubsystem findLatestNotDeletedByUuid(String uuid);

    @Query("FROM WipSubsystem sub WHERE sub.latest = true")
    List<WipSubsystem> findLatest();

    @Query("FROM WipSubsystem sub WHERE sub.latest = true AND sub.deleted = false")
    List<WipSubsystem> findLatestNotDeleted();

    @Query("FROM WipSubsystem sub WHERE sub.latest = true AND sub.deleted = false AND sub.parentUuid = ?1")
    List<WipSubsystem> findLatestNotDeletedByParent(String uuid);

    @Query("FROM WipSubsystem sub WHERE sub.uuid = ?1 AND sub.id < ?2 ORDER BY sub.id DESC")
    List<WipSubsystem> findPreviousByUuidAndId(String uuid, Long id);

}

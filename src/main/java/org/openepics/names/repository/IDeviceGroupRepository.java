/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.DeviceGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find device group structure (name part) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface IDeviceGroupRepository extends JpaRepository<DeviceGroup, Long> {

    @Query("FROM DeviceGroup dg WHERE dg.uuid = ?1")
    DeviceGroup findByUuid(String uuid);

    @Query("FROM DeviceGroup dg WHERE dg.deleted = false")
    List<DeviceGroup> findNotDeleted();

    @Query("FROM DeviceGroup dg WHERE dg.deleted = false AND dg.parentId = ?1")
    List<DeviceGroup> findNotDeletedByParent(Long id);

}

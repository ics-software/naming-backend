/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.NameStructure;
import org.openepics.names.repository.model.Persistable;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.util.RepositoryUtil;
import org.springframework.stereotype.Repository;

/**
 * Handle device group information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public class DeviceGroupRepository {

    @PersistenceContext
    private EntityManager em;

    /**
     * Count device groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return count of device groups
     */
    public Long countDeviceGroups(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<DeviceGroup> from = cq.from(DeviceGroup.class);

        cq.where(cb.and(preparePredicatesDeviceGroups(cb, cq, from, deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who).toArray(new Predicate[0])));
        cq.select(cb.count(from));

        return em.createQuery(cq).getSingleResult();
    }

    /**
     * Find device groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of device groups
     */
    public List<DeviceGroup> readDeviceGroups(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        return readDeviceGroups(deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who,
                null, null, null, null);
    }

    /**
     * Find device groups.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     * @return list of device groups
     */
    public List<DeviceGroup> readDeviceGroups(Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // note
        //     use of function for mnemonic path
        // where
        //     deleted
        //     queryFields, queryValues
        // order
        //     orderBy, isAsc
        // paging
        //     offset, limit

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<DeviceGroup> cq = cb.createQuery(DeviceGroup.class);
        Root<DeviceGroup> from = cq.from(DeviceGroup.class);

        cq.where(cb.and(preparePredicatesDeviceGroups(cb, cq, from, deleted,
                uuid, parentUuid, mnemonic, mnemonicEquivalence, mnemonicPath, description, who).toArray(new Predicate[0])));
        cq.select(from);

        if (orderBy != null) {
            Expression<String> exp = orderBy(from, orderBy);
            if (BooleanUtils.toBoolean(isAsc)) {
                cq.orderBy(cb.asc(exp));
            } else {
                cq.orderBy(cb.desc(exp));
            }
        }

        TypedQuery<DeviceGroup> query = em.createQuery(cq);
        if (offset != null && limit != null) {
            query.setFirstResult(offset * limit);
            query.setMaxResults(limit);
        }

        return query.getResultList();
    }

    /**
     * Prepare predicates for device groups.
     *
     * @param cb criteria builder
     * @param cq criteria query
     * @param from criteria query root
     * @param deleted deleted
     * @param uuid uuid
     * @param parentUuid parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicEquivalence mnemonic equivalence
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @return list of predicates
     */
    private List<Predicate> preparePredicatesDeviceGroups(CriteriaBuilder cb, CriteriaQuery cq, Root<DeviceGroup> from, Boolean deleted,
            String uuid, String parentUuid, String mnemonic, String mnemonicEquivalence, String mnemonicPath, String description, String who) {

        List<Predicate> predicates = new ArrayList<>();

        if (deleted != null) {
            predicates.add(cb.equal(from.get(NameStructure.FIELD_DELETED), deleted));
        }

        // prepare pattern
        //     jpa query characters % and _
        //     remove excess % characters

        if (!StringUtils.isEmpty(uuid)) {
            predicates.add(cb.and(cb.equal(from.get(NameStructure.FIELD_UUID), RepositoryUtil.preparePattern(uuid))));
        }
        if (!StringUtils.isEmpty(parentUuid)) {
            predicates.add(cb.and(cb.equal(cb.function(DeviceGroup.FUNCTION_GET_PARENT_UUID_DEVICEGROUP, String.class, from.get(Persistable.FIELD_ID)), RepositoryUtil.preparePattern(parentUuid))));
        }
        if (!StringUtils.isEmpty(mnemonic)) {
            predicates.add(cb.and(cb.like(from.get(Structure.FIELD_MNEMONIC), RepositoryUtil.preparePattern(mnemonic))));
        }
        if (!StringUtils.isEmpty(mnemonicEquivalence)) {
            predicates.add(cb.and(cb.like(from.get(Structure.FIELD_MNEMONIC_EQUIVALENCE), RepositoryUtil.preparePattern(mnemonicEquivalence))));
        }
        if (!StringUtils.isEmpty(mnemonicPath)) {
            predicates.add(cb.and(cb.like(cb.function(DeviceGroup.FUNCTION_GET_MNEMONIC_PATH_DEVICEGROUP, String.class, from.get(Persistable.FIELD_ID)), RepositoryUtil.preparePattern(mnemonicPath))));
        }
        if (!StringUtils.isEmpty(description)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_DESCRIPTION), RepositoryUtil.preparePattern(description))));
        }
        if (!StringUtils.isEmpty(who)) {
            predicates.add(cb.and(cb.like(from.get(NameStructure.FIELD_PROCESSED_BY), RepositoryUtil.preparePattern(who))));
        }

        return predicates;
    }

    /**
     * Prepare order by query expression.
     *
     * @param root root type in the from clause
     * @param orderBy field on which to order by
     * @return order by query expression
     */
    private Expression<String> orderBy(Root<DeviceGroup> root, FieldStructure orderBy) {
        if (FieldStructure.UUID.equals(orderBy)) {
            return root.get(NameStructure.FIELD_UUID);
        } else if (FieldStructure.PARENT.equals(orderBy)) {
            return root.get(DeviceGroup.FIELD_PARENT_ID);
        } else if (FieldStructure.MNEMONIC.equals(orderBy)) {
            return root.get(Structure.FIELD_MNEMONIC);
        } else if (FieldStructure.MNEMONICPATH.equals(orderBy)) {
            return root.get(Structure.FIELD_MNEMONIC);
        } else if (FieldStructure.ORDERING.equals(orderBy)) {
            return root.get(Structure.FIELD_ORDERING);
        } else if (FieldStructure.DESCRIPTION.equals(orderBy)) {
            return root.get(NameStructure.FIELD_DESCRIPTION);
        } else if (FieldStructure.WHEN.equals(orderBy)) {
            return root.get(NameStructure.FIELD_PROCESSED);
        } else {
            return root.get(Structure.FIELD_MNEMONIC);
        }
    }

    /**
     * Persist device group into persistence context.
     *
     * @param deviceGroup device group
     */
    public void createDeviceGroup(DeviceGroup deviceGroup) {
        em.persist(deviceGroup);
    }

    /**
     * Merge device group into persistence context.
     *
     * @param deviceGroup device group
     */
    public void updateDeviceGroup(DeviceGroup deviceGroup) {
        em.merge(deviceGroup);
    }

}

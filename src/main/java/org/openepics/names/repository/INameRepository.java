/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository;

import java.util.List;

import org.openepics.names.repository.model.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Find name (device) information in JPA.
 *
 * @author Lars Johansson
 */
@Repository
public interface INameRepository extends JpaRepository<Name, Long> {

    @Query("FROM Name n WHERE n.uuid = ?1")
    Name findByUuid(String uuid);

    @Query("SELECT n FROM Name n, SystemGroup sg "
            + "WHERE n.deleted = false "
            + "AND sg.id = n.systemGroupId "
            + "AND sg.uuid = ?1")
    List<Name> findNotDeletedBySystemGroupUuid(String uuid);

    @Query("SELECT n FROM Name n, System sys "
            + "WHERE n.deleted = false "
            + "AND sys.id = n.systemId "
            + "AND sys.uuid = ?1")
    List<Name> findNotDeletedBySystemUuid(String uuid);

    @Query("SELECT n FROM Name n, Subsystem sub, System sys "
            + "WHERE n.deleted = false "
            + "AND sub.id = n.subsystemId "
            + "AND sys.id = sub.parentId  "
            + "AND sys.uuid = ?1")
    List<Name> findNotDeletedBySystemUuidThroughSubsystem(String uuid);

    @Query("SELECT n FROM Name n, Subsystem sub "
            + "WHERE n.deleted = false "
            + "AND sub.id = n.subsystemId "
            + "AND sub.uuid = ?1")
    List<Name> findNotDeletedBySubsystemUuid(String uuid);

    @Query("SELECT n FROM Name n, DeviceType dt, DeviceGroup dg, Discipline di "
            + "WHERE n.deleted = false "
            + "AND dt.id = n.deviceTypeId "
            + "AND dg.id = dt.parentId "
            + "AND di.id = dg.parentId "
            + "AND di.uuid = ?1")
    List<Name> findNotDeletedByDisciplineUuidThroughDeviceType(String uuid);

    @Query("SELECT n FROM Name n, DeviceType dt "
            + "WHERE n.deleted = false "
            + "AND dt.id = n.deviceTypeId "
            + "AND dt.uuid = ?1")
    List<Name> findNotDeletedByDeviceTypeUuid(String uuid);

}

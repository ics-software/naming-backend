/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.StructureUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.notification.NotificationUtil;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides structures services for system.
 *
 * @author Lars Johansson
 */
@Service
public class SystemService {

    private static final Logger LOGGER = Logger.getLogger(SystemService.class.getName());

    private final NamesService namesService;
    private final SubsystemService subsystemService;
    private final ISystemGroupRepository iSystemGroupRepository;
    private final ISystemRepository iSystemRepository;
    private final ISubsystemRepository iSubsystemRepository;
    private final IAuditStructureRepository iAuditStructureRepository;
    private final SystemRepository systemRepository;
    private final AuditStructureRepository auditStructureRepository;

    @Autowired
    public SystemService(
            NamesService namesService,
            SubsystemService subsystemService,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IAuditStructureRepository iAuditStructureRepository,
            SystemRepository systemRepository,
            AuditStructureRepository auditStructureRepository) {
        this.namesService = namesService;
        this.subsystemService = subsystemService;
        this.iSystemGroupRepository = iSystemGroupRepository;
        this.iSystemRepository = iSystemRepository;
        this.iSubsystemRepository = iSubsystemRepository;
        this.iAuditStructureRepository = iAuditStructureRepository;
        this.systemRepository = systemRepository;
        this.auditStructureRepository = auditStructureRepository;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification createStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     create structure
        //     create audit
        //     additional
        //         automatically create name when system structure is created
        //         conditions on name and structure entry
        //             system structure should exist - mnemonic
        //             name should not exist         - system structure mnemonic path
        //     return
        //         structure element for created structure
        //         notification

        UUID uuid = UUID.randomUUID();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // create structure
        // create audit
        SystemGroup systemGroup = iSystemGroupRepository.findByUuid(structureElementCommand.getParent().toString());
        System system = new System(uuid, systemGroup.getId(),
                mnemonic, equivalenceClassRepresentative, structureElementCommand.getOrdering(),
                structureElementCommand.getDescription(), Status.APPROVED, Boolean.FALSE,
                when, username, null);
        systemRepository.createSystem(system);
        auditStructureRepository.createAuditStructure(new AuditStructure(TextUtil.CREATE, system));

        // additional
        boolean hasMnemonic = !StringUtils.isEmpty(system.getMnemonic());
        boolean existsName = hasMnemonic && namesService.existsName(StructureUtil.getMnemonicPath(system, holderStructures));
        if (hasMnemonic && !existsName) {
            NameElementCommand nameElement = new NameElementCommand(null, system.getUuid(), null, null, StructuresService.SYSTEM_STRUCTURE_ONLY);
            namesService.createName(nameElement, when, username, holderStructures, system);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.CREATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(system, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEM, StructureCommand.CREATE, null, system, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification updateStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     update structure
        //     create audit
        //     previous for notification
        //     additional - update related names
        //     return
        //         structure element for updated structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // update structure
        // create audit
        System system = iSystemRepository.findByUuid(uuid);
        system.setMnemonic(mnemonic);
        system.setMnemonicEquivalence(equivalenceClassRepresentative);
        system.setOrdering(structureElementCommand.getOrdering());
        system.setDescription(structureElementCommand.getDescription());
        systemRepository.updateSystem(system);
        AuditStructure auditStructure = new AuditStructure(TextUtil.UPDATE, system);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SYSTEM.name().toLowerCase(), uuid, auditStructure.getAuditId());
        System previous = new System(previousAuditStructure);

        // additional
        namesService.updateNames(previous, system, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.UPDATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(system, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEM, StructureCommand.UPDATE, previous, system, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification deleteStructure(StructureElementCommand structureElementCommand, Date when, String username,
            HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     delete structure (update)
        //     create audit
        //     previous for notification
        //     additional
        //         delete related names
        //         delete sub structures (and related names)
        //     return
        //         structure element for deleted structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();

        // delete structure
        // create audit
        System system = iSystemRepository.findByUuid(uuid);
        system.setDeleted(Boolean.TRUE);
        system.setAttributesProcessed(when, username, null);
        systemRepository.updateSystem(system);
        AuditStructure auditStructure = new AuditStructure(TextUtil.DELETE, system);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SYSTEM.name().toLowerCase(), uuid, auditStructure.getAuditId());
        System previous = new System(previousAuditStructure);

        // additional
        //     will propagate to sub structures
        namesService.deleteNames(system, username, holderStructures);

        List<StructureElementCommand> commands = Lists.newArrayList();
        List<Subsystem> subsystems = iSubsystemRepository.findNotDeletedByParent(system.getId());
        for (Subsystem subsystem : subsystems) {
            commands.add(new StructureElementCommand(subsystem.getUuid(), Type.SUBSYSTEM, null, null, null, null));
        }
        for (StructureElementCommand command : commands) {
            subsystemService.deleteStructure(command, when, username, holderStructures);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.COMMAND, StructureCommand.DELETE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(system, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEM, StructureCommand.DELETE, previous, system, holderStructures));
    }

}

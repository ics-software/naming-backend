/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditNameRepository;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditNameRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DeviceGroupRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.repository.model.AuditName;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderRepositories;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.NameElementUtil;
import org.openepics.names.util.NameUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.ValidateNameElementUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides names services.
 *
 * @author Lars Johansson
 */
@Service
public class NamesService {

    // note
    //     default
    //         active means valid not deleted
    //         obsolete values are not shown unless history is requested
    //     handling of system structure, device structure
    //         parent system structure uuid (system group, system, subsystem)
    //         parent device structure uuid (device type)
    //     namecommand
    //         cud - create update delete

    private static final Logger LOGGER = Logger.getLogger(NamesService.class.getName());

    private final EssNamingConvention namingConvention;
    private final HolderIRepositories holderIRepositories;
    private final HolderRepositories holderRepositories;

    @Autowired
    public NamesService(
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            IAuditNameRepository iAuditNameRepository,
            IAuditStructureRepository iAuditStructureRepository,
            NameRepository nameRepository,
            SystemGroupRepository systemGroupRepository,
            SystemRepository systemRepository,
            SubsystemRepository subsystemRepository,
            DisciplineRepository disciplineRepository,
            DeviceGroupRepository deviceGroupRepository,
            DeviceTypeRepository deviceTypeRepository,
            AuditNameRepository auditNameRepository,
            AuditStructureRepository auditStructureRepository) {

        this.namingConvention = new EssNamingConvention();
        this.holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository,
                iAuditNameRepository,
                iAuditStructureRepository);
        this.holderRepositories = new HolderRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository,
                auditNameRepository,
                auditStructureRepository);
    }

    @Transactional
    public List<NameElement> createNames(List<NameElementCommand> nameElementCommands, String username) {
        // validation outside method
        // transaction
        //     for each name element
        //         create name
        //         handle name element for created name
        //     no notify
        //     return name elements for created names

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        final List<NameElement> createdNameElements = Lists.newArrayList();
        for (NameElementCommand nameElementCommand : nameElementCommands) {
            NameElement createdNameElement = createName(nameElementCommand, when, username, holderStructures);
            createdNameElements.add(createdNameElement);

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_NAME, TextUtil.ELEMENT_IN, nameElementCommand));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_NAME, TextUtil.ELEMENT_OUT, createdNameElement));
            }
        }

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Create names",
                        nameElementCommands.size(),
                        createdNameElements.size()));
        return createdNameElements;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public NameElement createName(NameElementCommand nameElementCommand, Date when, String username, HolderStructures holderStructures) {
        return createName(nameElementCommand, when, username, holderStructures, null);
    }
    @Transactional(propagation = Propagation.MANDATORY)
    public NameElement createName(NameElementCommand nameElementCommand, Date when, String username, HolderStructures holderStructures, Structure structure) {
        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     find & prepare
        //     create name
        //     create audit
        //     return name element for created name

        UUID uuid = UUID.randomUUID();
        UUID parentSystemStructure = nameElementCommand.getParentSystemStructure();
        UUID parentDeviceStructure = nameElementCommand.getParentDeviceStructure();
        String index = nameElementCommand.getIndex();
        String description = nameElementCommand.getDescription();
        String comment = null;

        // find & prepare
        //     system structure - system group, system, subsystem - one of the three expected to be non-null, other two expected to be null
        //     device structure - device type - may be null
        SystemGroup systemGroup = holderIRepositories.systemGroupRepository().findByUuid(parentSystemStructure.toString());
        System system = holderIRepositories.systemRepository().findByUuid(parentSystemStructure.toString());
        Subsystem subsystem = holderIRepositories.subsystemRepository().findByUuid(parentSystemStructure.toString());
        DeviceType deviceType = null;
        if (parentDeviceStructure != null) {
            deviceType = holderIRepositories.deviceTypeRepository().findByUuid(parentDeviceStructure.toString());
        }

        String derivedName = null;
        if (systemGroup != null) {
            derivedName = NameUtil.getName(systemGroup, deviceType, index, holderStructures);
        } else if (system != null) {
            derivedName = NameUtil.getName(system, deviceType, index, holderStructures);
        } else if (subsystem != null) {
            derivedName = NameUtil.getName(subsystem, deviceType, index, holderStructures);
        }

        // create name
        // create audit
        Name name = new Name(uuid,
                systemGroup != null ? systemGroup.getId() : null,
                system != null ? system.getId() : null,
                subsystem != null ? subsystem.getId() : null,
                deviceType != null ? deviceType.getId() : null,
                index, derivedName, namingConvention.equivalenceClassRepresentative(derivedName), description,
                Status.APPROVED, Boolean.FALSE,
                when, username, comment);
        holderRepositories.nameRepository().createName(name);
        holderRepositories.auditNameRepository().createAuditName(new AuditName(TextUtil.CREATE, name));

        return NameElementUtil.getNameElement(name, holderStructures, structure);
    }

    // ----------------------------------------------------------------------------------------------------

    public ResponsePageNameElements readNames(Boolean deleted,
            String uuid, String name, String systemStructure, String deviceStructure, String index, String description, String who,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readNames(deleted,
                uuid, name, systemStructure, deviceStructure, index, description, who,
                Boolean.FALSE, orderBy, isAsc, offset, limit);
    }

    public ResponsePageNameElements readNames(Boolean deleted,
            String uuid, String name, String systemStructure, String deviceStructure, String index, String description, String who,
            Boolean includeHistory, FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        // validation outside method
        // read names
        // return name elements for names

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "deleted", deleted));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "uuid", uuid));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "name", name));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "systemStructure", systemStructure));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "deviceStructure", deviceStructure));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "index", index));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "description", description));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "who", who));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "includeHistory", includeHistory));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "orderBy", orderBy));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "isAsc", isAsc));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "offset", offset));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES, "limit", limit));
        }

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        // one name entry will give one name element
        //     history does not affect totalCount, sorting, pagination
        List<NameElement> nameElements = null;
        Long totalCount = null;
        if (Boolean.TRUE.equals(includeHistory)) {
            List<AuditName> auditNames = holderRepositories.auditNameRepository().readAuditNames(uuid, offset, limit);
            totalCount = holderRepositories.auditNameRepository().countAuditNames(uuid);

            nameElements = NameElementUtil.getNameElementsForAuditNames(auditNames, holderStructures);
        } else {
            List<Name> names = holderRepositories.nameRepository().readNames(deleted, uuid, name, null, systemStructure, deviceStructure, index, description, who, orderBy, isAsc, offset, limit);
            totalCount = holderRepositories.nameRepository().countNames(deleted, uuid, name, null, systemStructure, deviceStructure, index, description, who);

            nameElements = NameElementUtil.getNameElements(names, holderStructures);
        }

        ResponsePageNameElements response = new ResponsePageNameElements(nameElements, totalCount, nameElements.size(), offset, limit);
        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_RESPONSE,
                        TextUtil.READ_NAMES,
                        response.toString()));
        return response;
    }

    public ResponsePageNameElements readNamesStructure(String uuid, Boolean deleted,
            FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {
        // validation outside method
        // read by structure uuid
        // return name elements

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "uuid", uuid));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "deleted", deleted));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "orderBy", orderBy));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "isAsc", isAsc));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "offset", offset));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_STRUCTURE, "limit", limit));
        }

        // there are different kinds of references to structure ids for names, directly and indirectly, both are included
        //     directly
        //         system group
        //         system
        //         subsystem
        //         device type
        //     indirectly
        //         system group through system
        //         system group through subsystem
        //         system       through system
        //         discipline   through device type
        //         device group through device type
        //     name does not refer directly to discipline, device group
        //     e.g. name Sys-Sub:Dis-Dev-001
        //        - name refers to Sub that in turn refers to Sys. Sub is directly referenced. Sys is indirectly referenced.
        //        - name refers to Dev that in turn refers to Dis. Dev is directly referenced. Dis is indirectly referenced.

        // go through all structures and see if / where uuid is found
        //     system structure and device structure
        //     uuid in 0 or 1 of kind of structure
        //     negligible impact on performance for going through different structures

        // order by
        //     order by may be not fully ordered when there are multiple read operations

        // pagination
        //     separate pagination when result may be from multiple read operations
        //     read operations should not contain pagination in such cases

        // one uuid at a time
        //     no need to make sure that names are not found and added twice or more to result
        //     otherwise ordering and pagination will be messy
        //     consider multiplicity if search for both system structure uuid and device structure uuid

        List<Name> names = Lists.newArrayList();
        // directly
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SYSTEMGROUP, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SYSTEM, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SUBSYSTEM, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_DEVICETYPE, uuid, deleted, orderBy, isAsc));

        // indirectly
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SYSTEMGROUP_THROUGH_SYSTEM, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SYSTEMGROUP_THROUGH_SUBSYSTEM, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_SYSTEM_THROUGH_SUBSYSTEM, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_DISCIPLINE_THROUGH_DEVICETYPE, uuid, deleted, orderBy, isAsc));
        names.addAll(holderRepositories.nameRepository().readNamesByStructure(NameRepository.NameByStructure.NAME_BY_DEVICEGROUP_THROUGH_DEVICETYPE, uuid, deleted, orderBy, isAsc));

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        // pagination
        Long totalCount = Long.valueOf(names.size());
        final List<NameElement> nameElements = paginate(NameElementUtil.getNameElements(names, holderStructures), offset, limit);
        ResponsePageNameElements response = new ResponsePageNameElements(nameElements, totalCount, nameElements.size(), offset, limit);

        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_RESPONSE,
                        TextUtil.READ_NAMES_STRUCTURE,
                        response.toString()));
        return response;
    }

    private List<NameElement> paginate(List<NameElement> list, Integer offset, Integer limit) {
        // intended as pagination when list may contain result from multiple read operations
        //     read operations should not contain pagination in such cases
        if (offset == null || limit == null) {
            return list;
        }

        List<NameElement> listPagination = Lists.newArrayList();
        int offsetItems = offset * limit;
        int availableItems = list.size() - offsetItems;
        if (availableItems > 0) {
            for (int i = offsetItems; i < Math.min(offsetItems+availableItems, offsetItems+limit); i++) {
                listPagination.add(list.get(i));
            }
        }
        return listPagination;
    }

    public ResponsePageNameElements readNamesHistory(String uuid, Integer offset, Integer limit) {
        // validation outside method
        // read history for name
        // return name elements for names

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_NAMES_HISTORY, "uuid", uuid));

        return readNames(null,
                uuid, null, null, null, null, null, null,
                Boolean.TRUE, FieldName.WHEN, Boolean.TRUE, offset, limit);
    }

    // ----------------------------------------------------------------------------------------------------

    public Boolean existsName(String name) {
        // validation outside method
        // read exists

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.EXISTS_NAME, "name", name));

        List<Name> names = holderRepositories.nameRepository().readNames(false,
                null, name, null, null, null, null, null, null);
        return !names.isEmpty();
    }

    public Boolean isValidToCreateName(String name) {
        // validation outside method
        // validate data - not exists

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.IS_VALID_TO_CREATE_NAME, "name", name));

        // holder of valid content for system and device structures
        //     note false to not include deleted entries
        HolderStructures holderStructures = new HolderStructures(holderIRepositories, false);

        ValidateNameElementUtil.validateNameDataCreate(name, namingConvention, holderRepositories, holderStructures);

        return Boolean.TRUE;
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateNamesCreate(NameElementCommand nameElement) {
        validateNamesCreate(nameElement, new HolderStructures(holderIRepositories));
    }
    public void validateNamesCreate(NameElementCommand nameElement, HolderStructures holderStructures) {
        // validate name element
        //     input vs given task
        //     data  vs given task

        ValidateNameElementUtil.validateNameElementInputCreate(nameElement);
        ValidateNameElementUtil.validateNameElementDataCreate(nameElement, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }
    public void validateNamesCreate(List<NameElementCommand> nameElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (NameElementCommand nameElement : nameElements) {
            validateNamesCreate(nameElement, holderStructures);
        }
    }

    public void validateNamesUpdate(NameElementCommand nameElement) {
        validateNamesUpdate(nameElement, new HolderStructures(holderIRepositories));
    }
    public void validateNamesUpdate(NameElementCommand nameElement, HolderStructures holderStructures) {
        // validate name element
        //     input vs given task
        //     data  vs given task

        ValidateNameElementUtil.validateNameElementInputUpdate(nameElement);
        ValidateNameElementUtil.validateNameElementDataUpdate(nameElement, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }
    public void validateNamesUpdate(List<NameElementCommand> nameElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (NameElementCommand nameElement : nameElements) {
            validateNamesUpdate(nameElement, holderStructures);
        }
    }

    public void validateNamesDelete(NameElementCommand nameElement) {
        validateNamesDelete(nameElement, new HolderStructures(holderIRepositories));
    }
    public void validateNamesDelete(NameElementCommand nameElement, HolderStructures holderStructures) {
        // validate name element
        //     input vs given task
        //     data  vs given task

        ValidateNameElementUtil.validateNameElementInputDelete(nameElement);
        ValidateNameElementUtil.validateNameElementDataDelete(nameElement, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }
    public void validateNamesDelete(List<NameElementCommand> nameElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (NameElementCommand nameElement : nameElements) {
            validateNamesDelete(nameElement, holderStructures);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<NameElement> updateNames(List<NameElementCommand> nameElementCommands, String username) {
        // validation outside method
        // transaction
        //     for each name element
        //         find & prepare
        //         update name
        //         create audit
        //         handle name element for updated name
        //     no notify
        //     return name elements for updated names

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        final List<NameElement> updatedNameElements = Lists.newArrayList();
        for (NameElementCommand nameElementCommand : nameElementCommands) {
            UUID uuid = nameElementCommand.getUuid();
            UUID parentSystemStructure = nameElementCommand.getParentSystemStructure();
            UUID parentDeviceStructure = nameElementCommand.getParentDeviceStructure();
            String index = nameElementCommand.getIndex();
            String description = nameElementCommand.getDescription();
            String comment = null;

            // find & prepare
            //     system structure - system group, system, subsystem - one of the three expected to be non-null, other two expected to be null
            //     device structure - device type - may be null
            SystemGroup systemGroup = holderIRepositories.systemGroupRepository().findByUuid(parentSystemStructure.toString());
            System system = holderIRepositories.systemRepository().findByUuid(parentSystemStructure.toString());
            Subsystem subsystem = holderIRepositories.subsystemRepository().findByUuid(parentSystemStructure.toString());
            DeviceType deviceType = null;
            if (parentDeviceStructure != null) {
                deviceType = holderIRepositories.deviceTypeRepository().findByUuid(parentDeviceStructure.toString());
            }

            String derivedName = null;
            if (systemGroup != null) {
                derivedName = NameUtil.getName(systemGroup, deviceType, index, holderStructures);
            } else if (system != null) {
                derivedName = NameUtil.getName(system, deviceType, index, holderStructures);
            } else if (subsystem != null) {
                derivedName = NameUtil.getName(subsystem, deviceType, index, holderStructures);
            }

            // update name
            // create audit
            Name name = holderIRepositories.nameRepository().findByUuid(uuid.toString());
            if (name == null) {
                continue;
            }
            // skip if name element has same content as name
            //     proceed without fail
            if (NameElementUtil.hasSameContent(nameElementCommand, name, holderIRepositories, holderStructures)) {
                continue;
            }
            name.setInstanceIndex(index);
            name.setConventionName(derivedName);
            name.setConventionNameEquivalence(namingConvention.equivalenceClassRepresentative(derivedName));
            name.setDescription(description);
            name.setAttributesRequested(when, username, comment);
            holderRepositories.nameRepository().updateName(name);
            holderRepositories.auditNameRepository().createAuditName(new AuditName(TextUtil.UPDATE, name));

            NameElement updatedNameElement = NameElementUtil.getNameElement(name, holderStructures);
            updatedNameElements.add(updatedNameElement);

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_NAME, TextUtil.ELEMENT_IN, nameElementCommand));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_NAME, TextUtil.ELEMENT_OUT, updatedNameElement));
            }
        }

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Update names",
                        nameElementCommands.size(),
                        updatedNameElements.size()));
        return updatedNameElements;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public List<NameElement> updateNames(Structure previousStructure, Structure structure, String username, HolderStructures holderStructures) {
        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     find out names referenced by structure - not for device group
        //     prepare name element commands
        //     update names
        //     return name elements for updated names

        List<NameElement> updatedNameElements = Lists.newArrayList();
        if (previousStructure != null
                && structure != null
                && !StringUtils.equals(structure.getMnemonic(), previousStructure.getMnemonic())
                && !StringUtils.isEmpty(structure.getMnemonic())) {
            List<Name> names = null;
            List<NameElementCommand> nameElements = Lists.newArrayList();
            UUID parentSystemStructureUuid = null;
            UUID parentDeviceStructureUuid = null;

            if (previousStructure instanceof SystemGroup && structure instanceof SystemGroup) {
                names = holderIRepositories.nameRepository().findNotDeletedBySystemGroupUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSystemGroupUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (previousStructure instanceof System && structure instanceof System) {
                names = holderIRepositories.nameRepository().findNotDeletedBySystemUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSystemUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }

                names = holderIRepositories.nameRepository().findNotDeletedBySystemUuidThroughSubsystem(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSubsystemUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (previousStructure instanceof Subsystem && structure instanceof Subsystem) {
                names = holderIRepositories.nameRepository().findNotDeletedBySubsystemUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSubsystemUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (previousStructure instanceof Discipline && structure instanceof Discipline) {
                names = holderIRepositories.nameRepository().findNotDeletedByDisciplineUuidThroughDeviceType(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getParentSystemStructureUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (previousStructure instanceof DeviceType && structure instanceof DeviceType) {
                names = holderIRepositories.nameRepository().findNotDeletedByDeviceTypeUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getParentSystemStructureUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            }

            // update names
            updatedNameElements = updateNames(nameElements, username);
        }
        return updatedNameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<NameElement> deleteNames(List<NameElementCommand> nameElementCommands, String username) {
        // validation outside method
        // transaction
        //     for each name element
        //         delete name (update)
        //         create audit
        //         handle name element for deleted name (updated)
        //     no notify
        //     return name elements for deleted names

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        final List<NameElement> deletedNameElements = Lists.newArrayList();
        for (NameElementCommand nameElementCommand : nameElementCommands) {
            UUID uuid = nameElementCommand.getUuid();
            String comment = null;

            // update
            // create audit
            Name name = holderIRepositories.nameRepository().findByUuid(uuid.toString());
            if (name == null) {
                continue;
            }
            name.setDeleted(Boolean.TRUE);
            name.setAttributesRequested(when, username, comment);
            holderRepositories.nameRepository().updateName(name);
            holderRepositories.auditNameRepository().createAuditName(new AuditName(TextUtil.DELETE, name));

            NameElement deletedNameElement = NameElementUtil.getNameElement(name, holderStructures);
            deletedNameElements.add(deletedNameElement);

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_NAME, TextUtil.ELEMENT_IN, nameElementCommand));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_NAME, TextUtil.ELEMENT_OUT, deletedNameElement));
            }
        }

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Delete names",
                        nameElementCommands.size(),
                        deletedNameElements.size()));
        return deletedNameElements;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public List<NameElement> deleteNames(Structure structure, String username, HolderStructures holderStructures) {
        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     find out names referenced by structure
        //     prepare name element commands
        //     delete names (update)
        //     no notify
        //     return name elements for deleted names (updated)

        List<NameElement> deletedNameElements = Lists.newArrayList();
        if (structure != null) {
            List<Name> names = null;
            List<NameElementCommand> nameElements = Lists.newArrayList();
            UUID parentSystemStructureUuid = null;
            UUID parentDeviceStructureUuid = null;

            if (structure instanceof SystemGroup) {
                names = holderIRepositories.nameRepository().findNotDeletedBySystemGroupUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSystemGroupUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (structure instanceof System) {
                names = holderIRepositories.nameRepository().findNotDeletedBySystemUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSystemUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (structure instanceof Subsystem) {
                names = holderIRepositories.nameRepository().findNotDeletedBySubsystemUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getSubsystemUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            } else if (structure instanceof DeviceType) {
                names = holderIRepositories.nameRepository().findNotDeletedByDeviceTypeUuid(structure.getUuid().toString());
                for (Name name : names) {
                    parentSystemStructureUuid = NameElementUtil.getParentSystemStructureUuid(name, holderStructures);
                    parentDeviceStructureUuid = NameElementUtil.getDeviceTypeUuid(name, holderStructures);
                    nameElements.add(
                            new NameElementCommand(
                                    name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                                    name.getInstanceIndex(), name.getDescription()));
                }
            }

            // delete names
            deletedNameElements = deleteNames(nameElements, username);
        }
        return deletedNameElements;
    }

}

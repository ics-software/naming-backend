/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.compress.utils.Lists;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.ValidateUtil;
import org.openepics.names.util.notification.NotificationName;
import org.openepics.names.util.notification.NotificationStructure;
import org.openepics.names.util.notification.NotificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 * This class provides notification services.
 *
 * @author Lars Johansson
 */
@Service
public class NotificationService {

    private static final Logger LOGGER = Logger.getLogger(NotificationService.class.getName());

    private static final String NOTIFY_NAMES_CREATED_UPDATED_DELETED =
            "Notify names, # created: {0}, # updated: {1}, # deleted: {2}";
    private static final String NOTIFY_STRUCTURES_CREATED_UPDATED_DELETED =
            "Notify structures, # created: {0}, # updated: {1}, # deleted: {2}";

    private static final String CHANGES_NOTIFICATION_NAMES      = "[Changes for names]";
    private static final String CHANGES_NOTIFICATION_STRUCTURES = "[Changes for structures]";

    private static final String ADD_FOOTER  = "addfooter";
    private static final String BACKEND_URL = "backendurl";

    @Value("${naming.mail.administrator}")
    List<String> namingMailAdministrator;
    @Value("${naming.mail.notification.names}")
    List<String> namingMailNotificationNames;
    @Value("${naming.mail.notification.structures}")
    List<String> namingMailNotificationStructures;
    @Value("${naming.swagger.url}")
    String namingSwaggerUrl;

    private final MailService mailService;

    @Autowired
    public NotificationService(
            MailService mailService) {
        this.mailService = mailService;
    }

    /**
     * Send notifications for names to administrators and affected users by email.
     *
     * @param notifications list of name notifications
     * @param nameCommand name command
     */
    public void notifyNames(List<NotificationName> notifications, NameCommand nameCommand) {
        if (NameCommand.CREATE.equals(nameCommand)) {
            notifyNames(notifications, null, null);
        } else if (NameCommand.UPDATE.equals(nameCommand)) {
            notifyNames(null, notifications, null);
        } else if (NameCommand.DELETE.equals(nameCommand)) {
            notifyNames(null, null, notifications);
        }
    }

    /**
     * Send notifications for names to administrators and affected users by email.
     *
     * @param created list of notifications for created names
     * @param updated list of notifications for updated names
     * @param deleted list of notifications for deleted names
     */
    public void notifyNames(
            List<NotificationName> created,
            List<NotificationName> updated,
            List<NotificationName> deleted) {

        NotificationUtil.sortByNewName(created);
        NotificationUtil.sortByNewName(updated);
        NotificationUtil.sortByNewName(deleted);

        int numberCreated = getListSize(created);
        int numberUpdated = getListSize(updated);
        int numberDeleted = getListSize(deleted);

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        NOTIFY_NAMES_CREATED_UPDATED_DELETED,
                        numberCreated,
                        numberUpdated,
                        numberDeleted));

        if (ValidateUtil.areAllZero(numberCreated, numberUpdated, numberDeleted)) {
            return;
        }

        // email content
        final Context ctx = new Context();
        ctx.setVariable("numberCreated", numberCreated);
        ctx.setVariable("numberUpdated", numberUpdated);
        ctx.setVariable("numberDeleted", numberDeleted);
        ctx.setVariable("created", created);
        ctx.setVariable("updated", updated);
        ctx.setVariable("deleted", deleted);
        ctx.setVariable(ADD_FOOTER, true);
        ctx.setVariable(BACKEND_URL, namingSwaggerUrl);
        TemplateEngine engine = generateTemplateEngine();

        // email addresses for notification
        //     administrators
        //     set of users - about changes for names
        List<String> toEmailAddresses = Lists.newArrayList();
        if (!namingMailAdministrator.isEmpty()) {
            toEmailAddresses.addAll(namingMailAdministrator);
        }
        if (!namingMailNotificationNames.isEmpty()) {
            toEmailAddresses.addAll(namingMailNotificationNames);
        }

        // send notification
        String[] ccEmailAddresses = null;
        String[] replyToEmailAddresses = null;
        String subject = CHANGES_NOTIFICATION_NAMES + " - " + namingSwaggerUrl;
        mailService.sendEmail(toEmailAddresses.toArray(new String[0]), ccEmailAddresses, replyToEmailAddresses,
                subject, engine.process("templates/notification_names.html", ctx), false, null, null);
    }

    /**
     * Send notifications for structures to administrators and affected users by email.
     *
     * @param notifications list of structure notifications
     * @param structureCommand structure command
     */
    public void notifyStructures(List<NotificationStructure> notifications, StructureCommand structureCommand) {
        if (StructureCommand.CREATE.equals(structureCommand)) {
            notifyStructures(notifications, null, null);
        } else if (StructureCommand.UPDATE.equals(structureCommand)) {
            notifyStructures(null, notifications, null);
        } else if (StructureCommand.DELETE.equals(structureCommand)) {
            notifyStructures(null, null, notifications);
        }
    }

    /**
     * Send notifications for structures to administrators and affected users by email.
     *
     * @param created list of notifications for created structures
     * @param updated list of notifications for updated structures
     * @param deleted list of notifications for deleted structures
     */
    public void notifyStructures(
            List<NotificationStructure> created,
            List<NotificationStructure> updated,
            List<NotificationStructure> deleted) {

        NotificationUtil.sortByNewMnemonicpath(created);
        NotificationUtil.sortByNewMnemonicpath(updated);
        NotificationUtil.sortByNewMnemonicpath(deleted);

        int numberCreated   = getListSize(created);
        int numberUpdated   = getListSize(updated);
        int numberDeleted   = getListSize(deleted);

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        NOTIFY_STRUCTURES_CREATED_UPDATED_DELETED,
                        numberCreated,
                        numberUpdated,
                        numberDeleted));

        if (ValidateUtil.areAllZero(numberCreated, numberUpdated, numberDeleted)) {
            return;
        }

        // email content
        final Context ctx = new Context();
        ctx.setVariable("numberCreated",   numberCreated);
        ctx.setVariable("numberUpdated",   numberUpdated);
        ctx.setVariable("numberDeleted",   numberDeleted);
        ctx.setVariable("created",   created);
        ctx.setVariable("updated",   updated);
        ctx.setVariable("deleted",   deleted);
        ctx.setVariable(ADD_FOOTER, true);
        ctx.setVariable(BACKEND_URL, namingSwaggerUrl);
        TemplateEngine engine = generateTemplateEngine();

        // email addresses for notification
        //     administrators
        //     set of users - about changes for structures
        List<String> toEmailAddresses = Lists.newArrayList();
        if (!namingMailAdministrator.isEmpty()) {
            toEmailAddresses.addAll(namingMailAdministrator);
        }
        if (!namingMailNotificationStructures.isEmpty()) {
            toEmailAddresses.addAll(namingMailNotificationStructures);
        }

        // send notification
        String[] ccEmailAddresses = null;
        String[] replyToEmailAddresses = null;
        String subject = CHANGES_NOTIFICATION_STRUCTURES + " - " + namingSwaggerUrl;
        mailService.sendEmail(toEmailAddresses.toArray(new String[0]), ccEmailAddresses, replyToEmailAddresses,
                subject, engine.process("templates/notification_structures.html", ctx), false, null, null);
    }

    /**
     * Return list size, zero if empty list.
     *
     * @param list list
     * @return list size
     */
    private int getListSize(List<?> list) {
        return list != null ? list.size() : 0;
    }

    /**
     * Generates the Thymeleaf template engine
     *
     * @return generate template engine
     */
    private TemplateEngine generateTemplateEngine() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCacheable(false);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());

        TemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(templateResolver);
        return engine;
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security.rbac;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RBACToken {

    private String id;

    @JsonProperty("username")
    private String userName;
    private String firstName;
    private String lastName;
    private Date creationTime;
    private Date expirationTime;
    private String ip;
    private List<String> roles;
    private String signature;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getCreationTime() {
        return RBACToken.cloneDate(creationTime);
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = RBACToken.cloneDate(creationTime);
    }

    public Date getExpirationTime() {
        return RBACToken.cloneDate(expirationTime);
    }

    public void setExpirationTime(Date expirationTime) {
        this.expirationTime = RBACToken.cloneDate(expirationTime);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * Clones a Date to make object immutable.
     *
     * @param dateToClone the date that has to be cloned.
     * @return <code>null</code>, if date was null <code>cloned date</code>, if date was not null
     */
    public static Date cloneDate(Date dateToClone) {
      return dateToClone == null ? null : new Date(dateToClone.getTime());
    }

}

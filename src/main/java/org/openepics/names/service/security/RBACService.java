/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security;

import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.security.AuthenticationException;
import org.openepics.names.exception.security.EntityNotFoundException;
import org.openepics.names.exception.security.ParseException;
import org.openepics.names.exception.security.RemoteException;
import org.openepics.names.exception.security.RemoteServiceException;
import org.openepics.names.exception.security.UnauthorizedException;
import org.openepics.names.service.LogService;
import org.openepics.names.service.security.rbac.RBACToken;
import org.openepics.names.service.security.rbac.RBACUserInfo;
import org.openepics.names.service.security.rbac.RBACUserRoles;
import org.openepics.names.service.security.util.HttpClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import okhttp3.Headers;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class RBACService {

    private static final Logger LOGGER = Logger.getLogger(RBACService.class.getName());

    private static final String RBAC_SERVER_BASEURL = "rbac.server.address";
    private static final String AUTH_PATH = "auth";
    private static final String TOKEN_PATH = AUTH_PATH + "/token";

    private static final String AUTHENTICATION_EXCEPTION = "Authentication Exception";

    private final Environment env;
    private final HttpClientService httpClientService;
    private final LogService logService;

    @Autowired
    public RBACService(Environment env, HttpClientService httpClientService, LogService logService) {
        this.env = env;
        this.httpClientService = httpClientService;
        this.logService = logService;
    }

    /**
     * Tries to log in user with specific parameters into RBAC, and if successful then give back
     * userInformation, and a token.
     *
     * @param userName the loginName for the user
     * @param password the password for the user
     * @return userInformation, and token for the successfully logged in user
     * @throws AuthenticationException if authentication fails
     */
    public RBACToken loginUser(String userName, String password) throws RemoteException, AuthenticationException {
        String secretHeader = Base64.getEncoder().encodeToString((userName + ":" + password).getBytes());
        Headers headers = new Headers.Builder().add("Authorization", "BASIC " + secretHeader).build();

        // trying to log in
        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executePostRequest(
                            headers,
                            env.getProperty(RBAC_SERVER_BASEURL) + TOKEN_PATH,
                            null,
                            HttpClientService.XML_MEDIA_TYPE,
                            RBACToken.class);

            // successful login
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (ParseException | RemoteServiceException e) {
            logService.logException(LOGGER, Level.WARNING, e, "Login error");
            throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying to log in to RBAC");
        }

        throw new AuthenticationException("Bad username/password");
    }

    /**
     * Gathers information about the logged in user.
     *
     * @param token the token for the logged in user.
     * @return information about the logged in user.
     * @throws UnauthorizedException if the token is not valid
     */
    public RBACToken userInfoFromToken(String token) throws UnauthorizedException, RemoteException {
        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executeGetRequest(
                            headers,
                            env.getProperty(RBAC_SERVER_BASEURL) + TOKEN_PATH + "/" + token,
                            HttpClientService.XML_MEDIA_TYPE,
                            RBACToken.class);

            // token check was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying to getting token info from RBAC");
        }

        throw new UnauthorizedException("Token error");
    }

    /**
     * Can be used to renew a valid client token in RBAC.
     *
     * @param token the token for the logged in user.
     * @return the renewed token, and user information.
     * @throws UnauthorizedException if token was not valid for renewing.
     */
    public RBACToken renewToken(String token) throws UnauthorizedException, RemoteException {
        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACToken> rbacTokenServiceResponse =
                    httpClientService.executePostRequest(
                            headers,
                            env.getProperty(RBAC_SERVER_BASEURL) + TOKEN_PATH + "/" + token + "/renew",
                            null,
                            HttpClientService.XML_MEDIA_TYPE,
                            RBACToken.class);

            // token renewal was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying to renew token in RBAC");
        }

        throw new UnauthorizedException("Token renewal error");
    }

    /**
     * Can be used to log out a user from RBAC (deletes the token).
     *
     * @param token the token for the logged in user.
     * @throws RemoteException if token was not valid, or already deleted
     */
    public void deleteToken(String token) throws RemoteException {
        Headers headers = new Headers.Builder().build();

        try {
            Integer respCode =
                    httpClientService.executeDeleteRequest(
                            headers, env.getProperty(RBAC_SERVER_BASEURL) + TOKEN_PATH + "/" + token);

            // deleting token was successful
            if (HttpClientService.isSuccessHttpStatusCode(respCode)) {
                return;
            }
            LOGGER.log(Level.WARNING, "Failed to delete RBAC token, response code: {}", respCode);
            throw new RemoteException(
                    "RBAC error", "Failed to delete RBAC token, response code: " + respCode);
        } catch (RemoteServiceException | ParseException e) {
            logService.logException(LOGGER, Level.WARNING, e, "Failed to delete RBAC token");
            throw new RemoteException("RBAC error", "Failed to delete RBAC token: " + e.getMessage());
        }
    }

    public RBACUserInfo fetchUsersByRole(String role) throws AuthenticationException, RemoteException {
        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACUserInfo> rbacTokenServiceResponse =
                    httpClientService.executeGetRequest(
                            headers,
                            env.getProperty(RBAC_SERVER_BASEURL) + AUTH_PATH + "/" + role + "/users",
                            HttpClientService.XML_MEDIA_TYPE,
                            RBACUserInfo.class);

            // fetching users was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }

            if (HttpStatus.NOT_FOUND.value() == rbacTokenServiceResponse.getStatusCode()) {
                throw new EntityNotFoundException("RBAC role", role);
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying get users from RBAC");
        }

        throw new UnauthorizedException("Getting user info error");
    }

    /**
     * Fetching roles from RBAC based on username.
     *
     * @param username The login name to fetch the user roles.
     * @throws RemoteException When error occurs when trying to fetch roles from RBAC
     * @return List of roles that is associated to the user in RBAC
     */
    public RBACUserRoles fetchRolesByUsername(String username) throws AuthenticationException, RemoteException {
        Headers headers = new Headers.Builder().build();

        try {
            HttpClientService.ServiceResponse<RBACUserRoles> rbacTokenServiceResponse =
                    httpClientService.executeGetRequest(
                            headers,
                            env.getProperty(RBAC_SERVER_BASEURL) + AUTH_PATH + "/" + username + "/role",
                            HttpClientService.XML_MEDIA_TYPE,
                            RBACUserRoles.class);

            // fetching users was successful
            if (HttpClientService.isSuccessHttpStatusCode(rbacTokenServiceResponse.getStatusCode())) {
                return rbacTokenServiceResponse.getEntity();
            }
        } catch (RemoteServiceException | ParseException e) {
            throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying get roles by username from RBAC");
        }

        throw new RemoteException(AUTHENTICATION_EXCEPTION, "Error while trying to fetch user-roles");
    }

}

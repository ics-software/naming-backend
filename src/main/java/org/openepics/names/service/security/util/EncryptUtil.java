/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Encryption utility.
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class EncryptUtil {

    private static final Logger LOGGER = Logger.getLogger(EncryptUtil.class.getName());

    @Value("${aes.key}")
    private String secret;

    private static SecretKeySpec secretKey;
    private static final String ALGORITHM = "AES";

    private void prepareSecretKey(String myKey) {
        MessageDigest sha = null;
        try {
            byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.WARNING, SecurityTextUtil.ALGORITHM_NOT_FOUND, e);
        }
    }

    /**
     * String encryption wrapper.
     *
     * @param strToEncrypt string to encrypt
     * @return encrypted string
     */
    public String encrypt(String strToEncrypt) {
        try {
            prepareSecretKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder()
                    .encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, SecurityTextUtil.ERROR_WHILE_ENCRYPTING, e);
        }
        return null;
    }

    /**
     * String decryption wrapper.
     *
     * @param strToDecrypt string to decrypt
     * @return decrypted string
     */
    public String decrypt(String strToDecrypt) {
        try {
            prepareSecretKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, SecurityTextUtil.ERROR_WHILE_DECRYPTING, e);
        }
        return null;
    }

}

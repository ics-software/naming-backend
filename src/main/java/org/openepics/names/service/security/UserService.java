/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.ServiceException;
import org.openepics.names.exception.security.AuthenticationException;
import org.openepics.names.exception.security.UnauthorizedException;
import org.openepics.names.service.LogService;
import org.openepics.names.service.security.dto.LoginTokenDto;
import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.rbac.RBACToken;
import org.openepics.names.service.security.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class UserService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    private final RBACService rbacService;
    private final LogService logService;

    @Autowired
    public UserService(RBACService rbacService, LogService logService) {
        this.rbacService = rbacService;
        this.logService = logService;
    }

    /**
     * Logs in user with username/password in RBAC, and if successful, generates JWT token
     *
     * @param userName The user's login name
     * @param password The user's password
     * @return LoginTokenDto with roles that can be user for generating JWT token
     * @throws AuthenticationException When user tries to log in with bad username/password
     * @throws ServiceException If _other_ error occurs during the authentication process
     */
    public LoginTokenDto loginUser(String userName, String password)
            throws ServiceException, AuthenticationException {

        try {
            RBACToken rbacToken = rbacService.loginUser(userName, password);

            LocalDateTime createTime = LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(), ZoneId.systemDefault());
            LocalDateTime expirationTime = LocalDateTime.ofInstant(rbacToken.getExpirationTime().toInstant(), ZoneId.systemDefault());

            Duration duration = Duration.between(createTime, expirationTime);
            long expireInMinutes = Math.abs(duration.toMinutes());

            return new LoginTokenDto(rbacToken.getId(), expireInMinutes, rbacToken.getRoles());
        } catch (AuthenticationException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Error while trying to log in user to RBAC");
            throw e;
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Error while trying to log in user to RBAC");
            throw e;
        }
    }

    /**
     * Extracting user-info from RBAC using the token they have
     *
     * @param token The user's login token
     * @return The user-information stored in RBAC
     */
    public UserDetails getUserInfoFromToken(String token) {
        return SecurityUtil.convertToUserDetails(rbacService.userInfoFromToken(token));
    }

    /**
     * Trying to renew user's token in RBAC.
     *
     * @param token The user's login token.
     * @return Information about the renewed token (including the new expiration date)
     * @throws ServiceException When unexpected error occurs during the renewal process
     * @throws UnauthorizedException When RBAC gives error during the token renewal process
     */
    public LoginTokenDto renewToken(String token) throws ServiceException, UnauthorizedException {
        RBACToken rbacToken = rbacService.renewToken(token);

        LocalDateTime createTime = LocalDateTime.ofInstant(rbacToken.getCreationTime().toInstant(), ZoneId.systemDefault());
        LocalDateTime expirationTime = LocalDateTime.ofInstant(rbacToken.getExpirationTime().toInstant(), ZoneId.systemDefault());

        Duration duration = Duration.between(createTime, expirationTime);
        long expireInMinutes = Math.abs(duration.toMinutes());

        return new LoginTokenDto(rbacToken.getId(), expireInMinutes, rbacToken.getRoles());
    }

    /**
     * Trying to log out user from RBAC. This will result in deleting token from the system.
     *
     * @param token The user's token that has to be deleted from RBAC
     * @throws ServiceException When error occurs during the token logout process
     */
    public void logoutUser(String token) throws ServiceException {
        rbacService.deleteToken(token);
    }

}

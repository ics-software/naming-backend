/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security.util;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Configuration
public class OkHttpConfiguration {

    public static final String ALLOW_UNTRUSTED_CERTS = "allow.untrusted.certs";

    private final Environment env;

    @Autowired
    public OkHttpConfiguration(Environment env) {
        this.env = env;
    }

    @Bean
    public OkHttpClient okHttpClient() {

        OkHttpClient.Builder clientBuilder =
                new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .connectionPool(pool());

        boolean allowUntrusted = BooleanUtils.toBoolean(env.getProperty(ALLOW_UNTRUSTED_CERTS));

        if (allowUntrusted) {
            final X509TrustManager trustManager =
                    new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkServerTrusted(final X509Certificate[] chain, final String authType) {}

                @Override
                public void checkClientTrusted(final X509Certificate[] chain, final String authType) {}
            };

            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, new TrustManager[] {trustManager}, new java.security.SecureRandom());
            } catch (NoSuchAlgorithmException | KeyManagementException e) {
                return null;
            }

            clientBuilder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);

            HostnameVerifier hostnameVerifier = (hostname, session) -> true;
            clientBuilder.hostnameVerifier(hostnameVerifier);
        }

        return clientBuilder.build();
    }

    @Bean
    public ConnectionPool pool() {
        return new ConnectionPool(40, 10, TimeUnit.SECONDS);
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security.util;

import java.io.IOException;
import java.lang.reflect.Type;

import org.openepics.names.exception.security.ParseException;
import org.openepics.names.exception.security.RemoteServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@Service
public class HttpClientService {

    public static final MediaType JSON_MEDIA_TYPE = MediaType.get("application/json; charset=utf-8");
    public static final MediaType XML_MEDIA_TYPE  = MediaType.get("application/xml; charset=utf-8");

    private static final String UNABLE_TO_CALL_SERVICE = "Unable to call service";

    private final OkHttpClient okHttpClient;

    @Autowired
    public HttpClientService(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public static class ServiceResponse<T> {
        private final T entity;
        private final int statusCode;
        private final String errorMessage;
        private final Headers headers;

        public ServiceResponse(T entity, int statusCode, Headers headers) {
            this.entity = entity;
            this.statusCode = statusCode;
            this.errorMessage = null;
            this.headers = headers;
        }

        public ServiceResponse(int statusCode, String errorMessage, Headers headers) {
            this.entity = null;
            this.statusCode = statusCode;
            this.errorMessage = errorMessage;
            this.headers = headers;
        }

        public T getEntity() {
            return entity;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public Headers getHeaders() {
            return headers;
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(
            Headers headers, String url, Class<T> responseClass) throws RemoteServiceException {
        return executeGetRequest(headers, url, responseClass, null);
    }

    public <T> ServiceResponse<T> executeGetRequest(
            Headers headers, String url, Class<T> responseClass, String formatDate)
                    throws RemoteServiceException {
        Request request;
        request = new Request.Builder().headers(headers).url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                Gson gson = new Gson();
                if (formatDate != null) {
                    gson = new GsonBuilder().setDateFormat(formatDate).create();
                }
                return new ServiceResponse<>(
                        gson.fromJson(response.body().string(), responseClass),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(
            Headers headers, String url, Type type, String formatDate) throws RemoteServiceException {
        Request request;
        request = new Request.Builder().headers(headers).url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(
                        new GsonBuilder()
                        .setDateFormat(formatDate)
                        .create()
                        .fromJson(response.body().string(), type),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(Headers headers, String url, Type type)
            throws RemoteServiceException {
        Request request;
        request = new Request.Builder().headers(headers).url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(
                        new GsonBuilder().create().fromJson(response.body().string(), type),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public <T> ServiceResponse<T> executeGetRequest(
            Headers headers, String url, MediaType mediaType, Class<T> responseClass)
                    throws RemoteServiceException, ParseException {

        if (JSON_MEDIA_TYPE.equals(mediaType)) {
            return executeGetRequest(headers, url, responseClass);
        }

        Request request;

        request = new Request.Builder().headers(headers).url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(
                        new XmlMapper().readValue(response.body().string(), responseClass),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with GET method", e);
        }
    }

    public ServiceResponse<String> executePlainGetRequest(Headers headers, String url)
            throws RemoteServiceException {
        Request request;
        request = new Request.Builder().headers(headers).url(url).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(response.body().string(), response.code(), response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public <T> ServiceResponse<T> executePostRequest(
            Headers headers, String url, Object requestBody, Class<T> responseClass)
                    throws RemoteServiceException {
        RequestBody body = RequestBody.create(new Gson().toJson(requestBody), JSON_MEDIA_TYPE);
        Request request;
        request = new Request.Builder().headers(headers).url(url).post(body).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(
                        new Gson().fromJson(response.body().string(), responseClass),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(
                    response.code(),
                    response.body() != null ? response.body().string() : null,
                            response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public ServiceResponse<String> executePlainPostRequest(
            Headers headers, String url, Object requestBody) throws RemoteServiceException {
        RequestBody body = RequestBody.create(new Gson().toJson(requestBody), JSON_MEDIA_TYPE);
        Request request;
        request = new Request.Builder().headers(headers).url(url).post(body).build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(response.body().string(), response.code(), response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException(UNABLE_TO_CALL_SERVICE, e);
        }
    }

    public <T> ServiceResponse<T> executePostRequest(
            Headers headers, String url, Object requestBody, MediaType mediaType, Class<T> responseClass)
                    throws RemoteServiceException, ParseException {
        if (JSON_MEDIA_TYPE.equals(mediaType)) {
            return executePostRequest(headers, url, requestBody, responseClass);
        }

        Request request;

        try {
            RequestBody body =
                    RequestBody.create(new XmlMapper().writeValueAsString(requestBody), XML_MEDIA_TYPE);

            request = new Request.Builder().headers(headers).url(url).post(body).build();
        } catch (JsonProcessingException e) {
            throw new ParseException("Unable to parse object");
        }

        try (Response response = okHttpClient.newCall(request).execute()) {
            if (isSuccessHttpStatusCode(response.code())) {
                return new ServiceResponse<>(
                        new XmlMapper().readValue(response.body().string(), responseClass),
                        response.code(),
                        response.headers());
            }
            return new ServiceResponse<>(null, response.code(), response.headers());
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with POST method", e);
        }
    }

    public Integer executeDeleteRequest(Headers headers, String url)
            throws RemoteServiceException, ParseException {

        Request request;

        request = new Request.Builder().headers(headers).url(url).delete().build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            return response.code();
        } catch (IOException e) {
            throw new RemoteServiceException("Unable to call service with POST method", e);
        }
    }

    public static boolean isSuccessHttpStatusCode(int code) {
        return code >= 200 && code < 300;
    }

}

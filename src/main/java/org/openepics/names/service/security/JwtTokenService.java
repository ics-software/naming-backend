/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security;

import io.fusionauth.jwt.Signer;
import io.fusionauth.jwt.Verifier;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.hmac.HMACSigner;
import io.fusionauth.jwt.hmac.HMACVerifier;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.openepics.names.service.security.dto.UserDetails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Service
public class JwtTokenService {

    private static final String TOKEN = "token";
    private static final String ROLES = "roles";

    @Value("${jwt.secret}")
    private String secret;

    /**
     * Retrieves userName from JWT token.
     *
     * @param token the JWT token.
     * @return the userName from the JWT token.
     */
    public String getUsernameFromToken(String token) {
        return decodeJWT(token).subject;
    }

    public String getRBACTokenFromToken(String token) {
        return decodeJWT(token).getAllClaims().get(TOKEN).toString();
    }

    // check if the token has expired
    private Boolean isTokenExpired(String token) {
        return decodeJWT(token).isExpired();
    }

    /**
     * Retrieving a field value from the JWT token.
     *
     * @param token The JWT token.
     * @return The decoded value of the JWT token.
     */
    public JWT decodeJWT(String token) {
        Verifier verifier = HMACVerifier.newVerifier(secret);
        return JWT.getDecoder().decode(token, verifier);
    }

    /**
     * Generates JWT token from userInformation, and expiration interval.
     *
     * @param userDetails Information about the user.
     * @param tokenExpiration JWT token expiration interval.
     * @return The generated JWT token.
     */
    // while creating the token -
    // 1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    // 2. Sign the JWT using the HS512 algorithm and secret key.
    // 3. According to JWS Compact
    // Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   compaction of the JWT to a URL-safe string
    public String generateToken(UserDetails userDetails, long tokenExpiration) {
        Date jwtTokenValidity = new Date(System.currentTimeMillis() + tokenExpiration * 60 * 1000);
        Signer signer = HMACSigner.newSHA512Signer(secret);
        JWT jwt =
                new JWT()
                .setIssuer("Naming-tool")
                .setIssuedAt(ZonedDateTime.now(ZoneOffset.UTC))
                .setSubject(userDetails.getUserName())
                .addClaim(TOKEN, userDetails.getToken())
                .addClaim(ROLES, userDetails.getRoles())
                .setExpiration(ZonedDateTime.ofInstant(jwtTokenValidity.toInstant(), ZoneId.systemDefault()));

        return JWT.getEncoder().encode(jwt, signer);
    }

    // validate token

    /**
     * Checks if the JWT token is valid.
     *
     * @param token The JWT token that has to be checked.
     * @param userDetails The users details.
     * @return <code>true</code> if the JWT token is valid <code>false</code> if the JWT token is not
     *     valid
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUserName()) && !isTokenExpired(token));
    }

}

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service.security.util;

/**
 * Utility class to assist in handling of text, in particular for security related matters.
 *
 * @author Lars Johansson
 */
public class SecurityTextUtil {

    // authentication & authorization

    public static final String AUTHENTICATION_HEADER_PREFIX    = "Bearer ";
    public static final String COOKIE_AUTH_HEADER              = "naming-auth";

    // encryption

    public static final String ALGORITHM_NOT_FOUND             = "Algorithm not found";
    public static final String ERROR_WHILE_DECRYPTING          = "Error while decrypting";
    public static final String ERROR_WHILE_ENCRYPTING          = "Error while encrypting";

    /**
     * This class is not to be instantiated.
     */
    private SecurityTextUtil() {
        throw new IllegalStateException("Utility class");
    }

}

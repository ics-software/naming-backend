package org.openepics.names.service.security.util;

import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.rbac.RBACToken;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 * @author Lars Johansson
 */
public class SecurityUtil {

    /**
     * This class is not to be instantiated.
     */
    private SecurityUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Converts RBAC token to user details DTO
     *
     * @param rbacInfo RBAC token descriptor
     * @return User details DTO
     */
    public static UserDetails convertToUserDetails(RBACToken rbacInfo) {
        UserDetails result = null;

        if (rbacInfo != null) {
            result = new UserDetails();

            result.setUserName(rbacInfo.getUserName());
            result.setFullName(rbacInfo.getFirstName() + " " + rbacInfo.getLastName());
            result.setToken(rbacInfo.getId());
            result.setRoles(rbacInfo.getRoles());
        }

        return result;
    }

    /**
     * Return username for a user.
     *
     * @param userDetails user details
     * @return username
     */
    public static String getUsername(UserDetails userDetails) {
        return userDetails != null ? userDetails.getUserName() : null;
    }

}

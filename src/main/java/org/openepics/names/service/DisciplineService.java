/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.notification.NotificationUtil;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides structures services for discipline.
 *
 * @author Lars Johansson
 */
@Service
public class DisciplineService {

    private static final Logger LOGGER = Logger.getLogger(DisciplineService.class.getName());

    private final NamesService namesService;
    private final DeviceGroupService deviceGroupService;
    private final IDeviceGroupRepository iDeviceGroupRepository;
    private final IDisciplineRepository iDisciplineRepository;
    private final IAuditStructureRepository iAuditStructureRepository;
    private final DisciplineRepository disciplineRepository;
    private final AuditStructureRepository auditStructureRepository;

    @Autowired
    public DisciplineService(
            NamesService namesService,
            DeviceGroupService deviceGroupService,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDisciplineRepository iDisciplineRepository,
            IAuditStructureRepository iAuditStructureRepository,
            DisciplineRepository disciplineRepository,
            AuditStructureRepository auditStructureRepository) {
        this.namesService = namesService;
        this.deviceGroupService = deviceGroupService;
        this.iDeviceGroupRepository = iDeviceGroupRepository;
        this.iDisciplineRepository = iDisciplineRepository;
        this.iAuditStructureRepository = iAuditStructureRepository;
        this.disciplineRepository = disciplineRepository;
        this.auditStructureRepository = auditStructureRepository;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification createStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     create structure
        //     create audit
        //     return
        //         structure element for created structure
        //         notification

        UUID uuid = UUID.randomUUID();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // create structure
        // create audit
        Discipline discipline = new Discipline(uuid,
                mnemonic, equivalenceClassRepresentative, structureElementCommand.getOrdering(),
                structureElementCommand.getDescription(), Status.APPROVED, Boolean.FALSE,
                when, username, null);
        disciplineRepository.createDiscipline(discipline);
        auditStructureRepository.createAuditStructure(new AuditStructure(TextUtil.CREATE, discipline));

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.CREATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(discipline, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DISCIPLINE, StructureCommand.CREATE, null, discipline, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification updateStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     update structure
        //     create audit
        //     previous for notification
        //     additional
        //         update related names
        //     return
        //         structure element for updated structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // update structure
        // create audit
        Discipline discipline = iDisciplineRepository.findByUuid(uuid);
        discipline.setMnemonic(mnemonic);
        discipline.setMnemonicEquivalence(equivalenceClassRepresentative);
        discipline.setOrdering(structureElementCommand.getOrdering());
        discipline.setDescription(structureElementCommand.getDescription());
        disciplineRepository.updateDiscipline(discipline);
        AuditStructure auditStructure = new AuditStructure(TextUtil.UPDATE, discipline);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.DISCIPLINE.name().toLowerCase(), uuid, auditStructure.getAuditId());
        Discipline previous = new Discipline(previousAuditStructure);

        // additional
        namesService.updateNames(previous, discipline, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.UPDATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(discipline, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DISCIPLINE, StructureCommand.UPDATE, previous, discipline, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification deleteStructure(StructureElementCommand structureElementCommand, Date when, String username,
            HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     delete structure (update)
        //     create audit
        //     previous for notification
        //     additional
        //         delete related names
        //         delete sub structures (and related names)
        //     return
        //         structure element for deleted structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();

        // delete structure
        // create audit
        Discipline discipline = iDisciplineRepository.findByUuid(uuid);
        discipline.setDeleted(Boolean.TRUE);
        discipline.setAttributesProcessed(when, username, null);
        disciplineRepository.updateDiscipline(discipline);
        AuditStructure auditStructure = new AuditStructure(TextUtil.DELETE, discipline);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.DISCIPLINE.name().toLowerCase(), uuid, auditStructure.getAuditId());
        Discipline previous = new Discipline(previousAuditStructure);

        // additional
        //     will propagate to sub structures
        namesService.deleteNames(discipline, username, holderStructures);

        List<StructureElementCommand> commands = Lists.newArrayList();
        List<DeviceGroup> deviceGroups = iDeviceGroupRepository.findNotDeletedByParent(discipline.getId());
        for (DeviceGroup deviceGroup : deviceGroups) {
            commands.add(new StructureElementCommand(deviceGroup.getUuid(), Type.DEVICEGROUP, null, null, null, null));
        }
        for (StructureElementCommand command : commands) {
            deviceGroupService.deleteStructure(command, when, username, holderStructures);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.COMMAND, StructureCommand.DELETE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(discipline, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DISCIPLINE, StructureCommand.DELETE, previous, discipline, holderStructures));
    }

}

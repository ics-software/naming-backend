/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.exception.ServiceException;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in handling of logs.
 *
 * @author Lars Johansson
 */
@Service
public class LogService {

    private static final String DELIMITER = " ### ";

    /**
     * Comma-separated list of strings that, if any of given strings is contained in stack trace element, the latter will be included in log
     * (ref. whitelist). Otherwise, stack trace element is eligible to exclusion.
     * If filter is empty, then stack trace element is included.
     */
    @Value("${naming.logging.filter.includes}")
    List<String> loggingFilterIncludes;
    /**
     * Comma-separated list of strings that, if any of given strings is contained in stack trace element, the latter will be excluded from log
     * (ref. blacklist), Otherwise, stack trace element is eligible to inclusion.
     * If filter is empty, then stack trace element is included.
     */
    @Value("${naming.logging.filter.excludes}")
    List<String> loggingFilterExcludes;
    /**
     * Maximum number of elements from stack trace to log.
     * Negative value will log entire stack trace with no concern to filters for inclusion or exclusion.
     */
    @Value("${naming.logging.stacktrace.length}")
    int loggingStackTraceLength;

    /**
     * Log service exception.
     *
     * @param logger logger
     * @param level log level
     * @param e service exception
     */
    public void logServiceException(Logger logger, Level level, ServiceException e) {
        logServiceException(logger, level, e, null);
    }
    /**
     * Log service exception.
     *
     * @param logger logger
     * @param level log level
     * @param e service exception
     * @param msg message (additional and optional)
     */
    public void logServiceException(Logger logger, Level level, ServiceException e, String msg) {
        if (ValidateUtil.isAnyNull(logger, level, e)) {
            return;
        }

        String msgException = !StringUtils.isEmpty(msg)
                ? DELIMITER + msg
                : "";
        String message = !StringUtils.isEmpty(e.getMessage())
                ? DELIMITER + e.getMessage()
                : "";
        String details = !StringUtils.isEmpty(e.getDetails())
                ? DELIMITER + e.getDetails()
                : "";
        String field = !StringUtils.isEmpty(e.getField())
                ? DELIMITER + e.getField()
                : "";
        msgException = msgException + message + details + field;

        logger.log(level, msgException);
        logStackTraceElements(logger, level, e);
    }

    /**
     * Log exception.
     *
     * @param logger logger
     * @param level log level
     * @param e exception
     */
    public void logException(Logger logger, Level level, Exception e) {
        logException(logger, level, e, null);
    }
    /**
     * Log exception.
     *
     * @param logger logger
     * @param level log level
     * @param e exception
     * @param msg message (additional and optional)
     */
    public void logException(Logger logger, Level level, Exception e, String msg) {
        if (ValidateUtil.isAnyNull(logger, level, e)) {
            return;
        }

        if (!StringUtils.isEmpty(msg) ) {
            logger.log(level, msg);
        }
        logStackTraceElements(logger, level, e);
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param e exception
     */
    private void logStackTraceElements(Logger logger, Level level, Exception e) {
        if (ValidateUtil.isAnyNull(logger, level, e)) {
            return;
        }

        if (loggingStackTraceLength < 0) {
            // log stack trace
            logger.log(level, e.getMessage(), e);
        } else {
            // log filtered stack trace
            logger.log(level, e::toString);
            logStackTraceElements(logger, level, e.getStackTrace());
        }
    }

    /**
     * Log stack trace elements.
     *
     * @param logger logger
     * @param level log level
     * @param stackTraceElements stack trace elements
     */
    private void logStackTraceElements(Logger logger, Level level, StackTraceElement[] stackTraceElements) {
        if (ValidateUtil.isAnyNull(logger, level, stackTraceElements)) {
            return;
        }

        String str = null;
        int count = 0;
        boolean emptyFilterInclude = loggingFilterIncludes.isEmpty();
        boolean emptyFilterExclude = loggingFilterExcludes.isEmpty();
        List<Pattern> patternsFilterInclude = compilePatterns(loggingFilterIncludes);
        List<Pattern> patternsFilterExclude = compilePatterns(loggingFilterExcludes);
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            str = stackTraceElement.toString();

            if  ((emptyFilterInclude || ValidateUtil.isAnyMatch(patternsFilterInclude, str))
                    && !(!emptyFilterExclude && ValidateUtil.isAnyMatch(patternsFilterExclude, str))) {
                count++;
                logger.log(level, str);
                if (count == loggingStackTraceLength) {
                    break;
                }
            }
        }
    }

    /**
     * Compiles the given regular expressions into patterns.
     * Note that null and empty values are disregarded.
     *
     * @param regex regular expressions
     * @return list of patterns
     */
    private List<Pattern> compilePatterns(List<String> regex) {
        List<Pattern> patterns = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(regex)) {
            for (String s : regex) {
                if (!StringUtils.isEmpty(s)) {
                    patterns.add(Pattern.compile(s));
                }
            }
        }
        return patterns;
    }

}

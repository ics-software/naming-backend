/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditNameRepository;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditNameRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DeviceGroupRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderRepositories;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.Utilities;
import org.openepics.names.util.ValidateStructureElementUtil;
import org.openepics.names.util.notification.NotificationStructure;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides structures services.
 *
 * @author Lars Johansson
 */
@Service
public class StructuresService {

    // note
    //     default
    //         active means valid not deleted
    //         obsolete values are not shown unless history is requested
    //     sorting
    //         sorting not applicable when multiple repositories are queried in same method
    //     structurecommand
    //         cud - create update delete

    private static final Logger LOGGER = Logger.getLogger(StructuresService.class.getName());

    protected static final String SYSTEM_STRUCTURE_ONLY                 = "System structure only";

    private final EssNamingConvention namingConvention;
    private final NotificationService notificationService;
    private final SystemGroupService systemGroupService;
    private final SystemService systemService;
    private final SubsystemService subsystemService;
    private final DisciplineService disciplineService;
    private final DeviceGroupService deviceGroupService;
    private final DeviceTypeService deviceTypeService;
    private final HolderIRepositories holderIRepositories;
    private final HolderRepositories holderRepositories;

    @Autowired
    public StructuresService(
            NotificationService notificationService,
            SystemGroupService systemGroupService,
            SystemService systemService,
            SubsystemService subsystemService,
            DisciplineService disciplineService,
            DeviceGroupService deviceGroupService,
            DeviceTypeService deviceTypeService,
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            IAuditNameRepository iAuditNameRepository,
            IAuditStructureRepository iAuditStructureRepository,
            NameRepository nameRepository,
            SystemGroupRepository systemGroupRepository,
            SystemRepository systemRepository,
            SubsystemRepository subsystemRepository,
            DisciplineRepository disciplineRepository,
            DeviceGroupRepository deviceGroupRepository,
            DeviceTypeRepository deviceTypeRepository,
            AuditNameRepository auditNameRepository,
            AuditStructureRepository auditStructureRepository) {

        this.namingConvention = new EssNamingConvention();
        this.notificationService = notificationService;
        this.systemGroupService = systemGroupService;
        this.systemService = systemService;
        this.subsystemService = subsystemService;
        this.disciplineService = disciplineService;
        this.deviceGroupService = deviceGroupService;
        this.deviceTypeService = deviceTypeService;
        this.holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository,
                iAuditNameRepository,
                iAuditStructureRepository);
        this.holderRepositories = new HolderRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository,
                auditNameRepository,
                auditStructureRepository);
    }

    @Transactional
    public List<StructureElement> createStructures(List<StructureElementCommand> structureElements, String username) {
        // validation outside method
        // transaction
        //     for each structure element command
        //         create structure
        //         handle
        //             notification
        //             structure element for created structure
        //     notify
        //     return structure elements for created structures

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        List<NotificationStructure> notifications = Lists.newArrayList();
        final List<StructureElement> createdStructureElements = Lists.newArrayList();
        for (StructureElementCommand structureElement : structureElements) {
            Type type = structureElement.getType();
            StructureElementNotification structureElementNotification = null;

            if (Type.SYSTEMGROUP.equals(type)) {
                structureElementNotification = systemGroupService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.SYSTEM.equals(type)) {
                structureElementNotification = systemService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.SUBSYSTEM.equals(type)) {
                structureElementNotification = subsystemService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DISCIPLINE.equals(type)) {
                structureElementNotification = disciplineService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DEVICEGROUP.equals(type)) {
                structureElementNotification = deviceGroupService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DEVICETYPE.equals(type)) {
                structureElementNotification = deviceTypeService.createStructure(structureElement, when, username, namingConvention, holderStructures);
            } else {
                continue;
            }

            Utilities.addToCollection(notifications, structureElementNotification.notificationStructure());
            Utilities.addToCollection(createdStructureElements, structureElementNotification.structureElement());

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, "type", type));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.ELEMENT_IN, structureElement));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.ELEMENT_OUT, structureElementNotification.structureElement()));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.NOTIFICATION, structureElementNotification.notificationStructure()));
            }
        }

        // notify
        notificationService.notifyStructures(notifications, StructureCommand.CREATE);

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Create structures",
                        structureElements.size(),
                        createdStructureElements.size()));
        return createdStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    public ResponsePageStructureElements readStructures(Type type, Boolean deleted,
            String uuid, String parent, String mnemonic, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        return readStructures(type, deleted,
                uuid, parent, mnemonic, mnemonicPath, description, who,
                Boolean.FALSE, orderBy, isAsc, offset, limit, StructureChoice.STRUCTURE);
    }

    public ResponsePageStructureElements readStructures(Type type, Boolean deleted,
            String uuid, String parent, String mnemonic, String mnemonicPath, String description, String who,
            Boolean includeHistory, FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit,
            StructureChoice structureChoice) {
        // validation outside method
        // read structures
        // return structure elements for structures

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "type", type));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "deleted", deleted));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "uuid", uuid));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "parent", parent));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "mnemonic", mnemonic));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "mnemonicPath", mnemonicPath));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "description", description));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "who", who));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "includeHistory", includeHistory));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "orderBy", orderBy));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "isAsc", isAsc));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "offset", offset));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "limit", limit));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES, "structureChoice", structureChoice));
        }

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        List<StructureElement> structureElements = Lists.newArrayList();

        // systemgroup and discipline do not have parent uuid
        // order by works if type is available
        // pagination after queries since type is not required

        if (StringUtils.isEmpty(parent) && (Type.SYSTEMGROUP.equals(type) || type == null)) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<SystemGroup> systemGroups = holderRepositories.systemGroupRepository().readSystemGroups(deleted, uuid, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForSystemGroups(systemGroups, holderStructures, structureChoice));
            }
        }

        if (Type.SYSTEM.equals(type) || type == null) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<System> systems = holderRepositories.systemRepository().readSystems(deleted, uuid, parent, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForSystems(systems, holderStructures, structureChoice));
            }
        }

        if (Type.SUBSYSTEM.equals(type) || type == null) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<Subsystem> subsystems = holderRepositories.subsystemRepository().readSubsystems(deleted, uuid, parent, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForSubsystems(subsystems, holderStructures, structureChoice));
            }
        }

        if (StringUtils.isEmpty(parent) && (Type.DISCIPLINE.equals(type) || type == null)) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<Discipline> disciplines = holderRepositories.disciplineRepository().readDisciplines(deleted, uuid, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForDisciplines(disciplines, holderStructures, structureChoice));
            }
        }

        if (Type.DEVICEGROUP.equals(type) || type == null) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<DeviceGroup> deviceGroups = holderRepositories.deviceGroupRepository().readDeviceGroups(deleted, uuid, parent, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForDeviceGroups(deviceGroups, holderStructures, structureChoice));
            }
        }

        if (Type.DEVICETYPE.equals(type) || type == null) {
            if (Boolean.TRUE.equals(includeHistory)) {
                List<AuditStructure> auditStructures = holderRepositories.auditStructureRepository().readAuditStructures(null, uuid, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForAuditStructures(auditStructures, holderStructures, structureChoice));
            } else {
                List<DeviceType> deviceTypes = holderRepositories.deviceTypeRepository().readDeviceTypes(deleted, uuid, parent, mnemonic, null, mnemonicPath, description, who, orderBy, isAsc, null, null);
                structureElements.addAll(StructureElementUtil.getStructureElementsForDeviceTypes(deviceTypes, holderStructures, structureChoice));
            }
        }

        // pagination for normal read and history
        Long totalCount = Long.valueOf(structureElements.size());
        structureElements = paginate(structureElements, offset, limit);

        ResponsePageStructureElements response = new ResponsePageStructureElements(structureElements, totalCount, structureElements.size(), offset, limit);
        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_RESPONSE,
                        TextUtil.READ_STRUCTURES,
                        response.toString()));
        return response;
    }

    private List<StructureElement> paginate(List<StructureElement> list, Integer offset, Integer limit) {
        // intended as pagination when list may contain result from multiple read operations
        //     read operations should not contain pagination in such cases
        if (offset == null || limit == null) {
            return list;
        }

        List<StructureElement> listPagination = Lists.newArrayList();
        int offsetItems = offset * limit;
        int availableItems = list.size() - offsetItems;
        if (availableItems > 0) {
            for (int i = offsetItems; i < Math.min(offsetItems+availableItems, offsetItems+limit); i++) {
                listPagination.add(list.get(i));
            }
        }
        return listPagination;
    }

    public ResponsePageStructureElements readStructuresChildren(String uuid,
            Boolean deleted,
            FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {
        // validation outside method
        // read structure by uuid for type
        // return structure elements for structures

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES_CHILDREN, "uuid", uuid));
        }

        final List<StructureElement> structureElements = Lists.newArrayList();

        // no children in structures for subsystem and device type
        // go through all structures and see if / where uuid is found
        //     uuid in 0 or 1 of kind of structure
        //     negligible impact on performance for going through different structures

        ResponsePageStructureElements responseSystemGroup = readStructures(Type.SYSTEM,      deleted, null, uuid, null, null, null, null, orderBy, isAsc, offset, limit);
        ResponsePageStructureElements responseSystem      = readStructures(Type.SUBSYSTEM,   deleted, null, uuid, null, null, null, null, orderBy, isAsc, offset, limit);
        ResponsePageStructureElements responseDiscipline  = readStructures(Type.DEVICEGROUP, deleted, null, uuid, null, null, null, null, orderBy, isAsc, offset, limit);
        ResponsePageStructureElements responseDeviceGroup = readStructures(Type.DEVICETYPE,  deleted, null, uuid, null, null, null, null, orderBy, isAsc, offset, limit);

        structureElements.addAll(responseSystemGroup.getList());
        structureElements.addAll(responseSystem.getList());
        structureElements.addAll(responseDiscipline.getList());
        structureElements.addAll(responseDeviceGroup.getList());

        // uuid is in max one kind of structure
        return new ResponsePageStructureElements(structureElements, Long.valueOf(structureElements.size()), structureElements.size(), offset, limit);
    }

    /**
     * Read history for structure by uuid.
     *
     * @param uuid uuid
     * @return list of structure elements
     */
    public ResponsePageStructureElements readStructuresHistory(String uuid, Integer offset, Integer limit) {
        // validation outside method
        // read history for structure
        // return structure elements for structures
        // note
        //     type may speed up read

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.READ_STRUCTURES_HISTORY, "uuid", uuid));
        }

        // mnemonic path does not make same sense for history
        // very (!) tricky to find mnemonic path for uuid at proper time (history)
        // therefore empty mnemonic path for history for structure

        // go through structures and see if / where uuid is found
        //     uuid in 0 or 1 kind of structures
        //     uuid is in max one kind of structure
        ResponsePageStructureElements response = readStructures(Type.SYSTEMGROUP, null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY);
        response = response.getTotalCount() == 0 ? readStructures(Type.SYSTEM,      null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY) : response;
        response = response.getTotalCount() == 0 ? readStructures(Type.SUBSYSTEM,   null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY) : response;
        response = response.getTotalCount() == 0 ? readStructures(Type.DISCIPLINE,  null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY) : response;
        response = response.getTotalCount() == 0 ? readStructures(Type.DEVICEGROUP, null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY) : response;
        response = response.getTotalCount() == 0 ? readStructures(Type.DEVICETYPE,  null, uuid, null, null, null, null, null, Boolean.TRUE, FieldStructure.WHEN, Boolean.TRUE, offset, limit, StructureChoice.HISTORY) : response;

        return response;
    }

    // ----------------------------------------------------------------------------------------------------

    public Boolean existsStructure(Type type, String mnemonicPath) {
        // validation outside method
        // read exists

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.EXISTS_STRUCTURE, "type", type));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.EXISTS_STRUCTURE, "mnemonicPath", mnemonicPath));
        }

        if (Type.SYSTEMGROUP.equals(type)) {
            List<SystemGroup> systemGroups = holderRepositories.systemGroupRepository().readSystemGroups(false, null, null, null, mnemonicPath, null, null);
            return !systemGroups.isEmpty();
        } else if (Type.SYSTEM.equals(type)) {
            List<System> systems = holderRepositories.systemRepository().readSystems(false, null, null, null, null, mnemonicPath, null, null);
            return !systems.isEmpty();
        } else if (Type.SUBSYSTEM.equals(type)) {
            List<Subsystem> subsystems = holderRepositories.subsystemRepository().readSubsystems(false, null, null, null, null, mnemonicPath, null, null);
            return !subsystems.isEmpty();
        } else if (Type.DISCIPLINE.equals(type)) {
            List<Discipline> disciplines = holderRepositories.disciplineRepository().readDisciplines(false, null, null, null, mnemonicPath, null, null);
            return !disciplines.isEmpty();
        } else if (Type.DEVICEGROUP.equals(type)) {
            return Boolean.FALSE;
        } else if (Type.DEVICETYPE.equals(type)) {
            List<DeviceType> deviceTypes = holderRepositories.deviceTypeRepository().readDeviceTypes(false, null, null, null, null, mnemonicPath, null, null);
            return !deviceTypes.isEmpty();
        }

        return Boolean.FALSE;
    }

    public Boolean isValidToCreateStructure(Type type, String mnemonicPath) {
        // validation outside method
        // validate data - not exists

        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.IS_VALID_TO_CREATE_STRUCTURE, "type", type));
            LOGGER.log(Level.FINE, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.IS_VALID_TO_CREATE_STRUCTURE, "mnemonicPath", mnemonicPath));
        }

        // holder of valid content for system and device structures
        //     note false to not include deleted entries
        HolderStructures holderStructures = new HolderStructures(holderIRepositories, false);

        ValidateStructureElementUtil.validateStructureDataCreate(type, mnemonicPath, namingConvention, holderIRepositories, holderStructures);

        return Boolean.TRUE;
    }

    // ----------------------------------------------------------------------------------------------------

    public void validateStructuresCreate(StructureElementCommand structureElement) {
        validateStructuresCreate(structureElement, new HolderStructures(holderIRepositories));
    }
    public void validateStructuresCreate(StructureElementCommand structureElement, HolderStructures holderStructures) {
        // validate structure element
        //     input vs given task
        //     data  vs given task

        ValidateStructureElementUtil.validateStructureElementInputCreate(structureElement, namingConvention);
        ValidateStructureElementUtil.validateStructureElementDataCreate(structureElement, namingConvention, holderRepositories, holderStructures);
    }
    public void validateStructuresCreate(List<StructureElementCommand> structureElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (StructureElementCommand structureElement : structureElements) {
            validateStructuresCreate(structureElement, holderStructures);
        }
    }

    public void validateStructuresUpdate(StructureElementCommand structureElement) {
        validateStructuresUpdate(structureElement, new HolderStructures(holderIRepositories));
    }
    public void validateStructuresUpdate(StructureElementCommand structureElement, HolderStructures holderStructures) {
        // validate structure element
        //     input vs given task
        //     data  vs given task

        ValidateStructureElementUtil.validateStructureElementInputUpdate(structureElement, namingConvention);
        ValidateStructureElementUtil.validateStructureElementDataUpdate(structureElement, namingConvention, holderRepositories, holderStructures);
    }
    public void validateStructuresUpdate(List<StructureElementCommand> structureElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (StructureElementCommand structureElement : structureElements) {
            validateStructuresUpdate(structureElement, holderStructures);
        }
    }

    public void validateStructuresDelete(StructureElementCommand structureElement) {
        validateStructuresDelete(structureElement, new HolderStructures(holderIRepositories));
    }
    public void validateStructuresDelete(StructureElementCommand structureElement, HolderStructures holderStructures) {
        // validate structure element
        //     input vs given task
        //     data  vs given task

        ValidateStructureElementUtil.validateStructureElementInputDelete(structureElement, namingConvention);
        ValidateStructureElementUtil.validateStructureElementDataDelete(structureElement, namingConvention, holderRepositories, holderStructures);
    }
    public void validateStructuresDelete(List<StructureElementCommand> structureElements) {
        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        for (StructureElementCommand structureElement : structureElements) {
            validateStructuresDelete(structureElement, holderStructures);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<StructureElement> updateStructures(List<StructureElementCommand> structureElements, String username) {
        // validation outside method
        // transaction
        //     for each structure element command
        //         update structure
        //         handle
        //             notification
        //             structure element for updated structure
        //     notify
        //     return structure elements for updated structures

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        List<NotificationStructure> notifications = Lists.newArrayList();
        final List<StructureElement> updatedStructureElements = Lists.newArrayList();
        for (StructureElementCommand structureElement : structureElements) {
            Type type = structureElement.getType();
            StructureElementNotification structureElementNotification = null;

            if (Type.SYSTEMGROUP.equals(type)) {
                structureElementNotification = systemGroupService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.SYSTEM.equals(type)) {
                structureElementNotification = systemService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.SUBSYSTEM.equals(type)) {
                structureElementNotification = subsystemService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DISCIPLINE.equals(type)) {
                structureElementNotification = disciplineService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DEVICEGROUP.equals(type)) {
                structureElementNotification = deviceGroupService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else if (Type.DEVICETYPE.equals(type)) {
                structureElementNotification = deviceTypeService.updateStructure(structureElement, when, username, namingConvention, holderStructures);
            } else {
                continue;
            }

            Utilities.addToCollection(notifications, structureElementNotification.notificationStructure());
            Utilities.addToCollection(updatedStructureElements, structureElementNotification.structureElement());

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, "type", type));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.ELEMENT_IN, structureElement));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.ELEMENT_OUT, structureElementNotification.structureElement()));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.NOTIFICATION, structureElementNotification.notificationStructure()));
            }
        }

        // notify
        notificationService.notifyStructures(notifications, StructureCommand.UPDATE);

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Update structures",
                        structureElements.size(),
                        updatedStructureElements.size()));
        return updatedStructureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    @Transactional
    public List<StructureElement> deleteStructures(List<StructureElementCommand> structureElements, String username) {
        // validation outside method
        // transaction
        //     for each structure element command
        //         delete structure
        //         handle
        //             notification
        //             structure element for deleted structure
        //     notify
        //     return structure elements for deleted structures

        // holder of valid content for system and device structures
        HolderStructures holderStructures = new HolderStructures(holderIRepositories);

        Date when = new Date();
        List<NotificationStructure> notifications = Lists.newArrayList();
        final List<StructureElement> deletedStructureElements = Lists.newArrayList();
        for (StructureElementCommand structureElement : structureElements) {
            Type type = structureElement.getType();
            StructureElementNotification structureElementNotification = null;

            if (Type.SYSTEMGROUP.equals(type)) {
                structureElementNotification = systemGroupService.deleteStructure(structureElement, when, username, holderStructures);
            } else if (Type.SYSTEM.equals(type)) {
                structureElementNotification = systemService.deleteStructure(structureElement, when, username, holderStructures);
            } else if (Type.SUBSYSTEM.equals(type)) {
                structureElementNotification = subsystemService.deleteStructure(structureElement, when, username, holderStructures);
            } else if (Type.DISCIPLINE.equals(type)) {
                structureElementNotification = disciplineService.deleteStructure(structureElement, when, username, holderStructures);
            } else if (Type.DEVICEGROUP.equals(type)) {
                structureElementNotification = deviceGroupService.deleteStructure(structureElement, when, username, holderStructures);
            } else if (Type.DEVICETYPE.equals(type)) {
                structureElementNotification = deviceTypeService.deleteStructure(structureElement, when, username, holderStructures);
            } else {
                continue;
            }

            Utilities.addToCollection(notifications, structureElementNotification.notificationStructure());
            Utilities.addToCollection(deletedStructureElements, structureElementNotification.structureElement());

            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, "type", type));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.ELEMENT_IN, structureElement));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.ELEMENT_OUT, structureElementNotification.structureElement()));
                LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.NOTIFICATION, structureElementNotification.notificationStructure()));
            }
        }

        // notify
        notificationService.notifyStructures(notifications, StructureCommand.DELETE);

        LOGGER.log(Level.INFO,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS_IN_OUT,
                        "Delete structures",
                        structureElements.size(),
                        deletedStructureElements.size()));
        return deletedStructureElements;
    }

}

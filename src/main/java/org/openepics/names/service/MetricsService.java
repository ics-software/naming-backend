/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import org.openepics.names.repository.AuditNameRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DeviceGroupRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.HolderRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * This class provides metrics services.
 *
 * @author Lars Johansson
 */
@Service
public class MetricsService {

    // note
    //     history   - count of history entries (all)
    //     active    - count of active entries
    //     deleted   - count of deleted entries

    private static final String[] METRICS_NAME      = { "ce.naming.name.count",      "Count of name entries"};
    private static final String[] METRICS_STRUCTURE = { "ce.naming.structure.count", "Count of structure entries"};

    private static final String   TYPE           = "type";
    private static final String   STATUS         = "status";
    private static final String[] STATUS_METRICS = {"history", "active", "deleted"};

    private final HolderRepositories holderRepositories;

    @Autowired
    public MetricsService(
            MeterRegistry meterRegistry,
            NameRepository nameRepository,
            SystemGroupRepository systemGroupRepository,
            SystemRepository systemRepository,
            SubsystemRepository subsystemRepository,
            DisciplineRepository disciplineRepository,
            DeviceGroupRepository deviceGroupRepository,
            DeviceTypeRepository deviceTypeRepository,
            AuditNameRepository auditNameRepository,
            AuditStructureRepository auditStructureRepository) {
        this.holderRepositories = new HolderRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository,
                auditNameRepository,
                auditStructureRepository);
        registerGaugeMetrics(meterRegistry);
    }

    private void registerGaugeMetrics(final MeterRegistry meterRegistry){
        // see also report

        // prepare metrics for names
        //     history
        //     active
        //     deleted

        Gauge.builder(METRICS_NAME[0],
                () -> holderRepositories.auditNameRepository().countAuditNames(null))
                .description(METRICS_NAME[1])
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_NAME[0],
                () -> holderRepositories.nameRepository().countNames(Boolean.FALSE, null, null, null, null, null, null, null, null))
                .description(METRICS_NAME[1])
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_NAME[0],
                () -> holderRepositories.nameRepository().countNames(Boolean.TRUE, null, null, null, null, null, null, null, null))
                .description(METRICS_NAME[1])
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        // prepare metrics for structures
        //     history
        //     active
        //     deleted

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.SYSTEMGROUP, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEMGROUP.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.systemGroupRepository().countSystemGroups(Boolean.FALSE, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEMGROUP.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.systemGroupRepository().countSystemGroups(Boolean.TRUE, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEMGROUP.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.SYSTEM, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.systemRepository().countSystems(Boolean.FALSE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.systemRepository().countSystems(Boolean.TRUE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.SUBSYSTEM, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SUBSYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.subsystemRepository().countSubsystems(Boolean.FALSE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SUBSYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.subsystemRepository().countSubsystems(Boolean.TRUE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.SUBSYSTEM.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.DISCIPLINE, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DISCIPLINE.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.disciplineRepository().countDisciplines(Boolean.FALSE, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DISCIPLINE.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.disciplineRepository().countDisciplines(Boolean.TRUE, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DISCIPLINE.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.DEVICEGROUP, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICEGROUP.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.deviceGroupRepository().countDeviceGroups(Boolean.FALSE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICEGROUP.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.deviceGroupRepository().countDeviceGroups(Boolean.TRUE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICEGROUP.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);

        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.auditStructureRepository().countAuditStructures(Type.DEVICETYPE, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICETYPE.toString())
                .tag(STATUS, STATUS_METRICS[0])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.deviceTypeRepository().countDeviceTypes(Boolean.FALSE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICETYPE.toString())
                .tag(STATUS, STATUS_METRICS[1])
                .register(meterRegistry);
        Gauge.builder(METRICS_STRUCTURE[0],
                () -> holderRepositories.deviceTypeRepository().countDeviceTypes(Boolean.TRUE, null, null, null, null, null, null, null))
                .description(METRICS_STRUCTURE[1])
                .tag(TYPE, Type.DEVICETYPE.toString())
                .tag(STATUS, STATUS_METRICS[2])
                .register(meterRegistry);
    }

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.StructureUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.notification.NotificationUtil;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class provides structures services for subsystem.
 *
 * @author Lars Johansson
 */
@Service
public class SubsystemService {

    private static final Logger LOGGER = Logger.getLogger(SubsystemService.class.getName());

    private final NamesService namesService;
    private final ISystemRepository iSystemRepository;
    private final ISubsystemRepository iSubsystemRepository;
    private final IAuditStructureRepository iAuditStructureRepository;
    private final SubsystemRepository subsystemRepository;
    private final AuditStructureRepository auditStructureRepository;

    @Autowired
    public SubsystemService(
            NamesService namesService,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IAuditStructureRepository iAuditStructureRepository,
            SubsystemRepository subsystemRepository,
            AuditStructureRepository auditStructureRepository) {
        this.namesService = namesService;
        this.iSystemRepository = iSystemRepository;
        this.iSubsystemRepository = iSubsystemRepository;
        this.iAuditStructureRepository = iAuditStructureRepository;
        this.subsystemRepository = subsystemRepository;
        this.auditStructureRepository = auditStructureRepository;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification createStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     create structure
        //     create audit
        //     additional
        //         automatically create name when system structure is created
        //         conditions on name and structure entry
        //             system structure should exist - mnemonic
        //             name should not exist         - system structure mnemonic path
        //     return
        //         structure element for created structure
        //         notification

        UUID uuid = UUID.randomUUID();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // create structure
        // create audit
        System system = iSystemRepository.findByUuid(structureElementCommand.getParent().toString());
        Subsystem subsystem = new Subsystem(uuid, system.getId(),
                mnemonic, equivalenceClassRepresentative, structureElementCommand.getOrdering(),
                structureElementCommand.getDescription(), Status.APPROVED, Boolean.FALSE,
                when, username, null);
        subsystemRepository.createSubsystem(subsystem);
        auditStructureRepository.createAuditStructure(new AuditStructure(TextUtil.CREATE, subsystem));

        // additional
        boolean hasMnemonic = !StringUtils.isEmpty(subsystem.getMnemonic());
        boolean existsName = hasMnemonic && namesService.existsName(StructureUtil.getMnemonicPath(subsystem, holderStructures));
        if (hasMnemonic && !existsName) {
            NameElementCommand nameElement = new NameElementCommand(null, subsystem.getUuid(), null, null, StructuresService.SYSTEM_STRUCTURE_ONLY);
            namesService.createName(nameElement, when, username, holderStructures, subsystem);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.CREATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(subsystem, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SUBSYSTEM, StructureCommand.CREATE, null, subsystem, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification updateStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     update structure
        //     create audit
        //     previous for notification
        //     additional - update related names
        //     return
        //         structure element for updated structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // update structure
        // create audit
        Subsystem subsystem = iSubsystemRepository.findByUuid(uuid);
        subsystem.setMnemonic(mnemonic);
        subsystem.setMnemonicEquivalence(equivalenceClassRepresentative);
        subsystem.setOrdering(structureElementCommand.getOrdering());
        subsystem.setDescription(structureElementCommand.getDescription());
        subsystemRepository.updateSubsystem(subsystem);
        AuditStructure auditStructure = new AuditStructure(TextUtil.UPDATE, subsystem);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SUBSYSTEM.name().toLowerCase(), uuid, auditStructure.getAuditId());
        Subsystem previous = new Subsystem(previousAuditStructure);

        // additional
        namesService.updateNames(previous, subsystem, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.UPDATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(subsystem, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SUBSYSTEM, StructureCommand.UPDATE, previous, subsystem, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification deleteStructure(StructureElementCommand structureElementCommand, Date when, String username,
            HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     delete structure (update)
        //     create audit
        //     previous for notification
        //     additional
        //         delete related names
        //     return
        //         structure element for deleted structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();

        // delete structure
        // create audit
        Subsystem subsystem = iSubsystemRepository.findByUuid(uuid);
        subsystem.setDeleted(Boolean.TRUE);
        subsystem.setAttributesProcessed(when, username, null);
        subsystemRepository.updateSubsystem(subsystem);
        AuditStructure auditStructure = new AuditStructure(TextUtil.DELETE, subsystem);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SUBSYSTEM.name().toLowerCase(), uuid, auditStructure.getAuditId());
        Subsystem previous = new Subsystem(previousAuditStructure);

        // additional
        namesService.deleteNames(subsystem, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.COMMAND, StructureCommand.DELETE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(subsystem, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SUBSYSTEM, StructureCommand.DELETE, previous, subsystem, holderStructures));
    }

}

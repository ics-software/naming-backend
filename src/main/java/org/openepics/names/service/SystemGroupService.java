/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.StructureUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.notification.NotificationUtil;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

/**
 * This class provides structures services for system group.
 *
 * @author Lars Johansson
 */
@Service
public class SystemGroupService {

    private static final Logger LOGGER = Logger.getLogger(SystemGroupService.class.getName());

    private final NamesService namesService;
    private final SystemService systemService;
    private final ISystemGroupRepository iSystemGroupRepository;
    private final ISystemRepository iSystemRepository;
    private final IAuditStructureRepository iAuditStructureRepository;
    private final SystemGroupRepository systemGroupRepository;
    private final AuditStructureRepository auditStructureRepository;

    @Autowired
    public SystemGroupService(
            NamesService namesService,
            SystemService systemService,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            IAuditStructureRepository iAuditStructureRepository,
            SystemGroupRepository systemGroupRepository,
            AuditStructureRepository auditStructureRepository) {
        this.namesService = namesService;
        this.systemService = systemService;
        this.iSystemGroupRepository = iSystemGroupRepository;
        this.iSystemRepository = iSystemRepository;
        this.iAuditStructureRepository = iAuditStructureRepository;
        this.systemGroupRepository = systemGroupRepository;
        this.auditStructureRepository = auditStructureRepository;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification createStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {
        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     create structure
        //     create audit
        //     additional
        //         automatically create name when system structure is created
        //         conditions on name and structure entry
        //             system structure should exist - mnemonic
        //             name should not exist         - system structure mnemonic path
        //     return
        //         structure element for created structure
        //         notification

        UUID uuid = UUID.randomUUID();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // create structure
        // create audit
        SystemGroup systemGroup = new SystemGroup(uuid,
                mnemonic, equivalenceClassRepresentative, structureElementCommand.getOrdering(),
                structureElementCommand.getDescription(), Status.APPROVED, Boolean.FALSE,
                when, username, null);
        systemGroupRepository.createSystemGroup(systemGroup);
        auditStructureRepository.createAuditStructure(new AuditStructure(TextUtil.CREATE, systemGroup));

        // additional
        boolean hasMnemonic = !StringUtils.isEmpty(systemGroup.getMnemonic());
        boolean existsName = hasMnemonic && namesService.existsName(StructureUtil.getMnemonicPath(systemGroup, holderStructures));
        if (hasMnemonic && !existsName) {
            NameElementCommand nameElement = new NameElementCommand(null, systemGroup.getUuid(), null, null, StructuresService.SYSTEM_STRUCTURE_ONLY);
            namesService.createName(nameElement, when, username, holderStructures, systemGroup);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.CREATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(systemGroup, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEMGROUP, StructureCommand.CREATE, null, systemGroup, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification updateStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     update structure
        //     create audit
        //     previous for notification
        //     additional
        //         update related names
        //     return
        //         structure element for updated structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // update structure
        // create audit
        SystemGroup systemGroup = iSystemGroupRepository.findByUuid(uuid);
        systemGroup.setMnemonic(mnemonic);
        systemGroup.setMnemonicEquivalence(equivalenceClassRepresentative);
        systemGroup.setOrdering(structureElementCommand.getOrdering());
        systemGroup.setDescription(structureElementCommand.getDescription());
        systemGroupRepository.updateSystemGroup(systemGroup);
        AuditStructure auditStructure = new AuditStructure(TextUtil.UPDATE, systemGroup);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SYSTEMGROUP.name().toLowerCase(), uuid, auditStructure.getAuditId());
        SystemGroup previous = new SystemGroup(previousAuditStructure);

        // additional
        namesService.updateNames(previous, systemGroup, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.UPDATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(systemGroup, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEMGROUP, StructureCommand.UPDATE, previous, systemGroup, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification deleteStructure(StructureElementCommand structureElementCommand, Date when, String username,
            HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     delete structure (update)
        //     create audit
        //     previous for notification
        //     additional
        //         delete related names
        //         delete sub structures (and related names)
        //     return
        //         structure element for deleted structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();

        // delete structure
        // create audit
        SystemGroup systemGroup = iSystemGroupRepository.findByUuid(uuid);
        systemGroup.setDeleted(Boolean.TRUE);
        systemGroup.setAttributesProcessed(when, username, null);
        systemGroupRepository.updateSystemGroup(systemGroup);
        AuditStructure auditStructure = new AuditStructure(TextUtil.DELETE, systemGroup);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.SYSTEMGROUP.name().toLowerCase(), uuid, auditStructure.getAuditId());
        SystemGroup previous = new SystemGroup(previousAuditStructure);

        // additional
        //     will propagate to sub structures
        namesService.deleteNames(systemGroup, username, holderStructures);

        List<StructureElementCommand> commands = Lists.newArrayList();
        List<System> systems = iSystemRepository.findNotDeletedByParent(systemGroup.getId());
        for (System system : systems) {
            commands.add(new StructureElementCommand(system.getUuid(), Type.SYSTEM, null, null, null, null));
        }
        for (StructureElementCommand command : commands) {
            systemService.deleteStructure(command, when, username, holderStructures);
        }

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.COMMAND, StructureCommand.DELETE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(systemGroup, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.SYSTEMGROUP, StructureCommand.DELETE, previous, systemGroup, holderStructures));
    }

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.util.EssNamingConvention;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.notification.NotificationUtil;
import org.openepics.names.util.notification.StructureElementNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class provides structures services for device type.
 *
 * @author Lars Johansson
 */
@Service
public class DeviceTypeService {

    private static final Logger LOGGER = Logger.getLogger(DeviceTypeService.class.getName());

    private final NamesService namesService;
    private final IDeviceGroupRepository iDeviceGroupRepository;
    private final IDeviceTypeRepository iDeviceTypeRepository;
    private final IAuditStructureRepository iAuditStructureRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final AuditStructureRepository auditStructureRepository;

    @Autowired
    public DeviceTypeService(
            NamesService namesService,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            IAuditStructureRepository iAuditStructureRepository,
            DeviceTypeRepository deviceTypeRepository,
            AuditStructureRepository auditStructureRepository) {
        this.namesService = namesService;
        this.iDeviceGroupRepository = iDeviceGroupRepository;
        this.iDeviceTypeRepository = iDeviceTypeRepository;
        this.iAuditStructureRepository = iAuditStructureRepository;
        this.deviceTypeRepository = deviceTypeRepository;
        this.auditStructureRepository = auditStructureRepository;
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification createStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     create structure
        //     create audit
        //     return
        //         structure element for created structure
        //         notification

        UUID uuid = UUID.randomUUID();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // create structure
        // create audit
        DeviceGroup deviceGroup = iDeviceGroupRepository.findByUuid(structureElementCommand.getParent().toString());
        DeviceType deviceType = new DeviceType(uuid, deviceGroup.getId(),
                mnemonic, equivalenceClassRepresentative, structureElementCommand.getOrdering(),
                structureElementCommand.getDescription(), Status.APPROVED, Boolean.FALSE,
                when, username, null);
        deviceTypeRepository.createDeviceType(deviceType);
        auditStructureRepository.createAuditStructure(new AuditStructure(TextUtil.CREATE, deviceType));

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.CREATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.CREATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(deviceType, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DEVICETYPE, StructureCommand.CREATE, null, deviceType, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification updateStructure(StructureElementCommand structureElementCommand, Date when, String username,
            EssNamingConvention namingConvention, HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     update structure
        //     create audit
        //     previous for notification
        //     additional
        //         update related names
        //     return
        //         structure element for updated structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();
        String mnemonic = structureElementCommand.getMnemonic();
        mnemonic = StringUtils.isEmpty(mnemonic) ? null : mnemonic;
        String equivalenceClassRepresentative = namingConvention.equivalenceClassRepresentative(mnemonic);

        // update structure
        // create audit
        DeviceType deviceType = iDeviceTypeRepository.findByUuid(uuid);
        deviceType.setMnemonic(mnemonic);
        deviceType.setMnemonicEquivalence(equivalenceClassRepresentative);
        deviceType.setOrdering(structureElementCommand.getOrdering());
        deviceType.setDescription(structureElementCommand.getDescription());
        deviceTypeRepository.updateDeviceType(deviceType);
        AuditStructure auditStructure = new AuditStructure(TextUtil.UPDATE, deviceType);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.DEVICETYPE.name().toLowerCase(), uuid, auditStructure.getAuditId());
        DeviceType previous = new DeviceType(previousAuditStructure);

        // additional
        namesService.updateNames(previous, deviceType, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.UPDATE_STRUCTURE, TextUtil.COMMAND, StructureCommand.UPDATE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(deviceType, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DEVICETYPE, StructureCommand.UPDATE, previous, deviceType, holderStructures));
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public StructureElementNotification deleteStructure(StructureElementCommand structureElementCommand, Date when, String username,
            HolderStructures holderStructures) {

        // validation outside method
        // transaction
        //     support a current transaction, throw an exception if none exists
        //     delete structure (update)
        //     create audit
        //     previous for notification
        //     additional
        //         delete related names
        //     return
        //         structure element for deleted structure
        //         notification

        String uuid = structureElementCommand.getUuid().toString();

        // delete structure
        // create audit
        DeviceType deviceType = iDeviceTypeRepository.findByUuid(uuid);
        deviceType.setDeleted(Boolean.TRUE);
        deviceType.setAttributesProcessed(when, username, null);
        deviceTypeRepository.updateDeviceType(deviceType);
        AuditStructure auditStructure = new AuditStructure(TextUtil.DELETE, deviceType);
        auditStructureRepository.createAuditStructure(auditStructure);

        // previous
        AuditStructure previousAuditStructure = iAuditStructureRepository.findPreviousByAuditTableAndUuidAndAuditId(Type.DEVICETYPE.name().toLowerCase(), uuid, auditStructure.getAuditId());
        DeviceType previous = new DeviceType(previousAuditStructure);

        // additional
        namesService.deleteNames(deviceType, username, holderStructures);

        LOGGER.log(Level.FINE, () -> MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.DELETE_STRUCTURE, TextUtil.COMMAND, StructureCommand.DELETE));

        return new StructureElementNotification(
                StructureElementUtil.getStructureElementProcessed(deviceType, holderStructures, StructureChoice.STRUCTURE),
                NotificationUtil.prepareNotification(Type.DEVICETYPE, StructureCommand.DELETE, previous, deviceType, holderStructures));
    }

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.service;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * This class provides mail services.
 *
 * @author Lars Johansson
 */
@Service
public class MailService {

    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    private static final String NAMING_PREFIX            = "[NT] ";
    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    private static final String TEXT_HTML_CHARSET_UTF_8  = "text/html; charset=utf-8";

    @Value("${naming.mail.notification}")
    boolean namingMailNotification;
    @Value("${naming.mail.from}")
    String namingMailFrom;

    private final JavaMailSender emailSender;
    private final LogService logService;

    @Autowired
    public MailService(
            JavaMailSender emailSender,
            LogService logService) {
        this.emailSender = emailSender;
        this.logService = logService;
    }

    /**
     * Send email.
     *
     * @param toEmailAddresses list of email addresses (to)
     * @param ccEmailAddresses list of email addresses (cc)
     * @param replyToEmailAddresses list of email addresses (reply-to)
     * @param subject subject
     * @param content content
     * @param withAttachment if attachment is available (one or more)
     * @param attachmentNames names of attachments
     * @param attachments attachments
     */
    public void sendEmail(String[] toEmailAddresses, String[] ccEmailAddresses, String[] replyToEmailAddresses,
            String subject, String content,
            boolean withAttachment, List<String> attachmentNames, List<InputStream> attachments) {
        try {
            // content
            //     check
            //         need to have
            //             to email address
            //             subject
            //             content
            //     handle
            //         to
            // 	       cc
            //         reply to
            //         from
            //         date
            //         subject
            //         content
            //         attachment
            //     send

            if (toEmailAddresses == null || toEmailAddresses.length == 0) {
                LOGGER.log(Level.WARNING, "To email addresses unavailable, can not send email");
                return;
            }
            if (StringUtils.isEmpty(subject)) {
                LOGGER.log(Level.WARNING, "Subject unavailable, can not send email");
                return;
            }
            if (StringUtils.isEmpty(content)) {
                LOGGER.log(Level.WARNING, "Content unavailable, can not send email");
                return;
            }

            MimeMessage message = emailSender.createMimeMessage();

            // to
            // cc
            // reply to
            final List<Address> toRecipients = new ArrayList<>();
            for (final String toEmailAddress : toEmailAddresses) {
                if (StringUtils.isEmpty(toEmailAddress)) {
                    LOGGER.log(Level.FINE, "To email address can not be used");
                    continue;
                }
                toRecipients.add(new InternetAddress(toEmailAddress));
            }
            if (toRecipients.isEmpty()) {
                LOGGER.log(Level.WARNING, "To email addresses unavailable, can not send email");
                return;
            }
            message.setRecipients(Message.RecipientType.TO, toRecipients.toArray(new Address[0]));
            final List<Address> ccRecipients = new ArrayList<>();
            if (ccEmailAddresses != null) {
                for (final String ccEmailAddress : ccEmailAddresses) {
                    if (StringUtils.isEmpty(ccEmailAddress)) {
                        LOGGER.log(Level.FINE, "Cc email address can not be used");
                        continue;
                    }
                    ccRecipients.add(new InternetAddress(ccEmailAddress));
                }
            }
            if (!ccRecipients.isEmpty()) {
                message.setRecipients(Message.RecipientType.CC, ccRecipients.toArray(new Address[0]));
            }
            final List<Address> replyToAddresses = new ArrayList<>();
            if (replyToEmailAddresses != null) {
                for (final String replyToEmailAddress : replyToEmailAddresses) {
                    if (StringUtils.isEmpty(replyToEmailAddress)) {
                        LOGGER.log(Level.FINE, "Reply to email address can not be used");
                        continue;
                    }
                    replyToAddresses.add(new InternetAddress(replyToEmailAddress));
                }
            }
            if (!replyToAddresses.isEmpty()) {
                message.setReplyTo(replyToAddresses.toArray(new Address[0]));
            }

            // from
            // date
            // subject
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress(namingMailFrom));
            message.setSubject(NAMING_PREFIX + subject);

            // attachment
            // content
            if (withAttachment
                    && attachmentNames != null && !attachmentNames.isEmpty()
                    && attachments != null && !attachments.isEmpty()
                    && attachmentNames.size() == attachments.size()) {
                MimeMultipart multipart = new MimeMultipart();
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(content, TEXT_HTML_CHARSET_UTF_8);
                multipart.addBodyPart(messagePart);
                for (int i = 0; i < attachments.size(); i++) {
                    MimeBodyPart attachmentPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(attachments.get(i), APPLICATION_OCTET_STREAM);
                    attachmentPart.setDataHandler(new DataHandler(dataSource));
                    attachmentPart.setFileName(attachmentNames.get(i));
                    multipart.addBodyPart(attachmentPart);
                }
                message.setContent(multipart);
            } else {
                message.setContent(content, TEXT_HTML_CHARSET_UTF_8);
            }

            if (namingMailNotification) {
                LOGGER.log(Level.FINE, () -> MessageFormat.format("Sending email with subject {0}", subject));
                emailSender.send(message);
            } else {
                LOGGER.log(Level.INFO, "Sending email disabled");
            }
        } catch (MessagingException | IOException e) {
            logService.logException(LOGGER, Level.WARNING, e);
        }
    }

}

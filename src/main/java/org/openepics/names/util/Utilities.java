/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Collection;

/**
 * Utility class to assist in handling of various things.
 *
 * @author Lars Johansson
 */
public class Utilities {

    /**
     * This class is not to be instantiated.
     */
    private Utilities() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return size of collection, <tt>0</tt> if collection is <tt>null</tt>.
     *
     * @param <T> type parameter
     * @param collection collection
     * @return size of collection
     */
    public static <T> int getSize(Collection<T> collection) {
        return collection != null
                ? collection.size()
                : 0;
    }

    /***
     * Return length of array, <tt>0</tt> if array is <tt>null</tt>.
     *
     * @param <T> type parameter
     * @param values values
     * @return length of array
     */
    @SafeVarargs
    public static <T> int getLength(T... values) {
        return values != null
                ? values.length
                : 0;
    }

    /**
     * Add value to collection and return if operation was successful or not.
     *
     * @param <T> type parameter
     * @param collection collection
     * @param value value
     * @return if operation was successful or not
     */
    public static <T> boolean addToCollection(Collection<T> collection, T value) {
        return collection != null && value != null && collection.add(value);
    }

}

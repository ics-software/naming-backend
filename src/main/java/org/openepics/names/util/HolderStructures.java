/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;

/**
 * Utility class and holder of system and device structure content that are used to populate REST API return elements.
 * <p>
 * Class is used for performance reasons to speed up preparation of what is to be returned.
 * </p>
 *
 * @author Lars Johansson
 */
public class HolderStructures {

    private HashMap<Long, SystemGroup> systemGroups;
    private HashMap<Long, System>      systems;
    private HashMap<Long, Subsystem>   subsystems;

    private HashMap<Long, Discipline>  disciplines;
    private HashMap<Long, DeviceGroup> deviceGroups;
    private HashMap<Long, DeviceType>  deviceTypes;

    private boolean includeDeleted = true;

    /**
     * Public constructor to prepare containers for system and device structure content.
     *
     * @param holderIRepositories
     */
    public HolderStructures (HolderIRepositories holderIRepositories) {
        this(holderIRepositories, true);
    }

    /**
     * Public constructor to prepare containers for system and device structure content.
     *
     * @param holderIRepositories holder of references to repositories
     * @param includeDeleted include deleted structure entries
     */
    public HolderStructures (HolderIRepositories holderIRepositories, boolean includeDeleted) {
        List<SystemGroup> systemGroupsRead = null;
        List<System> systemsRead = null;
        List<Subsystem> subsystemsRead = null;
        List<Discipline> disciplinesRead = null;
        List<DeviceGroup> deviceGroupsRead = null;
        List<DeviceType> deviceTypesRead = null;

        if (includeDeleted) {
            systemGroupsRead = holderIRepositories.systemGroupRepository().findAll();
            systemsRead = holderIRepositories.systemRepository().findAll();
            subsystemsRead = holderIRepositories.subsystemRepository().findAll();
            disciplinesRead = holderIRepositories.disciplineRepository().findAll();
            deviceGroupsRead = holderIRepositories.deviceGroupRepository().findAll();
            deviceTypesRead = holderIRepositories.deviceTypeRepository().findAll();
        } else {
            systemGroupsRead = holderIRepositories.systemGroupRepository().findNotDeleted();
            systemsRead = holderIRepositories.systemRepository().findNotDeleted();
            subsystemsRead = holderIRepositories.subsystemRepository().findNotDeleted();
            disciplinesRead = holderIRepositories.disciplineRepository().findNotDeleted();
            deviceGroupsRead = holderIRepositories.deviceGroupRepository().findNotDeleted();
            deviceTypesRead = holderIRepositories.deviceTypeRepository().findNotDeleted();
        }

        // initial capacity
        //     default load factor 0.75
        //     set at proper size to avoid rehashing
        //     size/0.75+1 (may be rounded downwards so possibly +2 instead)
        float loadFactor = 0.75f;
        this.systemGroups = new HashMap<>((int)(systemGroupsRead.size()/loadFactor + 2), loadFactor);
        this.systems      = new HashMap<>((int)(systemsRead.size()/loadFactor + 2), loadFactor);
        this.subsystems   = new HashMap<>((int)(subsystemsRead.size()/loadFactor + 2), loadFactor);
        this.disciplines  = new HashMap<>((int)(disciplinesRead.size()/loadFactor + 2), loadFactor);
        this.deviceGroups = new HashMap<>((int)(deviceGroupsRead.size()/loadFactor + 2), loadFactor);
        this.deviceTypes  = new HashMap<>((int)(deviceTypesRead.size()/loadFactor + 2), loadFactor);

        for (SystemGroup systemGroup : systemGroupsRead) {
            this.systemGroups.put(systemGroup.getId(), systemGroup);
        }
        for (System system : systemsRead) {
            this.systems.put(system.getId(), system);
        }
        for (Subsystem subsystem : subsystemsRead) {
            this.subsystems.put(subsystem.getId(), subsystem);
        }
        for (Discipline discipline : disciplinesRead) {
            this.disciplines.put(discipline.getId(), discipline);
        }
        for (DeviceGroup deviceGroup : deviceGroupsRead) {
            this.deviceGroups.put(deviceGroup.getId(), deviceGroup);
        }
        for (DeviceType deviceType : deviceTypesRead) {
            this.deviceTypes.put(deviceType.getId(), deviceType);
        }
    }

    /**
     * Find system group name part by uuid.
     *
     * @param id id
     * @return system group name part
     */
    public SystemGroup findSystemGroupById(Long id) {
        return systemGroups.get(id);
    }
    /**
     * Find system name part by uuid.
     *
     * @param id id
     * @return system name part
     */
    public System findSystemById(Long id) {
        return systems.get(id);
    }
    /**
     * Find subsystem name part by uuid.
     *
     * @param id id
     * @return subsystem name part
     */
    public Subsystem findSubsystemById(Long id) {
        return subsystems.get(id);
    }

    /**
     * Find discipline name part by uuid.
     *
     * @param id id
     * @return discipline name part
     */
    public Discipline findDisciplineById(Long id) {
        return disciplines.get(id);
    }
    /**
     * Find device group name part by uuid.
     *
     * @param id id
     * @return device group name part
     */
    public DeviceGroup findDeviceGroupById(Long id) {
        return deviceGroups.get(id);
    }
    /**
     * Find device type name part by uuid.
     *
     * @param id id
     * @return device type name part
     */
    public DeviceType findDeviceTypeById(Long id) {
        return deviceTypes.get(id);
    }

    /**
     * Return if include deleted structure entries.
     *
     * @return if include deleted structure entries
     */
    public boolean getIncludeDeleted() {
        return includeDeleted;
    }

    /**
     * Return map with key-value pairs (id and system group).
     *
     * @return map with key-value pairs (id and system group)
     */
    public Map<Long, SystemGroup> getIdSystemGroups() {
        return systemGroups;
    }

    /**
     * Return map with key-value pairs (id and system).
     *
     * @return map with key-value pairs (id and system)
     */
    public Map<Long, System> getIdSystems() {
        return systems;
    }

    /**
     * Return map with key-value pairs (id and subsystem).
     *
     * @return map with key-value pairs (id and subsystem)
     */
    public Map<Long, Subsystem> getIdSubsystems() {
        return subsystems;
    }

    /**
     * Return map with key-value pairs (id and discipline).
     *
     * @return map with key-value pairs (id and discipline)
     */
    public Map<Long, Discipline> getIdDisciplines() {
        return disciplines;
    }

    /**
     * Return map with key-value pairs (id and device group).
     *
     * @return map with key-value pairs (id and device group)
     */
    public Map<Long, DeviceGroup> getIdDeviceGroups() {
        return deviceGroups;
    }

    /**
     * Return map with key-value pairs (id and device type).
     *
     * @return map with key-value pairs (id and device type)
     */
    public Map<Long, DeviceType> getIdDeviceTypes() {
        return deviceTypes;
    }

}

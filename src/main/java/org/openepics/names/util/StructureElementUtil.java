/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.openepics.names.repository.model.AuditStructure;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in populating structure elements based on repository content.
 *
 * @author Lars Johansson
 */
public class StructureElementUtil {

    // note
    //     processed attributes for structures are used, not requested attributes
    //         requested attributes also used prior to removal of use of status attribute

    /**
     * This class is not to be instantiated.
     */
    private StructureElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return list of structure elements for audit structures.
     *
     * @param auditStructures audit structures
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForAuditStructures(List<AuditStructure> auditStructures, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (AuditStructure auditStructure : auditStructures) {
            structureElements.add(getStructureElementProcessed(auditStructure, holderStructures, structureChoice));
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for system groups.
     *
     * @param systemGroups system groups
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSystemGroups(List<SystemGroup> systemGroups, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (SystemGroup systemGroup : systemGroups) {
            structureElements.add(getStructureElementProcessed(systemGroup, holderStructures, structureChoice));
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for systems.
     *
     * @param systems systems
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSystems(List<System> systems, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (System system : systems) {
            structureElements.add(getStructureElementProcessed(system, holderStructures, structureChoice));
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for subsystems.
     *
     * @param subsystems subsystems
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForSubsystems(List<Subsystem> subsystems, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (Subsystem subsystem : subsystems) {
            structureElements.add(getStructureElementProcessed(subsystem, holderStructures, structureChoice));
        }
        return structureElements;
    }

    /**
     * Populate and return list of structure elements for disciplines.
     *
     * @param disciplines disciplines
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDisciplines(List<Discipline> disciplines, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (Discipline discipline : disciplines) {
            structureElements.add(getStructureElementProcessed(discipline, holderStructures, structureChoice));
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for device groups.
     *
     * @param deviceGroups device groups
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDeviceGroups(List<DeviceGroup> deviceGroups, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (DeviceGroup deviceGroup : deviceGroups) {
            structureElements.add(getStructureElementProcessed(deviceGroup, holderStructures, structureChoice));
        }
        return structureElements;
    }
    /**
     * Populate and return list of structure elements for device types.
     *
     * @param deviceTypes device types
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective.
     *                        Structure perspective will give mnemonic path.
     *                        History perspective may give empty mnemonic path as non-trivial to find out for history.
     *                        If choice not given then default as structure perspective (processed).
     * @return list of structure elements
     */
    public static List<StructureElement> getStructureElementsForDeviceTypes(List<DeviceType> deviceTypes, HolderStructures holderStructures, StructureChoice structureChoice) {
        List<StructureElement> structureElements = Lists.newArrayList();
        for (DeviceType deviceType : deviceTypes) {
            structureElements.add(getStructureElementProcessed(deviceType, holderStructures, structureChoice));
        }
        return structureElements;
    }

    /**
     * Populate and return structure element for audit structure with focus on processed.
     *
     * @param auditStructure audit structure
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(AuditStructure auditStructure, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (auditStructure == null) {
            return null;
        }

        // find out attributes as audit table is shared for system and device structure content
        //     mnemonic path ambiguous if considered in past
        Type type = null;
        UUID parent = null;
        String mnemonicPath = null;
        Integer level = null;

        if (TextUtil.SYSTEMGROUP.equals(auditStructure.getAuditTable())) {
            type = Type.SYSTEMGROUP;
            level = 1;
        } else if (TextUtil.SYSTEM.equals(auditStructure.getAuditTable())) {
            type = Type.SYSTEM;
            parent = holderStructures.findSystemGroupById(auditStructure.getParentId()).getUuid();
            level = 2;
        } else if (TextUtil.SUBSYSTEM.equals(auditStructure.getAuditTable())) {
            type = Type.SUBSYSTEM;
            parent = holderStructures.findSystemById(auditStructure.getParentId()).getUuid();
            level = 3;
        } else if (TextUtil.DISCIPLINE.equals(auditStructure.getAuditTable())) {
            type = Type.DISCIPLINE;
            level = 1;
        } else if (TextUtil.DEVICEGROUP.equals(auditStructure.getAuditTable())) {
            type = Type.DEVICEGROUP;
            parent = holderStructures.findDisciplineById(auditStructure.getParentId()).getUuid();
            level = 2;
        } else if (TextUtil.DEVICETYPE.equals(auditStructure.getAuditTable())) {
            type = Type.DEVICETYPE;
            parent = holderStructures.findDeviceGroupById(auditStructure.getParentId()).getUuid();
            level = 3;
        }

        return getStructureElement(
                auditStructure.getUuid(), type, parent,
                auditStructure.getMnemonic(), auditStructure.getOrdering(), auditStructure.getDescription(),
                mnemonicPath, level,
                auditStructure.getStatus(), auditStructure.isDeleted(),
                auditStructure.getProcessed(), auditStructure.getProcessedBy(), auditStructure.getProcessedComment());
    }
    /**
     * Populate and return structure element for system group with focus on processed.
     *
     * @param systemGroup system group
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(SystemGroup systemGroup, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (systemGroup == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(systemGroup.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(systemGroup, holderStructures);

        return getStructureElement(
                systemGroup.getUuid(), Type.SYSTEMGROUP, null,
                systemGroup.getMnemonic(), systemGroup.getOrdering(), systemGroup.getDescription(),
                mnemonicPath, 1,
                systemGroup.getStatus(), systemGroup.isDeleted(),
                systemGroup.getProcessed(), systemGroup.getProcessedBy(), systemGroup.getProcessedComment());
    }
    /**
     * Populate and return structure element for system with focus on processed.
     *
     * @param system system
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(System system, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (system == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(system.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(system, holderStructures);

        return getStructureElement(
                system.getUuid(), Type.SYSTEM,   holderStructures.findSystemGroupById(system.getParentId()).getUuid(),
                system.getMnemonic(), system.getOrdering(), system.getDescription(),
                mnemonicPath, 2,
                system.getStatus(), system.isDeleted(),
                system.getProcessed(), system.getProcessedBy(), system.getProcessedComment());
    }
    /**
     * Populate and return structure element for subsystem with focus on processed.
     *
     * @param subsystem subsystem
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(Subsystem subsystem, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (subsystem == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(subsystem.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(subsystem, holderStructures);

        return getStructureElement(
                subsystem.getUuid(), Type.SUBSYSTEM, holderStructures.findSystemById(subsystem.getParentId()).getUuid(),
                subsystem.getMnemonic(), subsystem.getOrdering(), subsystem.getDescription(),
                mnemonicPath, 3,
                subsystem.getStatus(), subsystem.isDeleted(),
                subsystem.getProcessed(), subsystem.getProcessedBy(), subsystem.getProcessedComment());
    }

    /**
     * Populate and return structure element for discipline with focus on processed.
     *
     * @param discipline discipline
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(Discipline discipline, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (discipline == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(discipline.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(discipline, holderStructures);

        return getStructureElement(
                discipline.getUuid(), Type.DISCIPLINE, null,
                discipline.getMnemonic(), discipline.getOrdering(), discipline.getDescription(),
                mnemonicPath, 1,
                discipline.getStatus(), discipline.isDeleted(),
                discipline.getProcessed(), discipline.getProcessedBy(), discipline.getProcessedComment());
    }
    /**
     * Populate and return structure element for device group with focus on processed.
     *
     * @param deviceGroup device group
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(DeviceGroup deviceGroup, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (deviceGroup == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(deviceGroup.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(deviceGroup, holderStructures);

        return getStructureElement(
                deviceGroup.getUuid(), Type.DEVICEGROUP, holderStructures.findDisciplineById(deviceGroup.getParentId()).getUuid(),
                deviceGroup.getMnemonic(), deviceGroup.getOrdering(), deviceGroup.getDescription(),
                mnemonicPath, 2,
                deviceGroup.getStatus(), deviceGroup.isDeleted(),
                deviceGroup.getProcessed(), deviceGroup.getProcessedBy(), deviceGroup.getProcessedComment());
    }
    /**
     * Populate and return structure element for device type with focus on processed.
     *
     * @param deviceType device type
     * @param holderStructures holder of system and device structure content
     * @param structureChoice whether to consider content from structure perspective or history perspective
     * @return structure element
     */
    public static StructureElement getStructureElementProcessed(DeviceType deviceType, HolderStructures holderStructures, StructureChoice structureChoice) {
        if (deviceType == null) {
            return null;
        }

        // mnemonic path
        //     ambiguous if considered in past (requested, processed)
        //     not for history of structure (obsolete)
        String mnemonicPath = StructureChoice.HISTORY.equals(structureChoice) || !Status.APPROVED.equals(deviceType.getStatus())
                ? null
                : StructureUtil.getMnemonicPath(deviceType, holderStructures);

        return getStructureElement(
                deviceType.getUuid(), Type.DEVICETYPE, holderStructures.findDeviceGroupById(deviceType.getParentId()).getUuid(),
                deviceType.getMnemonic(), deviceType.getOrdering(), deviceType.getDescription(),
                mnemonicPath, 3,
                deviceType.getStatus(), deviceType.isDeleted(),
                deviceType.getProcessed(), deviceType.getProcessedBy(), deviceType.getProcessedComment());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Populate and return structure element.
     *
     * @param uuid uuid
     * @param type type
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param ordering ordering
     * @param description description
     * @param mnemonicPath mnemonic path
     * @param level level
     * @param status status
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     * @return structure element
     */
    protected static StructureElement getStructureElement(
            UUID uuid, Type type, UUID parent,
            String mnemonic, Integer ordering, String description,
            String mnemonicPath, Integer level,
            Status status, Boolean deleted,
            Date when, String who, String comment) {

        return new StructureElement(
                uuid, type, parent,
                mnemonic, ordering, description,
                mnemonicPath, level,
                status, deleted,
                when, who, comment);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert structure element command for create to structure element command.
     *
     * @param command structure element command for create
     * @return structure element command
     */
    public static StructureElementCommand convertCommandCreate2Command(StructureElementCommandCreate command) {
        return new StructureElementCommand(
                null,
                command.getType(),
                command.getParent(),
                command.getMnemonic(),
                command.getOrdering(),
                command.getDescription());
    }

    /**
     * Convert list of structure element commands for create to list of structure element commands.
     *
     * @param commands list of structure element commands for create
     * @return list of structure element commands
     */
    public static List<StructureElementCommand> convertCommandCreate2Command(List<StructureElementCommandCreate> commands) {
        List<StructureElementCommand> structureElementCommands = Lists.newArrayList();
        for (StructureElementCommandCreate command : commands) {
            structureElementCommands.add(StructureElementUtil.convertCommandCreate2Command(command));
        }
        return structureElementCommands;
    }

    /**
     * Convert structure element command for update to structure element command.
     *
     * @param command structure element command for update
     * @return structure element command
     */
    public static StructureElementCommand convertCommandUpdate2Command(StructureElementCommandUpdate command) {
        return new StructureElementCommand(
                command.getUuid(),
                command.getType(),
                command.getParent(),
                command.getMnemonic(),
                command.getOrdering(),
                command.getDescription());
    }

    /**
     * Convert list of structure element commands for update to list of structure element commands.
     *
     * @param commands list of structure element commands for update
     * @return list of structure element commands
     */
    public static List<StructureElementCommand> convertCommandUpdate2Command(List<StructureElementCommandUpdate> commands) {
        List<StructureElementCommand> structureElementCommands = Lists.newArrayList();
        for (StructureElementCommandUpdate command : commands) {
            structureElementCommands.add(StructureElementUtil.convertCommandUpdate2Command(command));
        }
        return structureElementCommands;
    }

    /**
     * Convert structure element command for confirm to structure element command.
     *
     * @param command structure element command for confirm
     * @return structure element command
     */
    public static StructureElementCommand convertCommandConfirm2Command(StructureElementCommandConfirm command) {
        return new StructureElementCommand(
                command.getUuid(),
                command.getType(),
                null,
                null,
                null,
                null);
    }

    /**
     * Convert list of structure element commands for confirm to list of structure element commands.
     *
     * @param commands list of structure element commands for confirm
     * @return list of structure element commands
     */
    public static List<StructureElementCommand> convertCommandConfirm2Command(List<StructureElementCommandConfirm> commands) {
        List<StructureElementCommand> structureElementCommands = Lists.newArrayList();
        for (StructureElementCommandConfirm command : commands) {
            structureElementCommands.add(StructureElementUtil.convertCommandConfirm2Command(command));
        }
        return structureElementCommands;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert structure element command to structure element command for create.
     *
     * @param command structure element command
     * @return structure element command for create
     */
    public static StructureElementCommandCreate convertCommand2CommandCreate(StructureElementCommand command) {
        return new StructureElementCommandCreate(
                command.getType(),
                command.getParent(),
                command.getMnemonic(),
                command.getOrdering(),
                command.getDescription());
    }

    /**
     * Convert array of structure element commands to array of structure element commands for create.
     *
     * @param commands array of structure element commands
     * @return array of structure element commands for create
     */
    public static StructureElementCommandCreate[] convertCommand2CommandCreate(StructureElementCommand[] commands) {
        StructureElementCommandCreate[] structureElementCommands = new StructureElementCommandCreate[commands.length];
        for (int i=0; i<commands.length; i++) {
            structureElementCommands[i] = StructureElementUtil.convertCommand2CommandCreate(commands[i]);

        }
        return structureElementCommands;
    }

    /**
     * Convert structure element command to structure element command for update.
     *
     * @param command structure element command
     * @return structure element command for update
     */
    public static StructureElementCommandUpdate convertCommand2CommandUpdate(StructureElementCommand command) {
        return new StructureElementCommandUpdate(
                command.getUuid(),
                command.getType(),
                command.getParent(),
                command.getMnemonic(),
                command.getOrdering(),
                command.getDescription());
    }

    /**
     * Convert array of structure element commands to array of structure element commands for update.
     *
     * @param commands array of structure element commands
     * @return array of structure element commands for update
     */
    public static StructureElementCommandUpdate[] convertCommand2CommandUpdate(StructureElementCommand[] commands) {
        StructureElementCommandUpdate[] structureElementCommands = new StructureElementCommandUpdate[commands.length];
        for (int i=0; i<commands.length; i++) {
            structureElementCommands[i] = StructureElementUtil.convertCommand2CommandUpdate(commands[i]);

        }
        return structureElementCommands;
    }

    /**
     * Convert structure element command to structure element command for confirm.
     *
     * @param command structure element command
     * @return structure element command for confirm
     */
    public static StructureElementCommandConfirm convertCommand2CommandConfirm(StructureElementCommand command) {
        return new StructureElementCommandConfirm(
                command.getUuid(),
                command.getType());
    }

    /**
     * Convert array of structure element commands to array of structure element commands for confirm.
     *
     * @param commands array of structure element commands
     * @return array of structure element commands for confirm
     */
    public static StructureElementCommandConfirm[] convertCommand2CommandConfirm(StructureElementCommand[] commands) {
        StructureElementCommandConfirm[] structureElementCommands = new StructureElementCommandConfirm[commands.length];
        for (int i=0; i<commands.length; i++) {
            structureElementCommands[i] = StructureElementUtil.convertCommand2CommandConfirm(commands[i]);
        }
        return structureElementCommands;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert structure element to structure element command for create.
     *
     * @param element structure element
     * @return structure element command for create
     */
    public static StructureElementCommandCreate convertElement2CommandCreate(StructureElement element) {
        return new StructureElementCommandCreate(
                element.getType(),
                element.getParent(),
                element.getMnemonic(),
                element.getOrdering(),
                element.getDescription());
    }

    /**
     * Convert array of structure elements to array of structure element commands for create.
     *
     * @param elements array of structure elements
     * @return array of structure element commands for create
     */
    public static StructureElementCommandCreate[] convertElement2CommandCreate(StructureElement[] elements) {
        StructureElementCommandCreate[] structureElementCommands = new StructureElementCommandCreate[elements.length];
        for (int i=0; i<elements.length; i++) {
            structureElementCommands[i] = convertElement2CommandCreate(elements[i]);
        }
        return structureElementCommands;
    }

    /**
     * Convert structure element to structure element command for update.
     *
     * @param element structure element
     * @return structure element command for update
     */
    public static StructureElementCommandUpdate convertElement2CommandUpdate(StructureElement element) {
        return new StructureElementCommandUpdate(
                element.getUuid(),
                element.getType(),
                element.getParent(),
                element.getMnemonic(),
                element.getOrdering(),
                element.getDescription());
    }

    /**
     * Convert array of structure elements to array of structure element commands for update.
     *
     * @param elements array of structure elements
     * @return array of structure element commands for update
     */
    public static StructureElementCommandUpdate[] convertElement2CommandUpdate(StructureElement[] elements) {
        StructureElementCommandUpdate[] structureElementCommands = new StructureElementCommandUpdate[elements.length];
        for (int i=0; i<elements.length; i++) {
            structureElementCommands[i] =  convertElement2CommandUpdate(elements[i]);
        }
        return structureElementCommands;
    }

    /**
     * Convert structure element to structure element command for confirm.
     *
     * @param element structure element
     * @return structure element command for confirm
     */
    public static StructureElementCommandConfirm convertElement2CommandConfirm(StructureElement element) {
        return new StructureElementCommandConfirm(
                element.getUuid(),
                element.getType());
    }

    /**
     * Convert array of structure elements to array of structure element commands for confirm.
     *
     * @param elements array of structure elements
     * @return array of structure element commands for confirm
     */
    public static StructureElementCommandConfirm[] convertElement2CommandConfirm(StructureElement[] elements) {
        StructureElementCommandConfirm[] structureElementCommands = new StructureElementCommandConfirm[elements.length];
        for (int i=0; i<elements.length; i++) {
            structureElementCommands[i] = convertElement2CommandConfirm(elements[i]);
        }
        return structureElementCommands;
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

/**
 * Utility class to assist in handling of text.
 *
 * @author Lars Johansson
 */
public class TextUtil {

    // application

    public static final String NAMING                          = "Naming";

    // database
    //     table - see field
    //     operation

    public static final String CREATE = "create";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";

    // field
    //     name, structure
    //         name also field

    public static final String NAME                            = "name";
    public static final String STRUCTURE                       = "structure";
    public static final String SYSTEMGROUP                     = "systemgroup";
    public static final String SYSTEM                          = "system";
    public static final String SUBSYSTEM                       = "subsystem";
    public static final String DISCIPLINE                      = "discipline";
    public static final String DEVICEGROUP                     = "devicegroup";
    public static final String DEVICETYPE                      = "devicetype";

    public static final String DESCRIPTION                     = "description";
    public static final String INDEX                           = "index";
    public static final String MNEMONIC                        = "mnemonic";
    public static final String MNEMONICPATH                    = "mnemonicPath";
    public static final String PARENT                          = "parent";
    public static final String PARENTDEVICESTRUCTURE           = "parentDeviceStructure";
    public static final String PARENTSYSTEMSTRUCTURE           = "parentSystemStructure";
    public static final String TYPE                            = "type";
    public static final String UUID                            = "uuid";

    public static final String CONVENTION_NAME                 = "convention name";
    public static final String CONVENTION_NAME_EQUIVALENCE     = "convention name equivalence";
    public static final String MNEMONIC_PATH                   = "mnemonic path";
    public static final String MNEMONIC_PATH_EQUIVALENCE       = "mnemonic path equivalence";
    public static final String SYSTEM_STRUCTURE                = "system structure";
    public static final String DEVICE_STRUCTURE                = "device structure";

    // common

    public static final String SPACE                           = " ";

    // validation

    public static final String EXISTS                          = "exists";
    public static final String HAS_DUPLICATE                   = "has duplicate";
    public static final String IS_NOT_AVAILABLE                = "is not available";
    public static final String IS_NOT_CORRECT                  = "is not correct";
    public static final String IS_NOT_FOUND                    = "is not found";
    public static final String IS_NOT_VALID                    = "is not valid";

    public static final String DATA_IS_DELETED                 = "data is deleted";
    public static final String DATA_IS_NOT_FOUND               = "data is not found";
    public static final String DATA_IS_NOT_VALID               = "data is not valid";

    public static final String VALUE_IS_NOT_AVAILABLE          = "value is not available";
    public static final String VALUE_IS_NOT_CORRECT            = "value is not correct";
    public static final String VALUE_IS_NOT_EMPTY              = "value is not empty";
    public static final String VALUE_IS_NOT_VALID              = "value is not valid";

    // validation message

    public static final String CONVENTION_NAME_EXISTS                  = CONVENTION_NAME             + SPACE + EXISTS;
    public static final String CONVENTION_NAME_EQUIVALENCE_EXISTS      = CONVENTION_NAME_EQUIVALENCE + SPACE + EXISTS;
    public static final String CONVENTION_NAME_IS_NOT_CORRECT          = CONVENTION_NAME             + SPACE + IS_NOT_CORRECT;

    public static final String MNEMONIC_PATH_HAS_DUPLICATE             = MNEMONIC_PATH               + SPACE + HAS_DUPLICATE;
    public static final String MNEMONIC_PATH_IS_NOT_AVAILABLE          = MNEMONIC_PATH               + SPACE + IS_NOT_AVAILABLE;
    public static final String MNEMONIC_PATH_IS_NOT_CORRECT            = MNEMONIC_PATH               + SPACE + IS_NOT_CORRECT;
    public static final String MNEMONIC_PATH_IS_NOT_FOUND              = MNEMONIC_PATH               + SPACE + IS_NOT_FOUND;
    public static final String MNEMONIC_PATH_IS_NOT_VALID              = MNEMONIC_PATH               + SPACE + IS_NOT_VALID;
    public static final String MNEMONIC_PATH_EQUIVALENCE_HAS_DUPLICATE = MNEMONIC_PATH_EQUIVALENCE   + SPACE + HAS_DUPLICATE;
    public static final String STRUCTURE_MNEMONIC_PATH_IS_NOT_CORRECT  = STRUCTURE                   + SPACE + MNEMONIC_PATH_IS_NOT_CORRECT;

    public static final String DEVICETYPE_OR_INDEX_IS_NOT_AVAILABLE    = "device type or index is not available";
    public static final String ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT    = "one or more elements are not correct";
    public static final String OPERATION_COULD_NOT_BE_PERFORMED        = "operation could not be performed";

    public static final String PARENT_IS_NOT_AVAILABLE                 = PARENT           + SPACE + IS_NOT_AVAILABLE;

    public static final String PARENT_IS_NOT_CORRECT                   = PARENT           + SPACE + IS_NOT_CORRECT;
    public static final String SYSTEM_STRUCTURE_IS_NOT_CORRECT         = SYSTEM_STRUCTURE + SPACE + IS_NOT_CORRECT;
    public static final String SYSTEMGROUP_IS_NOT_CORRECT              = SYSTEMGROUP      + SPACE + IS_NOT_CORRECT;
    public static final String SYSTEM_IS_NOT_CORRECT                   = SYSTEM           + SPACE + IS_NOT_CORRECT;
    public static final String SUBSYSTEM_IS_NOT_CORRECT                = SUBSYSTEM        + SPACE + IS_NOT_CORRECT;
    public static final String DISCIPLINE_IS_NOT_CORRECT               = DISCIPLINE       + SPACE + IS_NOT_CORRECT;
    public static final String DEVICEGROUP_IS_NOT_CORRECT              = DEVICEGROUP      + SPACE + IS_NOT_CORRECT;
    public static final String DEVICETYPE_IS_NOT_CORRECT               = DEVICETYPE       + SPACE + IS_NOT_CORRECT;

    public static final String SYSTEM_STRUCTURE_IS_NOT_FOUND           = SYSTEM_STRUCTURE + SPACE + IS_NOT_FOUND;
    public static final String DEVICE_STRUCTURE_IS_NOT_FOUND           = DEVICE_STRUCTURE + SPACE + IS_NOT_FOUND;
    public static final String DISCIPLINE_IS_NOT_FOUND                 = DISCIPLINE       + SPACE + IS_NOT_FOUND;

    public static final String DEVICEGROUP_IS_NOT_VALID                = DEVICEGROUP      + SPACE + IS_NOT_VALID;
    public static final String INDEX_IS_NOT_VALID                      = INDEX            + SPACE + IS_NOT_VALID;
    public static final String MNEMONIC_IS_NOT_VALID                   = MNEMONIC         + SPACE + IS_NOT_VALID;

    public static final String FILE_COULD_NOT_BE_PARSED                = "File could not be parsed";
    public static final String JSON_COULD_NOT_BE_PARSED                = "Json could not be parsed";

    public static final String ATTACHMENT_FILENAME_NAME_ELEMENT_XLSX      = "attachment; filename=NameElement.xlsx";
    public static final String ATTACHMENT_FILENAME_STRUCTURE_ELEMENT_XLSX = "attachment; filename=StructureElement.xlsx";

    // log
    public static final String DESCRIPTION_NUMBER_ELEMENTS        = "{0}, # elements: {1}";
    public static final String DESCRIPTION_NUMBER_ELEMENTS_IN_OUT = "{0}, # elements (in): {1}, # elements (out): {2}";
    public static final String DESCRIPTION_NAME_VALUE             = "{0}, {1}: {2}";
    public static final String DESCRIPTION_RESPONSE               = "{0}, response: {1}";
    public static final String COMMAND                            = "command";
    public static final String ELEMENT_IN                         = "element (in)";
    public static final String ELEMENT_OUT                        = "element (out)";
    public static final String NOTIFICATION                       = "notification";
    public static final String PREPARE_NOTIFICATION               = "Prepare notification";

    public static final String CREATE_NAME                        = "Create name";
    public static final String UPDATE_NAME                        = "Update name";
    public static final String DELETE_NAME                        = "Delete name";
    public static final String READ_NAMES                         = "Read names";
    public static final String READ_NAMES_SYSTEM_STRUCTURE        = "Read names system structure";
    public static final String READ_NAMES_DEVICE_STRUCTURE        = "Read names device structure";
    public static final String READ_NAMES_STRUCTURE               = "Read names structure";
    public static final String READ_NAMES_HISTORY                 = "Read names history";
    public static final String EXISTS_NAME                        = "Exists name";
    public static final String IS_VALID_TO_CREATE_NAME            = "Is valid to create name";

    public static final String CREATE_STRUCTURE                   = "Create structure";
    public static final String UPDATE_STRUCTURE                   = "Update structure";
    public static final String DELETE_STRUCTURE                   = "Delete structure";
    public static final String READ_STRUCTURES                    = "Read structures";
    public static final String READ_STRUCTURES_HISTORY            = "Read structures history";
    public static final String READ_STRUCTURES_CHILDREN           = "Read structures children";
    public static final String READ_STRUCTURES_MNEMONIC           = "Read structures mnemonic";
    public static final String READ_STRUCTURES_MNEMONIC_PATH      = "Read structures mnemonic path";
    public static final String EXISTS_STRUCTURE                   = "Exists structure";
    public static final String IS_VALID_TO_CREATE_STRUCTURE       = "Is valid to create structure";

    // process variable name property validation
    //     . character to be present
    public static final String PV_NAME_VALIDATION_CONVENTION_NAME_IS_NOT_REGISTERED_IN_NAMING =
            "The convention name is not registered in Naming.";
    public static final String PV_NAME_VALIDATION_NAME_IS_EMPTY =
            "The process variable name is empty.";
    public static final String PV_NAME_VALIDATION_NAME_CONTAINS_MORE_THAN_60_CHARACTERS =
            "The process variable name contains more than 60 characters.";
    public static final String PV_NAME_VALIDATION_PROPERTY_IS_EMPTY =
            "The process variable property is empty.";
    public static final String PV_NAME_VALIDATION_PROPERTY_CONTAINS_MORE_THAN_25_CHARACTERS =
            "The process variable property contains more than 25 characters.";
    public static final String PV_NAME_VALIDATION_PROPERTY_CONTAINS_MORE_THAN_20_CHARACTERS =
            "The process variable property contains more than 20 characters.";
    public static final String PV_NAME_VALIDATION_PROPERTY_CONTAINS_LESS_THAN_4_CHARACTERS =
            "The process variable property contains less than 4 characters.";
    public static final String PV_NAME_VALIDATION_PROPERTY_CONTAINS_CHARACTER_THAT_IS_NOT_ALLOWED =
            "The process variable property contains a character that is not allowed.";
    public static final String PV_NAME_VALIDATION_PROPERTY_IS_INTERNAL =
            "The process variable property is internal.";
    public static final String PV_NAME_VALIDATION_PROPERTY_CONTAINS_CHARACTER_IN_A_POSITION_THAT_IS_NOT_ALLOWED =
            "The process variable property contains # character in a position that is not allowed.";
    public static final String PV_NAME_VALIDATION_PROPERTY_DOES_NOT_START_WITH_WITH_A_LETTER =
            "The process variable property does not start with with a letter.";
    public static final String PV_NAME_VALIDATION_PROPERTY_DOES_NOT_START_WITH_WITH_AN_UPPER_CASE_LETTER =
            "The process variable property does not start with with an upper case letter.";

    /**
     * This class is not to be instantiated.
     */
    private TextUtil() {
        throw new IllegalStateException("Utility class");
    }

}

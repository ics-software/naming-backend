/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.repository.IAuditNameRepository;
import org.openepics.names.repository.model.AuditName;
import org.openepics.names.service.NotificationService;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

/**
 * Scheduler class to notify users and administrators about changes for names and structures in Naming.
 *
 * @author Lars Johansson
 */
@Component
@EnableScheduling
public class NotificationScheduler {

    private static final Logger LOGGER = Logger.getLogger(NotificationScheduler.class.getName());

    private static final String NOTIFY_NAMES_FROM_TO_CREATED_UPDATED_DELETED = "Notify names, {0} - {1}, # created: {2}, # updated: {3}, # deleted: {4}";

    private final NotificationService notificationService;
    private final IAuditNameRepository iAuditNameRepository;

    @Autowired
    public NotificationScheduler(
            NotificationService notificationService,
            IAuditNameRepository iAuditNameRepository) {
        this.notificationService = notificationService;
        this.iAuditNameRepository = iAuditNameRepository;
    }

    /**
     * Notify administrators and affected users about changes for names and structures.
     * Intention is to inform about changes for names (create, update, delete).
     * It is to be checked once a day, and notifications to be generated with emails.
     *
     * <br/><br/>
     * Scheduling is handled with cron expression.
     */
    @Scheduled(cron = "${naming.notify.schedule}")
    public void notifyForNames() {
        prepareAndNotifyForNames();
    }

    /**
     * Find out which names that are created, updated or deleted the previous day
     * and send notification to administrators and affected users for those names.
     */
    private void prepareAndNotifyForNames() {
        // prepare
        // find names for previous day
        // find changes for names
        //     split into create, update, delete
        //     prepare notification
        // log
        // send notification
        //     one email with sections for create, update, delete

        // prepare
        //     dates for previous day
        //     created
        //     updated
        //     deleted
        Date from = getPreviousDayFrom();
        Date to   = getPreviousDayTo();
        List<NotificationName> created = Lists.newArrayList();
        List<NotificationName> updated = Lists.newArrayList();
        List<NotificationName> deleted = Lists.newArrayList();

        // find names for previous day
        //     uuid ascending, id ascending
        //     distinct uuids
        List<AuditName> names = iAuditNameRepository.findByRequestedBetween(from, to);
        Set<UUID> uuids = new TreeSet<>();
        for (AuditName name : names) {
            uuids.add(name.getUuid());
        }

        // find changes for names
        //     (possibly one notification for uuid)
        //     per uuid
        //         prepare
        //             extract names per uuid (previous day)
        //             get names by uuid
        //             sort by id ascending
        //             step in list until same id
        //         compare
        //             extracted names to names by uuid
        //             split into
        //                 created
        //                 updated
        //                 deleted
        for (UUID uuid : uuids) {
            // prepare
            List<AuditName> extracted = extractByUuid(names, uuid);
            List<AuditName> namesByUuid = iAuditNameRepository.findByUuid(uuid.toString());
            sortByAuditId(extracted);
            sortByAuditId(namesByUuid);

            int index = -1;
            for (int i=0; i<namesByUuid.size(); i++) {
                if (extracted.get(0).getId().equals(namesByUuid.get(0).getId())) {
                    index = i;
                    break;
                }
            }

            // compare
            for (int i=0; i<extracted.size(); i++) {
                // find out what kind of change (may also use audit fields)
                AuditName name = extracted.get(i);
                if (i == 0 && index == 0) {
                    NotificationUtil.prepareAddNotification(created, NameCommand.CREATE, null, name);
                } else if (Boolean.TRUE.equals(name.isDeleted())) {
                    NotificationUtil.prepareAddNotification(deleted, NameCommand.DELETE, null, name);
                } else {
                    NotificationUtil.prepareAddNotification(updated, NameCommand.UPDATE, findPreviousByName(namesByUuid, name), name);
                }
            }
        }

        // log
        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        NOTIFY_NAMES_FROM_TO_CREATED_UPDATED_DELETED,
                        from.toString(),
                        to.toString(),
                        created.size(),
                        updated.size(),
                        deleted.size()));

        // send notification
        notificationService.notifyNames(created, updated, deleted);
    }

    /**
     * Return date for beginning of previous day.
     *
     * @return date date for beginning of previous day
     */
    private Date getPreviousDayFrom() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Return date for end of previous day.
     *
     * @return date for end of previous day
     */
    private Date getPreviousDayTo() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * Extract names that contain given uuid from list of names.
     *
     * @param list list of names
     * @param uuid uuid
     * @return list of names
     */
    private List<AuditName> extractByUuid(List<AuditName> list, UUID uuid) {
        List<AuditName> extracted = Lists.newArrayList();
        if (list != null && !list.isEmpty() && uuid != null) {
            for (AuditName name : list) {
                if (uuid.equals(name.getUuid())) {
                    extracted.add(name);
                }
            }
        }
        return extracted;
    }

    /**
     * Utility method to sort names according to id ascending.
     *
     * @param names names
     */
    private void sortByAuditId(List<AuditName> names) {
        Collections.sort(names, (AuditName arg0, AuditName arg1) -> arg0.getAuditId().compareTo(arg1.getAuditId()));
    }

    /**
     * Return name previous to given name in list.
     *
     * @param names list of names
     * @param name name
     * @return name
     */
    private AuditName findPreviousByName(List<AuditName> names, AuditName name) {
        if (ValidateUtil.isAnyNull(names, name)) {
            return null;
        }

        // sorted list
        for (int i=0; i<names.size(); i++) {
            if (name.getAuditId().equals(names.get(i).getAuditId())) {
                return i > 0
                        ? names.get(i-1)
                        : null;
            }
        }

        return null;
    }

}

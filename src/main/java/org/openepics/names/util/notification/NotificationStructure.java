/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import java.util.Date;
import java.util.UUID;

import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.StructureCommand;

/**
 * Utility class to store data for user notification of change for structure in Naming.
 *
 * @author Lars Johansson
 */
public class NotificationStructure {

    // note
    //     cud - create update delete

    private StructureCommand structureCommandCUD;
    private UUID uuid;
    private Type type;
    private String oldMnemonic;
    private String oldMnemonicpath;
    private String oldDescription;
    private String newMnemonic;
    private String newMnemonicpath;
    private String newDescription;
    private Date when;
    private String who;

    /**
     * Public constructor.
     *
     * @param structureCommandCUD structure command (create update delete)
     * @param type type
     * @param uuid uuid
     * @param oldMnemonic old mnemonic
     * @param oldMnemonicpath old mnemonic path
     * @param oldDescription old description
     * @param newMnemonic new mnemonic
     * @param newMnemonicpath new mnemonic path
     * @param newDescription new description
     * @param when when
     * @param who who
     */
    public NotificationStructure(StructureCommand structureCommandCUD,
            Type type, UUID uuid,
            String oldMnemonic, String oldMnemonicpath, String oldDescription,
            String newMnemonic, String newMnemonicpath, String newDescription,
            Date when, String who) {
        super();
        this.structureCommandCUD = structureCommandCUD;
        this.type = type;
        this.uuid = uuid;
        this.oldMnemonic = oldMnemonic;
        this.oldMnemonicpath = oldMnemonicpath;
        this.oldDescription = oldDescription;
        this.newMnemonic = newMnemonic;
        this.newMnemonicpath = newMnemonicpath;
        this.newDescription = newDescription;
        this.when = when;
        this.who = who;
    }

    public StructureCommand getStructureCommandCUD() {
        return structureCommandCUD;
    }
    public Type getType() {
        return type;
    }
    public UUID getUuid() {
        return uuid;
    }
    public String getOldMnemonic() {
        return oldMnemonic;
    }
    public String getOldMnemonicpath() {
        return oldMnemonicpath;
    }
    public String getOldDescription() {
        return oldDescription;
    }
    public String getNewMnemonic() {
        return newMnemonic;
    }
    public String getNewMnemonicpath() {
        return newMnemonicpath;
    }
    public String getNewDescription() {
        return newDescription;
    }
    public Date getWhen() {
        return when;
    }
    public String getWho() {
        return who;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"structureCommandCUD\": "   + getStructureCommandCUD());
        sb.append(", \"uuid\": "                + getUuid());
        sb.append(", \"type\": "                + getType());
        sb.append(", \"oldMnemonic\": "         + getOldMnemonic());
        sb.append(", \"oldMnemonicpath\": "     + getOldMnemonicpath());
        sb.append(", \"oldDescription\": "      + getOldDescription());
        sb.append(", \"newMnemonic\": "         + getNewMnemonic());
        sb.append(", \"newMnemonicpath\": "     + getNewMnemonicpath());
        sb.append(", \"newDescription\": "      + getNewDescription());
        sb.append(", \"when\": "                + getWhen());
        sb.append(", \"who\": "                 + getWho());
        sb.append("}");
        return sb.toString();
    }

}

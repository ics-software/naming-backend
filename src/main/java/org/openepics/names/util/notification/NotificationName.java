/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import java.util.Date;
import java.util.UUID;

import org.openepics.names.util.NameCommand;

/**
 * Utility class to store data for user notification of changes for name in Naming.
 *
 * @author Lars Johansson
 */
public class NotificationName {

    // cud - create update delete

    private NameCommand nameCommandCUD;
    private UUID uuid;
    private String oldIndex;
    private String oldName;
    private String oldDescription;
    private String newIndex;
    private String newName;
    private String newDescription;
    private Date when;
    private String who;

    /**
     * Public constructor.
     *
     * @param nameCommandCUD name command (create update delete)
     * @param uuid uuid
     * @param oldIndex old index
     * @param oldName old name
     * @param oldDescription old description
     * @param newIndex new index
     * @param newName new name
     * @param newDescription new description
     * @param when when
     * @param who who
     */
    public NotificationName(NameCommand nameCommandCUD,
            UUID uuid,
            String oldIndex, String oldName, String oldDescription,
            String newIndex, String newName, String newDescription,
            Date when, String who) {
        super();
        this.nameCommandCUD = nameCommandCUD;
        this.uuid = uuid;
        this.oldIndex = oldIndex;
        this.oldName = oldName;
        this.oldDescription = oldDescription;
        this.newIndex = newIndex;
        this.newName = newName;
        this.newDescription = newDescription;
        this.when = when;
        this.who = who;
    }

    public NameCommand getNameCommandCUD() {
        return nameCommandCUD;
    }
    public UUID getUuid() {
        return uuid;
    }
    public String getOldIndex() {
        return oldIndex;
    }
    public String getOldName() {
        return oldName;
    }
    public String getOldDescription() {
        return oldDescription;
    }
    public String getNewIndex() {
        return newIndex;
    }
    public String getNewName() {
        return newName;
    }
    public String getNewDescription() {
        return newDescription;
    }
    public Date getWhen() {
        return when;
    }
    public String getWho() {
        return who;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"nameCommandCUD\": "   + getNameCommandCUD());
        sb.append(", \"uuid\": "           + getUuid());
        sb.append(", \"oldIndex\": "       + getOldIndex());
        sb.append(", \"oldName\": "        + getOldName());
        sb.append(", \"oldDescription\": " + getOldDescription());
        sb.append(", \"newIndex\": "       + getNewIndex());
        sb.append(", \"newName\": "        + getNewName());
        sb.append(", \"newDescription\": " + getNewDescription());
        sb.append(", \"when\": "           + getWhen());
        sb.append(", \"who\": "            + getWho());
        sb.append("}");
        return sb.toString();
    }

}

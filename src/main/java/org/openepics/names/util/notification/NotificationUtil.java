/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.AuditName;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.HolderStructures;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.ValidateUtil;

/**
 * Utility class to assist in handling of notifications.
 *
 * @author Lars Johansson
 */
public class NotificationUtil {

    private static final Logger LOGGER = Logger.getLogger(NotificationUtil.class.getName());

    private static final String EMPTY = "";

    /**
     * This class is not to be instantiated.
     */
    private NotificationUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Prepare notification and add to list of notifications if notification is available.
     *
     * @param notifications list of notifications
     * @param nameCommand name command
     * @param previous previous (name)
     * @param name name
     */
    public static void prepareAddNotification(List<NotificationName> notifications, NameCommand nameCommand, AuditName previous, AuditName name) {
        if (notifications == null) {
            return;
        }

        NotificationName notification = NotificationUtil.prepareNotification(nameCommand, previous, name);
        if (notification != null) {
            notifications.add(notification);
        }
    }

    /**
     * Prepare and return notification for name with information about change.
     * If information is not correct, <tt>null</tt> is returned.
     *
     * @param nameCommand name command
     * @param previous previous (name)
     * @param name name
     * @return notification
     */
    public static NotificationName prepareNotification(NameCommand nameCommand, AuditName previous, AuditName name) {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "nameCommand", nameCommand));
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "previous", previous));
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "name", name));
        }

        if (ValidateUtil.isAnyNull(nameCommand, name)) {
            return null;
        }
        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.UPDATE) && previous == null) {
            return null;
        }

        // notify
        //     combinations of commands and names
        //     about information going from previous to present
        //     to be able to notify about change
        //     ------------------------------------------------
        //     create                   name
        //     update        previous + name
        //     delete                   name
        // support fields
        //     name
        //     description

        String oldIndex = EMPTY;
        String oldName = EMPTY;
        String oldDescription = EMPTY;
        String newIndex = StringUtils.stripToEmpty(name.getInstanceIndex());
        String newName = StringUtils.stripToEmpty(name.getConventionName());
        String newDescription = StringUtils.stripToEmpty(name.getDescription());

        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.UPDATE)) {
            oldIndex = StringUtils.stripToEmpty(previous.getInstanceIndex());
            oldName = StringUtils.stripToEmpty(previous.getConventionName());
            oldDescription = StringUtils.stripToEmpty(previous.getDescription());
        }

        return new NotificationName(
                nameCommand,
                name.getUuid(),
                oldIndex,
                oldName,
                oldDescription,
                newIndex,
                newName,
                newDescription,
                name.getRequested(),
                name.getRequestedBy());
    }

    /**
     * Prepare and return notification for system group with information about change.
     * If information is not correct, <tt>null</tt> is returned.
     *
     * @param type type
     * @param structureCommandCUD structure command (create update delete)
     * @param previous previous (structure)
     * @param structure structure
     * @param holderStructures holder of system and device structure content
     * @return notification
     */
    public static NotificationStructure prepareNotification(Type type, StructureCommand structureCommandCUD,
            Structure previous, Structure structure, HolderStructures holderStructures) {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "type", type));
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "structureCommandCUD", structureCommandCUD));
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "previous", previous));
            LOGGER.log(Level.FINER, MessageFormat.format(TextUtil.DESCRIPTION_NAME_VALUE, TextUtil.PREPARE_NOTIFICATION, "structure", structure));
        }

        if (ValidateUtil.isAnyNull(holderStructures, type, structureCommandCUD, structure)) {
            return null;
        }
        if (ValidateUtil.isAnyEqual(structureCommandCUD, StructureCommand.UPDATE) && previous == null) {
            return null;
        }

        // type
        //     system group
        //     system
        //     subsystem
        //     discipline
        //     device group
        //     device type
        // notify - structureCommandCUD + structureCommandACR
        //     combinations of commands and structures
        //     about information going from previous to present
        //     to be able to notify about change
        //     ------------------------------------------------
        //     create                             structure
        //     update                  previous + structure
        //     delete                             structure
        // support fields
        //     mnemonic
        //     mnemonic path - to support hierarchy
        //     description

        String oldMnemonic = EMPTY;
        String oldMnemonicpath = EMPTY;
        String oldDescription = EMPTY;
        String newMnemonic = EMPTY;
        String newMnemonicpath = EMPTY;
        String newDescription = EMPTY;

        Date when = structure.getProcessed();
        String who = structure.getProcessedBy();

        who = StringUtils.stripToEmpty(who);

        if (ValidateUtil.isAnyEqual(structureCommandCUD, StructureCommand.UPDATE)) {
            if (Type.SYSTEMGROUP.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((SystemGroup) previous, holderStructures);
            } else if (Type.SYSTEM.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((System) previous, holderStructures);
            } else if (Type.SUBSYSTEM.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((Subsystem) previous, holderStructures);
            } else if (Type.DISCIPLINE.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((Discipline) previous, holderStructures);
            } else if (Type.DEVICEGROUP.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((DeviceGroup) previous, holderStructures);
            } else if (Type.DEVICETYPE.equals(type)) {
                oldMnemonicpath = StructureUtil.getMnemonicPath((DeviceType) previous, holderStructures);
            }

            oldMnemonic = StringUtils.stripToEmpty(previous.getMnemonic());
            oldMnemonicpath = StringUtils.stripToEmpty(oldMnemonicpath);
            oldDescription = StringUtils.stripToEmpty(previous.getDescription());
        }

        if (Type.SYSTEMGROUP.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((SystemGroup) structure, holderStructures);
        } else if (Type.SYSTEM.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((System) structure, holderStructures);
        } else if (Type.SUBSYSTEM.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((Subsystem) structure, holderStructures);
        } else if (Type.DISCIPLINE.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((Discipline) structure, holderStructures);
        } else if (Type.DEVICEGROUP.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((DeviceGroup) structure, holderStructures);
        } else if (Type.DEVICETYPE.equals(type)) {
            newMnemonicpath = StructureUtil.getMnemonicPath((DeviceType) structure, holderStructures);
        }
        newMnemonic = StringUtils.stripToEmpty(structure.getMnemonic());
        newMnemonicpath = StringUtils.stripToEmpty(newMnemonicpath);
        newDescription = StringUtils.stripToEmpty(structure.getDescription());

        return new NotificationStructure(
                structureCommandCUD,
                type,
                structure.getUuid(),
                oldMnemonic,
                oldMnemonicpath,
                oldDescription,
                newMnemonic,
                newMnemonicpath,
                newDescription,
                when,
                who);
    }

    /**
     * Utility method to sort name notifications according to new name ascending.
     *
     * @param notifications name notifications
     */
    public static void sortByNewName(List<NotificationName> notifications) {
        if (notifications != null) {
            Collections.sort(notifications,
                    (NotificationName arg0, NotificationName arg1) -> arg0.getNewName().compareTo(arg1.getNewName()));
        }
    }

    /**
     * Utility method to sort structure notifications according to new mnemonic path ascending.
     *
     * @param notifications structure notifications
     */
    public static void sortByNewMnemonicpath(List<NotificationStructure> notifications) {
        if (notifications != null) {
            Collections.sort(notifications,
                    (NotificationStructure arg0, NotificationStructure arg1) -> arg0.getNewMnemonicpath().compareTo(arg1.getNewMnemonicpath()));
        }
    }

}

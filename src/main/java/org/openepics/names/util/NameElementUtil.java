/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.openepics.names.repository.model.AuditName;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in populating name elements based on repository content.
 * <br/><br/>
 * Different strategies for population of name elements are used in different methods.
 * The difference in strategies is for finding out system structure and device structure mnemonic paths.
 *
 * @author Lars Johansson
 */
public class NameElementUtil {

    // note
    //     requested attributes for names are used, not processed attributes

    /**
     * This class is not to be instantiated.
     */
    private NameElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return list of name elements for list of names.
     *
     * @param auditNames audit names
     * @param holderStructures container for system and device structure content
     * @return list of name elements
     */
    public static List<NameElement> getNameElementsForAuditNames(List<AuditName> auditNames, HolderStructures holderStructures) {
        List<NameElement> nameElements = Lists.newArrayList();
        for (AuditName auditName : auditNames) {
            nameElements.add(NameElementUtil.getNameElement(auditName.getNonAuditName(), holderStructures));
        }
        return nameElements;
    }
    /**
     * Populate and return list of name elements for list of names.
     *
     * @param names names
     * @param holderStructures container for system and device structure content
     * @return list of name elements
     */
    public static List<NameElement> getNameElements(List<Name> names, HolderStructures holderStructures) {
        List<NameElement> nameElements = Lists.newArrayList();
        for (Name name : names) {
            nameElements.add(NameElementUtil.getNameElement(name, holderStructures));
        }
        return nameElements;
    }

    /**
     * Populate and return name element for name.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return name element
     */
    public static NameElement getNameElement(Name name, HolderStructures holderStructures) {
        return getNameElement(name, holderStructures, null);
    }
    /**
     * Populate and return name element for name.
     *
     * @param name name
     * @param structure (system) structure
     * @return name element
     */
    public static NameElement getNameElement(Name name, Structure structure) {
        return getNameElement(name, null, structure);
    }
    /**
     * Populate and return name element for name.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @param structure (system) structure
     * @return name element
     */
    public static NameElement getNameElement(Name name, HolderStructures holderStructures, Structure structure) {
        if (name == null || (holderStructures == null && structure == null)) {
            return null;
        }
        if (structure != null && !(structure instanceof SystemGroup || structure instanceof System || structure instanceof Subsystem)) {
            return null;
        }

        UUID parentSystemStructureUuid = null;
        UUID parentDeviceStructureUuid = null;

        if (structure != null) {
            parentSystemStructureUuid = structure.getUuid();
        } else {
            parentSystemStructureUuid = getParentSystemStructureUuid(name, holderStructures);
            parentDeviceStructureUuid = name.getDeviceTypeId() != null ? holderStructures.findDeviceTypeById(name.getDeviceTypeId()).getUuid() : null;
        }

        return getNameElement(
                name.getUuid(), parentSystemStructureUuid, parentDeviceStructureUuid,
                name.getInstanceIndex(), name.getDescription(),
                NamingConventionUtil.extractMnemonicPathSystemStructure(name.getConventionName()),
                NamingConventionUtil.extractMnemonicPathDeviceStructure(name.getConventionName()),
                name.getConventionName(),
                name.getStatus(), name.isDeleted(),
                name.getRequested(), name.getRequestedBy(), name.getRequestedComment());
    }

    /**
     * Return UUID for parent system structure.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return uuid
     */
    public static UUID getParentSystemStructureUuid(Name name, HolderStructures holderStructures) {
        return name.getSubsystemId() != null
                ? holderStructures.findSubsystemById(name.getSubsystemId()).getUuid()
                : name.getSystemId() != null
                    ? holderStructures.findSystemById(name.getSystemId()).getUuid()
                    : name.getSystemGroupId() != null
                        ? holderStructures.findSystemGroupById(name.getSystemGroupId()).getUuid()
                        : null;
    }

    /**
     * Return UUID for system group or null if non-existent.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return uuid
     */
    public static UUID getSystemGroupUuid(Name name, HolderStructures holderStructures) {
        if (name == null || name.getSystemGroupId() == null ||  holderStructures == null) {
            return null;
        }

        return name.getSystemGroupId() != null
                ? holderStructures.findSystemGroupById(name.getSystemGroupId()).getUuid()
                : null;
    }
    /**
     * Return UUID for system or null if non-existent.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return uuid
     */
    public static UUID getSystemUuid(Name name, HolderStructures holderStructures) {
        if (name == null || name.getSystemId() == null ||  holderStructures == null) {
            return null;
        }

        return name.getSystemId() != null
                ? holderStructures.findSystemById(name.getSystemId()).getUuid()
                : null;
    }
    /**
     * Return UUID for subsystem or null if non-existent.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return uuid
     */
    public static UUID getSubsystemUuid(Name name, HolderStructures holderStructures) {
        if (name == null || name.getSubsystemId() == null ||  holderStructures == null) {
            return null;
        }

        return name.getSubsystemId() != null
                ? holderStructures.findSubsystemById(name.getSubsystemId()).getUuid()
                : null;
    }
    /**
     * Return UUID for device type or null if non-existent.
     *
     * @param name name
     * @param holderStructures container for system and device structure content
     * @return uuid
     */
    public static UUID getDeviceTypeUuid(Name name, HolderStructures holderStructures) {
        if (name == null || name.getDeviceTypeId() == null || holderStructures == null) {
            return null;
        }

        return name.getDeviceTypeId() != null
                ? holderStructures.findDeviceTypeById(name.getDeviceTypeId()).getUuid()
                : null;
    }

    /**
     * Populate and return name element.
     *
     * @param uuid uuid
     * @param parentSystemStructure parent system structure uuid (system group, system, subsystem)
     * @param parentDeviceStructure parent device structure uuid (device type)
     * @param index instance index
     * @param description description
     * @param systemStructure system structure mnemonic path
     * @param deviceStructure device structure mnemonic path
     * @param name name
     * @param status status
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     * @return name element
     */
    protected static NameElement getNameElement(
            UUID uuid, UUID parentSystemStructure, UUID parentDeviceStructure,
            String index,  String description,
            String systemStructure, String deviceStructure, String name,
            Status status, Boolean deleted,
            Date when, String who, String comment) {

        return new NameElement(
                uuid, parentSystemStructure, parentDeviceStructure,
                index, description,
                systemStructure, deviceStructure, name,
                status, deleted,
                when, who, comment);
    }

    /**
     * Check if name element and name have same content.
     *
     * @param nameElement name element
     * @param name name
     * @param holderIRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     * @return true if name element and name have same content, false otherwise
     */
    public static boolean hasSameContent(NameElementCommand nameElement, Name name, HolderIRepositories holderIRepositories, HolderStructures holderStructures) {
        // NameElement
        //     x parentSystemStructure (system group, system, subsystem)
        //     x parentDeviceStructure (device type)
        //     x index
        //     x name
        // NameElementCommand
        //     uuid
        //     parentSystemStructure
        //     parentDeviceStructure
        //     index
        //     description
        // Name
        //     x systemgroup_uuid
        //     x system_uuid
        //     x subsystem_uuid
        //     x devicetype_uuid
        //     x instance_index
        //     x convention_name

        // find out system group, system, subsystem
        //     one of the three expected to be non-null, other two expected to be null

        SystemGroup systemGroup = holderIRepositories.systemGroupRepository().findByUuid(nameElement.getParentSystemStructure().toString());
        System      system      = holderIRepositories.systemRepository().findByUuid(nameElement.getParentSystemStructure().toString());
        Subsystem   subsystem   = holderIRepositories.subsystemRepository().findByUuid(nameElement.getParentSystemStructure().toString());
        DeviceType  deviceType  = null;
        if (nameElement.getParentDeviceStructure() != null) {
            deviceType = holderIRepositories.deviceTypeRepository().findByUuid(nameElement.getParentDeviceStructure().toString());
        }

        String derivedName = null;
        if (systemGroup != null) {
            derivedName = NameUtil.getName(systemGroup, deviceType, nameElement.getIndex(), holderStructures);
        } else if (system != null) {
            derivedName = NameUtil.getName(system, deviceType, nameElement.getIndex(), holderStructures);
        } else if (subsystem != null) {
            derivedName = NameUtil.getName(subsystem, deviceType, nameElement.getIndex(), holderStructures);
        } else {
            return false;
        }

        if (nameElement.getUuid() == null) {
            if (name.getUuid() != null)
                return false;
        } else if (!nameElement.getUuid().equals(name.getUuid()))
            return false;
        if (derivedName == null) {
            if (name.getConventionName() != null)
                return false;
        } else if (!derivedName.equals(name.getConventionName()))
            return false;
        if (nameElement.getDescription() == null) {
            if (name.getDescription() != null)
                return false;
        } else if (!nameElement.getDescription().equals(name.getDescription()))
            return false;

        return true;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert name element command for create to name element command.
     *
     * @param command name element command for create
     * @return name element command
     */
    public static NameElementCommand convertCommandCreate2Command(NameElementCommandCreate command) {
        return new NameElementCommand(
                null,
                command.getParentSystemStructure(),
                command.getParentDeviceStructure(),
                command.getIndex(),
                command.getDescription());
    }

    /**
     * Convert list of name element commands for create to list of name element commands.
     *
     * @param commands list of name element commands for create
     * @return list of name element commands
     */
    public static List<NameElementCommand> convertCommandCreate2Command(List<NameElementCommandCreate> commands) {
        List<NameElementCommand> nameElementCommands = Lists.newArrayList();
        for (NameElementCommandCreate command : commands) {
            nameElementCommands.add(NameElementUtil.convertCommandCreate2Command(command));
        }
        return nameElementCommands;
    }

    /**
     * Convert name element command for update to name element command.
     *
     * @param command name element command for update
     * @return name element command
     */
    public static NameElementCommand convertCommandUpdate2Command(NameElementCommandUpdate command) {
        return new NameElementCommand(
                command.getUuid(),
                command.getParentSystemStructure(),
                command.getParentDeviceStructure(),
                command.getIndex(),
                command.getDescription());
    }

    /**
     * Convert list of name element commands for update to list of name element commands.
     *
     * @param commands list of name element commands for update
     * @return list of name element commands
     */
    public static List<NameElementCommand> convertCommandUpdate2Command(List<NameElementCommandUpdate> commands) {
        List<NameElementCommand> nameElementCommands = Lists.newArrayList();
        for (NameElementCommandUpdate command : commands) {
            nameElementCommands.add(NameElementUtil.convertCommandUpdate2Command(command));
        }
        return nameElementCommands;
    }

    /**
     * Convert name element command for confirm to name element command.
     *
     * @param command name element command for confirm
     * @return name element command
     */
    public static NameElementCommand convertCommandConfirm2Command(NameElementCommandConfirm command) {
        return new NameElementCommand(
                command.getUuid(),
                null,
                null,
                null,
                null);
    }

    /**
     * Convert list of name element commands for confirm to list of name element commands.
     *
     * @param commands list of name element commands for confirm
     * @return list of name element commands
     */
    public static List<NameElementCommand> convertCommandConfirm2Command(List<NameElementCommandConfirm> commands) {
        List<NameElementCommand> nameElementCommands = Lists.newArrayList();
        for (NameElementCommandConfirm command : commands) {
            nameElementCommands.add(NameElementUtil.convertCommandConfirm2Command(command));
        }
        return nameElementCommands;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert name element command to name element command for create.
     *
     * @param command name element command
     * @return name element command for create
     */
    public static NameElementCommandCreate convertCommand2CommandCreate(NameElementCommand command) {
        return new NameElementCommandCreate(
                command.getParentSystemStructure(),
                command.getParentDeviceStructure(),
                command.getIndex(),
                command.getDescription());
    }

    /**
     * Convert array of name element commands to array of name element commands for create.
     *
     * @param commands array of name element commands
     * @return array of name element commands for create
     */
    public static NameElementCommandCreate[] convertCommand2CommandCreate(NameElementCommand[] commands) {
        NameElementCommandCreate[] nameElementCommands = new NameElementCommandCreate[commands.length];
        for (int i=0; i<commands.length; i++) {
            nameElementCommands[i] = NameElementUtil.convertCommand2CommandCreate(commands[i]);
        }
        return nameElementCommands;
    }

    /**
     * Convert name element command to name element command for update.
     *
     * @param command name element command
     * @return name element command for update
     */
    public static NameElementCommandUpdate convertCommand2CommandUpdate(NameElementCommand command) {
        return new NameElementCommandUpdate(
                command.getUuid(),
                command.getParentSystemStructure(),
                command.getParentDeviceStructure(),
                command.getIndex(),
                command.getDescription());
    }

    /**
     * Convert array of name element commands to array of name element commands for update.
     *
     * @param commands array of name element commands
     * @return array of name element commands for update
     */
    public static NameElementCommandUpdate[] convertCommand2CommandUpdate(NameElementCommand[] commands) {
        NameElementCommandUpdate[] nameElementCommands = new NameElementCommandUpdate[commands.length];
        for (int i=0; i<commands.length; i++) {
            nameElementCommands[i] = NameElementUtil.convertCommand2CommandUpdate(commands[i]);
        }
        return nameElementCommands;
    }

    /**
     * Convert name element command to name element command for confirm.
     *
     * @param command name element command
     * @return name element command for confirm
     */
    public static NameElementCommandConfirm convertCommand2CommandConfirm(NameElementCommand command) {
        return new NameElementCommandConfirm(
                command.getUuid());
    }

    /**
     * Convert array of name element commands to array of name element commands for confirm.
     *
     * @param commands array of name element commands
     * @return array of name element commands for confirm
     */
    public static NameElementCommandConfirm[] convertCommand2CommandConfirm(NameElementCommand[] commands) {
        NameElementCommandConfirm[] nameElementCommands = new NameElementCommandConfirm[commands.length];
        for (int i=0; i<commands.length; i++) {
            nameElementCommands[i] = NameElementUtil.convertCommand2CommandConfirm(commands[i]);
        }
        return nameElementCommands;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert name element to name element command for create.
     *
     * @param element name element
     * @return name element command for create
     */
    public static NameElementCommandCreate convertElement2CommandCreate(NameElement element) {
        return new NameElementCommandCreate(
                element.getParentSystemStructure(),
                element.getParentDeviceStructure(),
                element.getIndex(),
                element.getDescription());
    }

    /**
     * Convert array of name elements to array of name element commands for create.
     *
     * @param elements array of name elements
     * @return array of name element commands for create
     */
    public static NameElementCommandCreate[] convertElement2CommandCreate(NameElement[] elements) {
        NameElementCommandCreate[] nameElementCommands = new NameElementCommandCreate[elements.length];
        for (int i=0; i<elements.length; i++) {
            nameElementCommands[i] = convertElement2CommandCreate(elements[i]);
        }
        return nameElementCommands;
    }

    /**
     * Convert name element to name element command for update.
     *
     * @param element name element
     * @return name element command for update
     */
    public static NameElementCommandUpdate convertElement2CommandUpdate(NameElement element) {
        return new NameElementCommandUpdate(
                element.getUuid(),
                element.getParentSystemStructure(),
                element.getParentDeviceStructure(),
                element.getIndex(),
                element.getDescription());
    }

    /**
     * Convert array of name elements to array of name element commands for update.
     *
     * @param elements array of name elements
     * @return array of name element commands for update
     */
    public static NameElementCommandUpdate[] convertElement2CommandUpdate(NameElement[] elements) {
        NameElementCommandUpdate[] nameElementCommands = new NameElementCommandUpdate[elements.length];
        for (int i=0; i<elements.length; i++) {
            nameElementCommands[i] =  convertElement2CommandUpdate(elements[i]);
        }
        return nameElementCommands;
    }

    /**
     * Convert name element to name element command for confirm.
     *
     * @param element name element
     * @return name element command for confirm
     */
    public static NameElementCommandConfirm convertElement2CommandConfirm(NameElement element) {
        return new NameElementCommandConfirm(
                element.getUuid());
    }

    /**
     * Convert array of name elements to array of name element commands for confirm.
     *
     * @param elements array of name elements
     * @return array of name element commands for confirm
     */
    public static NameElementCommandConfirm[] convertElement2CommandConfirm(NameElement[] elements) {
        NameElementCommandConfirm[] nameElementCommands = new NameElementCommandConfirm[elements.length];
        for (int i=0; i<elements.length; i++) {
            nameElementCommands[i] = convertElement2CommandConfirm(elements[i]);
        }
        return nameElementCommands;
    }

}

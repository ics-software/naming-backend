/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class to assist in handling of patterns for queries.
 *
 * @author Lars Johansson
 */
public class RepositoryUtil {

    private static final String PERCENT = "%";

    /**
     * This class is not to be instantiated.
     */
    private RepositoryUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Prepare pattern for use in where clause (predicate).
     * Purpose to remove excess characters and similar work.
     *
     * @param value value
     * @return prepared pattern
     */
    public static String preparePattern(String value) {
        // jpa query characters % and _
        // remove excess % characters
        String queryValue = value;
        if (!StringUtils.isEmpty(queryValue)) {
            if (queryValue.startsWith(PERCENT)) {
                while (queryValue.startsWith(PERCENT)) {
                    queryValue = queryValue.substring(1);
                }
                queryValue = PERCENT + queryValue;
            }
            if (queryValue.endsWith(PERCENT)) {
                while (queryValue.endsWith(PERCENT)) {
                    queryValue = queryValue.substring(0, queryValue.length()-1);
                }
                queryValue = queryValue + PERCENT;
            }
        }
        return queryValue;
    }

}

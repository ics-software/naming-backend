/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.openepics.names.repository.IAuditNameRepository;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;

/**
 * Utility record to collect references to repositories (interfaces).
 *
 * @author Lars Johansson
 */
public record HolderIRepositories (
        INameRepository nameRepository,
        ISystemGroupRepository systemGroupRepository,
        ISystemRepository systemRepository,
        ISubsystemRepository subsystemRepository,
        IDisciplineRepository disciplineRepository,
        IDeviceGroupRepository deviceGroupRepository,
        IDeviceTypeRepository deviceTypeRepository,
        IAuditNameRepository auditNameRepository,
        IAuditStructureRepository auditStructureRepository) {}

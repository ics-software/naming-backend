/*-
 * Copyright (c) 2014 European Spallation Source ERIC.
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Naming Service.
 * Naming Service is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */

package org.openepics.names.util;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.rest.beans.Type;

/**
 * A naming convention definition used by ESS.
 *
 * @author Marko Kolar
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see NamingConvention
 * @see NamingConventionUtil
 */
public class EssNamingConvention implements NamingConvention {

    // ----------------------------------------------------------------------------------------------------
    // rules for characters
    //     empty
    //     space
    //     value
    //     alphanumeric
    //     letter case
    //     length
    //     equivalenceClassRepresentative
    // note
    //     mnemonic can be empty
    //         system structure - system group
    //         device structure - device group
    // note
    //     mnemonic for system group may be part of ess name
    //     mnemonic for instance index may be omitted for ess name
    // ----------------------------------------------------------------------------------------------------

    private static final String MNEMONIC_ALPHABETIC_LOWERCASE = "^[a-z]+$";
    private static final String MNEMONIC_ALPHANUMERIC         = "^[a-zA-Z0-9]+$";
    private static final String MNEMONIC_NUMERIC              = "^[0-9]+$";
    private static final String MNEMONIC_NUMERIC_ZERO         = "^[0]+$";

    @Override
    public String equivalenceClassRepresentative(String name) {
        return name != null
                ? name.toUpperCase()
                        .replaceAll("(?<=[A-Za-z])0+", "")
                        .replace('I', '1').replace('L', '1').replace('O', '0')
                        .replaceAll("(?<!\\d)0+(?=\\d)", "")
                : null;
    }

    @Override
    public boolean isInstanceIndexValid(String conventionName) {
        // not override ruleset for instanceIndex
        return isInstanceIndexValid(conventionName, false);
    }

    @Override
    public boolean isInstanceIndexValid(String conventionName, boolean overrideRuleset) {
        // ability to override ruleset for administrator

        String instanceIndex = NamingConventionUtil.extractInstanceIndex(conventionName);
        if (overrideRuleset) {
            // previous rules, less restrictions

            if (instanceIndex != null && instanceIndex.length() <= 6) {
                return instanceIndex.matches(MNEMONIC_ALPHANUMERIC);
            } else {
                return false;
            }
        } else {
            // normal path

            // p&id numeric
            // p&id
            // scientific

            if (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(conventionName))) {
                if (NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(conventionName))) {
                    // 3 numeric
                    return isInstanceIndexValidNumeric(instanceIndex, 3, 3);
                }
                // 3 numeric & 0-3 alphabetic
                return isInstanceIndexValidPID(instanceIndex);
            } else {
                // 1-4 numeric & non-zero
                return isInstanceIndexValidNumeric(instanceIndex, 1, 4);
            }
        }
    }

    /**
     * Return if instance index is valid PID index.
     *
     * @param index instance index
     * @return if instance index is valid
     */
    private boolean isInstanceIndexValidPID(String index) {
        if (index != null && index.length() >= 3 && index.length() <= 6) {
            if (index.length() == 3) {
                return index.matches(MNEMONIC_NUMERIC)
                        && !index.matches(MNEMONIC_NUMERIC_ZERO);
            } else {
                return index.substring(0, 3).matches(MNEMONIC_NUMERIC)
                        && index.substring(3, index.length()).matches(MNEMONIC_ALPHABETIC_LOWERCASE);
            }
        }
        return false;
    }

    /**
     * Return if instance index is valid numeric index.
     *
     * @param index instance index
     * @param nMin minimum length
     * @param nMax maximum length
     * @return if instance index is valid
     */
    private boolean isInstanceIndexValidNumeric(String index, int nMin, int nMax) {
        if ((index == null || index.length() == 0)) {
            return nMin <= 0;
        } else {
            return (index.length() >= nMin && index.length() <= nMax)
                    && index.matches(MNEMONIC_NUMERIC)
                    && !index.matches(MNEMONIC_NUMERIC_ZERO);
        }
    }

    @Override
    public boolean isMnemonicPathValid(String mnemonicPath) {
        // note
        //     split with help of utility
        // valid if
        //     length 1, 2, 3
        //     same mnemonic only once in mnemonic path

        if (!StringUtils.isEmpty(mnemonicPath)) {
            String[] values = NamingConventionUtil.string2MnemonicPath(mnemonicPath.trim());
            if (values.length > 3) {
                return false;
            }
            switch (values.length) {
                case 3:
                    return !StringUtils.equals(values[0], values[1])
                            && !StringUtils.equals(values[0], values[2])
                            && !StringUtils.equals(values[1], values[2]);
                case 2:
                    return !StringUtils.equals(values[0], values[1]);
                case 1:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    @Override
    public boolean isMnemonicRequired(Type type) {
        return type != null
                && (Type.SYSTEM.equals(type)
                        || Type.SUBSYSTEM.equals(type)
                        || Type.DISCIPLINE.equals(type)
                        || Type.DEVICETYPE.equals(type));
    }

    @Override
    public MnemonicValidation validateMnemonic(Type type, String mnemonic) {
        // mnemonic validation for system and device structure elements
        // rules for characters
        //     depends on system/device structure, required or not
        //     alphanumeric - MNEMONIC_ALPHANUMERIC
        //     length
        //         system structure
        //             system group  - 0-6 characters, not required
        //             system        - 1-8 characters
        //             sub system    - 1-8 characters
        //         device structure
        //             discipline    - 1-6 characters
        //             device group  - not required
        //             device type   - 1-6 characters
        //
        // validation for last element in structure / path
        // accelerator as system
        //     system group as system, not required

        if (type != null) {
            boolean empty = StringUtils.isEmpty(mnemonic);
            int length = empty ? 0 : mnemonic.length();
            boolean length1to6 = (length >= 1) && (length <= 6);
            boolean length1to8 = (length >= 1) && (length <= 8);
            boolean matchMnemonic = !empty && mnemonic.matches(MNEMONIC_ALPHANUMERIC);

            MnemonicValidation result = MnemonicValidation.VALID;

            if (Type.SYSTEMGROUP.equals(type)) {
                if (empty) {
                    return result;
                } else if (!length1to6) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (Type.SYSTEM.equals(type) || Type.SUBSYSTEM.equals(type)) {
                if (empty) {
                    result = MnemonicValidation.EMPTY;
                } else if (!length1to8) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (Type.DISCIPLINE.equals(type) || Type.DEVICETYPE.equals(type)) {
                if (empty) {
                    result = MnemonicValidation.EMPTY;
                } else if (!length1to6) {
                    result = MnemonicValidation.TOO_LONG;
                } else if (!matchMnemonic) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            } else if (Type.DEVICEGROUP.equals(type)) {
                if (!empty) {
                    result = MnemonicValidation.NON_ACCEPTABLE_CHARS;
                }
            }

            return result;
        }
        return null;
    }

}

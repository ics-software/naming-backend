/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceGroup;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in handling of structure (name part) content.
 *
 * @author Lars Johansson
 */
public class StructureUtil {

    /**
     * This class is not to be instantiated.
     */
    private StructureUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return mnemonic path for system group.
     *
     * @param systemGroup system group
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(SystemGroup systemGroup, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(systemGroup, holderStructures)) {
            return null;
        }

        return !StringUtils.isEmpty(systemGroup.getMnemonic())
                ? systemGroup.getMnemonic()
                : null;
    }

    /**
     * Return mnemonic path for system.
     *
     * @param system system
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(System system, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(system, holderStructures)) {
            return null;
        }

        return system.getMnemonic();
    }

    /**
     * Return mnemonic path for subsystem.
     *
     * @param subsystem subsystem
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(Subsystem subsystem, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(subsystem, holderStructures)) {
            return null;
        }

        System system = holderStructures.findSystemById(subsystem.getParentId());
        return system != null
                ? system.getMnemonic() + "-" + subsystem.getMnemonic()
                : null;
    }

    /**
     * Return mnemonic path for discipline.
     *
     * @param discipline discipline
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(Discipline discipline, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(discipline, holderStructures)) {
            return null;
        }

        return discipline.getMnemonic();
    }

    /**
     * Return mnemonic path for device group.
     *
     * @param deviceGroup device group
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(DeviceGroup deviceGroup, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(deviceGroup, holderStructures)) {
            return null;
        }

        Discipline discipline = holderStructures.findDisciplineById(deviceGroup.getParentId());
        return discipline != null
                ? discipline.getMnemonic()
                : null;
    }

    /**
     * Return mnemonic path for device type.
     *
     * @param deviceType device type
     * @param holderStructures holder of system and device structure content
     * @return mnemonic path
     */
    public static String getMnemonicPath(DeviceType deviceType, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(deviceType, holderStructures)) {
            return null;
        }

        DeviceGroup deviceGroup = holderStructures.findDeviceGroupById(deviceType.getParentId());
        Discipline  discipline  = deviceGroup != null
                ? holderStructures.findDisciplineById(deviceGroup.getParentId())
                : null;
        return discipline != null && deviceGroup != null
                ? discipline.getMnemonic() + "-" + deviceType.getMnemonic()
                : null;
    }

    // --------------------------------------------------

    /**
     * Return a list of mnemonic paths for system groups.
     *
     * @param holderStructures holder of system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for system groups
     */
    public static List<String> getMnemonicPathsSystemGroup(HolderStructures holderStructures, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<Long, SystemGroup> entry : holderStructures.getIdSystemGroups().entrySet()) {
            if (!StringUtils.isEmpty(entry.getValue().getMnemonic())) {
                value = entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for systems.
     *
     * @param holderStructures holder of system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for systems
     */
    public static List<String> getMnemonicPathsSystem(HolderStructures holderStructures, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<Long, System> entry : holderStructures.getIdSystems().entrySet()) {
            value = entry.getValue().getMnemonic();

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for subsystems.
     *
     * @param holderStructures holder of system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for subsystems
     */
    public static List<String> getMnemonicPathsSubsystem(HolderStructures holderStructures, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<Long, Subsystem> entry : holderStructures.getIdSubsystems().entrySet()) {
            System system = holderStructures.findSystemById(entry.getValue().getParentId());
            value = system.getMnemonic() + "-" + entry.getValue().getMnemonic();

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for disciplines.
     *
     * @param holderStructures holder of system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for disciplines
     */
    public static List<String> getMnemonicPathsDiscipline(HolderStructures holderStructures, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<Long, Discipline> entry : holderStructures.getIdDisciplines().entrySet()) {
            if (!StringUtils.isEmpty(entry.getValue().getMnemonic())) {
                value = entry.getValue().getMnemonic();
            }

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    /**
     * Return a list of mnemonic paths for device types.
     *
     * @param holderStructures holder of system and device structure content
     * @param mnemonicEquivalence use mnemonic equivalence instead of mnemonic
     * @param namingConvention naming convention
     * @return a list of mnemonic paths for device types
     */
    public static List<String> getMnemonicPathsDeviceType(HolderStructures holderStructures, boolean mnemonicEquivalence, EssNamingConvention namingConvention) {
        List<String> mnemonicPaths = new ArrayList<>();
        String value = null;
        for (Entry<Long, DeviceType> entry : holderStructures.getIdDeviceTypes().entrySet()) {
            DeviceGroup deviceGroup = holderStructures.findDeviceGroupById(entry.getValue().getParentId());
            Discipline discipline = holderStructures.findDisciplineById(deviceGroup.getParentId());
            value = discipline.getMnemonic() + "-" + entry.getValue().getMnemonic();

            if (mnemonicEquivalence) {
                mnemonicPaths.add(namingConvention.equivalenceClassRepresentative(value));
            } else {
                mnemonicPaths.add(value);
            }
        }
        return mnemonicPaths;
    }

    // --------------------------------------------------

    protected static List<UUID> listSystemGroup2Uuid(List<SystemGroup> systemGroups) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (SystemGroup systemGroup : systemGroups) {
            listUuid.add(systemGroup.getUuid());
        }
        return listUuid;
    }
    protected static List<UUID> listSystem2Uuid(List<System> systems) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (System deviceType : systems) {
            listUuid.add(deviceType.getUuid());
        }
        return listUuid;
    }
    protected static List<UUID> listSubsystem2Uuid(List<Subsystem> subsystems) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (Subsystem subsystem : subsystems) {
            listUuid.add(subsystem.getUuid());
        }
        return listUuid;
    }

    protected static List<UUID> listDiscipline2Uuid(List<Discipline> disciplines) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (Discipline discipline : disciplines) {
            listUuid.add(discipline.getUuid());
        }
        return listUuid;
    }
    protected static List<UUID> listDeviceGroup2Uuid(List<DeviceGroup> deviceGroups) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (DeviceGroup deviceGroup : deviceGroups) {
            listUuid.add(deviceGroup.getUuid());
        }
        return listUuid;
    }
    protected static List<UUID> listDeviceType2Uuid(List<DeviceType> deviceTypes) {
        final List<UUID> listUuid = Lists.newArrayList();
        for (DeviceType deviceType : deviceTypes) {
            listUuid.add(deviceType.getUuid());
        }
        return listUuid;
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openepics.names.exception.ServiceException;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in handling of Excel files.
 *
 * @author Lars Johansson
 */
public class ExcelUtil {

    private static final Logger LOGGER = Logger.getLogger(ExcelUtil.class.getName());

    public static final String     MIME_TYPE_EXCEL                  = "application/vnd.ms-excel";
    public static final String     MIME_TYPE_OPENXML_SPREADSHEET    = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    // width (in units of 1/256th of a character width)
    // column width 3.44 inch (to fit header text and uuid)
    private static final int        SHEET_COLUMN_WIDTH              = 35 * 256 + 64;

    private static final int        NAMEELEMENTCOMMAND_LENGTH       = 5;
    private static final String[][] NAMEELEMENT_HEADER_COMMENT      = {
            {"Uuid",                  "Identity (uuid) of the name entry. Value is created server-side."},
            {"ParentSystemStructure", "Identity (uuid) for the system structure parent."},
            {"ParentDeviceStructure", "Identity (uuid) for the device structure parent (if the name entry refers to device structure)."},
            {"Index",                 "Index (instance) of the name entry (if the name entry refers to device structure)."},
            {"Description",           "Description (verbose) of the name entry."},
            // above NameElementCommand
            {"SystemStructure",       "Mnemonic path for for the system structure."},
            {"DeviceStructure",       "Mnemonic path for for the device structure."},
            {"Name",                  "Name (verbose) of the name entry."},
            {"Status",                "Status of the name entry."},
            {"Deleted",               "If the name entry is deleted."},
            {"When",                  "Date and time when the name entry was created."},
            {"Who",                   "Name (user) of who created the name entry."},
            {"Comment",               "Comment of the name entry command."} };

    private static final int        STRUCTUREELEMENTCOMMAND_LENGTH  = 6;
    private static final String[][] STRUCTUREELEMENT_HEADER_COMMENT = {
            {"Uuid",                  "Identity (uuid) of the structure entry. Value is created server-side."},
            {"Type",                  "Type of the structure entry. Valid values - SYSTEMGROUP, SYSTEM, SUBSYSTEM, DISCIPLINE, DEVICEGROUP, DEVICETYPE."},
            {"Parent",                "Identity (uuid) for the structure entry parent (if the structure entry has a parent)."},
            {"Mnemonic",              "Mnemonic of the structure entry."},
            {"Ordering",              "Ordering of the structure entry."},
            {"Description",           "Description (verbose) of the structure entry."},
            // above StructureElementCommand
            {"MnemonicPath",          "Mnemonic path of the structure entry."},
            {"Level",                 "Level of the structure entry."},
            {"Status",                "Status of the structure entry."},
            {"Deleted",               "If the structure entry is deleted."},
            {"When",                  "Date and time when the structure entry was created."},
            {"Who",                   "Name (user) of who created the structure entry."},
            {"Comment",               "Comment of the structure entry command."} };

    private static final String ARIAL = "Arial";
    private static final String SHEET = "Entries";
    private static final String TEXT  = "text";

    private static final String FAILED_TO_EXPORT_VALUES_TO_FILE                = "Failed to export values to file";
    private static final String FILE_COULD_NOT_BE_PARSED_FOR_VALUE_AT_ROW_CELL = "File could not be parsed for value at row: {0} cell: {1}";
    private static final String INVALID_SHEET_EXPECTED_SHEET                   = "Invalid sheet. Expected sheet: {0}";
    private static final String INVALID_VALUE_EXPECTED_VALUE                   = "Invalid value: {0} Expected value: {1}";
    private static final String INVALID_VALUE_UNEXPECTED_VALUE                 = "Invalid value: {0} Unexpected value.";

    /**
     * This class is not to be instantiated.
     */
    private ExcelUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Utility method to check if file has Excel format.
     *
     * @param file file
     * @return boolean if file has Excel format
     */
    public static boolean hasExcelFormat(MultipartFile file) {
        return MIME_TYPE_OPENXML_SPREADSHEET.equals(file.getContentType());
    }

    /**
     * Utility method to convert an Excel file to a list of name element commands given how Excel file is to be interpreted.
     *
     * @param is input stream
     * @param nameCommand name command
     * @return list of name elements
     */
    public static List<NameElementCommand> excelToNameElementCommands(InputStream is, NameCommand nameCommand) {
        // rules for conversion
        //     Excel as NameElementCommand accepted
        //     ( Excel as NameElement accepted )
        //     see validateHeaderNameElementCommand

        // NameElementCommand
        //     create -       parentSystemStructure, parentDeviceStructure, index, description
        //	   update - uuid, parentSystemStructure, parentDeviceStructure, index, description
        //	   delete - uuid

        int rowIndex = 0;
        int columnIndex = 0;

        try {
              Workbook workbook = new XSSFWorkbook(is);
              validateSheet(workbook);

              Sheet sheet = workbook.getSheet(SHEET);
              Iterator<Row> rows = sheet.iterator();
              List<NameElementCommand> list = Lists.newArrayList();
              boolean hasCell = false;

              // iterate over rows and columns (cells)
              while (rows.hasNext()) {
                  Row row = rows.next();
                  Iterator<Cell> cells = row.iterator();
                  NameElementCommand nameElementCommand = new NameElementCommand();

                  while (cells.hasNext()) {
                      hasCell = true;
                      Cell cell = cells.next();
                      rowIndex = cell.getRowIndex();
                      columnIndex = cell.getColumnIndex();

                      // ensure header row with syntax
                      if (rowIndex == 0) {
                          validateHeaderNameElementCommand(cell);
                          continue;
                      }

                      String value = columnIndex < NAMEELEMENTCOMMAND_LENGTH ? cell.getStringCellValue() : null;
                      switch (columnIndex) {
                      case 0:
                          nameElementCommand.setUuid(!StringUtils.isEmpty(value) ? UUID.fromString(value) : null);
                          break;
                      case 1:
                          nameElementCommand.setParentSystemStructure(!StringUtils.isEmpty(value) ? UUID.fromString(value) : null);
                          break;
                      case 2:
                          nameElementCommand.setParentDeviceStructure(!StringUtils.isEmpty(value) ? UUID.fromString(value) : null);
                          break;
                      case 3:
                          nameElementCommand.setIndex(value);
                          break;
                      case 4:
                          nameElementCommand.setDescription(value);
                          break;
                      default:
                          if (columnIndex > NAMEELEMENT_HEADER_COMMENT.length - 1) {
                              throw new IllegalArgumentException(
                                      MessageFormat.format(INVALID_VALUE_UNEXPECTED_VALUE, value));
                          }
                          break;
                      }
                  }
                  // not add header row
                  if (rowIndex > 0) {
                      list.add(nameElementCommand);
                  }
              }

              if (!hasCell) {
                  throw new ServiceException(
                          MessageFormat.format(FILE_COULD_NOT_BE_PARSED_FOR_VALUE_AT_ROW_CELL, rowIndex, columnIndex),
                          null, null, null);
              }

              workbook.close();
              LOGGER.log(Level.FINE,
                      () -> MessageFormat.format(
                              TextUtil.DESCRIPTION_NUMBER_ELEMENTS,
                              "Excel to name element commands",
                              list.size()));
              return list;
        } catch (IllegalArgumentException | IOException e) {
            throw new ServiceException(
                    MessageFormat.format(FILE_COULD_NOT_BE_PARSED_FOR_VALUE_AT_ROW_CELL, rowIndex, columnIndex),
                    e.getMessage(), null, e);
        }
    }

    /**
     * Utility method to convert an Excel file to a list of structure element commands given how Excel file is to be interpreted..
     *
     * @param is input stream
     * @param structureCommand structure command
     * @return list of structure elements
     */
    public static List<StructureElementCommand> excelToStructureElementCommands(InputStream is, StructureCommand structureCommand) {
        // rules for conversion
        //     Excel as StructureElementCommand accepted
        //     ( Excel as StructureElement accepted )
        //     see validateHeaderStructureElementCommand

        // StructureElementCommand
        //     create -       type, parent, mnemonic, ordering, description
        //     update - uuid, type, parent, mnemonic, ordering, description
        //     delete - uuid, type

        int rowIndex = 0;
        int columnIndex = 0;

        try {
              Workbook workbook = new XSSFWorkbook(is);
              validateSheet(workbook);

              Sheet sheet = workbook.getSheet(SHEET);
              Iterator<Row> rows = sheet.iterator();
              List<StructureElementCommand> list = Lists.newArrayList();
              boolean hasCell = false;

              // iterate over rows and columns (cells)
              while (rows.hasNext()) {
                  Row row = rows.next();
                  Iterator<Cell> cells = row.iterator();
                  StructureElementCommand structureElementCommand = new StructureElementCommand();

                  while (cells.hasNext()) {
                      hasCell = true;
                      Cell cell = cells.next();
                      rowIndex = cell.getRowIndex();
                      columnIndex = cell.getColumnIndex();

                      // ensure header row with syntax
                      if (rowIndex == 0) {
                          validateHeaderStructureElementCommand(cell);
                          continue;
                      }

                      String value = columnIndex < STRUCTUREELEMENTCOMMAND_LENGTH ? cell.getStringCellValue() : null;
                      switch (columnIndex) {
                      case 0:
                          structureElementCommand.setUuid(!StringUtils.isEmpty(value) ? UUID.fromString(value) : null);
                          break;
                      case 1:
                          structureElementCommand.setType(Type.valueOf(value));
                          break;
                      case 2:
                          structureElementCommand.setParent(!StringUtils.isEmpty(value) ? UUID.fromString(value) : null);
                          break;
                      case 3:
                          structureElementCommand.setMnemonic(value);
                          break;
                      case 4:
                          structureElementCommand.setOrdering(!StringUtils.isEmpty(value) ? Integer.parseInt(value) : null);
                          break;
                      case 5:
                          structureElementCommand.setDescription(value);
                          break;
                      default:
                          if (columnIndex > STRUCTUREELEMENT_HEADER_COMMENT.length - 1) {
                              throw new IllegalArgumentException(
                                      MessageFormat.format(INVALID_VALUE_UNEXPECTED_VALUE, value));
                          }
                          break;
                      }

                  }
                  // not add header row
                  if (rowIndex > 0) {
                      list.add(structureElementCommand);
                  }
              }

              if (!hasCell) {
                  throw new ServiceException(
                          MessageFormat.format(FILE_COULD_NOT_BE_PARSED_FOR_VALUE_AT_ROW_CELL, rowIndex, columnIndex),
                          null, null, null);
              }

              workbook.close();
              LOGGER.log(Level.FINE,
                      () -> MessageFormat.format(
                              TextUtil.DESCRIPTION_NUMBER_ELEMENTS,
                              "Excel to structure element commands",
                              list.size()));
              return list;
        } catch (IllegalArgumentException | IOException e) {
            throw new ServiceException(
                    MessageFormat.format(FILE_COULD_NOT_BE_PARSED_FOR_VALUE_AT_ROW_CELL, rowIndex, columnIndex),
                    e.getMessage(), null, e);
        }
    }

    /**
     * Validate that workbook has sheet with expected name.
     *
     * @param workbook workbook
     */
    private static void validateSheet(Workbook workbook) {
        if (workbook.getSheet(SHEET) == null) {
            throw new IllegalArgumentException(MessageFormat.format(INVALID_SHEET_EXPECTED_SHEET, SHEET));
        }
    }

    /**
     * Validate that cell is a proper header cell for a name element.
     *
     * @param cell cell
     */
    private static void validateHeaderNameElementCommand(Cell cell) {
        // validate that columns are NameElementCommand
        //     nothing less or more
        // have condition to allow NameElementCommand but not NameElement
        //     cell.getColumnIndex() >= NAMEELEMENTCOMMAND_LENGTH
        //         throw new IllegalArgumentException(
        //             MessageFormat.format(INVALID_VALUE_UNEXPECTED_VALUE, cell.getStringCellValue()))

        if (cell.getColumnIndex() < NAMEELEMENTCOMMAND_LENGTH
                && !NAMEELEMENT_HEADER_COMMENT[cell.getColumnIndex()][0].equals(cell.getStringCellValue())) {
            throw new IllegalArgumentException(
                    MessageFormat.format(
                            INVALID_VALUE_EXPECTED_VALUE,
                            cell.getStringCellValue(),
                            NAMEELEMENT_HEADER_COMMENT[cell.getColumnIndex()][0]));
        }
    }

    /**
     * Validate that cell is a proper header cell for a structure element.
     *
     * @param cell cell
     */
    private static void validateHeaderStructureElementCommand(Cell cell) {
        // validate that columns are StructureElementCommand
        //     nothing less or more
        // have condition to allow StructureElementCommand but not StructureElement
        //     cell.getColumnIndex() >= STRUCTUREELEMENTCOMMAND_LENGTH
        //         throw new IllegalArgumentException(
        //             MessageFormat.format(INVALID_VALUE_UNEXPECTED_VALUE, cell.getStringCellValue()))

        if (cell.getColumnIndex() < STRUCTUREELEMENTCOMMAND_LENGTH
                && !STRUCTUREELEMENT_HEADER_COMMENT[cell.getColumnIndex()][0].equals(cell.getStringCellValue())) {
            throw new IllegalArgumentException(
                    MessageFormat.format(
                            INVALID_VALUE_EXPECTED_VALUE,
                            cell.getStringCellValue(),
                            STRUCTUREELEMENT_HEADER_COMMENT[cell.getColumnIndex()][0]));
        }
    }

    /**
     * Utility method to convert a list of name elements to an Excel file.
     *
     * @param nameElements name elements
     * @return Excel file
     */
    public static ByteArrayInputStream nameElementsToExcel(List<NameElement> nameElements) {
        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS,
                        "Name elements to Excel",
                        nameElements.size()));
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            // prepare
            Drawing<?> drawing = sheet.createDrawingPatriarch();
            CreationHelper factory = workbook.getCreationHelper();
            ClientAnchor anchor = factory.createClientAnchor();

            // format, font, style
            DataFormat format = workbook.createDataFormat();
            short textFormat = format.getFormat(TEXT);
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short)12);
            headerFont.setFontName(ARIAL);
            Font dataFont = workbook.createFont();
            dataFont.setBold(false);
            dataFont.setFontHeightInPoints((short)12);
            dataFont.setFontName(ARIAL);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setDataFormat(textFormat);
            headerCellStyle.setFont(headerFont);
            CellStyle dataCellStyle = workbook.createCellStyle();
            dataCellStyle.setDataFormat(textFormat);
            dataCellStyle.setFont(dataFont);

            // header
            Row headerRow = sheet.createRow(0);
            for (int columnIndex = 0; columnIndex < NAMEELEMENT_HEADER_COMMENT.length; columnIndex++) {
                Cell cell = headerRow.createCell(columnIndex);

                // value
                cell.setCellValue(NAMEELEMENT_HEADER_COMMENT[columnIndex][0]);

                // width
                sheet.setColumnWidth(columnIndex, SHEET_COLUMN_WIDTH);

                // style
                cell.setCellStyle(headerCellStyle);

                // comment
                anchor.setRow1(headerRow.getRowNum());
                anchor.setRow2(headerRow.getRowNum()+3);
                anchor.setCol1(cell.getColumnIndex());
                anchor.setCol2(cell.getColumnIndex()+1);
                Comment cellComment = drawing.createCellComment(anchor);
                RichTextString richTextString = factory.createRichTextString(NAMEELEMENT_HEADER_COMMENT[columnIndex][1]);
                cellComment.setString(richTextString);
                cellComment.setAuthor(TextUtil.NAMING);
                cell.setCellComment(cellComment);
            }

            // data
            int rowIndex = 1;
            for (NameElement nameElement : nameElements) {
                Row row = sheet.createRow(rowIndex++);
                for (int columnIndex = 0; columnIndex < NAMEELEMENT_HEADER_COMMENT.length; columnIndex++) {
                    Cell cell = row.createCell(columnIndex);
                    cell.setCellStyle(dataCellStyle);
                    switch (columnIndex) {
                    case 0:
                        cell.setCellValue(nameElement.getUuid() != null ? nameElement.getUuid().toString() : null);
                        break;
                    case 1:
                        cell.setCellValue(nameElement.getParentSystemStructure() != null ? nameElement.getParentSystemStructure().toString() : null);
                        break;
                    case 2:
                        cell.setCellValue(nameElement.getParentDeviceStructure() != null ? nameElement.getParentDeviceStructure().toString() : null);
                        break;
                    case 3:
                        cell.setCellValue(nameElement.getIndex());
                        break;
                    case 4:
                        cell.setCellValue(nameElement.getDescription());
                        break;
                    case 5:
                        cell.setCellValue(nameElement.getSystemStructure());
                        break;
                    case 6:
                        cell.setCellValue(nameElement.getDeviceStructure());
                        break;
                    case 7:
                        cell.setCellValue(nameElement.getName());
                        break;
                    case 8:
                        cell.setCellValue(nameElement.getStatus() != null ? nameElement.getStatus().toString() : null);
                        break;
                    case 9:
                        cell.setCellValue(nameElement.isDeleted());
                        break;
                    case 10:
                        cell.setCellValue(DateFormat.getDateTimeInstance().format(nameElement.getWhen()));
                        break;
                    case 11:
                        cell.setCellValue(nameElement.getWho());
                        break;
                    case 12:
                        cell.setCellValue(nameElement.getComment());
                        break;
                    default:
                        break;
                    }
                }
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());

        } catch (IOException e) {
            throw new ServiceException(FAILED_TO_EXPORT_VALUES_TO_FILE, e.getMessage(), null, e);
        }
    }

    /**
     * Utility method to convert a list of structure elements to an Excel file.
     *
     * @param structureElements structure elements
     * @return Excel file
     */
    public static ByteArrayInputStream structureElementsToExcel(List<StructureElement> structureElements) {
        LOGGER.log(Level.FINE,
                () -> MessageFormat.format(
                        TextUtil.DESCRIPTION_NUMBER_ELEMENTS,
                        "Structure elements to Excel",
                        structureElements.size()));
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            // prepare
            Drawing<?> drawing = sheet.createDrawingPatriarch();
            CreationHelper factory = workbook.getCreationHelper();
            ClientAnchor anchor = factory.createClientAnchor();

            // format, font, style
            DataFormat format = workbook.createDataFormat();
            short textFormat = format.getFormat(TEXT);
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short)12);
            headerFont.setFontName(ARIAL);
            Font dataFont = workbook.createFont();
            dataFont.setBold(false);
            dataFont.setFontHeightInPoints((short)12);
            dataFont.setFontName(ARIAL);
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setDataFormat(textFormat);
            headerCellStyle.setFont(headerFont);
            CellStyle dataCellStyle = workbook.createCellStyle();
            dataCellStyle.setDataFormat(textFormat);
            dataCellStyle.setFont(dataFont);

            // header
            Row headerRow = sheet.createRow(0);
            for (int columnIndex = 0; columnIndex < STRUCTUREELEMENT_HEADER_COMMENT.length; columnIndex++) {
              Cell cell = headerRow.createCell(columnIndex);

              // value
              cell.setCellValue(STRUCTUREELEMENT_HEADER_COMMENT[columnIndex][0]);

              // width
              sheet.setColumnWidth(columnIndex, SHEET_COLUMN_WIDTH);

              // style
              cell.setCellStyle(headerCellStyle);

              // comment
              anchor.setRow1(headerRow.getRowNum());
              anchor.setRow2(headerRow.getRowNum()+3);
              anchor.setCol1(cell.getColumnIndex());
              anchor.setCol2(cell.getColumnIndex()+1);
              Comment cellComment = drawing.createCellComment(anchor);
              RichTextString richTextString = factory.createRichTextString(STRUCTUREELEMENT_HEADER_COMMENT[columnIndex][1]);
              cellComment.setString(richTextString);
              cellComment.setAuthor(TextUtil.NAMING);
              cell.setCellComment(cellComment);
            }

            // data
            int rowIndex = 1;
            for (StructureElement structureElement : structureElements) {
                Row row = sheet.createRow(rowIndex++);
                for (int columnIndex = 0; columnIndex < NAMEELEMENT_HEADER_COMMENT.length; columnIndex++) {
                    Cell cell = row.createCell(columnIndex);
                    cell.setCellStyle(dataCellStyle);
                    switch (columnIndex) {
                    case 0:
                        cell.setCellValue(structureElement.getUuid() != null ? structureElement.getUuid().toString() : null);
                        break;
                    case 1:
                        cell.setCellValue(structureElement.getType() != null ? structureElement.getType().toString() : null);
                        break;
                    case 2:
                        cell.setCellValue(structureElement.getParent() != null ? structureElement.getParent().toString() : null);
                        break;
                    case 3:
                        cell.setCellValue(structureElement.getMnemonic());
                        break;
                    case 4:
                        cell.setCellValue(structureElement.getOrdering() != null ? structureElement.getOrdering().toString() : null);
                        break;
                    case 5:
                        cell.setCellValue(structureElement.getDescription());
                        break;
                    case 6:
                        cell.setCellValue(structureElement.getMnemonicPath());
                        break;
                    case 7:
                        cell.setCellValue(structureElement.getLevel());
                        break;
                    case 8:
                        cell.setCellValue(structureElement.getStatus() != null ? structureElement.getStatus().toString() : null);
                        break;
                    case 9:
                        cell.setCellValue(structureElement.isDeleted());
                        break;
                    case 10:
                        cell.setCellValue(DateFormat.getDateTimeInstance().format(structureElement.getWhen()));
                        break;
                    case 11:
                        cell.setCellValue(structureElement.getWho());
                        break;
                    case 12:
                        cell.setCellValue(structureElement.getComment());
                        break;
                    default:
                        break;
                    }
                }
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());

        } catch (IOException e) {
            throw new ServiceException(FAILED_TO_EXPORT_VALUES_TO_FILE, e.getMessage(), null, e);
        }
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.old;

import java.text.SimpleDateFormat;
import java.util.UUID;

import org.openepics.names.old.business.NameRevisionStatus;
import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.openepics.names.repository.model.wip.WipDeviceType;
import org.openepics.names.repository.model.wip.WipDiscipline;
import org.openepics.names.repository.model.wip.WipName;
import org.openepics.names.repository.model.wip.WipSubsystem;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.openepics.names.rest.beans.old.HistoryElement;

/**
 * Utility class to assist in populating history elements based on repository content.
 *
 * @author Lars Johansson
 */
public class HistoryElementUtil {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * This class is not to be instantiated.
     */
    private HistoryElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return history element for system group with focus on processed.
     *
     * @param systemGroup system group
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipSystemGroup systemGroup) {
        if (systemGroup == null) {
            return null;
        }

        return getHistoryElement(
                systemGroup.getUuid(),
                systemGroup.getMnemonic(),
                systemGroup.getDescription(),
                systemGroup.getStatus() != null ? systemGroup.getStatus().toString() : "",
                systemGroup.getProcessed() != null ? SDF.format(systemGroup.getProcessed()) : "",
                systemGroup.getProcessedBy(),
                systemGroup.getProcessedComment());
    }
    /**
     * Populate and return history element for system group with focus on requested.
     *
     * @param systemGroup system group
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipSystemGroup systemGroup) {
        if (systemGroup == null) {
            return null;
        }

        return getHistoryElement(
                systemGroup.getUuid(),
                systemGroup.getMnemonic(),
                systemGroup.getDescription(),
                NameRevisionStatus.PENDING.name(),
                systemGroup.getRequested() != null ? SDF.format(systemGroup.getRequested()) : "",
                systemGroup.getRequestedBy(),
                systemGroup.getRequestedComment());
    }
    /**
     * Populate and return history element for system with focus on processed.
     *
     * @param system system
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipSystem system) {
        if (system == null) {
            return null;
        }

        return getHistoryElement(
                system.getUuid(),
                system.getMnemonic(),
                system.getDescription(),
                system.getStatus() != null ? system.getStatus().toString() : "",
                system.getProcessed() != null ? SDF.format(system.getProcessed()) : "",
                system.getProcessedBy(),
                system.getProcessedComment());
    }
    /**
     * Populate and return history element for system with focus on requested.
     *
     * @param system system
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipSystem system) {
        if (system == null) {
            return null;
        }

        return getHistoryElement(
                system.getUuid(),
                system.getMnemonic(),
                system.getDescription(),
                NameRevisionStatus.PENDING.name(),
                system.getRequested() != null ? SDF.format(system.getRequested()) : "",
                system.getRequestedBy(),
                system.getRequestedComment());
    }
    /**
     * Populate and return history element for subsystem with focus on processed.
     *
     * @param subsystem subsystem
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipSubsystem subsystem) {
        if (subsystem == null) {
            return null;
        }

        return getHistoryElement(
                subsystem.getUuid(),
                subsystem.getMnemonic(),
                subsystem.getDescription(),
                subsystem.getStatus() != null ? subsystem.getStatus().toString() : "",
                subsystem.getProcessed() != null ? SDF.format(subsystem.getProcessed()) : "",
                subsystem.getProcessedBy(),
                subsystem.getProcessedComment());
    }
    /**
     * Populate and return history element for subsystem with focus on requested.
     *
     * @param subsystem subsystem
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipSubsystem subsystem) {
        if (subsystem == null) {
            return null;
        }

        return getHistoryElement(
                subsystem.getUuid(),
                subsystem.getMnemonic(),
                subsystem.getDescription(),
                NameRevisionStatus.PENDING.name(),
                subsystem.getRequested() != null ? SDF.format(subsystem.getRequested()) : "",
                subsystem.getRequestedBy(),
                subsystem.getRequestedComment());
    }

    /**
     * Populate and return history element for discipline with focus on processed.
     *
     * @param discipline discipline
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipDiscipline discipline) {
        if (discipline == null) {
            return null;
        }

        return getHistoryElement(
                discipline.getUuid(),
                discipline.getMnemonic(),
                discipline.getDescription(),
                discipline.getStatus() != null ? discipline.getStatus().toString() : "",
                discipline.getProcessed() != null ? SDF.format(discipline.getProcessed()) : "",
                discipline.getProcessedBy(),
                discipline.getProcessedComment());
    }
    /**
     * Populate and return history element for discipline with focus on requested.
     *
     * @param discipline discipline
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipDiscipline discipline) {
        if (discipline == null) {
            return null;
        }

        return getHistoryElement(
                discipline.getUuid(),
                discipline.getMnemonic(),
                discipline.getDescription(),
                NameRevisionStatus.PENDING.name(),
                discipline.getRequested() != null ? SDF.format(discipline.getRequested()) : "",
                discipline.getRequestedBy(),
                discipline.getRequestedComment());
    }
    /**
     * Populate and return history element for device group with focus on processed.
     *
     * @param deviceGroup device group
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipDeviceGroup deviceGroup) {
        if (deviceGroup == null) {
            return null;
        }

        return getHistoryElement(
                deviceGroup.getUuid(),
                deviceGroup.getMnemonic(),
                deviceGroup.getDescription(),
                deviceGroup.getStatus() != null ? deviceGroup.getStatus().toString() : "",
                deviceGroup.getProcessed() != null ? SDF.format(deviceGroup.getProcessed()) : "",
                deviceGroup.getProcessedBy(),
                deviceGroup.getProcessedComment());
    }
    /**
     * Populate and return history element for device group with focus on requested.
     *
     * @param deviceGroup device group
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipDeviceGroup deviceGroup) {
        if (deviceGroup == null) {
            return null;
        }

        return getHistoryElement(
                deviceGroup.getUuid(),
                deviceGroup.getMnemonic(),
                deviceGroup.getDescription(),
                NameRevisionStatus.PENDING.name(),
                deviceGroup.getRequested() != null ? SDF.format(deviceGroup.getRequested()) : "",
                deviceGroup.getRequestedBy(),
                deviceGroup.getRequestedComment());
    }
    /**
     * Populate and return history element for device type with focus on processed.
     *
     * @param deviceType device type
     * @return history element
     */
    public static HistoryElement getHistoryElementProcessed(WipDeviceType deviceType) {
        if (deviceType == null) {
            return null;
        }

        return getHistoryElement(
                deviceType.getUuid(),
                deviceType.getMnemonic(),
                deviceType.getDescription(),
                deviceType.getStatus() != null ? deviceType.getStatus().toString() : "",
                deviceType.getProcessed() != null ? SDF.format(deviceType.getProcessed()) : "",
                deviceType.getProcessedBy(),
                deviceType.getProcessedComment());
    }
    /**
     * Populate and return history element for device type with focus on requested.
     *
     * @param deviceType device type
     * @return history element
     */
    public static HistoryElement getHistoryElementRequested(WipDeviceType deviceType) {
        if (deviceType == null) {
            return null;
        }

        return getHistoryElement(
                deviceType.getUuid(),
                deviceType.getMnemonic(),
                deviceType.getDescription(),
                NameRevisionStatus.PENDING.name(),
                deviceType.getRequested() != null ? SDF.format(deviceType.getRequested()) : "",
                deviceType.getRequestedBy(),
                deviceType.getRequestedComment());
    }

    /**
     * Populate and return history element.
     *
     * @param uuid uuid
     * @param mnemonic mnemonic
     * @param description description
     * @param status status
     * @param date date
     * @param user user
     * @param message message
     * @return history element
     */
    private static HistoryElement getHistoryElement(UUID uuid, String mnemonic, String description, String status, String date, String user, String message) {
        return new HistoryElement(uuid, null, mnemonic, description, status, date, user, message);
    }

    /**
     * Populate and return structure element for name.
     *
     * @param name name
     * @return history element
     */
    public static HistoryElement getHistoryElement(WipName name) {
        if (name == null) {
            return null;
        }

        return new HistoryElement(
                name.getUuid(),
                name.getConventionName(),
                name.getInstanceIndex(),
                name.getDescription(),
                name.getStatus() != null ? name.getStatus().toString() : "",
                name.getRequested() != null ? SDF.format(name.getRequested()) : "",
                name.getRequestedBy(),
                name.getRequestedComment());
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.old;

import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.openepics.names.repository.model.wip.WipDeviceType;
import org.openepics.names.repository.model.wip.WipDiscipline;
import org.openepics.names.repository.model.wip.WipName;
import org.openepics.names.repository.model.wip.WipSubsystem;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.openepics.names.rest.beans.old.DeviceNameElement;
import org.openepics.names.util.wip.HolderIWipRepositories;
import org.openepics.names.util.wip.HolderWipStructures;

/**
 * Utility class to assist in populating device name elements based on repository content.
 *
 * @author Lars Johansson
 */
public class DeviceNameElementUtil {

    protected static final String ACTIVE   = "ACTIVE";
    protected static final String DELETED  = "DELETED";

    /**
     * This class is not to be instantiated.
     */
    private DeviceNameElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Populate and return device name element for name.
     *
     * @param name name
     * @param holderStructures holder of system and device structure content
     * @return device name element
     */
    public static DeviceNameElement getDeviceNameElement(WipName name, HolderWipStructures holderStructures) {
        if (name == null) {
            return null;
        }

        // using holder of system and device structure content
        //     for performance reasons to speed up preparation of what is to be returned

        // find out how to populate return element for system structure, device structure
        int levelSystemStructure = DeviceNameElementUtil.getLevelSystemStructure(name);
        int levelDeviceStructure = DeviceNameElementUtil.getLevelDeviceStructure(name);

        // levelSystemStructure -1 ---> error
        // levelDeviceStructure -1 ---> error

        final DeviceNameElement deviceNameElement = new DeviceNameElement();

        // populate return element for system structure
        switch (levelSystemStructure) {
        case 3: {
            WipSubsystem   subsystem   = holderStructures.findSubsystemByUuid(name.getSubsystemUuid());
            WipSystem      system      = holderStructures.findSystemByUuid(subsystem.getParentUuid());
            WipSystemGroup systemGroup = holderStructures.findSystemGroupByUuid(system.getParentUuid());

            deviceNameElement.setSubsystem(subsystem.getMnemonic());
            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 2: {
            WipSystem      system      = holderStructures.findSystemByUuid(name.getSystemUuid());
            WipSystemGroup systemGroup = holderStructures.findSystemGroupByUuid(system.getParentUuid());

            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 1: {
            WipSystemGroup systemGroup = holderStructures.findSystemGroupByUuid(name.getSystemGroupUuid());

            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        // populate return element for device structure
        switch (levelDeviceStructure) {
        case 3: {
            WipDeviceType  deviceType  = holderStructures.findDeviceTypeByUuid(name.getDeviceTypeUuid());
            WipDeviceGroup deviceGroup = holderStructures.findDeviceGroupByUuid(deviceType.getParentUuid());
            WipDiscipline  discipline  = holderStructures.findDisciplineByUuid(deviceGroup.getParentUuid());

            deviceNameElement.setDeviceType(deviceType.getMnemonic());
            deviceNameElement.setDiscipline(discipline.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        deviceNameElement.setUuid(name.getUuid());
        deviceNameElement.setInstanceIndex(name.getInstanceIndex());
        deviceNameElement.setName(name.getConventionName());
        deviceNameElement.setDescription(name.getDescription());
        deviceNameElement.setStatus(name.isDeleted() ? DELETED : ACTIVE);

        return deviceNameElement;
    }

    /**
     * Populate and return device name element for name.
     *
     * @param name name
     * @param holderIWipRepositories holder of references to repositories
     * @return device name element
     */
    public static DeviceNameElement getDeviceNameElement(WipName name, HolderIWipRepositories holderIWipRepositories) {
        if (name == null) {
            return null;
        }

        // find out how to populate return element for system structure, device structure
        int levelSystemStructure = DeviceNameElementUtil.getLevelSystemStructure(name);
        int levelDeviceStructure = DeviceNameElementUtil.getLevelDeviceStructure(name);

        // levelSystemStructure -1 ---> error
        // levelDeviceStructure -1 ---> error

        final DeviceNameElement deviceNameElement = new DeviceNameElement();

        // populate return element for system structure
        switch (levelSystemStructure) {
        case 3: {
            WipSubsystem   subsystem   = holderIWipRepositories.subsystemRepository().findLatestByUuid(name.getSubsystemUuid().toString());
            WipSystem      system      = holderIWipRepositories.systemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
            WipSystemGroup systemGroup = holderIWipRepositories.systemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            deviceNameElement.setSubsystem(subsystem.getMnemonic());
            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 2: {
            WipSystem      system      = holderIWipRepositories.systemRepository().findLatestByUuid(name.getSystemUuid().toString());
            WipSystemGroup systemGroup = holderIWipRepositories.systemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            deviceNameElement.setSystem(system.getMnemonic());
            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        case 1: {
            WipSystemGroup systemGroup = holderIWipRepositories.systemGroupRepository().findLatestByUuid(name.getSystemGroupUuid().toString());

            deviceNameElement.setSystemGroup(systemGroup.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        // populate return element for device structure
        switch (levelDeviceStructure) {
        case 3: {
            WipDeviceType  deviceType  = holderIWipRepositories.deviceTypeRepository().findLatestByUuid(name.getDeviceTypeUuid().toString());
            WipDeviceGroup deviceGroup = holderIWipRepositories.deviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
            WipDiscipline  discipline  = holderIWipRepositories.disciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());

            deviceNameElement.setDeviceType(deviceType.getMnemonic());
            deviceNameElement.setDiscipline(discipline.getMnemonic());
            break;
        }
        default:
            // error
            break;
        }

        deviceNameElement.setUuid(name.getUuid());
        deviceNameElement.setInstanceIndex(name.getInstanceIndex());
        deviceNameElement.setName(name.getConventionName());
        deviceNameElement.setDescription(name.getDescription());
        deviceNameElement.setStatus(name.isDeleted() ? DELETED : ACTIVE);

        return deviceNameElement;
    }

    /**
     * Return level of system structure for name.
     * <ul>
     * <li>1  <--> system group</li>
     * <li>2  <--> system</li>
     * <li>3  <--> subsystem</li>
     * <li>-1 <--> invalid</li>
     * </ul>
     *
     * @param name name content
     * @return level of system structure
     */
    private static int getLevelSystemStructure(WipName name) {
        if (name == null) {
            return -1;
        }

        int count = 0;
        if (name.getSystemGroupUuid() != null) {
            count++;
        }
        if (name.getSystemUuid() != null) {
            count++;
        }
        if (name.getSubsystemUuid() != null) {
            count++;
        }
        if (count != 1) {
            return -1;
        }

        return name.getSubsystemUuid() != null
                ? 3
                : name.getSystemUuid() != null
                        ? 2
                        : name.getSystemGroupUuid() != null
                                ? 1
                                : -1;
    }

    /**
     * Return level of device structure for name.
     * <ul>
     * <li>3  <--> device type</li>
     * <li>-1 <--> invalid</li>
     * </ul>
     *
     * @param name content
     * @return level of device structure
     */
    private static int getLevelDeviceStructure(WipName name) {
        if (name == null) {
            return -1;
        }

        return name.getDeviceTypeUuid() != null
                ? 3
                : -1;
    }

}

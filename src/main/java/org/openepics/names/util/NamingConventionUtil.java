/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.Discipline;

/**
 * Utility class to provide split of device name in naming convention into name parts
 * for system structure, device structure, instance index and process variable.
 *
 * <p>
 * Note delimiter characters, in and between, system structure and device structure and process variable.
 * <ul>
 * <li> -
 * <li> :
 * </ul>
 * </p>
 *
 * <p>
 * Performance is important!
 * <br/>
 * <ul>
 * <li> indexOf somewhat faster than split
 * <li> not noticeable unless tens of thousands of calls made in short time
 * </ul>
 *
 * @author Lars Johansson
 *
 * @see NamingConvention
 */
public class NamingConventionUtil {

    // Note
    //
    //     ====================================================================================================
    //     This class handles names in naming convention, which are convention name and process variable name.
    //     This class is to be able to handle past and present names.
    //     ====================================================================================================
    //     ESS Naming Convention
    //         convention name = system structure
    //                         = system structure:device structure-instance index
    //         ------------------------------------------------------------
    //         system structure               part of convention name
    //         device structure        may be part of convention name
    //         instance index          may be part of convention name
    //         ------------------------------------------------------------
    //         property                   not part of convention name (!)
    //         pv name = name:property
    //         ------------------------------------------------------------
    //         system structure
    //             system group    Syg
    //             system          Sys
    //             subsystem       Sub
    //         device structure
    //             discipline      Dis
    //             device group
    //             device type     Dev
    //         instance index      Idx
    //         property            Prop
    //         ------------------------------------------------------------
    //         system group   optional
    //         device group   not part of name
    //         instance index optional
    //     ====================================================================================================
    //     Kind of names handled - naming convention
    //         ------------------------------------------------------------
    //         (1) able to handle
    //         ------------------------------------------------------------
    //             Syg
    //             Sys
    //             Sys-Sub
    //             Syg:Dis-Dev-Idx
    //             Sys:Dis-Dev-Idx
    //             Sys-Sub:Dis-Dev-Idx
    //         ------------------------------------------------------------
    //         (2) able to handle - old naming / naming convention - such names may exist
    //         ------------------------------------------------------------
    //             Sys-Sys:Dis-Dev
    //             Syg-Sys-Sys:Dis-Dev
    //             Syg-Sys-Sys:Dis-Dev-Idx
    //         ------------------------------------------------------------
    //         (3) able to handle property with (1) and (2)
    //         ------------------------------------------------------------
    //     ====================================================================================================
    //     delimiters
    //         :
    //         -
    //         ------------------------------------------------------------
    //         1 or 2 occurrence of : expected
    //             1 convention name
    //             2 pv name
    //     ====================================================================================================

    // delimiter
    public static final String DELIMITER_EXTRA = ":";
    public static final String DELIMITER_INTRA = "-";

    // p&id - pipeline & instrumentation diagram
    public static final String DISCIPLINE_P_ID       = "P&ID";
    public static final String DISCIPLINE_SCIENTIFIC = "Scientific";

    private static final String[] DISCIPLINES_P_ID           = {"Cryo", "EMR", "HVAC", "Proc", "SC", "Vac", "WtrC"};
    private static final String[] MNEMONIC_PATH_P_ID_NUMERIC = {"SC-IOC"};

    private static final String   EMPTY = "";
    private static final String[] EMPTY_ARRAY = new String[] {};

    private static final String PV_NAME_VALIDATION_PROPERTY_CHARACTERS_NOT_ALLOWED = "!@$%^&*()+={}[]|\\:;'\"<>,.?/~`";

    /**
     * This class is not to be instantiated.
     */
    private NamingConventionUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return string array with disciplines for p & id (pipeline & instrumentation diagram).
     *
     * @return string array with disciplines for p & id (pipeline & instrumentation diagram)
     */
    public static String[] getDisciplinesPID() {
        return Arrays.copyOf(DISCIPLINES_P_ID, DISCIPLINES_P_ID.length);
    }

    /**
     * Return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric.
     *
     * @return string array with mnemonic paths for p & id (pipeline & instrumentation diagram) numeric
     */
    public static String[] getMnemonicPathsPIDNumeric() {
        return Arrays.copyOf(MNEMONIC_PATH_P_ID_NUMERIC, MNEMONIC_PATH_P_ID_NUMERIC.length);
    }

    /**
     * Extract information about name.
     *
     * @param name convention name or process variable name
     * @return name information record
     */
    public static NameInformation extractNameInformation(String name) {
        return new NameInformation(
            name,
            extractConventionName(name),
            extractSystemGroup(name),
            extractSystem(name),
            extractSubsystem(name),
            extractDiscipline(name),
            extractDeviceType(name),
            extractInstanceIndex(name),
            extractProperty(name),
            extractMnemonicPathSystemStructure(name),
            extractMnemonicPathDeviceStructure(name),
            getInstanceIndexStyle(name),
            isDisciplinePID(extractDiscipline(name)),
            isMnemonicPathDeviceStructurePIDNumeric(extractMnemonicPathDeviceStructure(name)),
            isOffsite(name),
            isConventionName(name),
            isProcessVariableName(name)
        );
    }

    /**
     * Return information about process variable name, in particular about validation.
     *
     * @param name convention name or process variable name
     * @return process variable name information
     */
    public static ProcessVariableNameInformation extractProcessVariableNameInformation(String name) {
        // rules
        //     name
        //         not empty
        //     process variable name
        //         property not empty
        //     name length
        //         > 60
        //     property min length
        //         > 0
        //         < 4
        //     property max length
        //         > 25
        //         > 20
        //     property characters
        //         PV_CHARACTERS_NOT_ALLOWED
        //         #
        //         digit
        //         upper case
        //
        // note
        //     focus is on property of process variable name - NOT on convention name (!)
        //     separate checks for property for length and illegal characters
        //     may return multiple messages
        //         only one message about property length
        //     potentially valid if one message only and about internal process variable property

        List<String> messages = Lists.newArrayList();

        if (nullIfEmpty(name) == null) {
            messages.add(TextUtil.PV_NAME_VALIDATION_NAME_IS_EMPTY);
        } else if (!isProcessVariableName(name)) {
            messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_IS_EMPTY);
        } else {
            if (name.length() > 60) {
                messages.add(TextUtil.PV_NAME_VALIDATION_NAME_CONTAINS_MORE_THAN_60_CHARACTERS);
            }

            String property = extractProperty(name);
            boolean hasLengthMessage = false;
            int length = property.length();
            if (!hasLengthMessage && length > 25) {
                messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_CONTAINS_MORE_THAN_25_CHARACTERS);
                hasLengthMessage = true;
            }
            if (!hasLengthMessage && length > 20) {
                if (!((property.endsWith("-R") || property.endsWith("-S")) && property.length() <= 22)
                        && !(property.endsWith("-RB") && length <= 23)) {
                    messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_CONTAINS_MORE_THAN_20_CHARACTERS);
                    hasLengthMessage = true;
                }
            }
            if (!hasLengthMessage && length > 1 && length < 4 && !"Pwr".equals(property) ) {
                messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_CONTAINS_LESS_THAN_4_CHARACTERS);
                hasLengthMessage = true;
            }
            for (int i=0; i<PV_NAME_VALIDATION_PROPERTY_CHARACTERS_NOT_ALLOWED.length(); i++) {
                if (property.contains(PV_NAME_VALIDATION_PROPERTY_CHARACTERS_NOT_ALLOWED.substring(i, i+1) )) {
                    messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_CONTAINS_CHARACTER_THAT_IS_NOT_ALLOWED);
                    break;
                }
            }
            if (property.contains("#")) {
                if (property.startsWith("#")) {
                    messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_IS_INTERNAL);
                } else {
                    messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_CONTAINS_CHARACTER_IN_A_POSITION_THAT_IS_NOT_ALLOWED);
                }
            }
            if (Character.isDigit(property.charAt(0))
                    || property.startsWith("_")
                    || property.startsWith("-")) {
                messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_DOES_NOT_START_WITH_WITH_A_LETTER);
            }
            if (Character.isLowerCase(property.charAt(0))) {
                messages.add(TextUtil.PV_NAME_VALIDATION_PROPERTY_DOES_NOT_START_WITH_WITH_AN_UPPER_CASE_LETTER);
            }
        }

        boolean isValidFormat = messages.isEmpty();

        return new ProcessVariableNameInformation(name, isValidFormat, !messages.isEmpty() ? String.join(" ", messages) : null);
    }

    /**
     * Extract convention name from given name.
     *
     * @param name convention name or process variable name
     * @return convention name
     */
    public static String extractConventionName(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider convention name
            //     focus on extract convention name
            //     not check if convention name is correct

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 1 || s1.length == 2) {
                String value = name;
                if (value.endsWith(DELIMITER_EXTRA)) {
                    value = value.substring(0, value.length()-1);
                }
                return nullIfEmpty(value);
            } else if (s1.length == 3) {
                String value = StringUtils.reverse(name);
                int index = value.indexOf(DELIMITER_EXTRA);
                if (index != -1) {
                    value = StringUtils.reverse(value.substring(index + 1));
                    if (value.endsWith(DELIMITER_EXTRA)) {
                        value = value.substring(0, value.length()-1);
                    }
                    return nullIfEmpty(value);
                }
            }
        }
        return null;
    }

    /**
     * Extract system group from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return system group
     */
    public static String extractSystemGroup(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 1 || s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[0].split(DELIMITER_INTRA);
                if (s2.length == 3) {
                    return s2[0];
                }
            }
        }
        return null;
    }

    /**
     * Extract system from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return system
     */
    public static String extractSystem(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 1 || s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[0].split(DELIMITER_INTRA);
                if (s2.length == 3) {
                    return s2[1];
                } else if (s2.length == 2 || s2.length == 1) {
                    return nullIfEmpty(s2[0]);
                }
            }
        }
        return null;
    }

    /**
     * Extract subsystem from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return subsystem
     */
    public static String extractSubsystem(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     system structure
            //         before first occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 1 || s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[0].split(DELIMITER_INTRA);
                if (s2.length == 3) {
                    return s2[2];
                } else if (s2.length == 2) {
                    return s2[1];
                }
            }
        }
        return null;
    }

    /**
     * Extract discipline from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return discipline
     */
    public static String extractDiscipline(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[1].split(DELIMITER_INTRA);
                if (s2.length == 3 || s2.length == 2) {
                    return nullIfEmpty(s2[0]);
                }
            }
        }
        return null;
    }

    /**
     * Extract device type from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return device type
     */
    public static String extractDeviceType(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[1].split(DELIMITER_INTRA);
                if (s2.length == 3 || s2.length == 2) {
                    return s2[1];
                }
            }
        }
        return null;
    }

    /**
     * Extract instance index from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return instance index
     */
    public static String extractInstanceIndex(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     device structure
            //         after           first  occurrence of DELIMITER_EXTRA
            //         before possible second occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 2 || s1.length == 3) {
                String[] s2 = s1[1].split(DELIMITER_INTRA);
                if (s2.length == 3) {
                    return s2[2];
                }
            }
        }
        return null;
    }

    /**
     * Extract property from ESS name or <tt>null</tt> if it can not be extracted.
     *
     * @param name convention name or process variable name
     * @return property
     */
    public static String extractProperty(String name) {
        if (!StringUtils.isEmpty(name)) {
            // consider name
            //     after second occurrence of DELIMITER_EXTRA

            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length == 3) {
                return s1[2];
            }
        }
        return null;
    }

    /**
     * Extract mnemonic path for system structure from ESS name.
     *
     * @param name convention name or process variable name
     * @return mnemonic path for system structure
     */
    public static String extractMnemonicPathSystemStructure(String name) {
        // see documentation at top of class to learn about kind of names
        // (according to current and old naming convention)
        // that are to be handled
        //
        // performance is important!

        if (!StringUtils.isEmpty(name)) {
            String[] s1 = name.split(DELIMITER_EXTRA);
            return s1.length > 0
                    ? nullIfEmpty(s1[0])
                    : null;
        }
        return null;
    }

    /**
     * Extract mnemonic path for device structure from ESS name.
     * Instance index is not part of mnemonic path for device structure.
     *
     * @param name convention name or process variable name
     * @return mnemonic path for device structure
     */
    public static String extractMnemonicPathDeviceStructure(String name) {
        // see documentation at top of class to learn about kind of names
        // (according to current and old naming convention)
        // that are to be handled
        //
        // performance is important!

        if (!StringUtils.isEmpty(name)) {
            String[] s1 = name.split(DELIMITER_EXTRA);
            if (s1.length > 1) {
                String[] s2 = s1[1].split(DELIMITER_INTRA);
                if ((s2.length == 3 || s2.length == 2)
                        && nullIfEmpty(s2[0]) != null
                        && nullIfEmpty(s2[1]) != null) {
                    return s2[0] + DELIMITER_INTRA + s2[1];
                }
            }
        }
        return null;
    }

    /**
     * Return instance index style.
     *
     * @param name convention name or process variable name
     * @return instance index style
     */
    public static String getInstanceIndexStyle(String name) {
        String indexStyle = null;
        String mnemonicPathDeviceStructure = extractMnemonicPathDeviceStructure(name);
        if (!StringUtils.isEmpty(mnemonicPathDeviceStructure)) {
            String discipline = extractDiscipline(name);
            indexStyle = DISCIPLINE_SCIENTIFIC;
            for (String str : getDisciplinesPID()) {
                if (StringUtils.equals(str, discipline)) {
                    indexStyle = DISCIPLINE_P_ID;
                    break;
                }
            }
        }
        return indexStyle;
    }

    /**
     * Return instance index style.
     *
     * @param discipline discipline
     * @return instance index style
     */
    public static String getInstanceIndexStyle(Discipline discipline) {
        String indexStyle = null;
        if (discipline != null && !StringUtils.isEmpty(discipline.getMnemonic())) {
            indexStyle = DISCIPLINE_SCIENTIFIC;
            for (String str : getDisciplinesPID()) {
                if (StringUtils.equals(str, discipline.getMnemonic())) {
                    indexStyle = DISCIPLINE_P_ID;
                    break;
                }
            }
        }
        return indexStyle;
    }

    /**
     * Return if discipline is PID discipline.
     *
     * @param discipline discipline
     * @return if discipline is PID
     */
    public static boolean isDisciplinePID(String discipline) {
        if (!StringUtils.isEmpty(discipline)) {
            for (String s : getDisciplinesPID()) {
                if (s.equals(discipline)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return if mnemonic path device structure is PID numeric.
     *
     * @param mnemonicPath mnemonic path device structure
     * @return if mnemonic path device structure is PID numeric
     */
    public static boolean isMnemonicPathDeviceStructurePIDNumeric(String mnemonicPath) {
        if (!StringUtils.isEmpty(mnemonicPath)) {
            for (String s : getMnemonicPathsPIDNumeric()) {
                if (StringUtils.equals(s, mnemonicPath)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return if ESS name is considered OFFSITE.
     *
     * @param name convention name or process variable name
     * @return if name is considered OFFSITE
     */
    public static boolean isOffsite(String name) {
        return !StringUtils.isEmpty(extractSystemGroup(name))
                && !StringUtils.isEmpty(extractSystem(name))
                && !StringUtils.isEmpty(extractSubsystem(name));
    }

    /**
     * Return if name is considered convention name.
     *
     * @param name convention name or process variable name
     * @return
     */
    public static boolean isConventionName(String name) {
        return extractConventionName(name) != null && extractProperty(name) == null;
    }

    /**
     * Return if name is considered process variable name.
     *
     * @param name convention name or process variable name
     * @return
     */
    public static boolean isProcessVariableName(String name) {
        return extractConventionName(name) != null && extractProperty(name) != null;
    }

    /**
     * Utility method to convert a mnemonic path (an array of strings) to a string
     * with {@link NamingConventionUtil#DELIMITER_INTRA} as path separator.
     *
     * @param mnemonicPath array of strings
     * @return mnemonic path
     */
    public static String mnemonicPath2String(String... mnemonicPath) {
        if (mnemonicPath != null) {
            String value = StringUtils.join(mnemonicPath, DELIMITER_INTRA);
            value = StringUtils.replace(value, DELIMITER_INTRA + DELIMITER_INTRA, DELIMITER_INTRA);
            return StringUtils.strip(value, DELIMITER_INTRA);
        }
        return null;
    }

    /**
     * Utility method to convert a string to a mnemonic path (an array of strings)
     * with {@link NamingConventionUtil#DELIMITER_INTRA} as path separator.
     *
     * @param name name
     * @return mnemonic path (an array of strings)
     */
    public static String[] string2MnemonicPath(String name) {
        if (name != null) {
            return StringUtils.split(name, DELIMITER_INTRA);
        }
        return EMPTY_ARRAY;
    }

    /**
     * Utility method to return mnemonic path system structure for name.
     *
     * @param mnemonicPathSystemStructure mnemonic path system structure
     * @return mnemonic path system structure for name
     */
    public static String mnemonicPathSystemStructure4Name(String mnemonicPathSystemStructure) {
        // mnemonicPathSystemStructure A-B-C ---> B-C
        // mnemonicPathSystemStructure A-B   ---> A-B
        // mnemonicPathSystemStructure A     ---> A

        String[] mnemonicPath = NamingConventionUtil.string2MnemonicPath(mnemonicPathSystemStructure);
        if (mnemonicPath != null && mnemonicPath.length == 3) {
            return mnemonicPath[1] + "-" + mnemonicPath[2];
        }
        return mnemonicPathSystemStructure;
    }

    /**
     * Return null for string that is null or empty, otherwise string itself.
     *
     * @param str string
     * @return null for string that is null or empty, otherwise string itself
     */
    private static String nullIfEmpty(String str) {
        return EMPTY.equals(str) ? null : str;
    }

}

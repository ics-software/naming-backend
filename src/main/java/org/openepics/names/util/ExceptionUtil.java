/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.openepics.names.exception.DataConflictException;
import org.openepics.names.exception.DataDeletedException;
import org.openepics.names.exception.DataExistException;
import org.openepics.names.exception.DataNotAvailableException;
import org.openepics.names.exception.DataNotCorrectException;
import org.openepics.names.exception.DataNotFoundException;
import org.openepics.names.exception.DataNotValidException;
import org.openepics.names.exception.InputNotAvailableException;
import org.openepics.names.exception.InputNotCorrectException;
import org.openepics.names.exception.InputNotEmptyException;
import org.openepics.names.exception.InputNotValidException;

/**
 * Utility class to assist in handling of exceptions.
 *
 * @author Lars Johansson
 */
public class ExceptionUtil {

    /**
     * This class is not to be instantiated.
     */
    private ExceptionUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Create data conflict exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data conflict exception
     */
    public static DataConflictException createDataConflictException(String message, String details, String field) {
        return new DataConflictException(message, details, field);
    }

    /**
     * Create data deleted exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data deleted exception
     */
    public static DataDeletedException createDataDeletedException(String message, String details, String field) {
        return new DataDeletedException(message, details, field);
    }

    /**
     * Create data exist exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data exist exception
     */
    public static DataExistException createDataExistException(String message, String details, String field) {
        return new DataExistException(message, details, field);
    }

    /**
     * Create data not available exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data not available exception
     */
    public static DataNotAvailableException createDataNotAvailableException(String message, String details, String field) {
        return new DataNotAvailableException(message, details, field);
    }

    /**
     * Create data not correct exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data not correct exception
     */
    public static DataNotCorrectException createDataNotCorrectException(String message, String details, String field) {
        return new DataNotCorrectException(message, details, field);
    }

    /**
     * Create data not found exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data not found exception
     */
    public static DataNotFoundException createDataNotFoundException(String message, String details, String field) {
        return new DataNotFoundException(message, details, field);
    }

    /**
     * Create data not valid exception.
     * Intended for communication inside server.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return data not valid exception
     */
    public static DataNotValidException createDataNotValidException(String message, String details, String field) {
        return new DataNotValidException(message, details, field);
    }

    /**
     * Create input not available exception.
     * Intended for communication inside server.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return input not available exception
     */
    public static InputNotAvailableException createInputNotAvailableException(String message, String details, String field) {
        return new InputNotAvailableException(message, details, field);
    }

    /**
     * Create input not correct exception.
     * Intended for communication inside server.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return input not correct exception
     */
    public static InputNotCorrectException createInputNotCorrectException(String message, String details, String field) {
        return new InputNotCorrectException(message, details, field);
    }

    /**
     * Create input not empty exception.
     * Intended for communication inside server.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return input not empty exception
     */
    public static InputNotEmptyException createInputNotEmptyException(String message, String details, String field) {
        return new InputNotEmptyException(message, details, field);
    }

    /**
     * Create input not valid exception.
     * Intended for communication inside server.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param message message
     * @param details details
     * @param field field
     * @return input not valid exception
     */
    public static InputNotValidException createInputNotValidException(String message, String details, String field) {
        return new InputNotValidException(message, details, field);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate condition and throw data conflict exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataConflictException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataConflictException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data deleted exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataDeletedException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataDeletedException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data exist exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataExistException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataExistException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data not available exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataNotAvailableException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataNotAvailableException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data not correct exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataNotCorrectException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataNotCorrectException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data not found exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataNotFoundException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataNotFoundException(message, details, field);
        }
    }

    /**
     * Validate condition and throw data not valid exception if condition not <tt>true</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionDataNotValidException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createDataNotValidException(message, details, field);
        }
    }

    /**
     * Validate condition and throw input not available exception if condition not <tt>true</tt>.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionInputNotAvailableException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createInputNotAvailableException(message, details, field);
        }
    }

    /**
     * Validate condition and throw input not correct exception if condition not <tt>true</tt>.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionInputNotCorrectException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createInputNotCorrectException(message, details, field);
        }
    }

    /**
     * Validate condition and throw input not empty exception if condition not <tt>true</tt>.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionInputNotEmptyException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createInputNotEmptyException(message, details, field);
        }
    }

    /**
     * Validate condition and throw input not valid exception if condition not <tt>true</tt>.
     * Note that message is automatically generated if message is <tt>null</tt> and field <tt>not null</tt>.
     *
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     */
    public static void validateConditionInputNotValidException(boolean condition, String message, String details, String field) {
        if (!condition) {
            throw ExceptionUtil.createInputNotValidException(message, details, field);
        }
    }

}

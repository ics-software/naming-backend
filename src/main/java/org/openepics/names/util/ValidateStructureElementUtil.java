/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.exception.DataConflictException;
import org.openepics.names.repository.model.Discipline;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElementCommand;

import com.google.common.collect.Lists;

/**
 * Utility class to assist in handling of validation.
 *
 * @author Lars Johansson
 */
public class ValidateStructureElementUtil {

    // note
    //     StructureElement
    //         type, uuid, parent,
    //         mnemonic, ordering, description,
    //         mnemonic path, level,
    //         status, deleted,
    //         when, who, comment
    //
    //     StructureElementCommand
    //	       create  -       type, parent, mnemonic, ordering, description
    //	       update  - uuid, type, parent, mnemonic, ordering, description
    //	       delete  - uuid, type

    /**
     * This class is not to be instantiated.
     */
    private ValidateStructureElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Validate parameters for read structures.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who, who
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     */
    public static void validateStructuresInputRead(Boolean deleted,
            String uuid, String parent, String mnemonic, String mnemonicPath, String description, String who,
            Boolean includeHistory, FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     uuid
        //     parent

        if (!StringUtils.isEmpty(uuid)) {
            ValidateUtil.validateInputUuid(uuid);
        }
        if (!StringUtils.isEmpty(parent)) {
            ValidateUtil.validateInputUuid(parent);
        }
    }

    /**
     * Validate parameters for read structures.
     *
     * @param type type
     * @param deleted deleted
     * @param uuid uuid
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who, who
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     */
    public static void validateStructuresInputRead(Type type, Boolean deleted,
            String uuid, String parent, String mnemonic, String mnemonicPath, String description, String who,
            Boolean includeHistory, FieldStructure orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     uuid
        //     parent

        validateStructuresInputRead(deleted,
                uuid, parent, mnemonic, mnemonicPath, description, who,
                includeHistory, orderBy, isAsc, offset, limit);
    }

    /**
     * Validate structure element command input for create.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     */
    public static void validateStructureElementInputCreate(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElementCommand, StructureCommand.CREATE, namingConvention);
    }

    /**
     * Validate structure element command input for update.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     */
    public static void validateStructureElementInputUpdate(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElementCommand, StructureCommand.UPDATE, namingConvention);
    }

    /**
     * Validate structure element command input for delete.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     */
    public static void validateStructureElementInputDelete(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention) {
        validateStructureElementInput(structureElementCommand, StructureCommand.DELETE, namingConvention);
    }

    /**
     * Validate structure element command input for given task.
     *
     * @param structureElementCommand structure element command
     * @param structureCommand structure command
     * @param namingConvention naming convention
     */
    private static void validateStructureElementInput(StructureElementCommand structureElementCommand, StructureCommand structureCommand, EssNamingConvention namingConvention) {
        // command - attributes
        //     create  -       type, parent, mnemonic, ordering, description
        //	   update  - uuid, type, parent, mnemonic, ordering, description
        //	   delete  - uuid, type
        //
        //     create, update - optional - parent, mnemonic

        if (ValidateUtil.isAnyNull(structureElementCommand, namingConvention, structureCommand)) {
            return;
        }

        UUID uuid = structureElementCommand.getUuid();
        Type type = structureElementCommand.getType();
        UUID parent = structureElementCommand.getParent();
        String mnemonic = structureElementCommand.getMnemonic();
        String description = structureElementCommand.getDescription();

        // validate input
        //     update, delete
        //         uuid
        //     all
        //         type
        //     create. update
        //         parent
        //         mnemonic
        //         description

        if (ValidateUtil.isAnyEqual(structureCommand, StructureCommand.UPDATE, StructureCommand.DELETE)) {
            ValidateUtil.validateInputUuid(uuid != null ? uuid.toString() : null);
        }

        ValidateUtil.validateInputType(type);

        if (ValidateUtil.isAnyEqual(structureCommand, StructureCommand.CREATE, StructureCommand.UPDATE)) {
            if (ValidateUtil.isAnyEqual(type, Type.SYSTEM, Type.SUBSYSTEM, Type.DEVICEGROUP, Type.DEVICETYPE)) {
                ExceptionUtil.validateConditionInputNotAvailableException(parent != null,
                        TextUtil.PARENT_IS_NOT_AVAILABLE, structureElementCommand.toString(), TextUtil.PARENT);
                ValidateUtil.validateInputUuid(parent.toString());
            }

            // validate mnemonic
            //     validate mnemonic input (value itself, not in relation to other values)
            //     validateMnemonic takes isMnemonicRequired into account
            MnemonicValidation mnemonicValidation = namingConvention.validateMnemonic(type, mnemonic);
            ExceptionUtil.validateConditionInputNotValidException(MnemonicValidation.VALID.equals(mnemonicValidation),
                    TextUtil.MNEMONIC_IS_NOT_VALID, structureElementCommand.toString(), TextUtil.MNEMONIC);

            ValidateUtil.validateInputDescription(description);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate structure element command data for create.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateStructureElementDataCreate(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateStructureElementDataInItself(structureElementCommand, StructureCommand.CREATE, holderRepositories);
        validateStructureElementDataRelativeOtherData(structureElementCommand, StructureCommand.CREATE, namingConvention,
                holderRepositories, holderStructures);
    }

    /**
     * Validate structure element command data for update.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateStructureElementDataUpdate(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateStructureElementDataInItself(structureElementCommand, StructureCommand.UPDATE, holderRepositories);
        validateStructureElementDataRelativeOtherData(structureElementCommand, StructureCommand.UPDATE, namingConvention,
                holderRepositories, holderStructures);
    }

    /**
     * Validate structure element command data for delete.
     *
     * @param structureElementCommand structure element command
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateStructureElementDataDelete(StructureElementCommand structureElementCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateStructureElementDataInItself(structureElementCommand, StructureCommand.DELETE, holderRepositories);
        validateStructureElementDataRelativeOtherData(structureElementCommand, StructureCommand.DELETE, namingConvention,
                holderRepositories, holderStructures);
    }

    /**
     * Validate structure element command data in itself for given task.
     *
     * @param structureElementCommand structure element command
     * @param structureCommand structure command
     * @param holderRepositories holder repositories
     */
    public static void validateStructureElementDataInItself(StructureElementCommand structureElementCommand, StructureCommand structureCommand,
            HolderRepositories holderRepositories) {
        // command - attributes
        //     create  -       type, parent, mnemonic, ordering, description
        //	   update  - uuid, type, parent, mnemonic, ordering, description
        //	   delete  - uuid, type
        //
        //     create, update - optional - parent, mnemonic
        //
        // check structure element data in itself
        //     update, delete
        //         uuid - not deleted - list size 1

        if (ValidateUtil.isAnyNull(structureElementCommand, structureCommand, holderRepositories)) {
            return;
        }

        UUID uuid = structureElementCommand.getUuid();
        Type type = structureElementCommand.getType();
        UUID parent = structureElementCommand.getParent();

        String message = type.toString().toLowerCase() + TextUtil.SPACE + TextUtil.IS_NOT_CORRECT;
        String details = structureElementCommand.toString();
        String field   = TextUtil.UUID;

        // validate data in itself
        //     systemgroup, discipline
        //         not have parent
        //     update, delete
        //         entry found
        //         entry not deleted

        if (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.DISCIPLINE) && parent != null) {
            throw ExceptionUtil.createDataNotCorrectException(TextUtil.PARENT_IS_NOT_CORRECT, details, field);
        } else if (ValidateUtil.isAnyEqual(structureCommand, StructureCommand.UPDATE, StructureCommand.DELETE)) {
            validateStructuresFound(uuid, type,
                    holderRepositories, message, details, field);
            validateStructuresValidSize1(uuid, type, Boolean.FALSE,
                    holderRepositories, message, details, field);
        }
    }

    private static void validateStructuresFound(UUID uuid, Type type,
            HolderRepositories holderRepositories, String message, String details, String field) {
        ExceptionUtil.validateConditionDataNotFoundException(
                getStructuresSize(uuid, type, null, holderRepositories) > 0, message, details, field);
    }
    private static void validateStructuresValidSize1(UUID uuid, Type type, Boolean deleted,
            HolderRepositories holderRepositories, String message, String details, String field) {
        ExceptionUtil.validateConditionDataNotValidException(
                getStructuresSize(uuid, type, deleted, holderRepositories) == 1, message, details, field);
    }
    private static int getStructuresSize(UUID uuid, Type type, Boolean deleted,
            HolderRepositories holderRepositories) {
        if (Type.SYSTEMGROUP.equals(type)) {
            return holderRepositories.systemGroupRepository().readSystemGroups(deleted, uuid.toString(), null, null, null, null, null).size();
        } else if (Type.SYSTEM.equals(type)) {
            return holderRepositories.systemRepository().readSystems(deleted, uuid.toString(), null, null, null, null, null, null).size();
        } else if (Type.SUBSYSTEM.equals(type)) {
            return holderRepositories.subsystemRepository().readSubsystems(deleted, uuid.toString(), null, null, null, null, null, null).size();
        } else if (Type.DISCIPLINE.equals(type)) {
            return holderRepositories.disciplineRepository().readDisciplines(deleted, uuid.toString(), null, null, null, null, null).size();
        } else if (Type.DEVICEGROUP.equals(type)) {
            return holderRepositories.deviceGroupRepository().readDeviceGroups(deleted, uuid.toString(), null, null, null, null, null, null).size();
        } else if (Type.DEVICETYPE.equals(type)) {
            return holderRepositories.deviceTypeRepository().readDeviceTypes(deleted, uuid.toString(), null, null, null, null, null, null).size();
        }
        return 0;
    }

    /**
     * Validate structure element command data relative other data for given task.
     *
     * @param structureElementCommand structure element command
     * @param structureCommand structure command
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateStructureElementDataRelativeOtherData(StructureElementCommand structureElementCommand, StructureCommand structureCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories, HolderStructures holderStructures) {
        // command - attributes
        //     create  -       type, parent, mnemonic, ordering, description
        //	   update  - uuid, type, parent, mnemonic, ordering, description
        //	   delete  - uuid, type
        //
        //     create, update - optional - parent, mnemonic
        //
        // check structure element data in relation to other data
        //     create, update
        //         parent               - (if applicable) not deleted
        //         mnemonic             - (if not same)   not exists
        //         mnemonic equivalence - (if not same)   equivalence not exists
        //     approve
        //         uuid - pending, deleted
        //         no or less checks if entry to be deleted                - approve (delete)
        //         more or same checks as above if entry is not be deleted - approve (create), approve (update)
        //         need checks as content may have changed from time of create, update, delete to time of approve
        //     checks on mnemonic, mnemonic equivalence are to ensure can coexist, can move
        //     possibly
        //         additional checks if names are affected

        if (ValidateUtil.isAnyNull(structureElementCommand, structureCommand, namingConvention, holderRepositories, holderStructures)) {
            return;
        }

        String message = null;
        String details = structureElementCommand.toString();
        String field   = TextUtil.MNEMONIC;

        UUID uuid = structureElementCommand.getUuid();
        Type type = structureElementCommand.getType();
        UUID parent = structureElementCommand.getParent();
        String mnemonic = structureElementCommand.getMnemonic();

        // validate data relative other data
        //     create, update
        //         entry parent
        //         entry mnemonic
        //         entry mnemonic equivalence

        if (ValidateUtil.isAnyEqual(structureCommand, StructureCommand.CREATE, StructureCommand.UPDATE)) {
            if (Type.SYSTEMGROUP.equals(type)) {
                // note rules for mnemonic for system group
                if (!StringUtils.isEmpty(mnemonic)) {
                    // mnemonic
                    message = TextUtil.SYSTEMGROUP_IS_NOT_CORRECT;
                    validateStructuresMnemonic(uuid, type, parent, mnemonic,
                            structureCommand, namingConvention, holderRepositories, message, details, field);

                    // mnemonic equivalence
                    validateStructuresMnemonicequivalence(uuid, type, parent, mnemonic,
                            structureCommand, namingConvention, holderRepositories, message, details, field);
                }
            } else if (Type.SYSTEM.equals(type)) {
                // parent
                message = TextUtil.SYSTEMGROUP_IS_NOT_CORRECT;
                validateStructuresParent(type, parent, false,
                        holderRepositories, message, details, field);

                // mnemonic
                message = TextUtil.SYSTEM_IS_NOT_CORRECT;
                validateStructuresMnemonic(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);

                // mnemonic equivalence
                validateStructuresMnemonicequivalence(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);
            } else if (Type.SUBSYSTEM.equals(type)) {
                // parent
                message = TextUtil.SYSTEM_IS_NOT_CORRECT;
                validateStructuresParent(type, parent, false,
                        holderRepositories, message, details, field);

                // mnemonic
                message = TextUtil.SUBSYSTEM_IS_NOT_CORRECT;
                validateStructuresMnemonic(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);

                // mnemonic equivalence
                validateStructuresMnemonicequivalence(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);
            } else if (Type.DISCIPLINE.equals(type)) {
                message = TextUtil.DISCIPLINE_IS_NOT_CORRECT;

                // mnemonic
                validateStructuresMnemonic(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);

                // mnemonic equivalence
                validateStructuresMnemonicequivalence(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);
            } else if (Type.DEVICEGROUP.equals(type)) {
                // parent
                message = TextUtil.DISCIPLINE_IS_NOT_CORRECT;
                List<Discipline> disciplines = holderRepositories.disciplineRepository().readDisciplines(false, parent.toString(), null, null, null, null, null);
                ExceptionUtil.validateConditionDataNotFoundException(ValidateUtil.isSize(disciplines, 1),
                        message, details, TextUtil.PARENT);

                // note rules for mnemonic for device group

                // mnemonic
                //     fits logic here but really validateStructureElementDataInItself
                message = TextUtil.DEVICEGROUP_IS_NOT_CORRECT;
                ExceptionUtil.validateConditionDataConflictException(StringUtils.isEmpty(mnemonic), message, details, field);
            } else if (Type.DEVICETYPE.equals(type)) {
                // parent
                message = TextUtil.DEVICEGROUP_IS_NOT_CORRECT;
                validateStructuresParent(type, parent, false,
                        holderRepositories, message, details, field);

                // mnemonic
                message = TextUtil.DEVICETYPE_IS_NOT_CORRECT;
                validateStructuresMnemonic(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);

                // mnemonic equivalence
                validateStructuresMnemonicequivalence(uuid, type, parent, mnemonic,
                        structureCommand, namingConvention, holderRepositories, message, details, field);
            }
        }
    }

    private static void validateStructuresParent(Type type, UUID parent, Boolean deleted,
            HolderRepositories holderRepositories, String message, String details, String field){
        // about finding out if structures are in place in order for structure operation to be allowed

        ExceptionUtil.validateConditionDataNotFoundException(
                getStructuresParentSize(type, parent, deleted, holderRepositories) == 1, message, details, field);
    }
    private static void validateStructuresMnemonic(UUID uuid, Type type, UUID parent, String mnemonic,
            StructureCommand structureCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories,
            String message, String details, String field){
        // about finding out if structures are in place in order for structure operation to be allowed

        // parent may be null
        List<UUID> listUuid = null;

        listUuid = getStructures(type, parent, mnemonic, false, false, namingConvention, holderRepositories);
        validateConditionIfStructureCommand(StructureCommand.CREATE, structureCommand, listUuid.isEmpty(), message, details, field);
        validateConditionIfStructureCommand(StructureCommand.UPDATE, structureCommand, ValidateUtil.isEmptyOrEquals(listUuid, uuid), message, details, field);
    }
    private static void validateStructuresMnemonicequivalence(UUID uuid, Type type, UUID parent, String mnemonic,
            StructureCommand structureCommand, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories,
            String message, String details, String field){
        // about finding out if structures are in place in order for structure operation to be allowed

        // parent may be null
        List<UUID> listUuid = null;

        listUuid = getStructures(type, parent, mnemonic, false, true, namingConvention, holderRepositories);
        validateConditionIfStructureCommand(StructureCommand.CREATE, structureCommand, listUuid.isEmpty(), message, details, field);
        validateConditionIfStructureCommand(StructureCommand.UPDATE, structureCommand, ValidateUtil.isEmptyOrEquals(listUuid, uuid), message, details, field);
    }
    private static int getStructuresParentSize(Type type, UUID parent, Boolean deleted,
            HolderRepositories holderRepositories) {
        String queryValue = parent.toString();
        if (Type.SYSTEM.equals(type)) {
            return holderRepositories.systemGroupRepository().readSystemGroups(deleted, queryValue, null, null, null, null, null).size();
        } else if (Type.SUBSYSTEM.equals(type)) {
            return holderRepositories.systemRepository().readSystems(deleted, queryValue, null, null, null, null, null, null).size();
        } else if (Type.DEVICEGROUP.equals(type)) {
            return holderRepositories.disciplineRepository().readDisciplines(deleted, queryValue, null, null, null, null, null).size();
        } else if (Type.DEVICETYPE.equals(type)) {
            return holderRepositories.deviceGroupRepository().readDeviceGroups(deleted, queryValue, null, null, null, null, null, null).size();
        }
        return 0;
    }
    private static List<UUID> getStructures(Type type, UUID parent, String mnemonic, Boolean deleted, Boolean equivalence,
            EssNamingConvention namingConvention, HolderRepositories holderRepositories) {
        // get structures to be used in validation
        // may be difficult to understand, need context of how and where it is used
        //     before ok to create or approve, structures need to exist or not exist, this method helps to find out

        String queryUuid = null;
        String queryParent = null;
        String queryMnemonic = null;
        String queryMnemonicEquivalence = null;

        boolean hasMnemonic = !StringUtils.isEmpty(mnemonic);
        if (hasMnemonic) {
            if (equivalence) {
                queryMnemonicEquivalence = namingConvention.equivalenceClassRepresentative(mnemonic);
            } else {
                queryMnemonic = mnemonic;
            }
        }

        // uuid, parent, mnemonic
        boolean queryParentTable = false;
        if (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.DISCIPLINE)) {
            // no parent - query table for type
            if (hasMnemonic) {
            } else {
                return Lists.newArrayList();
            }
        } else {
            // parent
            if (hasMnemonic) {
                // query table for type
                queryParent = parent.toString();
            } else {
                // query table for parent to type
                queryParentTable = true;
                queryUuid = parent.toString();
                queryParent = null;
                queryMnemonic = null;
                queryMnemonicEquivalence = null;
            }
        }

        // query table and return list with uuid
        if (Type.SYSTEMGROUP.equals(type)) {
            return StructureUtil.listSystemGroup2Uuid(
                    holderRepositories.systemGroupRepository().readSystemGroups(deleted, queryUuid, queryMnemonic, queryMnemonicEquivalence, null, null, null));
        } else if (Type.SYSTEM.equals(type)) {
            if (queryParentTable) {
                return StructureUtil.listSystemGroup2Uuid(holderRepositories.systemGroupRepository().readSystemGroups(deleted, queryUuid, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            } else {
                return StructureUtil.listSystem2Uuid(holderRepositories.systemRepository().readSystems(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            }
        } else if (Type.SUBSYSTEM.equals(type)) {
            if (queryParentTable) {
                return StructureUtil.listSystem2Uuid(holderRepositories.systemRepository().readSystems(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            } else {
                return StructureUtil.listSubsystem2Uuid(holderRepositories.subsystemRepository().readSubsystems(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            }
        } else if (Type.DISCIPLINE.equals(type)) {
            return StructureUtil.listDiscipline2Uuid(holderRepositories.disciplineRepository().readDisciplines(deleted, queryUuid, queryMnemonic, queryMnemonicEquivalence, null, null, null));
        } else if (Type.DEVICEGROUP.equals(type)) {
            if (queryParentTable) {
                return StructureUtil.listDiscipline2Uuid(holderRepositories.disciplineRepository().readDisciplines(deleted, queryUuid, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            } else {
                return StructureUtil.listDeviceGroup2Uuid(holderRepositories.deviceGroupRepository().readDeviceGroups(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));            }
        } else if (Type.DEVICETYPE.equals(type)) {
            if (queryParentTable) {
                return StructureUtil.listDeviceGroup2Uuid(holderRepositories.deviceGroupRepository().readDeviceGroups(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            } else {
                return StructureUtil.listDeviceType2Uuid(holderRepositories.deviceTypeRepository().readDeviceTypes(deleted, queryUuid, queryParent, queryMnemonic, queryMnemonicEquivalence, null, null, null));
            }
        }
        return Lists.newArrayList();
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate structure data.
     * This method corresponds to validate structure element data for create, albeit in a different way.
     *
     * @param type type
     * @param mnemonicPath mnemonic path
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateStructureDataCreate(Type type, String mnemonicPath, EssNamingConvention namingConvention,
            HolderIRepositories holderIRepositories, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(type, mnemonicPath, namingConvention, holderIRepositories, holderStructures)) {
            return;
        }

        String details = mnemonicPath;
        String field = TextUtil.MNEMONIC_PATH;

        String[] path = NamingConventionUtil.string2MnemonicPath(mnemonicPath);
        String mnemonicpathEquivalence = namingConvention.equivalenceClassRepresentative(mnemonicPath);

        // validate path, including null check
        ExceptionUtil.validateConditionDataNotCorrectException(path != null && path.length >= 1 && path.length <= 2,
                TextUtil.STRUCTURE_MNEMONIC_PATH_IS_NOT_CORRECT, details, field);

        if (Type.SYSTEMGROUP.equals(type)) {
            validateStructuresMnemonicpathNotValid(path.length == 1, type, details, field);

            // system group may have empty path but there will be mnemonic path in this context

            // mnemonic path
            SystemGroup sg = holderIRepositories.systemGroupRepository().findNotDeletedByMnemonic(path[0]);
            validateStructuresMnemonicpathDuplicate(sg == null, type, details, field);

            // mnemonic path equivalence
            List<SystemGroup> systemGroups = holderIRepositories.systemGroupRepository().findNotDeleted();
            for (SystemGroup systemGroup : systemGroups) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(systemGroup.getMnemonicEquivalence(), namingConvention.equivalenceClassRepresentative(path[0])),
                        type, details, field);
            }
        } else if (Type.SYSTEM.equals(type)) {
            validateStructuresMnemonicpathNotValid(path.length == 1, type, details, field);

            // path with 1 element  - system group or  system ---> check both individually
            // check mnemonic, mnemonic equivalence

            // mnemonic path system group
            List<String> mnemonicPaths = StructureUtil.getMnemonicPathsSystemGroup(holderStructures, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(mnemonicPath, existingPath),
                        type, details, field);
            }

            // mnemonic path equivalence system group
            mnemonicPaths = StructureUtil.getMnemonicPathsSystemGroup(holderStructures, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)),
                        type, details, field);
            }

            // mnemonic path
            mnemonicPaths = StructureUtil.getMnemonicPathsSystem(holderStructures, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(mnemonicPath, existingPath),
                        type, details, field);
            }

            // mnemonic path equivalence
            mnemonicPaths = StructureUtil.getMnemonicPathsSystem(holderStructures, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)),
                        type, details, field);
            }
        } else if (Type.SUBSYSTEM.equals(type)) {
            validateStructuresMnemonicpathNotValid(path.length == 2, type, details, field);

            System sys = holderIRepositories.systemRepository().findNotDeletedByMnemonic(path[0]);
            validateStructuresMnemonicpathNotFound(sys != null, type, details, field);
            SystemGroup sg = holderIRepositories.systemGroupRepository().findNotDeletedById(sys.getParentId());

            // mnemonic path
            validateStructuresMnemonicpathDuplicate(!StringUtils.equals(path[0], path[1]), type, details, field);
            if (!StringUtils.isEmpty(sg.getMnemonic())) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(sg.getMnemonic(), path[1]),
                        type, details, field);
            }

            List<String> mnemonicPaths = StructureUtil.getMnemonicPathsSubsystem(holderStructures, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(mnemonicPath, existingPath),
                        type, details, field);
            }

            // mnemonic path equivalence
            validateStructuresMnemonicpathDuplicate(
                    !StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])),
                    type, details, field);
            if (!StringUtils.isEmpty(sg.getMnemonic())) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(namingConvention.equivalenceClassRepresentative(sg.getMnemonic()), namingConvention.equivalenceClassRepresentative(path[1])),
                        type, details, field);
            }

            mnemonicPaths = StructureUtil.getMnemonicPathsSubsystem(holderStructures, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)),
                        type, details, field);
            }
        } else if (Type.DISCIPLINE.equals(type)) {
            validateStructuresMnemonicpathNotValid(path.length == 1, type, details, field);

            // mnemonic path
            Discipline di = holderIRepositories.disciplineRepository().findNotDeletedByMnemonic(path[0]);
            validateStructuresMnemonicpathDuplicate(di == null, type, details, field);

            // mnemonic path equivalence
            List<Discipline> disciplines = holderIRepositories.disciplineRepository().findNotDeleted();
            for (Discipline discipline : disciplines) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(discipline.getMnemonicEquivalence(), namingConvention.equivalenceClassRepresentative(path[0])),
                        type, details, field);
            }
        } else if (Type.DEVICEGROUP.equals(type)) {
            throw ExceptionUtil.createDataNotValidException(TextUtil.DEVICEGROUP_IS_NOT_VALID, details, field);
        } else if (Type.DEVICETYPE.equals(type)) {
            // since device group is between discipline and device type and device group has no mnemonic,
            //     it can not be traced to which device group that this device type belongs,
            //     therefore it can not be known to which mnemonic line it belongs
            //     by looking at mnemonic
            // instead similar approach as for other Type, i.e. compare with list of mnemonic paths
            //     rest of checks in validate create

            validateStructuresMnemonicpathNotValid(path.length == 2, type, details, field);

            // discipline
            Discipline discipline = holderIRepositories.disciplineRepository().findNotDeletedByMnemonic(path[0]);
            ExceptionUtil.validateConditionDataNotFoundException(discipline != null,
                    TextUtil.DISCIPLINE_IS_NOT_FOUND, details, field);

            // mnemonic path
            validateStructuresMnemonicpathDuplicate(!StringUtils.equals(path[0], path[1]), type, details, field);

            List<String> mnemonicPaths = StructureUtil.getMnemonicPathsDeviceType(holderStructures, false, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathDuplicate(
                        !StringUtils.equals(mnemonicPath, existingPath),
                        type, details, field);
            }

            // mnemonic path equivalence
            validateStructuresMnemonicpathDuplicate(
                    !StringUtils.equals(namingConvention.equivalenceClassRepresentative(path[0]), namingConvention.equivalenceClassRepresentative(path[1])),
                    type, details, field);

            mnemonicPaths = StructureUtil.getMnemonicPathsDeviceType(holderStructures, true, namingConvention);
            for (String existingPath : mnemonicPaths) {
                validateStructuresMnemonicpathequivalenceDuplicate(
                        !StringUtils.equals(mnemonicpathEquivalence, namingConvention.equivalenceClassRepresentative(existingPath)),
                        type, details, field);
            }
        }
    }

    private static void validateStructuresMnemonicpathNotFound(boolean condition, Type type, String details, String field) {
        ExceptionUtil.validateConditionDataNotFoundException(condition,
                type.toString().toLowerCase() + TextUtil.SPACE + TextUtil.MNEMONIC_PATH_IS_NOT_FOUND,
                details,
                field);
    }
    private static void validateStructuresMnemonicpathNotValid(boolean condition, Type type, String details, String field) {
        ExceptionUtil.validateConditionDataNotValidException(condition,
                type.toString().toLowerCase() + TextUtil.SPACE + TextUtil.MNEMONIC_PATH_IS_NOT_VALID,
                details,
                field);
    }
    private static void validateStructuresMnemonicpathDuplicate(boolean condition, Type type, String details, String field) {
        ExceptionUtil.validateConditionDataConflictException(condition,
                type.toString().toLowerCase() + TextUtil.SPACE + TextUtil.MNEMONIC_PATH_HAS_DUPLICATE,
                details,
                field);
    }
    private static void validateStructuresMnemonicpathequivalenceDuplicate(boolean condition, Type type, String details, String field) {
        ExceptionUtil.validateConditionDataConflictException(condition,
                type.toString().toLowerCase() + TextUtil.SPACE + TextUtil.MNEMONIC_PATH_EQUIVALENCE_HAS_DUPLICATE,
                details,
                field);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate condition if precondition is fulfilled and throw
     * data conflict exception with reason if validation fails.
     *
     * @param precondition precondition
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     *
     * @see DataConflictException
     */
    private static void validateConditionIfPrecondition(boolean precondition,
            boolean condition, String message, String details, String field) {
        if (precondition) {
            ExceptionUtil.validateConditionDataConflictException(condition, message, details, field);
        }
    }

    /**
     * Validate condition if precondition (StructureCommand) is fulfilled and
     * throw data conflict exception with reason if validation fails.
     *
     * @param expected expected structure command
     * @param actual actual structure command
     * @param condition condition
     * @param message message
     * @param details details
     * @param field field
     *
     * @see DataConflictException
     */
    private static void validateConditionIfStructureCommand(StructureCommand expected, StructureCommand actual,
            boolean condition, String message, String details, String field) {
        validateConditionIfPrecondition(expected != null && expected.equals(actual),
                condition, message, details, field);
    }

}

/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class for URL handling, e.g. encoding URL.
 *
 * @author Lars Johansson
 *
 * @see EncodingUtility
 */
public class URLUtility {

    /**
     * / corresponds to Unicode \u002F
     */
    private static final String DELIMITER_PATH = "\u002F";
    /**
     * ? corresponds to Unicode \u003F
     */
    private static final String DELIMITER_PATH_QUERY = "\u003F";
    /**
     * & corresponds to Unicode \u003F
     */
    private static final String DELIMITER_ATTRIBUTES_VALUES = "\u0026";
    /**
     * = corresponds to Unicode \u003D
     */
    private static final String DELIMITER_ATTRIBUTE_VALUE = "\u003D";

    private static final Pattern PATTERN_DELIMITER_PATH_QUERY =
            Pattern.compile("\\" + DELIMITER_PATH_QUERY);
    private static final Pattern PATTERN_DELIMITER_ATTRIBUTES_VALUES =
            Pattern.compile("\\" + DELIMITER_ATTRIBUTES_VALUES);
    private static final Pattern PATTERN_DELIMITER_ATTRIBUTE_VALUE =
            Pattern.compile("\\" + DELIMITER_ATTRIBUTE_VALUE);

    /**
     * This class is not to be instantiated.
     */
    private URLUtility() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Encode a URL and return the encoded value.
     *
     * <p>
     * Note that all or parts of URL may be encoded. In case of attributes, attribute values but not names are to be encoded.
     * There are special characters that are used to separate parts of URL. Such characters are / ? & =
     * </p>
     *
     * <br/> Different URLs
     * <ul>
     * <li> a           </li>
     * <li> a/b         </li>
     * <li> a/b?c       </li>
     * <li> a/b?c=d     </li>
     * <li> a/b?c=d&e=f </li>
     * <li> ...         </li>
     * </ul>
     *
     * <p>
     *     Example a.
     *     <ul>
     *     <li>a is encoded</li>
     *     </ul>
     * </p>
     * <p>
     *     Example a/b.
     *     <li>a is not encoded</li>
     *     <li>b is encoded</li>
     * </p>
     * <p>
     *     Example a/b?c.
     *     <li>a is not encoded</li>
     *     <li>b is encoded</li>
     *     <li>c is encoded</li>
     * </p>
     * <p>
     *     Example a/b?c=d.
     *     <li>a is not encoded</li>
     *     <li>b is encoded</li>
     *     <li>c is not encoded</li>
     *     <li>d is encoded</li>
     * </p>
     * <p>
     *     Example a/b?c=d&e=f.
     *     <li>a is not encoded</li>
     *     <li>b is encoded</li>
     *     <li>c is not encoded</li>
     *     <li>d is encoded</li>
     *     <li>e is not encoded</li>
     *     <li>f is encoded</li>
     * </p>
     *
     * <p>
     * Example 1. For <code>abc.xhtml?attribute1=value1&amp;attribute2=value2</code>, it corresponds to b?c=d&e=f above.
     * Then <code>abc.xhtml</code>, <code>value1</code> and <code>value2</code> are to be encoded, not other parts of url.
     * <7p>
     *
     * <p>
     * Example 2. For <code>cable-types.xhtml?cableTypeName=1C20RG-58HV+ 2C20</code>, it corresponds to b?c=d above.
     * Then <code>cable-types.xhtml</code> and <code>1C20RG-58HV+ 2C20</code> is to be encoded, not other parts of url.
     * </p>
     *
     * @param url a url
     * @return the encoded value
     *
     * @see EncodingUtility#ENCODING_SCHEME
     * @see EncodingUtility#encode(String)
     * @see EncodingUtility#decode(String)
     */
    public static String encodeURL(String url) {
        if (url == null)
            return url;

        // not trim url

        // start prepare return value
        StringBuilder encodedURL = new StringBuilder();

        String path0 = null;
        String path1 = null;

        // split path on last DELIMITER_PATH
        String reverseUrl = StringUtils.reverse(url);
        int index = reverseUrl.indexOf(DELIMITER_PATH);
        if (index == -1) {
            path1 = url;
        } else {
            path0 = StringUtils.reverse(reverseUrl.substring(index + 1));
            path1 = StringUtils.reverse(reverseUrl.substring(0, index));
            encodedURL.append(path0);
            encodedURL.append(DELIMITER_PATH);
        }

      // split path and query
      String[] pathQuery = PATTERN_DELIMITER_PATH_QUERY.split(path1, 2);
      if (pathQuery.length != 2) {
          encodedURL.append(EncodingUtility.encode(path1));
          return encodedURL.toString();
      }
      encodedURL.append(EncodingUtility.encode(pathQuery[0]));
      encodedURL.append(DELIMITER_PATH_QUERY);

        // split attribute-value pairs
        String[] attributesValues = PATTERN_DELIMITER_ATTRIBUTES_VALUES.split(pathQuery[1]);

        // loop through attributes_values, encode values
        for (String pair : attributesValues) {
            // split attribute and value
            String[] attributeValue = PATTERN_DELIMITER_ATTRIBUTE_VALUE.split(pair, 2);

            if (attributeValue.length == 1) {
                // not encode attribute
                encodedURL.append(EncodingUtility.encode(attributeValue[0]));
                encodedURL.append(DELIMITER_ATTRIBUTES_VALUES);
            } else {
                // encode value
              encodedURL.append(attributeValue[0]);
              encodedURL.append(DELIMITER_ATTRIBUTE_VALUE);
              encodedURL.append(EncodingUtility.encode(attributeValue[1]));
              encodedURL.append(DELIMITER_ATTRIBUTES_VALUES);
            }
        }

        // remove trailing delimiter
        int length = encodedURL.length();
        if (encodedURL.substring(length - 1).equals(DELIMITER_ATTRIBUTES_VALUES)) {
            encodedURL.setLength(length - 1);
        }

        return encodedURL.toString();
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.wip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.openepics.names.repository.model.wip.WipDeviceType;
import org.openepics.names.repository.model.wip.WipDiscipline;
import org.openepics.names.repository.model.wip.WipSubsystem;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.repository.model.wip.WipSystemGroup;

/**
 * Utility class and holder of system and device structure content that are used to populate REST API return elements.
 * <p>
 * Class is used for performance reasons to speed up preparation of what is to be returned.
 * </p>
 *
 * @author Lars Johansson
 */
public class HolderWipStructures {

    private HashMap<UUID, WipSystemGroup> systemGroups;
    private HashMap<UUID, WipSystem>      systems;
    private HashMap<UUID, WipSubsystem>   subsystems;

    private HashMap<UUID, WipDiscipline>  disciplines;
    private HashMap<UUID, WipDeviceGroup> deviceGroups;
    private HashMap<UUID, WipDeviceType>  deviceTypes;

    private boolean includeDeleted = true;

    /**
     * Public constructor to prepare containers for system and device structure content.
     *
     * @param holderIWipRepositories
     */
    public HolderWipStructures (HolderIWipRepositories holderIWipRepositories) {
        this(holderIWipRepositories, true);
    }

    /**
     * Public constructor to prepare containers for system and device structure content.
     *
     * @param holderIWipRepositories holder of references to repositories
     * @param includeDeleted include deleted structure entries
     */
    public HolderWipStructures (HolderIWipRepositories holderIWipRepositories, boolean includeDeleted) {
        List<WipSystemGroup> systemGroupsRead = null;
        List<WipSystem> systemsRead = null;
        List<WipSubsystem> subsystemsRead = null;
        List<WipDiscipline> disciplinesRead = null;
        List<WipDeviceGroup> deviceGroupsRead = null;
        List<WipDeviceType> deviceTypesRead = null;

        if (includeDeleted) {
            systemGroupsRead = holderIWipRepositories.systemGroupRepository().findLatest();
            systemsRead = holderIWipRepositories.systemRepository().findLatest();
            subsystemsRead = holderIWipRepositories.subsystemRepository().findLatest();
            disciplinesRead = holderIWipRepositories.disciplineRepository().findLatest();
            deviceGroupsRead = holderIWipRepositories.deviceGroupRepository().findLatest();
            deviceTypesRead = holderIWipRepositories.deviceTypeRepository().findLatest();
        } else {
            systemGroupsRead = holderIWipRepositories.systemGroupRepository().findLatestNotDeleted();
            systemsRead = holderIWipRepositories.systemRepository().findLatestNotDeleted();
            subsystemsRead = holderIWipRepositories.subsystemRepository().findLatestNotDeleted();
            disciplinesRead = holderIWipRepositories.disciplineRepository().findLatestNotDeleted();
            deviceGroupsRead = holderIWipRepositories.deviceGroupRepository().findLatestNotDeleted();
            deviceTypesRead = holderIWipRepositories.deviceTypeRepository().findLatestNotDeleted();
        }

        // initial capacity
        //     default load factor 0.75
        //     set at proper size to avoid rehashing
        //     size/0.75+1 (may be rounded downwards so possibly +2 instead)
        float loadFactor = 0.75f;
        this.systemGroups = new HashMap<>((int)(systemGroupsRead.size()/loadFactor + 2), loadFactor);
        this.systems      = new HashMap<>((int)(systemsRead.size()/loadFactor + 2), loadFactor);
        this.subsystems   = new HashMap<>((int)(subsystemsRead.size()/loadFactor + 2), loadFactor);
        this.disciplines  = new HashMap<>((int)(disciplinesRead.size()/loadFactor + 2), loadFactor);
        this.deviceGroups = new HashMap<>((int)(deviceGroupsRead.size()/loadFactor + 2), loadFactor);
        this.deviceTypes  = new HashMap<>((int)(deviceTypesRead.size()/loadFactor + 2), loadFactor);

        for (WipSystemGroup systemGroup : systemGroupsRead) {
            this.systemGroups.put(systemGroup.getUuid(), systemGroup);
        }
        for (WipSystem system : systemsRead) {
            this.systems.put(system.getUuid(), system);
        }
        for (WipSubsystem subsystem : subsystemsRead) {
            this.subsystems.put(subsystem.getUuid(), subsystem);
        }
        for (WipDiscipline discipline : disciplinesRead) {
            this.disciplines.put(discipline.getUuid(), discipline);
        }
        for (WipDeviceGroup deviceGroup : deviceGroupsRead) {
            this.deviceGroups.put(deviceGroup.getUuid(), deviceGroup);
        }
        for (WipDeviceType deviceType : deviceTypesRead) {
            this.deviceTypes.put(deviceType.getUuid(), deviceType);
        }
    }

    /**
     * Find system group name part by uuid.
     *
     * @param uuid uuid
     * @return system group name part
     */
    public WipSystemGroup findSystemGroupByUuid(UUID uuid) {
        return systemGroups.get(uuid);
    }
    /**
     * Find system name part by uuid.
     *
     * @param uuid uuid
     * @return system name part
     */
    public WipSystem findSystemByUuid(UUID uuid) {
        return systems.get(uuid);
    }
    /**
     * Find subsystem name part by uuid.
     *
     * @param uuid uuid
     * @return subsystem name part
     */
    public WipSubsystem findSubsystemByUuid(UUID uuid) {
        return subsystems.get(uuid);
    }

    /**
     * Find discipline name part by uuid.
     *
     * @param uuid uuid
     * @return discipline name part
     */
    public WipDiscipline findDisciplineByUuid(UUID uuid) {
        return disciplines.get(uuid);
    }
    /**
     * Find device group name part by uuid.
     *
     * @param uuid uuid
     * @return device group name part
     */
    public WipDeviceGroup findDeviceGroupByUuid(UUID uuid) {
        return deviceGroups.get(uuid);
    }
    /**
     * Find device type name part by uuid.
     *
     * @param uuid uuid
     * @return device type name part
     */
    public WipDeviceType findDeviceTypeByUuid(UUID uuid) {
        return deviceTypes.get(uuid);
    }

    /**
     * Return if include deleted structure entries.
     *
     * @return if include deleted structure entries
     */
    public boolean getIncludeDeleted() {
        return includeDeleted;
    }

    /**
     * Return map with key-value pairs (uuid and system group).
     *
     * @return map with key-value pairs (uuid and system group)
     */
    public Map<UUID, WipSystemGroup> getUuidSystemGroups() {
        return systemGroups;
    }

    /**
     * Return map with key-value pairs (uuid and system).
     *
     * @return map with key-value pairs (uuid and system)
     */
    public Map<UUID, WipSystem> getUuidSystems() {
        return systems;
    }

    /**
     * Return map with key-value pairs (uuid and subsystem).
     *
     * @return map with key-value pairs (uuid and subsystem)
     */
    public Map<UUID, WipSubsystem> getUuidSubsystems() {
        return subsystems;
    }

    /**
     * Return map with key-value pairs (uuid and discipline).
     *
     * @return map with key-value pairs (uuid and discipline)
     */
    public Map<UUID, WipDiscipline> getUuidDisciplines() {
        return disciplines;
    }

    /**
     * Return map with key-value pairs (uuid and device group).
     *
     * @return map with key-value pairs (uuid and device group)
     */
    public Map<UUID, WipDeviceGroup> getUuidDeviceGroups() {
        return deviceGroups;
    }

    /**
     * Return map with key-value pairs (uuid and device type).
     *
     * @return map with key-value pairs (uuid and device type)
     */
    public Map<UUID, WipDeviceType> getUuidDeviceTypes() {
        return deviceTypes;
    }

}

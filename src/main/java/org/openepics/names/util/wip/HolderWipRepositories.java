/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.wip;

import org.openepics.names.repository.wip.WipDeviceGroupRepository;
import org.openepics.names.repository.wip.WipDeviceTypeRepository;
import org.openepics.names.repository.wip.WipDisciplineRepository;
import org.openepics.names.repository.wip.WipNameRepository;
import org.openepics.names.repository.wip.WipSubsystemRepository;
import org.openepics.names.repository.wip.WipSystemGroupRepository;
import org.openepics.names.repository.wip.WipSystemRepository;

/**
 * Utility record to collect references to repositories.
 *
 * @author Lars Johansson
 */
public record HolderWipRepositories (
        WipNameRepository nameRepository,
        WipSystemGroupRepository systemGroupRepository,
        WipSystemRepository systemRepository,
        WipSubsystemRepository subsystemRepository,
        WipDisciplineRepository disciplineRepository,
        WipDeviceGroupRepository deviceGroupRepository,
        WipDeviceTypeRepository deviceTypeRepository) {}

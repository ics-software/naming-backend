/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.rest.beans.Type;
import org.springframework.util.CollectionUtils;

/**
 * Utility class to assist in handling of validation.
 *
 * @author Lars Johansson
 */
public class ValidateUtil {

    /**
     * This class is not to be instantiated.
     */
    private ValidateUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Validate description.
     *
     * @param description description
     */
    public static void validateInputDescription(String description) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(description),
                TextUtil.VALUE_IS_NOT_AVAILABLE, description, TextUtil.DESCRIPTION);
    }

    /**
     * Validate index.
     *
     * @param index index
     */
    public static void validateInputIndex(String index) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(index),
                TextUtil.VALUE_IS_NOT_AVAILABLE, index, TextUtil.INDEX);
    }

    /**
     * Validate mnemonic.
     *
     * @param mnemonic mnemonic
     */
    public static void validateInputMnemonic(String mnemonic) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(mnemonic),
                TextUtil.VALUE_IS_NOT_AVAILABLE, mnemonic, TextUtil.MNEMONIC);
    }

    /**
     * Validate mnemonic path.
     *
     * @param mnemonicPath mnemonic path
     */
    public static void validateInputMnemonicPath(String mnemonicPath) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(mnemonicPath),
                TextUtil.VALUE_IS_NOT_AVAILABLE, mnemonicPath, TextUtil.MNEMONICPATH);
    }

    /**
     * Validate name.
     *
     * @param name name
     */
    public static void validateInputName(String name) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(name),
                TextUtil.VALUE_IS_NOT_AVAILABLE, name, TextUtil.NAME);
    }

    /**
     * Validate type.
     *
     * @param type type
     */
    public static void validateInputType(Type type) {
        validateInputType(type, null);
    }

    /**
     * Validate type.
     *
     * @param type type
     * @param expected expected type
     */
    public static void validateInputType(Type type, Type expected) {
        // available
        ExceptionUtil.validateConditionInputNotAvailableException(type != null,
                TextUtil.VALUE_IS_NOT_AVAILABLE, null, TextUtil.TYPE);

        // expected type
        if (expected != null) {
            ExceptionUtil.validateConditionInputNotCorrectException(expected.equals(type),
                    TextUtil.VALUE_IS_NOT_CORRECT, type.toString(), TextUtil.TYPE);
        }
    }

    /**
     * Validate uuid.
     *
     * @param uuid uuid
     */
    public static void validateInputUuid(String uuid) {
        // available
        // correct
        ExceptionUtil.validateConditionInputNotAvailableException(!StringUtils.isEmpty(uuid),
                TextUtil.VALUE_IS_NOT_AVAILABLE, uuid, TextUtil.UUID);
        try {
            UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            throw ExceptionUtil.createInputNotCorrectException(TextUtil.VALUE_IS_NOT_CORRECT, uuid, TextUtil.UUID);
        }
    }

    public static void validateInputAvailable(boolean inputAvailable) {
        ExceptionUtil.validateConditionInputNotAvailableException(inputAvailable,
                TextUtil.VALUE_IS_NOT_AVAILABLE, null, null);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate that collection has given size.
     * In practice, validate that collection is not null and has given size.
     *
     * @param collection collection
     * @param size size
     * @return boolean if validation is ok
     */
    public static boolean isSize(Collection<?> collection, int size) {
        return collection != null && collection.size() == size;
    }

    /**
     * Validate that collection is null or empty.
     *
     * @param collection collection
     * @return boolean if validation is ok
     */
    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Validate that collection is null or does not have given size.
     *
     * @param collection collection
     * @param size size
     * @return boolean if validation is ok
     */
    public static boolean isNullOrNotSize(Collection<?> collection, int size) {
        return collection == null || collection.size() != size;
    }

    /**
     * Validate that list is equal to values.
     * In practice, validate that both are null or both have same length and same content
     *
     * @param <T> type
     * @param list list
     * @param values values
     * @return boolean if validation is ok
     */
    @SafeVarargs
    public static <T> boolean equals(List<T> list, T... values) {
        if (list == null && values == null) {
            return true;
        } else if (list != null
                && values != null
                && list.size() == values.length) {
            for (int i=0; i<list.size(); i++) {
                if (list.get(i) != null
                        && values[i] != null
                        && !list.get(i).equals(values[i])) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate that list is empty or is equal to values.
     * In practice, validate that list is not null and empty, or equal to values
     *
     * @param <T> type
     * @param list list
     * @param values values
     * @return boolean if validation is ok
     */
    @SafeVarargs
    public static <T> boolean isEmptyOrEquals(List<T> list, T... values) {
        return list != null && list.isEmpty()
                || equals(list, values);
    }

    /**
     * Validate given objects such that <tt>true</tt> is returned if any object is null.
     * This may be used to validate that given objects are not null.
     *
     * @param values values
     * @return boolean if validation is ok
     */
    public static boolean isAnyNull(Object... values) {
        if (values == null) {
            return true;
        }
        for (Object value : values) {
            if (value == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate given objects such that <tt>true</tt> is returned if all objects are null.
     * This may be used to validate that given objects are not null.
     *
     * @param values values
     * @return boolean if validation is ok
     */
    public static boolean areAllNull(Object... values) {
        if (values == null) {
            return true;
        }
        for (Object value : values) {
            if (value != null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate given enums such that <tt>true</tt> is returned if any enum in values is equal to given input enum.
     *
     * @param input input
     * @param values values
     * @return boolean if validation is ok
     */
    public static boolean isAnyEqual(Enum<?> input, Enum<?>... values) {
        if (input == null || values == null) {
            return false;
        }
        for (Enum<?> value : values) {
            if (input.equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate given values such that <tt>true</tt> is returned if any value is <tt>true</tt>.
     *
     * @param values values
     * @return if validation is ok
     */
    public static boolean isAnyTrue(boolean... values) {
        if (values == null) {
            return false;
        }
        for (boolean value : values) {
            if (value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Validate given ints such that <tt>true</tt> is returned if all ints are zero (0).
     * This may be used to validate that given ints are not zero.
     *
     * @param values values
     * @return boolean if validation is ok
     */
    public static boolean areAllZero(int... values) {
        if (values == null) {
            return false;
        }
        for (int value : values) {
            if (value != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns boolean if any of patterns match given input string.
     *
     * @param patterns list of patterns
     * @param input input string
     * @return boolean if any of patterns match given input string
     */
    public static boolean isAnyMatch(List<Pattern> patterns, String input) {
        if (CollectionUtils.isEmpty(patterns) || StringUtils.isEmpty(input)) {
            return false;
        }
        for (Pattern p : patterns) {
            if (p.matcher(input).find()) {
                return true;
            }
        }
        return false;
    }

}

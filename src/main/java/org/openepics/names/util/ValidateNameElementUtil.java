/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.element.NameElementCommand;

/**
 * Utility class to assist in handling of validation.
 *
 * @author Lars Johansson
 */
public class ValidateNameElementUtil {

    // note
    //     NameElement
    //         uuid,
    //         systemGroup, system, subsystem, deviceType, systemStructure, deviceStructure,
    //         index, description, name,
    //         status, deleted,
    //         when, who, comment
    //
    //     NameElementCommand
    //	       create -       parentSystemStructure, parentDeviceStructure, index, description
    //	       update - uuid, parentSystemStructure, parentDeviceStructure, index, description
    //	       delete - uuid

    /**
     * This class is not to be instantiated.
     */
    private ValidateNameElementUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Validate parameters for read names.
     *
     * @param deleted deleted
     * @param uuid uuid
     * @param name name
     * @param systemStructure system structure mnemonic
     * @param deviceStructure device structure mnemonic
     * @param index index
     * @param description description
     * @param who, who
     * @param includeHistory include history
     * @param orderBy order by
     * @param isAsc is ascending
     * @param offset offset
     * @param limit limit
     */
    public static void validateNamesInputRead(Boolean deleted,
            String uuid, String name, String systemStructure, String deviceStructure, String index, String description, String who,
            Boolean includeHistory, FieldName orderBy, Boolean isAsc, Integer offset, Integer limit) {

        // validate input
        //     uuid

        if (!StringUtils.isEmpty(uuid)) {
            ValidateUtil.validateInputUuid(uuid);
        }
    }

    /**
     * Validate name element command input for create.
     *
     * @param nameElementCommand name element command
     */
    public static void validateNameElementInputCreate(NameElementCommand nameElementCommand) {
        validateNameElementInput(nameElementCommand, NameCommand.CREATE);
    }

    /**
     * Validate name element command input for update.
     *
     * @param nameElementCommand name element command
     */
    public static void validateNameElementInputUpdate(NameElementCommand nameElementCommand) {
        validateNameElementInput(nameElementCommand, NameCommand.UPDATE);
    }

    /**
     * Validate name element command input for delete.
     *
     * @param nameElement name element command input for given task.
     */
    public static void validateNameElementInputDelete(NameElementCommand nameElementCommand) {
        validateNameElementInput(nameElementCommand, NameCommand.DELETE);
    }

    /**
     * Validate name element command input for given task.
     *
     * @param nameElement name element command
     * @param nameCommand name command
     */
    private static void validateNameElementInput(NameElementCommand nameElementCommand, NameCommand nameCommand) {
        // command - attributes
        //     create -       parentSystemStructure, parentDeviceStructure, index, description
        //	   update - uuid, parentSystemStructure, parentDeviceStructure, index, description
        //	   delete - uuid
        //
        //     create, update - optional - parentDeviceStructure, index

        if (ValidateUtil.isAnyNull(nameElementCommand, nameCommand)) {
            return;
        }

        UUID uuid = nameElementCommand.getUuid();
        UUID parentSystemStructure = nameElementCommand.getParentSystemStructure();
        UUID parentDeviceStructure = nameElementCommand.getParentDeviceStructure();
        String index = nameElementCommand.getIndex();
        String description = nameElementCommand.getDescription();

        // validate input
        //     update, delete
        //         uuid
        //     create, update
        //         parentSystemStructure
        //         parentDeviceStructure
        //         index
        //         description

        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.UPDATE, NameCommand.DELETE)) {
            ValidateUtil.validateInputUuid(uuid != null ? uuid.toString() : null);
        }

        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.CREATE, NameCommand.UPDATE)) {
            ExceptionUtil.validateConditionInputNotAvailableException(parentSystemStructure != null,
                    TextUtil.VALUE_IS_NOT_AVAILABLE, nameElementCommand.toString(), TextUtil.PARENTSYSTEMSTRUCTURE);

            // optional (either none or both)
            //     parentDeviceStructure
            // 	   index
            if (parentDeviceStructure != null && index == null) {
                throw ExceptionUtil.createInputNotCorrectException(TextUtil.VALUE_IS_NOT_CORRECT, nameElementCommand.toString(), TextUtil.INDEX);
            } else if (parentDeviceStructure == null && index != null) {
                throw ExceptionUtil.createInputNotCorrectException(TextUtil.VALUE_IS_NOT_CORRECT, nameElementCommand.toString(), TextUtil.PARENTDEVICESTRUCTURE);
            }

            ValidateUtil.validateInputDescription(description);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate name element command data for create.
     *
     * @param nameElementCommand name element command
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateNameElementDataCreate(NameElementCommand nameElementCommand, EssNamingConvention namingConvention,
            HolderIRepositories holderIRepositories, HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateNameElementData(nameElementCommand, NameCommand.CREATE, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }

    /**
     * Validate name element command data for update.
     *
     * @param nameElementCommand name element command
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateNameElementDataUpdate(NameElementCommand nameElementCommand, EssNamingConvention namingConvention,
            HolderIRepositories holderIRepositories, HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateNameElementData(nameElementCommand, NameCommand.UPDATE, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }

    /**
     * Validate name element command data for delete.
     *
     * @param nameElementCommand name element command
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    public static void validateNameElementDataDelete(NameElementCommand nameElementCommand, EssNamingConvention namingConvention,
            HolderIRepositories holderIRepositories, HolderRepositories holderRepositories, HolderStructures holderStructures) {
        validateNameElementData(nameElementCommand, NameCommand.DELETE, namingConvention,
                holderIRepositories, holderRepositories, holderStructures);
    }

    /**
     * Validate name element command data for given task.
     *
     * @param nameElementCommand name element command
     * @param nameCommand name command
     * @param namingConvention naming convention
     * @param holderIRepositories holder repositories
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     */
    private static void validateNameElementData(NameElementCommand nameElementCommand, NameCommand nameCommand, EssNamingConvention namingConvention,
            HolderIRepositories holderIRepositories, HolderRepositories holderRepositories, HolderStructures holderStructures) {
        // command - attributes
        //     create -       parentSystemStructure, parentDeviceStructure, index, description
        //	   update - uuid, parentSystemStructure, parentDeviceStructure, index, description
        //	   delete - uuid
        //
        //     create, update - optional - parentDeviceStructure, index
        //
        // name
        //      - system structure uuid
        //    ( - system structure uuid, device structure uuid )
        //      - system structure uuid, device structure uuid, index

        if (ValidateUtil.isAnyNull(nameElementCommand, nameCommand, namingConvention,
                holderIRepositories, holderRepositories, holderStructures)) {
            return;
        }
        if (ValidateUtil.isAnyNull(holderIRepositories.nameRepository())) {
            return;
        }

        String details = nameElementCommand.toString();

        UUID uuid = nameElementCommand.getUuid();
        UUID parentSystemstructure = nameElementCommand.getParentSystemStructure();
        UUID parentDevicestructure = nameElementCommand.getParentDeviceStructure();
        String index = nameElementCommand.getIndex();

        // validate data
        //     update, delete
        //         uuid available, not deleted
        //     create, update
        //         parentSystemstructure - systemgroup, system, subsystem
        //         parentDevicestructure - devicetype
        //         index                 - index, name

        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.UPDATE, NameCommand.DELETE)) {
            List<Name> names = holderRepositories.nameRepository().readNames(false, uuid.toString(), null, null, null, null, null, null, null);
            ExceptionUtil.validateConditionDataNotFoundException(ValidateUtil.isSize(names, 1),
                    TextUtil.DATA_IS_NOT_FOUND, details, TextUtil.UUID);
        }

        SystemGroup systemGroup = null;
        System      system      = null;
        Subsystem   subsystem   = null;
        DeviceType  deviceType  = null;

        int countDevicestructureIndex = 0;
        String derivedName = null;

        boolean condition = true;

        if (ValidateUtil.isAnyEqual(nameCommand, NameCommand.CREATE, NameCommand.UPDATE)) {
            // systemgroup, system, subsystem - in repository
            //     found
            //     not deleted
            // devicetype                     - possibly, in repository
            //     found
            //     not deleted
            // name                           - naming convention rules
            //     derive name from system structure, device structure, index
            // index                          - possibly, naming convention rules
            //     valid
            // name
            //     name not exists
            //     name equivalence not exists

            // find out system group, system, subsystem
            //     one of the three expected to be non-null, other two expected to be null
            systemGroup = holderIRepositories.systemGroupRepository().findByUuid(parentSystemstructure.toString());
            system      = holderIRepositories.systemRepository().findByUuid(parentSystemstructure.toString());
            subsystem   = holderIRepositories.subsystemRepository().findByUuid(parentSystemstructure.toString());

            // device structure
            if (parentDevicestructure != null) {
                countDevicestructureIndex++;
                deviceType = holderIRepositories.deviceTypeRepository().findByUuid(parentDevicestructure.toString());
                ExceptionUtil.validateConditionDataNotFoundException(deviceType != null,
                        TextUtil.DATA_IS_NOT_FOUND, details, TextUtil.PARENTDEVICESTRUCTURE);
                ExceptionUtil.validateConditionDataDeletedException(!deviceType.isDeleted(),
                        TextUtil.DATA_IS_DELETED, details, TextUtil.PARENTDEVICESTRUCTURE);
            }

            // system structure
            if (systemGroup != null) {
                ExceptionUtil.validateConditionDataDeletedException(!systemGroup.isDeleted(),
                        TextUtil.DATA_IS_DELETED, details, TextUtil.PARENTSYSTEMSTRUCTURE);
                derivedName = NameUtil.getName(systemGroup, deviceType, index, holderStructures);
            } else if (system != null) {
                ExceptionUtil.validateConditionDataDeletedException(!system.isDeleted(),
                        TextUtil.DATA_IS_DELETED, details, TextUtil.PARENTSYSTEMSTRUCTURE);
                derivedName = NameUtil.getName(system, deviceType, index, holderStructures);
            } else if (subsystem != null) {
                ExceptionUtil.validateConditionDataDeletedException(!subsystem.isDeleted(),
                        TextUtil.DATA_IS_DELETED, details, TextUtil.PARENTSYSTEMSTRUCTURE);
                derivedName = NameUtil.getName(subsystem, deviceType, index, holderStructures);
            } else {
                throw ExceptionUtil.createDataNotFoundException(TextUtil.DATA_IS_NOT_FOUND, details, TextUtil.PARENTSYSTEMSTRUCTURE);
            }

            // index
            if (!StringUtils.isEmpty(index)) {
                countDevicestructureIndex++;
                condition = namingConvention.isInstanceIndexValid(derivedName, false);
                ExceptionUtil.validateConditionDataNotValidException(condition,
                        TextUtil.DATA_IS_NOT_VALID, details, TextUtil.INDEX);
            }

            condition = countDevicestructureIndex == 0 || countDevicestructureIndex == 2;
            ExceptionUtil.validateConditionDataNotCorrectException(condition,
                    TextUtil.DEVICETYPE_OR_INDEX_IS_NOT_AVAILABLE, details, TextUtil.PARENTDEVICESTRUCTURE);

            // name
            //     ok with same name, name equivalence if same uuid
            List<Name> names = holderRepositories.nameRepository().readNames(false, null, derivedName, null, null, null, null, null, null);
            if (NameCommand.CREATE.equals(nameCommand)) {
                condition = ValidateUtil.isNullOrEmpty(names);
            } else {
                // NameCommand.UPDATE
                condition = ValidateUtil.isNullOrEmpty(names) || names.size() == 1 && names.get(0).getUuid().equals(uuid);
            }
            ExceptionUtil.validateConditionDataConflictException(condition,
                    TextUtil.CONVENTION_NAME_EXISTS, details, TextUtil.PARENTSYSTEMSTRUCTURE);

            names = holderRepositories.nameRepository().readNames(false, null, null, namingConvention.equivalenceClassRepresentative(derivedName), null, null, null, null, null);
            if (NameCommand.CREATE.equals(nameCommand)) {
                condition = ValidateUtil.isNullOrEmpty(names);
            } else {
                // NameCommand.UPDATE.equals(nameCommand)
                condition = ValidateUtil.isNullOrEmpty(names) || names.size() == 1 && names.get(0).getUuid().equals(uuid);
            }
            ExceptionUtil.validateConditionDataConflictException(condition,
                    TextUtil.CONVENTION_NAME_EQUIVALENCE_EXISTS, details, TextUtil.PARENTSYSTEMSTRUCTURE);
        } else if (NameCommand.DELETE.equals(nameCommand)) {
            // n.a.
            //     uuid    - already handled
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Validate name data.
     * This method corresponds to name element data for create, albeit in a different way.
     *
     * @param name name
     * @param namingConvention naming convention
     * @param holderRepositories holder repositories
     * @param holderStructures holder of system and device structure content
     *
     * @see ValidateNameElementUtil#validateNameElementDataCreate(NameElementCommand, EssNamingConvention,
     *         HolderIRepositories, HolderRepositories, HolderStructures)
     */
    public static void validateNameDataCreate(String name, EssNamingConvention namingConvention,
            HolderRepositories holderRepositories, HolderStructures holderStructures) {
        if (ValidateUtil.isAnyNull(name, namingConvention, holderRepositories, holderStructures)) {
            return;
        }

        String details = name;
        String field = TextUtil.NAME;

        // find out system group, system, subsystem       + check if valid
        // find out discipline, device group, device type + check if valid
        // find out instance index                        + check if valid
        // check name equivalence

        String sg  = NamingConventionUtil.extractSystemGroup(name);
        String sys = NamingConventionUtil.extractSystem(name);
        String sub = NamingConventionUtil.extractSubsystem(name);
        String dt  = NamingConventionUtil.extractDeviceType(name);
        String idx = NamingConventionUtil.extractInstanceIndex(name);

        String mnemonicPathSystemStructure = NamingConventionUtil.extractMnemonicPathSystemStructure(name);
        String mnemonicPathDeviceStructure = NamingConventionUtil.extractMnemonicPathDeviceStructure(name);

        int count = 0;
        int countSgSys = 0;
        int countSgSub = 0;
        if (sg != null) {
            count++;
            countSgSys++;
            countSgSub++;
        }
        if (sys != null) {
            count++;
            countSgSys++;
        }
        if (sub != null) {
            count++;
            countSgSub++;
        }

        // one or two system structure mnemonics + not system group and system + not system group and subsystem
        if (count < 1 || count > 2 || countSgSys == 2 || countSgSub == 2) {
            throw ExceptionUtil.createDataNotCorrectException(TextUtil.SYSTEM_STRUCTURE_IS_NOT_CORRECT, details, field);
        }

        // ensure that system structure parents and device structure parents are available and not deleted
        //     if device type
        //     if system group
        //     else
        //         if system
        //         else
        //             (if not system then error)
        //         if subsystem

        SystemGroup systemGroup = null;
        System      system      = null;
        Subsystem   subsystem   = null;
        DeviceType  deviceType  = null;

        String derivedName = null;

        // device structure
        if (!StringUtils.isEmpty(dt)) {
            List<DeviceType> deviceTypes = holderRepositories.deviceTypeRepository().readDeviceTypes(false, null, null, null, null, mnemonicPathDeviceStructure, null, null);
            ExceptionUtil.validateConditionDataNotFoundException(ValidateUtil.isSize(deviceTypes, 1),
                    TextUtil.DEVICE_STRUCTURE_IS_NOT_FOUND, details, field);
            deviceType = deviceTypes.get(0);
        }

        // system structure
        List<Subsystem>   subsystems   = holderRepositories.subsystemRepository().readSubsystems(false, null, null, null, null, mnemonicPathSystemStructure, null, null);
        List<System>      systems      = holderRepositories.systemRepository().readSystems(false, null, null, null, null, mnemonicPathSystemStructure, null, null);
        List<SystemGroup> systemGroups = holderRepositories.systemGroupRepository().readSystemGroups(false, null, null, null, mnemonicPathSystemStructure, null, null);

        if (ValidateUtil.isSize(subsystems, 1)) {
            subsystem = subsystems.get(0);
            derivedName = NameUtil.getName(subsystem, deviceType, NamingConventionUtil.extractInstanceIndex(name), holderStructures);
        } else if (ValidateUtil.isSize(systems, 1)) {
            system = systems.get(0);
            derivedName = NameUtil.getName(system, deviceType, NamingConventionUtil.extractInstanceIndex(name), holderStructures);
        } else if (ValidateUtil.isSize(systemGroups, 1)) {
            systemGroup = systemGroups.get(0);
            derivedName = NameUtil.getName(systemGroup, deviceType, NamingConventionUtil.extractInstanceIndex(name), holderStructures);
        } else  {
            throw ExceptionUtil.createDataNotFoundException(TextUtil.SYSTEM_STRUCTURE_IS_NOT_FOUND, details, field);
        }

        // index
        if (!StringUtils.isEmpty(idx)) {
            ExceptionUtil.validateConditionDataNotValidException(namingConvention.isInstanceIndexValid(name, false),
                    TextUtil.INDEX_IS_NOT_VALID, details, field);
        }

        // name
        //     name corresponds to derived name
        //     convention name not exists
        //     convention name equivalence not exists
        ExceptionUtil.validateConditionDataNotCorrectException(StringUtils.equals(name, derivedName),
                TextUtil.CONVENTION_NAME_IS_NOT_CORRECT, details, field);

        List<Name> names = holderRepositories.nameRepository().readNames(false, null, name, null, null, null, null, null, null);
        ExceptionUtil.validateConditionDataConflictException(names.isEmpty(),
                TextUtil.CONVENTION_NAME_EXISTS, details, field);

        names = holderRepositories.nameRepository().readNames(false, null, null, namingConvention.equivalenceClassRepresentative(name), null, null, null, null, null);
        ExceptionUtil.validateConditionDataConflictException(names.isEmpty(),
                TextUtil.CONVENTION_NAME_EQUIVALENCE_EXISTS, details, field);
    }

}

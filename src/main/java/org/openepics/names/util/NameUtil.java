/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.DeviceType;
import org.openepics.names.repository.model.Subsystem;
import org.openepics.names.repository.model.System;
import org.openepics.names.repository.model.SystemGroup;

/**
 * Utility class to assist in handling of name content.
 *
 * @author Lars Johansson
 */
public class NameUtil {

    /**
     * This class is not to be instantiated.
     */
    private NameUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Return convention name for system and device structure mnemonic paths and index.
     *
     * @param systemStructure system structure mnemonic path
     * @param deviceStructure device structure mnemonic path
     * @param index index
     * @return convention name
     */
    public static String getName(String systemStructure, String deviceStructure, String index) {
        return !StringUtils.isEmpty(systemStructure)
                ? !StringUtils.isEmpty(deviceStructure) && !StringUtils.isEmpty(index)
                    ? systemStructure + NamingConventionUtil.DELIMITER_EXTRA + deviceStructure + NamingConventionUtil.DELIMITER_INTRA + index
                    : systemStructure
                : null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return convention name given system group, device type and index.
     *
     * @param systemGroup system group
     * @param deviceType device type
     * @param index index
     * @param holderStructures container for system and device structure content
     * @return convention name
     */
    public static String getName(SystemGroup systemGroup, DeviceType deviceType, String index, HolderStructures holderStructures) {
        return getName(
                StructureUtil.getMnemonicPath(systemGroup, holderStructures),
                StructureUtil.getMnemonicPath(deviceType, holderStructures),
                index);
    }

    /**
     * Return convention name given system, device type and index.
     *
     * @param system
     * @param deviceType device type
     * @param index index
     * @param holderStructures container for system and device structure content
     * @return convention name
     */
    public static String getName(System system, DeviceType deviceType, String index, HolderStructures holderStructures) {
        return getName(
                StructureUtil.getMnemonicPath(system, holderStructures),
                StructureUtil.getMnemonicPath(deviceType, holderStructures),
                index);
    }

    /**
     * Return convention name given subsystem, device type and index.
     *
     * @param subsystem
     * @param deviceType device type
     * @param index index
     * @param holderStructures container for system and device structure content
     * @return convention name
     */
    public static String getName(Subsystem subsystem, DeviceType deviceType, String index, HolderStructures holderStructures) {
        return getName(
                StructureUtil.getMnemonicPath(subsystem, holderStructures),
                StructureUtil.getMnemonicPath(deviceType, holderStructures),
                index);
    }

}

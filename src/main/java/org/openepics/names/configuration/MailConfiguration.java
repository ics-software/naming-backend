/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Set up email configuration for Naming backend.
 *
 * @author Lars Johansson
 */
@Configuration
public class MailConfiguration {

    @Value("${naming.smtp.host}")
    String namingSmtpHost;
    @Value("${naming.smtp.port}")
    int namingSmtpPort;
    @Value("${naming.smtp.username}")
    String namingSmtpUsername;
    @Value("${naming.smtp.password}")
    String namingSmtpPassword;

    @Value("${spring.mail.properties.mail.smtp.auth}")
    String springMailPropertiesMailSmtpAuth;
    @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
    String springMailPropertiesMailSmtpStarttlsEnable;
    @Value("${spring.mail.properties.mail.smtp.connectiontimeout}")
    String springMailPropertiesMailSmtpConnectiontimeout;
    @Value("${spring.mail.properties.mail.smtp.timeout}")
    String springMailPropertiesMailSmtpTimeout;
    @Value("${spring.mail.properties.mail.smtp.writetimeout}")
    String springMailPropertiesMailSmtpWritetimeout;
    @Value("${spring.mail.debug}")
    String springMailDebug;

    /**
     * Set up email configuration with sender.
     */
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(namingSmtpHost);
        mailSender.setPort(namingSmtpPort);
        mailSender.setUsername(namingSmtpUsername);
        mailSender.setPassword(namingSmtpPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", springMailPropertiesMailSmtpAuth);
        props.put("mail.smtp.starttls.enable", springMailPropertiesMailSmtpStarttlsEnable);
        props.put("mail.smtp.connectiontimeout", springMailPropertiesMailSmtpConnectiontimeout);
        props.put("mail.smtp.timeout", springMailPropertiesMailSmtpTimeout);
        props.put("mail.smtp.writetimeout", springMailPropertiesMailSmtpTimeout);
        props.put("mail.debug", springMailDebug);

        return mailSender;
    }

}

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.configuration;

import java.util.Arrays;
import java.util.List;

import org.openepics.names.rest.filter.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpMethod;
import org.springframework.security.authorization.method.AuthorizationManagerBeforeMethodInterceptor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import com.google.common.collect.ImmutableList;

/**
 * Set up security configuration for Naming backend.
 *
 * @author Lars Johansson
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = false)
public class SecurityConfiguration {

    // note
    //     ability to run application with and without security
    //     spring and swagger work together
    //     spring
    //         @EnableMethodSecurity
    //         @PreAuthorize
    //         +
    //         AuthorizationManagerBeforeMethodInterceptor
    //         SecurityFilterChain
    //     swagger
    //         @SecurityRequirement

    private static final String API_V1_AUTHENTICATION   = "/api/v1/authentication";
    private static final String API_V1_NAMES            = "/api/v1/names";
    private static final String API_V1_NAMES_PATHS      = "/api/v1/names/*";
    private static final String API_V1_STRUCTURES       = "/api/v1/structures";
    private static final String API_V1_STRUCTURES_PATHS = "/api/v1/structures/*";
    private static final String API_V1_PV_NAMES         = "/api/v1/pvNames";
    private static final String API_V1_CONVERT          = "/api/v1/convert";

    private static final String ACTUATOR                = "/actuator";
    private static final String ACTUATOR_PATHS          = "/actuator/**";
    private static final String HEALTHCHECK             = "/healthcheck";
    private static final String REPORT_PATHS            = "/report/**";

    public static final String ROLE_ADMINISTRATOR       = "NamingAdministrator";
    public static final String ROLE_USER                = "NamingUser";
    public static final String IS_ADMINISTRATOR         = "hasAuthority('" + ROLE_ADMINISTRATOR + "')";
    public static final String IS_USER                  = "hasAuthority('" + ROLE_USER + "')";
    public static final String IS_ADMINISTRATOR_OR_USER = IS_ADMINISTRATOR + " or " + IS_USER;

    private static final List<String> ALLOWED_ROLES_TO_LOGIN =
            Arrays.asList(
                    SecurityConfiguration.ROLE_ADMINISTRATOR,
                    SecurityConfiguration.ROLE_USER);

    @Value("${naming.security.enabled:false}")
    private boolean namingSecurityEnabled;
    @Value("${springdoc.api-docs.path}")
    private String apiDocsPath;
    @Value("${springdoc.swagger-ui.path}")
    private String swaggerUiPath;

    private final UserDetailsService jwtUserDetailsService;
    private final JwtRequestFilter jwtRequestFilter;

    @Autowired
    public SecurityConfiguration(
            UserDetailsService jwtUserDetailsService,
            JwtRequestFilter jwtRequestFilter) {
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtRequestFilter = jwtRequestFilter;
    }

    public static List<String> getAllowedRolesToLogin() {
        return ImmutableList.copyOf(ALLOWED_ROLES_TO_LOGIN);
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    @ConditionalOnProperty(name = "naming.security.enabled", havingValue = "true")
    static AuthorizationManagerBeforeMethodInterceptor preAuthorizeAuthorizationMethodInterceptor() {
        return AuthorizationManagerBeforeMethodInterceptor.preAuthorize();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
      // configure AuthenticationManager so that it knows from where to load
      // user for matching credentials
      // Use BCryptPasswordEncoder
      auth.userDetailsService(jwtUserDetailsService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // disable csrf since internal
        // authentication and authorization
        //     create, read, update, delete
        //     not needed for read but needed for others, user for names, admin for structures
        //     accept requests to
        //         openapi, swagger
        //         healthcheck
        //         login, logout
        // patterns that are not mentioned are to be authenticated
        // stateless session as session not used to store user's state

        // TODO naming-convention-tool
        //     about
        //         paths to be removed when such functionality is removed
        //         /verification to verify data migration
        //         /rest rest api for comparison
        //     paths
        //         /verification/**
        //         /rest/deviceNames/**
        //         /rest/healthcheck
        //         /rest/history/**
        //         /rest/parts/**

        http
            .csrf().disable()
            .cors();

        if (namingSecurityEnabled) {
            http
                .authorizeRequests()
                    // api docs, swagger
                    // authentication - login, logout
                    // names - read
                    // structures - read
                    // other
                    .mvcMatchers("/v3/api-docs/**", apiDocsPath + "/**", "/api/swagger-ui/**", "/swagger-ui/**", swaggerUiPath).permitAll()
                    .mvcMatchers(HttpMethod.POST,   API_V1_AUTHENTICATION + "/login").permitAll()
                    .mvcMatchers(HttpMethod.GET,    API_V1_NAMES, API_V1_NAMES_PATHS).permitAll()
                    .mvcMatchers(HttpMethod.GET,    API_V1_STRUCTURES, API_V1_STRUCTURES_PATHS).permitAll()
                    .mvcMatchers(HttpMethod.GET,    API_V1_PV_NAMES).permitAll()
                    .mvcMatchers(HttpMethod.GET,    API_V1_CONVERT).permitAll()
                    .mvcMatchers(HttpMethod.GET,    REPORT_PATHS).permitAll()
                    .mvcMatchers(HttpMethod.GET,    HEALTHCHECK).permitAll()
                    .mvcMatchers(HttpMethod.GET,    ACTUATOR, ACTUATOR_PATHS).permitAll()
                    // naming-convention-tool
                    .mvcMatchers(HttpMethod.GET,    "/verification/**").permitAll()
                    .mvcMatchers(HttpMethod.GET,    "/rest/deviceNames/**").permitAll()
                    .mvcMatchers(HttpMethod.GET,    "/rest/healthcheck").permitAll()
                    .mvcMatchers(HttpMethod.GET,    "/rest/history/**").permitAll()
                    .mvcMatchers(HttpMethod.GET,    "/rest/parts/**").permitAll()
                    // any other requests to be authenticated
                    .anyRequest().authenticated()
                // add a filter to validate the tokens with every request
                .and()
                .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        return http.build();
    }

    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        // allow % that is URL encoded %25 may be in path
        // see org.springframework.security.web.firewall.StrictHttpFirewall
        return web -> web.httpFirewall(allowUrlEncodedPercenthHttpFirewall());
    }

    private HttpFirewall allowUrlEncodedPercenthHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        return firewall;
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.exception.handler.GlobalControllerExceptionHandler;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.service.LogService;
import org.openepics.names.service.security.JwtTokenService;
import org.openepics.names.service.security.JwtUserDetailsService;
import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.util.EncryptUtil;
import org.openepics.names.service.security.util.SecurityTextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.fusionauth.jwt.JWTExpiredException;

/**
 * Purpose of class to intercept rest calls and handle cookies and tokens in uniformed manner.
 *
 * @author <a href="mailto:imre.toth@ess.eu">Imre Toth</a>
 */
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = Logger.getLogger(JwtRequestFilter.class.getName());

    private final JwtUserDetailsService jwtUserDetailsService;
    private final JwtTokenService jwtTokenService;
    private final EncryptUtil encryptUtil;
    private final GlobalControllerExceptionHandler globalControllerExceptionHandler;
    private final LogService logService;

    @Autowired
    public JwtRequestFilter(
            JwtUserDetailsService jwtUserDetailsService,
            JwtTokenService jwtTokenService,
            EncryptUtil encryptUtil,
            GlobalControllerExceptionHandler globalControllerExceptionHandler,
            LogService logService) {
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtTokenService = jwtTokenService;
        this.encryptUtil = encryptUtil;
        this.globalControllerExceptionHandler = globalControllerExceptionHandler;
        this.logService = logService;
    }

    private static class TokenInfo {
        private String userName;
        private String rbacToken;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getRbacToken() {
            return rbacToken;
        }

        public void setRbacToken(String rbacToken) {
            this.rbacToken = rbacToken;
        }
    }

    private TokenInfo extractTokenInfo(String jwtToken) {
        TokenInfo result = new TokenInfo();
        try {
            result.setUserName(jwtTokenService.getUsernameFromToken(jwtToken));
            result.setRbacToken(encryptUtil.decrypt(jwtTokenService.getRBACTokenFromToken(jwtToken)));
        } catch (JWTExpiredException e) {
            // not log as it may be considerable amount of exceptions
            // otherwise message: JWT Token has expired
            logService.logException(LOGGER, Level.WARNING, e, "JWT Token has expired");
            return null;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e, "Unable to get info from JWT Token");
            return null;
        }

        return result;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain)
                    throws ServletException, IOException {

        final String requestTokenHeader = httpServletRequest.getHeader("Authorization");

        String jwtToken = null;
        TokenInfo tokenInfo = null;
        try {
            // JWT Token is in the form "Bearer token". Remove Bearer word and get
            // only the Token
            if (requestTokenHeader != null) {
                if (requestTokenHeader.startsWith(SecurityTextUtil.AUTHENTICATION_HEADER_PREFIX)) {
                    jwtToken = requestTokenHeader.substring(SecurityTextUtil.AUTHENTICATION_HEADER_PREFIX.length());
                } else {
                    jwtToken = requestTokenHeader;
                }

                tokenInfo = extractTokenInfo(jwtToken);
            } else {
                Cookie cookie = WebUtils.getCookie(httpServletRequest, SecurityTextUtil.COOKIE_AUTH_HEADER);

                if (cookie != null) {
                    String cookieValue = cookie.getValue();
                    if (StringUtils.isNotEmpty(cookieValue)) {
                        jwtToken = cookieValue;
                        tokenInfo = extractTokenInfo(cookieValue);
                    }
                }
            }

            // Once we get the token validate it.
            if (tokenInfo != null
                    && tokenInfo.getRbacToken() != null
                    && SecurityContextHolder.getContext().getAuthentication() == null) {

                UserDetails userDetails = this.jwtUserDetailsService.loadUserByToken(tokenInfo.getRbacToken());

                // if token is valid configure Spring Security to manually set
                // authentication
                if (jwtTokenService.validateToken(jwtToken, userDetails)) {

                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                            new UsernamePasswordAuthenticationToken(
                                    userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                    // After setting the Authentication in the context, we specify
                    // that the current user is authenticated. So it passes the
                    // Spring Security Configurations successfully.
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        } catch (RuntimeException e) {
            // MyObject is whatever the output of the below method
            ResponseEntity<Response> objectResponseEntity = null;
            try {
                objectResponseEntity = globalControllerExceptionHandler.handleConflict(e, new ServletWebRequest(httpServletRequest));

                // set the response object
                httpServletResponse.setStatus(objectResponseEntity.getStatusCode().value());
                httpServletResponse.setContentType("application/json");

                // pass down the actual obj that exception handler normally send
                ObjectMapper mapper = new ObjectMapper();
                PrintWriter out = httpServletResponse.getWriter();
                out.print(mapper.writeValueAsString(objectResponseEntity.getBody()));
                out.flush();

                return;
            } catch (Exception ex) {
                logService.logException(LOGGER, Level.WARNING, ex, "JWT Request filter processing error");
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}

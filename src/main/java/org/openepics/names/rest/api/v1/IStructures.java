/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import java.util.List;

import org.openepics.names.configuration.SecurityConfiguration;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.service.security.dto.UserDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides structures data for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "2. Structures",
     description = "handle structures data for Naming application")
@RequestMapping("/api/v1/structures")
public interface IStructures {

    /*
       StructureElementCommandCreate  -       type, parent, mnemonic, ordering, description
       StructureElementCommandUpdate  - uuid, type, parent, mnemonic, ordering, description
       StructureElementCommandConfirm - uuid, type
       ----------------------------------------------------------------------------------------------------
       converted into StructureElementCommand
       ----------------------------------------------------------------------------------------------------
       StructureElementCommand - subset of StructureElement attributes
       StructureElement
                          uuid            (UUID)
                          type            (Type)
                          parent          (UUID)
                          mnemonic        (String)
                          ordering        (String)
                          description     (String)
                          -------------------------
                          mnemonicPath    (String)
                          level           (Integer)
                          status          (Status)
                          deleted         (Boolean)
                          when            (Date)
                          who             (String)
                          comment         (String)

       Methods
           create    POST   /structures                                       - createStructures
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures                                       - readStructures
           read      GET    /structures/{uuid}                                - readStructure
           read      GET    /structures/children/{uuid}                       - readStructuresChildren
           read      GET    /structures/history/{uuid}                        - readStructuresHistory
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures/exists/{type}/{mnemonicPath}          - existsStructure                hidden from Swagger UI
           read      GET    /structures/isValidToCreate/{type}/{mnemonicPath} - isValidToCreateStructure       hidden from Swagger UI
           ----------------------------------------------------------------------------------------------------
           read      GET    /structures/validateCreate                        - validateStructuresCreate       hidden from Swagger UI
           read      GET    /structures/validateUpdate                        - validateStructuresUpdate       hidden from Swagger UI
           read      GET    /structures/validateDelete                        - validateStructuresDelete       hidden from Swagger UI
           ----------------------------------------------------------------------------------------------------
           update    PUT    /structures                                       - updateStructures
           ----------------------------------------------------------------------------------------------------
           delete    DELETE /structures                                       - deleteStructures

       Note
           authentication/authorization
               3 levels - no, user, administrator
                   no            - read
                 ( user )
                   administrator - create, update, delete
           default for read is to have lifecycle attributes unless query for specific resource or specific use case
               status, deleted
           mnemonic path for structure includes all levels of mnemonics
     */

    public static final String DEFAULT_PAGE            = "0";
    public static final String DEFAULT_PAGE_SIZE       = "100";
    public static final String DEFAULT_SORT_FIELD_WHEN = "WHEN";
    public static final String DEFAULT_SORT_ORDER_ASC  = "true";

    /**
     * Create (propose) structures by list of structure elements.
     * Return list of structure elements for created structures (proposals).
     *
     * @param structureElementCommands list of structure elements
     * @return list of structure elements for created structures (proposals)
     */
    @Operation(
            summary     = "Create (propose) structures by array of structure elements",
            description = """
                          Create (propose) structures by array of structure elements.
                          Return array of structure elements for created structures (proposals).

                          Required attributes:
                          - type
                          - description

                          Optional attributes:
                          - parent
                          - mnemonic
                          - ordering

                          Note
                          - Uuid is created by Naming
                          - Parent is required for System, Subsystem, DeviceGroup, DeviceType
                          - Mnemonic is required for System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description  = "OK. Return array of structure elements for created structures (proposals).",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "409",
                    description  = "Conflict. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR)
    public ResponseEntity<List<StructureElement>> createStructures(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandCreate.class,
                                            requiredProperties = {"type","description"}))),
                    required = true)
            @RequestBody List<StructureElementCommandCreate> structureElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Find valid structures (search).
     * Return list of structure elements.
     * Use deleted (false) to query for active structures.
     *
     * @param type type of structure
     * @param deleted if deleted structures are to be selected or not, omitted for both deleted and non-deleted structures
     * (true for deleted structures, false for non-deleted structures, omitted for both deleted and non-deleted structures)
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param mnemonicPath mnemonic path
     * @param description description
     * @param who who
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param page page starting from 0, offset
     * @param pageSize page size, limit
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid structures (search)",
            description = """
                          Find valid structures (search).
                          Return array of structure elements.

                          Use deleted (false) to query for active structures.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of structure elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageStructureElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            produces = {"application/json"})
    public ResponsePageStructureElements readStructures(
            @Parameter(in = ParameterIn.QUERY, description = "search by type of structure") @RequestParam(required = false) Type type,
            @Parameter(in = ParameterIn.QUERY, description = "if deleted structures are to be selected or not, omitted for both deleted and non-deleted structures") @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "search by parent uuid") @RequestParam(required = false) String parent,
            @Parameter(in = ParameterIn.QUERY, description = "search by mnemonic") @RequestParam(required = false) String mnemonic,
            @Parameter(in = ParameterIn.QUERY, description = "search by mnemonic path") @RequestParam(required = false) String mnemonicPath,
            @Parameter(in = ParameterIn.QUERY, description = "search by description") @RequestParam(required = false) String description,
            @Parameter(in = ParameterIn.QUERY, description = "search by who") @RequestParam(required = false) String who,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "page starting from 0, offset") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @Parameter(in = ParameterIn.QUERY, description = "page size, limit") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize);

    /**
     * Find valid structure by uuid (exact match).
     * Return list of structure elements.
     *
     * @param uuid uuid
     * @return list of structure elements.
     */
    @Operation(
            summary     = "Find valid structure by uuid (exact match)",
            description = """
                          Find valid structure by uuid (exact match).
                          Return list of structure elements.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return list of structure elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageStructureElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageStructureElements>  readStructure(
            @Parameter(in = ParameterIn.PATH, description = "find by uuid") @PathVariable("uuid") String uuid);

    /**
     * Find valid children structures by parent uuid (exact match).
     * Return list of structure elements.
     * Use deleted (false) to query for active structures.
     *
     * @param uuid uuid
     * @param deleted if deleted structures are to be selected or not, omitted for both deleted and non-deleted structures
     * (true for deleted structures, false for non-deleted structures, omitted for both deleted and non-deleted structures)
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param page page starting from 0, offset
     * @param pageSize page size, limit
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find valid children structures by parent uuid (exact match)",
            description = """
                          Find valid children structures by parent uuid (exact match).
                          Return array of structure elements.

                          Use deleted (false) to query for active structures.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of structure elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageStructureElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/children/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageStructureElements> readStructuresChildren(
            @Parameter(in = ParameterIn.PATH,  description = "find by uuid") @PathVariable("uuid") String uuid,
            @Parameter(in = ParameterIn.QUERY, description = "if deleted structures are to be selected or not, omitted for both deleted and non-deleted structures", required = false) @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldStructure orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "page starting from 0, offset") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @Parameter(in = ParameterIn.QUERY, description = "page size, limit") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize);

    /**
     * Find history for structure by uuid (exact match).
     * Return list of structure elements.
     * History consists of line of uuid.
     * The line of uuid is not broken in retrieving history.
     * If the uuid is found in a structure entry, the entire line of uuid is returned.
     *
     * @param uuid uuid
     * @return list of structure elements
     */
    @Operation(
            summary     = "Find history for structure by uuid (exact match)",
            description = """
                          Find history for structure by uuid (exact match).
                          Return array of structure elements.

                          History consists of line of uuid.
                          The line of uuid is not broken in retrieving history.
                          If the uuid is found in a structure entry, the entire line of uuid is returned.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of structure elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageStructureElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/history/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageStructureElements> readStructuresHistory(
            @Parameter(in = ParameterIn.PATH,  description = "find by uuid") @PathVariable("uuid") String uuid);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if mnemonic path exists in structure (exact match).
     *
     * <p>
     * Returned object has three fields (response, message, details).
     * <ul>
     * <li>response: boolean (true/false)</li>
     * <li>message: reason, if method fails</li>
     * <li>details: details, if method fails</li>
     * </ul>
     * </p>
     *
     * <p>
     * Note that method will always return false for DeviceGroup. Because DeviceGroup does not have mnemonic,
     * its mnemonic path is the mnemonic of its parent (Discipline).  However a query for mnemonic path
     * for DeviceGroup is ambiguous as it lacks mnemonic. Purpose of method is to check is a particular entry
     * exists in hierarchy of structures. Purpose of method is not to check if multiple entries match mnemonic path.
     * Therefor method returns false for DeviceGroup.
     * </p>
     *
     * @param type type of structure to search in
     * @param mnemonicPath mnemonic path to find structure for
     * @return if mnemonic path exists in structure
     */
    @Hidden
    @Operation(
            summary     = "Return if mnemonic path exists in structure (exact match)",
            description = """
                          Return if mnemonic path exists in structure (exact match).
                          Response is true if mnemonic path exists, false otherwise.

                          Note that method will always return false for DeviceGroup. Because DeviceGroup does not have mnemonic,
                          its mnemonic path is the mnemonic of its parent (Discipline).  However a query for mnemonic path
                          for DeviceGroup is ambiguous as it lacks mnemonic. Purpose of method is to check is a particular entry
                          exists in hierarchy of structures. Purpose of method is not to check if multiple entries match mnemonic path.
                          Therefor method returns false for DeviceGroup.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if mnemonic path exists, false otherwise.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/exists/{type}/{mnemonicPath}")
    public ResponseEntity<ResponseBoolean> existsStructure(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to find structure for") @PathVariable("mnemonicPath") String mnemonicPath);

    /**
     * Return if mnemonic path is valid to create in structure (exact match).
     * Method answers question 'would it be ok to create given mnemonic path in structure?'.
     *
     * <p>
     * Note that method can not fully answer question posed above. One reason is that system group
     * may have empty mnemonic. Another reason is that device groups has no mnemonic and is between
     * discipline and device type. Thus it can not be found which discipline (uuid-wise) that
     * a device type belongs.
     * </p>
     *
     * @param type type of structure to search in
     * @param mnemonicPath mnemonic path to find structure for
     * @return if mnemonic path is valid to create in structure
     */
    @Hidden
    @Operation(
            summary     = "Return if mnemonic path is valid to create in structure",
            description = """
                          Return if mnemonic path is valid to create in structure.
                          Method answers question 'would it be ok to create given mnemonic path in structure?'.

                          Note that method can not fully answer question posed above. One reason is that system group
                          may have empty mnemonic. Another reason is that device groups has no mnemonic and is
                          between discipline and device type. Thus it can not be found which discipline (uuid-wise)
                          that a device type belongs.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if structure is valid to create, false otherwise.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/isValidToCreate/{type}/{mnemonicPath}",
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isValidToCreateStructure(
            @Parameter(in = ParameterIn.PATH,  description = "type of structure to search in") @PathVariable("type") Type type,
            @Parameter(in = ParameterIn.PATH,  description = "mnemonic path to find structure for") @PathVariable("mnemonicPath") String mnemonicPath);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if structures are valid to create (propose) by list of structure elements.
     * If structures are valid to create, successful create of structures can be expected.
     *
     * <p>
     * Returned object has five fields (message, details, field, value, list).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>field:     field, if method fails</li>
     * <li>value:     boolean (true/false), overall result of method</li>
     * <li>list:      list of response objects (with fields reason, details, field, value), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElementCommands list of structure elements
     * @return if list of structures is valid to create (propose)
     */
    @Hidden
    @Operation(
            summary     = "Return if structures are valid to create (propose) by list of structure elements",
            description = """
                          Return if structures are valid to create (propose) by list of structure elements.
                          If structures are valid to create, successful create of structures can be expected.

                          Required attributes:
                          - type
                          - description

                          Optional attributes:
                          - parent
                          - mnemonic
                          - ordering

                          Note
                          - Uuid is created by Naming
                          - Parent is required for System, Subsystem, DeviceGroup, DeviceType
                          - Mnemonic is required for System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all structure elements validated ok, false otherwise. Responses contain array with result for each structure element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateCreate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresCreate(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandCreate.class,
                                            requiredProperties = {"type","description"}))),
                    required = true)
            @RequestBody List<StructureElementCommandCreate> structureElementCommands);

    /**
     * Return if structures are valid to update (propose) by list of structure elements.
     * If structures are valid to update, successful update of structures can be expected.
     *
     * <p>
     * Returned object has five fields (message, details, field, value, list).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>field:     field, if method fails</li>
     * <li>value:     boolean (true/false), overall result of method</li>
     * <li>list:      list of response objects (with fields reason, details, field, value), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElementCommands list of structure elements
     * @return if list of structures is valid to update (propose)
     */
    @Hidden
    @Operation(
            summary     = "Return if structures are valid to update (propose) by list of structure elements",
            description = """
                          Return if structures are valid to update (propose) by list of structure elements.
                          If structures are valid to update, successful update of structures can be expected.

                          Required attributes:
                          - uuid
                          - type
                          - description

                          Optional attributes:
                          - parent
                          - mnemonic
                          - ordering

                          Note
                          - Parent is required for System, Subsystem, DeviceGroup, DeviceType
                          - Mnemonic is required for System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all structure elements validated ok, false otherwise. Responses contain array with result for each structure element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateUpdate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresUpdate(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandUpdate.class,
                                            requiredProperties = {"uuid","type","description"}))),
                    required = true)
            @RequestBody List<StructureElementCommandUpdate> structureElementCommands);

    /**
     * Return if structures are valid to delete (propose) by list of structure elements.
     * If structures are valid to delete, successful delete of structures can be expected.
     *
     * <p>
     * Returned object has five fields (message, details, field, value, list).
     * <ul>
     * <li>message:   reason, if method fails</li>
     * <li>details:   details, if method fails</li>
     * <li>field:     field, if method fails</li>
     * <li>value:     boolean (true/false), overall result of method</li>
     * <li>list:      list of response objects (with fields reason, details, field, value), one for each input element</li>
     * </ul>
     * </p>
     *
     * @param structureElementCommands list of structure elements
     * @return if list of structures is valid to delete (propose)
     */
    @Hidden
    @Operation(
            summary     = "Return if structures are valid to delete (propose) by list of structure elements",
            description = """
                          Return if structures are valid to delete (propose) by list of structure elements.
                          If structures are valid to delete, successful delete of structures can be expected.

                          Required attributes:
                          - uuid
                          - type
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all structure elements validated ok, false otherwise. Responses contain array with result for each structure element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateDelete",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresDelete(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandConfirm.class,
                                            requiredProperties = {"uuid","type"}))),
                    required = true)
            @RequestBody List<StructureElementCommandConfirm> structureElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Update (propose) structures by list of structure elements.
     * Return list of structure elements for updated structures (proposals).
     *
     * @param structureElementCommands list of structure elements
     * @return list of structure elements for updated structures (proposals)
     */
    @Operation(
            summary     = "Update (propose) structures by array of structure elements",
            description = """
                          Update (propose) structures by array of structure elements.
                          Return array of structure elements for updated structures (proposals).

                          Required attributes:
                          - uuid
                          - type
                          - description

                          Optional attributes:
                          - parent
                          - mnemonic
                          - ordering

                          Note
                          - Parent is required for System, Subsystem, DeviceGroup, DeviceType
                          - Mnemonic is required for System, Subsystem, Discipline, DeviceType, may be set for SystemGroup, not allowed for DeviceGroup
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of structure elements for updated structures (proposals).",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = StructureElement.class)))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "409",
                    description  = "Conflict. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PutMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR)
    public List<StructureElement> updateStructures(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandUpdate.class,
                                            requiredProperties = {"uuid","type","description"}))),
                    required = true)
            @RequestBody List<StructureElementCommandUpdate> structureElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Delete (propose) structures by list of structure elements.
     * Return response code.
     *
     * @param structureElementCommands list of structure elements
     * @return response code
     */
    @Operation(
            summary     = "Delete (propose) structures by array of structure elements",
            description = """
                          Delete (propose) structures by array of structure elements.

                          Required attributes:
                          - uuid
                          - type
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description  = "No content. Structures were deleted (proposals)."),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @DeleteMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR)
    public ResponseEntity<Response> deleteStructures(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of structure elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = StructureElementCommandConfirm.class,
                                            requiredProperties = {"uuid","type"}))),
                    required = true)
            @RequestBody List<StructureElementCommandConfirm> structureElementCommands);

}

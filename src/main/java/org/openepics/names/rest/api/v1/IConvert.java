/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import java.util.List;

import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseString;
import org.openepics.names.util.ExcelUtil;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides conversion of names and structures data from one format to another, e.g. Excel to Json, for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "4. Convert",
     description = "handle conversion of names and structures data from one format to another for Naming application")
@RequestMapping("/api/v1/convert")
public interface IConvert {

    /*
       NameElementCommand - encompasses
           NameElementCommandCreate
           NameElementCommandUpdate
           NameElementCommandConfirm
       ----------------------------------------------------------------------------------------------------
       StructureElementCommand - encompasses
           StructureElementCommandCreate
           StructureElementCommandUpdate
           StructureElementCommandConfirm

       Methods
           read      POST   /excel2Json/names      - convertExcel2JsonNames
           read      POST   /excel2Json/structures - convertExcel2JsonStructures
           ----------------------------------------------------------------------------------------------------
           read      POST   /json2Excel/names      - convertJson2ExcelNames
           read      POST   /json2Excel/structures - convertJson2ExcelStructures
     */

    /**
     * Convert names data in Excel file to json.
     * Expected columns as in Excel template for names.
     * This endpoint is for conversion of content in Excel to be able to send it to Naming application as json.
     *
     * @param file Excel file according to template for names
     * @return json that corresponds to rows in Excel file
     */
    @Operation(
            summary     = "Convert names data in Excel file to json",
            description = """
                          Convert names data in Excel file to json.

                          Expected columns as in Excel template for names.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response contains json equivalent to content in Excel file.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseString.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            value = "/excel2Json/names",
            produces = {"application/json"},
            consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseString> convertExcel2JsonNames(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "Excel file according to template for names",
                    content = @Content(mediaType = ExcelUtil.MIME_TYPE_OPENXML_SPREADSHEET),
                    required = true)
            @RequestParam("file") MultipartFile file);

    /**
     * Convert structures data in Excel file to json.
     * Expected columns as in Excel template for structures.
     * This endpoint is for conversion of content in Excel to be able to send it to Naming application as json.
     *
     * @param file Excel file according to template for structures
     * @return json that corresponds to rows in Excel file
     */
    @Operation(
            summary     = "Convert structures data in Excel file to json",
            description = """
                          Convert structures data in Excel file to json.

                          Expected columns as in Excel template for structures.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response contains json equivalent to content in Excel file.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseString.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            value = "/excel2Json/structures",
            produces = {"application/json"},
            consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseString> convertExcel2JsonStructures(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "Excel file according to template for structures",
                    content = @Content(mediaType = ExcelUtil.MIME_TYPE_OPENXML_SPREADSHEET),
                    required = true)
            @RequestParam("file") MultipartFile file);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Convert names data in json to Excel file.
     * Expected content for json like result for names query.
     *
     * @param json json like result for names query - array of name elements
     * @return Excel file
     */
    @Operation(
            summary     = "Convert names data in json to Excel file",
            description = """
                          Convert names data in json to Excel file.

                          Expected content for json like result for names query.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response contains Excel file with content equivalent to json.",
                    content = @Content(mediaType = ExcelUtil.MIME_TYPE_EXCEL)),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            value = "/json2Excel/names",
            produces = {"application/vnd.ms-excel"},
            consumes = {"application/json"})
    public ResponseEntity<Resource> convertJson2ExcelNames(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "test json",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = NameElement.class))))
            @RequestBody List<NameElement> nameElements);

    /**
     * Convert structures data in json to Excel file.
     * Expected content for json like result for structures query.
     *
     * @param json json like result for names query - array of structure elements
     * @return Excel file
     */
    @Operation(
            summary     = "Convert structures data in json to Excel file",
            description = """
                          Convert structures data in json to Excel file.

                          Expected content for json like result for structures query.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response contains Excel file with content equivalent to json.",
                    content = @Content(mediaType = ExcelUtil.MIME_TYPE_EXCEL)),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            value = "/json2Excel/structures",
            produces = {"application/vnd.ms-excel"},
            consumes = {"application/json"})
    public ResponseEntity<Resource> convertJson2ExcelStructures(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "test json",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = StructureElement.class))))
            @RequestBody List<StructureElement> structureElements);
}

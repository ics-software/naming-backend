/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides validation of process variable names data for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "3. Process variable names",
     description = "handle validation of process variable names data for Naming application")
@RequestMapping("/api/v1/pvNames")
public interface IPvNames {

    /**
    *
    * @param pvname
    * @return
    */
   @Operation(
           summary     = "Return if process variable name is valid",
           description = """
                         Return if process variable name is valid.

                         Expected valid if convention name is registered in Naming and property has valid format.
                         """)
   @ApiResponses(value = {
           @ApiResponse(
                   responseCode = "200",
                   description  = "OK. Response is true if process variable name is valid, false otherwise.",
                   content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
           @ApiResponse(
                   responseCode = "500",
                   description  = "Internal server error. Reason and information such as message, details, field are available.",
                   content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
   })
   @GetMapping(
           value = "/{pvName}/validity",
           produces = {"application/json"})
   public ResponseEntity<ResponseBoolean> isValidProcessVariableName(
           @Parameter(
                   in = ParameterIn.DEFAULT,
                   description = "process variable name",
                   content = @Content(mediaType = "text/plain"))
           @PathVariable("pvName") String pvName);

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.api.v1;

import java.util.List;

import org.openepics.names.configuration.SecurityConfiguration;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.service.security.dto.UserDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides names data for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "1. Names",
     description = "handle names data for Naming application")
@RequestMapping("/api/v1/names")
public interface INames {

    /*
       NameElementCommandCreate  -       parentSystemStructure, parentDeviceStructure, index, description
       NameElementCommandUpdate  - uuid, parentSystemStructure, parentDeviceStructure, index, description
       NameElementCommandConfirm - uuid
       ----------------------------------------------------------------------------------------------------
       converted into NameElementCommand
       ----------------------------------------------------------------------------------------------------
       NameElementCommand        - uuid, parentSystemStructure, parentDeviceStructure, index, description
       NameElement
                     uuid                     (UUID)
                     parentSystemStructure    (UUID)
                     parentDeviceStructure    (UUID)
                     index                    (String)
                     description              (String)
                     ----------------------------------
                     systemStructure          (String)
                     deviceStructure          (String)
                     name                     (String)
                     status                   (Status)
                     deleted                  (Boolean)
                     when                     (Date)
                     who                      (String)
                     comment                  (String)

       Methods
           create    POST   /names                                - createNames
           ----------------------------------------------------------------------------------------------------
           read      GET    /names                                - readNames
           read      GET    /names/{uuid}                         - readName
           read      GET    /names/structure/{uuid}               - readNamesStructure
           read      GET    /names/history/{uuid}                 - readNamesHistory
           ----------------------------------------------------------------------------------------------------
           read      GET    /names/exists/{name}                  - existsName                   hidden from Swagger UI
           read      GET    /names/isValidToCreate/{name}         - isValidToCreateName          hidden from Swagger UI
           ----------------------------------------------------------------------------------------------------
           read      GET    /names/validateCreate                 - validateNamesCreate          hidden from Swagger UI
           read      GET    /names/validateUpdate                 - validateNamesUpdate          hidden from Swagger UI
           read      GET    /names/validateDelete                 - validateNamesDelete          hidden from Swagger UI
           ----------------------------------------------------------------------------------------------------
           update    PUT    /names                                - updateNames
           ----------------------------------------------------------------------------------------------------
           delete    DELETE /names                                - deleteNames

       Note
           read      GET    /names/{name} - both name and uuid (name - exact and search, uuid exact)
           authentication/authorization
               3 levels - no, user, administrator
                   no            - read
                   user          - create, update, delete
                 ( administrator )
           default for read is to have lifecycle attributes unless query for specific resource or specific use case
               status, deleted
     */

    public static final String DEFAULT_PAGE            = "0";
    public static final String DEFAULT_PAGE_SIZE       = "100";
    public static final String DEFAULT_SORT_FIELD_WHEN = "WHEN";
    public static final String DEFAULT_SORT_ORDER_ASC  = "true";

    /**
     * Create names by list of name elements.
     * Return list of name elements for created names.
     *
     * @param nameElementCommands list of name elements
     * @return list of name elements for created names
     */
    @Operation(
            summary     = "Create names by array of name elements",
            description = """
                          Create names by array of name elements.
                          Return array of name elements for created names.

                          Required attributes:
                          - parentSystemStructure
                          - description

                          Optional attributes:
                          - parentDeviceStructure
                          - index

                          Note
                          - Uuid is created by Naming
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description  = "OK. Return array of name elements for created names.",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "409",
                    description  = "Conflict. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PostMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR_OR_USER)
    public ResponseEntity<List<NameElement>> createNames(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandCreate.class,
                                            requiredProperties = {"parentSystemStructure","description"}))),
                    required = true)
            @RequestBody List<NameElementCommandCreate> nameElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Find valid names (search).
     * Return list of name elements.
     * Use deleted (false) to query for active names.
     *
     * @param deleted if deleted names are to be selected or not, omitted for both deleted and non-deleted names
     * (true for deleted names, false for non-deleted names, omitted for both deleted and non-deleted names)
     * @param name name
     * @param systemStructure system structure mnemonic path
     * @param deviceStructure device structure mnemonic path
     * @param index index
     * @param description description
     * @param who who
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param page page starting from 0, offset
     * @param pageSize page size, limit
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names (search)",
            description = """
                          Find valid names (search).
                          Return array of name elements.

                          Use deleted (false) to query for active names.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of name elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageNameElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            produces = {"application/json"})
    public ResponsePageNameElements readNames(
            @Parameter(in = ParameterIn.QUERY, description = "if deleted names are to be selected or not, omitted for both deleted and non-deleted names") @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "search by name") @RequestParam(required = false) String name,
            @Parameter(in = ParameterIn.QUERY, description = "search by system structure mnemonic path") @RequestParam(required = false) String systemStructure,
            @Parameter(in = ParameterIn.QUERY, description = "search by device structure mnemonic path") @RequestParam(required = false) String deviceStructure,
            @Parameter(in = ParameterIn.QUERY, description = "search by index") @RequestParam(required = false) String index,
            @Parameter(in = ParameterIn.QUERY, description = "search by description") @RequestParam(required = false) String description,
            @Parameter(in = ParameterIn.QUERY, description = "search by who") @RequestParam(required = false) String who,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "page starting from 0, offset") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @Parameter(in = ParameterIn.QUERY, description = "page size, limit") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize);

    /**
     * Find valid name by uuid (exact match).
     * Return list of name elements.
     *
     * @param uuid uuid
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid name by uuid (exact match)",
            description = """
                          Find valid name by uuid (exact match).
                          Return array of name elements.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of name elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageNameElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageNameElements> readName(
            @Parameter(in = ParameterIn.PATH,  description = "find by uuid or name") @PathVariable("uuid") String uuid);

    /**
     * Find valid names by structure uuid that is referenced (exact match).
     * Return list of name elements.
     * Use deleted (false) to query for active names.
     *
     * @param uuid uuid
     * @param deleted if deleted names are to be selected or not, omitted for both deleted and non-deleted names
     * (true for deleted names, false for non-deleted names, omitted for both deleted and non-deleted names)
     * @param orderBy order by field
     * @param isAsc sort order, ascending or descending
     * @param page page starting from 0, offset
     * @param pageSize page size, limit
     * @return list of name elements
     */
    @Operation(
            summary     = "Find valid names by structure uuid that is referenced (exact match)",
            description = """
                          Find valid names by structure uuid that is referenced (exact match).
                          Return list of name elements.

                          Endpoint applies for any type of structure.

                          Use deleted (false) to query for active names.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of name elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageNameElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/structure/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageNameElements> readNamesStructure(
            @Parameter(in = ParameterIn.PATH,  description = "find by structure uuid") @PathVariable("uuid") String uuid,
            @Parameter(in = ParameterIn.QUERY, description = "if deleted names are to be selected or not, omitted for both deleted and non-deleted names") @RequestParam(required = false) Boolean deleted,
            @Parameter(in = ParameterIn.QUERY, description = "order by field") @RequestParam(required = false) FieldName orderBy,
            @Parameter(in = ParameterIn.QUERY, description = "sort order, ascending or descending") @RequestParam(required = false) Boolean isAsc,
            @Parameter(in = ParameterIn.QUERY, description = "page starting from 0, offset") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) Integer page,
            @Parameter(in = ParameterIn.QUERY, description = "page size, limit") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) Integer pageSize);

    /**
     * Find history for name by uuid (exact match).
     * Return list of name elements.
     * History consists of line of uuid.
     * The line of uuid is not broken in retrieving history.
     * If the uuid is found in a name entry, the entire line of uuid is returned.
     *
     * @param uuid uuid
     * @return list of name elements
     */
    @Operation(
            summary     = "Find history for name by uuid (exact match)",
            description = """
                          Find history for name by uuid (exact match).
                          Return array of name elements.

                          History consists of line of uuid.
                          The line of uuid is not broken in retrieving history.
                          If the uuid is found in a name entry, the entire line of uuid is returned.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of name elements.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponsePageNameElements.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/history/{uuid}",
            produces = {"application/json"})
    public ResponseEntity<ResponsePageNameElements> readNamesHistory(
            @Parameter(in = ParameterIn.PATH,  description = "find by uuid") @PathVariable("uuid") String uuid);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if name exists (exact match).
     *
     * <p>
     * Response is true if name exists, false otherwise.
     * Message and details are available if no response is available.
     * </p>
     *
     * @param name name to check if it exists
     * @return if name exists
     */
    @Hidden
    @Operation(
            summary     = "Return if name exists (exact match)",
            description = """
                          Return if name exists (exact match).
                          Response is true if name exists, false otherwise.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if name exists, false otherwise.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/exists/{name}",
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> existsName(
             @Parameter(in = ParameterIn.PATH, description = "name to check if it exists") @PathVariable("name") String name);

    /**
     * Return if name is valid to create (exact match).
     * Method answers question 'would it be ok to create given name?'.
     *
     * <p>
     * Response is true if name is valid to create, false otherwise.
     * Message and details are available if no response is available.
     * </p>
     *
     * @param name name to check if it is valid to create
     * @return if name is valid to create
     */
    @Hidden
    @Operation(
            summary     = "Return if name is valid to create (exact match)",
            description = """
                          Return if name is valid to create (exact match).
                          Method answers question 'would it be ok to create given name?'.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if name is valid to create, false otherwise.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBoolean.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/isValidToCreate/{name}",
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isValidToCreateName(
            @Parameter(in = ParameterIn.PATH, description = "name to check if it is valid to create") @PathVariable("name") String name);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return if names are valid to create by list of name elements.
     * If names are valid to create, successful create of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array
     * with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElementCommands list of name elements
     * @return if list of names is valid to create
     */
    @Hidden
    @Operation(
            summary     = "Return if names are valid to create by list of name elements",
            description = """
                          Return if names are valid to create by list of name elements.
                          If names are valid to create, successful create of names can be expected.

                          Required attributes:
                          - parentSystemStructure
                          - description

                          Optional attributes:
                          - parentDeviceStructure
                          - index

                          Note
                          - Uuid is created by Naming
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all name elements validated ok, false otherwise. Responses contain array with result for each name element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateCreate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesCreate(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandCreate.class,
                                            requiredProperties = {"parentSystemStructure","parentDeviceStructure","index","description"}))),
                    required = true)
            @RequestBody List<NameElementCommandCreate> nameElementCommands);

    /**
     * Return if names are valid to update by list of name elements.
     * If names are valid to update, successful update of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array
     * with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElementCommands list of name elements
     * @return if list of name elements is valid to update
     */
    @Hidden
    @Operation(
            summary     = "Return if names are valid to update by list of name elements",
            description = """
                          Return if names are valid to update by list of name elements.
                          If names are valid to update, successful update of names can be expected.

                          Required attributes:
                          - uuid
                          - parentSystemStructure
                          - description

                          Optional attributes:
                          - parentDeviceStructure
                          - index
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all name elements validated ok, false otherwise. Responses contain array with result for each name element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateUpdate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesUpdate(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandUpdate.class,
                                            requiredProperties = {"uuid","parentSystemStructure","description"}))),
                    required = true)
            @RequestBody List<NameElementCommandUpdate> nameElementCommands);

    /**
     * Return if names are valid to delete by list of name elements.
     * If names are valid to delete, successful delete of names can be expected.
     *
     * <p>
     * Response is true if all name elements validated ok, false otherwise, responses contain array
     * with result for each name element. Message and details are available if no response is available.
     * </p>
     *
     * @param nameElementCommands list of name elements
     * @return if list of name elements is valid to delete
     */
    @Hidden
    @Operation(
            summary     = "Return if names are valid to delete by list of name elements for confirm",
            description = """
                          Return if names are valid to delete by list of name elements.
                          If names are valid to delete, successful delete of names can be expected.

                          Required attributes:
                          - uuid
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Response is true if all name elements validated ok, false otherwise. Responses contain array with result for each name element.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResponseBooleanList.class))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @GetMapping(
            value = "/validateDelete",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateNamesDelete(
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandConfirm.class,
                                            requiredProperties = {"uuid"}))),
                    required = true)
            @RequestBody List<NameElementCommandConfirm> nameElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Update names by list of name elements.
     * Return list of name elements for updated names.
     *
     * @param nameElementCommands list of name elements
     * @return list of name elements for updated names
     */
    @Operation(
            summary     = "Update names by array of name elements",
            description = """
                          Update names by array of name elements.
                          Return array of name elements for updated names.

                          Required attributes:
                          - uuid
                          - parentSystemStructure
                          - description

                          Optional attributes:
                          - parentDeviceStructure
                          - index
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "OK. Return array of name elements for updated names.",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = NameElement.class)))),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "409",
                    description  = "Conflict. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @PutMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR_OR_USER)
    public List<NameElement> updateNames(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandUpdate.class,
                                            requiredProperties = {"uuid","parentSystemStructure","description"}))),
                    required = true)
            @RequestBody List<NameElementCommandUpdate> nameElementCommands);

    // ----------------------------------------------------------------------------------------------------

    /**
     * Delete names by list of name elements.
     * Return response code.
     *
     * @param nameElementCommands list of name elements
     * @return response code
     */
    @Operation(
            summary     = "Delete names by array of name elements",
            description = """
                          Delete names by array of name elements.

                          Required attributes:
                          - uuid
                          """,
            security = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description  = "No content. Names were deleted."),
            @ApiResponse(
                    responseCode = "400",
                    description  = "Bad request. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Forbidden. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "404",
                    description  = "Not Found. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "422",
                    description  = "Unprocessable entity. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Internal server error. Reason and information such as message, details, field are available.",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)))
    })
    @DeleteMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    @PreAuthorize(SecurityConfiguration.IS_ADMINISTRATOR_OR_USER)
    public ResponseEntity<Response> deleteNames(
            @AuthenticationPrincipal UserDetails user,
            @Parameter(
                    in = ParameterIn.DEFAULT,
                    description = "array of name elements",
                    content = @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(
                                    schema = @Schema(
                                            implementation = NameElementCommandConfirm.class,
                                            requiredProperties = {"uuid"}))),
                    required = true)
            @RequestBody List<NameElementCommandConfirm> nameElementCommands);

}

package org.openepics.names.rest.api.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.openepics.names.exception.ServiceException;
import org.openepics.names.rest.beans.security.Login;
import org.openepics.names.rest.beans.security.LoginResponse;
import org.openepics.names.service.security.dto.UserDetails;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RequestMapping("/api/v1/authentication")
@Tag(name = "5. Authentication")
public interface IAuthentication {

    @Operation(
            summary     = "Login user and acquire JWT token",
            description = "Login user with username, and password in order to acquire JWT token")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "Ok",
                    content = @Content(schema = @Schema(implementation = LoginResponse.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Bad username, and/or password",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "User doesn't have permission to log in",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "503",
                    description  = "Login server unavailable",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Service exception",
                    content = @Content(schema = @Schema(implementation = ServiceException.class)))
    })
    @PostMapping(
            value = "/login",
            produces = {"application/json"},
            consumes = {"application/json"})
    ResponseEntity<LoginResponse> login(@RequestBody Login loginInfo);

    @Operation(
            summary     = "Renewing JWT token",
            description = "Renewing valid, non-expired JWT token",
            security    = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "Ok",
                    content = @Content(schema = @Schema(implementation = LoginResponse.class))),
            @ApiResponse(
                    responseCode = "401",
                    description  = "Unauthorized",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "403",
                    description  = "Permission denied",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "503",
                    description  = "Login server unavailable",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Service exception",
                    content = @Content(schema = @Schema(implementation = ServiceException.class)))
    })
    @PostMapping(
            value = "/renew",
            produces = {"application/json"})
    ResponseEntity<LoginResponse> tokenRenew(@AuthenticationPrincipal UserDetails user);

    @Operation(
            summary     = "Logout",
            description = "Logging out the user",
            security    = {@SecurityRequirement(name = "bearerAuth")})
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description  = "Ok"),
            @ApiResponse(
                    responseCode = "500",
                    description  = "Service exception",
                    content = @Content(schema = @Schema(implementation = ServiceException.class))),
            @ApiResponse(
                    responseCode = "503",
                    description  = "Login server unavailable or remote error",
                    content = @Content(schema = @Schema(implementation = ServiceException.class)))
    })
    @DeleteMapping(
            value = "/logout",
            produces = {"application/json"})
    ResponseEntity<Object> logout(@AuthenticationPrincipal UserDetails user);

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.ServiceException;
import org.openepics.names.rest.api.v1.INames;
import org.openepics.names.rest.beans.FieldName;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.service.LogService;
import org.openepics.names.service.NamesService;
import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.util.SecurityUtil;
import org.openepics.names.util.NameElementUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.ValidateNameElementUtil;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides name data for Naming application.
 *
 * @author Lars Johansson
 */
@RestController
@EnableAutoConfiguration
public class NamesController implements INames {

    // note
    //     controller layer to call validation prior to invoking service layer
    //         business logic may be validation
    //         different kinds of validation

    private static final Logger LOGGER = Logger.getLogger(NamesController.class.getName());

    private final LogService logService;
    private final NamesService namesService;

    @Autowired
    public NamesController(
            LogService logService,
            NamesService namesService) {
        this.logService = logService;
        this.namesService = namesService;
    }

    @Override
    public ResponseEntity<List<NameElement>> createNames(UserDetails user,
            List<NameElementCommandCreate> nameElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // create names

        try {
            List<NameElementCommand> commands = NameElementUtil.convertCommandCreate2Command(nameElementCommands);
            namesService.validateNamesCreate(commands);
            return new ResponseEntity<>(namesService.createNames(commands, SecurityUtil.getUsername(user)), Response.getHeaderJson(), HttpStatus.CREATED);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponsePageNameElements readNames(Boolean deleted,
            String name, String systemStructure, String deviceStructure, String index, String description, String who,
            FieldName orderBy, Boolean isAsc, Integer page, Integer pageSize) {
        // validate
        // read names

        try {
            ValidateNameElementUtil.validateNamesInputRead(
                    deleted,
                    null, name, systemStructure, deviceStructure, index, description, who,
                    null,
                    orderBy, isAsc,
                    page, pageSize);
            return namesService.readNames(deleted,
                    null, name, systemStructure, deviceStructure, index, description, who,
                    orderBy, isAsc, page, pageSize);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageNameElements> readName(String uuid) {
        // validate
        // read name

        try {
            // use offset, limit
            //     have limit for size of search result

            ValidateUtil.validateInputUuid(uuid);
            ResponsePageNameElements nameElements = namesService.readNames(null,
                    uuid, null, null, null, null, null, null,
                    null, null, Integer.valueOf(INames.DEFAULT_PAGE), Integer.valueOf(INames.DEFAULT_PAGE_SIZE));

            HttpStatus status = null;
            if (nameElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(nameElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageNameElements> readNamesStructure(String uuid, Boolean deleted,
            FieldName orderBy, Boolean isAsc, Integer page, Integer pageSize) {
        // validate
        // read names

        try {
            ValidateUtil.validateInputUuid(uuid);
            ResponsePageNameElements nameElements = namesService.readNamesStructure(uuid, deleted,
                    orderBy, isAsc, page, pageSize);

            HttpStatus status = null;
            if (nameElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(nameElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageNameElements> readNamesHistory(String uuid) {
        // validate
        // read names

        try {
            ValidateUtil.validateInputUuid(uuid);
            ResponsePageNameElements nameElements = namesService.readNamesHistory(uuid,
                    Integer.valueOf(INames.DEFAULT_PAGE), Integer.valueOf(INames.DEFAULT_PAGE_SIZE));

            HttpStatus status = null;
            if (nameElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(nameElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBoolean> existsName(String name) {
        // validate
        // read names

        try {
            ValidateUtil.validateInputName(name);
            return new ResponseEntity<>(new ResponseBoolean(namesService.existsName(name)), Response.getHeaderJson(), HttpStatus.OK);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()), Response.getHeaderJson(), HttpStatus.OK);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.getHeaderJson(), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> isValidToCreateName(String name) {
        // validate
        // read names

        try {
            ValidateUtil.validateInputName(name);
            return new ResponseEntity<>(new ResponseBoolean(namesService.isValidToCreateName(name)), Response.getHeaderJson(), HttpStatus.OK);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()), Response.getHeaderJson(), HttpStatus.OK);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.getHeaderJson(), HttpStatus.OK);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesCreate(List<NameElementCommandCreate> nameElementCommands) {
        // convert
        // validate

        List<NameElementCommand> commands = NameElementUtil.convertCommandCreate2Command(nameElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElementCommand command : commands) {
            try {
                namesService.validateNamesCreate(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesUpdate(List<NameElementCommandUpdate> nameElementCommands) {
        // convert
        // validate

        List<NameElementCommand> commands = NameElementUtil.convertCommandUpdate2Command(nameElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElementCommand command : commands) {
            try {
                namesService.validateNamesUpdate(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateNamesDelete(List<NameElementCommandConfirm> nameElementCommands) {
        // convert
        // validate

        List<NameElementCommand> commands = NameElementUtil.convertCommandConfirm2Command(nameElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (NameElementCommand command : commands) {
            try {
                namesService.validateNamesDelete(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<NameElement> updateNames(UserDetails user,
            List<NameElementCommandUpdate> nameElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // update names

        try {
            List<NameElementCommand> commands = NameElementUtil.convertCommandUpdate2Command(nameElementCommands);
            namesService.validateNamesUpdate(commands);
            return namesService.updateNames(commands, SecurityUtil.getUsername(user));
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<Response> deleteNames(UserDetails user,
            List<NameElementCommandConfirm> nameElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // delete names

        try {
            List<NameElementCommand> commands = NameElementUtil.convertCommandConfirm2Command(nameElementCommands);
            namesService.validateNamesDelete(commands);
            namesService.deleteNames(commands, SecurityUtil.getUsername(user));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

}

/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.openepics.names.repository.model.wip.WipDeviceType;
import org.openepics.names.repository.model.wip.WipDiscipline;
import org.openepics.names.repository.model.wip.WipSubsystem;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.openepics.names.repository.wip.IWipDeviceGroupRepository;
import org.openepics.names.repository.wip.IWipDeviceTypeRepository;
import org.openepics.names.repository.wip.IWipDisciplineRepository;
import org.openepics.names.repository.wip.IWipNameRepository;
import org.openepics.names.repository.wip.IWipSubsystemRepository;
import org.openepics.names.repository.wip.IWipSystemGroupRepository;
import org.openepics.names.repository.wip.IWipSystemRepository;
import org.openepics.names.rest.beans.old.PartElement;
import org.openepics.names.service.LogService;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.old.PartElementUtil;
import org.openepics.names.util.wip.HolderIWipRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides name part data for Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/parts")
@EnableAutoConfiguration
public class PartControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /parts/mnemonic/{mnemonic}                - findPartsByMnemonic(String)
           read      GET /parts/mnemonic/search/{mnemonic}         - findPartsByMnemonicSearch(String)
           read      GET /parts/mnemonicPath/search/{mnemonicPath} - findPartsByMnemonicPathSearch(String)
     */

    private static final Logger LOGGER = Logger.getLogger(PartControllerV0.class.getName());

    private final LogService logService;
    private final HolderIWipRepositories holderIWipRepositories;

    @Autowired
    public PartControllerV0(
            LogService logService,
            IWipNameRepository iWipNameRepository,
            IWipSystemGroupRepository iWipSystemGroupRepository,
            IWipSystemRepository iWipSystemRepository,
            IWipSubsystemRepository iWipSubsystemRepository,
            IWipDisciplineRepository iWipDisciplineRepository,
            IWipDeviceGroupRepository iWipDeviceGroupRepository,
            IWipDeviceTypeRepository iWipDeviceTypeRepository) {

        this.logService = logService;
        this.holderIWipRepositories = new HolderIWipRepositories(
                iWipNameRepository,
                iWipSystemGroupRepository,
                iWipSystemRepository,
                iWipSubsystemRepository,
                iWipDisciplineRepository,
                iWipDeviceGroupRepository,
                iWipDeviceTypeRepository);
    }

    /**
     * Find name parts by mnemonic.
     * Note mnemonic (exact match, case sensitive).
     *
     * @param mnemonic mnemonic to look for
     * @return a list of name parts
     */
    @GetMapping("/mnemonic/{mnemonic}")
    public List<PartElement> findPartsByMnemonic(@PathVariable("mnemonic") String mnemonic) {
        // note
        //     exact match
        //     case sensitive

        final List<PartElement> partElements = Lists.newArrayList();

        // find in system structure

        List<WipSystemGroup> systemGroups = holderIWipRepositories.systemGroupRepository().findLatestByMnemonic(mnemonic);
        List<WipSystem>      systems      = holderIWipRepositories.systemRepository().findLatestByMnemonic(mnemonic);
        List<WipSubsystem>   subsystems   = holderIWipRepositories.subsystemRepository().findLatestByMnemonic(mnemonic);

        for (WipSystemGroup systemGroup : systemGroups) {
            partElements.add(PartElementUtil.getPartElement(systemGroup));
        }
        for (WipSystem system : systems) {
            partElements.add(PartElementUtil.getPartElement(system, holderIWipRepositories));
        }
        for (WipSubsystem subsystem : subsystems) {
            partElements.add(PartElementUtil.getPartElement(subsystem, holderIWipRepositories));
        }

        // find in system structure

        List<WipDiscipline> disciplines = holderIWipRepositories.disciplineRepository().findLatestByMnemonic(mnemonic);
        List<WipDeviceType> deviceTypes = holderIWipRepositories.deviceTypeRepository().findLatestByMnemonic(mnemonic);

        for (WipDiscipline discipline : disciplines) {
            partElements.add(PartElementUtil.getPartElement(discipline));
        }
        for (WipDeviceType deviceType : deviceTypes) {
            partElements.add(PartElementUtil.getPartElement(deviceType, holderIWipRepositories));
        }

        LOGGER.log(Level.FINE, "findPartsByMnemonic, mnemonic:          {0}", mnemonic);
        LOGGER.log(Level.FINE, "findPartsByMnemonic, partElements.size: {0}", partElements.size());

        return partElements;
    }

    /**
     * Find name parts by mnemonic search.
     * Note mnemonic (search, case sensitive, regex).
     *
     * @param mnemonic mnemonic to search for
     * @return a list of name parts
     */
    @GetMapping("/mnemonic/search/{mnemonic}")
    public List<PartElement> findPartsByMnemonicSearch(@PathVariable("mnemonic") String mnemonic) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<PartElement> partElements = Lists.newArrayList();

        Pattern pattern = null;
        try {
            pattern = Pattern.compile(mnemonic);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
        }

        // find in system structure

        List<WipSystemGroup> systemGroups = holderIWipRepositories.systemGroupRepository().findLatest();
        List<WipSystem>      systems      = holderIWipRepositories.systemRepository().findLatest();
        List<WipSubsystem>   subsystems   = holderIWipRepositories.subsystemRepository().findLatest();

        for (WipSystemGroup systemGroup : systemGroups) {
            String sub = systemGroup.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(systemGroup));
            }
        }
        for (WipSystem system : systems) {
            String sub = system.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(system, holderIWipRepositories));
            }
        }
        for (WipSubsystem subsystem : subsystems) {
            String sub = subsystem.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(subsystem, holderIWipRepositories));
            }
        }

        // find in device structure

        List<WipDiscipline> disciplines = holderIWipRepositories.disciplineRepository().findLatest();
        List<WipDeviceType> deviceTypes = holderIWipRepositories.deviceTypeRepository().findLatest();

        for (WipDiscipline discipline : disciplines) {
            String sub = discipline.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(discipline));
            }
        }
        for (WipDeviceType deviceType : deviceTypes) {
            String sub = deviceType.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(deviceType, holderIWipRepositories));
            }
        }

        LOGGER.log(Level.FINE, "getAllPartsByMnemonicSearch, mnemonic:          {0}", mnemonic);
        LOGGER.log(Level.FINE, "getAllPartsByMnemonicSearch, partElements.size: {0}", partElements.size());

        return partElements;
    }

    /**
     * Find name parts by mnemonic path search.
     * Note mnemonic path (search, case sensitive, regex).
     *
     * @param mnemonicPath mnemonic path to search for
     * @return a list of name parts
     */
    @GetMapping("/mnemonicPath/search/{mnemonicPath}")
    public List<PartElement> findPartsByMnemonicPathSearch(@PathVariable("mnemonicPath") String mnemonicPath) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<PartElement> partElements = Lists.newArrayList();

        Pattern pattern = null;
        try {
            pattern = Pattern.compile(mnemonicPath);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
        }

        // find in system structure

        List<WipSystemGroup> systemGroups = holderIWipRepositories.systemGroupRepository().findLatest();
        List<WipSystem>      systems      = holderIWipRepositories.systemRepository().findLatest();
        List<WipSubsystem>   subsystems   = holderIWipRepositories.subsystemRepository().findLatest();

        for (WipSystemGroup systemGroup : systemGroups) {
            String sub = systemGroup.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(systemGroup));
            }
        }
        for (WipSystem system : systems) {
            WipSystemGroup systemGroup = holderIWipRepositories.systemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            String sub = system.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(system, holderIWipRepositories));
            }
        }
        for (WipSubsystem subsystem : subsystems) {
            WipSystem      system      = holderIWipRepositories.systemRepository().findLatestByUuid(subsystem.getParentUuid().toString());
            WipSystemGroup systemGroup = holderIWipRepositories.systemGroupRepository().findLatestByUuid(system.getParentUuid().toString());

            String sub = NamingConventionUtil.mnemonicPath2String(systemGroup.getMnemonic(), system.getMnemonic(), subsystem.getMnemonic());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(subsystem, holderIWipRepositories));
            }
        }

        // find in device structure

        List<WipDiscipline> disciplines = holderIWipRepositories.disciplineRepository().findLatest();
        List<WipDeviceType> deviceTypes = holderIWipRepositories.deviceTypeRepository().findLatest();

        for (WipDiscipline discipline : disciplines) {
            String sub = discipline.getMnemonic();
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(discipline));
            }
        }
        for (WipDeviceType deviceType : deviceTypes) {
            WipDeviceGroup deviceGroup = holderIWipRepositories.deviceGroupRepository().findLatestByUuid(deviceType.getParentUuid().toString());
            WipDiscipline  discipline  = holderIWipRepositories.disciplineRepository().findLatestByUuid(deviceGroup.getParentUuid().toString());

            String sub = NamingConventionUtil.mnemonicPath2String(discipline.getMnemonic(), deviceType.getMnemonic());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                partElements.add(PartElementUtil.getPartElement(deviceType, holderIWipRepositories));
            }
        }

        LOGGER.log(Level.FINE, "findPartsByMnemonicPathSearch, mnemonicPath:      {0}", mnemonicPath);
        LOGGER.log(Level.FINE, "findPartsByMnemonicPathSearch, partElements.size: {0}", partElements.size());

        return partElements;
    }

}

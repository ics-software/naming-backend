/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.ServiceException;
import org.openepics.names.rest.api.v1.IStructures;
import org.openepics.names.rest.beans.FieldStructure;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.service.LogService;
import org.openepics.names.service.StructuresService;
import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.util.SecurityUtil;
import org.openepics.names.util.StructureElementUtil;
import org.openepics.names.util.TextUtil;
import org.openepics.names.util.ValidateStructureElementUtil;
import org.openepics.names.util.ValidateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides structures data for Naming application.
 *
 * @author Lars Johansson
 */
@RestController
@EnableAutoConfiguration
public class StructuresController implements IStructures {

    // note
    //     controller layer to call validation prior to invoking service layer
    //         business logic may be validation
    //         different kinds of validation

    private static final Logger LOGGER = Logger.getLogger(StructuresController.class.getName());

    private final LogService logService;
    private final StructuresService structuresService;

    @Autowired
    public StructuresController(
            LogService logService,
            StructuresService structuresService) {
        this.logService = logService;
        this.structuresService = structuresService;
    }

    @Override
    public ResponseEntity<List<StructureElement>> createStructures(UserDetails user,
            List<StructureElementCommandCreate> structureElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // create structures

        try {
            List<StructureElementCommand> commands = StructureElementUtil.convertCommandCreate2Command(structureElementCommands);
            structuresService.validateStructuresCreate(commands);
            return new ResponseEntity<>(structuresService.createStructures(commands, SecurityUtil.getUsername(user)), Response.getHeaderJson(), HttpStatus.CREATED);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponsePageStructureElements readStructures(Type type, Boolean deleted,
            String parent, String mnemonic, String mnemonicPath, String description, String who,
            FieldStructure orderBy, Boolean isAsc, Integer page, Integer pageSize) {
        // validate
        // read structures

        try {
            ValidateStructureElementUtil.validateStructuresInputRead(type, deleted,
                    null, parent, mnemonic, mnemonicPath, description, who,
                    null, orderBy, isAsc, page, pageSize);
            return structuresService.readStructures(type, deleted,
                    null, parent, mnemonic, mnemonicPath, description, who,
                    orderBy, isAsc, page, pageSize);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageStructureElements> readStructure(String uuid) {
        // validate
        // read structure

        try {
            // use offset, limit
            //     have limit for size of search result

            ValidateUtil.validateInputUuid(uuid);
            ResponsePageStructureElements structureElements = structuresService.readStructures(null, null,
                    uuid, null, null, null, null, null,
                    null, null, Integer.valueOf(IStructures.DEFAULT_PAGE), Integer.valueOf(IStructures.DEFAULT_PAGE_SIZE));

            HttpStatus status = null;
            if (structureElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(structureElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageStructureElements> readStructuresChildren(String uuid, Boolean deleted,
            FieldStructure orderBy, Boolean isAsc, Integer page, Integer pageSize) {
        // validate
        // read structures

        try {
            ValidateUtil.validateInputUuid(uuid);
            ResponsePageStructureElements structureElements = structuresService.readStructuresChildren(uuid, deleted,
                    orderBy, isAsc, page, pageSize);

            HttpStatus status = null;
            if (structureElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(structureElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponsePageStructureElements> readStructuresHistory(String uuid) {
        // validate
        // read structures

        try {
            ValidateUtil.validateInputUuid(uuid);
            ResponsePageStructureElements structureElements = structuresService.readStructuresHistory(uuid,
                    Integer.valueOf(IStructures.DEFAULT_PAGE), Integer.valueOf(IStructures.DEFAULT_PAGE_SIZE));

            HttpStatus status = null;
            if (structureElements.getListSize() == 0) {
                status = HttpStatus.NOT_FOUND;
            } else {
                status = HttpStatus.OK;
            }

            return new ResponseEntity<>(structureElements, Response.getHeaderJson(), status);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBoolean> existsStructure(Type type, String mnemonicPath) {
        // validate
        // read structures

        try {
            ValidateUtil.validateInputType(type);
            ValidateUtil.validateInputMnemonicPath(mnemonicPath);
            return new ResponseEntity<>(new ResponseBoolean(structuresService.existsStructure(type, mnemonicPath)), Response.getHeaderJson(), HttpStatus.OK);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()), Response.getHeaderJson(), HttpStatus.OK);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.getHeaderJson(), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<ResponseBoolean> isValidToCreateStructure(Type type, String mnemonicPath) {
        // validate
        // read structures

        try {
            ValidateUtil.validateInputType(type);
            ValidateUtil.validateInputMnemonicPath(mnemonicPath);
            return new ResponseEntity<>(new ResponseBoolean(structuresService.isValidToCreateStructure(type, mnemonicPath)), Response.getHeaderJson(), HttpStatus.OK);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()), Response.getHeaderJson(), HttpStatus.OK);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return new ResponseEntity<>(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED), Response.getHeaderJson(), HttpStatus.OK);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresCreate(List<StructureElementCommandCreate> structureElementCommands) {
        // convert
        // validate

        List<StructureElementCommand> commands = StructureElementUtil.convertCommandCreate2Command(structureElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElementCommand command : commands) {
            try {
                structuresService.validateStructuresCreate(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresUpdate(List<StructureElementCommandUpdate> structureElementCommands) {
        // convert
        // validate

        List<StructureElementCommand> commands = StructureElementUtil.convertCommandUpdate2Command(structureElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElementCommand command : commands) {
            try {
                structuresService.validateStructuresUpdate(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ResponseBooleanList> validateStructuresDelete(List<StructureElementCommandConfirm> structureElementCommands) {
        // convert
        // validate

        List<StructureElementCommand> commands = StructureElementUtil.convertCommandConfirm2Command(structureElementCommands);
        boolean response = true;
        String reason = "";
        List<ResponseBoolean> responses = Lists.newArrayList();
        for (StructureElementCommand command : commands) {
            try {
                structuresService.validateStructuresDelete(command);
                responses.add(new ResponseBoolean(Boolean.TRUE));
            } catch (ServiceException e) {
                logService.logServiceException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, e.getMessage(), e.getDetails(), e.getField()));
            } catch (Exception e) {
                logService.logException(LOGGER, Level.WARNING, e);
                if (response) {
                    response = false;
                    reason = TextUtil.ONE_OR_MORE_ELEMENTS_ARE_NOT_CORRECT;
                }
                responses.add(new ResponseBoolean(Boolean.FALSE, TextUtil.OPERATION_COULD_NOT_BE_PERFORMED));
            }
        }
        return new ResponseEntity<>(new ResponseBooleanList(responses, Boolean.valueOf(response), reason), Response.getHeaderJson(), HttpStatus.OK);
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public List<StructureElement> updateStructures(UserDetails user,
            List<StructureElementCommandUpdate> structureElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // update structures

        try {
            List<StructureElementCommand> commands = StructureElementUtil.convertCommandUpdate2Command(structureElementCommands);
            structuresService.validateStructuresUpdate(commands);
            return structuresService.updateStructures(commands, SecurityUtil.getUsername(user));
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<Response> deleteStructures(UserDetails user,
            List<StructureElementCommandConfirm> structureElementCommands) {
        // validate authority - user & admin
        // convert
        // validate
        // delete structures

        try {
            List<StructureElementCommand> commands = StructureElementUtil.convertCommandConfirm2Command(structureElementCommands);
            structuresService.validateStructuresDelete(commands);
            structuresService.deleteStructures(commands, SecurityUtil.getUsername(user));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

}

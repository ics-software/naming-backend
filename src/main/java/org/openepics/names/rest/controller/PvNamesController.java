/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import org.openepics.names.rest.api.v1.IPvNames;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.service.NamesService;
import org.openepics.names.util.NameInformation;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.ProcessVariableNameInformation;
import org.openepics.names.util.TextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * This part of REST API provides validation of PV names data for Naming application.
 *
 * @author Lars Johansson
 * @see NamingConventionUtil
 */
@RestController
@EnableAutoConfiguration
public class PvNamesController implements IPvNames {

    private final NamesService namesService;

    @Autowired
    public PvNamesController(
            NamesService namesService) {
        this.namesService = namesService;
    }

    @Override
    public ResponseEntity<ResponseBoolean> isValidProcessVariableName(String pvName) {
        Boolean value = Boolean.TRUE;
        String message = null;
        String details = pvName;
        String field = "pvName";

        NameInformation nameInformation = NamingConventionUtil.extractNameInformation(pvName);
        ProcessVariableNameInformation pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(pvName);

        if (!Boolean.TRUE.equals(namesService.existsName(nameInformation.conventionName()))) {
            value = Boolean.FALSE;
            message = TextUtil.PV_NAME_VALIDATION_CONVENTION_NAME_IS_NOT_REGISTERED_IN_NAMING;
        } else if (!pvNameInformation.isValidFormat()) {
            value = Boolean.FALSE;
            message = pvNameInformation.message();
        }

        return new ResponseEntity<>(new ResponseBoolean(value, message, details, field), Response.getHeaderJson(), HttpStatus.OK);
    }

}

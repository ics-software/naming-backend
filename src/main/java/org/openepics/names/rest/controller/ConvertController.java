/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.ServiceException;
import org.openepics.names.rest.api.v1.IConvert;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.response.Response;
import org.openepics.names.rest.beans.response.ResponseString;
import org.openepics.names.service.LogService;
import org.openepics.names.util.ExcelUtil;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.TextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This part of REST API provides conversion of names and structures data from one format to another, e.g. Excel to Json, for Naming application.
 *
 * @author Lars Johansson
 */
@RestController
@EnableAutoConfiguration
public class ConvertController implements IConvert {

    private static final Logger LOGGER = Logger.getLogger(ConvertController.class.getName());

    private final LogService logService;
    private final ObjectMapper mapper;

    @Autowired
    public ConvertController(
            LogService logService) {
        this.logService = logService;
        this.mapper = new ObjectMapper();
    }

    @Override
    public ResponseEntity<ResponseString> convertExcel2JsonNames(MultipartFile file) {
        // convert from input
        // ( convert )
        // Excel according to template for names (NameElementCommand)
        // rows are converted to array of name element commands (json)

        try {
            if (ExcelUtil.hasExcelFormat(file)) {
                List<NameElementCommand> commands = ExcelUtil.excelToNameElementCommands(file.getInputStream(), NameCommand.UPDATE);

                return new ResponseEntity<>(new ResponseString(mapper.writeValueAsString(commands.toArray())), Response.getHeaderJson(), HttpStatus.OK);
            } else {
                throw new ServiceException(TextUtil.FILE_COULD_NOT_BE_PARSED, null, null, null);
            }
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (IOException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw new ServiceException(TextUtil.FILE_COULD_NOT_BE_PARSED, e.getMessage(), null, e);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<ResponseString> convertExcel2JsonStructures(MultipartFile file) {
        // convert from input
        // ( convert )
        // Excel according to template for structures (StructureElementCommand)
        // rows are converted to array of structure element commands (json)

        try {
            if (ExcelUtil.hasExcelFormat(file)) {
                List<StructureElementCommand> commands = ExcelUtil.excelToStructureElementCommands(file.getInputStream(), StructureCommand.UPDATE);

                return new ResponseEntity<>(new ResponseString(mapper.writeValueAsString(commands.toArray())), Response.getHeaderJson(), HttpStatus.OK);
            } else {
                throw new ServiceException(TextUtil.FILE_COULD_NOT_BE_PARSED, null, null, null);
            }
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (IOException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw new ServiceException(TextUtil.FILE_COULD_NOT_BE_PARSED, e.getMessage(), null, e);
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    // ----------------------------------------------------------------------------------------------------

    @Override
    public ResponseEntity<Resource> convertJson2ExcelNames(List<NameElement> nameElements) {
        // convert from input
        // ( convert )
        // json according to array of name elements (NameElement)

        try {
            InputStreamResource isr = new InputStreamResource(ExcelUtil.nameElementsToExcel(nameElements));
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, TextUtil.ATTACHMENT_FILENAME_NAME_ELEMENT_XLSX)
                    .contentType(MediaType.parseMediaType(ExcelUtil.MIME_TYPE_EXCEL))
                    .body(isr);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

    @Override
    public ResponseEntity<Resource> convertJson2ExcelStructures(List<StructureElement> structureElements) {
        // convert from input
        // ( convert )
        // json according to array of structure elements (StructureElement)

        try {
            InputStreamResource isr = new InputStreamResource(ExcelUtil.structureElementsToExcel(structureElements));
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, TextUtil.ATTACHMENT_FILENAME_STRUCTURE_ELEMENT_XLSX)
                    .contentType(MediaType.parseMediaType(ExcelUtil.MIME_TYPE_EXCEL))
                    .body(isr);
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e);
            throw e;
        }
    }

}

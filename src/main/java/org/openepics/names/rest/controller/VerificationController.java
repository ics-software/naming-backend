/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.old.model.DeviceRevision;
import org.openepics.names.old.model.NamePartRevision;
import org.openepics.names.old.model.NamePartRevisionStatus;
import org.openepics.names.old.nameviews.NameViewProvider;
import org.openepics.names.repository.IAuditNameRepository;
import org.openepics.names.repository.IAuditStructureRepository;
import org.openepics.names.repository.IDeviceGroupRepository;
import org.openepics.names.repository.IDeviceTypeRepository;
import org.openepics.names.repository.IDisciplineRepository;
import org.openepics.names.repository.INameRepository;
import org.openepics.names.repository.ISubsystemRepository;
import org.openepics.names.repository.ISystemGroupRepository;
import org.openepics.names.repository.ISystemRepository;
import org.openepics.names.repository.AuditNameRepository;
import org.openepics.names.repository.AuditStructureRepository;
import org.openepics.names.repository.DeviceGroupRepository;
import org.openepics.names.repository.DeviceTypeRepository;
import org.openepics.names.repository.DisciplineRepository;
import org.openepics.names.repository.NameRepository;
import org.openepics.names.repository.SubsystemRepository;
import org.openepics.names.repository.SystemGroupRepository;
import org.openepics.names.repository.SystemRepository;
import org.openepics.names.repository.model.wip.WipDeviceGroup;
import org.openepics.names.repository.model.wip.WipDeviceType;
import org.openepics.names.repository.model.wip.WipDiscipline;
import org.openepics.names.repository.model.wip.WipName;
import org.openepics.names.repository.model.wip.WipStructure;
import org.openepics.names.repository.model.wip.WipSubsystem;
import org.openepics.names.repository.model.wip.WipSystem;
import org.openepics.names.repository.model.wip.WipSystemGroup;
import org.openepics.names.repository.old.IDeviceRevisionRepository;
import org.openepics.names.repository.old.INamePartRevisionRepository;
import org.openepics.names.repository.wip.IWipDeviceGroupRepository;
import org.openepics.names.repository.wip.IWipDeviceTypeRepository;
import org.openepics.names.repository.wip.IWipDisciplineRepository;
import org.openepics.names.repository.wip.IWipNameRepository;
import org.openepics.names.repository.wip.IWipSubsystemRepository;
import org.openepics.names.repository.wip.IWipSystemGroupRepository;
import org.openepics.names.repository.wip.IWipSystemRepository;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.HolderIRepositories;
import org.openepics.names.util.HolderRepositories;
import org.openepics.names.util.ValidateUtil;
import org.openepics.names.util.wip.HolderIWipRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Hidden;

/**
 * This part of REST API provides verification of data migration for Naming application.
 *
 * <p>
 * Prerequisites.
 * These are required to ensure that comparisons of content in old and migrated tables
 * can be done and that result of comparisons are valid.
 * <ul>
 * <li>migration script run</li>
 * <li>tables corresponding to both old and migrated database available</li>
 * <li>must be done directly after migration has been done</li>
 * <li>no changes of any kind in any way for content, either names or structures, may have been introduced</li>
 * </ul>
 *
 * <p>
 * Verification is preferably done in following order
 * <ol>
 * <li>structures</li>
 * <li>names</li>
 * <li>data is reachable</li>
 * <li>comparison of old REST API vs. new REST API</li>
 * </ol>
 *
 * <p>
 * Structures.
 * <ul>
 * <li>Verification of structures (after) vs. name parts (before)</li>
 * <li>Check that each attribute as expected in each structure entry</li>
 * </ul>
 *
 * <p>
 * Names.
 * <ul>
 * <li>Verification of names (after) vs. devices (before)</li>
 * <li>Check that each attribute as expected in each name entry</li>
 * </ul>
 *
 * <p>
 * Data is reachable.
 * <ul>
 * <li>Verification of data in the sense that all data can be reached</li>
 * <li>Concerns itself with new database, not old database</li>
 * </ul>
 *
 * <p>
 * Comparison of old REST API vs. new REST API.
 * <ul>
 * <li>Verification of differences between old REST API and new REST API</li>
 * <li>Check that all content in new REST API is in old REST API and that difference is old/obsolete data</li>
 * </ul>
 *
 * <p>
 * Note
 * <ul>
 * <li>log statements and output are technical and implementation-like, and meant to give detailed information</li>
 * <li>after migration has been done and verified, this part of REST API no longer holds any function and may be removed
 * together with related code not used elsewhere. As a suggestion, this may done in the second release of the software.</li>
 * </ul>
 *
 * <p>
 * Ideally, some knowledge of database tables and object structures acquired to dig into this class.
 * It can be done in any case and there is documentation available.
 * It is recommended to have database tables available in database management tool for any further queries.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/verification")
@EnableAutoConfiguration
public class VerificationController {

    // methods
    //     read      GET /verification/migration_namepartrevision - readMigrationStructures
    //     read      GET /verification/migration_devicerevision   - readMigrationNames
    //     read      GET /verification/data_reachable             - readDataReachable
    //     read      GET /verification/restapi_oldvsnew           - readRestApiOldVsNew

    private static final Logger LOGGER = Logger.getLogger(VerificationController.class.getName());

    private static final String NEWLINE       = "\n";
    private static final String SPACE         = " ";

    private static final String DIVIDER_32    = "--------------------------------";
    private static final String DIVIDER_96    = DIVIDER_32 + DIVIDER_32 + DIVIDER_32;
    private static final String DIVIDER_128   = DIVIDER_32 + DIVIDER_32 + DIVIDER_32 + DIVIDER_32;

    // prepare text
    private static final int SPACE_UNTIL_SIZE_32 = 32;
    private static final int SPACE_UNTIL_SIZE_40 = 40;
    private static final int SPACE_UNTIL_SIZE_48 = 48;

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private final IDeviceRevisionRepository deviceRevisionRepository;
    private final INamePartRevisionRepository namePartRevisionRepository;

    private final HolderIWipRepositories holderIWipRepositories;
    private final HolderIRepositories holderIRepositories;
    private final HolderRepositories holderRepositories;

    @Autowired
    public VerificationController(
            IDeviceRevisionRepository deviceRevisionRepository,
            INamePartRevisionRepository namePartRevisionRepository,
            IWipNameRepository iWipNameRepository,
            IWipSystemGroupRepository iWipSystemGroupRepository,
            IWipSystemRepository iWipSystemRepository,
            IWipSubsystemRepository iWipSubsystemRepository,
            IWipDisciplineRepository iWipDisciplineRepository,
            IWipDeviceGroupRepository iWipDeviceGroupRepository,
            IWipDeviceTypeRepository iWipDeviceTypeRepository,
            INameRepository iNameRepository,
            ISystemGroupRepository iSystemGroupRepository,
            ISystemRepository iSystemRepository,
            ISubsystemRepository iSubsystemRepository,
            IDisciplineRepository iDisciplineRepository,
            IDeviceGroupRepository iDeviceGroupRepository,
            IDeviceTypeRepository iDeviceTypeRepository,
            IAuditNameRepository iAuditNameRepository,
            IAuditStructureRepository iAuditStructureRepository,
            NameRepository nameRepository,
            SystemGroupRepository systemGroupRepository,
            SystemRepository systemRepository,
            SubsystemRepository subsystemRepository,
            DisciplineRepository disciplineRepository,
            DeviceGroupRepository deviceGroupRepository,
            DeviceTypeRepository deviceTypeRepository,
            AuditNameRepository auditNameRepository,
            AuditStructureRepository auditStructureRepository) {

        this.deviceRevisionRepository = deviceRevisionRepository;
        this.namePartRevisionRepository = namePartRevisionRepository;

        this.holderIWipRepositories = new HolderIWipRepositories(
                iWipNameRepository,
                iWipSystemGroupRepository,
                iWipSystemRepository,
                iWipSubsystemRepository,
                iWipDisciplineRepository,
                iWipDeviceGroupRepository,
                iWipDeviceTypeRepository);
        this.holderIRepositories = new HolderIRepositories(
                iNameRepository,
                iSystemGroupRepository,
                iSystemRepository,
                iSubsystemRepository,
                iDisciplineRepository,
                iDeviceGroupRepository,
                iDeviceTypeRepository,
                iAuditNameRepository,
                iAuditStructureRepository);
        this.holderRepositories = new HolderRepositories(
                nameRepository,
                systemGroupRepository,
                systemRepository,
                subsystemRepository,
                disciplineRepository,
                deviceGroupRepository,
                deviceTypeRepository,
                auditNameRepository,
                auditStructureRepository);
    }

    /**
     * Perform verification of data migration with focus on structures, i.e. name part revisions.
     * Ok if all entries ok and no entry nok.
     *
     * @return report of data migration
     */
    @GetMapping(
            path     = {"/migration_structures"},
            produces = {"text/plain"})
    public String verifyMigrationStructures() {
        // about
        //     verification of structures (after) vs. name parts (before)

        // how
        //     find data for verification
        //         structures - systemgroup, system, subsystem, discipline, devicegroup, devicetype
        //         name parts - namepartrevision, namepart
        //     utility
        //         interpret data into hashmaps for faster retrieval in checks
        //     check
        //         for each entry in each structure
        //             check entry by entry
        //                 each attribute as expected
        //     ok if
        //         all entries ok
        //         no entry nok

        // e.g. if there has been usage of new database tables (new entries) before verification, there are more rows in those tables
        //     which means new and more ids than in old database tables
        //     which means those new ids don't exist in old database tables
        //     --> retrieval of device revisions for those ids will give null

        StringBuilder report = new StringBuilder();

        prepareLogReport(report, "About verification for data migration of structures");
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date()));
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, "    Compare structures data before and after migration");
        prepareLogReport(report, "    Check that each entry is migrated and that content after migration is same as content before migration");
        prepareLogReport(report, "    ");
        prepareLogReport(report, "    1) Find data for structures before migration (all entries in tables)");
        prepareLogReport(report, "       Find data for structures after  migration (all entries in tables)");
        prepareLogReport(report, "    2) Prepare map (id, namepartrevision) for data before migration");
        prepareLogReport(report, "       Prepare map (uuid, max id)         for data after  migration, for each kind of structure, corresponding to latest approved entry in each line of uuid");
        prepareLogReport(report, "    3) Process each kind of structure entry and each entry therein (data after migration)");
        prepareLogReport(report, "       For each entry, find corresponding data before migration and check that attributes are same and that correct entry has latest attribute set");
        prepareLogReport(report, "       Count entries for which data are same and not same (ok, nok)");
        prepareLogReport(report, "    4) Result is ok if");
        prepareLogReport(report, "           number of entries before and after migration are same");
        prepareLogReport(report, "           number of entries which are not same is 0");
        prepareLogReport(report, DIVIDER_128);

        // find data

        List<NamePartRevision> namePartRevisions = namePartRevisionRepository.findAll();
        List<WipSystemGroup>      systemGroups      = holderIWipRepositories.systemGroupRepository().findAll();
        List<WipSystem>           systems           = holderIWipRepositories.systemRepository().findAll();
        List<WipSubsystem>        subsystems        = holderIWipRepositories.subsystemRepository().findAll();
        List<WipDiscipline>       disciplines       = holderIWipRepositories.disciplineRepository().findAll();
        List<WipDeviceGroup>      deviceGroups      = holderIWipRepositories.deviceGroupRepository().findAll();
        List<WipDeviceType>       deviceTypes       = holderIWipRepositories.deviceTypeRepository().findAll();

        int sumStructures = systemGroups.size()
                + systems.size()
                + subsystems.size()
                + disciplines.size()
                + deviceGroups.size()
                + deviceTypes.size();

        prepareLogReport(report, "Find data");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# namepartrevision:", SPACE_UNTIL_SIZE_48) + namePartRevisions.size());
        prepareLogReport(report, addSpaceUntilSize("# wip systemgroup:", SPACE_UNTIL_SIZE_48) + systemGroups.size());
        prepareLogReport(report, addSpaceUntilSize("# wip system:", SPACE_UNTIL_SIZE_48) + systems.size());
        prepareLogReport(report, addSpaceUntilSize("# wip subsystem:", SPACE_UNTIL_SIZE_48) + subsystems.size());
        prepareLogReport(report, addSpaceUntilSize("# wip discipline:", SPACE_UNTIL_SIZE_48) + disciplines.size());
        prepareLogReport(report, addSpaceUntilSize("# wip devicegroup:", SPACE_UNTIL_SIZE_48) + deviceGroups.size());
        prepareLogReport(report, addSpaceUntilSize("# wip devicetype:", SPACE_UNTIL_SIZE_48) + deviceTypes.size());
        prepareLogReport(report, addSpaceUntilSize("# sum:", SPACE_UNTIL_SIZE_48) + sumStructures);
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# systemgroup:", SPACE_UNTIL_SIZE_48) + holderIRepositories.systemGroupRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# system:", SPACE_UNTIL_SIZE_48) + holderIRepositories.systemRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# subsystem:", SPACE_UNTIL_SIZE_48) + holderIRepositories.subsystemRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# discipline:", SPACE_UNTIL_SIZE_48) + holderIRepositories.disciplineRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# devicegroup:", SPACE_UNTIL_SIZE_48) + holderIRepositories.deviceGroupRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# devicetype:", SPACE_UNTIL_SIZE_48) + holderIRepositories.deviceTypeRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# audit systemgroup:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.SYSTEMGROUP, null));
        prepareLogReport(report, addSpaceUntilSize("# audit system:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.SYSTEM, null));
        prepareLogReport(report, addSpaceUntilSize("# audit subsystem:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.SUBSYSTEM, null));
        prepareLogReport(report, addSpaceUntilSize("# audit discipline:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.DISCIPLINE, null));
        prepareLogReport(report, addSpaceUntilSize("# audit devicegroup:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.DEVICEGROUP, null));
        prepareLogReport(report, addSpaceUntilSize("# audit devicetype:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditStructureRepository().countAuditStructures(Type.DEVICETYPE, null));
        prepareLogReport(report, DIVIDER_96);

        // utility
        //     interpret lists into hashmaps for faster retrieval in for loop below
        //     used to help check name entries
        //     ----------
        //     mapIdNamePartRevision   - find corresponding (old) name part revision for given id
        //     mapUuidMaxIdSystemGroup - find out latest system group id for given uuid
        //     mapUuidMaxIdSystem      - find out latest system id for given uuid
        //     mapUuidMaxIdSubsystem   - find out latest subsystem id for given uuid
        //     mapUuidMaxIdDiscipline  - find out latest discipline id for given uuid
        //     mapUuidMaxIdDeviceGroup - find out latest device group id for given uuid
        //     mapUuidMaxIdDeviceType  - find out latest device type id for given uuid

        HashMap<Long, NamePartRevision> mapIdNamePartRevision = new HashMap<>((int)(namePartRevisions.size()/0.75 + 2));
        for (NamePartRevision namePartRevision : namePartRevisions) {
            mapIdNamePartRevision.put(namePartRevision.getId(), namePartRevision);
        }
        HashMap<UUID, Long> mapUuidMaxIdSystemGroup = new HashMap<>();
        for (WipSystemGroup systemGroup : systemGroups) {
            if (		(mapUuidMaxIdSystemGroup.get(systemGroup.getUuid()) == null
                        ||  systemGroup.getId() > mapUuidMaxIdSystemGroup.get(systemGroup.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(systemGroup.getStatus().name())) {
                mapUuidMaxIdSystemGroup.put(systemGroup.getUuid(), systemGroup.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdSystem = new HashMap<>();
        for (WipSystem system : systems) {
            if ((		mapUuidMaxIdSystem.get(system.getUuid()) == null
                        ||  system.getId() > mapUuidMaxIdSystem.get(system.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(system.getStatus().name())) {
                mapUuidMaxIdSystem.put(system.getUuid(), system.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdSubsystem = new HashMap<>();
        for (WipSubsystem subsystem : subsystems) {
            if (		(mapUuidMaxIdSubsystem.get(subsystem.getUuid()) == null
                        ||  subsystem.getId() > mapUuidMaxIdSubsystem.get(subsystem.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(subsystem.getStatus().name())) {
                mapUuidMaxIdSubsystem.put(subsystem.getUuid(), subsystem.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDiscipline = new HashMap<>();
        for (WipDiscipline discipline : disciplines) {
            if ((mapUuidMaxIdDiscipline.get(discipline.getUuid()) == null
                    ||  discipline.getId() > mapUuidMaxIdDiscipline.get(discipline.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(discipline.getStatus().name())) {
                mapUuidMaxIdDiscipline.put(discipline.getUuid(), discipline.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDeviceGroup = new HashMap<>();
        for (WipDeviceGroup deviceGroup : deviceGroups) {
            if ((mapUuidMaxIdDeviceGroup.get(deviceGroup.getUuid()) == null
                    ||  deviceGroup.getId() > mapUuidMaxIdDeviceGroup.get(deviceGroup.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(deviceGroup.getStatus().name())) {
                mapUuidMaxIdDeviceGroup.put(deviceGroup.getUuid(), deviceGroup.getId());
            }
        }
        HashMap<UUID, Long> mapUuidMaxIdDeviceType = new HashMap<>();
        for (WipDeviceType deviceType : deviceTypes) {
            if ((mapUuidMaxIdDeviceType.get(deviceType.getUuid()) == null
                    ||  deviceType.getId() > mapUuidMaxIdDeviceType.get(deviceType.getUuid()))
                    && NamePartRevisionStatus.APPROVED.name().equals(deviceType.getStatus().name())) {
                mapUuidMaxIdDeviceType.put(deviceType.getUuid(), deviceType.getId());
            }
        }

        prepareLogReport(report, "Utility");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# map id namepartrevision:", SPACE_UNTIL_SIZE_48) + mapIdNamePartRevision.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved systemgroup:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdSystemGroup.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved system:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdSystem.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved subsystem:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdSubsystem.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved discipline:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdDiscipline.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved devicegroup:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdDeviceGroup.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid approved devicetype:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdDeviceType.size());
        prepareLogReport(report, DIVIDER_96);

        // keep track of id
        //     ok
        //     nok
        SortedSet<Long> idOkNamePartRevision  = new TreeSet<>();
        SortedSet<Long> idNokNamePartRevision = new TreeSet<>();

        prepareLogReport(report, addSpaceUntilSize("Check", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# ok namepartrevision", SPACE_UNTIL_SIZE_32) + addSpaceUntilSize("# nok namepartrevision", SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("before", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // system group
        // check entry by entry
        //     each attribute as expected
        boolean check = false;
        NamePartRevision namePartRevision = null;
        for (WipSystemGroup systemGroup : systemGroups) {
            namePartRevision = mapIdNamePartRevision.get(systemGroup.getId());

            // no check for parent uuid
            check = namePartRevision != null
                    && equals(systemGroup, namePartRevision, mapUuidMaxIdSystemGroup);

            // add to count
            if (check) {
                idOkNamePartRevision.add(systemGroup.getId());
            } else {
                idNokNamePartRevision.add(systemGroup.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after systemgroup", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // system
        // check entry by entry
        //     each attribute as expected
        for (WipSystem system : systems) {
            namePartRevision = mapIdNamePartRevision.get(system.getId());

            check = namePartRevision != null
                    && system.getParentUuid().equals(namePartRevision.getParent().getUuid())
                    && equals(system, namePartRevision, mapUuidMaxIdSystem);

            // add to count
            if (check) {
                idOkNamePartRevision.add(system.getId());
            } else {
                idNokNamePartRevision.add(system.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after system", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // subsystem
        // check entry by entry
        //     each attribute as expected
        for (WipSubsystem subsystem : subsystems) {
            namePartRevision = mapIdNamePartRevision.get(subsystem.getId());

            check = namePartRevision != null
                    && subsystem.getParentUuid().equals(namePartRevision.getParent().getUuid())
                    && equals(subsystem, namePartRevision, mapUuidMaxIdSubsystem);

            // add to count
            if (check) {
                idOkNamePartRevision.add(subsystem.getId());
            } else {
                idNokNamePartRevision.add(subsystem.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after subsystem", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // discipline
        // check entry by entry
        //     each attribute as expected
        for (WipDiscipline discipline : disciplines) {
            namePartRevision = mapIdNamePartRevision.get(discipline.getId());

            // no check for parent uuid
            check = namePartRevision != null
                    && equals(discipline, namePartRevision, mapUuidMaxIdDiscipline);

            // add to count
            if (check) {
                idOkNamePartRevision.add(discipline.getId());
            } else {
                idNokNamePartRevision.add(discipline.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after discipline", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // device group
        // check entry by entry
        //     each attribute as expected
        for (WipDeviceGroup deviceGroup : deviceGroups) {
            namePartRevision = mapIdNamePartRevision.get(deviceGroup.getId());

            check = namePartRevision != null
                    && deviceGroup.getParentUuid().equals(namePartRevision.getParent().getUuid())
                    && equals(deviceGroup, namePartRevision, mapUuidMaxIdDeviceGroup);

            // add to count
            if (check) {
                idOkNamePartRevision.add(deviceGroup.getId());
            } else {
                idNokNamePartRevision.add(deviceGroup.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after devicegroup", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));

        // device type
        // check entry by entry
        //     each attribute as expected
        for (WipDeviceType deviceType : deviceTypes) {
            namePartRevision = mapIdNamePartRevision.get(deviceType.getId());

            check = namePartRevision != null
                    && deviceType.getParentUuid().equals(namePartRevision.getParent().getUuid())
                    && equals(deviceType, namePartRevision, mapUuidMaxIdDeviceType);

            // add to count
            if (check) {
                idOkNamePartRevision.add(deviceType.getId());
            } else {
                idNokNamePartRevision.add(deviceType.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after devicetype", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkNamePartRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokNamePartRevision.size(), SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);

        // ok if
        //     all entries ok
        //     no entry nok
        boolean ok = sumStructures == namePartRevisions.size()
                && idOkNamePartRevision.size() == namePartRevisions.size()
                && idNokNamePartRevision.isEmpty();

        prepareLogReport(report, "Result");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, "ok:  " + ok);
        prepareLogReport(report, DIVIDER_128);

        return report.toString();
    }

    /**
     * Perform verification of data migration with focus on names, i.e. device revisions.
     * Ok if all entries ok and no entry nok.
     *
     * @return report of data migration
     */
    @GetMapping(
            path     = {"/migration_names"},
            produces = {"text/plain"})
    public String verifyMigrationNames() {
        // about
        //     verification of names (after) vs. devices (before)

        // how
        //     find data for verification
        //         names   - name
        //         devices - devicerevision, device
        //         with help of namepartrevision, namepart
        //     utility
        //         interpret data into hashmaps for faster retrieval in checks
        //     check
        //         for each name
        //             check entry by entry
        //                 each attribute as expected
        //     ok if
        //         all entries ok
        //         no entry nok

        // e.g. if there has been usage of new database tables (new entries) before verification, there are more rows in those tables
        //     which means new and more ids than in old database tables
        //     which means those new ids don't exist in old database tables
        //     --> retrieval of device revisions for those ids will give null

        StringBuilder report = new StringBuilder();

        prepareLogReport(report, "About verification for data migration of names");
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date()));
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, "    Compare names data before and after migration");
        prepareLogReport(report, "    Check that each entry is migrated and that content after migration is same as content before migration");
        prepareLogReport(report, "    ");
        prepareLogReport(report, "    1) Find data for structures before migration (all entries in tables)");
        prepareLogReport(report, "       Find data for names      before migration (all entries in tables)");
        prepareLogReport(report, "       Find data for names      after  migration (all entries in tables)");
        prepareLogReport(report, "    2) Prepare map (uuid, namepartrevision) for data before migration");
        prepareLogReport(report, "       Prepare map (id, devicerevision)     for data before migration");
        prepareLogReport(report, "       Prepare map (uuid, max id)           for data after  migration, for each name, corresponding to latest approved entry in each line of uuid");
        prepareLogReport(report, "    3) Process each name entry (data after migration)");
        prepareLogReport(report, "       For each entry, find corresponding data before migration and check that attributes are same and that correct entry has latest attribute set");
        prepareLogReport(report, "       Count entries for which data are same and not same (ok, nok)");
        prepareLogReport(report, "    4) Result is ok if");
        prepareLogReport(report, "           number of entries before and after migration are same");
        prepareLogReport(report, "           number of entries which are not same is 0");
        prepareLogReport(report, DIVIDER_128);

        // find data

        List<NamePartRevision> namePartRevisions = namePartRevisionRepository.findAll();
        List<DeviceRevision>   deviceRevisions   = deviceRevisionRepository.findAll();
        List<WipName>          names             = holderIWipRepositories.nameRepository().findAll();

        prepareLogReport(report, "Find data");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# namepartrevision:", SPACE_UNTIL_SIZE_48) + namePartRevisions.size());
        prepareLogReport(report, addSpaceUntilSize("# devicerevision:", SPACE_UNTIL_SIZE_48) + deviceRevisions.size());
        prepareLogReport(report, addSpaceUntilSize("# wip name:", SPACE_UNTIL_SIZE_48) + names.size());
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# name:", SPACE_UNTIL_SIZE_48) + holderIRepositories.nameRepository().findAll().size());
        prepareLogReport(report, addSpaceUntilSize("# audit name:", SPACE_UNTIL_SIZE_48) + holderRepositories.auditNameRepository().countAuditNames(null));
        prepareLogReport(report, DIVIDER_96);

        // utility
        //     interpret lists into hashmaps for faster retrieval in for loop below
        //     used to help check name entries
        //     ----------
        //     mapUuidNamePartRevision - find out name part revision for given uuid
        //     mapIdDeviceRevision     - find corresponding (old) device revision for given id
        //     mapUuidMaxIdName        - find out latest name id for given uuid

        HashMap<UUID, NamePartRevision> mapUuidNamePartRevision = new HashMap<>((int)(namePartRevisions.size()/0.75 + 2));
        for (NamePartRevision namePartRevision : namePartRevisions) {
            mapUuidNamePartRevision.put(namePartRevision.getNamePart().getUuid(), namePartRevision);
        }
        HashMap<Long, DeviceRevision> mapIdDeviceRevision = new HashMap<>((int)(deviceRevisions.size()/0.75 + 2));
        for (DeviceRevision deviceRevision : deviceRevisions) {
            mapIdDeviceRevision.put(deviceRevision.getId(), deviceRevision);
        }
        HashMap<UUID, Long> mapUuidMaxIdName = new HashMap<>();
        for (WipName name : names) {
            if (mapUuidMaxIdName.get(name.getUuid()) == null
                    ||  name.getId() > mapUuidMaxIdName.get(name.getUuid())) {
                mapUuidMaxIdName.put(name.getUuid(), name.getId());
            }
        }

        prepareLogReport(report, "Utility");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# map uuid namepartrevision:", SPACE_UNTIL_SIZE_48) + mapUuidNamePartRevision.size());
        prepareLogReport(report, addSpaceUntilSize("# map id devicerevision:", SPACE_UNTIL_SIZE_48) + mapIdDeviceRevision.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid maxid name:", SPACE_UNTIL_SIZE_48) + mapUuidMaxIdName.size());
        prepareLogReport(report, DIVIDER_96);

        // keep track of id
        //     ok
        //     nok
        SortedSet<Long> idOkDeviceRevision  = new TreeSet<>();
        SortedSet<Long> idNokDeviceRevision = new TreeSet<>();

        prepareLogReport(report, addSpaceUntilSize("Check", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# ok devicerevision", SPACE_UNTIL_SIZE_32) + addSpaceUntilSize("# nok devicerevision", SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("before", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkDeviceRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokDeviceRevision.size(), SPACE_UNTIL_SIZE_32));

        // name
        // check entry by entry
        //     each attribute as expected
        boolean check = false;
        DeviceRevision deviceRevision = null;
        try {
        for (WipName name : names) {
            deviceRevision = mapIdDeviceRevision.get(name.getId());

            check = deviceRevision != null
                    && equals(name, deviceRevision, mapUuidMaxIdName, mapUuidNamePartRevision);

            // add to count
            if (check) {
                idOkDeviceRevision.add(name.getId());
            } else {
                idNokDeviceRevision.add(name.getId());
            }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }

        prepareLogReport(report, addSpaceUntilSize("after", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(idOkDeviceRevision.size(), SPACE_UNTIL_SIZE_32) + addSpaceUntilSize(idNokDeviceRevision.size(), SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);

        // ok if
        //     all entries ok
        //     no entry nok
        boolean ok = names.size() == deviceRevisions.size()
                && idOkDeviceRevision.size() == deviceRevisions.size()
                && idNokDeviceRevision.isEmpty();

        prepareLogReport(report, "Result");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, "ok:  " + ok);
        prepareLogReport(report, DIVIDER_128);

        return report.toString();
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Perform verification of data in the sense that all data can be reached. Current data is one thing but suggested data (pending) and old data (obsolete) is another thing.
     * All data reached verification can be done like selecting all entries from tables (names, system group, system, subsystem, discipline, device group, device type)
     * and then take uuid and retrieve history for uuid. By that, all entries should be reached. In a sense, select distinct uuid, then retrieve history by uuid,
     * all ids should be encompassed by looking at all returned rows. Requires a couple of for loops and maps and sets to keep track of things.
     *
     * This verification concerns itself with new database, not old database.
     *
     * @return report of reachability of data
     */
    @GetMapping(
            path     = {"/data_reachable"},
            produces = {"text/plain"})
    public String verifyDataReachable() {
        StringBuilder report = new StringBuilder();

        prepareLogReport(report, "About verification for reachability of data");
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date()));
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, "    Check that each entry in tables for migrated data (structures, names) also can be reached through find by id");
        prepareLogReport(report, "    ");
        prepareLogReport(report, "    1) Find data for structures after migration (all entries in tables)");
        prepareLogReport(report, "       Find data for names      after migration (all entries in tables)");
        prepareLogReport(report, "    2) Process each name entry (data after migration)");
        prepareLogReport(report, "       For each entry, find corresponding data by id");
        prepareLogReport(report, "       Count entries found by id");
        prepareLogReport(report, "    3) Process each kind of structure entry and each entry therein (data after migration)");
        prepareLogReport(report, "       For each entry, find corresponding data by id");
        prepareLogReport(report, "       Count entries found by id");
        prepareLogReport(report, "    4) Result is ok if");
        prepareLogReport(report, "           number of entries (name)      is same as number of entries found by id");
        prepareLogReport(report, "           number of entries (structure) is same as number of entries found by id");
        prepareLogReport(report, DIVIDER_128);

        List<WipName>             names             = holderIWipRepositories.nameRepository().findAll();
        List<WipSystemGroup>      systemGroups      = holderIWipRepositories.systemGroupRepository().findAll();
        List<WipSystem>           systems           = holderIWipRepositories.systemRepository().findAll();
        List<WipSubsystem>        subsystems        = holderIWipRepositories.subsystemRepository().findAll();
        List<WipDiscipline>       disciplines       = holderIWipRepositories.disciplineRepository().findAll();
        List<WipDeviceGroup>      deviceGroups      = holderIWipRepositories.deviceGroupRepository().findAll();
        List<WipDeviceType>       deviceTypes       = holderIWipRepositories.deviceTypeRepository().findAll();

        prepareLogReport(report, "Find data");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# name:", SPACE_UNTIL_SIZE_48) + names.size());
        prepareLogReport(report, addSpaceUntilSize("# systemgroup:", SPACE_UNTIL_SIZE_48) + systemGroups.size());
        prepareLogReport(report, addSpaceUntilSize("# system:", SPACE_UNTIL_SIZE_48) + systems.size());
        prepareLogReport(report, addSpaceUntilSize("# subsystem:", SPACE_UNTIL_SIZE_48) + subsystems.size());
        prepareLogReport(report, addSpaceUntilSize("# discipline:", SPACE_UNTIL_SIZE_48) + disciplines.size());
        prepareLogReport(report, addSpaceUntilSize("# devicegroup:", SPACE_UNTIL_SIZE_48) + deviceGroups.size());
        prepareLogReport(report, addSpaceUntilSize("# devicetype:", SPACE_UNTIL_SIZE_48) + deviceTypes.size());
        prepareLogReport(report, DIVIDER_96);

        HashSet<Long> foundbyIdName        = new HashSet<>();
        HashSet<Long> foundbyIdSystemGroup = new HashSet<>();
        HashSet<Long> foundbyIdSystem      = new HashSet<>();
        HashSet<Long> foundbyIdSubsystem   = new HashSet<>();
        HashSet<Long> foundbyIdDiscipline  = new HashSet<>();
        HashSet<Long> foundbyIdDeviceGroup = new HashSet<>();
        HashSet<Long> foundbyIdDeviceType  = new HashSet<>();

        List<WipName>             namesByUuid             = null;
        List<WipSystemGroup>      systemGroupsByUuid      = null;
        List<WipSystem>           systemsByUuid           = null;
        List<WipSubsystem>        subsystemsByUuid        = null;
        List<WipDiscipline>       disciplinesByUuid       = null;
        List<WipDeviceGroup>      deviceGroupsByUuid      = null;
        List<WipDeviceType>       deviceTypesByUuid       = null;

        prepareLogReport(report, addSpaceUntilSize("Check", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# found by id", SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);

        // get started
        //     go through list(s)
        //     get entries per uuid - to mimic find history through REST API by uuid
        //     keep track of ids in set(s)
        //     ok if set(s) size same as list(s) size
        //     any entry not in set or once in set

        for (WipName name : names) {
            // to mimic - Find history for name by uuid
            namesByUuid = holderIWipRepositories.nameRepository().findByUuid(name.getUuid().toString());
            for (WipName nameByUuid : namesByUuid) {
                foundbyIdName.add(nameByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after name", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdName.size(), SPACE_UNTIL_SIZE_32));

        for (WipSystemGroup systemGroup : systemGroups) {
            // to mimic - Find history for system group by uuid
            systemGroupsByUuid = holderIWipRepositories.systemGroupRepository().findByUuid(systemGroup.getUuid().toString());
            for (WipSystemGroup systemGroupByUuid : systemGroupsByUuid) {
                foundbyIdSystemGroup.add(systemGroupByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after systemgroup", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdSystemGroup.size(), SPACE_UNTIL_SIZE_32));

        for (WipSystem system : systems) {
            // to mimic - Find history for system by uuid
            systemsByUuid = holderIWipRepositories.systemRepository().findByUuid(system.getUuid().toString());
            for (WipSystem systemByUuid : systemsByUuid) {
                foundbyIdSystem.add(systemByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after system", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdSystem.size(), SPACE_UNTIL_SIZE_32));

        for (WipSubsystem subsystem : subsystems) {
            // to mimic - Find history for subsystem by uuid
            subsystemsByUuid = holderIWipRepositories.subsystemRepository().findByUuid(subsystem.getUuid().toString());
            for (WipSubsystem subsystemByUuid : subsystemsByUuid) {
                foundbyIdSubsystem.add(subsystemByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after subsystem", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdSubsystem.size(), SPACE_UNTIL_SIZE_32));

        for (WipDiscipline discipline : disciplines) {
            // to mimic - Find history for discipline by uuid
            disciplinesByUuid = holderIWipRepositories.disciplineRepository().findByUuid(discipline.getUuid().toString());
            for (WipDiscipline disciplineByUuid : disciplinesByUuid) {
                foundbyIdDiscipline.add(disciplineByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after discipline", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdDiscipline.size(), SPACE_UNTIL_SIZE_32));

        for (WipDeviceGroup deviceGroup : deviceGroups) {
            // to mimic - Find history for device group by uuid
            deviceGroupsByUuid = holderIWipRepositories.deviceGroupRepository().findByUuid(deviceGroup.getUuid().toString());
            for (WipDeviceGroup deviceGroupByUuid : deviceGroupsByUuid) {
                foundbyIdDeviceGroup.add(deviceGroupByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after devicegroup", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdDeviceGroup.size(), SPACE_UNTIL_SIZE_32));

        for (WipDeviceType deviceType : deviceTypes) {
            // to mimic - Find history for device type by uuid
            deviceTypesByUuid = holderIWipRepositories.deviceTypeRepository().findByUuid(deviceType.getUuid().toString());
            for (WipDeviceType deviceTypeByUuid : deviceTypesByUuid) {
                foundbyIdDeviceType.add(deviceTypeByUuid.getId());
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after devicetype", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize(foundbyIdDeviceType.size(), SPACE_UNTIL_SIZE_32));
        prepareLogReport(report, DIVIDER_96);

        boolean ok = names.size() == foundbyIdName.size()
                && systemGroups.size() == foundbyIdSystemGroup.size()
                && systems.size() == foundbyIdSystem.size()
                && subsystems.size() == foundbyIdSubsystem.size()
                && disciplines.size() == foundbyIdDiscipline.size()
                && deviceGroups.size() == foundbyIdDeviceGroup.size()
                && deviceTypes.size() == foundbyIdDeviceType.size();

        prepareLogReport(report, "Result");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, "ok:  " + ok);
        prepareLogReport(report, DIVIDER_128);

        return report.toString();
    }

    /**
     * Perform verification of differences between old REST API and new REST API.
     *
     * Amount is less in new than in old. Verify that all content in new REST API is in old REST API and that difference is old/obsolete data.
     *
     * @return
     */
    @GetMapping(
            path     = {"/restapi_oldvsnew"},
            produces = {"text/plain"})
    public String verifyRestApiOldVsNew() {
        StringBuilder report = new StringBuilder();

        prepareLogReport(report, "About verification for REST API, old vs new");
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date()));
        prepareLogReport(report, DIVIDER_128);
        prepareLogReport(report, "    Check that difference between content in new REST API vs content in old REST API is as expected");
        prepareLogReport(report, "    Old REST API - One value per name equivalence");
        prepareLogReport(report, "    New REST API - Obsolete values are not shown unless history is requested");
        prepareLogReport(report, "    ");
        prepareLogReport(report, "    1) Find data for names      before migration (all entries in tables)");
        prepareLogReport(report, "       Prepare cache for names  before migration");
        prepareLogReport(report, "       Find data for names      after  migration (latest)");
        prepareLogReport(report, "    2) Prepare map (id, devicerevision)     for data before migration");
        prepareLogReport(report, "       Prepare map (id, devicerevision)     for data before migration for comparing difference old - new REST API (same as above at start))");
        prepareLogReport(report, "       Prepare map (id, uuid)               for data before migration");
        prepareLogReport(report, "       Prepare map (uuid, name)             for data after  migration (latest)");
        prepareLogReport(report, "    3) Look into old REST API");
        prepareLogReport(report, "    4) Compare old REST API with new REST API");
        prepareLogReport(report, "       Make note of difference (1)");
        prepareLogReport(report, "    5) Check that difference is old / obsolete data");
        prepareLogReport(report, "       Make note of difference (2)");
        prepareLogReport(report, "    6) Result is ok if");
        prepareLogReport(report, "           all entries in difference are superseded by later entries in new REST API");
        prepareLogReport(report, "           no entry in difference is available in new REST API");
        prepareLogReport(report, DIVIDER_128);

        // prepare old REST API

        List<DeviceRevision> deviceRevisions = deviceRevisionRepository.findAll();

        // old REST API cache
        NameViewProvider nameViewProvider = new NameViewProvider();
        nameViewProvider.update(deviceRevisions);

        // prepare new REST API

        List<WipName> namesLatest = holderIWipRepositories.nameRepository().findLatest();

        prepareLogReport(report, "Find data");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# devicerevision:", SPACE_UNTIL_SIZE_48) + deviceRevisions.size());
        prepareLogReport(report, addSpaceUntilSize("# cache:", SPACE_UNTIL_SIZE_48) + nameViewProvider.getNameRevisions().entrySet().size());
        prepareLogReport(report, addSpaceUntilSize("# name latest:", SPACE_UNTIL_SIZE_48) + namesLatest.size());
        prepareLogReport(report, DIVIDER_96);

        // prepare comparison

        HashMap<Long, DeviceRevision> mapIdDeviceRevision           = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        HashMap<Long, DeviceRevision> mapIdDeviceRevisionDifference = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        HashMap<Long, UUID>           mapIdUuidDeviceRevision       = new HashMap<>((int)(nameViewProvider.getNameRevisions().entrySet().size()/0.75 + 2));
        for (Map.Entry<String, DeviceRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            mapIdDeviceRevision.put(entry.getValue().getId(), entry.getValue());
            mapIdDeviceRevisionDifference.put(entry.getValue().getId(), entry.getValue());
            mapIdUuidDeviceRevision.put(entry.getValue().getId(), entry.getValue().getDevice().getUuid());
        }

        HashMap<UUID, WipName> mapUuidNameLatest = new HashMap<>((int)(namesLatest.size()/0.75 + 2));
        for (WipName name : namesLatest) {
            mapUuidNameLatest.put(name.getUuid(), name);
        }

        prepareLogReport(report, "Utility");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# map id devicerevision:", SPACE_UNTIL_SIZE_48) + mapIdDeviceRevision.size());
        prepareLogReport(report, addSpaceUntilSize("# map id devicerevision difference:", SPACE_UNTIL_SIZE_48) + mapIdDeviceRevisionDifference.size());
        prepareLogReport(report, addSpaceUntilSize("# map id uuid devicerevision:", SPACE_UNTIL_SIZE_48) + mapIdUuidDeviceRevision.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid name latest:", SPACE_UNTIL_SIZE_48) + mapUuidNameLatest.size());
        prepareLogReport(report, DIVIDER_96);

        // find out about old REST API and quality of data
        // 0. uuid
        //        know how many uuid exist multiple times in old rest api

        HashMap<UUID, Long> mapUUIDCountCache = new HashMap<>();
        for (Entry<String, DeviceRevision> entry : nameViewProvider.getNameRevisions().entrySet()) {
            UUID uuid = entry.getValue().getDevice().getUuid();
            Long count = mapUUIDCountCache.get(uuid);
            long value = count != null ? count.longValue() + 1 : 1;
            mapUUIDCountCache.put(uuid, value);
        }

        Map<UUID, Long> mapUUIDCountCacheDuplicateSorted = mapUUIDCountCache.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.naturalOrder()))
                .filter(e -> e.getValue() > 1)
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
        Optional<Entry<UUID, Long>> lastEntry = mapUUIDCountCacheDuplicateSorted.entrySet().stream().reduce((e1, e2) -> e2);
        UUID lastKey = lastEntry.isPresent() ? lastEntry.get().getKey() : null;
        Long lastCount = mapUUIDCountCacheDuplicateSorted.get(lastKey);

        prepareLogReport(report, "Check old REST API");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("# map uuid count cache:", SPACE_UNTIL_SIZE_48) + mapUUIDCountCache.size());
        prepareLogReport(report, addSpaceUntilSize("# map uuid count cache duplicate sorted:", SPACE_UNTIL_SIZE_48) + mapUUIDCountCacheDuplicateSorted.size());
        prepareLogReport(report, addSpaceUntilSize("# count most duplicated uuid:", SPACE_UNTIL_SIZE_48) + lastCount);
        prepareLogReport(report, DIVIDER_96);

        // compare old REST API with new REST API
        // 1. find difference
        //        subtract id in new REST API from old REST API
        //        result is difference

        boolean idInOld = true;
        int countIdInOld = 0;
        int countIdNotInOld = 0;
        int countIdNotInOldButDeleted = 0;
        for (WipName name : namesLatest) {
            idInOld = mapIdDeviceRevisionDifference.containsKey(name.getId());
            if (idInOld) {
                countIdInOld++;
            } else if (!idInOld) {
                // either bug in old REST API or bug in implementation of cache for old REST API - NEED TO INVESTIGATE
                // after investigation, appears bug in old REST API
                countIdNotInOld++;
                if (name.isDeleted()) {
                    countIdNotInOldButDeleted++;
                }
            }
            mapIdDeviceRevisionDifference.remove(name.getId());
        }

        prepareLogReport(report, "Check");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, addSpaceUntilSize("after difference overview", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# map id devicerevision difference:", SPACE_UNTIL_SIZE_40) + mapIdDeviceRevisionDifference.size());
        prepareLogReport(report, addSpaceUntilSize("after difference overview", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# count id in old:", SPACE_UNTIL_SIZE_40) + countIdInOld);
        prepareLogReport(report, addSpaceUntilSize("after difference overview", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# count id not in old:", SPACE_UNTIL_SIZE_40) + countIdNotInOld);
        prepareLogReport(report, addSpaceUntilSize("after difference overview", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# count id not in old but deleted:", SPACE_UNTIL_SIZE_40) + countIdNotInOldButDeleted);
        prepareLogReport(report, DIVIDER_96);

        // 2. check difference - is old/obsolete data
        //        can not know reason for certain
        //        find uuid corresponding to id, check that id not last in line of uuid
        //        ---------------------------------------------------------------------
        //        find uuid, name corresponding to id - should find name
        //            wrong if no name
        //            wrong if name is latest

        ArrayList<DeviceRevision> listDeviceRevisionWrong = new ArrayList<>();
        int countNameNull = 0;
        int countNameIdEqualsDiffId = 0;
        int countNameIdGreaterThanDiffId = 0;
        int countNameIdLessThanDiffId = 0;
        for (Entry<Long, DeviceRevision> entry : mapIdDeviceRevisionDifference.entrySet()) {
            // for each difference
            //     find out uuid, name -
            //     note use of mapUuidNameLatest - latest (!)

            UUID uuid = mapIdUuidDeviceRevision.get(entry.getKey());
            WipName name = uuid != null ? mapUuidNameLatest.get(uuid) : null;

            // something may be wrong
            //     no name
            //     name is in remaining difference

            if (name == null) {
                // something is wrong
                listDeviceRevisionWrong.add(entry.getValue());
                countNameNull++;
                continue;
            }
            if (name.getId().equals(entry.getKey())) {
                listDeviceRevisionWrong.add(entry.getValue());
                countNameIdEqualsDiffId++;
            } else if (name.getId().longValue() > entry.getKey().longValue()) {
                //  ok
                countNameIdGreaterThanDiffId++;
            } else {
                listDeviceRevisionWrong.add(entry.getValue());
                countNameIdLessThanDiffId++;
            }
        }
        prepareLogReport(report, addSpaceUntilSize("after difference detail", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# wrong devicerevision:", SPACE_UNTIL_SIZE_40) + listDeviceRevisionWrong.size());
        prepareLogReport(report, addSpaceUntilSize("after difference detail", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# name is nulll:", SPACE_UNTIL_SIZE_40) + countNameNull);
        prepareLogReport(report, addSpaceUntilSize("after difference detail", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# name id > id in diff:", SPACE_UNTIL_SIZE_40) + countNameIdGreaterThanDiffId);
        prepareLogReport(report, addSpaceUntilSize("after difference detail", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# name id = id in diff:", SPACE_UNTIL_SIZE_40) + countNameIdEqualsDiffId);
        prepareLogReport(report, addSpaceUntilSize("after difference detail", SPACE_UNTIL_SIZE_48) + addSpaceUntilSize("# name id < id in diff:", SPACE_UNTIL_SIZE_40) + countNameIdLessThanDiffId);
        prepareLogReport(report, DIVIDER_96);

        boolean ok = mapIdDeviceRevisionDifference.size() == countNameIdGreaterThanDiffId
                && listDeviceRevisionWrong.isEmpty();

        prepareLogReport(report, "Result");
        prepareLogReport(report, DIVIDER_96);
        prepareLogReport(report, "ok:  " + ok);
        prepareLogReport(report, DIVIDER_128);

        return report.toString();
    }

    /**
     * Utility method for preparing reports for logging purposes.
     *
     * @param report
     * @param message
     */
    private void prepareLogReport(StringBuilder report, String message) {
        if (!StringUtils.isEmpty(message)) {
            report.append(message + NEWLINE);
            LOGGER.log(Level.FINE, message);
        }
    }

    /**
     * Utility method to compare Structure with NamePartRevision.
     * No check for parent uuid in this method.
     *
     * @param structure structure
     * @param namePartRevision name part revision
     * @param mapUuidMaxId map with max id for APPROVED entry in line of uuid for structure
     * @return
     */
    private boolean equals(WipStructure structure, NamePartRevision namePartRevision, HashMap<UUID, Long> mapUuidMaxId) {
        // check
        //     check entry by entry
        //         each attribute as expected
        //     ----------
        //     to check parent uuid
        //     ----------
        //     date may be in different format for different objects, to be formatted before being compared
        //     ----------
        //     e.g.
        //     system.id                   = namepartrevision.id
        //     system.version              = namepartrevision.version
        //     system.uuid                 = namepartrevision.namepart_id      (namepart.id    --> namepart.uuid)
        //     ( system.parent_uuid        = namepartrevision.parent_id        (namepart.id    --> namepart.uuid) )
        //     system.name                 = namepartrevision.name
        //     system.mnemonic             = namepartrevision.mnemonic
        //     system.mnemonic_equivalence = namepartrevision.mnemoniceqclass
        //     system.description          = namepartrevision.description
        //     system.status               = namepartrevision.status
        //     system.latest               = true if id = get max id for uuid  (consider status, but not PENDING)
        //     system.deleted              = namepartrevision.deleted
        //     system.requested            = namepartrevision.requestdate
        //     system.requested_by         = namepartrevision.requestedby_id   (useraccount.id --> useraccount.username)
        //     system.requested_comment    = namepartrevision.requestercomment
        //     system.processed            = namepartrevision.processdate
        //     system.processed_by         = namepartrevision.processedby_id   (useraccount.id --> useraccount.username)
        //     system.processed_comment    = namepartrevision.processorcomment
        //
        //     check if deleted && (cancelled, rejected, pending) &&  no approved in line of uuid, then it might be ok from that perspective
        //     consider - status, latest, deleted

        if (ValidateUtil.isAnyNull(structure, namePartRevision, mapUuidMaxId)) {
            return false;
        }

        boolean check =  structure.getId().equals(namePartRevision.getId());
        check = check && structure.getVersion().equals(namePartRevision.getVersion());
        check = check && structure.getUuid().equals(namePartRevision.getNamePart().getUuid());

        // no check for parent uuid in this method
        //     name and description (NamePartRevision) are concatenated into description (Structure)
        //         postgresql - nullif(concat_ws('. ', npr1."name", npr1.description)
        //         to be checked - with care
        String concat = null;
        String delimiter = ". ";
        if (!StringUtils.isEmpty(namePartRevision.getName()) && !StringUtils.isEmpty(namePartRevision.getDescription())) {
            concat = namePartRevision.getName() + delimiter + namePartRevision.getDescription();
        } else if (StringUtils.isEmpty(namePartRevision.getName()) && !StringUtils.isEmpty(namePartRevision.getDescription())) {
            concat = namePartRevision.getDescription();
        } else if (!StringUtils.isEmpty(namePartRevision.getName()) && StringUtils.isEmpty(namePartRevision.getDescription())) {
            concat = namePartRevision.getName();
        }

        // check = check && StringUtils.equals(structure.getName(), namePartRevision.getName());
        check = check && StringUtils.equals(structure.getMnemonic(), namePartRevision.getMnemonic());
        check = check && StringUtils.equals(structure.getMnemonicEquivalence(), namePartRevision.getMnemonicEqClass());
        // check = check && StringUtils.equals(structure.getDescription(), namePartRevision.getDescription());
        check = check && StringUtils.equals(structure.getDescription(), concat);
        check = check && ((structure.getStatus() == null && namePartRevision.getStatus() == null)
                            || (structure.getStatus().name().equals(namePartRevision.getStatus().name())));

        // latest
        //     true if id = get max id for uuid
        //     special rules
        //         for pending, not consider pending
        //         for latest, deleted but not approved - ok because there are erroneous entries in database
        //             handled with V5__Data_transformation_status_deleted.sql
        check = check &&
                (structure.isLatest() == structure.getId().equals(mapUuidMaxId.get(structure.getUuid()))
                ||  structure.isLatest() && structure.isDeleted());
        check = check &&
                (structure.isDeleted() == namePartRevision.isDeleted()
                ||  structure.isLatest() && structure.isDeleted());

        // date may be in different format for different objects, to be formatted before being compared
        check = check && ((structure.getRequested() == null && namePartRevision.getRequestDate() == null)
                            || StringUtils.equals(SDF.format(structure.getRequested()), SDF.format(namePartRevision.getRequestDate())));
        check = check && (structure.getRequestedBy() == null && namePartRevision.getRequestedBy() == null
                            || 	StringUtils.equals(structure.getRequestedBy(), namePartRevision.getRequestedBy().getUsername()));
        check = check && StringUtils.equals(structure.getRequestedComment(), namePartRevision.getRequesterComment());
        check = check && ((structure.getProcessed() == null && namePartRevision.getProcessDate() == null)
                            || StringUtils.equals(SDF.format(structure.getProcessed()), SDF.format(namePartRevision.getProcessDate())));
        check = check && (structure.getProcessedBy() == null && namePartRevision.getProcessedBy() == null
                            || 	StringUtils.equals(structure.getProcessedBy(), namePartRevision.getProcessedBy().getUsername()));
        check = check && StringUtils.equals(structure.getProcessedComment(), namePartRevision.getProcessorComment());

        return check;
    }

    /**
     * Utility method to compare Name with DeviceRevision.
     *
     * @param name name
     * @param deviceRevision device revision
     * @param mapUuidMaxIdName map with max id for entry in line of uuid for name
     * @param mapUuidNamePartRevision map with uuid for name part revision
     * @return
     */
    private boolean equals(WipName name, DeviceRevision deviceRevision, HashMap<UUID, Long> mapUuidMaxIdName, HashMap<UUID, NamePartRevision> mapUuidNamePartRevision) {
        // check
        //     check entry by entry
        //         each attribute as expected
        //     ----------
        //     to check  1st, 2nd, 3rd parent to determine if systemgroup, system or subsystem
        //     otherwise not clear how to make sure if it is supposed to be systemgroup and not system, subsystem and vice versa
        //     ----------
        //     date may be in different format for different objects, to be formatted before being compared
        //     ----------
        //     name.id                                = devicerevision.id
        //     name.version                           = devicerevision.version
        //     name.uuid                              = devicerevision.device_id             (device.id      --> device.uuid)
        //     name.namepartrevision_systemgroup_uuid = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_system_uuid      = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_subsystem_uuid   = devicerevision.section_id            (namepart.id    --> namepart.uuid
        //     name.namepartrevision_devicetype_uuid  = devicerevision.devicetype_id         (namepart.id    --> namepart.uuid
        //     name.instance_index                    = devicerevision.instanceindex
        //     name.convention_name                   = devicerevision.conventionname
        //     name.convention_name_equivalence       = devicerevision.conventionnameeqclass
        //     name.description                       = devicerevision.additionalinfo
        //     name.status                            = null
        //     name.latest                            = true if id = get max id for uuid     (not consider status)
        //     name.deleted                           = devicerevision.deleted
        //     name.requested                         = devicerevision.requestdate
        //     name.requested_by                      = devicerevision.requestedby_id        (useraccount.id --> useraccount.username)
        //     name.requested_comment                 = null
        //     name.processed                         = null
        //     name.processed_by                      = null
        //     name.processed_comment                 = devicerevision.processorcomment

        if (ValidateUtil.isAnyNull(name, deviceRevision, mapUuidMaxIdName)) {
            return false;
        }

        boolean check =  name.getId().equals(deviceRevision.getId());
        check = check && name.getVersion().equals(deviceRevision.getVersion());
        check = check && name.getUuid().equals(deviceRevision.getDevice().getUuid());

        // to check  1st, 2nd, 3rd parent to determine if systemgroup, system or subsystem
        // otherwise not clear how to make sure if it is supposed to be systemgroup and not system, subsystem and vice versa
        NamePartRevision parent1 = deviceRevision.getSection() != null
                ? mapUuidNamePartRevision.get(deviceRevision.getSection().getUuid())
                : null;
        NamePartRevision parent2 = parent1 != null && parent1.getParent() != null
                ? mapUuidNamePartRevision.get(parent1.getParent().getUuid())
                : null;
        NamePartRevision parent3 = parent2 != null && parent2.getParent() != null
                ? mapUuidNamePartRevision.get(parent2.getParent().getUuid())
                : null;
        // parent 1    but not parent 2, 3 - system group
        // parent 1, 2 but not parent 3    - system
        // parent 1, 2, 3                  - subsystem
        // else nok
        UUID systemGroupUuid = name.getSystemGroupUuid();
        UUID systemUuid = name.getSystemUuid();
        UUID subsystemUuid = name.getSubsystemUuid();
        UUID sectionUuid = deviceRevision.getSection().getUuid();
        if (parent1 != null && parent2 == null && parent3 == null) {
            check = check && sectionUuid.equals(systemGroupUuid) && systemUuid == null && subsystemUuid == null;
        } else if (parent1 != null && parent2 != null && parent3 == null) {
            check = check && sectionUuid.equals(systemUuid) && systemGroupUuid == null && subsystemUuid == null;
        } else if (parent1 != null && parent2 != null && parent3 != null) {
            check = check && sectionUuid.equals(subsystemUuid) && systemGroupUuid == null && systemUuid == null;
        } else {
            check = false;
        }

        check = check && ((name.getDeviceTypeUuid() == null && deviceRevision.getDeviceType() == null)
                            || (name.getDeviceTypeUuid().equals(deviceRevision.getDeviceType().getUuid())));
        check = check && StringUtils.equals(name.getInstanceIndex(), deviceRevision.getInstanceIndex());
        check = check && StringUtils.equals(name.getConventionName(), deviceRevision.getConventionName());
        check = check && StringUtils.equals(name.getConventionNameEquivalence(), deviceRevision.getConventionNameEqClass());
        check = check && StringUtils.equals(name.getDescription(), deviceRevision.getAdditionalInfo());
        check = check && name.getStatus() == null;

        // latest
        //     true if id = get max id for uuid
        check = check && name.isLatest() == name.getId().equals(mapUuidMaxIdName.get(name.getUuid()));
        check = check && name.isDeleted() == deviceRevision.isDeleted();

        // date may be in different format for different objects, to be formatted before being compared
        check = check && ((name.getRequested() == null && deviceRevision.getRequestDate() == null)
                            || StringUtils.equals(SDF.format(name.getRequested()), SDF.format(deviceRevision.getRequestDate())));
        check = check && StringUtils.equals(name.getRequestedBy(), deviceRevision.getRequestedBy().getUsername());
        check = check && StringUtils.equals(name.getRequestedComment(), deviceRevision.getProcessorComment());
        check = check && name.getProcessed() == null;
        check = check && name.getProcessedBy() == null;
        check = check && name.getProcessedComment() == null;

        return check;
    }

    /**
     * Add space to string (for value) until it has size.
     *
     * @param str value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(String str, int size) {
        if (str != null && str.length() < size) {
            StringBuilder sb = new StringBuilder(str);
            while (sb.length() < size)  {
                sb.append(SPACE);
            }
            return sb.toString();
        }
        return str;
    }
    /**
     * Add space to string (for value) until it has size.
     *
     * @param val value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(long val, int size) {
        return addSpaceUntilSize(String.valueOf(val), size);
    }

}

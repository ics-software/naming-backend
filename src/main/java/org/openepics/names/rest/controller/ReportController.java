/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.wip.WipDeviceGroupRepository;
import org.openepics.names.repository.wip.WipDeviceTypeRepository;
import org.openepics.names.repository.wip.WipDisciplineRepository;
import org.openepics.names.repository.wip.WipNameRepository;
import org.openepics.names.repository.wip.WipSubsystemRepository;
import org.openepics.names.repository.wip.WipSystemGroupRepository;
import org.openepics.names.repository.wip.WipSystemRepository;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.service.NamesService;
import org.openepics.names.service.StructuresService;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.StructureChoice;
import org.openepics.names.util.wip.HolderWipRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides reports for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "Report",
     description = "provide reports for Naming application")
@RestController
@RequestMapping("/report")
@EnableAutoConfiguration
public class ReportController {

    private static final String NEWLINE       = "\n";
    private static final String SPACE         = " ";

    private static final String DIVIDER_32    = "--------------------------------";
    private static final String DIVIDER_96    = DIVIDER_32 + DIVIDER_32 + DIVIDER_32;
    private static final String DIVIDER_128   = DIVIDER_32 + DIVIDER_32 + DIVIDER_32 + DIVIDER_32;

    private static final String NBR           = "#: ";
    private static final String NBR_HISTORY   = "# history: ";
    private static final String NBR_DISTINCT  = "# distinct: ";
    private static final String NBR_ACTIVE    = "# active: ";
    private static final String NBR_DELETED   = "# deleted: ";
    private static final String NBR_PENDING   = "# pending: ";
    private static final String NBR_CANCELLED = "# cancelled: ";
    private static final String NBR_REJECTED  = "# rejected: ";
    private static final String NBR_OTHER     = "# other: ";

    private final NamesService namesService;
    private final StructuresService structuresService;
    private final HolderWipRepositories holderWipRepositories;

    @Autowired
    public ReportController(
            NamesService namesService,
            StructuresService structuresService,
            WipNameRepository wipNnameRepository,
            WipSystemGroupRepository wipSystemGroupRepository,
            WipSystemRepository wipSystemRepository,
            WipSubsystemRepository wipSubsystemRepository,
            WipDisciplineRepository wipDisciplineRepository,
            WipDeviceGroupRepository wipDeviceGroupRepository,
            WipDeviceTypeRepository wipDeviceTypeRepository) {

        this.namesService = namesService;
        this.structuresService = structuresService;
        this.holderWipRepositories = new HolderWipRepositories(
                wipNnameRepository,
                wipSystemGroupRepository,
                wipSystemRepository,
                wipSubsystemRepository,
                wipDisciplineRepository,
                wipDeviceGroupRepository,
                wipDeviceTypeRepository);
    }

    /**
     * Return report about Naming.
     *
     * @return report about Naming (text)
     */
    @Operation(
            summary     = "About Naming",
            description = """
                          About Naming.

                          Return report about Naming (text).

                          Content
                          - Metrics for Naming
                              1) Overview
                              2) ESS names
                              3) System structure
                              4) Device structure
                              5) Device structure - P&ID Disciplines
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Method completed OK. Return report about Naming (text).",
                    content = @Content(mediaType = "text/plain", schema = @Schema(implementation = String.class)))
    })
    @GetMapping(
            path     = {"/about"},
            produces = {"text/plain"})
    public String reportAbout() {
        // report metrics about Naming
        //     ess names
        //         read valid, valid deleted, valid not deleted
        //         count for kind of name
        //     system structure, device structure
        //         read valid per structure
        //         count for status
        //
        // note
        //     obsolete values are not shown
        //     metrics can be read with service layer or repository layer
        // ----------------------------------------
        // prepare metrics
        //     names      - read, count
        //     structures - read, count
        // prepare text
        //     names      - format
        //     structures - format
        // report

        // prepare metrics
        //     read names  - valid, valid deleted, valid not deleted
        //     count names - kind of name
        //     count names - history
        ResponsePageNameElements nameElementsEssNames = namesService.readNames(null, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null);
        ResponsePageNameElements nameElementsEssNamesDeleted = namesService.readNames(Boolean.TRUE, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null);
        ResponsePageNameElements nameElementsEssNamesNotDeleted = namesService.readNames(Boolean.FALSE, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null);
        Long nbrEssNamesHistory = holderWipRepositories.nameRepository().countNamesHistory(null, null, null, null, null, null, null, null);

        HolderReportName holderReportName = new HolderReportName(nameElementsEssNamesNotDeleted);

        // prepare metrics
        //     read structures  - valid
        //     count structures - status
        //     count structures - history
        ResponsePageStructureElements structureElementsSystemGroup = structuresService.readStructures(Type.SYSTEMGROUP, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);
        ResponsePageStructureElements structureElementsSystem = structuresService.readStructures(Type.SYSTEM, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);
        ResponsePageStructureElements structureElementsSubsystem = structuresService.readStructures(Type.SUBSYSTEM, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);
        ResponsePageStructureElements structureElementsDiscipline = structuresService.readStructures(Type.DISCIPLINE, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);
        ResponsePageStructureElements structureElementsDeviceGroup = structuresService.readStructures(Type.DEVICEGROUP, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);
        ResponsePageStructureElements structureElementsDeviceType = structuresService.readStructures(Type.DEVICETYPE, null, null, null, null, null, null, null, Boolean.FALSE, null, null, null, null, StructureChoice.STRUCTURE);

        HolderReportStructure holderReportStructureSystemGroup = new HolderReportStructure(structureElementsSystemGroup);
        HolderReportStructure holderReportStructureSystem = new HolderReportStructure(structureElementsSystem);
        HolderReportStructure holderReportStructureSubsystem = new HolderReportStructure(structureElementsSubsystem);
        HolderReportStructure holderReportStructureDiscipline = new HolderReportStructure(structureElementsDiscipline);
        HolderReportStructure holderReportStructureDeviceGroup = new HolderReportStructure(structureElementsDeviceGroup);
        HolderReportStructure holderReportStructureDeviceType = new HolderReportStructure(structureElementsDeviceType);

        Long nbrSystemGroupHistory = holderWipRepositories.systemGroupRepository().countSystemGroupsHistory(null, null, null, null, null, null);
        Long nbrSystemHistory = holderWipRepositories.systemRepository().countSystemsHistory(null, null, null, null, null, null, null);
        Long nbrSubsystemHistory = holderWipRepositories.subsystemRepository().countSubsystemsHistory(null, null, null, null, null, null, null);
        Long nbrDisciplineHistory = holderWipRepositories.disciplineRepository().countDisciplinesHistory(null, null, null, null, null, null);
        Long nbrDeviceGroupHistory = holderWipRepositories.deviceGroupRepository().countDeviceGroupsHistory(null, null, null, null, null, null, null);
        Long nbrDeviceTypeHistory = holderWipRepositories.deviceTypeRepository().countDeviceTypesHistory(null, null, null, null, null, null, null);

        // prepare text
        int spaceUntilSizeStructure = 16;

        String metricsOverviewEssName     = NBR_ACTIVE + nameElementsEssNamesNotDeleted.getListSize();
        String metricsOverviewSystemGroup = NBR_ACTIVE + holderReportStructureSystemGroup.getNumberActive();
        String metricsOverviewSystem      = NBR_ACTIVE + holderReportStructureSystem.getNumberActive();
        String metricsOverviewSubsystem   = NBR_ACTIVE + holderReportStructureSubsystem.getNumberActive();
        String metricsOverviewDiscipline  = NBR_ACTIVE + holderReportStructureDiscipline.getNumberActive();
        String metricsOverviewDeviceGroup = NBR_ACTIVE + holderReportStructureDeviceGroup.getNumberActive();
        String metricsOverviewDeviceType  = NBR_ACTIVE + holderReportStructureDeviceType.getNumberActive();

        String metricsEssName =
                NBR_HISTORY    + addSpaceUntilSize(nbrEssNamesHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT + addSpaceUntilSize(nameElementsEssNames.getListSize(), spaceUntilSizeStructure)
                + NBR_ACTIVE   + addSpaceUntilSize(nameElementsEssNamesNotDeleted.getListSize(), spaceUntilSizeStructure)
                + NBR_DELETED  + addSpaceUntilSize(nameElementsEssNamesDeleted.getListSize(), spaceUntilSizeStructure);
        String metricsEssNameSystemstructure                     = NBR + holderReportName.getNumberEssNameSystemstructure();
        String metricsEssNameSystemstructureDevicestructure      = NBR + holderReportName.getNumberEssNameSystemstructureDevicestructure();
        String metricsEssNameSystemstructureDevicestructureIndex = NBR + holderReportName.getNumberEssNameSystemstructureDevicestructureIndex();
        String metricsEssNameOther                               = NBR_OTHER + holderReportName.getNumberEssNameOther();

        String metricsSystemstructureSystemGroup =
                NBR_HISTORY     + addSpaceUntilSize(nbrSystemGroupHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureSystemGroup.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureSystemGroup.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureSystemGroup.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureSystemGroup.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureSystemGroup.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureSystemGroup.getNumberRejected(), spaceUntilSizeStructure);
        String metricsSystemstructureSystem =
                NBR_HISTORY     + addSpaceUntilSize(nbrSystemHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureSystem.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureSystem.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureSystem.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureSystem.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureSystem.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureSystem.getNumberRejected(), spaceUntilSizeStructure);
        String metricsSystemstructureSubsystem =
                NBR_HISTORY     + addSpaceUntilSize(nbrSubsystemHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureSubsystem.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureSubsystem.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureSubsystem.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureSubsystem.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureSubsystem.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureSubsystem.getNumberRejected(), spaceUntilSizeStructure);

        String metricsDevicestructureDiscipline =
                NBR_HISTORY     + addSpaceUntilSize(nbrDisciplineHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureDiscipline.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureDiscipline.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureDiscipline.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureDiscipline.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureDiscipline.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureDiscipline.getNumberRejected(), spaceUntilSizeStructure);
        String metricsDevicestructureDeviceGroup =
                NBR_HISTORY     + addSpaceUntilSize(nbrDeviceGroupHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureDeviceGroup.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureDeviceGroup.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureDeviceGroup.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureDeviceGroup.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureDeviceGroup.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureDeviceGroup.getNumberRejected(), spaceUntilSizeStructure);
        String metricsDevicestructureDeviceType =
                NBR_HISTORY     + addSpaceUntilSize(nbrDeviceTypeHistory, spaceUntilSizeStructure)
                + NBR_DISTINCT  + addSpaceUntilSize(holderReportStructureDeviceType.getSizeMapUuidCount(), spaceUntilSizeStructure)
                + NBR_ACTIVE    + addSpaceUntilSize(holderReportStructureDeviceType.getNumberActive(), spaceUntilSizeStructure)
                + NBR_DELETED   + addSpaceUntilSize(holderReportStructureDeviceType.getNumberDeleted(), spaceUntilSizeStructure)
                + NBR_PENDING   + addSpaceUntilSize(holderReportStructureDeviceType.getNumberPending(), spaceUntilSizeStructure)
                + NBR_CANCELLED + addSpaceUntilSize(holderReportStructureDeviceType.getNumberCancelled(), spaceUntilSizeStructure)
                + NBR_REJECTED  + addSpaceUntilSize(holderReportStructureDeviceType.getNumberRejected(), spaceUntilSizeStructure);

        StringBuilder metricsPIDDiscipline = new StringBuilder();
        for (String discipline : NamingConventionUtil.getDisciplinesPID()) {
            metricsPIDDiscipline.append(addSpaceUntilSize(discipline, spaceUntilSizeStructure)).append(SPACE);
        }
        StringBuilder metricsPIDNumeric = new StringBuilder();
        metricsPIDNumeric.append(addSpaceUntilSize("", 68)).append("SC-IOC (*)");

        // report header
        StringBuilder sb = new StringBuilder();
        sb.append("About Naming").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append(DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date())).append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append("Name and structure data are displayed per line of uuid which is unique identifier through lifecycle of entry.").append(NEWLINE)
        .append("Note # means number of, active means valid not deleted, obsolete values are not shown unless history is requested.").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE)
        .append("Metrics for Naming").append(NEWLINE)
        .append("    1) # entries - Overview").append(NEWLINE)
        .append("    2) # entries - ESS names").append(NEWLINE)
        .append("    3) # entries - System structure").append(NEWLINE)
        .append("    4) # entries - Device structure").append(NEWLINE)
        .append("    5) Device structure - P&ID Disciplines").append(NEWLINE)
        .append(DIVIDER_128).append(NEWLINE);

        // report overview
        // report ess names
        // report system structure
        // report device structure
        // report device structure - p&id discipline
        sb.append("1) # entries - Overview").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("ESS Name                " + metricsOverviewEssName).append(NEWLINE);
        sb.append("System Group            " + metricsOverviewSystemGroup).append(NEWLINE);
        sb.append("System                  " + metricsOverviewSystem).append(NEWLINE);
        sb.append("Subsystem               " + metricsOverviewSubsystem).append(NEWLINE);
        sb.append("Discipline              " + metricsOverviewDiscipline).append(NEWLINE);
        sb.append("Device Group            " + metricsOverviewDeviceGroup).append(NEWLINE);
        sb.append("Device Type             " + metricsOverviewDeviceType).append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("2) # entries - ESS names").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        // # names with system structure only - system group, system, system + subsystem
        sb.append("ESS Name                " + metricsEssName).append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("System structure                                                               " + metricsEssNameSystemstructure).append(NEWLINE);
        sb.append("System structure + Device structure                                            " + metricsEssNameSystemstructureDevicestructure).append(NEWLINE);
        sb.append("System structure + Device structure + Index                                    " + metricsEssNameSystemstructureDevicestructureIndex).append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("3) # entries - System structure").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        // lifecycle (# active, # deleted, # pending, ...
        sb.append("System Group            " + metricsSystemstructureSystemGroup).append(NEWLINE);
        sb.append("System                  " + metricsSystemstructureSystem).append(NEWLINE);
        sb.append("Subsystem               " + metricsSystemstructureSubsystem).append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("4) # entries - Device structure").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        // lifecycle (# active, # deleted, # pending, ...
        sb.append("Discipline              " + metricsDevicestructureDiscipline).append(NEWLINE);
        sb.append("Device Group            " + metricsDevicestructureDeviceGroup).append(NEWLINE);
        sb.append("Device Type             " + metricsDevicestructureDeviceType).append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        sb.append("5) Device structure - P&ID Disciplines").append(NEWLINE);
        sb.append("                      P&ID Numeric (*)").append(NEWLINE);
        sb.append(DIVIDER_96).append(NEWLINE);
        // disciplines p&id, scientific
        sb.append(metricsPIDDiscipline.toString()).append(NEWLINE);
        sb.append(metricsPIDNumeric.toString()).append(NEWLINE);
        sb.append(DIVIDER_128).append(NEWLINE);

        // part of preparation for report is to keep track of kind of names and structures,
        // which are expected to have values according to certain kinds and statuses.
        // if unexpected kinds and statuses are kept track of (should be zero),
        // if non-zero, this is listed as message which may (should) be told to system responsible.
        // possibility to show any kind of message to user

        boolean hasMessage = holderReportName.getNumberEssNameOther() > 0
                || holderReportStructureSystemGroup.getNumberOther() > 0
                || holderReportStructureSystem.getNumberOther() > 0
                || holderReportStructureSubsystem.getNumberOther() > 0
                || holderReportStructureDiscipline.getNumberOther() > 0
                || holderReportStructureDeviceGroup.getNumberOther() > 0
                || holderReportStructureDeviceType.getNumberOther() > 0;
        if (hasMessage) {
            sb.append("Message").append(NEWLINE);
            sb.append(DIVIDER_96).append(NEWLINE);
            if (holderReportName.getNumberEssNameOther() > 0) {
                sb.append("    ESS Name            ").append(metricsEssNameOther).append(NEWLINE);
            }
            if (holderReportStructureSystemGroup.getNumberOther() > 0) {
                sb.append("    System Group        ").append(NBR_OTHER + holderReportStructureSystemGroup.getNumberOther()).append(NEWLINE);
            }
            if (holderReportStructureSystem.getNumberOther() > 0) {
                sb.append("    System              ").append(NBR_OTHER + holderReportStructureSystem.getNumberOther()).append(NEWLINE);
            }
            if (holderReportStructureSubsystem.getNumberOther() > 0) {
                sb.append("    Subsystem           ").append(NBR_OTHER + holderReportStructureSubsystem.getNumberOther()).append(NEWLINE);
            }
            if (holderReportStructureDiscipline.getNumberOther() > 0) {
                sb.append("    Discipline          ").append(NBR_OTHER + holderReportStructureDiscipline.getNumberOther()).append(NEWLINE);
            }
            if (holderReportStructureDeviceGroup.getNumberOther() > 0) {
                sb.append("    Device Group        ").append(NBR_OTHER + holderReportStructureDeviceGroup.getNumberOther()).append(NEWLINE);
            }
            if (holderReportStructureDeviceType.getNumberOther() > 0) {
                sb.append("    Device Type         ").append(NBR_OTHER + holderReportStructureDeviceType.getNumberOther()).append(NEWLINE);
            }
            sb.append(DIVIDER_128).append(NEWLINE);
        }

        return sb.toString();
    }

    /**
     * Add space to string (for value) until it has size.
     *
     * @param str value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(String str, int size) {
        if (str != null && str.length() < size) {
            StringBuilder sb = new StringBuilder(str);
            while (sb.length() < size)  {
                sb.append(SPACE);
            }
            return sb.toString();
        }
        return str;
    }
    /**
     * Add space to string (for value) until it has size.
     *
     * @param val value
     * @param size size
     * @return string expanded to size
     */
    private String addSpaceUntilSize(long val, int size) {
        return addSpaceUntilSize(String.valueOf(val), size);
    }

    /**
     * Utility class and holder of content about names for reporting purposes.
     *
     * @author Lars Johansson
     */
    private class HolderReportName {

        private int nbrEssNameSystemstructure = 0;
        private int nbrEssNameSystemstructureDevicestructure = 0;
        private int nbrEssNameSystemstructureDevicestructureIndex = 0;
        private int nbrEssNameOther = 0;

        public HolderReportName(ResponsePageNameElements nameElements) {
            for (NameElement nameElement : nameElements.getList()) {
                if (!StringUtils.isEmpty(nameElement.getSystemStructure())
                        && StringUtils.isEmpty(nameElement.getDeviceStructure())
                        && StringUtils.isEmpty(nameElement.getIndex())) {
                    nbrEssNameSystemstructure++;
                } else if (!StringUtils.isEmpty(nameElement.getSystemStructure())
                        && !StringUtils.isEmpty(nameElement.getDeviceStructure())
                        && StringUtils.isEmpty(nameElement.getIndex())) {
                    nbrEssNameSystemstructureDevicestructure++;
                } else if (!StringUtils.isEmpty(nameElement.getSystemStructure())
                        && !StringUtils.isEmpty(nameElement.getDeviceStructure())
                        && !StringUtils.isEmpty(nameElement.getIndex())) {
                    nbrEssNameSystemstructureDevicestructureIndex++;
                } else {
                    nbrEssNameOther++;
                }
            }
        }

        public int getNumberEssNameSystemstructure() {
            return nbrEssNameSystemstructure;
        }
        public int getNumberEssNameSystemstructureDevicestructure() {
            return nbrEssNameSystemstructureDevicestructure;
        }
        public int getNumberEssNameSystemstructureDevicestructureIndex() {
            return nbrEssNameSystemstructureDevicestructureIndex;
        }
        public int getNumberEssNameOther() {
            return nbrEssNameOther;
        }

    }

    /**
     * Utility class and holder of content about structures for reporting purposes.
     *
     * @author Lars Johansson
     */
    private class HolderReportStructure {

        private Map<UUID, Long> mapUuidCount = new HashMap<>();

        private int nbrActive = 0;
        private int nbrDeleted = 0;
        private int nbrPending = 0;
        private int nbrCancelled = 0;
        private int nbrRejected = 0;
        private int nbrOther = 0;

        public HolderReportStructure(ResponsePageStructureElements structureElements) {
            for (StructureElement structureElement : structureElements.getList()) {
                addOne(structureElement.getUuid(), mapUuidCount);

                if (Status.APPROVED.equals(structureElement.getStatus())
                        && Boolean.FALSE.equals(structureElement.isDeleted())) {
                    nbrActive++;
                } else if (Status.APPROVED.equals(structureElement.getStatus())
                        && Boolean.TRUE.equals(structureElement.isDeleted())) {
                    nbrDeleted++;
                } else if (Boolean.TRUE.equals(structureElement.isDeleted())) {
                    // ref. V5__Data_transformation_status_deleted.sql
                    // migration script to transform data to not rely on status attribute
                    nbrDeleted++;
                } else if (Status.PENDING.equals(structureElement.getStatus())) {
                    nbrPending++;
                } else if (Status.CANCELLED.equals(structureElement.getStatus())) {
                    nbrCancelled++;
                } else if (Status.REJECTED.equals(structureElement.getStatus())) {
                    nbrRejected++;
                } else {
                    nbrOther++;
                }
            }
        }

        /**
         * Add one to count for key in map.
         *
         * @param key key
         * @param map map
         */
        private void addOne(UUID key, Map<UUID, Long> map) {
            if (key == null) {
                return;
            }

            Long no = map.get(key);
            no = no != null ? no + 1 : 1;
            map.put(key, no);
        }

        public int getSizeMapUuidCount() {
            return mapUuidCount.size();
        }
        public int getNumberActive() {
            return nbrActive;
        }
        public int getNumberDeleted() {
            return nbrDeleted;
        }
        public int getNumberPending() {
            return nbrPending;
        }
        public int getNumberCancelled() {
            return nbrCancelled;
        }
        public int getNumberRejected() {
            return nbrRejected;
        }
        public int getNumberOther() {
            return nbrOther;
        }

    }

}

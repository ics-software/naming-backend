package org.openepics.names.rest.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openepics.names.exception.ServiceException;

import org.openepics.names.exception.security.AuthenticationException;
import org.openepics.names.exception.security.RemoteException;
import org.openepics.names.exception.security.UnauthorizedException;
import org.openepics.names.rest.api.v1.IAuthentication;
import org.openepics.names.rest.beans.security.Login;
import org.openepics.names.rest.beans.security.LoginResponse;
import org.openepics.names.service.LogService;
import org.openepics.names.service.security.AuthenticationService;
import org.openepics.names.service.security.dto.UserDetails;
import org.openepics.names.service.security.util.SecurityTextUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:zoltan.runyo@ess.eu">Zoltan Runyo</a>
 */
@RestController
public class AuthenticationController implements IAuthentication {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationController.class.getName());

    private final AuthenticationService authenticationService;
    private final LogService logService;

    @Autowired
    public AuthenticationController(
            AuthenticationService authenticationService,
            LogService logService) {
        this.authenticationService = authenticationService;
        this.logService = logService;
    }

    @Override
    public ResponseEntity<LoginResponse> login(Login loginInfo) {
        try {
            LoginResponse login =
                    authenticationService.login(loginInfo.getUsername(), loginInfo.getPassword());

            ResponseCookie loginCookie =
                    ResponseCookie.from(SecurityTextUtil.COOKIE_AUTH_HEADER, login.getToken())
                    .httpOnly(true)
                    .secure(true)
                    .path("/")
                    .build();

            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, loginCookie.toString())
                    .body(login);
        } catch (UnauthorizedException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "User doesn't have permission to log in");
            throw e;
        } catch (AuthenticationException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Error while user tried to log in");
            throw e;
        } catch (RemoteException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Remote exception while user tried to log in");
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e, "Error while trying to log in user");
            throw e;
        }
    }

    @Override
    public ResponseEntity<LoginResponse> tokenRenew(UserDetails user) {
        try {
            LoginResponse renew = authenticationService.renewToken(user);

            ResponseCookie renewCookie =
                    ResponseCookie.from(SecurityTextUtil.COOKIE_AUTH_HEADER, renew.getToken())
                    .httpOnly(true)
                    .secure(true)
                    .path("/")
                    .build();

            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, renewCookie.toString())
                    .body(renew);
        } catch (UnauthorizedException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Authorization error while trying to renew token");
            throw e;
        } catch (RemoteException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e, "Remote exception while trying to renew token");
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e, "Error while trying to renew token");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Object> logout(UserDetails user) {
        try {
            authenticationService.logout(user);

            ResponseCookie deleteSpringCookie =
                    ResponseCookie.from(SecurityTextUtil.COOKIE_AUTH_HEADER, null)
                    .path("/")
                    .httpOnly(true)
                    .secure(true)
                    .build();

            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, deleteSpringCookie.toString())
                    .build();
        } catch (RemoteException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (ServiceException e) {
            logService.logServiceException(LOGGER, Level.WARNING, e);
            throw e;
        } catch (Exception e) {
            logService.logException(LOGGER, Level.WARNING, e, "Error while trying to logout user");
            throw new ServiceException(e.getMessage());
        }
    }

}

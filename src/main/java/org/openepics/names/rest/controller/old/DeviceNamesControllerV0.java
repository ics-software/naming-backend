/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller.old;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import io.swagger.v3.oas.annotations.Hidden;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.repository.model.wip.WipName;
import org.openepics.names.repository.wip.IWipDeviceGroupRepository;
import org.openepics.names.repository.wip.IWipDeviceTypeRepository;
import org.openepics.names.repository.wip.IWipDisciplineRepository;
import org.openepics.names.repository.wip.IWipNameRepository;
import org.openepics.names.repository.wip.IWipSubsystemRepository;
import org.openepics.names.repository.wip.IWipSystemGroupRepository;
import org.openepics.names.repository.wip.IWipSystemRepository;
import org.openepics.names.rest.beans.old.DeviceNameElement;
import org.openepics.names.service.LogService;
import org.openepics.names.util.NamingConventionUtil;
import org.openepics.names.util.old.DeviceNameElementUtil;
import org.openepics.names.util.wip.HolderIWipRepositories;
import org.openepics.names.util.wip.HolderWipStructures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

/**
 * This part of REST API provides device name data for Naming application.
 *
 * @author Lars Johansson
 */
@Hidden
@RestController
@RequestMapping("/rest/deviceNames")
@EnableAutoConfiguration
public class DeviceNamesControllerV0 {

    // note
    //     global exception handler available

    /*
       Methods
           read      GET /deviceNames                                - findNames()
           read      GET /deviceNames/search/{name}                  - findNamesSearch(String)
           read      GET /deviceNames/system/{system}                - findNamesBySystem(String)
           read      GET /deviceNames/system/search/{system}         - findNamesBySystemSearch(String)
           read      GET /deviceNames/subsystem/{subsystem}          - findNamesBySubsystem(String)
           read      GET /deviceNames/subsystem/search/{subsystem}   - findNamesBySubsystemSearch(String)
           read      GET /deviceNames/discipline/{discipline}        - findNamesByDiscipline(String)
           read      GET /deviceNames/discipline/search/{discipline} - findNamesByDisciplineSearch(String)
           read      GET /deviceNames/devicetype/{devicetype}        - findNamesByDeviceType(String)
           read      GET /deviceNames/devicetype/search/{devicetype} - findNamesByDeviceTypeSearch(String)
           read      GET /deviceNames/{uuid}                         - findName(String) (uuid or name)
     */

    private static final Logger LOGGER = Logger.getLogger(DeviceNamesControllerV0.class.getName());

    private final LogService logService;
    private final HolderIWipRepositories holderIWipRepositories;

    @Autowired
    public DeviceNamesControllerV0(
            LogService logService,
            IWipNameRepository iWipNameRepository,
            IWipSystemGroupRepository iWipSystemGroupRepository,
            IWipSystemRepository iWipSystemRepository,
            IWipSubsystemRepository iWipSubsystemRepository,
            IWipDisciplineRepository iWipDisciplineRepository,
            IWipDeviceGroupRepository iWipDeviceGroupRepository,
            IWipDeviceTypeRepository iWipDeviceTypeRepository) {

        this.logService = logService;
        this.holderIWipRepositories = new HolderIWipRepositories(
                iWipNameRepository,
                iWipSystemGroupRepository,
                iWipSystemRepository,
                iWipSubsystemRepository,
                iWipDisciplineRepository,
                iWipDeviceGroupRepository,
                iWipDeviceTypeRepository);
    }

    /**
     * Find valid names.
     *
     * @return all valid names
     */
    @GetMapping
    public List<DeviceNameElement> findNames() {
        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName name : names) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
        }

        LOGGER.log(Level.FINE, "findNames, deviceNameElement    s.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by name search.
     * <br/>
     * Note
     * <ul>
     * <li>name (search, case sensitive, regex)
     * <li>search done for all parts of name
     * </ul>
     *
     * @param name name to search for
     * @return a list of names
     */
    @GetMapping("/search/{name}")
    public List<DeviceNameElement> findNamesSearch(@PathVariable("name") String name) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(name);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return deviceNameElements;
        }

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName namee : names) {
            if (pattern.matcher(namee.getConventionName()).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(namee, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesSearch, name:                    {0}", name);
        LOGGER.log(Level.FINE, "findNamesSearch, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by system.
     * Note system (exact match, case sensitive).
     *
     * @param system system to look for
     * @return a list of names
     */
    @GetMapping("/system/{system}")
    public List<DeviceNameElement> findNamesBySystem(@PathVariable("system") String system) {
        // note
        //     exact match
        //     case sensitive

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        List<WipName> namesSystemGroup            = holderIWipRepositories.nameRepository().findLatestBySystemGroupMnemonic(system);
        for (WipName name : namesSystemGroup) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
        }
        List<WipName> namesSystem                 = holderIWipRepositories.nameRepository().findLatestBySystemMnemonic(system);
        List<WipName> namesSystemThroughSubsystem = holderIWipRepositories.nameRepository().findLatestBySystemMnemonicThroughSubsystem(system);
        for (WipName name : namesSystem) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
        }
        for (WipName name : namesSystemThroughSubsystem) {
            deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
        }

        LOGGER.log(Level.FINE, "findNamesBySystem, system:                  {0}", system);
        LOGGER.log(Level.FINE, "findNamesBySystem, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by system search.
     * Note system (search, case sensitive, regex).
     *
     * @param system system to search for
     * @return a list of names
     */
    @GetMapping("/system/search/{system}")
    public List<DeviceNameElement> findNamesBySystemSearch(@PathVariable("system") String system) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(system);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return deviceNameElements;
        }

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName name : names) {
            String sub = NamingConventionUtil.extractSystem(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesSearch, system:                  {0}", system);
        LOGGER.log(Level.FINE, "findNamesSearch, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by subsystem.
     * Note subsystem (exact match, case sensitive).
     *
     * @param subsystem subsystem to look for
     * @return a list of names
     */
    @GetMapping("/subsystem/{subsystem}")
    public List<DeviceNameElement> findNamesBySubsystem(@PathVariable("subsystem") String subsystem) {
        // note
        //     exact match
        //     case sensitive

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestBySubsystemMnemonic(subsystem);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // holder of valid content for system and device structures
            HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

            for (WipName name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesBySubsystem, subsystem:               {0}", subsystem);
        LOGGER.log(Level.FINE, "findNamesBySubsystem, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by subsystem search.
     * Note subsystem (search, case sensitive, regex).
     *
     * @param subsystem subsystem to search for
     * @return a list of names
     */
    @GetMapping("/subsystem/search/{subsystem}")
    public List<DeviceNameElement> findNamesBySubsystemSearch(@PathVariable("subsystem") String subsystem) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(subsystem);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return deviceNameElements;
        }

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName name : names) {
            String sub = NamingConventionUtil.extractSubsystem(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesBySubsystemSearch, subsystem:               {0}", subsystem);
        LOGGER.log(Level.FINE, "findNamesBySubsystemSearch, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by discipline.
     * Note discipline (exact match, case sensitive).
     *
     * @param discipline discipline to look for
     * @return a list of names
     */
    @GetMapping("/discipline/{discipline}")
    public List<DeviceNameElement> findNamesByDiscipline(@PathVariable("discipline") String discipline) {
        // note
        //     exact match
        //     case sensitive

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestByDisciplineMnemonicThroughDeviceType(discipline);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // holder of valid content for system and device structures
            HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

            for (WipName name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesByDiscipline, discipline:              {0}", discipline);
        LOGGER.log(Level.FINE, "findNamesByDiscipline, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by discipline search.
     * Note discipline (search, case sensitive, regex).
     *
     * @param discipline discipline to search for
     * @return a list of names
     */
    @GetMapping("/discipline/search/{discipline}")
    public List<DeviceNameElement> findNamesByDisciplineSearch(@PathVariable("discipline") String discipline) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(discipline);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return deviceNameElements;
        }

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName name : names) {
            String sub = NamingConventionUtil.extractDiscipline(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesByDisciplineSearch, discipline:              {0}", discipline);
        LOGGER.log(Level.FINE, "findNamesByDisciplineSearch, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by device type.
     * Note device type (exact match, case sensitive).
     *
     * @param deviceType device type to look for
     * @return a list of names
     */
    @GetMapping("/devicetype/{devicetype}")
    public List<DeviceNameElement> findNamesByDeviceType(@PathVariable("devicetype") String deviceType) {
        // note
        //     exact match
        //     case sensitive

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestByDeviceTypeMnemonic(deviceType);

        // create collection with known initial capacity
        final List<DeviceNameElement> deviceNameElements = new ArrayList<>(names.size());

        if (!names.isEmpty()) {
            // holder of valid content for system and device structures
            HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

            for (WipName name : names) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesByDeviceType, deviceType:              {0}", deviceType);
        LOGGER.log(Level.FINE, "findNamesByDeviceType, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find names by device type search.
     * Note device type (search, case sensitive, regex).
     *
     * @param deviceType device type to search for
     * @return a list of names
     */
    @GetMapping("/devicetype/search/{devicetype}")
    public List<DeviceNameElement> findNamesByDeviceTypeSearch(@PathVariable("devicetype") String deviceType) {
        // note
        //     search
        //     case sensitive
        //     regex

        final List<DeviceNameElement> deviceNameElements = Lists.newArrayList();
        Pattern pattern = null;
        try {
            pattern = Pattern.compile(deviceType);
        } catch (PatternSyntaxException e) {
            logService.logException(LOGGER, Level.WARNING, e);
            return deviceNameElements;
        }

        List<WipName> names = holderIWipRepositories.nameRepository().findLatestNotDeleted();

        // holder of valid content for system and device structures
        HolderWipStructures holderStructures = new HolderWipStructures(holderIWipRepositories);

        for (WipName name : names) {
            String sub = NamingConventionUtil.extractDeviceType(name.getConventionName());
            if (!StringUtils.isEmpty(sub) && pattern.matcher(sub).find()) {
                deviceNameElements.add(DeviceNameElementUtil.getDeviceNameElement(name, holderStructures));
            }
        }

        LOGGER.log(Level.FINE, "findNamesByDeviceTypeSearch, deviceType:              {0}", deviceType);
        LOGGER.log(Level.FINE, "findNamesByDeviceTypeSearch, deviceNameElements.size: {0}", deviceNameElements.size());

        return deviceNameElements;
    }

    /**
     * Find name by uuid or name (exact match, case sensitive).
     *
     * @param uuid string uuid or device name
     * @return name (most recent)
     */
    @GetMapping("/{uuid}")
    public DeviceNameElement findName(@PathVariable("uuid") String uuid) {
        // note
        //     exact match
        //     case sensitive

        WipName name = null;
        try {
            UUID.fromString(uuid);
            name = holderIWipRepositories.nameRepository().findLatestByUuid(uuid);
        } catch (IllegalArgumentException e) {
            name = holderIWipRepositories.nameRepository().findLatestByConventionName(uuid);
        }
        DeviceNameElement deviceNameElement = DeviceNameElementUtil.getDeviceNameElement(name, holderIWipRepositories);

        LOGGER.log(Level.FINE, "findName, uuid:              {0}", uuid);
        LOGGER.log(Level.FINE, "findName, deviceNameElement: {0}", deviceNameElement);

        return deviceNameElement;
    }

}

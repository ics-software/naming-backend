/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.controller;

import java.text.DateFormat;
import java.util.Date;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * This part of REST API provides healthcheck for Naming application.
 *
 * @author Lars Johansson
 */
@Tag(name        = "Healthcheck",
     description = "perform healthcheck for Naming application")
@RestController
@RequestMapping("/healthcheck")
@EnableAutoConfiguration
public class HealthcheckController {

    // methods
    //     read      GET /healthcheck - readHealthcheck()

    /**
     * Perform healthcheck for Naming application.
     * To be used mainly for checking HTTP response code, in particular HTTP STATUS OK - 200.
     *
     * @return server timestamp
     */
    @Operation(
            summary     = "Perform healthcheck for Naming application",
            description = """
                          Perform healthcheck for Naming application.
                          To be used mainly for checking HTTP response code, in particular HTTP STATUS OK - 200.
                          """)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Method completed OK. Return healthcheck as server timestamp.",
                    content = @Content(mediaType = "text/plain", schema = @Schema(implementation = String.class)))
    })
    @GetMapping(
            produces = {"text/plain"})
    public String healthcheck() {
        // return healthcheck as server timestamp
        //     datetime - dateStyle, timeStyle - full

        return DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL).format(new Date());
    }

}

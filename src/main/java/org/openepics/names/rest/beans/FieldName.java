/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This enum represents fields for name in beans and for communication.
 *
 * @author Lars Johansson
 */
@Schema(description = "A collection of values for fields for a name that may used in client query "
        + "for information to server.")
public enum FieldName {

    @Schema(description = "Identity of the name. Value is set server-side.")
    UUID,
    @Schema(description = "Name (verbose) of the name.")
    NAME,
    @Schema(description = "Mnemonic path for for the system structure.")
    SYSTEMSTRUCTURE,
    @Schema(description = "Mnemonic path for for the device structure.")
    DEVICESTRUCTURE,
    @Schema(description = "Index of the name.")
    INDEX,
    @Schema(description = "Description (verbose) of the name.")
    DESCRIPTION,
    @Schema(description = "Date and time when the name was created.")
    WHEN;

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Objects;
import java.util.UUID;

import org.openepics.names.rest.beans.Type;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that modify structures.
 *
 * @author Lars Johansson
 */
public class StructureElementCommandCreate {

    @Schema(description = "Type of the structure entry. Valid values - SYSTEMGROUP, SYSTEM, SUBSYSTEM, DISCIPLINE, DEVICEGROUP, DEVICETYPE.")
    private Type type;
    @Schema(description = "Identity (uuid) for the structure entry parent (if the structure entry has a parent).")
    private UUID parent;
    @Schema(description = "Mnemonic of the structure entry.")
    private String mnemonic;
    @Schema(description = "Ordering of the structure entry.")
    private Integer ordering;
    @Schema(description = "Description (verbose) of the structure entry.")
    private String description;

    /**
     * Public constructor.
     */
    public StructureElementCommandCreate() {
    }

    /**
     * Public constructor.
     *
     * @param type type
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param ordering ordering
     * @param description description
     */
    public StructureElementCommandCreate(
            Type type, UUID parent,
            String mnemonic, Integer ordering, String description) {
        super();
        this.type = type;
        this.parent = parent;
        this.mnemonic = mnemonic;
        this.ordering = ordering;
        this.description = description;
    }

    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public UUID getParent() {
        return parent;
    }
    public void setParent(UUID parent) {
        this.parent = parent;
    }
    public String getMnemonic() {
        return mnemonic;
    }
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    public Integer getOrdering() {
        return ordering;
    }
    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((StructureElementCommandCreate) obj);
    }

    public boolean equals(StructureElementCommandCreate other) {
        if (other == null)
            return false;

        if (getType() == null) {
            if (other.getType() != null)
                return false;
        } else if (!getType().equals(other.getType()))
            return false;
        if (getParent() == null) {
            if (other.getParent() != null)
                return false;
        } else if (!getParent().equals(other.getParent()))
            return false;
        if (getMnemonic() == null) {
            if (other.getMnemonic() != null)
                return false;
        } else if (!getMnemonic().equals(other.getMnemonic()))
            return false;
        if (getOrdering() == null) {
            if (other.getOrdering() != null)
                return false;
        } else if (!getOrdering().equals(other.getOrdering()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getType(), getParent(), getMnemonic(), getDescription());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"type\": "                    + getType());
        sb.append(", \"parent\": "                + getParent());
        sb.append(", \"mnemonic\": "              + getMnemonic());
        sb.append(", \"ordering\": "              + getOrdering());
        sb.append(", \"description\": "           + getDescription());
        sb.append("}");
        return sb.toString();
    }

}

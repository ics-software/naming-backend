/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that read structures.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a structure entry.
                      It is used to show information for a structure entry.
                      """)
public class StructureElement extends StructureElementCommand {

    @Schema(description = "Mnemonic path of the structure entry.")
    private String mnemonicPath;
    @Schema(description = "Level of the structure entry.")
    private Integer level;
    @Schema(description = "Status of the structure entry.")
    private Status status;
    @Schema(description = "If the structure entry is deleted.")
    private Boolean deleted;
    @Schema(description = "Date and time when the structure entry was created.")
    private Date when;
    @Schema(description = "Name (user) of who created the structure entry.")
    private String who;
    @Schema(description = "Comment of the structure entry.")
    private String comment;

    /**
     * Public constructor.
     */
    public StructureElement() {
        super();
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param type type
     * @param parent parent
     * @param mnemonic mnemonic
     * @param ordering ordering
     * @param description description
     * @param mnemonicPath mnemonic path
     * @param level level
     * @param status status
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     */
    public StructureElement(
            UUID uuid, Type type, UUID parent,
            String mnemonic, Integer ordering, String description,
            String mnemonicPath, Integer level,
            Status status, Boolean deleted,
            Date when, String who, String comment) {
        super(uuid, type, parent, mnemonic, ordering, description);
        this.mnemonicPath = mnemonicPath;
        this.level = level;
        this.status = status;
        this.deleted = deleted;
        this.when = when;
        this.who = who;
        this.comment = comment;
    }

    public String getMnemonicPath() {
        return mnemonicPath;
    }
    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }
    public Integer getLevel() {
        return level;
    }
    public void setLevel(Integer level) {
        this.level = level;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getWhen() {
        return when;
    }
    public void setWhen(Date when) {
        this.when = when;
    }
    public String getWho() {
        return who;
    }
    public void setWho(String who) {
        this.who = who;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((StructureElement) obj);
    }

    public boolean equals(StructureElement other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getMnemonicPath() == null) {
            if (other.getMnemonicPath() != null)
                return false;
        } else if (!getMnemonicPath().equals(other.getMnemonicPath()))
            return false;
        if (getLevel() == null) {
            if (other.getLevel() != null)
                return false;
        } else if (!getLevel().equals(other.getLevel()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getWhen() == null) {
            if (other.getWhen() != null)
                return false;
        } else if (!getWhen().equals(other.getWhen()))
            return false;
        if (getWho() == null) {
            if (other.getWho() != null)
                return false;
        } else if (!getWho().equals(other.getWho()))
            return false;
        if (getComment() == null) {
            if (other.getComment() != null)
                return false;
        } else if (!getComment().equals(other.getComment()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid(), getWhen(), getComment());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "           + getUuid());
        sb.append(", \"type\": "         + getType());
        sb.append(", \"parent\": "       + getParent());
        sb.append(", \"mnemonic\": "     + getMnemonic());
        sb.append(", \"mnemonicPath\": " + getMnemonicPath());
        sb.append(", \"ordering\": "     + getOrdering());
        sb.append(", \"level\": "        + getLevel());
        sb.append(", \"description\": "  + getDescription());
        sb.append(", \"status\": "       + getStatus());
        sb.append(", \"deleted\": "      + isDeleted());
        sb.append(", \"when\": "         + getWhen());
        sb.append(", \"who\": "          + getWho());
        sb.append(", \"comment\": "      + getComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "           + getUuid());
        sb.append(", \"type\": "         + getType());
        sb.append(", \"mnemonic\": "     + getMnemonic());
        sb.append(", \"status\": "       + getStatus());
        sb.append(", \"deleted\": "      + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

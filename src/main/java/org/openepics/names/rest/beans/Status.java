/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This enum represents status for names and structures data in beans and for communication.
 *
 * @author Lars Johansson
 */
@Schema(description = "A collection of values for status of a name and structure entry. "
        + "It is used to show where in its lifecycle that a name and structure entry is located.")
public enum Status {

    @Schema(description = "(Proposal for) Name or structure entry has been approved by administrator.")
    APPROVED,
    @Schema(description = "Name or structure entry that is no longer in use with data considered history.")
    ARCHIVED,
    @Schema(description = "(Proposal for) Name or structure entry has been cancelled by user.")
    CANCELLED,
    @Schema(description = "(Proposal for) Name or structure entry has been entered by user and is pending for administrator to approve or reject.")
    PENDING,
    @Schema(description = "(Proposal for) Name or structure entry has been rejected by administrator.")
    REJECTED;

}

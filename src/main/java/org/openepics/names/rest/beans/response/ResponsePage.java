/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.response;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a response from server to client.
                      It is used to show information related to an operation for a name and structure entry.
                      Values are set server-side.
                      """)
public abstract class ResponsePage extends Response {

    // note
    //     offset - page
    //     limit  - page size

    @Schema(description = "Total count of items that may be returned to client from server.")
    private Long totalCount;
    @Schema(description = "Count of items that is returned to client from server.")
    private Integer listSize;
    @Schema(description = "Page number for response to client from server.")
    private Integer page;
    @Schema(description = "Page size for response to client from server.")
    private Integer pageSize;

    /**
     * Constructor.
     */
    protected ResponsePage() {
        super("", "", "");
    }

    /**
     * Constructor.
     *
     * @param message response message
     * @param details response details
     * @param field response field
     */
    protected ResponsePage(String message, String details, String field) {
        super(message, details, field);
    }

    /**
     * Return total count.
     *
     * @return total count
     */
    public Long getTotalCount() {
        return totalCount;
    }

    /**
     * Set total count.
     *
     * @param totalCount total count
     */
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * Return list size.
     *
     * @return list size
     */
    public Integer getListSize() {
        return listSize;
    }

    /**
     * Set list size.
     *
     * @param listSize list size
     */
    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    /**
     * Return page.
     *
     * @return page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * Set page.
     *
     * @param page page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * Return page size.
     *
     * @return page size
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Set page size.
     *
     * @param pageSize page size
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}

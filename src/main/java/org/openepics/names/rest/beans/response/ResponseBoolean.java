/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.response;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a response from server to client.
                      It is used to show information related to an operation for a name and structure entry.
                      Values are set server-side.
                      """)
public class ResponseBoolean extends Response {

    @Schema(description = "Outcome of the response.")
    private Boolean value = null;

    /**
     * Constructor.
     */
    public ResponseBoolean() {
        super("", "", "");
    }

    /**
     * Constructor.
     *
     * @param value response value
     */
    public ResponseBoolean(Boolean value) {
        this();
        this.value = value;
    }

    /**
     * Constructor.
     *
     * @param value response value
     * @param message response message
     */
    public ResponseBoolean(Boolean value, String message) {
        super(message, "", "");
        this.value = value;
    }

    /**
     * Constructor.
     *
     * @param value response value
     * @param message response message
     * @param details response details
     * @param field response field
     */
    public ResponseBoolean(Boolean value, String message, String details, String field) {
        super(message, details, field);
        this.value = value;
    }

    /**
     * Return response value.
     *
     * @return response value
     */
    public Boolean getValue() {
        return this.value;
    }

}

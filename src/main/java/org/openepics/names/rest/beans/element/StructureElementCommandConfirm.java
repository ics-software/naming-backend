/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Objects;
import java.util.UUID;

import org.openepics.names.rest.beans.Type;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that modify structures.
 *
 * @author Lars Johansson
 */
public class StructureElementCommandConfirm {

    @Schema(description = "Identity (uuid) of the structure entry. Value is created server-side.")
    private UUID uuid;
    @Schema(description = "Type of the structure entry. Valid values - SYSTEMGROUP, SYSTEM, SUBSYSTEM, DISCIPLINE, DEVICEGROUP, DEVICETYPE.")
    private Type type;

    /**
     * Public constructor.
     */
    public StructureElementCommandConfirm() {
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param type type
     */
    public StructureElementCommandConfirm(
            UUID uuid, Type type) {
        super();
        this.uuid = uuid;
        this.type = type;
    }

    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    public Type getType() {
        return type;
    }
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((StructureElementCommandConfirm) obj);
    }

    public boolean equals(StructureElementCommandConfirm other) {
        if (other == null)
            return false;

        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getType() == null) {
            if (other.getType() != null)
                return false;
        } else if (!getType().equals(other.getType()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "                    + getUuid());
        sb.append(", \"type\": "                  + getType());
        sb.append("}");
        return sb.toString();
    }

}

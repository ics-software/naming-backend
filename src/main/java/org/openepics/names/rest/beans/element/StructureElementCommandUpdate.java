/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Objects;
import java.util.UUID;

import org.openepics.names.rest.beans.Type;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that modify structures.
 *
 * @author Lars Johansson
 */
public class StructureElementCommandUpdate extends StructureElementCommandCreate {

    @Schema(description = "Identity (uuid) of the structure entry. Value is created server-side.")
    private UUID uuid;

    /**
     * Public constructor.
     */
    public StructureElementCommandUpdate() {
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param type type
     * @param parent parent uuid
     * @param mnemonic mnemonic
     * @param ordering ordering
     * @param description description
     */
    public StructureElementCommandUpdate(
            UUID uuid, Type type, UUID parent,
            String mnemonic, Integer ordering, String description) {
        super(type, parent, mnemonic, ordering, description);
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((StructureElementCommandUpdate) obj);
    }

    public boolean equals(StructureElementCommandUpdate other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "                    + getUuid());
        sb.append(", \"type\": "                  + getType());
        sb.append(", \"parent\": "                + getParent());
        sb.append(", \"mnemonic\": "              + getMnemonic());
        sb.append(", \"ordering\": "              + getOrdering());
        sb.append(", \"description\": "           + getDescription());
        sb.append("}");
        return sb.toString();
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import org.openepics.names.rest.beans.Status;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that read names.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a name.
                      It is used to show information for a name.
                      """)
public class NameElement extends NameElementCommand {

    @Schema(description = "Mnemonic path for for the system structure.")
    private String systemStructure;
    @Schema(description = "Mnemonic path for for the device structure.")
    private String deviceStructure;
    @Schema(description = "Name (verbose) of the name entry.")
    private String name;
    @Schema(description = "Status of the name entry.")
    private Status status;
    @Schema(description = "If the name entry is deleted.")
    private Boolean deleted;
    @Schema(description = "Date and time when the name entry was created.")
    private Date when;
    @Schema(description = "Name (user) of who created the name entry.")
    private String who;
    @Schema(description = "Comment of the name entry.")
    private String comment;

    /**
     * Public constructor.
     */
    public NameElement() {
        super();
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param parentSystemStructure parent system structure uuid
     * @param parentDeviceStructure parent device structure uuid
     * @param index index
     * @param description description
     * @param systemStructure system structure mnemonic path
     * @param deviceStructure device structure mnemonic path
     * @param name name
     * @param status status
     * @param deleted deleted
     * @param when when
     * @param who who
     * @param comment comment
     */
    public NameElement(
            UUID uuid, UUID parentSystemStructure, UUID parentDeviceStructure,
            String index, String description,
            String systemStructure, String deviceStructure, String name,
            Status status, Boolean deleted,
            Date when, String who, String comment) {
        super(uuid, parentSystemStructure, parentDeviceStructure, index, description);
        this.systemStructure = systemStructure;
        this.deviceStructure = deviceStructure;
        this.name = name;
        this.status = status;
        this.deleted = deleted;
        this.when = when;
        this.who = who;
        this.comment = comment;
    }

    public String getSystemStructure() {
        return systemStructure;
    }
    public void setSystemStructure(String systemStructure) {
        this.systemStructure = systemStructure;
    }
    public String getDeviceStructure() {
        return deviceStructure;
    }
    public void setDeviceStructure(String deviceStructure) {
        this.deviceStructure = deviceStructure;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public Boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    public Date getWhen() {
        return when;
    }
    public void setWhen(Date when) {
        this.when = when;
    }
    public String getWho() {
        return who;
    }
    public void setWho(String who) {
        this.who = who;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((NameElement) obj);
    }

    public boolean equals(NameElement other) {
        if (other == null)
            return false;

        if (!super.equals(other)) {
            return false;
        }

        if (getSystemStructure() == null) {
            if (other.getSystemStructure() != null)
                return false;
        } else if (!getSystemStructure().equals(other.getSystemStructure()))
            return false;
        if (getDeviceStructure() == null) {
            if (other.getDeviceStructure() != null)
                return false;
        } else if (!getDeviceStructure().equals(other.getDeviceStructure()))
            return false;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (getStatus() == null) {
            if (other.getStatus() != null)
                return false;
        } else if (!getStatus().equals(other.getStatus()))
            return false;
        if (isDeleted() == null) {
            if (other.isDeleted() != null)
                return false;
        } else if (!isDeleted().equals(other.isDeleted()))
            return false;
        if (getWhen() == null) {
            if (other.getWhen() != null)
                return false;
        } else if (!getWhen().equals(other.getWhen()))
            return false;
        if (getWho() == null) {
            if (other.getWho() != null)
                return false;
        } else if (!getWho().equals(other.getWho()))
            return false;
        if (getComment() == null) {
            if (other.getComment() != null)
                return false;
        } else if (!getComment().equals(other.getComment()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUuid(), getWhen(), getComment());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "                    + getUuid());
        sb.append(", \"parentSystemStructure\": " + getParentSystemStructure());
        sb.append(", \"parentDeviceStructure\": " + getParentDeviceStructure());
        sb.append(", \"systemStructure\": "       + getSystemStructure());
        sb.append(", \"deviceStructure\": "       + getDeviceStructure());
        sb.append(", \"index\": "                 + getIndex());
        sb.append(", \"description\": "           + getDescription());
        sb.append(", \"name\": "                  + getName());
        sb.append(", \"status\": "                + getStatus());
        sb.append(", \"deleted\": "               + isDeleted());
        sb.append(", \"when\": "                  + getWhen());
        sb.append(", \"who\": "                   + getWho());
        sb.append(", \"comment\": "               + getComment());
        sb.append("}");
        return sb.toString();
    }

    public String toStringSimple() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "                    + getUuid());
        sb.append(", \"name\": "                  + getName());
        sb.append(", \"status\": "                + getStatus());
        sb.append(", \"deleted\": "               + isDeleted());
        sb.append("}");
        return sb.toString();
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.response;

import java.util.List;

import org.openepics.names.rest.beans.element.StructureElement;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a response from server to client.
                      It is used to show information related to an operation for a name and structure entry.
                      Values are set server-side.
                      """)
public class ResponsePageStructureElements extends ResponsePage {

    @Schema(description = "Items (structure entries) that are returned to client from server.")
    private List<StructureElement> list = Lists.newArrayList();

    /**
     * Constructor.
     */
    public ResponsePageStructureElements() {
        super("", "", "");
        super.setTotalCount(0L);
        super.setListSize(0);
        super.setPage(0);
        super.setPageSize(0);
    }

    /**
     * Constructor.
     *
     * @param list list of structure elements
     */
    public ResponsePageStructureElements(List<StructureElement> list) {
        this();
        this.list = list;
    }

    /**
     * Constructor.
     *
     * @param list list of structure elements
     * @param totalCount total count
     * @param listSize list size
     * @param page page
     * @param pageSize page size
     */
    public ResponsePageStructureElements(List<StructureElement> list, Long totalCount, Integer listSize, Integer page, Integer pageSize) {
        this(list);
        super.setTotalCount(totalCount);
        super.setListSize(listSize);
        super.setPage(page);
        super.setPageSize(pageSize);
    }

    /**
     * Return list of structure elements.
     *
     * @return list of structure elements
     */
    public List<StructureElement> getList() {
        return this.list;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"message\": "             + getMessage());
        sb.append(", \"details\": "           + getDetails());
        sb.append(", \"field\": "             + getField());
        sb.append(", \"totalCount\": "        + getTotalCount());
        sb.append(", \"listSize\": "          + getListSize());
        sb.append(", \"page\": "              + getPage());
        sb.append(", \"pageSize\": "          + getPageSize());
        sb.append("}");
        return sb.toString();
    }

}

/*
 * Copyright (c) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.old;

import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data transfer object representing name parts for JSON and XML serialization.
 * Intended usage is for System Structure and Device Structure.
 *
 * @author Lars Johansson
 */
@XmlRootElement
public class PartElement {

    private String type;
    private UUID uuid;
    private String name;
    private String namePath;
    private String mnemonic;
    private String mnemonicPath;
    private String level;
    private String description;
    private String status;

    /**
     * Constructor.
     */
    public PartElement() {
        super();
    }

    /**
     * Constructor.
     *
     * @param type the type
     * @param uuid the uuid
     * @param name the name
     * @param namePath the name path
     * @param mnemonic the mnemonic
     * @param mnemonicPath the mnemonic path
     * @param level the level
     * @param description the description
     * @param status the status
     */
    public PartElement(String type, UUID uuid,
            String name, String namePath, String mnemonic, String mnemonicPath, String level,
            String description, String status) {
        super();
        this.type = type;
        this.uuid = uuid;
        this.name = name;
        this.namePath = namePath;
        this.mnemonic = mnemonic;
        this.mnemonicPath = mnemonicPath;
        this.level = level;
        this.description = description;
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the uuid
     */
    public UUID getUuid() {
        return uuid;
    }
    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the name path
     */
    public String getNamePath() {
        return namePath;
    }
    /**
     * @param mnemonicPath the name path to set
     */
    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }
    /**
     * @return the mnemonic
     */
    public String getMnemonic() {
        return mnemonic;
    }
    /**
     * @param mnemonic the mnemonic to set
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
    /**
     * @return the mnemonic path
     */
    public String getMnemonicPath() {
        return mnemonicPath;
    }
    /**
     * @param mnemonicPath the mnemonic path to set
     */
    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }
    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }
    /**
     * @param level the level to set
     */
    public void setLevel(String level) {
        this.level = level;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import java.util.Objects;
import java.util.UUID;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Bean (data transfer object) for communication and (json) serialization.
 * Intended for operations that modify names.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a command for a name from client to server.
                      It is used to create, update, delete and validate a name.
                      """)
public class NameElementCommand {

    @Schema(description = "Identity (uuid) of the name entry. Value is created server-side.")
    private UUID uuid;
    @Schema(description = "Identity (uuid) for the system structure parent.")
    private UUID parentSystemStructure;
    @Schema(description = "Identity (uuid) for the device structure parent (if the name entry refers to device structure).")
    private UUID parentDeviceStructure;
    @Schema(description = "Index (instance) of the name entry (if the name entry refers to device structure).")
    private String index;
    @Schema(description = "Description (verbose) of the name entry.")
    private String description;

    /**
     * Public constructor.
     */
    public NameElementCommand() {
    }

    /**
     * Public constructor.
     *
     * @param uuid uuid
     * @param parentSystemStructure parent system structure uuid
     * @param parentDeviceStructure parent device structure uuid
     * @param index index
     * @param description description
     */
    public NameElementCommand(
            UUID uuid, UUID parentSystemStructure, UUID parentDeviceStructure,
            String index, String description) {
        super();
        this.uuid = uuid;
        this.parentSystemStructure = parentSystemStructure;
        this.parentDeviceStructure = parentDeviceStructure;
        this.index = index;
        this.description = description;
    }

    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    public UUID getParentSystemStructure() {
        return parentSystemStructure;
    }
    public void setParentSystemStructure(UUID parentSystemStructure) {
        this.parentSystemStructure = parentSystemStructure;
    }
    public UUID getParentDeviceStructure() {
        return parentDeviceStructure;
    }
    public void setParentDeviceStructure(UUID parentDeviceStructure) {
        this.parentDeviceStructure = parentDeviceStructure;
    }
    public String getIndex() {
        return index;
    }
    public void setIndex(String index) {
        this.index = index;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equals ((NameElementCommand) obj);
    }

    public boolean equals(NameElementCommand other) {
        if (other == null)
            return false;

        if (getUuid() == null) {
            if (other.getUuid() != null)
                return false;
        } else if (!getUuid().equals(other.getUuid()))
            return false;
        if (getParentSystemStructure() == null) {
            if (other.getParentSystemStructure() != null)
                return false;
        } else if (!getParentSystemStructure().equals(other.getParentSystemStructure()))
            return false;
        if (getParentDeviceStructure() == null) {
            if (other.getParentDeviceStructure() != null)
                return false;
        } else if (!getParentDeviceStructure().equals(other.getParentDeviceStructure()))
            return false;
        if (getIndex() == null) {
            if (other.getIndex() != null)
                return false;
        } else if (!getIndex().equals(other.getIndex()))
            return false;
        if (getDescription() == null) {
            if (other.getDescription() != null)
                return false;
        } else if (!getDescription().equals(other.getDescription()))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getUuid(), getParentSystemStructure(), getParentDeviceStructure(), getIndex(),
                getDescription());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"uuid\": "                    + getUuid());
        sb.append(", \"parentSystemStructure\": " + getParentSystemStructure());
        sb.append(", \"parentDeviceStructure\": " + getParentDeviceStructure());
        sb.append(", \"index\": "                 + getIndex());
        sb.append(", \"description\": "           + getDescription());
        sb.append("}");
        return sb.toString();
    }

}

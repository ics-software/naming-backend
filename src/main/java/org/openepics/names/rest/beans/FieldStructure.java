/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This enum represents fields for structures in beans and for communication.
 *
 * @author Lars Johansson
 */
@Schema(description = "A collection of values for fields for a structure entry that may used in client query "
        + "for information to server.")
public enum FieldStructure {

    @Schema(description = "Identity of the structure entry. Value is set server-side.")
    UUID,
    @Schema(description = "Identity for the structure entry parent (if the structure entry has a parent).")
    PARENT,
    @Schema(description = "Mnemonic of the structure entry.")
    MNEMONIC,
    @Schema(description = "Mnemonic path for for the structure entry.")
    MNEMONICPATH,
    @Schema(description = "Ordering of the structure entry.")
    ORDERING,
    @Schema(description = "Description (verbose) of the structure entry.")
    DESCRIPTION,
    @Schema(description = "Date and time when the structure entry was created.")
    WHEN;

}

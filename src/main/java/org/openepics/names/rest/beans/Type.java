/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This enum represents types for structures data in beans and for communication.
 *
 * @author Lars Johansson
 */
@Schema(description = "A collection of values for type of structure entry. "
        + "It is used to show what kind of entry and which level in parent / child hierarchy "
        + "for system structure and device structure. ")
public enum Type {

    @Schema(description = "System structure 1st level.")
    SYSTEMGROUP,
    @Schema(description = "System structure 2nd level.")
    SYSTEM,
    @Schema(description = "System structure 3rd level.")
    SUBSYSTEM,
    @Schema(description = "Device structure 1st level.")
    DISCIPLINE,
    @Schema(description = "Device structure 2nd level.")
    DEVICEGROUP,
    @Schema(description = "Device structure 3rd level.")
    DEVICETYPE;

}

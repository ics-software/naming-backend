/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.response;

import org.springframework.http.HttpHeaders;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a response from server to client.
                      It is used to show information related to an operation for a name and structure entry.
                      Values are set server-side.
                      """)
public class Response {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APP_JSON     = "application/json";

    private static HttpHeaders HEADER_JSON;

    @Schema(description = "Message of the response.")
    private String message = null;
    @Schema(description = "Details for the message of the response.")
    private String details = null;
    @Schema(description = "Field of the response for the object that was sent to server.")
    private String field = null;

    /**
     * Constructor.
     *
     * @param message response message
     */
    public Response() {
        this.message = "";
        this.details = "";
        this.field = "";
    }

    /**
     * Constructor.
     *
     * @param message response message
     * @param details response details
     * @param field response field
     */
    public Response(String message, String details, String field) {
        this.message = message;
        this.details = details;
        this.field = field;
    }

    /**
     * Return http header for content type json.
     *
     * @return http header
     */
    public static HttpHeaders getHeaderJson() {
        if (HEADER_JSON == null) {
            HEADER_JSON = new HttpHeaders();
            HEADER_JSON.add(Response.CONTENT_TYPE, Response.APP_JSON);
        }
        return HEADER_JSON;
    }

    /**
     * Return response message.
     *
     * @return response message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Set response message.
     *
     * @param message response message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Return response details.
     *
     * @return response details
     */
    public String getDetails() {
        return this.details;
    }

    /**
     * Set response details.
     *
     * @param details response details
     */
    public void setDetails(String details) {
        this.details = details;
    }

    /**
     * Return response field.
     *
     * @return response field
     */
    public String getField() {
        return this.field;
    }

    /**
     * Set response field.
     *
     * @param field response field
     */
    public void setField(String field) {
        this.field = field;
    }

}

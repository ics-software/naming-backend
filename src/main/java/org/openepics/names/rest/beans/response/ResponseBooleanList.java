/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.response;

import java.util.List;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * This class is used to ensure response to request origin are handled in a uniform way.
 *
 * @author Lars Johansson
 */
@Schema(description = """
                      A collection of attributes that is used to encapsulate a response from server to client.
                      It is used to show information related to an operation for a name and structure entry.
                      Values are set server-side.
                      """)
public class ResponseBooleanList extends ResponseBoolean {

    @Schema(description = "Items that are returned to client from server.")
    private List<ResponseBoolean> list = Lists.newArrayList();

    /**
     * Constructor.
     */
    public ResponseBooleanList() {
        super();
    }

    /**
     * Constructor.
     *
     * @param list list of response boolean objects
     * @param value response value
     * @param message response message
     */
    public ResponseBooleanList(List<ResponseBoolean> list, Boolean value, String message) {
        super(value, message, "", "");
        this.list = list;
    }

    /**
     * Return list of response boolean objects.
     *
     * @return list of response boolean objects
     */
    public List<ResponseBoolean> getList() {
        return this.list;
    }

}

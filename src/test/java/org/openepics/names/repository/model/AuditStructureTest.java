/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;

/**
 * Unit tests for AuditStructure class.
 *
 * @author Lars Johansson
 *
 * @see System
 */
class AuditStructureTest {

    @Test
    void equals() {
        AuditStructure aus1 = null, aus2 = null;
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus1 = new AuditStructure("auditOperation", s1);
        aus2 = new AuditStructure("auditOperation", s2);

        assertEquals(aus1, aus2);
    }

    @Test
    void equalsNoArgs() {
        AuditStructure aus1 = null, aus2 = null;
        System s1 = null, s2 = null;

        s1 = new System();
        s2 = new System();
        aus1 = new AuditStructure("auditOperation", s1);
        aus2 = new AuditStructure("auditOperation", s2);

        assertEquals(aus1, aus2);
    }

    @Test
    void notEquals() {
        AuditStructure aus1 = null, aus2 = null;
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus1 = new AuditStructure("auditOperation", s1);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 2L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA2", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA2", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 42, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description 2",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.REJECTED, false,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, true,
                date, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                null, "test who", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who 2", "comment");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment 2");
        aus2 = new AuditStructure("auditOperation", s2);
        assertNotEquals(aus1, aus2);
    }

    @Test
    void notEqualsNoArgs() {
        AuditStructure aus1 = null, aus2 = null;
        SystemGroup sg = null;
        System sys = null;
        Subsystem sub = null;
        Discipline di = null;
        DeviceGroup dg = null;
        DeviceType dt = null;

        sg = new SystemGroup();
        sys = new System();
        sub = new Subsystem();
        di = new Discipline();
        dg = new DeviceGroup();
        dt = new DeviceType();

        aus1 = new AuditStructure("auditOperation", sg);
        aus2 = new AuditStructure("auditOperation", sys);
        assertNotEquals(aus1, aus2);

        aus2 = new AuditStructure("auditOperation", sub);
        assertNotEquals(aus1, aus2);

        aus2 = new AuditStructure("auditOperation", di);
        assertNotEquals(aus1, aus2);

        aus2 = new AuditStructure("auditOperation", dg);
        assertNotEquals(aus1, aus2);

        aus2 = new AuditStructure("auditOperation", dt);
        assertNotEquals(aus1, aus2);
    }

    @Test
    void hashCodeEquals() {
        AuditStructure aus1 = null, aus2 = null;
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus1 = new AuditStructure("auditOperation", s1);
        aus2 = new AuditStructure("auditOperation", s2);

        assertNotNull(aus1);
        assertNotNull(aus2);
        assertEquals(aus1, aus2);
        assertEquals(aus1.hashCode(), aus2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        AuditStructure aus1 = null, aus2 = null;
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus1 = new AuditStructure("auditOperation", s1);
        aus2 = new AuditStructure("auditOperation", s2);

        assertNotNull(aus1);
        assertNotNull(aus2);
        assertNotEquals(aus1, aus2);
        assertNotEquals(aus1.hashCode(), aus2.hashCode());
    }

    @Test
    void toString_() {
        AuditStructure aus1 = null;
        System s1 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        aus1 = new AuditStructure("auditOperation", s1);

        String str = aus1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
        assertTrue(str.contains("description"));
        assertTrue(str.contains("comment"));
    }

}

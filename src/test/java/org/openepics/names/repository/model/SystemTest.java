/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;

/**
 * Unit tests for System class.
 *
 * @author Lars Johansson
 *
 * @see System
 */
class SystemTest {

    @Test
    void equals() {
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertEquals(s1, s2);

        AuditStructure aus1 = new AuditStructure("auditOperation", s1);
        assertEquals(new System(aus1), s2);
    }

    @Test
    void equalsNoArgs() {
        System s1 = null, s2 = null;

        assertEquals(s1, s2);

        s1 = new System();
        s2 = new System();
        assertEquals(s1, s2);
    }

    @Test
    void notEquals() {
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 2L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA2", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA2", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 42, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description 2",
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.REJECTED, false,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, true,
                date, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                null, "test who", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who 2", "comment");
        assertNotEquals(s1, s2);

        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment 2");
        assertNotEquals(s1, s2);
    }

    @Test
    void notEqualsNoArgs() {
        System s1 = null;
        Subsystem s2 = null;

        s1 = new System();
        s2 = new Subsystem();

        assertNotEquals(s1, s2);
    }

    @Test
    void hashCodeEquals() {
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");

        assertNotNull(s1);
        assertNotNull(s2);
        assertEquals(s1, s2);
        assertEquals(s1.hashCode(), s2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        System s1 = null, s2 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");
        s2 = new System(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");

        assertNotNull(s1);
        assertNotNull(s2);
        assertNotEquals(s1, s2);
        assertNotEquals(s1.hashCode(), s2.hashCode());
    }

    @Test
    void toString_() {
        System s1 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");

        String str = s1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
        assertTrue(str.contains("description"));
        assertTrue(str.contains("comment"));
    }

    @Test
    void toStringSimple() {
        System s1 = null;
        Date date = new Date();

        s1 = new System(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), 1L,
                "AA1", "AA1", 41, "description",
                Status.APPROVED, false,
                date, "test who", "comment");

        String str = s1.toStringSimple();

        assertNotNull(str);
        assertTrue (str.contains("uuid"));
        assertFalse(str.contains("description"));
        assertFalse(str.contains("comment"));
    }

}

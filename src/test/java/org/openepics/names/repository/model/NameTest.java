/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;

/**
 * Unit tests for Name class.
 *
 * @author Lars Johansson
 *
 * @see Name
 */
class NameTest {

    @Test
    void equals() {
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);

        assertEquals(n1, n2);
    }

    @Test
    void equalsNoArgs() {
        Name n1 = null, n2 = null;

        n1 = new Name();
        n2 = new Name();

        assertEquals(n1, n2);
    }

    @Test
    void notEquals() {
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 2L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, 1L, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, 1L, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, 1L,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                "010", "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acd", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACD", null,
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", "description",
                Status.APPROVED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.REJECTED, false,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, true,
                date, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                null, "test who", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who 2", null);
        assertNotEquals(n1, n2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", "comment 2");
        assertNotEquals(n1, n2);
    }

    @Test
    void notEqualsNoArgs() {
        Name n1 = null;
        NameStructure n2 = null;

        n1 = new Name();
        n2 = new NameStructure();

        assertNotEquals(n1, n2);
    }

    @Test
    void hashCodeEquals() {
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);

        assertNotNull(n1);
        assertNotNull(n2);
        assertEquals(n1, n2);
        assertEquals(n1.hashCode(), n2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);

        assertNotNull(n1);
        assertNotNull(n2);
        assertNotEquals(n1, n2);
        assertNotEquals(n1.hashCode(), n2.hashCode());
    }

    @Test
    void toString_() {
        Name n1 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);

        String str = n1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
        assertTrue(str.contains("description"));
        assertTrue(str.contains("comment"));
    }

    @Test
    void toStringSimple() {
        Name n1 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);

        String str = n1.toStringSimple();

        assertNotNull(str);
        assertTrue (str.contains("uuid"));
        assertFalse(str.contains("description"));
        assertFalse(str.contains("comment"));
    }

}

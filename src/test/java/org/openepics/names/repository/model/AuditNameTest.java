/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.repository.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;

/**
 * Unit tests for AuditName class.
 *
 * @author Lars Johansson
 *
 * @see Name
 */
class AuditNameTest {

    @Test
    void equals() {
        AuditName aun1 = null, aun2 = null;
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        aun2 = new AuditName("auditOperation", n2);

        assertEquals(aun1, aun2);
    }

    @Test
    void equalsNoArgs() {
        AuditName aun1 = null, aun2 = null;
        Name n1 = null, n2 = null;

        n1 = new Name();
        n2 = new Name();
        aun1 = new AuditName("auditOperation", n1);
        aun2 = new AuditName("auditOperation", n2);

        assertEquals(aun1, aun2);
    }

    @Test
    void notEquals() {
        AuditName aun1 = null, aun2 = null;
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        assertNotEquals(aun1, aun2);

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 2L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, 1L, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, 1L, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, 1L,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                "010", "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acd", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACD", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", "description 2",
                Status.APPROVED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.REJECTED, false,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, true,
                date, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                null, "test who", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who 2", null);
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);

        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", "comment 2");
        aun2 = new AuditName("auditOperation", n2);
        assertNotEquals(aun1, aun2);
    }

    @Test
    void notEqualsNoArgs() {
        AuditName aun1 = null;
        Name n1 = null;

        n1 = new Name();
        aun1 = new AuditName("auditOperation", n1);

        assertNotEquals(n1, aun1);
    }

    @Test
    void hashCodeEquals() {
        AuditName aun1 = null, aun2 = null;
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        aun2 = new AuditName("auditOperation", n2);

        assertNotNull(aun1);
        assertNotNull(aun2);
        assertEquals(aun1, aun2);
        assertEquals(aun1.hashCode(), aun2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        AuditName aun1 = null, aun2 = null;
        Name n1 = null, n2 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        n2 = new Name(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        aun2 = new AuditName("auditOperation", n2);

        assertNotNull(aun1);
        assertNotNull(aun2);
        assertNotEquals(aun1, aun2);
        assertNotEquals(aun1.hashCode(), aun2.hashCode());
    }

    @Test
    void toString_() {
        AuditName aun1 = null;
        Name n1 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);

        String str = aun1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
        assertTrue(str.contains("description"));
        assertTrue(str.contains("comment"));
    }

    @Test
    void getNonAuditName() {
        AuditName aun1 = null;
        Name n1 = null, nan1 = null;
        Date date = new Date();

        n1 = new Name(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"), 1L, null, null, null,
                null, "Acc", "ACC", null,
                Status.APPROVED, false,
                date, "test who", null);
        aun1 = new AuditName("auditOperation", n1);
        nan1 = aun1.getNonAuditName();

        assertEquals(n1, nan1);
    }

}

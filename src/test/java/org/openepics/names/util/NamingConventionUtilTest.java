/*
 * Copyright (c) 2019 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Discipline;

/**
 * Unit tests for NamingConventionUtil class.
 *
 * @author Lars Johansson
 *
 * @see NamingConventionUtil
 */
class NamingConventionUtilTest {

    // misc
    private static final String NULL    = null;
    private static final String EMPTY   = "";

    // support
    private static final String ACC     = "Acc";
    private static final String SUB     = "Sub";
    private static final String DIS     = "Dis";
    private static final String DEV     = "Dev";
    private static final String IDX     = "Idx";
    private static final String DIS_DEV = "Dis-Dev";
    private static final String PROP    = "Property";

    // convention
    //     standard
    //     corner cases
    //     property
    private static final String SYG                        = "Syg";
    private static final String SYS                        = "Sys";
    private static final String SYS_SUB                    = "Sys-Sub";
    private static final String SYG_DIS_DEV_IDX            = "Syg:Dis-Dev-Idx";
    private static final String SYS_DIS_DEV_IDX            = "Sys:Dis-Dev-Idx";
    private static final String SYS_SUB_DIS_DEV_IDX        = "Sys-Sub:Dis-Dev-Idx";

    private static final String SYG_SYS_SUB                = "Syg-Sys-Sub";
    private static final String SYG_SYS_SUB_DIS_DEV        = "Syg-Sys-Sub:Dis-Dev";
    private static final String SYG_SYS_SUB_DIS_DEV_IDX    = "Syg-Sys-Sub:Dis-Dev-Idx";
    private static final String SYS_SUB_DIS_DEV            = "Sys-Sub:Dis-Dev";

    private static final String SYG_PROP                   = "Syg::Property";
    private static final String SYS_PROP                   = "Sys::Property";
    private static final String SYS_SUB_PROP               = "Sys-Sub::Property";
    private static final String SYG_DIS_DEV_IDX_PROP       = "Syg:Dis-Dev-Idx:Property";
    private static final String SYS_DIS_DEV_IDX_PROP       = "Sys:Dis-Dev-Idx:Property";
    private static final String SYS_SUB_DIS_DEV_IDX_PROP   = "Sys-Sub:Dis-Dev-Idx:Property";

    // examples
    //     system structure
    //     system structure + device structure
    //     system structure + device structure + instance index
    //     offsite
    //     pid, pid numeric
    //     ----------------------------------------
    //     conventionname_abc_def
    //         a - 1/0 - system group   or not
    //         b - 1/0 - system         or not
    //         c - 1/0 - subsystem      or not
    //         d - 1/0 - discipline     or not
    //         e - 1/0 - device type    or not
    //         f - 1/0 - instance index or not
    private static final String CONVENTIONNAME_010         = "LEBT";
    private static final String CONVENTIONNAME_011         = "LEBT-010";
    private static final String CONVENTIONNAME_010_111     = "CWL:WtrC-EC-003";
    private static final String CONVENTIONNAME_011_110     = "LEBT-010:PwrC-PSChop";
    private static final String CONVENTIONNAME_011_111     = "CWL-CWSE05:WtrC-EC-003";
    private static final String CONVENTIONNAME_111_111     = "UU-FREIA-010CDL:Cryo-CV-001";
    private static final String CONVENTIONNAME_011_111_SC1 = "CWL-CWSE05:SC-EC-003";
    private static final String CONVENTIONNAME_011_111_SC2 = "CWL-CWSE05:SC-IOC-003";

    // error
    private static final String ERROR_SYSTEM_STRUCTURE_1   = "-Sys:Dis-Dev-Idx";
    private static final String ERROR_SYSTEM_STRUCTURE_2   = "Sys-:Dis-Dev-Idx";
    private static final String ERROR_DEVICE_STRUCTURE_1   = "Sys-Sub:-Dev-Idx";
    private static final String ERROR_DEVICE_STRUCTURE_2   = "Sys-Sub:Dis-Dev-";
    private static final String ERROR_DEVICE_STRUCTURE_3   = "Sys-Sub:DisDev";
    private static final String ERROR_INSTANCE_INDEX       = "Sys-Sub:Dis-Dev-Idx-";
    private static final String ERROR_DELIMITERS_PROPERTY  = "::Property";
    private static final String ERROR_NAME_DELIMITER       = "Sys-Sub:Dis-Dev-Idx:";
    private static final String ERROR_DELIMITER_1          = ":";
    private static final String ERROR_DELIMITER_2          = "::";
    private static final String ERROR_DELIMITER_3          = ":::";


    /**
     * Test of extraction of name information from ESS convention name and process variable name.
     */
    @Test
    void extractNameInformation() {
        NameInformation nameInformation = null;

        // misc
        nameInformation = NamingConventionUtil.extractNameInformation(NULL);
        assertNull  (nameInformation.name());
        assertNull  (nameInformation.conventionName());
        assertNull  (nameInformation.systemGroup());
        assertNull  (nameInformation.system());
        assertNull  (nameInformation.subsystem());
        assertNull  (nameInformation.discipline());
        assertNull  (nameInformation.deviceType());
        assertNull  (nameInformation.instanceIndex());
        assertNull  (nameInformation.property());
        assertNull  (nameInformation.mnemonicPathSystemStructure());
        assertNull  (nameInformation.mnemonicPathDeviceStructure());
        assertNull  (nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(EMPTY);
        assertEquals(EMPTY,         nameInformation.name());
        assertNull  (nameInformation.conventionName());
        assertNull  (nameInformation.systemGroup());
        assertNull  (nameInformation.system());
        assertNull  (nameInformation.subsystem());
        assertNull  (nameInformation.discipline());
        assertNull  (nameInformation.deviceType());
        assertNull  (nameInformation.instanceIndex());
        assertNull  (nameInformation.property());
        assertNull  (nameInformation.mnemonicPathSystemStructure());
        assertNull  (nameInformation.mnemonicPathDeviceStructure());
        assertNull  (nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        // convention
        nameInformation = NamingConventionUtil.extractNameInformation(SYS);
        assertEquals(SYS,           nameInformation.name());
        assertEquals(SYS,           nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS,           nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertNull  (nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(SYS_SUB);
        assertEquals(SYS_SUB,       nameInformation.name());
        assertEquals(SYS_SUB,       nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertNull  (nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(SYS_SUB_PROP);
        assertEquals(SYS_SUB_PROP,  nameInformation.name());
        assertEquals(SYS_SUB,       nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(PROP,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertTrue  (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(SYS_DIS_DEV_IDX);
        assertEquals(SYS_DIS_DEV_IDX,           nameInformation.name());
        assertEquals(SYS_DIS_DEV_IDX,           nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS,           nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(SYG_SYS_SUB_DIS_DEV_IDX);
        assertEquals(SYG_SYS_SUB_DIS_DEV_IDX,   nameInformation.name());
        assertEquals(SYG_SYS_SUB_DIS_DEV_IDX,   nameInformation.conventionName());
        assertEquals(SYG,           nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYG_SYS_SUB,   nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertTrue  (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(SYS_SUB_DIS_DEV_IDX_PROP);
        assertEquals(SYS_SUB_DIS_DEV_IDX_PROP,  nameInformation.name());
        assertEquals(SYS_SUB_DIS_DEV_IDX,       nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(PROP,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertTrue  (nameInformation.isProcessVariableName());
    }

    /**
     * Test of extraction of name information from ESS convention name and process variable name.
     */
    @Test
    void extractNameInformationError() {
        // test various incorrect names
        //     convention name or process variable name

        NameInformation nameInformation = null;

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_SYSTEM_STRUCTURE_1);
        assertEquals(ERROR_SYSTEM_STRUCTURE_1,  nameInformation.name());
        assertEquals(ERROR_SYSTEM_STRUCTURE_1,  nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(NULL,          nameInformation.system());
        assertEquals(SYS,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals("-Sys",        nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_SYSTEM_STRUCTURE_2);
        assertEquals(ERROR_SYSTEM_STRUCTURE_2,  nameInformation.name());
        assertEquals(ERROR_SYSTEM_STRUCTURE_2,  nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals("Sys-",        nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DEVICE_STRUCTURE_1);
        assertEquals(ERROR_DEVICE_STRUCTURE_1,  nameInformation.name());
        assertEquals(ERROR_DEVICE_STRUCTURE_1,  nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DEVICE_STRUCTURE_2);
        assertEquals(ERROR_DEVICE_STRUCTURE_2,  nameInformation.name());
        assertEquals(ERROR_DEVICE_STRUCTURE_2,  nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DEVICE_STRUCTURE_3);
        assertEquals(ERROR_DEVICE_STRUCTURE_3,  nameInformation.name());
        assertEquals(ERROR_DEVICE_STRUCTURE_3,  nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_INSTANCE_INDEX);
        assertEquals(ERROR_INSTANCE_INDEX,      nameInformation.name());
        assertEquals(ERROR_INSTANCE_INDEX,      nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DELIMITERS_PROPERTY);
        assertEquals(ERROR_DELIMITERS_PROPERTY, nameInformation.name());
        assertEquals(NULL,          nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(NULL,          nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(PROP,          nameInformation.property());
        assertEquals(NULL,          nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_NAME_DELIMITER);
        assertEquals(ERROR_NAME_DELIMITER,      nameInformation.name());
        assertEquals(SYS_SUB_DIS_DEV_IDX,       nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(SYS,           nameInformation.system());
        assertEquals(SUB,           nameInformation.subsystem());
        assertEquals(DIS,           nameInformation.discipline());
        assertEquals(DEV,           nameInformation.deviceType());
        assertEquals(IDX,           nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(SYS_SUB,       nameInformation.mnemonicPathSystemStructure());
        assertEquals(DIS_DEV,       nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertTrue  (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DELIMITER_1);
        assertEquals(ERROR_DELIMITER_1,         nameInformation.name());
        assertEquals(NULL,          nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(NULL,          nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(NULL,          nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DELIMITER_2);
        assertEquals(ERROR_DELIMITER_2,         nameInformation.name());
        assertEquals(NULL,          nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(NULL,          nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(NULL,          nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());

        nameInformation = NamingConventionUtil.extractNameInformation(ERROR_DELIMITER_3);
        assertEquals(ERROR_DELIMITER_3,         nameInformation.name());
        assertEquals(NULL,          nameInformation.conventionName());
        assertEquals(NULL,          nameInformation.systemGroup());
        assertEquals(NULL,          nameInformation.system());
        assertEquals(NULL,          nameInformation.subsystem());
        assertEquals(NULL,          nameInformation.discipline());
        assertEquals(NULL,          nameInformation.deviceType());
        assertEquals(NULL,          nameInformation.instanceIndex());
        assertEquals(NULL,          nameInformation.property());
        assertEquals(NULL,          nameInformation.mnemonicPathSystemStructure());
        assertEquals(NULL,          nameInformation.mnemonicPathDeviceStructure());
        assertEquals(NULL,          nameInformation.instanceIndexStyle());
        assertFalse (nameInformation.isDisciplinePID());
        assertFalse (nameInformation.isMnemonicPathDeviceStructurePIDNumeric());
        assertFalse (nameInformation.isOffsite());
        assertFalse (nameInformation.isConventionName());
        assertFalse (nameInformation.isProcessVariableName());
    }

    /**
     * Test of extraction of process variable name information from ESS convention name and process variable name.
     */
    @Test
    void extractProcessVariableNameInformation() {
        ProcessVariableNameInformation pvNameInformation = null;

        // misc
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(NULL);
        assertEquals (NULL,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(EMPTY);
        assertEquals (EMPTY,                      pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        // convention
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG);
        assertEquals (SYG,                        pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_SUB);
        assertEquals (SYS_SUB,                    pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_SYS_SUB);
        assertEquals (SYG_SYS_SUB,                pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_DIS_DEV_IDX);
        assertEquals (SYG_DIS_DEV_IDX,            pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_DIS_DEV_IDX);
        assertEquals (SYS_DIS_DEV_IDX,            pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_SUB_DIS_DEV_IDX);
        assertEquals (SYS_SUB_DIS_DEV_IDX,        pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        // ----------

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_SYS_SUB);
        assertEquals (SYG_SYS_SUB,                pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_SYS_SUB_DIS_DEV);
        assertEquals (SYG_SYS_SUB_DIS_DEV,        pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_SYS_SUB_DIS_DEV_IDX);
        assertEquals (SYG_SYS_SUB_DIS_DEV_IDX,    pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_SUB_DIS_DEV);
        assertEquals (SYS_SUB_DIS_DEV,            pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        // ----------

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_PROP);
        assertEquals (SYG_PROP,                   pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_PROP);
        assertEquals (SYS_PROP,                   pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_SUB_PROP);
        assertEquals (SYS_SUB_PROP,               pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYG_DIS_DEV_IDX_PROP);
        assertEquals (SYG_DIS_DEV_IDX_PROP,       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_DIS_DEV_IDX_PROP);
        assertEquals (SYS_DIS_DEV_IDX_PROP,       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(SYS_SUB_DIS_DEV_IDX_PROP);
        assertEquals (SYS_SUB_DIS_DEV_IDX_PROP,   pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        // examples
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_010);
        assertEquals (CONVENTIONNAME_010,         pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_011);
        assertEquals (CONVENTIONNAME_011,         pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_010_111);
        assertEquals (CONVENTIONNAME_010_111,     pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_011_110);
        assertEquals (CONVENTIONNAME_011_110,     pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_011_111);
        assertEquals (CONVENTIONNAME_011_111,     pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_111_111);
        assertEquals (CONVENTIONNAME_111_111,     pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_011_111_SC1);
        assertEquals (CONVENTIONNAME_011_111_SC1, pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(CONVENTIONNAME_011_111_SC2);
        assertEquals (CONVENTIONNAME_011_111_SC2, pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());
    }

    /**
     * Test of extraction of process variable name information from ESS convention name and process variable name.
     */
    @Test
    void extractProcessVariableNameInformationError() {
        // test various incorrect names
        //     convention name or process variable name

        ProcessVariableNameInformation pvNameInformation = null;

        // error
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_SYSTEM_STRUCTURE_1);
        assertEquals (ERROR_SYSTEM_STRUCTURE_1,   pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_SYSTEM_STRUCTURE_2);
        assertEquals (ERROR_SYSTEM_STRUCTURE_2,   pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DEVICE_STRUCTURE_1);
        assertEquals (ERROR_DEVICE_STRUCTURE_1,   pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DEVICE_STRUCTURE_2);
        assertEquals (ERROR_DEVICE_STRUCTURE_2,   pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DEVICE_STRUCTURE_3);
        assertEquals (ERROR_DEVICE_STRUCTURE_3,   pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_INSTANCE_INDEX);
        assertEquals (ERROR_INSTANCE_INDEX,       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DELIMITERS_PROPERTY);
        assertEquals (ERROR_DELIMITERS_PROPERTY,  pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_NAME_DELIMITER);
        assertEquals (ERROR_NAME_DELIMITER,       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DELIMITER_1);
        assertEquals (ERROR_DELIMITER_1,          pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DELIMITER_2);
        assertEquals (ERROR_DELIMITER_2,          pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(ERROR_DELIMITER_3);
        assertEquals (ERROR_DELIMITER_3,          pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());
    }

    /**
     * Test of extraction of process variable name information from ESS convention name and process variable name.
     */
    @Test
    void extractProcessVariableNameInformationValid() {
        // test various valid and invalid names
        //     convention name or process variable name

        ProcessVariableNameInformation pvNameInformation = null;
        String name = null;

        // incorrect
        //     SYS_SUB_DIS_DEV_IDX:property
        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "AAAAAAAAAABBBBBBBBBBCCCCCCCCCCDDDDDDDDDDEEEEEEEEEEFFFFFFFFFF";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "AAAAAAAAAABBBBBBBBBBCCCCCCCCCC";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRST";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        // length 21, ends with -R, -S, -RB
        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTU";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRS-R";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRS-S";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQR-RB";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        // length 22, ends with -R, -S, -RB
        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTUV";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRST-R";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRST-S";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRS-RB";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        // length 23, ends with -R, -S, -RB
        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTUVW";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTU-R";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTU-S";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        // length 24, ends with -R, -S, -RB
        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTUVWX";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTUV-R";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTUV-S";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJKLMNOPQRSTU-RB";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "A";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABC";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "Pwr";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertTrue   (pvNameInformation.isValidFormat());
        assertNull   (pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDEFGHIJ()";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "#";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "#ABCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "ABCDE#FGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "1BCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "_BCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "-BCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());

        name = SYS_SUB_DIS_DEV_IDX + ":" + "aBCDEFGHIJ";
        pvNameInformation = NamingConventionUtil.extractProcessVariableNameInformation(name);
        assertEquals (name,                       pvNameInformation.name());
        assertFalse  (pvNameInformation.isValidFormat());
        assertNotNull(pvNameInformation.message());
    }

    /**
     * Test of extraction of system group from ESS convention name and process variable name.
     */
    @Test
    void extractSystemGroup() {
        // misc
        assertNull(NamingConventionUtil.extractSystemGroup(NULL));
        assertNull(NamingConventionUtil.extractSystemGroup(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_SUB));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYG_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_SUB_DIS_DEV_IDX));

        assertEquals(SYG,       NamingConventionUtil.extractSystemGroup(SYG_SYS_SUB));
        assertEquals(SYG,       NamingConventionUtil.extractSystemGroup(SYG_SYS_SUB_DIS_DEV));
        assertEquals(SYG,       NamingConventionUtil.extractSystemGroup(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_SUB_DIS_DEV));

        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYG_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_SUB_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYG_DIS_DEV_IDX_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_DIS_DEV_IDX_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_110));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111));
        assertEquals("UU",      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_111_111));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111_SC1));
        assertEquals(NULL,      NamingConventionUtil.extractSystemGroup(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of system from ESS convention name and process variable name.
     */
    @Test
    void extractSystem() {
        // misc
        assertNull(NamingConventionUtil.extractSystem(NULL));
        assertNull(NamingConventionUtil.extractSystem(EMPTY));

        // convention
        assertEquals(SYG,       NamingConventionUtil.extractSystem(SYG));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_SUB));
        assertEquals(SYG,       NamingConventionUtil.extractSystem(SYG_DIS_DEV_IDX));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_DIS_DEV_IDX));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_SUB_DIS_DEV_IDX));

        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYG_SYS_SUB));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYG_SYS_SUB_DIS_DEV));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_SUB_DIS_DEV));

        assertEquals(SYG,       NamingConventionUtil.extractSystem(SYG_PROP));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_PROP));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_SUB_PROP));
        assertEquals(SYG,       NamingConventionUtil.extractSystem(SYG_DIS_DEV_IDX_PROP));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_DIS_DEV_IDX_PROP));
        assertEquals(SYS,       NamingConventionUtil.extractSystem(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_010));
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_011));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_010_111));
        assertEquals("LEBT",    NamingConventionUtil.extractSystem(CONVENTIONNAME_011_110));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111));
        assertEquals("FREIA",   NamingConventionUtil.extractSystem(CONVENTIONNAME_111_111));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWL",     NamingConventionUtil.extractSystem(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of subsystem group from ESS convention name and process variable name.
     */
    @Test
    void extractSubsystem() {
        // misc
        assertNull(NamingConventionUtil.extractSubsystem(NULL));
        assertNull(NamingConventionUtil.extractSubsystem(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYS));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYS_SUB));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYG_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYS_DIS_DEV_IDX));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYS_SUB_DIS_DEV_IDX));

        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYG_SYS_SUB));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYG_SYS_SUB_DIS_DEV));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYS_SUB_DIS_DEV));

        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYG_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYS_PROP));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYS_SUB_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYG_DIS_DEV_IDX_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(SYS_DIS_DEV_IDX_PROP));
        assertEquals(SUB,       NamingConventionUtil.extractSubsystem(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(CONVENTIONNAME_010));
        assertEquals("010",     NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011));
        assertEquals(NULL,      NamingConventionUtil.extractSubsystem(CONVENTIONNAME_010_111));
        assertEquals("010",     NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_110));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111));
        assertEquals("010CDL",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_111_111));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWSE05",  NamingConventionUtil.extractSubsystem(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of discipline from ESS convention name and process variable name.
     */
    @Test
    void extractDiscipline() {
        // misc
        assertNull(NamingConventionUtil.extractDiscipline(NULL));
        assertNull(NamingConventionUtil.extractDiscipline(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYS));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYS_SUB));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYG_DIS_DEV_IDX));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYS_DIS_DEV_IDX));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYG_SYS_SUB));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYG_SYS_SUB_DIS_DEV));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV));

        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYG_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYS_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(SYS_SUB_PROP));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYG_DIS_DEV_IDX_PROP));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYS_DIS_DEV_IDX_PROP));
        assertEquals(DIS,       NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011));
        assertEquals("WtrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010_111));
        assertEquals("PwrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_110));
        assertEquals("WtrC",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111));
        assertEquals("Cryo",    NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111));
        assertEquals("SC",      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC1));
        assertEquals("SC",      NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of device type from ESS convention name and process variable name.
     */
    @Test
    void extractDeviceType() {
        // misc
        assertNull(NamingConventionUtil.extractDeviceType(NULL));
        assertNull(NamingConventionUtil.extractDeviceType(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYS));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYS_SUB));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYG_DIS_DEV_IDX));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYS_DIS_DEV_IDX));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYG_SYS_SUB));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYG_SYS_SUB_DIS_DEV));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYS_SUB_DIS_DEV));

        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYG_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYS_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(SYS_SUB_PROP));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYG_DIS_DEV_IDX_PROP));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYS_DIS_DEV_IDX_PROP));
        assertEquals(DEV,       NamingConventionUtil.extractDeviceType(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_010_111));
        assertEquals("PSChop",  NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_110));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111));
        assertEquals("CV",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_111_111));
        assertEquals("EC",      NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111_SC1));
        assertEquals("IOC",     NamingConventionUtil.extractDeviceType(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of instance index from ESS convention name and process variable name.
     */
    @Test
    void extractInstanceIndex() {
        // misc
        assertNull(NamingConventionUtil.extractInstanceIndex(NULL));
        assertNull(NamingConventionUtil.extractInstanceIndex(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYS));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYS_SUB));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYG_DIS_DEV_IDX));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYS_DIS_DEV_IDX));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYG_SYS_SUB));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYG_SYS_SUB_DIS_DEV));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYS_SUB_DIS_DEV));

        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYG_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYS_PROP));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(SYS_SUB_PROP));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYG_DIS_DEV_IDX_PROP));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYS_DIS_DEV_IDX_PROP));
        assertEquals(IDX,       NamingConventionUtil.extractInstanceIndex(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_110));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111));
        assertEquals("001",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_111_111));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111_SC1));
        assertEquals("003",     NamingConventionUtil.extractInstanceIndex(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of property from ESS convention name and process variable name.
     */
    @Test
    void extractProperty() {
        // misc
        assertNull(NamingConventionUtil.extractProperty(NULL));
        assertNull(NamingConventionUtil.extractProperty(EMPTY));

        // convention
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYG));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYS));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYS_SUB));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYG_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYS_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYG_SYS_SUB));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYG_SYS_SUB_DIS_DEV));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(SYS_SUB_DIS_DEV));

        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYG_PROP));
        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYS_PROP));
        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYS_SUB_PROP));
        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYG_DIS_DEV_IDX_PROP));
        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYS_DIS_DEV_IDX_PROP));
        assertEquals(PROP,      NamingConventionUtil.extractProperty(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_010));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_011));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_010_111));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_011_110));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_011_111));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_111_111));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_011_111_SC1));
        assertEquals(NULL,      NamingConventionUtil.extractProperty(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of mnemonic path for system structure from ESS convention name and process variable name.
     */
    @Test
    void extractMnemonicPathSystemStructure() {
        // may be put in loop for performance test
        // loop size may be increased to 10000000 in order to view performance

        // misc
        assertEquals(NULL,               NamingConventionUtil.extractMnemonicPathSystemStructure(NULL));
        assertEquals(NULL,               NamingConventionUtil.extractMnemonicPathSystemStructure(EMPTY));

        // convention
        assertEquals(SYG,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYG));
        assertEquals(SYS,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYS));
        assertEquals(SYS_SUB,            NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_SUB));
        assertEquals(SYG,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_DIS_DEV_IDX));
        assertEquals(SYS,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_DIS_DEV_IDX));
        assertEquals(SYS_SUB,            NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_SUB_DIS_DEV_IDX));

        assertEquals(SYG_SYS_SUB,        NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_SYS_SUB));
        assertEquals(SYG_SYS_SUB,        NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_SYS_SUB_DIS_DEV));
        assertEquals(SYG_SYS_SUB,        NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(SYS_SUB,            NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_SUB_DIS_DEV));

        assertEquals(SYG,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_PROP));
        assertEquals(SYS,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_PROP));
        assertEquals(SYS_SUB,            NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_SUB_PROP));
        assertEquals(SYG,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYG_DIS_DEV_IDX_PROP));
        assertEquals(SYS,                NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_DIS_DEV_IDX_PROP));
        assertEquals(SYS_SUB,            NamingConventionUtil.extractMnemonicPathSystemStructure(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals("LEBT",             NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_010));
        assertEquals("LEBT-010",         NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011));
        assertEquals("CWL",              NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_010_111));
        assertEquals("LEBT-010",         NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_110));
        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111));
        assertEquals("UU-FREIA-010CDL",  NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_111_111));
        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111_SC1));
        assertEquals("CWL-CWSE05",       NamingConventionUtil.extractMnemonicPathSystemStructure(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test of extraction of mnemonic path for device structure from ESS convention name and process variable name.
     */
    @Test
    void extractMnemonicPathDeviceStructure() {
        // may be put in loop for performance test
        // loop size may be increased to 1000000 in order to view performance

        // misc
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(NULL));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(EMPTY));

        // convention
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_DIS_DEV_IDX));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_DIS_DEV_IDX));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB_DIS_DEV));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV));

        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_PROP));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_PROP));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_PROP));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_DIS_DEV_IDX_PROP));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_DIS_DEV_IDX_PROP));
        assertEquals(DIS_DEV,       NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010));
        assertEquals(NULL,          NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011));
        assertEquals("WtrC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010_111));
        assertEquals("PwrC-PSChop", NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_110));
        assertEquals("WtrC-EC",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111));
        assertEquals("Cryo-CV",     NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111));
        assertEquals("SC-EC",       NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC1));
        assertEquals("SC-IOC",      NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test to get index style from ESS convention name and process variable name.
     */
    @Test
    void getInstanceIndexStyleConventionName() {
        // misc
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(NULL));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(EMPTY));

        // convention
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYG));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYS));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYS_SUB));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYG_DIS_DEV_IDX));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYS_DIS_DEV_IDX));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYS_SUB_DIS_DEV_IDX));

        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYG_SYS_SUB));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYG_SYS_SUB_DIS_DEV));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYG_SYS_SUB_DIS_DEV_IDX));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYS_SUB_DIS_DEV));

        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYG_PROP));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYS_PROP));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(SYS_SUB_PROP));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYG_DIS_DEV_IDX_PROP));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYS_DIS_DEV_IDX_PROP));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle("Mech-TDS"));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle("Cryo-TDS"));

        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_010));
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_010_111));
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_110));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_111_111));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111_SC1));
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID,       NamingConventionUtil.getInstanceIndexStyle(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test to get index style from discipline.
     */
    @Test
    void getInstanceIndexStyleDiscipline() {
        Discipline discipline = new Discipline();

        discipline.setMnemonic(NULL);
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic(EMPTY);
        assertEquals(NULL, NamingConventionUtil.getInstanceIndexStyle(discipline));

        discipline.setMnemonic(ACC);
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("LEBT");
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("010");
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));

        discipline.setMnemonic("Mech");
        assertEquals(NamingConventionUtil.DISCIPLINE_SCIENTIFIC, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("Cryo");
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID, NamingConventionUtil.getInstanceIndexStyle(discipline));
        discipline.setMnemonic("SC");
        assertEquals(NamingConventionUtil.DISCIPLINE_P_ID, NamingConventionUtil.getInstanceIndexStyle(discipline));
    }

    /**
     * Test to check if discipline is PID.
     */
    @Test
    void isDisciplinePID() {
        // misc
        assertFalse(NamingConventionUtil.isDisciplinePID(NULL));
        assertFalse(NamingConventionUtil.isDisciplinePID(EMPTY));

        // convention
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_SUB)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV_IDX)));

        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_SYS_SUB)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_SYS_SUB_DIS_DEV)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_SYS_SUB_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV)));

        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_PROP)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_PROP)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_SUB_PROP)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYG_DIS_DEV_IDX_PROP)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_DIS_DEV_IDX_PROP)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(SYS_SUB_DIS_DEV_IDX_PROP)));

        // examples
        assertFalse(NamingConventionUtil.isDisciplinePID("Mech"));
        assertTrue (NamingConventionUtil.isDisciplinePID("Cryo"));

        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_010_111)));
        assertFalse(NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_110)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_111_111)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC1)));
        assertTrue (NamingConventionUtil.isDisciplinePID(NamingConventionUtil.extractDiscipline(CONVENTIONNAME_011_111_SC2)));
    }

    /**
     * Test to check if mnemonic path (device structure) is PID numeric.
     */
    @Test
    void isMnemonicPathPIDNumeric() {
        // misc
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NULL));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(EMPTY));

        // convention
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV_IDX)));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB_DIS_DEV)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_SYS_SUB_DIS_DEV_IDX)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV)));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_PROP)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_PROP)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_PROP)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYG_DIS_DEV_IDX_PROP)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_DIS_DEV_IDX_PROP)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(SYS_SUB_DIS_DEV_IDX_PROP)));

        // examples
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric("Mech"));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric("Cryo"));

        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_010_111)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_110)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_111_111)));
        assertFalse(NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC1)));
        assertTrue (NamingConventionUtil.isMnemonicPathDeviceStructurePIDNumeric(NamingConventionUtil.extractMnemonicPathDeviceStructure(CONVENTIONNAME_011_111_SC2)));
    }

    /**
     * Test to check if ESS convention name and process variable name are offsite.
     */
    @Test
    void isOffsite() {
        // misc
        assertFalse(NamingConventionUtil.isOffsite(NULL));
        assertFalse(NamingConventionUtil.isOffsite(EMPTY));

        // convention
        assertFalse(NamingConventionUtil.isOffsite(SYG));
        assertFalse(NamingConventionUtil.isOffsite(SYS));
        assertFalse(NamingConventionUtil.isOffsite(SYS_SUB));
        assertFalse(NamingConventionUtil.isOffsite(SYG_DIS_DEV_IDX));
        assertFalse(NamingConventionUtil.isOffsite(SYS_DIS_DEV_IDX));
        assertFalse(NamingConventionUtil.isOffsite(SYS_SUB_DIS_DEV_IDX));

        assertTrue (NamingConventionUtil.isOffsite(SYG_SYS_SUB));
        assertTrue (NamingConventionUtil.isOffsite(SYG_SYS_SUB_DIS_DEV));
        assertTrue (NamingConventionUtil.isOffsite(SYG_SYS_SUB_DIS_DEV_IDX));
        assertFalse(NamingConventionUtil.isOffsite(SYS_SUB_DIS_DEV));

        assertFalse(NamingConventionUtil.isOffsite(SYG_PROP));
        assertFalse(NamingConventionUtil.isOffsite(SYS_PROP));
        assertFalse(NamingConventionUtil.isOffsite(SYS_SUB_PROP));
        assertFalse(NamingConventionUtil.isOffsite(SYG_DIS_DEV_IDX_PROP));
        assertFalse(NamingConventionUtil.isOffsite(SYS_DIS_DEV_IDX_PROP));
        assertFalse(NamingConventionUtil.isOffsite(SYS_SUB_DIS_DEV_IDX_PROP));

        // examples
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_010));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_010_111));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_110));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111));
        assertTrue (NamingConventionUtil.isOffsite(CONVENTIONNAME_111_111));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111_SC1));
        assertFalse(NamingConventionUtil.isOffsite(CONVENTIONNAME_011_111_SC2));
    }

    /**
     * Test to check conversion of string (e.g. name) to mnemonic path.
     */
    @Test
    void string2MnemonicPath() {
        String[] array = null;

        array = NamingConventionUtil.string2MnemonicPath(null);
        assertNotNull(array);
        assertEquals(0, array.length);

        array = NamingConventionUtil.string2MnemonicPath("");
        assertNotNull(array);
        assertEquals(0, array.length);

        array = NamingConventionUtil.string2MnemonicPath("A");
        assertNotNull(array);
        assertEquals(1, array.length);
        assertEquals("A", array[0]);

        array = NamingConventionUtil.string2MnemonicPath("A-B");
        assertNotNull(array);
        assertEquals(2, array.length);
        assertEquals("A", array[0]);
        assertEquals("B", array[1]);

        array = NamingConventionUtil.string2MnemonicPath("A-B-C");
        assertNotNull(array);
        assertEquals(3, array.length);
        assertEquals("A", array[0]);
        assertEquals("B", array[1]);
        assertEquals("C", array[2]);
    }
}

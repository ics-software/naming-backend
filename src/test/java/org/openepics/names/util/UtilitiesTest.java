/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;

import com.google.common.collect.Lists;

/**
 * Unit tests for Utilities class.
 *
 * @author Lars Johansson
 *
 * @see Utilities
 */
class UtilitiesTest {

    /**
     * Test of getting size of collection.
     */
    @Test
    void getSize() {
        assertEquals(0, Utilities.getSize(null));
        assertEquals(0, Utilities.getSize(Collections.emptyList()));
        assertEquals(0, Utilities.getSize(Collections.emptySet()));

        List<String> list = Lists.newArrayList();
        assertEquals(0, Utilities.getSize(list));
        list.add("a");
        assertEquals(1, Utilities.getSize(list));
        list.add("b");
        list.add("c");
        assertEquals(3, Utilities.getSize(list));
        list.add("d");
        list.add("e");
        list.add("f");
        assertEquals(6, Utilities.getSize(list));
    }

    /**
     * Test of getting length of array.
     */
    @Test
    void getLength() {
        Object[] values = null;
        assertEquals(0, Utilities.getLength(values));
        values = new Object[]{"a", 1L};
        assertEquals(2, Utilities.getLength(values));

        assertEquals(1, Utilities.getLength("a"));
        assertEquals(2, Utilities.getLength("a", "b"));
        assertEquals(3, Utilities.getLength("a", "b", "c"));
        assertEquals(1, Utilities.getLength(1L));
        assertEquals(2, Utilities.getLength(1L, 2L));
        assertEquals(3, Utilities.getLength(1L, 2L, 3L));

        assertEquals(1, Utilities.getLength(Type.SYSTEMGROUP));
        assertEquals(1, Utilities.getLength(Type.SYSTEM));
        assertEquals(1, Utilities.getLength(Type.SUBSYSTEM));
        assertEquals(1, Utilities.getLength(Type.DISCIPLINE));
        assertEquals(1, Utilities.getLength(Type.DEVICEGROUP));
        assertEquals(1, Utilities.getLength(Type.DEVICETYPE));

        assertEquals(2, Utilities.getLength(Type.SYSTEMGROUP, Type.SYSTEM));
        assertEquals(3, Utilities.getLength(Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM));
        assertEquals(4, Utilities.getLength(Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE));
        assertEquals(5, Utilities.getLength(Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE, Type.DEVICEGROUP));
        assertEquals(6, Utilities.getLength(Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE, Type.DEVICEGROUP, Type.DEVICETYPE));

        assertEquals(2, Utilities.getLength(Type.DEVICETYPE, Type.DEVICEGROUP));
        assertEquals(3, Utilities.getLength(Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE));
        assertEquals(4, Utilities.getLength(Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM));
        assertEquals(5, Utilities.getLength(Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM, Type.SYSTEM));
        assertEquals(6, Utilities.getLength(Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM, Type.SYSTEM, Type.SYSTEMGROUP));
    }

    /**
     * Test of adding value to collection.
     */
    @Test
    void addToCollection() {
        List<StructureElement> list = null;
        StructureElement structureElement = null;
        assertFalse(Utilities.addToCollection(list,  structureElement));

        structureElement = StructureElementUtil.getStructureElement(
                null, null, null,
                null, null, null,
                null, null,
                null, null,
                null, null, null);
        assertFalse(Utilities.addToCollection(list,  structureElement));

        list = Lists.newArrayList();
        structureElement = null;
        assertFalse(Utilities.addToCollection(list,  structureElement));

        structureElement = StructureElementUtil.getStructureElement(
                null, null, null,
                null, null, null,
                null, null,
                null, null,
                null, null, null);
        assertTrue(Utilities.addToCollection(list,  structureElement));
        assertEquals(1, list.size());
    }

}

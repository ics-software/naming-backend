/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.repository.model.Name;
import org.openepics.names.repository.model.Structure;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;

/**
 * Unit tests for NameElementUtil class.
 *
 * @author Lars Johansson
 *
 * @see NameElementUtil
 */
class NameElementUtilTest {

    /**
     * Test of get name element for name.
     */
    @Test
    void getNameElement() {
        Name name = null;
        HolderStructures holderStructures = null;
        Structure structure = null;

        NameElement nameElement = NameElementUtil.getNameElement(name, holderStructures);
        assertNull(nameElement);
        nameElement = NameElementUtil.getNameElement(name, structure);
        assertNull(nameElement);

        name = new Name();
        nameElement = NameElementUtil.getNameElement(name, holderStructures);
        assertNull(nameElement);
        nameElement = NameElementUtil.getNameElement(name, structure);
        assertNull(nameElement);
    }

    /**
     * Test of get name element for content.
     */
    @Test
    void getNameElementContent() {
        UUID uuid = UUID.randomUUID();
        UUID parentSystemStructure =  UUID.randomUUID();
        UUID parentDeviceStructure = UUID.randomUUID();
        String index = "ty";
        String description = "op";
        String systemStructure = "qw";
        String deviceStructure = "er";
        String name = "ui";
        Status status = Status.APPROVED;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "as";
        String comment = "df";

        NameElement nameElement = NameElementUtil.getNameElement(
                uuid, parentSystemStructure, parentDeviceStructure,
                index, description,
                systemStructure, deviceStructure, name,
                status, deleted,
                when, who, comment);

        assertNotNull(nameElement);
        assertEquals(uuid, nameElement.getUuid());
        assertEquals(parentSystemStructure, nameElement.getParentSystemStructure());
        assertEquals(parentDeviceStructure, nameElement.getParentDeviceStructure());
        assertEquals(systemStructure, nameElement.getSystemStructure());
        assertEquals(deviceStructure, nameElement.getDeviceStructure());
        assertEquals(index, nameElement.getIndex());
        assertEquals(name, nameElement.getName());
        assertEquals(description, nameElement.getDescription());
        assertEquals(status, nameElement.getStatus());
        assertEquals(deleted, nameElement.isDeleted());
        assertEquals(when, nameElement.getWhen());
        assertEquals(who, nameElement.getWho());
        assertEquals(comment, nameElement.getComment());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert name element command for create to name element command.
     */
    @Test
    void convertCommandCreate2Command() {
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        String str1 = "qwer";
        String str2 = "asdf";

        NameElementCommandCreate commandCreate = new NameElementCommandCreate(uuid2, uuid3, str1, str2);
        NameElementCommand command = NameElementUtil.convertCommandCreate2Command(commandCreate);

        assertNotNull(command);
        assertNull(command.getUuid());
        assertEquals(uuid2, command.getParentSystemStructure());
        assertEquals(uuid3, command.getParentDeviceStructure());
        assertEquals(str1, command.getIndex());
        assertEquals(str2, command.getDescription());
    }

    /**
     * Test of convert name element command for update to name element command.
     */
    @Test
    void convertCommandUpdate2Command() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        String str1 = "qwer";
        String str2 = "asdf";

        NameElementCommandUpdate commandUpdate = new NameElementCommandUpdate(uuid1, uuid2, uuid3, str1, str2);
        NameElementCommand command = NameElementUtil.convertCommandUpdate2Command(commandUpdate);

        assertNotNull(command);
        assertEquals(uuid1, command.getUuid());
        assertEquals(uuid2, command.getParentSystemStructure());
        assertEquals(uuid3, command.getParentDeviceStructure());
        assertEquals(str1, command.getIndex());
        assertEquals(str2, command.getDescription());
    }

    /**
     * Test of convert name element command for confirm to name element command.
     */
    @Test
    void convertCommandConfirm2Command() {
        UUID uuid1 = UUID.randomUUID();

        NameElementCommandConfirm commandConfirm = new NameElementCommandConfirm(uuid1);
        NameElementCommand command = NameElementUtil.convertCommandConfirm2Command(commandConfirm);

        assertNotNull(command);
        assertEquals(uuid1, command.getUuid());
        assertNull(command.getParentSystemStructure());
        assertNull(command.getParentDeviceStructure());
        assertNull(command.getIndex());
        assertNull(command.getDescription());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert name element command to name element command for create.
     */
    @Test
    void convertCommand2CommandCreate() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        String str1 = "qwer";
        String str2 = "asdf";

        NameElementCommand command = new NameElementCommand(uuid1, uuid2, uuid3, str1, str2);
        NameElementCommandCreate commandCreate = NameElementUtil.convertCommand2CommandCreate(command);

        assertNotNull(commandCreate);
        assertEquals(uuid2, commandCreate.getParentSystemStructure());
        assertEquals(uuid3, commandCreate.getParentDeviceStructure());
        assertEquals(str1, commandCreate.getIndex());
        assertEquals(str2, commandCreate.getDescription());

        NameElementCommandCreate[] commandCreate2 = NameElementUtil.convertCommand2CommandCreate(new NameElementCommand[] {command});

        assertNotNull(commandCreate2);
        assertEquals(1, commandCreate2.length);
        assertNotNull(commandCreate2[0]);
        assertEquals(uuid2, commandCreate2[0].getParentSystemStructure());
        assertEquals(uuid3, commandCreate2[0].getParentDeviceStructure());
        assertEquals(str1, commandCreate2[0].getIndex());
        assertEquals(str2, commandCreate2[0].getDescription());
    }

    /**
     * Test of convert name element command to name element command for update.
     */
    @Test
    void convertCommand2CommandUpdate() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        String str1 = "qwer";
        String str2 = "asdf";

        NameElementCommand command = new NameElementCommand(uuid1, uuid2, uuid3, str1, str2);
        NameElementCommandUpdate commandUpdate = NameElementUtil.convertCommand2CommandUpdate(command);

        assertNotNull(commandUpdate);
        assertEquals(uuid1, commandUpdate.getUuid());
        assertEquals(uuid2, commandUpdate.getParentSystemStructure());
        assertEquals(uuid3, commandUpdate.getParentDeviceStructure());
        assertEquals(str1, commandUpdate.getIndex());
        assertEquals(str2, commandUpdate.getDescription());

        NameElementCommandUpdate[] commandUpdate2 = NameElementUtil.convertCommand2CommandUpdate(new NameElementCommand[] {command});

        assertNotNull(commandUpdate2);
        assertEquals(1, commandUpdate2.length);
        assertNotNull(commandUpdate2[0]);
        assertEquals(uuid1, commandUpdate2[0].getUuid());
        assertEquals(uuid2, commandUpdate2[0].getParentSystemStructure());
        assertEquals(uuid3, commandUpdate2[0].getParentDeviceStructure());
        assertEquals(str1, commandUpdate2[0].getIndex());
        assertEquals(str2, commandUpdate2[0].getDescription());
    }

    /**
     * Test of convert name element command to name element command for confirm.
     */
    @Test
    void convertCommand2CommandConfirm() {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        UUID uuid3 = UUID.randomUUID();
        String str1 = "qwer";
        String str2 = "asdf";

        NameElementCommand command = new NameElementCommand(uuid1, uuid2, uuid3, str1, str2);
        NameElementCommandConfirm commandConfirm = NameElementUtil.convertCommand2CommandConfirm(command);

        assertNotNull(commandConfirm);
        assertEquals(uuid1, commandConfirm.getUuid());

        NameElementCommandConfirm[] commandConfirm2 = NameElementUtil.convertCommand2CommandConfirm(new NameElementCommand[] {command});

        assertNotNull(commandConfirm2);
        assertEquals(1, commandConfirm2.length);
        assertNotNull(commandConfirm2[0]);
        assertEquals(uuid1, commandConfirm2[0].getUuid());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert name element to name element command for create.
     */
    @Test
    void convertElement2CommandCreate() {
        UUID uuid = UUID.randomUUID();
        UUID parentSystemStructure =  UUID.randomUUID();
        UUID parentDeviceStructure = UUID.randomUUID();
        String index = "ty";
        String description = "op";
        String systemStructure = "qw";
        String deviceStructure = "er";
        String name = "ui";
        Status status = Status.APPROVED;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "as";
        String comment = "df";

        NameElement element = NameElementUtil.getNameElement(
                uuid, parentSystemStructure, parentDeviceStructure,
                index, description,
                systemStructure, deviceStructure, name,
                status, deleted,
                when, who, comment);

        NameElementCommandCreate commandCreate = NameElementUtil.convertElement2CommandCreate(element);

        assertNotNull(commandCreate);
        assertEquals(parentSystemStructure, commandCreate.getParentSystemStructure());
        assertEquals(parentDeviceStructure, commandCreate.getParentDeviceStructure());
        assertEquals(index, commandCreate.getIndex());
        assertEquals(description, commandCreate.getDescription());

        NameElementCommandCreate[] commandCreate2 = NameElementUtil.convertElement2CommandCreate(new NameElement[] {element});

        assertNotNull(commandCreate2);
        assertEquals(1, commandCreate2.length);
        assertNotNull(commandCreate2[0]);
        assertEquals(parentSystemStructure, commandCreate2[0].getParentSystemStructure());
        assertEquals(parentDeviceStructure, commandCreate2[0].getParentDeviceStructure());
        assertEquals(index, commandCreate2[0].getIndex());
        assertEquals(description, commandCreate2[0].getDescription());
    }

    /**
     * Test of convert name element to name element command for update.
     */
    @Test
    void convertElement2CommandUpdate() {
        UUID uuid = UUID.randomUUID();
        UUID parentSystemStructure =  UUID.randomUUID();
        UUID parentDeviceStructure = UUID.randomUUID();
        String index = "ty";
        String description = "op";
        String systemStructure = "qw";
        String deviceStructure = "er";
        String name = "ui";
        Status status = Status.APPROVED;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "as";
        String comment = "df";

        NameElement element = NameElementUtil.getNameElement(
                uuid, parentSystemStructure, parentDeviceStructure,
                index, description,
                systemStructure, deviceStructure, name,
                status, deleted,
                when, who, comment);

        NameElementCommandUpdate commandUpdate = NameElementUtil.convertElement2CommandUpdate(element);

        assertNotNull(commandUpdate);
        assertEquals(uuid, commandUpdate.getUuid());
        assertEquals(parentSystemStructure, commandUpdate.getParentSystemStructure());
        assertEquals(parentDeviceStructure, commandUpdate.getParentDeviceStructure());
        assertEquals(index, commandUpdate.getIndex());
        assertEquals(description, commandUpdate.getDescription());

        NameElementCommandUpdate[] commandUpdate2 = NameElementUtil.convertElement2CommandUpdate(new NameElement[] {element});

        assertNotNull(commandUpdate2);
        assertEquals(1, commandUpdate2.length);
        assertNotNull(commandUpdate2[0]);
        assertEquals(uuid, commandUpdate2[0].getUuid());
        assertEquals(parentSystemStructure, commandUpdate2[0].getParentSystemStructure());
        assertEquals(parentDeviceStructure, commandUpdate2[0].getParentDeviceStructure());
        assertEquals(index, commandUpdate2[0].getIndex());
        assertEquals(description, commandUpdate2[0].getDescription());
    }

    /**
     * Test of convert name element to name element command for confirm.
     */
    @Test
    void convertElement2CommandConfirm() {
        UUID uuid = UUID.randomUUID();
        UUID parentSystemStructure =  UUID.randomUUID();
        UUID parentDeviceStructure = UUID.randomUUID();
        String index = "ty";
        String description = "op";
        String systemStructure = "qw";
        String deviceStructure = "er";
        String name = "ui";
        Status status = Status.APPROVED;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "as";
        String comment = "df";

        NameElement element = NameElementUtil.getNameElement(
                uuid, parentSystemStructure, parentDeviceStructure,
                index, description,
                systemStructure, deviceStructure, name,
                status, deleted,
                when, who, comment);

        NameElementCommandConfirm commandConfirm = NameElementUtil.convertElement2CommandConfirm(element);

        assertNotNull(commandConfirm);
        assertEquals(uuid, commandConfirm.getUuid());

        NameElementCommandConfirm[] commandConfirm2 = NameElementUtil.convertElement2CommandConfirm(new NameElement[] {element});

        assertNotNull(commandConfirm2);
        assertEquals(1, commandConfirm2.length);
        assertNotNull(commandConfirm2[0]);
        assertEquals(uuid, commandConfirm2[0].getUuid());
    }

}

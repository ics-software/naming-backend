/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Purpose to test URLUtility class.
 *
 * @author Lars Johansson
 */
class URLUtilityTest {

    /**
     * Test for encoding a URL.
     *
     * @param input value to be encoded
     * @param expected expected encoded value
     */
    @ParameterizedTest
    @CsvSource(value = {
            ",",
            "'',''",
            "' ',+",
            "'%',%25",
            "'&',%26",
            "'?',?",
            "abcd1234,abcd1234",
            "abcd.xhtml,abcd.xhtml",
            "abcd.xhtml?efgh,abcd.xhtml?efgh",
            "/abcd.xhtml?uvwx-0%,/abcd.xhtml?uvwx-0%25",
            "/uvwx-0%?efgh=ijkl,/uvwx-0%25?efgh=ijkl",
            "abcd.xhtml?efg+h,abcd.xhtml?efg%2Bh",
            "abcd.xhtml?ef gh,abcd.xhtml?ef+gh",
            "abcd.xhtml?efgh=ijkl,abcd.xhtml?efgh=ijkl",
            "abcd.xhtml?efgh=,abcd.xhtml?efgh=",
            "abcd.xhtml?efgh=ijkl mnop+qrst+ uvwx,abcd.xhtml?efgh=ijkl+mnop%2Bqrst%2B+uvwx",
            "abcd.xhtml?efgh=ijkl&mnop=qrst,abcd.xhtml?efgh=ijkl&mnop=qrst",
            "abcd.xhtml?efgh=i+kl&mnop=qr t,abcd.xhtml?efgh=i%2Bkl&mnop=qr+t",
            "abcd.xhtml?efgh&uvwx=yzabc  def++ghijklm+ nop&abcdefgh=,abcd.xhtml?efgh&uvwx=yzabc++def%2B%2Bghijklm%2B+nop&abcdefgh=",
            "http://127.0.0.1:8080/a/b/c/d/e/f/RFQ-010:EMR-FS-0%,http://127.0.0.1:8080/a/b/c/d/e/f/RFQ-010%3AEMR-FS-0%25",
            "http://127.0.0.1:8080/api/v1/structures?mnemonicPath=Sg-A%,http://127.0.0.1:8080/api/v1/structures?mnemonicPath=Sg-A%25",
            "http://127.0.0.1:8080/api/v1/structures?mnemonicPath=Sg-A%&deleted=false,http://127.0.0.1:8080/api/v1/structures?mnemonicPath=Sg-A%25&deleted=false"
    })
    void encodeURL(String input, String expected) {
        assertEquals(expected, URLUtility.encodeURL(input));
    }

}

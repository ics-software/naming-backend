/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Unit tests for NamingUtil class.
 *
 * @author Lars Johansson
 *
 * @see NameUtil
 */
class NameUtilTest {

    /**
     * Test of get convention name for system and device structure mnemonic paths and index.
     */
    @ParameterizedTest
    @CsvSource(value = {
            "  ,  ,  ,  ",
            "'','','',  ",
            "a, , ,a",
            "a,b, ,a",
            "a, ,c,a",
            " ,b,c, ",
            " , ,c, ",
            "a,b,c,a:b-c"
    })
    void getName(String systemStructure, String deviceStructure, String index, String expected) {
        assertEquals(expected, NameUtil.getName(systemStructure, deviceStructure, index));
    }

}

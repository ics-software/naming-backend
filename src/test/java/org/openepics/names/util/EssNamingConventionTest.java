/*
* Copyright (c) 2014 European Spallation Source
* Copyright (c) 2014 Cosylab d.d.
*
* This file is part of Naming Service.
* Naming Service is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or any newer version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
*/

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;

/**
 * Unit tests for EssNamingConvention class.
 *
 * @author Karin Rathsman
 * @author Lars Johansson
 *
 * @see NamingConvention
 * @see EssNamingConvention
 */
public class EssNamingConventionTest {

    // ----------------------------------------------------------------------------------------------------
    // table of content
    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     validateMnemonic
    //         system structure
    //         device structure
    //     validateMnemonic (2)
    //         system structure
    //         device structure
    //     isInstanceIndexValid
    //         device
    //     isInstanceIndexValid (2)
    //         device
    //     equivalenceClassRepresentative
    //     conventionName
    // name in relation to other
    //     canMnemonicsCoexist
    //         system structure
    //         device structure
    //         system structure & device structure
    //     canMnemonicsCoexist (2)
    //         system structure
    //         device structure
    //         system structure & device structure
    // ----------------------------------------------------------------------------------------------------

    private static final String EMPTY = "";
    private static final String NULL  = null;
    private static final String SPACE = " ";

    private static final String A = "A";

    private static final String SUP = "Sup";
    private static final String SYS = "Sys";
    private static final String SUB = "Sub";
    private static final String DIS = "Dis";
    private static final String DEV = "Dev";
    private static final String IDX = "Idx";
    private static final String GRP = "Grp";

    private static final String SYS_COLON     = "Sys:";
    private static final String SYS_COLON_S3  = "Sys:   ";

    private static final String ECAT10 = "ECAT10";
    private static final String IOC    = "IOC";
    private static final String TDS    = "TDS";
    private static final String TD180  = "TD180";

    private static final String NUM_0         = "0";
    private static final String NUM_1         = "1";
    private static final String NUM_12        = "12";
    private static final String NUM_123       = "123";
    private static final String NUM_1234      = "1234";
    private static final String NUM_12345     = "12345";
    private static final String NUM_123456    = "123456";
    private static final String NUM_1234567   = "1234567";
    private static final String NUM_12345678  = "12345678";
    private static final String NUM_123456789 = "123456789";

    private static final String ALPHABETIC_NOT_OK       = "alphabetic value not allowed";
    private static final String BLANKS_NOT_OK           = "value with blanks not allowed";
    private static final String EMPTY_NOT_OK            = "empty value not allowed";
    private static final String LENGTH_NOT_OK           = "length of value not allowed";
    private static final String LENGTH_OK               = "length of value allowed";
    private static final String NON_ALPHANUMERIC_NOT_OK = "non-alphanumeric value not allowed";
    private static final String NUMERIC_OK              = "numeric value allowed";
    private static final String NULL_NOT_OK             = "null value not allowed";
    private static final String ONLY_ZEROS_NOT_OK       = "value with only zeros not allowed";

    private static EssNamingConvention namingConvention;

    /**
     * One-time initialization code.
     */
    @BeforeAll
    public static void oneTimeSetUp() {
        namingConvention = new EssNamingConvention();
    }

    /**
     * One-time cleanup code.
     */
    @AfterAll
    public static void oneTimeTearDown() {
        namingConvention = null;
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     equivalenceClassRepresentative
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of equivalence class representative method.
     */
    @Test
    void equivalenceClassRepresentative() {
        assertEquals("RFQ-10:EMR-TT-1",  namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-001"));
        assertEquals("RFQ-10:EMR-TT-10", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-010"));
        assertEquals("RFQ-10:EMR-TT-11", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-011"));
        assertEquals("RFQ-10:EMR-TT-12", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-012"));
        assertEquals("RFQ-10:EMR-TT-13", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-013"));
        assertEquals("RFQ-10:EMR-TT-14", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-014"));
        assertEquals("RFQ-10:EMR-TT-15", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-015"));
        assertEquals("RFQ-10:EMR-TT-16", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-016"));
        assertEquals("RFQ-10:EMR-TT-17", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-017"));
        assertEquals("RFQ-10:EMR-TT-18", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-018"));
        assertEquals("RFQ-10:EMR-TT-19", namingConvention.equivalenceClassRepresentative("RFQ-010:EMR-TT-019"));

        assertEquals(
                "PB1-FPM1:CTR1-ECAT-100",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECAT-100"));
        assertEquals(
                "PB1-FPM1:CTR1-ECATC-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATC-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECATE-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATE-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-101"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-102",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-102"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-103",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-103"));
        assertEquals(
                "PB1-FPM1:CTR1-ECAT10-104",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-ECATIO-104"));
        assertEquals(
                "PB1-FPM1:CTR1-EVR-101",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-EVR-101"));
        assertEquals(
                "PB1-FPM1:CTR1-EVR-201",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-EVR-201"));
        assertEquals(
                "PB1-FPM1:CTR1-1PC-100",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-IPC-100"));
        assertEquals(
                "PB1-FPM1:CTR1-1PC-200",
                namingConvention.equivalenceClassRepresentative("PBI-FPM01:Ctrl-IPC-200"));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>1</tt>.
     */
    @Test
    void equivalenceClassRepresentativeSimilarTo1() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative(NUM_1));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("I"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("l"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("L"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("i"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative(NUM_1),
                namingConvention.equivalenceClassRepresentative("b"));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>0</tt>.
     */
    @Test
    void equivalenceClassRepresentativeSimilarTo0() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative("o"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative("O"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative(NUM_0));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative(NUM_0),
                namingConvention.equivalenceClassRepresentative("b"));
    }

    /**
     * Test of equivalence class representative method for similarity to <tt>V</tt>.
     */
    @Test
    void equivalenceClassRepresentativeSimilarToV() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("v"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("V"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative("V1"),
                namingConvention.equivalenceClassRepresentative("w1"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative("V1"),
                namingConvention.equivalenceClassRepresentative("W1"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("w"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("W"));
        assertNotEquals(
                namingConvention.equivalenceClassRepresentative("V"),
                namingConvention.equivalenceClassRepresentative("b"));
    }

    /**
     * Test of equivalence class representative method for lower and upper case characters.
     */
    @Test
    void equivalenceClassRepresentativeLowerAndUpperCaseCharacters() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("tEsTS"),
                namingConvention.equivalenceClassRepresentative("TeSts"));
    }

    /**
     * Test of equivalence class representative method for zero prefixed number.
     */
    @Test
    void equivalenceClassRepresentativeZeroPrefixedNumber() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("01"),
                namingConvention.equivalenceClassRepresentative("001"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("zero01"),
                namingConvention.equivalenceClassRepresentative("zero1"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("ze01ro"),
                namingConvention.equivalenceClassRepresentative("ze1ro"));
    }

    /**
     * Test of equivalence class representative method for zero after alpha character.
     */
    @Test
    void equivalenceClassRepresentativeZeroAfterAlphaCharacter() {
        assertEquals(
                namingConvention.equivalenceClassRepresentative("Sub0001"),
                namingConvention.equivalenceClassRepresentative("Sub1"));
        assertEquals(
                namingConvention.equivalenceClassRepresentative("01Sub001"),
                namingConvention.equivalenceClassRepresentative("01Sub1"));
    }

    /**
     * Test of equivalence class representative method for chained calls to get equivalence class.
     */
    @Test
    void equivalenceClassRepresentativeTwiceEquivalence() {
        // note that of 2 chained calls to equivalenceClassRepresentative, 2nd call may modify
        // <==> 2nd output not same as 1st output
        assertEquals(
                "CRS-T1CP:CRY0-XZ-XXXXX1",
                namingConvention.equivalenceClassRepresentative("CrS-TICP:Cryo-XZ-XXXXX1"));
        assertEquals(
                "CRS-T1CP:CRY-XZ-XXXXX1",
                namingConvention.equivalenceClassRepresentative("CRS-T1CP:CRY0-XZ-XXXXX1"));
        assertEquals("CRY0",   namingConvention.equivalenceClassRepresentative("Cryo"));
        assertEquals("CRY",    namingConvention.equivalenceClassRepresentative("CRY0"));
    }

    /**
     * Test of equivalence class representative method for device type.
     */
    @Test
    void equivalenceClassRepresentativeDeviceType() {
        // note handling of trailing zeroes
        assertEquals(ECAT10,   namingConvention.equivalenceClassRepresentative("ECATIO"));
        assertEquals("ECAT1",  namingConvention.equivalenceClassRepresentative("ECATI0"));
        assertEquals(ECAT10,   namingConvention.equivalenceClassRepresentative(ECAT10));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     isInstanceIndexValid
    //         device
    //         device override
    //         device PID
    //         device PID override
    // ----------------------------------------------------------------------------------------------------

    /**
     * Test validity of instance index for device.
     */
    @Test
    void isInstanceIndexValid() {
        String conventionNameBase = SYS + "-" + SUB + ":" + DIS + "-" + DEV;

        assertFalse(namingConvention.isInstanceIndexValid(NULL),                                   NULL_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase),                     EMPTY_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + EMPTY),       EMPTY_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + SPACE),       BLANKS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + IDX),         ALPHABETIC_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abcdef"),    ALPHABETIC_NOT_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123),     NUMERIC_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0"),         ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00"),        ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000"),       ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000"),      ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00000"),     ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000000"),    ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000000"),   ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abc123"),    ALPHABETIC_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "a!"),        NON_ALPHANUMERIC_NOT_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "01"),        LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "001"),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0001"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0110"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "10"),        LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "100"),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "1000"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123),     LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234),    LENGTH_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12345),   LENGTH_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123456),  LENGTH_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234567), LENGTH_NOT_OK);

        conventionNameBase = SUP + "-" + SYS + "-" + SUB + ":" + DIS + "-" + DEV;

        assertFalse(namingConvention.isInstanceIndexValid(NULL),                                   NULL_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase),                     EMPTY_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + EMPTY),       EMPTY_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + SPACE),       BLANKS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + IDX),         ALPHABETIC_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abcdef"),    ALPHABETIC_NOT_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123),     NUMERIC_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0"),         ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00"),        ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000"),       ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000"),      ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00000"),     ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000000"),    ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000000"),   ONLY_ZEROS_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abc123"),    ALPHABETIC_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "a!"),        NON_ALPHANUMERIC_NOT_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "01"),        LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "001"),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0001"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0110"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "10"),        LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "100"),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "1000"),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1),       LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12),      LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123),     LENGTH_OK);
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234),    LENGTH_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12345),   LENGTH_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123456),  LENGTH_NOT_OK);
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234567), LENGTH_NOT_OK);
    }

    /**
     * Test validity of instance index for device.
     */
    @Test
    void isInstanceIndexValidOverride() {
        // override - length, characters

        String conventionNameBase = SYS + "-" + SUB + ":" + DIS + "-" + DEV;

        assertFalse(namingConvention.isInstanceIndexValid(NULL,                                   true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase,                     true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + EMPTY,       true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + SPACE,       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + IDX,         true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abcdef",    true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123,     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0",         true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00000",     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000000",    true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abc123",    true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "a!",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "01",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "001",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0001",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0110",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "10",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "100",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "1000",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1,       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12,      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123,     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234,    true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12345,   true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123456,  true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234567, true));

        conventionNameBase = SUP + "-" + SYS + "-" + SUB + ":" + DIS + "-" + DEV;

        assertFalse(namingConvention.isInstanceIndexValid(NULL,                                   true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase,                     true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + EMPTY,       true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + SPACE,       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + IDX,         true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abcdef",    true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123,     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0",         true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "00000",     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "000000",    true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0000000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "abc123",    true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "a!",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "01",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "001",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0001",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "0110",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "10",        true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "100",       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + "1000",      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1,       true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12,      true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123,     true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234,    true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_12345,   true));
        assertTrue (namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_123456,  true));
        assertFalse(namingConvention.isInstanceIndexValid(conventionNameBase + "-" + NUM_1234567, true));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    void isInstanceIndexValidPIDNumeric() {
        // depend on discipline, device type

        // mnemonic path system structure
        final String systemStructure  = TDS + "-" + TD180;

        // mnemonic path device structure
        final String deviceStructure1 = "Mech" + "-" + IOC;
        final String deviceStructure2 = "Cryo" + "-" + IOC;
        final String deviceStructure3 = "EMR"  + "-" + IOC;
        final String deviceStructure4 = "HVAC" + "-" + IOC;
        final String deviceStructure5 = "Proc" + "-" + IOC;
        final String deviceStructure6 = "SC"   + "-" + IOC;
        final String deviceStructure7 = "Vac"  + "-" + IOC;
        final String deviceStructure8 = "WtrC" + "-" + IOC;

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "000A"));

        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "0001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "0110"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "10"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + "1000"));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001abcd));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure1 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure2 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure3 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure4 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure5 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure6 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure7 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure + ":" + deviceStructure8 + "-" + _001ABCD));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    void isInstanceIndexValidPID() {
        // depend on discipline

        // mnemonic paths system structure and device structure
        final String systemStructure1  = A + "-" + A + "-" + A;
        final String deviceStructure1  = A + "-" + A;

        // mnemonic paths system structure and device structure
        final String systemStructure2  = TDS + "-" + TD180;
        final String deviceStructure21 = "Mech" + "-" + TDS;
        final String deviceStructure22 = "Cryo" + "-" + TDS;
        final String deviceStructure23 = "EMR"  + "-" + TDS;
        final String deviceStructure24 = "HVAC" + "-" + TDS;
        final String deviceStructure25 = "Proc" + "-" + TDS;
        final String deviceStructure26 = "SC"   + "-" + TDS;
        final String deviceStructure27 = "Vac"  + "-" + TDS;
        final String deviceStructure28 = "WtrC" + "-" + TDS;

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "00"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0000"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "1001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "10001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "abc"));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000a"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000a"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000A"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000A"));

        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "1"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "1"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "01"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "01"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "0001"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "0001"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "0110"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "0110"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "10"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "10"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "100"));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "1000"));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "1000"));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001a));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001a));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ab));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ab));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001abc));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001abc));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001abcd));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001abcd));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001A));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001AB));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ABC));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ABCD));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ABCD));
    }

    /**
     * Test validity of instance index, for device.
     */
    @Test
    void isInstanceIndexValidPIDOverride() {
        // depend on discipline
        // override - length, characters

        // mnemonic paths system structure and device structure
        final String systemStructure1  = A + "-" + A + "-" + A;
        final String deviceStructure1  = A + "-" + A;

        // mnemonic paths system structure and device structure
        final String systemStructure2  = TDS + "-" + TD180;
        final String deviceStructure21 = "Mech" + "-" + TDS;
        final String deviceStructure22 = "Cryo" + "-" + TDS;
        final String deviceStructure23 = "EMR"  + "-" + TDS;
        final String deviceStructure24 = "HVAC" + "-" + TDS;
        final String deviceStructure25 = "Proc" + "-" + TDS;
        final String deviceStructure26 = "SC"   + "-" + TDS;
        final String deviceStructure27 = "Vac"  + "-" + TDS;
        final String deviceStructure28 = "WtrC" + "-" + TDS;

        final String _001a    = "001a";
        final String _001ab   = "001ab";
        final String _001abc  = "001abc";
        final String _001abcd = "001abcd";
        final String _001A    = "001A";
        final String _001AB   = "001AB";
        final String _001ABC  = "001ABC";
        final String _001ABCD = "001ABCD";

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1,                   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0",       true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "00",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "000",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "1",       true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "01",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "001",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "001",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "0001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "1001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "10001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure1 + ":" + deviceStructure1 + "-" + "abc",     true));

        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27,                  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28,                  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000a",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "000A",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "000A",   true));

        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "1",      true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "01",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "001",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "0001",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "0110",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "10",     true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "100",    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + "1000",   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + "1000",   true));

        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001a,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ab,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001abc,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001abc,  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001abcd, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001abcd, true));

        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001A,    true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001AB,   true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ABC,  true));
        assertTrue (namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ABC,  true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure21 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure22 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure23 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure24 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure25 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure26 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure27 + "-" + _001ABCD, true));
        assertFalse(namingConvention.isInstanceIndexValid(systemStructure2 + ":" + deviceStructure28 + "-" + _001ABCD, true));
    }

    /**
     * Test validity of mnemonic path.
     */
    @Test
    void isMnemonicPathValid() {
        assertFalse(namingConvention.isMnemonicPathValid(NULL));
        assertFalse(namingConvention.isMnemonicPathValid(EMPTY));
        assertFalse(namingConvention.isMnemonicPathValid(SPACE));

        assertTrue (namingConvention.isMnemonicPathValid("A"));
        assertTrue (namingConvention.isMnemonicPathValid("A-B"));
        assertTrue (namingConvention.isMnemonicPathValid("A-B-C"));
        assertFalse(namingConvention.isMnemonicPathValid("A-B-C-D"));

        assertFalse(namingConvention.isMnemonicPathValid("A-A"));
        assertFalse(namingConvention.isMnemonicPathValid("A-A-A"));
        assertFalse(namingConvention.isMnemonicPathValid("A-A-B"));
        assertFalse(namingConvention.isMnemonicPathValid("A-B-A"));
        assertFalse(namingConvention.isMnemonicPathValid("B-A-A"));
    }

    /**
     * Test if mnemonic is required.
     */
    @Test
    void isMnemonicRequired() {
        assertFalse(namingConvention.isMnemonicRequired(null));

        assertFalse(namingConvention.isMnemonicRequired(Type.SYSTEMGROUP));
        assertTrue (namingConvention.isMnemonicRequired(Type.SYSTEM));
        assertTrue (namingConvention.isMnemonicRequired(Type.SUBSYSTEM));

        assertTrue (namingConvention.isMnemonicRequired(Type.DISCIPLINE));
        assertFalse(namingConvention.isMnemonicRequired(Type.DEVICEGROUP));
        assertTrue (namingConvention.isMnemonicRequired(Type.DEVICETYPE));
    }

    // ----------------------------------------------------------------------------------------------------
    // name by itself
    //     validateMnemonic
    //         system structure
    //         device structure
    // ----------------------------------------------------------------------------------------------------

    // empty
    // blank
    // alphabetic
    // numeric
    // alphanumeric
    // non-alphanumeric
    // length
    // mix

    /**
     * Test validity of mnemonic for system group.
     */
    @Test
    void isSystemGroupValid(){
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NULL));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup("Sys "));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(SYS));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup("000"));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup("Sys0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystemGroup(SYS_COLON));
        assertEquals(MnemonicValidation.TOO_LONG,             testSystemGroup(SYS_COLON_S3));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_1));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_12));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_1234));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_12345));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup(NUM_123456));
        assertEquals(MnemonicValidation.TOO_LONG,             testSystemGroup(NUM_1234567));

        // other
        assertEquals(MnemonicValidation.VALID,                testSystemGroup("Ac1"));
        assertEquals(MnemonicValidation.VALID,                testSystemGroup("Acc"));
    }

    /**
     * Test validity of mnemonic for system.
     */
    @Test
    void isSystemValid() {
        assertEquals(MnemonicValidation.EMPTY,                testSystem(NULL));
        assertEquals(MnemonicValidation.EMPTY,                testSystem(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem("Sys "));
        assertEquals(MnemonicValidation.VALID,                testSystem(SYS));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSystem("000"));
        assertEquals(MnemonicValidation.VALID,                testSystem("Sys0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SYS_COLON));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSystem(SYS_COLON_S3));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_1));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_12));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_1234));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_12345));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_123456));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_1234567));
        assertEquals(MnemonicValidation.VALID,                testSystem(NUM_12345678));
        assertEquals(MnemonicValidation.TOO_LONG,             testSystem(NUM_123456789));
    }

    /**
     * Test validity of mnemonic for subsystem.
     */
    @Test
    void isSubsystemValid() {
        assertEquals(MnemonicValidation.EMPTY,                testSubsystem(NULL));
        assertEquals(MnemonicValidation.EMPTY,                testSubsystem(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem("Sub "));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(SUB));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSubsystem("000"));
        assertEquals(MnemonicValidation.VALID,                testSubsystem("Sub0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testSubsystem("Sub:"));
        assertEquals(MnemonicValidation.TOO_LONG,             testSubsystem("Sub:     "));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_1));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_12));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_1234));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_12345));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_123456));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_1234567));
        assertEquals(MnemonicValidation.VALID,                testSubsystem(NUM_12345678));
        assertEquals(MnemonicValidation.TOO_LONG,             testSubsystem(NUM_123456789));
    }

    /**
     * Test validity of mnemonic for discipline.
     */
    @Test
    void isDisciplineValid() {
        assertEquals(MnemonicValidation.EMPTY,                testDiscipline(NULL));
        assertEquals(MnemonicValidation.EMPTY,                testDiscipline(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline("Dis "));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(DIS));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testDiscipline("000"));
        assertEquals(MnemonicValidation.VALID,                testDiscipline("Dis0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDiscipline("Dis:"));
        assertEquals(MnemonicValidation.TOO_LONG,             testDiscipline("Dis:   "));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_1));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_12));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_1234));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_12345));
        assertEquals(MnemonicValidation.VALID,                testDiscipline(NUM_123456));
        assertEquals(MnemonicValidation.TOO_LONG,             testDiscipline(NUM_1234567));
    }

    /**
     * Test validity of mnemonic for device group.
     */
    @Test
    void isDeviceGroupValid() {
        assertEquals(MnemonicValidation.VALID,                testDeviceGroup(NULL));
        assertEquals(MnemonicValidation.VALID,                testDeviceGroup(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp "));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(GRP));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_123));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("000"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp:"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("Grp:   "));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_1));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_12));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_123));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_1234));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_12345));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup(NUM_123456));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceGroup("123456789012345"));
    }

    /**
     * Test validity of mnemonic for device type.
     */
    @Test
    void isDeviceTypeValid() {
        assertEquals(MnemonicValidation.EMPTY,                testDeviceType(NULL));
        assertEquals(MnemonicValidation.EMPTY,                testDeviceType(EMPTY));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType(SPACE));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType("Dev "));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(DEV));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testDeviceType("000"));
        assertEquals(MnemonicValidation.VALID,                testDeviceType("Dev0"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType(":"));
        assertEquals(MnemonicValidation.NON_ACCEPTABLE_CHARS, testDeviceType("Dev:"));
        assertEquals(MnemonicValidation.TOO_LONG,             testDeviceType("Dev:   "));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_1));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_12));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_123));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_1234));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_12345));
        assertEquals(MnemonicValidation.VALID,                testDeviceType(NUM_123456));
        assertEquals(MnemonicValidation.TOO_LONG,             testDeviceType(NUM_1234567));
    }

    // ----------------------------------------------------------------------------------------------------

    // utility methods
    private MnemonicValidation testSystemGroup(String mnemonic){
        return namingConvention.validateMnemonic(Type.SYSTEMGROUP, mnemonic);
    }
    private MnemonicValidation testSystem(String mnemonic){
        return namingConvention.validateMnemonic(Type.SYSTEM, mnemonic);
    }
    private MnemonicValidation testSubsystem(String mnemonic){
        return namingConvention.validateMnemonic(Type.SUBSYSTEM, mnemonic);
    }
    private MnemonicValidation testDiscipline(String mnemonic){
        return namingConvention.validateMnemonic(Type.DISCIPLINE, mnemonic);
    }
    private MnemonicValidation testDeviceGroup(String mnemonic){
        return namingConvention.validateMnemonic(Type.DEVICEGROUP, mnemonic);
    }
    private MnemonicValidation testDeviceType(String mnemonic){
        return namingConvention.validateMnemonic(Type.DEVICETYPE, mnemonic);
    }

}

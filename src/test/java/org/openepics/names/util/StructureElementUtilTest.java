/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;

/**
 * Unit tests for StructureElementUtil class.
 *
 * @author Lars Johansson
 *
 * @see StructureElementUtil
 */
class StructureElementUtilTest {

    /**
     * Test of get structure element for content.
     */
    @Test
    void getStructureElementContent() {
        UUID uuid = UUID.randomUUID();
        Type type = Type.DEVICETYPE;
        UUID parent = UUID.randomUUID();
        String mnemonic = "jk";
        Integer ordering = 41;
        String description = "cv";
        String mnemonicPath = "zx";
        Integer level = 3;
        Status status = Status.PENDING;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "bn";
        String comment = "ml";

        StructureElement structureElement = StructureElementUtil.getStructureElement(
                uuid, type, parent,
                mnemonic, ordering, description,
                mnemonicPath, level,
                status, deleted,
                when, who, comment);

        assertNotNull(structureElement);
        assertEquals(uuid, structureElement.getUuid());
        assertEquals(type, structureElement.getType());
        assertEquals(parent, structureElement.getParent());
        assertEquals(mnemonic, structureElement.getMnemonic());
        assertEquals(mnemonicPath, structureElement.getMnemonicPath());
        assertEquals(ordering, structureElement.getOrdering());
        assertEquals(level, structureElement.getLevel());
        assertEquals(description, structureElement.getDescription());
        assertEquals(status, structureElement.getStatus());
        assertEquals(deleted, structureElement.isDeleted());
        assertEquals(when, structureElement.getWhen());
        assertEquals(who, structureElement.getWho());
        assertEquals(comment, structureElement.getComment());
    }

    // ----------------------------------------------------------------------------------------------------

    @Test
    void equals() {
        StructureElement se1 = new StructureElement();
        StructureElement se2 = new StructureElement();

        //        Field[] fields = se1.getClass().getFields();
        //        Field[] declaredFields = se1.getClass().getDeclaredFields();
        //
        //        Field[] superclass_fields = se1.getClass().getSuperclass().getFields();
        //        Field[] superclass_declaredFields = se1.getClass().getSuperclass().getDeclaredFields();
        //
        //        for (Field field : fields) {
        //            System.out.println("fields.field.getName:                    " + field.getName());
        //        }
        //        System.out.println("-----------------------------------------");
        //        for (Field field : declaredFields) {
        //            System.out.println("declaredFields.field.getName:            " + field.getName());
        //        }
        //        System.out.println("-----------------------------------------");
        //        for (Field field : superclass_fields) {
        //            System.out.println("superclass_fields.field.getName:         " + field.getName());
        //        }
        //        System.out.println("-----------------------------------------");
        //        for (Field field : superclass_declaredFields) {
        //            System.out.println("superclass_declaredFields.field.getName: " + field.getName());
        //        }

        assertTrue(se1.equals(se2));
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert structure element command for create to structure element command.
     */
    @Test
    void convertCommandCreate2Command() {
        Type type = Type.SUBSYSTEM;
        UUID uuid2 = UUID.randomUUID();
        String str2 = "asdf";
        String str3 = "zxcv";
        Integer int1 = 41;

        StructureElementCommandCreate commandCreate = new StructureElementCommandCreate(type, uuid2, str2, int1, str3);
        StructureElementCommand command = StructureElementUtil.convertCommandCreate2Command(commandCreate);

        assertNull(command.getUuid());
        assertEquals(type, command.getType());
        assertEquals(uuid2, command.getParent());
        assertEquals(str2, command.getMnemonic());
        assertEquals(int1, command.getOrdering());
        assertEquals(str3, command.getDescription());
    }

    /**
     * Test of convert structure element command for update to structure element command.
     */
    @Test
    void convertCommandUpdate2Command() {
        UUID uuid1 = UUID.randomUUID();
        Type type = Type.SUBSYSTEM;
        UUID uuid2 = UUID.randomUUID();
        String str2 = "asdf";
        String str3 = "zxcv";
        Integer int1 = 41;

        StructureElementCommandUpdate commandUpdate = new StructureElementCommandUpdate(uuid1, type, uuid2, str2, int1, str3);
        StructureElementCommand command = StructureElementUtil.convertCommandUpdate2Command(commandUpdate);

        assertEquals(uuid1, command.getUuid());
        assertEquals(type, command.getType());
        assertEquals(uuid2, command.getParent());
        assertEquals(str2, command.getMnemonic());
        assertEquals(int1, command.getOrdering());
        assertEquals(str3, command.getDescription());
    }

    /**
     * Test of convert structure element command for confirm to structure element command.
     */
    @Test
    void convertCommandConfirm2Command() {
        UUID uuid1 = UUID.randomUUID();
        Type type = Type.SUBSYSTEM;

        StructureElementCommandConfirm commandConfirm = new StructureElementCommandConfirm(uuid1, type);
        StructureElementCommand command = StructureElementUtil.convertCommandConfirm2Command(commandConfirm);

        assertEquals(uuid1, command.getUuid());
        assertEquals(type, command.getType());
        assertNull(command.getParent());
        assertNull(command.getMnemonic());
        assertNull(command.getOrdering());
        assertNull(command.getDescription());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert structure element command to structure element command for create.
     */
    @Test
    void convertCommand2CommandCreate() {
        UUID uuid1 = UUID.randomUUID();
        Type type = Type.SUBSYSTEM;
        UUID uuid2 = UUID.randomUUID();
        String str2 = "asdf";
        String str3 = "zxcv";
        Integer int1 = 41;

        StructureElementCommand command = new StructureElementCommand(uuid1, type, uuid2, str2, int1, str3);
        StructureElementCommandCreate commandCreate = StructureElementUtil.convertCommand2CommandCreate(command);

        assertNotNull(commandCreate);
        assertEquals(type, commandCreate.getType());
        assertEquals(uuid2, commandCreate.getParent());
        assertEquals(str2, commandCreate.getMnemonic());
        assertEquals(int1, command.getOrdering());
        assertEquals(str3, commandCreate.getDescription());

        StructureElementCommandCreate[] commandCreate2 = StructureElementUtil.convertCommand2CommandCreate(new StructureElementCommand[] {command});

        assertNotNull(commandCreate2);
        assertEquals(1, commandCreate2.length);
        assertNotNull(commandCreate2[0]);
        assertEquals(type, commandCreate2[0].getType());
        assertEquals(uuid2, commandCreate2[0].getParent());
        assertEquals(str2, commandCreate2[0].getMnemonic());
        assertEquals(int1, commandCreate2[0].getOrdering());
        assertEquals(str3, commandCreate2[0].getDescription());
    }

    /**
     * Test of convert structure element command to structure element command for update.
     */
    @Test
    void convertCommand2CommandUpdate() {
        UUID uuid1 = UUID.randomUUID();
        Type type = Type.SUBSYSTEM;
        UUID uuid2 = UUID.randomUUID();
        String str2 = "asdf";
        String str3 = "zxcv";
        Integer int1 = 41;

        StructureElementCommand command = new StructureElementCommand(uuid1, type, uuid2, str2, int1, str3);
        StructureElementCommandUpdate commandUpdate = StructureElementUtil.convertCommand2CommandUpdate(command);

        assertNotNull(commandUpdate);
        assertEquals(uuid1, commandUpdate.getUuid());
        assertEquals(type, commandUpdate.getType());
        assertEquals(uuid2, commandUpdate.getParent());
        assertEquals(str2, commandUpdate.getMnemonic());
        assertEquals(int1, commandUpdate.getOrdering());
        assertEquals(str3, commandUpdate.getDescription());

        StructureElementCommandUpdate[] commandUpdate2 = StructureElementUtil.convertCommand2CommandUpdate(new StructureElementCommand[] {command});

        assertNotNull(commandUpdate2);
        assertEquals(1, commandUpdate2.length);
        assertNotNull(commandUpdate2[0]);
        assertEquals(uuid1, commandUpdate2[0].getUuid());
        assertEquals(type, commandUpdate2[0].getType());
        assertEquals(uuid2, commandUpdate2[0].getParent());
        assertEquals(str2, commandUpdate2[0].getMnemonic());
        assertEquals(int1, commandUpdate2[0].getOrdering());
        assertEquals(str3, commandUpdate2[0].getDescription());
    }

    /**
     * Test of convert structure element command to structure element command for confirm.
     */
    @Test
    void convertCommand2CommandConfirm() {
        UUID uuid1 = UUID.randomUUID();
        Type type = Type.SUBSYSTEM;
        UUID uuid2 = UUID.randomUUID();
        String str2 = "asdf";
        String str3 = "zxcv";
        Integer int1 = 41;

        StructureElementCommand command = new StructureElementCommand(uuid1, type, uuid2, str2, int1, str3);
        StructureElementCommandConfirm commandConfirm = StructureElementUtil.convertCommand2CommandConfirm(command);

        assertNotNull(commandConfirm);
        assertEquals(uuid1, commandConfirm.getUuid());
        assertEquals(type, commandConfirm.getType());

        StructureElementCommandConfirm[] commandConfirm2 = StructureElementUtil.convertCommand2CommandConfirm(new StructureElementCommand[] {command});

        assertNotNull(commandConfirm2);
        assertEquals(1, commandConfirm2.length);
        assertNotNull(commandConfirm2[0]);
        assertEquals(uuid1, commandConfirm2[0].getUuid());
        assertEquals(type, commandConfirm2[0].getType());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of convert structure element to structure element command for create.
     */
    @Test
    void convertElement2CommandCreate() {
        UUID uuid = UUID.randomUUID();
        Type type = Type.DEVICETYPE;
        UUID parent = UUID.randomUUID();
        String mnemonic = "jk";
        Integer ordering = 41;
        String description = "cv";
        String mnemonicPath = "zx";
        Integer level = 3;
        Status status = Status.PENDING;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "bn";
        String comment = "ml";

        StructureElement element = StructureElementUtil.getStructureElement(
                uuid, type, parent,
                mnemonic, ordering, description,
                mnemonicPath, level,
                status, deleted,
                when, who, comment);

        StructureElementCommandCreate commandCreate = StructureElementUtil.convertElement2CommandCreate(element);

        assertNotNull(commandCreate);
        assertEquals(type, commandCreate.getType());
        assertEquals(parent, commandCreate.getParent());
        assertEquals(mnemonic, commandCreate.getMnemonic());
        assertEquals(ordering, commandCreate.getOrdering());
        assertEquals(description, commandCreate.getDescription());

        StructureElementCommandCreate[] commandCreate2 = StructureElementUtil.convertElement2CommandCreate(new StructureElement[] {element});

        assertNotNull(commandCreate2);
        assertEquals(1, commandCreate2.length);
        assertNotNull(commandCreate2[0]);
        assertEquals(type, commandCreate2[0].getType());
        assertEquals(parent, commandCreate2[0].getParent());
        assertEquals(mnemonic, commandCreate2[0].getMnemonic());
        assertEquals(ordering, commandCreate2[0].getOrdering());
        assertEquals(description, commandCreate2[0].getDescription());
    }

    /**
     * Test of convert structure element to structure element command for update.
     */
    @Test
    void convertElement2CommandUpdate() {
        UUID uuid = UUID.randomUUID();
        Type type = Type.DEVICETYPE;
        UUID parent = UUID.randomUUID();
        String mnemonic = "jk";
        Integer ordering = 41;
        String description = "cv";
        String mnemonicPath = "zx";
        Integer level = 3;
        Status status = Status.PENDING;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "bn";
        String comment = "ml";

        StructureElement element = StructureElementUtil.getStructureElement(
                uuid, type, parent,
                mnemonic, ordering, description,
                mnemonicPath, level,
                status, deleted,
                when, who, comment);

        StructureElementCommandUpdate commandUpdate = StructureElementUtil.convertElement2CommandUpdate(element);

        assertNotNull(commandUpdate);
        assertEquals(uuid, commandUpdate.getUuid());
        assertEquals(type, commandUpdate.getType());
        assertEquals(parent, commandUpdate.getParent());
        assertEquals(mnemonic, commandUpdate.getMnemonic());
        assertEquals(ordering, commandUpdate.getOrdering());
        assertEquals(description, commandUpdate.getDescription());

        StructureElementCommandUpdate[] commandUpdate2 = StructureElementUtil.convertElement2CommandUpdate(new StructureElement[] {element});

        assertNotNull(commandUpdate2);
        assertEquals(1, commandUpdate2.length);
        assertNotNull(commandUpdate2[0]);
        assertEquals(uuid, commandUpdate2[0].getUuid());
        assertEquals(type, commandUpdate2[0].getType());
        assertEquals(parent, commandUpdate2[0].getParent());
        assertEquals(mnemonic, commandUpdate2[0].getMnemonic());
        assertEquals(ordering, commandUpdate2[0].getOrdering());
        assertEquals(description, commandUpdate2[0].getDescription());
    }

    /**
     * Test of convert structure element to structure element command for confirm.
     */
    @Test
    void convertElement2CommandConfirm() {
        UUID uuid = UUID.randomUUID();
        Type type = Type.DEVICETYPE;
        UUID parent = UUID.randomUUID();
        String mnemonic = "jk";
        Integer ordering = 41;
        String description = "cv";
        String mnemonicPath = "zx";
        Integer level = 3;
        Status status = Status.PENDING;
        Boolean deleted = Boolean.FALSE;
        Date when = new Date();
        String who = "bn";
        String comment = "ml";

        StructureElement element = StructureElementUtil.getStructureElement(
                uuid, type, parent,
                mnemonic, ordering, description,
                mnemonicPath, level,
                status, deleted,
                when, who, comment);

        StructureElementCommandConfirm commandConfirm = StructureElementUtil.convertElement2CommandConfirm(element);

        assertNotNull(commandConfirm);
        assertEquals(uuid, commandConfirm.getUuid());
        assertEquals(type, commandConfirm.getType());

        StructureElementCommandConfirm[] commandConfirm2 = StructureElementUtil.convertElement2CommandConfirm(new StructureElement[] {element});

        assertNotNull(commandConfirm2);
        assertEquals(1, commandConfirm2.length);
        assertNotNull(commandConfirm2[0]);
        assertEquals(uuid, commandConfirm2[0].getUuid());
        assertEquals(type, commandConfirm2[0].getType());
    }

}

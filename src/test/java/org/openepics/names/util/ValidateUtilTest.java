/*
 * Copyright (c) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.compress.utils.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openepics.names.exception.InputNotAvailableException;
import org.openepics.names.rest.beans.Type;

/**
 * Unit tests for ValidateUtil class.
 *
 * @author Lars Johansson
 *
 * @see ValidateUtil
 */
class ValidateUtilTest {

    /**
     * Test of validate input description.
     */
    @Test
    void validateInputDescriptionNull() {
        try {
            ValidateUtil.validateInputDescription(null);
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals(null, e.getDetails());
            assertEquals("description", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input description.
     */
    @Test
    void validateInputDescriptionEmpty() {
        try {
            ValidateUtil.validateInputDescription("");
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals("", e.getDetails());
            assertEquals("description", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input description.
     */
    @Test
    void validateInputDescription() {
        ValidateUtil.validateInputDescription("asdf");
    }

    /**
     * Test of validate input mnemonic.
     */
    @Test
    void validateInputMnemonicNull() {
        try {
            ValidateUtil.validateInputMnemonic(null);
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals(null, e.getDetails());
            assertEquals("mnemonic", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input mnemonic.
     */
    @Test
    void validateInputMnemonicEmpty() {
        try {
            ValidateUtil.validateInputMnemonic("");
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals("", e.getDetails());
            assertEquals("mnemonic", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input mnemonic.
     */
    @Test
    void validateInputMnemonic() {
        ValidateUtil.validateInputMnemonic("asdf");
    }

    /**
     * Test of validate input name.
     */
    @Test
    void validateInputNameNull() {
        try {
            ValidateUtil.validateInputName(null);
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals(null, e.getDetails());
            assertEquals("name", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input name.
     */
    @Test
    void validateInputNameEmpty() {
        try {
            ValidateUtil.validateInputName("");
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals("", e.getDetails());
            assertEquals("name", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input name.
     */
    @Test
    void validateInputName() {
        ValidateUtil.validateInputName("asdf");
    }

    /**
     * Test of validate input type.
     */
    @Test
    void validateInputTypeNull() {
        try {
            ValidateUtil.validateInputType(null);
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals(null, e.getDetails());
            assertEquals("type", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input type.
     */
    @Test
    void validateInputType() {
        ValidateUtil.validateInputType(Type.SYSTEMGROUP);
        ValidateUtil.validateInputType(Type.SYSTEM);
        ValidateUtil.validateInputType(Type.SUBSYSTEM);
        ValidateUtil.validateInputType(Type.DISCIPLINE);
        ValidateUtil.validateInputType(Type.DEVICEGROUP);
        ValidateUtil.validateInputType(Type.DEVICETYPE);
    }

    /**
     * Test of validate input uuid.
     */
    @Test
    void validateInputUuidNull() {
        try {
            ValidateUtil.validateInputUuid(null);
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals(null, e.getDetails());
            assertEquals("uuid", e.getField());
            assertEquals(null, e.getCause());
        } catch (Exception e) {
            fail();
        }
    }
    /**
     * Test of validate input uuid.
     */
    @Test
    void validateInputUuidEmpty() {
        try {
            ValidateUtil.validateInputUuid("");
            fail();
        } catch (InputNotAvailableException e) {
            assertEquals(TextUtil.VALUE_IS_NOT_AVAILABLE, e.getMessage());
            assertEquals("", e.getDetails());
            assertEquals("uuid", e.getField());
            assertEquals(null, e.getCause());
        }
    }
    /**
     * Test of validate input uuid.
     */
    @Test
    void validateInputUuid() {
        ValidateUtil.validateInputUuid(UUID.randomUUID().toString());
    }

    /**
     * Test of validate collection.
     */
    @Test
    void isSize() {
        assertFalse(ValidateUtil.isSize(null, 0));
        assertFalse(ValidateUtil.isSize(null, 1));
        assertFalse(ValidateUtil.isSize(null, 2));

        List<String> list = new ArrayList<>();

        assertTrue (ValidateUtil.isSize(list, 0));
        assertFalse(ValidateUtil.isSize(list, 1));
        assertFalse(ValidateUtil.isSize(list, 2));

        list.add("a");

        assertFalse(ValidateUtil.isSize(list, 0));
        assertTrue (ValidateUtil.isSize(list, 1));
        assertFalse(ValidateUtil.isSize(list, 2));

        list.add("b");

        assertFalse(ValidateUtil.isSize(list, 0));
        assertFalse(ValidateUtil.isSize(list, 1));
        assertTrue (ValidateUtil.isSize(list, 2));
    }

    /**
     * Test of validate collection.
     */
    @Test
    void isNullOrEmpty() {
        assertTrue (ValidateUtil.isNullOrEmpty(null));

        List<String> list = new ArrayList<>();

        assertTrue (ValidateUtil.isNullOrEmpty(list));

        list.add("a");

        assertFalse(ValidateUtil.isNullOrEmpty(list));

        list.add("b");

        assertFalse(ValidateUtil.isNullOrEmpty(list));
    }

    /**
     * Test of validate collection.
     */
    @Test
    void isNullOrNotSize() {
        assertTrue (ValidateUtil.isNullOrNotSize(null, 0));
        assertTrue (ValidateUtil.isNullOrNotSize(null, 1));
        assertTrue (ValidateUtil.isNullOrNotSize(null, 2));

        List<String> list = new ArrayList<>();

        assertFalse(ValidateUtil.isNullOrNotSize(list, 0));
        assertTrue (ValidateUtil.isNullOrNotSize(list, 1));
        assertTrue (ValidateUtil.isNullOrNotSize(list, 2));

        list.add("a");

        assertTrue (ValidateUtil.isNullOrNotSize(list, 0));
        assertFalse(ValidateUtil.isNullOrNotSize(list, 1));
        assertTrue (ValidateUtil.isNullOrNotSize(list, 2));

        list.add("b");

        assertTrue (ValidateUtil.isNullOrNotSize(list, 0));
        assertTrue (ValidateUtil.isNullOrNotSize(list, 1));
        assertFalse(ValidateUtil.isNullOrNotSize(list, 2));
    }

    /**
     * Test of validate list.
     */
    @Test
    void equals() {
        List<String> list = null;

        assertFalse(ValidateUtil.equals(list, ""));
        assertFalse(ValidateUtil.equals(list, "a"));
        assertFalse(ValidateUtil.equals(list, "a", "a"));
        assertFalse(ValidateUtil.equals(list, "a", "b"));
        assertFalse(ValidateUtil.equals(list, "a", "b", "c"));

        list = new ArrayList<>();

        assertFalse(ValidateUtil.equals(list, ""));
        assertFalse(ValidateUtil.equals(list, "a"));
        assertFalse(ValidateUtil.equals(list, "a", "a"));
        assertFalse(ValidateUtil.equals(list, "a", "b"));
        assertFalse(ValidateUtil.equals(list, "a", "b", "c"));

        list.add("a");

        assertFalse(ValidateUtil.equals(list, ""));
        assertTrue (ValidateUtil.equals(list, "a"));
        assertFalse(ValidateUtil.equals(list, "a", "a"));
        assertFalse(ValidateUtil.equals(list, "a", "b"));
        assertFalse(ValidateUtil.equals(list, "a", "b", "c"));

        list.add("a");

        assertFalse(ValidateUtil.equals(list, ""));
        assertFalse(ValidateUtil.equals(list, "a"));
        assertTrue (ValidateUtil.equals(list, "a", "a"));
        assertFalse(ValidateUtil.equals(list, "a", "b"));
        assertFalse(ValidateUtil.equals(list, "a", "b", "c"));

        list.remove(1);
        list.add("b");

        assertFalse(ValidateUtil.equals(list, ""));
        assertFalse(ValidateUtil.equals(list, "a"));
        assertFalse(ValidateUtil.equals(list, "a", "a"));
        assertTrue (ValidateUtil.equals(list, "a", "b"));
        assertFalse(ValidateUtil.equals(list, "a", "b", "c"));
    }

    /**
     * Test of validate list.
     */
    @Test
    void equalsUuid() {
        List<UUID> list = null;

        UUID a = UUID.randomUUID();
        UUID b = UUID.randomUUID();
        UUID c = UUID.randomUUID();

        assertFalse(ValidateUtil.equals(list, a));
        assertFalse(ValidateUtil.equals(list, a, a));
        assertFalse(ValidateUtil.equals(list, a, b));
        assertFalse(ValidateUtil.equals(list, a, b, c));

        list = new ArrayList<>();

        assertFalse(ValidateUtil.equals(list, a));
        assertFalse(ValidateUtil.equals(list, a, a));
        assertFalse(ValidateUtil.equals(list, a, b));
        assertFalse(ValidateUtil.equals(list, a, b, c));

        list.add(a);

        assertTrue (ValidateUtil.equals(list, a));
        assertFalse(ValidateUtil.equals(list, a, a));
        assertFalse(ValidateUtil.equals(list, a, b));
        assertFalse(ValidateUtil.equals(list, a, b, c));

        list.add(a);

        assertFalse(ValidateUtil.equals(list, a));
        assertTrue (ValidateUtil.equals(list, a, a));
        assertFalse(ValidateUtil.equals(list, a, b));
        assertFalse(ValidateUtil.equals(list, a, b, c));

        list.remove(1);
        list.add(b);

        assertFalse(ValidateUtil.equals(list, a));
        assertFalse(ValidateUtil.equals(list, a, a));
        assertTrue (ValidateUtil.equals(list, a, b));
        assertFalse(ValidateUtil.equals(list, a, b, c));
    }

    /**
     * Test of validate list.
     */
    @Test
    void isEmptyOrEquals() {
        List<String> list = null;

        assertFalse(ValidateUtil.isEmptyOrEquals(list, ""));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b", "c"));

        list = new ArrayList<>();

        assertTrue (ValidateUtil.isEmptyOrEquals(list, ""));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a"));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a", "a"));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a", "b"));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a", "b", "c"));

        list.add("a");

        assertFalse(ValidateUtil.isEmptyOrEquals(list, ""));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b", "c"));

        list.add("a");

        assertFalse(ValidateUtil.isEmptyOrEquals(list, ""));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a"));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a", "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b", "c"));

        list.remove(1);
        list.add("b");

        assertFalse(ValidateUtil.isEmptyOrEquals(list, ""));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "a"));
        assertTrue (ValidateUtil.isEmptyOrEquals(list, "a", "b"));
        assertFalse(ValidateUtil.isEmptyOrEquals(list, "a", "b", "c"));
    }

    /**
     * Test of validate an array if any object in array is null.
     */
    @Test
    void isAnyNull() {
        String s = null;
        Calendar c = null;

        assertTrue (ValidateUtil.isAnyNull(s));
        assertTrue (ValidateUtil.isAnyNull(c));
        assertTrue (ValidateUtil.isAnyNull(s, c));

        s = "a";

        assertFalse(ValidateUtil.isAnyNull(s));
        assertTrue (ValidateUtil.isAnyNull(c));
        assertTrue (ValidateUtil.isAnyNull(s, c));

        s = null;
        c = Calendar.getInstance();

        assertTrue (ValidateUtil.isAnyNull(s));
        assertFalse(ValidateUtil.isAnyNull(c));
        assertTrue (ValidateUtil.isAnyNull(s, c));

        s = "a";
        c = Calendar.getInstance();

        assertFalse(ValidateUtil.isAnyNull(s));
        assertFalse(ValidateUtil.isAnyNull(c));
        assertFalse(ValidateUtil.isAnyNull(s, c));
    }

    /**
     * Test to validate an array if any object in array is null.
     *
     * @param s string
     * @param i integer
     * @param f float
     * @param expected expected
     */
    @ParameterizedTest
    @CsvSource(value = {
            "   , , ,true",
            "   , ,1,true",
            "   ,1, ,true",
            "   ,1,1,true",
            "'1', , ,true",
            "'1', ,1,true",
            "'1',1, ,true",
            "'1',1,1,false"
    })
    void isAnyNullParameterized(String s, Integer i, Float f, boolean expected) {
        assertEquals(expected, ValidateUtil.isAnyNull(s, i, f));
    }

    /**
     * Test of validate an array if all objects in array are null.
     */
    @Test
    void areAllNull() {
        String s = null;
        Calendar c = null;

        assertTrue (ValidateUtil.areAllNull(s));
        assertTrue (ValidateUtil.areAllNull(c));
        assertTrue (ValidateUtil.areAllNull(s, c));

        s = "a";

        assertFalse(ValidateUtil.areAllNull(s));
        assertTrue (ValidateUtil.areAllNull(c));
        assertFalse(ValidateUtil.areAllNull(s, c));

        s = null;
        c = Calendar.getInstance();

        assertTrue (ValidateUtil.areAllNull(s));
        assertFalse(ValidateUtil.areAllNull(c));
        assertFalse(ValidateUtil.areAllNull(s, c));

        s = "a";
        c = Calendar.getInstance();

        assertFalse(ValidateUtil.areAllNull(s));
        assertFalse(ValidateUtil.areAllNull(c));
        assertFalse(ValidateUtil.areAllNull(s, c));
    }

    /**
     * Test to validate an array if any object is equal to given enum.
     */
    @Test
    void isAnyEqualType() {
        Type type = Type.SUBSYSTEM;

        assertFalse(ValidateUtil.isAnyEqual(null, Type.SYSTEMGROUP));
        assertFalse(ValidateUtil.isAnyEqual(null, Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE, Type.DEVICEGROUP, Type.DEVICETYPE));

        assertFalse(ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP));
        assertFalse(ValidateUtil.isAnyEqual(type, Type.SYSTEM));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.SUBSYSTEM));
        assertFalse(ValidateUtil.isAnyEqual(type, Type.DISCIPLINE));
        assertFalse(ValidateUtil.isAnyEqual(type, Type.DEVICEGROUP));
        assertFalse(ValidateUtil.isAnyEqual(type, Type.DEVICETYPE));

        assertFalse(ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.SYSTEM));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE, Type.DEVICEGROUP));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.SYSTEMGROUP, Type.SYSTEM, Type.SUBSYSTEM, Type.DISCIPLINE, Type.DEVICEGROUP, Type.DEVICETYPE));

        assertFalse(ValidateUtil.isAnyEqual(type, Type.DEVICETYPE, Type.DEVICEGROUP));
        assertFalse(ValidateUtil.isAnyEqual(type, Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM, Type.SYSTEM));
        assertTrue (ValidateUtil.isAnyEqual(type, Type.DEVICETYPE, Type.DEVICEGROUP, Type.DISCIPLINE, Type.SUBSYSTEM, Type.SYSTEM, Type.SYSTEMGROUP));
    }

    /**
     * Test to validate an array if any object is equal to given enum.
     */
    @Test
    void isAnyEqualNameChoice() {
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.CREATE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.UPDATE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.DELETE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.CREATE, NameCommand.UPDATE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.CREATE, NameCommand.DELETE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.UPDATE, NameCommand.DELETE));
        assertFalse(ValidateUtil.isAnyEqual(null, NameCommand.CREATE, NameCommand.UPDATE, NameCommand.DELETE));

        assertFalse(ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.CREATE));
        assertTrue (ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.UPDATE));
        assertFalse(ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.DELETE));
        assertTrue (ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.CREATE, NameCommand.UPDATE));
        assertFalse(ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.CREATE, NameCommand.DELETE));
        assertTrue (ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.UPDATE, NameCommand.DELETE));
        assertTrue (ValidateUtil.isAnyEqual(NameCommand.UPDATE, NameCommand.CREATE, NameCommand.UPDATE, NameCommand.DELETE));
    }

    /**
     * Test to validate an array if any value in array is true.
     *
     * @param s string
     * @param i integer
     * @param f float
     * @param expected expected
     */
    @ParameterizedTest
    @CsvSource(value = {
            "false,false,false,false",
            "false,false,true, true",
            "false,true, false,true",
            "false,true, true, true",
            "true, false,false,true",
            "true, false,true, true",
            "true, true, false,true",
            "true, true, true, true"
    })
    void isAnyTrueParameterized(boolean b1, boolean b2, boolean b3, boolean expected) {
        assertEquals(expected, ValidateUtil.isAnyTrue(b1, b2, b3));
    }

    /**
     * Test of validate an array if all ints in array are zero.
     */
    @Test
    void areAllZero() {
        assertFalse(ValidateUtil.areAllZero(null));

        assertTrue (ValidateUtil.areAllZero(0));
        assertFalse(ValidateUtil.areAllZero(1));

        assertTrue (ValidateUtil.areAllZero(0, 0));
        assertFalse(ValidateUtil.areAllZero(0, 1));
        assertFalse(ValidateUtil.areAllZero(1, 0));
        assertFalse(ValidateUtil.areAllZero(1, 1));

        assertTrue (ValidateUtil.areAllZero(0, 0, 0));
        assertFalse(ValidateUtil.areAllZero(0, 0, 1));
        assertFalse(ValidateUtil.areAllZero(0, 1, 0));
        assertFalse(ValidateUtil.areAllZero(0, 1, 1));
        assertFalse(ValidateUtil.areAllZero(1, 0, 0));
        assertFalse(ValidateUtil.areAllZero(1, 0, 1));
        assertFalse(ValidateUtil.areAllZero(1, 1, 0));
        assertFalse(ValidateUtil.areAllZero(1, 1, 1));
    }

    /**
     * Test of validate if any of a list of patterns matches an input string.
     */
    @Test
    void isAnyMatch() {
        List<Pattern> patterns = null;
        String input = null;
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "this is a test";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));

        patterns = Lists.newArrayList();
        input = null;
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "this is a test string";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));

        patterns.add(Pattern.compile("b"));
        input = null;
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "this is a test string";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));

        patterns.add(Pattern.compile("st"));
        input = null;
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "";
        assertFalse(ValidateUtil.isAnyMatch(patterns, input));
        input = "this is a test string";
        assertTrue (ValidateUtil.isAnyMatch(patterns, input));
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of validate input read names.
     */
    @Test
    void validateInputReadNames() {
        ValidateNameElementUtil.validateNamesInputRead(
                null, null, null, null, null, null, null, null,
                null, null, null, null, null);
    }

    /**
     * Test of validate input create name element.
     */
    @Test
    void validateInputCreateNameElement() {
        ValidateNameElementUtil.validateNameElementInputCreate(null);
    }

    /**
     * Test of validate data create name element.
     */
    @Test
    void validateDataCreateNameElement() {
        ValidateNameElementUtil.validateNameElementDataCreate(null, null,
                null, null, null);
    }

    /**
     * Test of validate data create name.
     */
    @Test
    void validateDataCreateName() {
        ValidateNameElementUtil.validateNameDataCreate(null, null,
                null, null);
    }
    /**
     * Test of validate input update name element.
     */
    @Test
    void validateInputUpdateNameElement() {
        ValidateNameElementUtil.validateNameElementInputUpdate(null);
    }

    /**
     * Test of validate data update name element.
     */
    @Test
    void validateDataUpdateNameElement() {
        ValidateNameElementUtil.validateNameElementDataUpdate(null, null,
                null, null, null);
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Test of validate input read structures.
     */
    @Test
    void validateInputReadStructuresTypeNull() {
        try {
            ValidateStructureElementUtil.validateStructuresInputRead(
                    null, null,
                    null, null, null, null, null, null,
                    null, null, null, null, null);
        } catch (InputNotAvailableException e) {
            fail();
        }
    }

    /**
     * Test of validate input read structures.
     */
    @Test
    void validateInputReadStructures() {
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.SYSTEMGROUP, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.SYSTEM, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.SUBSYSTEM, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.DISCIPLINE, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.DEVICEGROUP, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
        ValidateStructureElementUtil.validateStructuresInputRead(
                Type.DEVICETYPE, null,
                null, null, null, null, null, null,
                null, null, null, null, null);
    }

    /**
     * Test of validate data create structure element.
     */
    @Test
    void validateDataCreateStructureElement() {
        ValidateStructureElementUtil.validateStructureElementDataCreate(null, null,
                null, null);
    }

    /**
     * Test of validate data create structure.
     */
    @Test
    void validateDataCreateStructuret() {
        ValidateStructureElementUtil.validateStructureDataCreate(null, null, null,
                null, null);
    }

    /**
     * Test of validate input update structure element.
     */
    @Test
    void validateInputUpdateStructureElement() {
        ValidateStructureElementUtil.validateStructureElementInputUpdate(null, null);
    }

    /**
     * Test of validate data update structure element.
     */
    @Test
    void validateDataUpdateStructureElement() {
        ValidateStructureElementUtil.validateStructureElementDataUpdate(null, null,
                null, null);
    }

}

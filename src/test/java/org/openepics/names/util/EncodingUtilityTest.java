/*
 * Copyright (c) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Purpose to test EncodingUtility class.
 *
 * @author Lars Johansson
 */
class EncodingUtilityTest {

    /**
     * Test for encoding a string.
     */
    @Test
    void encodeNull() {
        String s = null;
        assertThrows(NullPointerException.class, () -> { EncodingUtility.encode(s); });
    }

    /**
     * Test for encoding a string.
     *
     * @param input value to be encoded
     * @param expected expected encoded value
     */
    @ParameterizedTest
    @CsvSource(value = {
            "'',''",
            "%,%25",
            "+,%2B",
            "_,_",
            "' ',+",
            "12345678,12345678",
            "abcdefgh,abcdefgh",
            "abc+efgh,abc%2Befgh",
            "abcd fgh,abcd+fgh",
            "ab de+gh,ab+de%2Bgh"
    })
    void encode(String input, String expected) {
        assertEquals(expected, EncodingUtility.encode(input));
    }

    /**
     * Test for decoding a string.
     */
    @Test
    void decodeNull() {
        String s = null;
        assertThrows(NullPointerException.class, () -> { EncodingUtility.decode(s); });

    }

    /**
     * Test for decoding a string.
     *
     * @param input value to be decoded
     * @param expected expected decoded value
     */
    @ParameterizedTest
    @CsvSource(value = {
            "'',''",
            "%25,%",
            "%2B,+",
            "_,_",
            "+,' '",
            "12345678,12345678",
            "abcdefgh,abcdefgh",
            "abc%2Befgh,abc+efgh",
            "abcd+fgh,abcd fgh",
            "ab+de%2Bgh,ab de+gh"
    })
    void decode(String input, String expected) {
        assertEquals(expected, EncodingUtility.decode(input));
    }

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.StructureCommand;

import com.google.common.collect.Lists;

/**
 * Unit tests for class NotificationUtil class.
 *
 * @author Lars Johansson
 *
 * @see class NotificationUtil
 */
class NotificationUtilTest {

    @Test
    void sortByNewName() {
        NotificationName nn1 = new NotificationName(
                NameCommand.CREATE,
                UUID.randomUUID(),
                "oldIndexQ", "oldNameW", "oldDescriptionE",
                "newIndexR", "newNameT", "newDescriptionY",
                new Date(), "whoI");
        NotificationName nn2 = new NotificationName(
                NameCommand.UPDATE,
                UUID.randomUUID(),
                "oldIndexO", "oldNameP", "oldDescriptionA",
                "newIndexS", "newNameD", "newDescriptionF",
                new Date(), "whoH");
        NotificationName nn3 = new NotificationName(
                NameCommand.DELETE,
                UUID.randomUUID(),
                "oldIndexJ", "oldNameK", "oldDescriptionL",
                "newIndexZ", "newNameX", "newDescriptionC",
                new Date(), "whoB");

        List<NotificationName> notifications = Lists.newArrayList();
        notifications.add(nn1);
        notifications.add(nn2);
        notifications.add(nn3);

        NotificationUtil.sortByNewName(notifications);

        assertTrue(StringUtils.equals(notifications.get(0).getNewName(), "newNameD"));
        assertTrue(StringUtils.equals(notifications.get(1).getNewName(), "newNameT"));
        assertTrue(StringUtils.equals(notifications.get(2).getNewName(), "newNameX"));
    }

    @Test
    void sortByNewMnemonicpath() {
        NotificationStructure ns1 = new NotificationStructure(
                StructureCommand.CREATE,
                Type.SYSTEM, UUID.randomUUID(),
                "oldMnemonicW", "oldMnemonicpathE", "oldDescriptionR",
                "newMnemonicY", "newMnemonicpathU", "newDescriptionI",
                new Date(), "whoP");
        NotificationStructure ns2 = new NotificationStructure(
                StructureCommand.CREATE,
                Type.DISCIPLINE, UUID.randomUUID(),
                "oldMnemonicS", "oldMnemonicpathD", "oldDescriptionF",
                "newMnemonicH", "newMnemonicpathJ", "newDescriptionK",
                new Date(), "whoZ");
        NotificationStructure ns3 = new NotificationStructure(
                StructureCommand.CREATE,
                Type.DEVICETYPE, UUID.randomUUID(),
                "oldMnemonicC", "oldMnemonicpathV", "oldDescriptionB",
                "newMnemonicM", "newMnemonicpathQ", "newDescriptionW",
                new Date(), "whoR");

        List<NotificationStructure> notifications = Lists.newArrayList();
        notifications.add(ns1);
        notifications.add(ns2);
        notifications.add(ns3);

        NotificationUtil.sortByNewMnemonicpath(notifications);

        assertTrue(StringUtils.equals(notifications.get(0).getNewMnemonicpath(), "newMnemonicpathJ"));
        assertTrue(StringUtils.equals(notifications.get(1).getNewMnemonicpath(), "newMnemonicpathQ"));
        assertTrue(StringUtils.equals(notifications.get(2).getNewMnemonicpath(), "newMnemonicpathU"));
    }

}

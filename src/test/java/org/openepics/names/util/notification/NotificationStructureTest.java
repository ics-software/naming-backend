/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.util.StructureCommand;

/**
 * Unit tests for NotificationStructure class.
 *
 * @author Lars Johansson
 *
 * @see NotificationStructure
 */
class NotificationStructureTest {

    @Test
    void create() {
        NotificationStructure ns = new NotificationStructure(
                StructureCommand.CREATE,
                Type.SYSTEM, UUID.randomUUID(),
                "oldMnemonicW", "oldMnemonicpathE", "oldDescriptionR",
                "newMnemonicY", "newMnemonicpathU", "newDescriptionI",
                new Date(), "whoP");

        assertNotNull(ns);
        assertTrue(StringUtils.isNotEmpty(ns.toString()));
    }

    @Test
    void update() {
        NotificationStructure ns = new NotificationStructure(
                StructureCommand.UPDATE,
                Type.DISCIPLINE, UUID.randomUUID(),
                "oldMnemonicS", "oldMnemonicpathD", "oldDescriptionF",
                "newMnemonicH", "newMnemonicpathJ", "newDescriptionK",
                new Date(), "whoZ");

        assertNotNull(ns);
        assertTrue(StringUtils.isNotEmpty(ns.toString()));
    }

    @Test
    void delete() {
        NotificationStructure ns = new NotificationStructure(
                StructureCommand.DELETE,
                Type.DEVICETYPE, UUID.randomUUID(),
                "oldMnemonicC", "oldMnemonicpathV", "oldDescriptionB",
                "newMnemonicM", "newMnemonicpathQ", "newDescriptionW",
                new Date(), "whoR");

        assertNotNull(ns);
        assertTrue(StringUtils.isNotEmpty(ns.toString()));
    }

}

/*
 * Copyright (C) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util.notification;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.openepics.names.util.NameCommand;

/**
 * Unit tests for NotificationName class.
 *
 * @author Lars Johansson
 *
 * @see NotificationName
 */
class NotificationNameTest {

    @Test
    void create() {
        NotificationName nn = new NotificationName(
                NameCommand.CREATE,
                UUID.randomUUID(),
                "oldIndexQ", "oldNameW", "oldDescriptionE",
                "newIndexR", "newNameT", "newDescriptionY",
                new Date(), "whoI");

        assertNotNull(nn);
        assertTrue(StringUtils.isNotEmpty(nn.toString()));
    }

    @Test
    void update() {
        NotificationName nn = new NotificationName(
                NameCommand.UPDATE,
                UUID.randomUUID(),
                "oldIndexO", "oldNameP", "oldDescriptionA",
                "newIndexS", "newNameD", "newDescriptionF",
                new Date(), "whoH");

        assertNotNull(nn);
        assertTrue(StringUtils.isNotEmpty(nn.toString()));
    }

    @Test
    void delete() {
        NotificationName nn = new NotificationName(
                NameCommand.DELETE,
                UUID.randomUUID(),
                "oldIndexJ", "oldNameK", "oldDescriptionL",
                "newIndexZ", "newNameX", "newDescriptionC",
                new Date(), "whoB");

        assertNotNull(nn);
        assertTrue(StringUtils.isNotEmpty(nn.toString()));
    }

}

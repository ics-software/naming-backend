/*
 * Copyright (C) 2021 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;
import org.openepics.names.exception.DataConflictException;
import org.openepics.names.exception.DataDeletedException;
import org.openepics.names.exception.DataExistException;
import org.openepics.names.exception.DataNotAvailableException;
import org.openepics.names.exception.DataNotCorrectException;
import org.openepics.names.exception.DataNotFoundException;
import org.openepics.names.exception.DataNotValidException;
import org.openepics.names.exception.InputNotAvailableException;
import org.openepics.names.exception.InputNotCorrectException;
import org.openepics.names.exception.InputNotValidException;

/**
 * Unit tests for ExceptionUtil class.
 *
 * @author Lars Johansson
 *
 * @see ExceptionUtil
 */
class ExceptionUtilTest {

    /**
     * Test of create data conflict exception.
     */
    @Test
    void createDataConflictException() {
        DataConflictException exception = ExceptionUtil.createDataConflictException(null, null, null);
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertNull(exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data deleted exception.
     */
    @Test
    void createDataDeletedException() {
        DataDeletedException exception = ExceptionUtil.createDataDeletedException(null, null, null);
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertNull(exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data exist exception.
     */
    @Test
    void createDataExistException() {
        DataExistException exception = ExceptionUtil.createDataExistException("1", "2", "3");
        assertNotNull(exception);
        assertEquals("1", exception.getMessage());
        assertEquals("2", exception.getDetails());
        assertEquals("3", exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data not available exception.
     */
    @Test
    void createDataNotAvailableException() {
        DataNotAvailableException exception = ExceptionUtil.createDataNotAvailableException("1", "2", null);
        assertNotNull(exception);
        assertEquals("1", exception.getMessage());
        assertEquals("2", exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data not correct exception.
     */
    @Test
    void createDataNotCorrectException() {
        DataNotCorrectException exception = ExceptionUtil.createDataNotCorrectException("1", null, "3");
        assertNotNull(exception);
        assertEquals("1", exception.getMessage());
        assertNull(exception.getDetails());
        assertEquals("3", exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data not found exception.
     */
    @Test
    void createDataNotFoundException() {
        DataNotFoundException exception = ExceptionUtil.createDataNotFoundException("1", null, null);
        assertNotNull(exception);
        assertEquals("1", exception.getMessage());
        assertNull(exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create data not valid exception.
     */
    @Test
    void createDataNotValidException() {
        DataNotValidException exception = ExceptionUtil.createDataNotValidException(null, "2", "3");
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertEquals("2", exception.getDetails());
        assertEquals("3", exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create input not available exception.
     */
    @Test
    void createInputNotAvailableException() {
        InputNotAvailableException exception = ExceptionUtil.createInputNotAvailableException(null, "2", null);
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertEquals("2", exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create input not correct exception.
     */
    @Test
    void createInputNotCorrectException() {
        InputNotCorrectException exception = ExceptionUtil.createInputNotCorrectException(null, null, "3");
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertNull(exception.getDetails());
        assertEquals("3", exception.getField());
        assertNull(exception.getCause());
    }

    /**
     * Test of create input not valid exception.
     */
    @Test
    void createInputNotValidException() {
        InputNotValidException exception = ExceptionUtil.createInputNotValidException(null, null, null);
        assertNotNull(exception);
        assertNull(exception.getMessage());
        assertNull(exception.getDetails());
        assertNull(exception.getField());
        assertNull(exception.getCause());
    }

}

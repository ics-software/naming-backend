/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.util.URLUtility;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.containers.ContainerState;
import org.testcontainers.containers.wait.strategy.Wait;

import com.github.dockerjava.api.DockerClient;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtil {

    public static final String NAMING = "naming";

    static final int HTTP_UNPROCESSABLE_ENTITY = 422;

    static final String AUTH_USER     = "user:userPass";
    static final String AUTH_ADMIN    = "admin:adminPass";
    static final String EMPTY_JSON    = "[]";
    static final String HTTP          = "http://";

    static final String HEADER_ACCEPT_APPLICATION_JSON          = "'accept: application/json'";
    static final String HEADER_ACCEPT_APPLICATION_VND_MS_EXCEL  = "'accept: application/vnd.ms-excel'";
    static final String HEADER_CONTENT_TYPE_APPLICATION_JSON    = "'Content-Type: application/json'";
    static final String HEADER_CONTENT_TYPE_MULTIPART_FORM_DATA = "'Content-Type: multipart/form-data'";

    static final String IP_PORT_NAMING    = "127.0.0.1:8080";

    static final String API_V1_CONVERT    = "/api/v1/convert";
    static final String API_V1_NAMES      = "/api/v1/names";
    static final String API_V1_PVNAMES    = "/api/v1/pvNames";
    static final String API_V1_STRUCTURES = "/api/v1/structures";

    static final String HTTP_IP_PORT_NAMING                              = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING;

    static final String HTTP_IP_PORT_NAMING_API_V1_NAMES                 = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING + API_V1_NAMES;
    static final String HTTP_AUTH_USER_IP_PORT_NAMING_API_V1_NAMES       = ITUtil.HTTP + ITUtil.AUTH_USER  + "@" + ITUtil.IP_PORT_NAMING + API_V1_NAMES;
    static final String HTTP_AUTH_ADMIN_IP_PORT_NAMING_API_V1_NAMES      = ITUtil.HTTP + ITUtil.AUTH_ADMIN + "@" + ITUtil.IP_PORT_NAMING + API_V1_NAMES;

    static final String HTTP_IP_PORT_NAMING_API_V1_PVNAMES               = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING + API_V1_PVNAMES;

    static final String HTTP_IP_PORT_NAMING_API_V1_STRUCTURES            = ITUtil.HTTP +                           ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;
    static final String HTTP_AUTH_USER_IP_PORT_NAMING_API_V1_STRUCTURES  = ITUtil.HTTP + ITUtil.AUTH_USER  + "@" + ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;
    static final String HTTP_AUTH_ADMIN_IP_PORT_NAMING_API_V1_STRUCTURES = ITUtil.HTTP + ITUtil.AUTH_ADMIN + "@" + ITUtil.IP_PORT_NAMING + API_V1_STRUCTURES;

    private static final String BRACKET_BEGIN     = "[";
    private static final String BRACKET_END       = "]";
    private static final String CURLY_BRACE_BEGIN = "{";
    private static final String CURLY_BRACE_END   = "}";
    private static final String HTTP_REPLY        = "HTTP";

    // integration test - docker

    public static final String INTEGRATIONTEST_DOCKER_COMPOSE = "docker-compose-integrationtest.yml";
    public static final String INTEGRATIONTEST_LOG_MESSAGE    = ".*Started NamingApplication.*";

    // code coverage

    public static final String JACOCO_EXEC_PATH      = "/naming/jacoco.exec";
    public static final String JACOCO_TARGET_PREFIX  = "target/jacoco_";
    public static final String JACOCO_TARGET_SUFFIX  = ".exec";
    public static final String JACOCO_SKIPITCOVERAGE = "skipITCoverage";

    /**
     * This class is not to be instantiated.
     */
    private ITUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Provide a default compose setup for testing.
     * For Docker Compose V2.
     *
     * Intended usage is as field annotated with @Container from class annotated with @Testcontainers.
     *
     * @return compose container
     */
    public static ComposeContainer defaultComposeContainers() {
        return new ComposeContainer(new File(ITUtil.INTEGRATIONTEST_DOCKER_COMPOSE))
                .withEnv(ITUtil.JACOCO_SKIPITCOVERAGE, System.getProperty(ITUtil.JACOCO_SKIPITCOVERAGE))
                .withLocalCompose(true)
                .waitingFor(ITUtil.NAMING, Wait.forLogMessage(ITUtil.INTEGRATIONTEST_LOG_MESSAGE, 1));
    }

    /**
     * Extract coverage report from compose container to file system.
     *
     * @param environment compose container
     * @param destinationPath destination path, i.e. where in file system to put coverage report
     * that has been extracted from container
     */
    public static void extractJacocoReport(ComposeContainer environment, String destinationPath) {
        // extract jacoco report from container file system
        //     stop jvm to make data available

        if (!Boolean.FALSE.toString().equals(System.getProperty(ITUtil.JACOCO_SKIPITCOVERAGE))) {
            return;
        }

        Optional<ContainerState> container = environment.getContainerByServiceName(ITUtil.NAMING);
        if (container.isPresent()) {
            ContainerState cs = container.get();
            DockerClient dc = cs.getDockerClient();
            dc.stopContainerCmd(cs.getContainerId()).exec();
            try {
                cs.copyFileFromContainer(ITUtil.JACOCO_EXEC_PATH, destinationPath);
            } catch (Exception e) {
                // proceed if file cannot be copied
            }
        }
    }

    /**
     * Do GET request with given string as URL and return response code.
     *
     * @param spec string to parse as URL
     * @return response code
     *
     * @throws IOException
     */
    static int doGet(String spec) throws IOException {
        URL url = new URL(spec);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        return con.getResponseCode();
    }

    /**
     * Do GET request with given string as URL and return response with string array with response code and response string.
     *
     * @param spec string to parse as URL
     * @return string array with response code and response string
     *
     * @throws IOException
     */
    static String[] doGetJson(String spec) throws IOException {
        // note that URL is encoded

        URL url = new URL(URLUtility.encodeURL(spec));
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        int responseCode = con.getResponseCode();

        String line;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = responseCode == HttpURLConnection.HTTP_OK
                ? new BufferedReader(new InputStreamReader(con.getInputStream()))
                : new BufferedReader(new InputStreamReader(con.getErrorStream()))) {
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
        }

        return new String[] {String.valueOf(responseCode), sb.toString().trim()};
    }

    /**
     * Run a shell command and return response with string array with response code and response string.
     *
     * @param command shell command
     * @return string array with response code and response string
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws Exception
     */
    static String[] runShellCommand(String command) throws IOException, InterruptedException, Exception {
        // run shell command & return http response code if available

        final ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);

        String responseCode = null;
        String responseContent = null;
        try {
            final Process process = processBuilder.start();
            final BufferedReader errorStream = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            final BufferedReader inputStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
            final boolean processFinished = process.waitFor(30, TimeUnit.SECONDS);

            String line = null;
            while ((line = inputStream.readLine()) != null) {
                if (line.startsWith(HTTP_REPLY)) {
                    // response code, e.g. "HTTP/1.1 200", "HTTP/1.1 401", "HTTP/1.1 500"
                    String[] s = line.trim().split(" ");
                    if (s != null && s.length == 2) {
                        responseCode = s[1];
                    }
                } else if ((line.startsWith(BRACKET_BEGIN) && line.endsWith(BRACKET_END))
                        || (line.startsWith(CURLY_BRACE_BEGIN) && line.endsWith(CURLY_BRACE_END))) {
                    // response string, json
                    responseContent = line;
                }
            }

            if (!processFinished) {
                throw new Exception("Timed out waiting to execute command: " + command);
            }
            if (process.exitValue() != 0) {
                throw new Exception(
                        String.format("Shell command finished with status %d error: %s",
                                process.exitValue(),
                                errorStream.lines().collect(Collectors.joining())));
            }
        } catch (IOException | InterruptedException e) {
            throw e;
        }
        return new String[] {responseCode, responseContent};
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first contains response code OK (200).
     *
     * @param response string array with response of http request, response code and content
     *
     * @see HttpURLConnection#HTTP_OK
     */
    static void assertResponseLength2CodeOK(String[] response) {
        assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
    }

    /**
     * Assert that response object is as expected, an array with 2 elements
     * of which first element contains given response code.
     *
     * @param response string array with response of http request, response code and content
     * @param expectedResponseCode expected response code
     *
     * @see HttpURLConnection for available response codes
     */
    static void assertResponseLength2Code(String[] response, int expectedResponseCode) {
        assertNotNull(response);
        assertEquals(2, response.length);
        assertEquals(expectedResponseCode, Integer.parseInt(response[0]));
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Assert response boolean available with expected response.
     *
     * @param actual response boolean object
     * @param expected expected response
     */
    static void assertEqualsResponseBoolean(ResponseBoolean actual, Boolean expected) {
        assertNotNull(actual);
        assertEquals(expected, actual.getValue());
    }
    /**
     * Assert response boolean list available with expected response.
     *
     * @param actual response boolean list object
     * @param expected expected response
     */
    static void assertEqualsResponseBoolean(ResponseBooleanList actual, Boolean expected) {
        assertNotNull(actual);
        assertEquals(expected, actual.getValue());
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Add one to count for key in map.
     *
     * @param key key
     * @param map map
     */
    static void addOne(UUID key, Map<UUID, Long> map) {
        addOne(key, map, null);
    }
    /**
     * Add one to count for key in map.
     *
     * @param key key
     * @param map map
     * @param mapNok map nok
     */
    static void addOne(UUID key, Map<UUID, Long> map, Map<UUID, Long> mapNok) {
        if (key == null) {
            return;
        }

        Long no = map.get(key);
        if (mapNok != null && no == null) {
            no = mapNok.get(key);
            no = no != null ? no + 1 : 1;
            mapNok.put(key, no);
        } else {
            no = no != null ? no + 1 : 1;
            map.put(key, no);
        }
    }

    // ----------------------------------------------------------------------------------------------------

    // enum for http methods
    static enum MethodChoice        {POST, GET, PUT, DELETE, PATCH};

    // enum for different authorizations
    static enum AuthorizationChoice {NONE, USER, ADMIN};

    // enum for different endpoints
    static enum EndpointChoice      {NAMES, STRUCTURES, CONVERT};

    /**
     * Utility method to return curl for POST with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for POST with path and json
     */
    static String curlPostPathJson2Excel(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        StringBuilder header = new StringBuilder();
        header.append(" -H " + HEADER_ACCEPT_APPLICATION_VND_MS_EXCEL);
        header.append(" -H " + HEADER_CONTENT_TYPE_APPLICATION_JSON);

        return curlMethodAuthEndpointPathJson(header.toString(), MethodChoice.POST, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for POST with path.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param file file
     * @return curl for POST with path
     */
    static String curlPostPathExcel2Json(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, File file) {
        StringBuilder header = new StringBuilder();
        header.append(" -H " + HEADER_ACCEPT_APPLICATION_JSON);
        header.append(" -H " + HEADER_CONTENT_TYPE_MULTIPART_FORM_DATA);
        header.append(" -F 'file=@" + file.getPath() + ";type=application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'");

        return curlMethodAuthEndpointPathJson(header.toString(), MethodChoice.POST, authorizationChoice, endpointChoice, path, null);
    }

    /**
     * Utility method to return curl for POST (create information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for POST (create information) with path and json
     */
    static String curlPostPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.POST, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for GET (get information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for GET (get information) with path and json
     */
    static String curlGetPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.GET, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for PUT (update information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for PUT (update information) with path and json
     */
    static String curlPutPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.PUT, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for DELETE (delete information) with path and json
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for DELETE (delete information) with path and json
     */
    static String curlDeletePathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.DELETE, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Utility method to return curl for PATCH (patch information) with path and json.
     *
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl for PATCH with path and json
     */
    static String curlPatchPathJson(AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(MethodChoice.PATCH, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Prepare curl command for test to run for contacting server.
     *
     * @param methodChoice method choice
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl command to run
     */
    private static String curlMethodAuthEndpointPathJson(MethodChoice methodChoice, AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        return curlMethodAuthEndpointPathJson(null, methodChoice, authorizationChoice, endpointChoice, path, json);
    }

    /**
     * Prepare curl command for test to run for contacting server.
     *
     * @param header header
     * @param methodChoice method choice
     * @param authorizationChoice authorization choice
     * @param endpointChoice endpoint choice
     * @param path particular path
     * @param json json data
     * @return curl command to run
     */
    private static String curlMethodAuthEndpointPathJson(String header, MethodChoice methodChoice, AuthorizationChoice authorizationChoice, EndpointChoice endpointChoice, String path, String json) {
        String pathstr = !StringUtils.isEmpty(path)
                ? path
                : "";

        String data = !StringUtils.isEmpty(json)
                ? " -d '" + json + "'"
                : "";

        String optionHeader = !StringUtils.isEmpty(header)
                ? header
                : !StringUtils.isEmpty(json)
                    ? " -H " + ITUtil.HEADER_CONTENT_TYPE_APPLICATION_JSON
                    : "";

        String options =
                optionHeader
                + " -X"  + ITUtil.getMethodString(methodChoice)
                + " -i ";

        return "curl"
            + options
            + ITUtil.HTTP
            + ITUtil.getAuthorizationString(authorizationChoice)
            + ITUtil.IP_PORT_NAMING
            + ITUtil.getEndpointString(endpointChoice)
            + pathstr
            + data;
    }

    /**
     * Utility method to return string for http method. To be used when constructing url to send query to server.
     *
     * @param methodChoice method choice, i.e. POST, GET, PUT, DELETE, PATCH
     * @return string for http method
     */
    private static String getMethodString(MethodChoice methodChoice) {
        switch (methodChoice) {
        case POST:
            return "POST";
        case GET:
            return "GET";
        case PUT:
            return "PUT";
        case PATCH:
            return "PATCH";
        case DELETE:
            return "DELETE";
        default:
            return "GET";
        }
    }

    /**
     * Utility method to return string for authorization. To be used when constructing url to send query to server.
     *
     * @param authorizationChoice authorization choice
     * @return string for authorization
     */
    private static String getAuthorizationString(AuthorizationChoice authorizationChoice) {
        switch (authorizationChoice) {
        case ADMIN:
            return ITUtil.AUTH_ADMIN  + "@";
        case USER:
            return ITUtil.AUTH_USER  + "@";
        case NONE:
            return StringUtils.EMPTY;
        default:
            return StringUtils.EMPTY;
        }
    }

    /**
     * Utility method to return string for endpoint. To be used when constructing url to send query to server.
     *
     * @param endpointChoice endpoint choice
     * @return string for endpoint
     */
    private static String getEndpointString(EndpointChoice endpointChoice) {
        switch (endpointChoice) {
        case NAMES:
            return ITUtil.API_V1_NAMES;
        case STRUCTURES:
            return ITUtil.API_V1_STRUCTURES;
        case CONVERT:
            return ITUtil.API_V1_CONVERT;
        default:
            return StringUtils.EMPTY;
        }
    }

}

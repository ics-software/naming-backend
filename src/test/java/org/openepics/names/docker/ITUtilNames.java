/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;

import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.util.NameCommand;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtilNames {

    static final ObjectMapper mapper = new ObjectMapper();

    /**
     * This class is not to be instantiated.
     */
    private ITUtilNames() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return string for name element array.
     *
     * @param value name element array
     * @return string for name element array
     */
    static String object2Json(NameElement[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for name element command array.
     *
     * @param value name element command array
     * @return string for name element command array
     */
    static String object2Json(NameElementCommandCreate[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for name element command array.
     *
     * @param value name element command array
     * @return string for name element command array
     */
    static String object2Json(NameElementCommandUpdate[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for name element command array.
     *
     * @param value name element command array
     * @return string for name element command array
     */
    static String object2Json(NameElementCommandConfirm[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertConvertExcel2Json(File, int)
     */
    public static String assertConvertExcel2Json(File file) {
        return assertConvertExcel2Json(file, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to convert Excel to json and assert response code.
     *
     * @param file file
     * @param expectedResponseCode expected response code
     * @return json
     */
    public static String assertConvertExcel2Json(File file, int expectedResponseCode) {
        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathExcel2Json(AuthorizationChoice.NONE, EndpointChoice.CONVERT, "/excel2Json/names", file));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            return response[1];
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertConvertJson2Excel(NameElement[], int)
     */
    public static String assertConvertJson2Excel(NameElement[] nameElements) {
        return assertConvertJson2Excel(nameElements, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to convert json to Excel and assert response code.
     *
     * @param nameElements name elements
     * @param expectedResponseCode expected response code
     * @return response for Excel file
     */
    public static String assertConvertJson2Excel(NameElement[] nameElements, int expectedResponseCode) {
        try {
            // response - no content (null) if response is not json
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson2Excel(AuthorizationChoice.NONE, EndpointChoice.CONVERT, "/json2Excel/names", object2Json(nameElements)));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            return response[1];
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertFind(String, int)
     */
    public static ResponsePageNameElements assertFind(String queryString) {
        return assertFind(queryString, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to find name element.
     *
     * @param queryString query string
     * @param expectedResponseCode expected response code
     * @return response page name elements
     */
    public static ResponsePageNameElements assertFind(String queryString, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageNameElements responsePageNameElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageNameElements = mapper.readValue(response[1], ResponsePageNameElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                assertTrue(responsePageNameElements.getListSize() > 0);
                assertNotNull(responsePageNameElements.getList().get(0));
            } else if (HttpURLConnection.HTTP_NOT_FOUND == expectedResponseCode){
                assertEquals(0, responsePageNameElements.getListSize());
            }

            return responsePageNameElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * @see ITUtilNames#assertRead(String, int, int)a
     */
    public static ResponsePageNameElements assertRead(String queryString) {
        return assertRead(queryString, -1, -1, HttpURLConnection.HTTP_OK);
    }
    /**
     * @see ITUtilNames#assertRead(String, int, int)a
     */
    public static ResponsePageNameElements assertRead(String queryString, int expectedEqual) {
        return assertRead(queryString, expectedEqual, expectedEqual, HttpURLConnection.HTTP_OK);
    }
    public static ResponsePageNameElements assertRead(String queryString, int expectedGreaterThanOrEqual, int expectedLessThanOrEqual) {
        return assertRead(queryString, expectedGreaterThanOrEqual, expectedLessThanOrEqual, HttpURLConnection.HTTP_OK);

    }
    /**
     * Utility method to read name elements and assert number of items in list.
     *
     * @param queryString query string
     * @param expectedGreaterThanOrEqual (if non-negative number) greater than or equal to this number of items
     * @param expectedLessThanOrEqual (if non-negative number) less than or equal to this number of items
     * @param expectedResponseCode expected response code
     * @return response page name elements
     */
    public static ResponsePageNameElements assertRead(String queryString, int expectedGreaterThanOrEqual, int expectedLessThanOrEqual, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageNameElements responsePageNameElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageNameElements = mapper.readValue(response[1], ResponsePageNameElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                // expected number of items in list
                //     (if non-negative number)
                //     expectedGreaterThanOrEqual <= nbr of items <= expectedLessThanOrEqual
                if (expectedGreaterThanOrEqual >= 0) {
                    assertTrue(responsePageNameElements.getListSize() >= expectedGreaterThanOrEqual);
                }
                if (expectedLessThanOrEqual >= 0) {
                    assertTrue(responsePageNameElements.getListSize() <= expectedLessThanOrEqual);
                }
            }

            return responsePageNameElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * @see ITUtilNames#assertHistory(String, int, int)
     */
    public static ResponsePageNameElements assertHistory(String queryString, int expectedEqual) {
        return assertHistory(queryString, expectedEqual, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to read history for name elements and assert number of items in list.
     *
     * @param queryString query string
     * @param expectedEqual expected number of items (if non-negative number)
     * @param expectedResponseCode expected response code
     * @return response page name elements
     */
    public static ResponsePageNameElements assertHistory(String queryString, int expectedEqual, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageNameElements responsePageNameElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/history" + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageNameElements = mapper.readValue(response[1], ResponsePageNameElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                // expected number of items in list
                //     (if non-negative number)
                //     expectedGreaterThanOrEqual <= nbr of items <= expectedLessThanOrEqual
                if (expectedEqual >= 0) {
                    assertTrue(responsePageNameElements.getListSize() >= expectedEqual);
                }
                if (expectedEqual >= 0) {
                    assertTrue(responsePageNameElements.getListSize() <= expectedEqual);
                }
            }

            return responsePageNameElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * Utility method to check if name exists.
     *
     * @param name name
     * @param expected expected response
     */
    public static void assertExists(String name, Boolean expected) {
        try {
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/exists/" + name);
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), expected);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Utility method to check if name is valid to create.
     *
     * @param name name
     * @param expected expected response
     */
    public static void assertIsValidToCreate(String name, Boolean expected) {
        try {
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + "/isValidToCreate/" + name);
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), expected);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandCreate nameElementCommand, NameCommand nameCommand, String index, Boolean expected) {
        nameElementCommand.setIndex(index);
        assertValidate(AuthorizationChoice.NONE, object2Json(new NameElementCommandCreate[] {nameElementCommand}), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandCreate nameElementCommand, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new NameElementCommandCreate[] {nameElementCommand}), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandUpdate nameElementCommand, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new NameElementCommandUpdate[] {nameElementCommand}), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandConfirm nameElementCommand, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new NameElementCommandConfirm[] {nameElementCommand}), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandCreate[] nameElementCommands, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(nameElementCommands), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandUpdate[] nameElementCommands, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(nameElementCommands), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(NameElementCommandConfirm[] nameElementCommands, NameCommand nameCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(nameElementCommands), nameCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilNames#assertValidate(AuthorizationChoice, String, NameCommand, int, Boolean)
     */
    public static void assertValidate(String json, NameCommand nameCommand, int expectedResponseCode) {
        assertValidate(AuthorizationChoice.NONE, json, nameCommand, expectedResponseCode, null);
    }
    /**
     * Utility method to validate json and assert expected response.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param nameCommand name command
     * @param expectedResponseCode expected response code
     * @param expected expected response
     */
    public static void assertValidate(AuthorizationChoice authorizationChoice, String json, NameCommand nameCommand, int expectedResponseCode, Boolean expected) {
        String path = getValidatePath(nameCommand);

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(authorizationChoice, EndpointChoice.NAMES, path, json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            if (expected != null) {
                ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), expected);
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Utility method to return validate path for name command.
     *
     * @param nameCommand name command
     * @return validate path
     */
    private static String getValidatePath(NameCommand nameCommand) {
        String validatePath = "";
        switch (nameCommand) {
            case CREATE:
                validatePath = "/validateCreate";
                break;
            case UPDATE:
                validatePath = "/validateUpdate";
                break;
            case DELETE:
                validatePath = "/validateDelete";
                break;
            default:
                break;
        }
        return validatePath;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertCreate(AuthorizationChoice, String, int)
     */
    public static NameElement assertCreate(NameElementCommandCreate nameElementCommand) {
        NameElement[] nameElements = assertCreate(AuthorizationChoice.NONE, object2Json(new NameElementCommandCreate[] {nameElementCommand}), HttpURLConnection.HTTP_CREATED);
        return nameElements != null
                ? nameElements[0]
                : null;
    }
    /**
     * @see ITUtilNames#assertCreate(AuthorizationChoice, String, int)
     */
    public static NameElement assertCreate(NameElementCommandCreate nameElementCommand, int expectedResponseCode) {
        NameElement[] nameElements = assertCreate(AuthorizationChoice.NONE, object2Json(new NameElementCommandCreate[] {nameElementCommand}), expectedResponseCode);
        return nameElements != null
                ? nameElements[0]
                : null;
    }
    /**
     * @see ITUtilNames#assertCreate(AuthorizationChoice, String, int)
     */
    public static NameElement[] assertCreate(NameElementCommandCreate[] nameElementCommands) {
        return assertCreate(AuthorizationChoice.NONE, object2Json(nameElementCommands), HttpURLConnection.HTTP_CREATED);
    }
    /**
     * @see ITUtilNames#assertCreate(AuthorizationChoice, String, int)
     */
    public static NameElement[] assertCreate(String json, int expectedResponseCode) {
        return assertCreate(AuthorizationChoice.NONE, json, expectedResponseCode);
    }
    /**
     * Utility method to create json (name elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     * @return created name element
     */
    public static NameElement[] assertCreate(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        NameElement[] nameElements = null;

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(authorizationChoice, EndpointChoice.NAMES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            if (HttpURLConnection.HTTP_CREATED == expectedResponseCode) {
                nameElements = mapper.readValue(response[1], NameElement[].class);

                for (NameElement createdNameElement : nameElements) {
                    assertNotNull(createdNameElement.getUuid());
                    assertEquals(Boolean.FALSE, createdNameElement.isDeleted());
                    assertNotNull(createdNameElement.getWhen());
                }

                // not set uuid for input parameter(s)
                //     convenience, easy when array length 1, otherwise not easy
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return nameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertUpdate(AuthorizationChoice, String, int)
     */
    public static NameElement assertUpdate(NameElementCommandUpdate nameElementCommand) {
        NameElement[] nameElements = assertUpdate(AuthorizationChoice.NONE, object2Json(new NameElementCommandUpdate[] {nameElementCommand}), HttpURLConnection.HTTP_OK);
        return nameElements != null
                ? nameElements[0]
                : null;
    }
    /**
     * @see ITUtilNames#assertUpdate(AuthorizationChoice, String, int)
     */
    public static NameElement assertUpdate(NameElementCommandUpdate nameElementCommand, int expectedResponseCode) {
        NameElement[] nameElements = assertUpdate(AuthorizationChoice.NONE, object2Json(new NameElementCommandUpdate[] {nameElementCommand}), expectedResponseCode);
        return nameElements != null
                ? nameElements[0]
                : null;
    }
    /**
     * @see ITUtilNames#assertUpdate(AuthorizationChoice, String, int)
     */
    public static NameElement[] assertUpdate(NameElementCommandUpdate[] nameElementCommands) {
        return assertUpdate(AuthorizationChoice.NONE, object2Json(nameElementCommands), HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to update json (name elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     * @return updated name element
     */
    public static NameElement[] assertUpdate(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        NameElement[] nameElements = null;

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPutPathJson(authorizationChoice, EndpointChoice.NAMES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                nameElements = mapper.readValue(response[1], NameElement[].class);

                for (NameElement updatedNameElement : nameElements) {
                    assertNotNull(updatedNameElement.getUuid());
                    assertEquals(Boolean.FALSE, updatedNameElement.isDeleted());
                    assertNotNull(updatedNameElement.getWhen());
                }
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return nameElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilNames#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(NameElementCommandConfirm nameElementCommand) {
        assertDelete(AuthorizationChoice.NONE, object2Json(new NameElementCommandConfirm[] {nameElementCommand}), HttpURLConnection.HTTP_NO_CONTENT);
    }
    /**
     * @see ITUtilNames#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(NameElementCommandConfirm nameElementCommand, int expectedResponseCode) {
        assertDelete(AuthorizationChoice.NONE, object2Json(new NameElementCommandConfirm[] {nameElementCommand}), expectedResponseCode);
    }
    /**
     * @see ITUtilNames#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(NameElementCommandConfirm[] nameElementCommands) {
        assertDelete(AuthorizationChoice.NONE, object2Json(nameElementCommands), HttpURLConnection.HTTP_NO_CONTENT);
    }
    /**
     * Utility method to delete json (name elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     */
    public static void assertDelete(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlDeletePathJson(authorizationChoice, EndpointChoice.NAMES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

}

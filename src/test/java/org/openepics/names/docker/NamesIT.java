/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.NameElementUtil;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test names endpoints.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class NamesIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
    private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineEMR   = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceTypeFS    = null;
    private static UUID deviceTypeRFA   = null;
    private static UUID deviceTypeTT    = null;

    @BeforeAll
    public static void initAll() {
        // setup
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010PRL", null, "01 Phase Reference Line"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "N1U1",   null, "Power switch board 01. Electrical power cabinets")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010PRL = structureElements[0].getUuid();
        subsystem010    = structureElements[1].getUuid();
        subsystemN1U1   = structureElements[2].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR", null, "Electromagnetic Resonators")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR, null, null, "Control")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "TT",  null, "Temperature Transmitter")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceTypeFS  = structureElements[0].getUuid();
        deviceTypeRFA = structureElements[1].getUuid();
        deviceTypeTT  = structureElements[2].getUuid();

        // check content
        ITUtilStructures.assertRead("",                  10);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    3);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   3);
        ITUtilNames.assertRead("", 5);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + NamesIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void existsName() {
        // test if name exists

        ITUtilNames.assertExists("RFQ-010PRL:EMR-RFA-071", Boolean.FALSE);
        ITUtilNames.assertExists("RFQ-010PRL",             Boolean.TRUE);
    }

    @Test
    void isValidToCreateName() {
        // test if name is valid to create

        ITUtilNames.assertIsValidToCreate("RFQ-010PRL:EMR-RFA-071", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ-010PRL",             Boolean.FALSE);
    }

    @Test
    void checkCreate() {
        // test conditions for create name
        //     not create itself
        //
        // validate create
        //         system structure
        //     or  system structure + device structure + index
        //     +
        //     description

        NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();

        ITUtilNames.assertExists("RFQ-010PRL:EMR-RFA-051", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ-010PRL:EMR-RFA-051", Boolean.TRUE);

        // validate create

        ITUtilNames.assertValidate("[{asdf]", NameCommand.CREATE, HttpURLConnection.HTTP_BAD_REQUEST);
        ITUtilNames.assertCreate("[{asdf]", HttpURLConnection.HTTP_BAD_REQUEST);

        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // attributes - name - system structure
        //     subsystem
        //     description

        nameElementCommandCreate.setDescription("description");
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // convention name exists
        // already exists since subsystem was created
        nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);

        // attributes - name - system structure + device structure + index
        //     subsystem, device type, index
        //     description

        nameElementCommandCreate.setIndex("051");
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandCreate.setParentDeviceStructure(deviceTypeRFA);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.TRUE);

        // ----------

        nameElementCommandCreate.setParentSystemStructure(null);
        nameElementCommandCreate.setIndex(null);
        nameElementCommandCreate.setParentDeviceStructure(null);

        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // attributes - name - system structure
        //     system
        //     description

        // convention name exists
        // already exists since system was created
        nameElementCommandCreate.setParentSystemStructure(systemRFQ);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);

        // attributes - name - system structure + device structure + index
        //     system, device type, index
        //     description

        nameElementCommandCreate.setIndex("051");
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandCreate.setParentDeviceStructure(deviceTypeRFA);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.TRUE);

        // ----------

        nameElementCommandCreate.setParentSystemStructure(null);
        nameElementCommandCreate.setIndex(null);
        nameElementCommandCreate.setParentDeviceStructure(null);

        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // attributes - name - system structure
        //     system group
        //     description

        // convention name exists
        // already exists since system group was created
        nameElementCommandCreate.setParentSystemStructure(systemGroupAcc);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);

        // attributes - name - system structure + device structure + index
        //     system group, device type, index
        //     description

        nameElementCommandCreate.setIndex("051");
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandCreate.setParentDeviceStructure(deviceTypeTT);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.TRUE);

        nameElementCommandCreate.setParentDeviceStructure(deviceTypeRFA);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.TRUE);

        // ----------

        nameElementCommandCreate.setParentSystemStructure(null);
        nameElementCommandCreate.setIndex(null);
        nameElementCommandCreate.setParentDeviceStructure(null);

        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // attributes - name - system structure + device structure + index
        //     system, device type, index
        //     description

        // convention name exists
        // already exists since system was created
        nameElementCommandCreate.setParentSystemStructure(systemRFQ);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);

        nameElementCommandCreate.setParentDeviceStructure(deviceTypeRFA);
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandCreate.setIndex("051");
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.TRUE);

        // random uuid

        nameElementCommandCreate.setParentSystemStructure(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_NOT_FOUND);

        nameElementCommandCreate.setParentSystemStructure(systemRFQ);
        nameElementCommandCreate.setParentDeviceStructure(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_NOT_FOUND);
    }

    @Test
    void create() {
        // test create name
        //     with and without index

        try {
            ObjectMapper mapper = new ObjectMapper();
            NameElementCommandCreate nameElementCommandCreate  = null;
            NameElementCommandCreate nameElementCommandCreate2 = null;

            nameElementCommandCreate  = new NameElementCommandCreate(subsystem010PRL, deviceTypeRFA, "052", "description");
            nameElementCommandCreate2 = new NameElementCommandCreate(subsystem010PRL, deviceTypeRFA, "061", "description");

            ITUtilNames.assertExists("RFQ-010PRL:EMR-RFA-052", Boolean.FALSE);
            ITUtilNames.assertIsValidToCreate("RFQ-010PRL:EMR-RFA-052", Boolean.TRUE);

            ITUtilNames.assertValidate(
                    "[" + mapper.writeValueAsString(nameElementCommandCreate) + "," + mapper.writeValueAsString(nameElementCommandCreate2) + "]",
                    NameCommand.CREATE, HttpURLConnection.HTTP_OK);

            ITUtilNames.assertValidate(nameElementCommandCreate,  NameCommand.CREATE, Boolean.TRUE);
            ITUtilNames.assertValidate(nameElementCommandCreate2, NameCommand.CREATE, Boolean.TRUE);

            // create
            ITUtilNames.assertCreate(nameElementCommandCreate);

            ITUtilNames.assertExists("RFQ-010PRL:EMR-RFA-052", Boolean.TRUE);
            ITUtilNames.assertIsValidToCreate("RFQ-010PRL:EMR-RFA-052", Boolean.FALSE);

            ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, Boolean.FALSE);
            ITUtilNames.assertCreate(nameElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    void checkUpdate() {
        // test conditions for update name
        //     not update
        //
        // note
        //     create in order to update
        //
        // validate update
        //     uuid
        //     +
        //         system structure
        //     or  system structure + device structure + index
        //     +
        //     description

        NameElementCommandCreate nameElementCommandCreate = null;
        NameElementCommandUpdate nameElementCommandUpdate = null;
        NameElement createdNameElement = null;

        nameElementCommandCreate = new NameElementCommandCreate(subsystem010PRL, deviceTypeRFA, "053", "description");

        // create
        createdNameElement = ITUtilNames.assertCreate(nameElementCommandCreate);
        nameElementCommandUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElement);

        // validate update

        nameElementCommandUpdate.setUuid(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setUuid(createdNameElement.getUuid());
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setDescription(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setDescription("checkUpdate");
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        // ----------

        nameElementCommandUpdate.setParentSystemStructure(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setParentSystemStructure(systemGroupAcc);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setParentSystemStructure(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setParentSystemStructure(systemRFQ);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setParentSystemStructure(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // order of tests not guaranteed

        nameElementCommandUpdate.setParentSystemStructure(subsystem010PRL);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        // ----------

        nameElementCommandUpdate.setParentDeviceStructure(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // convention name exists
        // already exists since subsystem was created
        nameElementCommandUpdate.setIndex(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_CONFLICT);

        nameElementCommandUpdate.setIndex("053");
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setParentDeviceStructure(deviceGroupEMR);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);

        nameElementCommandUpdate.setParentDeviceStructure(deviceTypeFS);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setParentDeviceStructure(deviceTypeTT);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setParentDeviceStructure(deviceTypeRFA);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setIndex(null);
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandUpdate.setIndex("053");
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        // system structure, device structure not used for validation
        // random uuid

        nameElementCommandUpdate.setUuid(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);
        nameElementCommandUpdate.setUuid(createdNameElement.getUuid());

        nameElementCommandUpdate.setParentSystemStructure(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);
        nameElementCommandUpdate.setParentSystemStructure(subsystem010PRL);

        nameElementCommandUpdate.setParentDeviceStructure(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);
    }

    @Test
    void update() {
        // test update name
        //
        // note
        //     create in order to update

        NameElementCommandCreate nameElementCommandCreate = null;
        NameElementCommandUpdate nameElementCommandUpdate = null;
        NameElementCommand nameElementCommand = null;
        NameElement createdNameElement = null;

        nameElementCommand = new NameElementCommand(null, subsystem010PRL, deviceTypeRFA, "054", "description");

        nameElementCommandCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommand);
        nameElementCommandUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommand);

        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);

        // create
        createdNameElement = ITUtilNames.assertCreate(nameElementCommandCreate);

        // status code
        nameElementCommandUpdate.setUuid(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertUpdate(nameElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);
        nameElementCommandUpdate.setUuid(createdNameElement.getUuid());

        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        nameElementCommandUpdate.setDescription("updated description");

        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);

        // update
        ITUtilNames.assertUpdate(nameElementCommandUpdate);
        nameElementCommandUpdate.setDescription("another description");

        ITUtilNames.assertValidate(nameElementCommandUpdate, NameCommand.UPDATE, Boolean.TRUE);
    }

    @Test
    void checkDelete() {
        // test conditions for delete name
        //     not delete
        //
        // note
        //     create in order to delete
        //
        // validate delete
        //     uuid

        NameElementCommandCreate nameElementCommandCreate = null;
        NameElementCommandConfirm nameElementCommandConfirm = null;
        NameElement createdNameElement = null;

        nameElementCommandCreate = new NameElementCommandCreate(subsystem010PRL, deviceTypeRFA, "055", "description");

        // create
        createdNameElement = ITUtilNames.assertCreate(nameElementCommandCreate);
        nameElementCommandConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElement);

        // validate delete

        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.TRUE);

        nameElementCommandConfirm.setUuid(null);
        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.FALSE);
        ITUtilNames.assertDelete(nameElementCommandConfirm, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        nameElementCommandConfirm.setUuid(UUID.randomUUID());
        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.FALSE);
        ITUtilNames.assertDelete(nameElementCommandConfirm, HttpURLConnection.HTTP_NOT_FOUND);

        nameElementCommandConfirm.setUuid(createdNameElement.getUuid());
        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void delete() {
        // test delete name
        //
        // note
        //     create in order to delete

        NameElementCommandCreate nameElementCommandCreate = null;
        NameElementCommandConfirm nameElementCommandConfirm = null;
        NameElement createdNameElement = null;

        nameElementCommandCreate = new NameElementCommandCreate(subsystem010PRL, deviceTypeRFA, "056", "description");

        ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.DELETE, Boolean.FALSE);

        // create
        createdNameElement = ITUtilNames.assertCreate(nameElementCommandCreate);
        nameElementCommandConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElement);

        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.TRUE);

        // delete
        ITUtilNames.assertDelete(nameElementCommandConfirm);

        ITUtilNames.assertValidate(nameElementCommandConfirm, NameCommand.DELETE, Boolean.FALSE);
        ITUtilNames.assertDelete(nameElementCommandConfirm, HttpURLConnection.HTTP_NOT_FOUND);
    }

    @Test
    void readSearchHistoryDeleted() {
        // test read names in various ways for create
        //     search
        //   ( latest )
        //     history
        //     deleted
        //
        // note
        //     create (and more) to read (with content)

        NameElementCommandUpdate nameElementCommandUpdate = null;
        NameElementCommandConfirm nameElementCommandConfirm = null;
        NameElement nameElement1, nameElement7, nameElement8 = null;
        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElement[] createdNameElements = null;
        ResponsePageNameElements response, response2 = null;
        UUID uuid, uuid2 = null;

        // create
        nameElementCommandsCreate = new NameElementCommandCreate[] {
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "001", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "002", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "003", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "004", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "005", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "006", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "007", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "008", "description")
        };
        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        nameElement1 = createdNameElements[0];
        uuid = nameElement1.getUuid();
        uuid2 = createdNameElements[1].getUuid();
        nameElement7 = createdNameElements[6];
        nameElement8 = createdNameElements[7];

        // update element 1 twice
        nameElementCommandUpdate = NameElementUtil.convertElement2CommandUpdate(nameElement1);
        nameElementCommandUpdate.setDescription("updated description");

        ITUtilNames.assertUpdate(nameElementCommandUpdate);

        nameElementCommandUpdate.setDescription("updated description again");

        ITUtilNames.assertUpdate(nameElementCommandUpdate);

        // delete element 7, 8
        nameElementCommandConfirm = NameElementUtil.convertElement2CommandConfirm(nameElement7);
        ITUtilNames.assertDelete(nameElementCommandConfirm);
        nameElementCommandConfirm = NameElementUtil.convertElement2CommandConfirm(nameElement8);
        ITUtilNames.assertDelete(nameElementCommandConfirm);

        // ----------------------------------------------------------------------------------------------------
        // read & search
        //     deleted, uuid, name, systemStructure, deviceStructure, index, description
        //     combination
        //     pagination
        //         page, pageSize
        //         default pageSize 100
        //     sorting
        //         FieldStructure
        //         data content give what kind of sorting may be done and tested in this way
        //         compare in query result for sorting asc and desc
        //             content for field in first and last items    -    always possible
        //             first and last items                         -    not always possible

        ITUtilNames.assertRead("",                                                 13, -1);

        ITUtilNames.assertRead("?deleted=false",                                   11, -1);
        ITUtilNames.assertRead("?deleted=true",                                     2, -1);

        ITUtilNames.assertRead("?name=RFQ-010:EMR-FS-005",                          1);
        ITUtilNames.assertRead("?name=RFQ-010%",                                   10, -1);
        ITUtilNames.assertRead("?name=RFQ-10%",                                     0);

        ITUtilNames.assertRead("?systemStructure=RFQ-010",                          9, -1);
        ITUtilNames.assertRead("?systemStructure=RFQ-010&deleted=false",            7, -1);
        ITUtilNames.assertRead("?systemStructure=RFQ-010&deleted=true",             2, -1);
        ITUtilNames.assertRead("?systemStructure=RFQ-0",                            0);
        ITUtilNames.assertRead("?systemStructure=RFQ-0__",                          9, -1);
        ITUtilNames.assertRead("?systemStructure=RFQ-N1U1",                         1, -1);

        ITUtilNames.assertRead("?deviceStructure=EMR-FS",                           8);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&deleted=false",             6);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&deleted=true",              2);
        ITUtilNames.assertRead("?deviceStructure=EMR-F",                            0);
        ITUtilNames.assertRead("?deviceStructure=EMR-F_",                           8);
        ITUtilNames.assertRead("?deviceStructure=EMR-TT",                           0);

        ITUtilNames.assertRead("?index=003", 1);

        ITUtilNames.assertRead("?description=description",                          7, -1);
        ITUtilNames.assertRead("?description=%description%",                        8, -1);
        ITUtilNames.assertRead("?description=updated description",                  0, -1);
        ITUtilNames.assertRead("?description=updated description%",                 1, -1);
        ITUtilNames.assertRead("?description=updated description again",            1);

        ITUtilNames.assertRead("?who=",                                            13, -1);
        ITUtilNames.assertRead("?who=test",                                         0);

        ITUtilNames.assertRead("?deviceStructure=EMR-FS&index=003",                 1);

        // order by
        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=UUID&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=UUID&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getUuid(),  response2.getList().get(response2.getList().size()-1).getUuid());
        assertEquals(response2.getList().get(0).getUuid(),  response.getList().get(response.getList().size()-1).getUuid());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=NAME&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=NAME&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getName(),  response2.getList().get(response2.getList().size()-1).getName());
        assertEquals(response2.getList().get(0).getName(),  response.getList().get(response.getList().size()-1).getName());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=SYSTEMSTRUCTURE&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=SYSTEMSTRUCTURE&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getSystemStructure(),  response2.getList().get(response2.getList().size()-1).getSystemStructure());
        assertEquals(response2.getList().get(0).getSystemStructure(),  response.getList().get(response.getList().size()-1).getSystemStructure());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=DEVICESTRUCTURE&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=DEVICESTRUCTURE&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getDeviceStructure(),  response2.getList().get(response2.getList().size()-1).getDeviceStructure());
        assertEquals(response2.getList().get(0).getDeviceStructure(),  response.getList().get(response.getList().size()-1).getDeviceStructure());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=INDEX&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=INDEX&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getIndex(),  response2.getList().get(response2.getList().size()-1).getIndex());
        assertEquals(response2.getList().get(0).getIndex(),  response.getList().get(response.getList().size()-1).getIndex());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=DESCRIPTION&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=DESCRIPTION&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getDescription(),  response2.getList().get(response2.getList().size()-1).getDescription());
        assertEquals(response2.getList().get(0).getDescription(),  response.getList().get(response.getList().size()-1).getDescription());

        response  = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=WHEN&isAsc=true");
        response2 = ITUtilNames.assertRead("?deviceStructure=EMR-FS&orderBy=WHEN&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());
        assertEquals(response2.getList().get(0).getWhen().toString(),  response.getList().get(response.getList().size()-1).getWhen().toString());

        // pagination
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=0&pageSize=100",                                        8);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=1&pageSize=100",                                        0);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=0&pageSize=8",                                          8);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=1&pageSize=8",                                          0);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=0&pageSize=3",                                          3);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=1&pageSize=3",                                          3);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=2&pageSize=3",                                          2);
        ITUtilNames.assertRead("?deviceStructure=EMR-FS&page=3&pageSize=3",                                          0);

        // order by, pagination
        response  = ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=true&page=0&pageSize=3",                   3);
        response2 = ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=false&page=2&pageSize=3",                  2);
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());
        response  = ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=false&page=0&pageSize=3",                  3);
        response2 = ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=true&page=2&pageSize=3",                   2);
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());

        // uuid
        //     /{uuid}
        ITUtilNames.assertFind("/" + systemGroupAcc.toString(),      HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + systemRFQ.toString(),           HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + subsystem010.toString(),        HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + disciplineEMR.toString(),       HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + deviceGroupEMR.toString(),      HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + deviceTypeFS.toString(),        HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertFind("/" + uuid.toString());

        // structure uuid
        //     /structure/{uuid}
        //         may be non-trivial to understand - may inspect database to learn
        ITUtilNames.assertRead("/structure/" + systemGroupAcc.toString(),                                           13, -1);
        ITUtilNames.assertRead("/structure/" + systemGroupAcc.toString() + "?deleted=false",                        11, -1);
        ITUtilNames.assertRead("/structure/" + systemGroupAcc.toString() + "?deleted=true",                          2, -1);

        ITUtilNames.assertRead("/structure/" + systemRFQ.toString(),                                                12, -1);
        ITUtilNames.assertRead("/structure/" + systemRFQ.toString() + "?deleted=false",                             10, -1);
        ITUtilNames.assertRead("/structure/" + systemRFQ.toString() + "?deleted=true",                               2, -1);

        ITUtilNames.assertRead("/structure/" + subsystem010.toString(),                                              9);
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?deleted=false",                           7);
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?deleted=true",                            2);

        ITUtilNames.assertRead("/structure/" + subsystemN1U1.toString(),                                             1);
        ITUtilNames.assertRead("/structure/" + subsystemN1U1.toString() + "?deleted=false",                          1);
        ITUtilNames.assertRead("/structure/" + subsystemN1U1.toString() + "?deleted=true",                           0, 0,  HttpURLConnection.HTTP_NOT_FOUND);

        ITUtilNames.assertRead("/structure/" + disciplineEMR.toString(),                                             8, -1);
        ITUtilNames.assertRead("/structure/" + disciplineEMR.toString() + "?deleted=false",                          6, -1);
        ITUtilNames.assertRead("/structure/" + disciplineEMR.toString() + "?deleted=true",                           2, -1);

        ITUtilNames.assertRead("/structure/" + deviceGroupEMR.toString(),                                            8, -1);
        ITUtilNames.assertRead("/structure/" + deviceGroupEMR.toString() + "?deleted=false",                         6, -1);
        ITUtilNames.assertRead("/structure/" + deviceGroupEMR.toString() + "?deleted=true",                          2, -1);

        ITUtilNames.assertRead("/structure/" + deviceTypeFS.toString(),                                              8);
        ITUtilNames.assertRead("/structure/" + deviceTypeFS.toString() + "?deleted=false",                           6);
        ITUtilNames.assertRead("/structure/" + deviceTypeFS.toString() + "?deleted=true",                            2);

        ITUtilNames.assertRead("/structure/" + deviceTypeTT.toString(),                                              0, 0,  HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertRead("/structure/" + deviceTypeTT.toString() + "?deleted=false",                           0, 0,  HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertRead("/structure/" + deviceTypeTT.toString() + "?deleted=true",                            0, 0,  HttpURLConnection.HTTP_NOT_FOUND);

        //     order by
        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=UUID&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=UUID&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getUuid(),  response2.getList().get(response2.getList().size()-1).getUuid());
        assertEquals(response2.getList().get(0).getUuid(),  response.getList().get(response.getList().size()-1).getUuid());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=NAME&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=NAME&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getName(),  response2.getList().get(response2.getList().size()-1).getName());
        assertEquals(response2.getList().get(0).getName(),  response.getList().get(response.getList().size()-1).getName());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=SYSTEMSTRUCTURE&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=SYSTEMSTRUCTURE&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getSystemStructure(),  response2.getList().get(response2.getList().size()-1).getSystemStructure());
        assertEquals(response2.getList().get(0).getSystemStructure(),  response.getList().get(response.getList().size()-1).getSystemStructure());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=DEVICESTRUCTURE&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=DEVICESTRUCTURE&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getDeviceStructure(),  response2.getList().get(response2.getList().size()-1).getDeviceStructure());
        assertEquals(response2.getList().get(0).getDeviceStructure(),  response.getList().get(response.getList().size()-1).getDeviceStructure());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=INDEX&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=INDEX&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getIndex(),  response2.getList().get(response2.getList().size()-1).getIndex());
        assertEquals(response2.getList().get(0).getIndex(),  response.getList().get(response.getList().size()-1).getIndex());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=DESCRIPTION&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=DESCRIPTION&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getDescription(),  response2.getList().get(response2.getList().size()-1).getDescription());
        assertEquals(response2.getList().get(0).getDescription(),  response.getList().get(response.getList().size()-1).getDescription());

        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=true");
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());
        assertEquals(response2.getList().get(0).getWhen().toString(),  response.getList().get(response.getList().size()-1).getWhen().toString());

        //     pagination
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?page=0&pageSize=4",                       4);
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?page=1&pageSize=4",                       4);
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?page=2&pageSize=4",                       1);
        ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?page=3&pageSize=4",                       0, 0,  HttpURLConnection.HTTP_NOT_FOUND);

        //     order by, pagination
        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=true&page=0&pageSize=4",       4);
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=false&page=2&pageSize=4",      1);
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());
        response  = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=false&page=0&pageSize=4",      4);
        response2 = ITUtilNames.assertRead("/structure/" + subsystem010.toString() + "?orderBy=WHEN&isAsc=true&page=2&pageSize=4",       1);
        assertEquals(response.getList().get(0).getWhen().toString(),  response2.getList().get(response2.getList().size()-1).getWhen().toString());

        // history
        //     /history/{uuid}
        ITUtilNames.assertHistory("/" + systemGroupAcc.toString(),  0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + systemRFQ.toString(),       0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + subsystem010.toString(),    0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + disciplineEMR.toString(),   0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + deviceGroupEMR.toString(),  0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + deviceTypeFS.toString(),    0, HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilNames.assertHistory("/" + uuid.toString(),            3);
        ITUtilNames.assertHistory("/" + uuid2.toString(),           1);
    }

}

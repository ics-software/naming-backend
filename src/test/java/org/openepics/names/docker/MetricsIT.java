/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test metrics with Prometheus.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
public class MetricsIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     test if metrics is available

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010    = null;

    private static UUID disciplineEMR   = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceTypeFS    = null;

    @BeforeAll
    public static void initAll() {
        // setup
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;
        NameElementCommandCreate[] nameElementCommandsCreate = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010 = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR", null, "Electromagnetic Resonators")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR, null, null, "Control")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "FS",  null, "Flow Switch")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceTypeFS = structureElements[0].getUuid();

        nameElementCommandsCreate = new NameElementCommandCreate[] {
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "001", "description")
        };
        ITUtilNames.assertCreate(nameElementCommandsCreate);

        // check content
        ITUtilStructures.assertRead("",                   6);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    1);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   1);
        ITUtilNames.assertRead("", 4);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + MetricsIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void actuator() {
        try {
            String address = ITUtil.HTTP_IP_PORT_NAMING + "/actuator";
            int responseCode = ITUtil.doGet(address);

            assertEquals(HttpURLConnection.HTTP_OK, responseCode);
        } catch (IOException e) {
            fail();
        }
    }

    @Test
    void actuatorPrometheus() {
        try {
            String address = ITUtil.HTTP_IP_PORT_NAMING + "/actuator/prometheus";
            int responseCode = ITUtil.doGet(address);

            assertEquals(HttpURLConnection.HTTP_OK, responseCode);
        } catch (IOException e) {
            fail();
        }
    }

}

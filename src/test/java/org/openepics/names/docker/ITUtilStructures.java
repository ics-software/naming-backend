/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;

import org.openepics.names.docker.ITUtil.AuthorizationChoice;
import org.openepics.names.docker.ITUtil.EndpointChoice;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.rest.beans.response.ResponseBoolean;
import org.openepics.names.rest.beans.response.ResponseBooleanList;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.util.StructureCommand;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtilStructures {

    static final ObjectMapper mapper = new ObjectMapper();

    /**
     * This class is not to be instantiated.
     */
    private ITUtilStructures() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * Return string for structure element array.
     *
     * @param value structure element array
     * @return string for structure element array
     */
    static String object2Json(StructureElement[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for structure element command array.
     *
     * @param value structure element command array
     * @return string for structure element command array
     */
    static String object2Json(StructureElementCommandCreate[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for structure element command array.
     *
     * @param value structure element command array
     * @return string for structure element command array
     */
    static String object2Json(StructureElementCommandUpdate[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    /**
     * Return string for structure element command array.
     *
     * @param value structure element command array
     * @return string for structure element command array
     */
    static String object2Json(StructureElementCommandConfirm[] value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertConvertExcel2Json(File, int)
     */
    public static String assertConvertExcel2Json(File file) {
        return assertConvertExcel2Json(file, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to convert Excel to json and assert response code.
     *
     * @param file file
     * @param expectedResponseCode expected response code
     * @return json
     */
    public static String assertConvertExcel2Json(File file, int expectedResponseCode) {
        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathExcel2Json(AuthorizationChoice.NONE, EndpointChoice.CONVERT, "/excel2Json/structures", file));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            return response[1];
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertConvertJson2Excel(StructureElement[], int)
     */
    public static String assertConvertJson2Excel(StructureElement[] structureElements) {
        return assertConvertJson2Excel(structureElements, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to convert json to Excel and assert response code.
     *
     * @param structureElements structure elements
     * @param expectedResponseCode expected response code
     * @return response for Excel file
     */
    public static String assertConvertJson2Excel(StructureElement[] structureElements, int expectedResponseCode) {
        try {
            // response - no content (null) if response is not json
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson2Excel(AuthorizationChoice.NONE, EndpointChoice.CONVERT, "/json2Excel/structures", object2Json(structureElements)));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            return response[1];
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertFind(String, int)
     */
    public static ResponsePageStructureElements assertFind(String queryString) {
        return assertFind(queryString, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to find structure elements.
     *
     * @param queryString query string
     * @param expectedResponseCode expected response code
     * @return response page structure elements
     */
    public static ResponsePageStructureElements assertFind(String queryString, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageStructureElements responsePageStructureElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageStructureElements = mapper.readValue(response[1], ResponsePageStructureElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                assertTrue(responsePageStructureElements.getListSize() > 0);
                assertNotNull(responsePageStructureElements.getList().get(0));
            } else if (HttpURLConnection.HTTP_NOT_FOUND == expectedResponseCode){
                assertEquals(0, responsePageStructureElements.getListSize());
            }

            return responsePageStructureElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * @see ITUtilStructures#assertRead(String, int, int, int)
     */
    public static ResponsePageStructureElements assertRead(String queryString) {
        return assertRead(queryString, -1, -1, HttpURLConnection.HTTP_OK);
    }
    /**
     * @see ITUtilStructures#assertRead(String, int, int, int)
     */
    public static ResponsePageStructureElements assertRead(String queryString, int expectedEqual) {
        return assertRead(queryString, expectedEqual, expectedEqual, HttpURLConnection.HTTP_OK);
    }
    /**
     * @see ITUtilStructures#assertRead(String, int, int, int)
     */
    public static ResponsePageStructureElements assertRead(String queryString, int expectedGreaterThanOrEqual, int expectedLessThanOrEqual) {
        return assertRead(queryString, expectedGreaterThanOrEqual, expectedLessThanOrEqual, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to read structure elements and assert number of items in list.
     *
     * @param queryString query string
     * @param expectedGreaterThanOrEqual (if non-negative number) greater than or equal to this number of items
     * @param expectedLessThanOrEqual (if non-negative number) less than or equal to this number of items
     * @param expectedResponseCode expected response code
     * @return response page structure elements
     */
    public static ResponsePageStructureElements assertRead(String queryString, int expectedGreaterThanOrEqual, int expectedLessThanOrEqual, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageStructureElements responsePageStructureElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageStructureElements = mapper.readValue(response[1], ResponsePageStructureElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                // expected number of items in list
                //     (if non-negative number)
                //     expectedGreaterThanOrEqual <= nbr of items <= expectedLessThanOrEqual
                if (expectedGreaterThanOrEqual >= 0) {
                    assertTrue(responsePageStructureElements.getListSize() >= expectedGreaterThanOrEqual);
                }
                if (expectedLessThanOrEqual >= 0) {
                    assertTrue(responsePageStructureElements.getListSize() <= expectedLessThanOrEqual);
                }
            }

            return responsePageStructureElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * @see ITUtilStructures#assertHistory(String, int, int)
     */
    public static ResponsePageStructureElements assertHistory(String queryString, int expectedEqual) {
        return assertHistory(queryString, expectedEqual, HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to read history for structure elements and assert number of items in list.
     *
     * @param queryString query string
     * @param expectedEqual expected number of items (if non-negative number)
     * @param expectedResponseCode expected response code
     * @return response page structure elements
     */
    public static ResponsePageStructureElements assertHistory(String queryString, int expectedEqual, int expectedResponseCode) {
        try {
            String[] response = null;
            ResponsePageStructureElements responsePageStructureElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/history" + queryString);
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            responsePageStructureElements = mapper.readValue(response[1], ResponsePageStructureElements.class);

            // response code and value
            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                // expected number of items in list
                //     (if non-negative number)
                //     expectedGreaterThanOrEqual <= nbr of items <= expectedLessThanOrEqual
                if (expectedEqual >= 0) {
                    assertTrue(responsePageStructureElements.getListSize() >= expectedEqual);
                }
                if (expectedEqual >= 0) {
                    assertTrue(responsePageStructureElements.getListSize() <= expectedEqual);
                }
            }

            return responsePageStructureElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

    /**
     * Utility method to check if structure exists.
     *
     * @param type type
     * @param mnemonicPath mnemonic path
     * @param expected expected response
     */
    public static void assertExists(Type type, String mnemonicPath, Boolean expected) {
        try {
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/exists/" + type.toString() + "/" + mnemonicPath);
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), expected);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Utility method to check if structure is valid to create.
     * Method will not produce a message.
     *
     * @param type type
     * @param mnemonicPath mnemonic path
     * @param expected expected response
     */
    public static void assertIsValidToCreate(Type type, String mnemonicPath, Boolean expected) {
        try {
            String[] response = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_STRUCTURES + "/isValidToCreate/" + type.toString() + "/" + mnemonicPath);
            ITUtil.assertResponseLength2CodeOK(response);
            ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBoolean.class), expected);
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandCreate structureElementCommand, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandCreate[] {structureElementCommand}), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandUpdate structureElementCommand, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandUpdate[] {structureElementCommand}), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandConfirm structureElementCommand, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandConfirm[] {structureElementCommand}), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandCreate[] structureElementCommands, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(structureElementCommands), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandUpdate[] structureElementCommands, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(structureElementCommands), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(StructureElementCommandConfirm[] structureElementCommands, StructureCommand structureCommand, Boolean expected) {
        assertValidate(AuthorizationChoice.NONE, object2Json(structureElementCommands), structureCommand, HttpURLConnection.HTTP_OK, expected);
    }
    /**
     * @see ITUtilStructures#assertValidate(AuthorizationChoice, String, StructureCommand, int, Boolean)
     */
    public static void assertValidate(String json, StructureCommand structureCommand, int expectedResponseCode) {
        assertValidate(AuthorizationChoice.NONE, json, structureCommand, expectedResponseCode, null);
    }
    /**
     * Utility method to validate structure elements and assert expected response.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param structureCommand structure command
     * @param expectedResponseCode expected response code
     * @param expected expected response
     */
    public static void assertValidate(AuthorizationChoice authorizationChoice, String json, StructureCommand structureCommand, int expectedResponseCode, Boolean expected) {
        String path = getValidatePath(structureCommand);

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlGetPathJson(authorizationChoice, EndpointChoice.STRUCTURES, path, json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
            if (expected != null) {
                ITUtil.assertEqualsResponseBoolean(mapper.readValue(response[1], ResponseBooleanList.class), expected);
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

    /**
     * Utility method to return validate path for structure command.
     *
     * @param structureCommand structure command
     * @return validate path
     */
    private static String getValidatePath(StructureCommand structureCommand) {
        String validatePath = "";
        switch (structureCommand) {
            case CREATE:
                validatePath = "/validateCreate";
                break;
            case UPDATE:
                validatePath = "/validateUpdate";
                break;
            case DELETE:
                validatePath = "/validateDelete";
                break;
            default:
                break;
        }
        return validatePath;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertCreate(AuthorizationChoice, String, int)
     */
    public static StructureElement assertCreate(StructureElementCommandCreate structureElementCommand) {
        StructureElement[] structureElements = assertCreate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandCreate[] {structureElementCommand}), HttpURLConnection.HTTP_CREATED);
        return structureElements != null
                ? structureElements[0]
                : null;
    }
    /**
     * @see ITUtilStructures#assertCreate(AuthorizationChoice, String, int)
     */
    public static StructureElement assertCreate(StructureElementCommandCreate structureElementCommand, int expectedResponseCode) {
        StructureElement[] structureElements = assertCreate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandCreate[] {structureElementCommand}), expectedResponseCode);
        return structureElements != null
                ? structureElements[0]
                : null;
    }
    /**
     * @see ITUtilStructures#assertCreate(AuthorizationChoice, String, int)
     */
    public static StructureElement[] assertCreate(StructureElementCommandCreate[] structureElementCommands) {
        return assertCreate(AuthorizationChoice.NONE, object2Json(structureElementCommands), HttpURLConnection.HTTP_CREATED);
    }
    /**
     * @see ITUtilStructures#assertCreate(AuthorizationChoice, String, int)
     */
    public static StructureElement[] assertCreate(String json, int expectedResponseCode) {
        return assertCreate(AuthorizationChoice.NONE, json, expectedResponseCode);
    }
    /**
     * Utility method to create json (structure elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     * @return created structure element
     */
    public static StructureElement[] assertCreate(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        StructureElement[] structureElements  = null;

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPostPathJson(authorizationChoice, EndpointChoice.STRUCTURES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            if (HttpURLConnection.HTTP_CREATED == expectedResponseCode) {
                structureElements = mapper.readValue(response[1], StructureElement[].class);

                for (StructureElement createdStructureElement : structureElements) {
                    assertNotNull(createdStructureElement.getUuid());
                    assertEquals(Status.APPROVED, createdStructureElement.getStatus());
                    assertEquals(Boolean.FALSE,   createdStructureElement.isDeleted());
                    assertNotNull(createdStructureElement.getWhen());
                }

                // not set uuid for input parameter(s)
                //     convenience, easy when array length 1, otherwise not easy
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return structureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertUpdate(AuthorizationChoice, String, int)
     */
    public static StructureElement assertUpdate(StructureElementCommandUpdate structureElementCommand) {
        StructureElement[] structureElements = assertUpdate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandUpdate[] {structureElementCommand}), HttpURLConnection.HTTP_OK);
        return structureElements != null
                ? structureElements[0]
                : null;
    }
    /**
     * @see ITUtilStructures#assertUpdate(AuthorizationChoice, String, int)
     */
    public static StructureElement assertUpdate(StructureElementCommandUpdate structureElementCommand, int expectedResponseCode) {
        StructureElement[] structureElements = assertUpdate(AuthorizationChoice.NONE, object2Json(new StructureElementCommandUpdate[] {structureElementCommand}), expectedResponseCode);
        return structureElements != null
                ? structureElements[0]
                : null;
    }
    /**
     * @see ITUtilStructures#assertUpdate(AuthorizationChoice, String, int)
     */
    public static StructureElement[] assertUpdate(StructureElementCommandUpdate[] structureElementCommands) {
        return assertUpdate(AuthorizationChoice.NONE, object2Json(structureElementCommands), HttpURLConnection.HTTP_OK);
    }
    /**
     * Utility method to update json (structure elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     * @return updated structure element
     */
    public static StructureElement[] assertUpdate(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        StructureElement[] structureElements  = null;

        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlPutPathJson(authorizationChoice, EndpointChoice.STRUCTURES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);

            if (HttpURLConnection.HTTP_OK == expectedResponseCode) {
                structureElements = mapper.readValue(response[1], StructureElement[].class);

                for (StructureElement updatedStructureElement : structureElements) {
                    assertNotNull(updatedStructureElement.getUuid());
                    assertEquals(Status.APPROVED, updatedStructureElement.getStatus());
                    assertEquals(Boolean.FALSE,   updatedStructureElement.isDeleted());
                    assertNotNull(updatedStructureElement.getWhen());
                }
            }
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }

        return structureElements;
    }

    // ----------------------------------------------------------------------------------------------------

    /**
     * @see ITUtilStructures#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(StructureElementCommandConfirm structureElementCommand) {
        assertDelete(AuthorizationChoice.NONE, object2Json(new StructureElementCommandConfirm[] {structureElementCommand}), HttpURLConnection.HTTP_NO_CONTENT);
    }
    /**
     * @see ITUtilStructures#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(StructureElementCommandConfirm structureElementCommand, int expectedResponseCode) {
        assertDelete(AuthorizationChoice.NONE, object2Json(new StructureElementCommandConfirm[] {structureElementCommand}), expectedResponseCode);
    }
    /**
     * @see ITUtilStructures#assertDelete(AuthorizationChoice, String, int)
     */
    public static void assertDelete(StructureElementCommandConfirm[] structureElementCommands) {
        assertDelete(AuthorizationChoice.NONE, object2Json(structureElementCommands), HttpURLConnection.HTTP_NO_CONTENT);
    }
    /**
     * Utility method to delete json (structure elements) and assert result and response code.
     *
     * @param authorizationChoice authorization choice (none, user, admin)
     * @param json json
     * @param expectedResponseCode expected response code
     */
    public static void assertDelete(AuthorizationChoice authorizationChoice, String json, int expectedResponseCode) {
        try {
            String[] response = null;

            response = ITUtil.runShellCommand(ITUtil.curlDeletePathJson(authorizationChoice, EndpointChoice.STRUCTURES, "", json));
            ITUtil.assertResponseLength2Code(response, expectedResponseCode);
        } catch (IOException e) {
            fail();
        } catch (InterruptedException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
    }

}

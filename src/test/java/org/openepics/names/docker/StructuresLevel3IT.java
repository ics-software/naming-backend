/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test structures endpoints.
 * In particular create level 3 entries and create all entries before approve all entries.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class StructuresLevel3IT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json
    //
    //     create in order to approve
    //     all create first, all approve separately after all create

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
    private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineCryo  = null;
    private static UUID disciplineEMR   = null;
    private static UUID disciplineHVAC  = null;
    private static UUID disciplineProc  = null;
    private static UUID disciplineSC    = null;
    private static UUID disciplineVac   = null;
    private static UUID disciplineWtrC  = null;
    private static UUID disciplineBMD   = null;

    private static UUID deviceGroupCryo = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceGroupHVAC = null;
    private static UUID deviceGroupProc = null;
    private static UUID deviceGroupSC   = null;
    private static UUID deviceGroupVac  = null;
    private static UUID deviceGroupWtrC = null;
    private static UUID deviceGroupBMD  = null;

    private static UUID deviceType_Cryo_FS  = null;
    private static UUID deviceType_Cryo_IOC = null;
    private static UUID deviceType_Cryo_RFA = null;
    private static UUID deviceType_Cryo_TT  = null;

    private static UUID deviceType_EMR_FS   = null;
    private static UUID deviceType_EMR_IOC  = null;
    private static UUID deviceType_EMR_RFA  = null;
    private static UUID deviceType_EMR_TT   = null;

    private static UUID deviceType_HVAC_FS  = null;
    private static UUID deviceType_HVAC_IOC = null;
    private static UUID deviceType_HVAC_RFA = null;
    private static UUID deviceType_HVAC_TT  = null;

    private static UUID deviceType_Proc_FS  = null;
    private static UUID deviceType_Proc_IOC = null;
    private static UUID deviceType_Proc_RFA = null;
    private static UUID deviceType_Proc_TT  = null;

    private static UUID deviceType_SC_FS    = null;
    private static UUID deviceType_SC_IOC   = null;
    private static UUID deviceType_SC_RFA   = null;
    private static UUID deviceType_SC_TT    = null;

    private static UUID deviceType_Vac_FS   = null;
    private static UUID deviceType_Vac_IOC  = null;
    private static UUID deviceType_Vac_RFA  = null;
    private static UUID deviceType_Vac_TT   = null;

    private static UUID deviceType_WtrC_FS  = null;
    private static UUID deviceType_WtrC_IOC = null;
    private static UUID deviceType_WtrC_RFA = null;
    private static UUID deviceType_WtrC_TT  = null;

    private static UUID deviceType_BMD_FS   = null;
    private static UUID deviceType_BMD_IOC  = null;
    private static UUID deviceType_BMD_RFA  = null;
    private static UUID deviceType_BMD_TT   = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Cryo", null, "Cryogenics"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR",  null, "Electromagnetic Resonators"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "HVAC", null, "Heating, Cooling and Air Conditioning"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Proc", null, "General Process Control"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "SC",   null, "Software Controllers"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Vac",  null, "Vacuum"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "WtrC", null, "Water Cooling"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "BMD",  null, "Beam Magnets and Deflectors")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineCryo = structureElements[0].getUuid();
        disciplineEMR  = structureElements[1].getUuid();
        disciplineHVAC = structureElements[2].getUuid();
        disciplineProc = structureElements[3].getUuid();
        disciplineSC   = structureElements[4].getUuid();
        disciplineVac  = structureElements[5].getUuid();
        disciplineWtrC = structureElements[6].getUuid();
        disciplineBMD  = structureElements[7].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineCryo, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR,  null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineHVAC, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineProc, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineSC,   null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineVac,  null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineWtrC, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineBMD,  null, null, "empty")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupCryo = structureElements[0].getUuid();
        deviceGroupEMR  = structureElements[1].getUuid();
        deviceGroupHVAC = structureElements[2].getUuid();
        deviceGroupProc = structureElements[3].getUuid();
        deviceGroupSC   = structureElements[4].getUuid();
        deviceGroupVac  = structureElements[5].getUuid();
        deviceGroupWtrC = structureElements[6].getUuid();
        deviceGroupBMD  = structureElements[7].getUuid();

        // check content
        ITUtilStructures.assertRead("",                  18);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    0);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   8);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  8);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   0);
        ITUtilNames.assertRead("", 2);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + StructuresLevel3IT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void createSubsystem() {
        // test create subsystem
        //     all create

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010PRL", null, "01 Phase Reference Line"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "N1U1",   null, "Power switch board 01. Electrical power cabinets")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010PRL = structureElements[0].getUuid();
        subsystem010    = structureElements[1].getUuid();
        subsystemN1U1   = structureElements[2].getUuid();

        assertNotNull(subsystem010PRL);
        assertNotNull(subsystem010);
        assertNotNull(subsystemN1U1);
    }

    @Test
    void createDeviceType() {
        // test create device type
        //     all create

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "TT",  null, "Temperature Transmitter")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceType_Cryo_FS  = structureElements[0].getUuid();
        deviceType_Cryo_IOC = structureElements[1].getUuid();
        deviceType_Cryo_RFA = structureElements[2].getUuid();
        deviceType_Cryo_TT  = structureElements[3].getUuid();
        deviceType_EMR_FS   = structureElements[4].getUuid();
        deviceType_EMR_IOC  = structureElements[5].getUuid();
        deviceType_EMR_RFA  = structureElements[6].getUuid();
        deviceType_EMR_TT   = structureElements[7].getUuid();
        deviceType_HVAC_FS  = structureElements[8].getUuid();
        deviceType_HVAC_IOC = structureElements[9].getUuid();
        deviceType_HVAC_RFA = structureElements[10].getUuid();
        deviceType_HVAC_TT  = structureElements[11].getUuid();
        deviceType_Proc_FS  = structureElements[12].getUuid();
        deviceType_Proc_IOC = structureElements[13].getUuid();
        deviceType_Proc_RFA = structureElements[14].getUuid();
        deviceType_Proc_TT  = structureElements[15].getUuid();
        deviceType_SC_FS    = structureElements[16].getUuid();
        deviceType_SC_IOC   = structureElements[17].getUuid();
        deviceType_SC_RFA   = structureElements[18].getUuid();
        deviceType_SC_TT    = structureElements[19].getUuid();
        deviceType_Vac_FS   = structureElements[20].getUuid();
        deviceType_Vac_IOC  = structureElements[21].getUuid();
        deviceType_Vac_RFA  = structureElements[22].getUuid();
        deviceType_Vac_TT   = structureElements[23].getUuid();
        deviceType_WtrC_FS  = structureElements[24].getUuid();
        deviceType_WtrC_IOC = structureElements[25].getUuid();
        deviceType_WtrC_RFA = structureElements[26].getUuid();
        deviceType_WtrC_TT  = structureElements[27].getUuid();
        deviceType_BMD_FS   = structureElements[28].getUuid();
        deviceType_BMD_IOC  = structureElements[29].getUuid();
        deviceType_BMD_RFA  = structureElements[30].getUuid();
        deviceType_BMD_TT   = structureElements[31].getUuid();

        assertNotNull(deviceType_Cryo_FS);
        assertNotNull(deviceType_Cryo_IOC);
        assertNotNull(deviceType_Cryo_RFA);
        assertNotNull(deviceType_Cryo_TT);
        assertNotNull(deviceType_EMR_FS);
        assertNotNull(deviceType_EMR_IOC);
        assertNotNull(deviceType_EMR_RFA);
        assertNotNull(deviceType_EMR_TT);
        assertNotNull(deviceType_HVAC_FS);
        assertNotNull(deviceType_HVAC_IOC);
        assertNotNull(deviceType_HVAC_RFA);
        assertNotNull(deviceType_HVAC_TT);
        assertNotNull(deviceType_Proc_FS);
        assertNotNull(deviceType_Proc_IOC);
        assertNotNull(deviceType_Proc_RFA);
        assertNotNull(deviceType_Proc_TT);
        assertNotNull(deviceType_SC_FS);
        assertNotNull(deviceType_SC_IOC);
        assertNotNull(deviceType_SC_RFA);
        assertNotNull(deviceType_SC_TT);
        assertNotNull(deviceType_Vac_FS);
        assertNotNull(deviceType_Vac_IOC);
        assertNotNull(deviceType_Vac_RFA);
        assertNotNull(deviceType_Vac_TT);
        assertNotNull(deviceType_WtrC_FS);
        assertNotNull(deviceType_WtrC_IOC);
        assertNotNull(deviceType_WtrC_RFA);
        assertNotNull(deviceType_WtrC_TT);
        assertNotNull(deviceType_BMD_FS);
        assertNotNull(deviceType_BMD_IOC);
        assertNotNull(deviceType_BMD_RFA);
        assertNotNull(deviceType_BMD_TT);

        // possibly combine with readStructures
    }

}

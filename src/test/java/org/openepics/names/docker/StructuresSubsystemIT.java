/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.HttpURLConnection;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test structures endpoints and subsystem.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class StructuresSubsystemIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json
    //
    //     history
    //         mnemonic path does not make same sense for history
    //         (very) tricky to find mnemonic path for uuid at proper time (history)
    //         therefore empty mnemonic path for history for structure
    //         one history entry if less than one second between requested and processed, otherwise two history entries

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupUuid = null;
    private static UUID systemUuid = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Sg", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupUuid  = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupUuid, "Sys", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemUuid  = structureElements[0].getUuid();

        // check content
        ITUtilStructures.assertRead("",                   2);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    0);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   0);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  0);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   0);
        ITUtilNames.assertRead("", 2);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + StructuresSubsystemIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void checkCreate() {
        // test conditions for create subsystem
        //     not create itself
        //
        // validate create
        //     type
        //     parent
        //     name
        //     mnemonic
        //     description

        StructureElementCommandCreate structureElementCommandCreate = new StructureElementCommandCreate();

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Cc", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "CC", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sg-Sys-Sg",  Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sg-Sys-Sys", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sg-Sys-Cc",  Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sg-Cc",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sg-CC",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Cc",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-CC",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "SYS-Cc",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "SYS-CC",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Sg",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-SG",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Sys",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-SYS",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Cc",         Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "CC",         Boolean.FALSE);

        ITUtilStructures.assertValidate("[{asdf]", StructureCommand.CREATE, HttpURLConnection.HTTP_BAD_REQUEST);
        ITUtilStructures.assertCreate("[{asdf]", HttpURLConnection.HTTP_BAD_REQUEST);

        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setType(Type.SUBSYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setParent(systemUuid);
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("Cc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setOrdering(41);
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setDescription("description");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setType(null);
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);
    }

    @Test
    void checkCreateMnemonic() {
        // test conditions for create subsystem
        //     not create itself
        //
        // note
        //     mnemonic

        StructureElementCommandCreate structureElementCommandCreate = new StructureElementCommandCreate();

        structureElementCommandCreate.setType(Type.SUBSYSTEM);
        structureElementCommandCreate.setParent(systemUuid);
        structureElementCommandCreate.setDescription("description");

        // mnemonic rules

        structureElementCommandCreate.setMnemonic(null);
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("C");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Cc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Ccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Cccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Ccccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Cccccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Ccccccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Cccccccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Ccccccccc");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        // mnemonic rules (2)

        structureElementCommandCreate.setMnemonic(" ");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("Sub ");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("Sub");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("000");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("Sub0");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic(":");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("Sub:");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("Sub:   ");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandCreate.setMnemonic("1");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("12");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("123");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("1234");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("12345");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("123456");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("1234567");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("12345678");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandCreate.setMnemonic("123456789");
        ITUtilStructures.assertValidate(structureElementCommandCreate, StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);
    }

    @Test
    void create() {
        // test create subsystem

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElementCommandConfirm structureElementCommandConfirm = null;
        StructureElementCommand structureElementCommand = null;
        StructureElement structureElement = null;

        structureElementCommand = new StructureElementCommand(null, Type.SUBSYSTEM, systemUuid, "Ca", null, "description");

        structureElementCommandCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommand);
        structureElementCommandUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommand);
        structureElementCommandConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommand);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Ca", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Ca", Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        structureElementCommandConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElement);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Ca", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Ca", Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);
    }

    @Test
    void checkUpdate() {
        // test conditions for update subsystem
        //     not update
        //
        // note
        //     create in order to update
        //
        // validate update
        //     uuid
        //     type
        //     parent
        //     name
        //     mnemonic
        //     description

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElement structureElement = null;
        UUID uuid = null;

        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Cu", null, "description");

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        uuid = structureElement.getUuid();

        // validate update

        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setUuid(null);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandUpdate.setUuid(UUID.randomUUID());
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);

        structureElementCommandUpdate.setUuid(uuid);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setType(null);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandUpdate.setType(Type.DEVICETYPE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, HttpURLConnection.HTTP_NOT_FOUND);

        structureElementCommandUpdate.setType(Type.SUBSYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setParent(null);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandUpdate.setParent(systemUuid);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setMnemonic(null);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandUpdate.setMnemonic("Cu");
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setOrdering(41);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandUpdate.setDescription(null);
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertUpdate(structureElementCommandUpdate, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandUpdate.setDescription("description");
        ITUtilStructures.assertValidate(structureElementCommandUpdate, StructureCommand.UPDATE, Boolean.TRUE);
    }

    @Test
    void update() {
        // test update subsystem
        //
        // note
        //     create in order to update

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElementCommandConfirm structureElementCommandConfirm = null;
        StructureElement structureElement = null;

        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Ua", null, "description");

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        structureElementCommandConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElement);

        structureElementCommandUpdate.setDescription("description update check");

        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);

        // update
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void checkDelete() {
        // test conditions for delete subsystem
        //     not delete
        //
        // note
        //     create in order to delete
        //
        // validate delete
        //     uuid
        //     type

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandConfirm structureElementCommandConfirm = null;
        StructureElement structureElement = null;
        UUID uuid = null;

        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Cd", null, "description");

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElement);
        uuid = structureElement.getUuid();

        // validate delete

        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);

        structureElementCommandConfirm.setUuid(null);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);
        ITUtilStructures.assertDelete(structureElementCommandConfirm, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandConfirm.setUuid(UUID.randomUUID());
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);
        ITUtilStructures.assertDelete(structureElementCommandConfirm, HttpURLConnection.HTTP_NOT_FOUND);

        structureElementCommandConfirm.setUuid(uuid);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);

        structureElementCommandConfirm.setType(null);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);
        ITUtilStructures.assertDelete(structureElementCommandConfirm, ITUtil.HTTP_UNPROCESSABLE_ENTITY);

        structureElementCommandConfirm.setType(Type.DEVICETYPE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertDelete(structureElementCommandConfirm, HttpURLConnection.HTTP_NOT_FOUND);

        structureElementCommandConfirm.setType(Type.SUBSYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void delete() {
        // test delete subsystem
        //
        // note
        //     create in order to delete, approve

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElementCommandConfirm structureElementCommandConfirm = null;
        StructureElement structureElement = null;

        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Da", null, "description");

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        structureElementCommandConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElement);

        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);

        // delete
        ITUtilStructures.assertDelete(structureElementCommandConfirm);

        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);
        ITUtilStructures.assertDelete(structureElementCommandConfirm, HttpURLConnection.HTTP_CONFLICT);
    }

    @Test
    void readSearch() {
        // test read subsystem in various ways
        //
        // note
        //     create in order to read

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElement structureElement  = null;
        int count = 0;

        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Rshs", 41, "description");

        // read (1)
        count = ITUtilStructures.assertRead("?type=SUBSYSTEM").getListSize();

        ITUtilStructures.assertRead("?type=SUBSYSTEM",                                             0, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=Rshs",                               0);

        ITUtilStructures.assertRead("?mnemonic=Sg",                                                1);
        ITUtilStructures.assertRead("?mnemonic=Sys",                                               1);
        ITUtilStructures.assertRead("?mnemonic=Rshs",                                              0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys",                                            0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs",                                          0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs",                                       0);
        ITUtilStructures.assertRead("?mnemonic=Sg&deleted=false",                                  1);
        ITUtilStructures.assertRead("?mnemonic=Sys&deleted=false",                                 1);
        ITUtilStructures.assertRead("?mnemonic=Rshs&deleted=false",                                0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys&deleted=false",                              0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs&deleted=false",                            0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs&deleted=false",                         0);
        ITUtilStructures.assertRead("?mnemonic=Sg&deleted=true",                                   0);
        ITUtilStructures.assertRead("?mnemonic=Sys&deleted=true",                                  0);
        ITUtilStructures.assertRead("?mnemonic=Rshs&deleted=true",                                 0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys&deleted=true",                               0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs&deleted=true",                             0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs&deleted=true",                          0);

        ITUtilStructures.assertRead("?mnemonicPath=Sg",                                            1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys",                                           1);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs",                                          0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys",                                        0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs",                                      0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs",                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg&deleted=false",                              1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys&deleted=false",                             1);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs&deleted=false",                            0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=false",                          0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs&deleted=false",                        0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs&deleted=false",                     0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg&deleted=true",                               0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys&deleted=true",                              0);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs&deleted=true",                             0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=true",                           0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs&deleted=true",                         0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs&deleted=true",                      0);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Rshs",        Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Rshs",    Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sg-Sys-Rshs", Boolean.FALSE);

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);

        // read (2)
        assertEquals(count + 1, ITUtilStructures.assertRead("?type=SUBSYSTEM", 1, -1).getListSize());

        ITUtilStructures.assertRead("?type=SUBSYSTEM",                                             1, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=Rshs",                               1);

        ITUtilStructures.assertFind("/" + structureElement.getUuid().toString());

        ITUtilStructures.assertRead("?mnemonic=Sg",                                                1);
        ITUtilStructures.assertRead("?mnemonic=Sys",                                               1);
        ITUtilStructures.assertRead("?mnemonic=Rshs",                                              1);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys",                                            0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs",                                          0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs",                                       0);
        ITUtilStructures.assertRead("?mnemonic=Sg&deleted=false",                                  1);
        ITUtilStructures.assertRead("?mnemonic=Sys&deleted=false",                                 1);
        ITUtilStructures.assertRead("?mnemonic=Rshs&deleted=false",                                1);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys&deleted=false",                              0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs&deleted=false",                            0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs&deleted=false",                         0);
        ITUtilStructures.assertRead("?mnemonic=Sg&deleted=true",                                   0);
        ITUtilStructures.assertRead("?mnemonic=Sys&deleted=true",                                  0);
        ITUtilStructures.assertRead("?mnemonic=Rshs&deleted=true",                                 0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys&deleted=true",                               0);
        ITUtilStructures.assertRead("?mnemonic=Sys-Rshs&deleted=true",                             0);
        ITUtilStructures.assertRead("?mnemonic=Sg-Sys-Rshs&deleted=true",                          0);

        ITUtilStructures.assertRead("?mnemonicPath=Sg",                                            1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys",                                           1);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs",                                          0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys",                                        0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs",                                      1);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs",                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg&deleted=false",                              1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys&deleted=false",                             1);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs&deleted=false",                            0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=false",                          0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs&deleted=false",                        1);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs&deleted=false",                     0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg&deleted=true",                               0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys&deleted=true",                              0);
        ITUtilStructures.assertRead("?mnemonicPath=Rshs&deleted=true",                             0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=true",                           0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-Rshs&deleted=true",                         0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys-Rshs&deleted=true",                      0);

        ITUtilStructures.assertHistory("/" + structureElement.getUuid().toString(),                1);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Rshs",        Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Rshs",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sg-Sys-Rshs", Boolean.FALSE);
    }

    @Test
    void readSearch2Trees() {
        // test read subsystem in various ways
        //
        // note
        //     create in order to read
        //     2 different lines  of uuid with combinations of values
        //         9 entries for each line of uuid
        //         mnemonic - Ab, A9
        //
        //     exclude content (with latest) before latest
        //     exclude content (with latest) after  latest (cancelled, rejected)
        //     keep most recent content (without latest)   (to have most recent in line of uuid without latest)

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElement structureElement = null;
        UUID uuid, uuid2 = null;

        // a number of entries
        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Ab", null, "description mnemonic Ab");
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        uuid = structureElement.getUuid();

        structureElementCommandUpdate.setDescription("description mnemonic Ab 2");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 3");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 4");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 5");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 6");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 7");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 8");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic Ab 9");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        // a number of entries
        structureElementCommandCreate = new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "A9", 2, "description mnemonic A9");
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        uuid2 = structureElement.getUuid();

        structureElementCommandUpdate.setDescription("description mnemonic A9 2");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 3");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 4");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 5");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 6");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 7");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 8");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);
        structureElementCommandUpdate.setDescription("description mnemonic A9 9");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        // ----------------------------------------------------------------------------------------------------
        // from first structure element
        assertNotNull(uuid);

        ITUtilStructures.assertFind("/" + uuid.toString());
        ITUtilStructures.assertFind("/" + uuid2.toString());

        ITUtilStructures.assertRead("?mnemonic=Ab",                                  1);
        ITUtilStructures.assertRead("?mnemonic=Ab&deleted=false",                    1);
        ITUtilStructures.assertRead("?mnemonic=Ab&deleted=true",                     0);

        ITUtilStructures.assertRead("?mnemonic=A9",                                  1);
        ITUtilStructures.assertRead("?mnemonic=A9&deleted=false",                    1);
        ITUtilStructures.assertRead("?mnemonic=A9&deleted=true",                     0);

        ITUtilStructures.assertHistory("/" + uuid.toString(),                        9);
        ITUtilStructures.assertHistory("/" + uuid2.toString(),                       9);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Ab",        Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "A9",        Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Ab",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-A9",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sg-Sys-Ab", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sg-Sys-A9", Boolean.FALSE);
    }

    @Test
    void readSearchMultipleTrees() {
        // test read subsystem in various ways
        //
        // note
        //     create in order to read
        //     multiple commands at same time may have effects on order by (when)
        //
        //     exclude content (with latest) before latest
        //     exclude content (with latest) after  latest (cancelled, rejected)
        //     keep most recent content (without latest)   (to have most recent in line of uuid without latest)

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElement[] structureElements = null;

        ResponsePageStructureElements response, response2 = null;
        UUID uuid = null;
        UUID uuid2 = null;
        UUID uuidRandom = UUID.randomUUID();

        String description2 = "some other description";
        String description3 = "more description";
        String description4 = "yet another description";

        // create
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AA1", 1, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AA2", 1, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AA3", 1, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AA4", 1, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AA5", 1, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        uuid = structureElements[0].getUuid();

        // create
        // update
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AB1", 2, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AB2", 2, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AB3", 2, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AB4", 2, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AB5", 2, "description"),
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description2);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);

        // create
        // delete
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AC1", 3, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AC2", 3, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AC3", 3, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AC4", 3, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AC5", 3, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);
        ITUtilStructures.assertDelete(structureElementCommandsConfirm);

        // create
        // update
        // delete
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AD1", 4, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AD2", 4, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AD3", 4, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AD4", 4, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AD5", 4, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description2);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);
        ITUtilStructures.assertDelete(structureElementCommandsConfirm);

        // create
        // update
        // update
        // update
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AE1", 5, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AE2", 5, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AE3", 5, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AE4", 5, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AE5", 5, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description2);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description3);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description4);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);

        // create
        // update
        // update
        // update
        // delete
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AF1", 6, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AF2", 6, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AF3", 6, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AF4", 6, "description"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "AF5", 6, "description"),
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        uuid2 = structureElements[0].getUuid();
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description2);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description3);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        for (StructureElementCommandUpdate structureElementCommandUpdate : structureElementCommandsUpdate) {
            structureElementCommandUpdate.setDescription(description4);
        }
        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);
        ITUtilStructures.assertDelete(structureElementCommandsConfirm);

        // 85 system group entries

        // ----------------------------------------------------------------------------------------------------
        // from first structure element
        assertNotNull(uuid);

        // read & search
        //     /{uuid}
        //     type, deleted, uuid, parent, name, mnemonic, mnemonicPath, description
        //     combination
        //     pagination
        //         page, pageSize
        //         default pageSize 100
        //     sorting
        //         FieldStructure
        //         data content give what kind of sorting may be done and tested in this way
        //         compare in query result for sorting asc and desc
        //             content for field in first and last items    -    always possible
        //             first and last items                         -    not always possible

        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__",                                                         30);

        ITUtilStructures.assertRead("?type=SUBSYSTEM&deleted=false&mnemonic=A__",                                           15);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&deleted=true&mnemonic=A__",                                            15);

        ITUtilStructures.assertRead("?type=SUBSYSTEM&parent=" + systemGroupUuid.toString(),                                  0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&parent=" + systemUuid.toString(),                                      30, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&parent=" + uuid.toString(),                                             0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&parent=" + uuid2.toString(),                                            0);

        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonicPath=A__",                                                      0);

        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=desc",                                                      0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=desc%",                                                    10, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=sc",                                                        0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=sc%",                                                       0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=%sc",                                                       0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=%sc%",                                                     30, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&description=description",                                              10, -1);

        ITUtilStructures.assertRead("?type=SUBSYSTEM&who=",                                                                 30, -1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&who=test",                                                              0);

        // order by
        //     avoid
        //         MNEMONICPATH        - different for requested/processed, not set for PENDING, ambiguous/not set for HISTORY
        //     use with care
        //         PARENT
        //         WHEN                - different for requested/processed
        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=UUID&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=UUID&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getUuid(),  response2.getList().get(response2.getList().size()-1).getUuid());
        assertEquals(response2.getList().get(0).getUuid(),  response.getList().get(response.getList().size()-1).getUuid());

        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=PARENT&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=PARENT&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getParent(),  response2.getList().get(response2.getList().size()-1).getParent());
        assertEquals(response2.getList().get(0).getParent(),  response.getList().get(response.getList().size()-1).getParent());

        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=MNEMONIC&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=MNEMONIC&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getMnemonic(),  response2.getList().get(response2.getList().size()-1).getMnemonic());
        assertEquals(response2.getList().get(0).getMnemonic(),  response.getList().get(response.getList().size()-1).getMnemonic());

        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=ORDERING&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=ORDERING&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(1, response.getList().get(0).getOrdering());
        assertTrue(response.getList().get(0).getMnemonic().startsWith("AA"));
        assertEquals(6, response2.getList().get(0).getOrdering());
        assertTrue(response2.getList().get(0).getMnemonic().startsWith("AF"));

        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=DESCRIPTION&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=DESCRIPTION&isAsc=false");
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertEquals(response.getList().get(0).getDescription(),  response2.getList().get(response2.getList().size()-1).getDescription());
        assertEquals(response2.getList().get(0).getDescription(),  response.getList().get(response.getList().size()-1).getDescription());

        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=WHEN&isAsc=true");
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=AF_&orderBy=WHEN&isAsc=false");
        String value0 = response.getList().get(0).getWhen() != null ? response.getList().get(0).getWhen().toString() : null;
        String value1 = response.getList().get(1).getWhen() != null ? response.getList().get(1).getWhen().toString() : null;
        String value2 = response2.getList().get(response2.getList().size()-2).getWhen() != null ? response2.getList().get(response2.getList().size()-2).getWhen().toString() : null;
        String value3 = response2.getList().get(response2.getList().size()-1).getWhen() != null ? response2.getList().get(response2.getList().size()-1).getWhen().toString() : null;
        assertTrue(response.getList().size() > 0);
        assertEquals(response.getList().size(),  response2.getList().size());
        assertTrue(value0 != null && (StringUtils.equals(value0, value2) || StringUtils.equals(value0, value3))
                || value1 != null && (StringUtils.equals(value1, value2) || StringUtils.equals(value1, value3)));
        assertTrue(value3 != null && (StringUtils.equals(value3, value0) || StringUtils.equals(value3, value1))
                || value2 != null && (StringUtils.equals(value2, value0) || StringUtils.equals(value2, value1)));

        // pagination
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=0&pageSize=100",                                     30);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=1&pageSize=100",                                      0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=0&pageSize=30",                                      30);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=1&pageSize=30",                                       0);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=0&pageSize=12",                                      12);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=1&pageSize=12",                                      12);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=2&pageSize=12",                                       6);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=3&pageSize=12",                                       0);

        // pagination
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=0&pageSize=12",                                      12);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=1&pageSize=12",                                      12);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=2&pageSize=12",                                       6);
        ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&page=3&pageSize=12",                                       0);

        // order by, pagination
        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=WHEN&isAsc=true&page=0&pageSize=12",  12);
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=WHEN&isAsc=false&page=2&pageSize=12",  6);
        assertEquals(response.getList().get(0).getWhen(), response2.getList().get(response2.getList().size()-1).getWhen());
        response  = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=WHEN&isAsc=false&page=0&pageSize=12", 12);
        response2 = ITUtilStructures.assertRead("?type=SUBSYSTEM&mnemonic=A__&orderBy=WHEN&isAsc=true&page=2&pageSize=12",   6);
        assertEquals(response.getList().get(0).getWhen(), response2.getList().get(response2.getList().size()-1).getWhen());

        // uuid
        //     /{uuid}
        ITUtilStructures.assertFind("/" + uuidRandom.toString(), HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilStructures.assertFind("/" + systemGroupUuid.toString());
        ITUtilStructures.assertFind("/" + systemUuid.toString());
        ITUtilStructures.assertFind("/" + uuid.toString());
        ITUtilStructures.assertFind("/" + uuid2.toString());

        // children
        //     /children/{uuid}
        //     uuid, deleted
        ITUtilStructures.assertRead("/children/" + uuid.toString(),                                                          0, 0,  HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilStructures.assertRead("/children/" + uuid2.toString(),                                                         0, 0,  HttpURLConnection.HTTP_NOT_FOUND);
        ITUtilStructures.assertRead("/children/" + systemUuid.toString(),                                                   15, -1);
        ITUtilStructures.assertRead("/children/" + systemUuid.toString() + "?deleted=false",                                 9, -1);
        ITUtilStructures.assertRead("/children/" + systemUuid.toString() + "?deleted=true",                                  5, -1);
        ITUtilStructures.assertRead("/children/" + systemGroupUuid.toString(),                                               1);

        // mnemonic
        //     ?mnemonic={mnemonic}
        ITUtilStructures.assertRead("?mnemonic=A",                                                                           0);
        ITUtilStructures.assertRead("?mnemonic=A__",                                                                        30, -1);
        ITUtilStructures.assertRead("?mnemonic=AE_",                                                                         5);
        ITUtilStructures.assertRead("?mnemonic=AE1",                                                                         1);
        ITUtilStructures.assertRead("?mnemonic=Sg",                                                                          1);
        ITUtilStructures.assertRead("?mnemonic=A&deleted=false",                                                             0);
        ITUtilStructures.assertRead("?mnemonic=A__&deleted=false",                                                          15, -1);
        ITUtilStructures.assertRead("?mnemonic=AE_&deleted=false",                                                           5);
        ITUtilStructures.assertRead("?mnemonic=AE1&deleted=false",                                                           1);
        ITUtilStructures.assertRead("?mnemonic=Sg&deleted=false",                                                            1);

        // mnemonic path
        //     ?mnemonicPath={mnemonicPath}
        ITUtilStructures.assertRead("?mnemonicPath=A",                                                                       0);
        ITUtilStructures.assertRead("?mnemonicPath=A__",                                                                     0);
        ITUtilStructures.assertRead("?mnemonicPath=AE_",                                                                     0);
        ITUtilStructures.assertRead("?mnemonicPath=AE1",                                                                     0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A%",                                                                 15, -1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A",                                                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A__",                                                                15, -1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-AE_",                                                                 5);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-AE1",                                                                 1);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A%",                                                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A",                                                                    0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A__",                                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-AE_",                                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-AE1",                                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys%",                                                                 0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys",                                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys__",                                                                0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys_",                                                                 0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys",                                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=A&deleted=false",                                                         0);
        ITUtilStructures.assertRead("?mnemonicPath=A__&deleted=false",                                                       0);
        ITUtilStructures.assertRead("?mnemonicPath=AE_&deleted=false",                                                       0);
        ITUtilStructures.assertRead("?mnemonicPath=AE1&deleted=false",                                                       0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A%&deleted=false",                                                   15, -1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A&deleted=false",                                                     0);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-A__&deleted=false",                                                  15, -1);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-AE_&deleted=false",                                                   5);
        ITUtilStructures.assertRead("?mnemonicPath=Sys-AE1&deleted=false",                                                   1);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A%&deleted=false",                                                     0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A&deleted=false",                                                      0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-A__&deleted=false",                                                    0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-AE_&deleted=false",                                                    0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-AE1&deleted=false",                                                    0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys%&deleted=false",                                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=false",                                                    0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys__&deleted=false",                                                  0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys_&deleted=false",                                                   0);
        ITUtilStructures.assertRead("?mnemonicPath=Sg-Sys&deleted=false",                                                    0);

        // history
        //     /history/{uuid}
        ITUtilStructures.assertHistory("/" + uuid.toString(),                                                                1);
        ITUtilStructures.assertHistory("/" + uuid2.toString(),                                                               5);
        ITUtilStructures.assertHistory("/" + systemUuid.toString(),                                                          1);
        ITUtilStructures.assertHistory("/" + systemGroupUuid.toString(),                                                     1);
        ITUtilStructures.assertHistory("/" + uuidRandom.toString(),                                                          0, HttpURLConnection.HTTP_NOT_FOUND);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AA1", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AA2", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AA3", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AA4", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AA5", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AB1", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AB2", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AB3", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AB4", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AB5", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AC1", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AC2", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AC3", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AC4", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AC5", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AD1", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AD2", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AD3", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AD4", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AD5", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AE1", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AE2", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AE3", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AE4", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AE5", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AF1", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AF2", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AF3", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AF4", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AF5", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AG1", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AG2", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AG3", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AG4", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AG5", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AH1", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AH2", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AH3", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AH4", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-AH5", Boolean.FALSE);
    }

}

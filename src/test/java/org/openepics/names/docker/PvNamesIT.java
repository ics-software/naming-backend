/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test if process variable name is valid.
 * </p>
 *
 * @author Lars Johansson
 * @see NamesIT
 */
@Testcontainers
public class PvNamesIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010    = null;

    private static UUID disciplineEMR   = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceTypeFS    = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;
        NameElementCommandCreate[] nameElementCommandsCreate = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010 = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR", null, "Electromagnetic Resonators")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR, null, null, "Control")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "FS",  null, "Flow Switch")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceTypeFS = structureElements[0].getUuid();

        nameElementCommandsCreate = new NameElementCommandCreate[] {
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "001", "description")
        };
        ITUtilNames.assertCreate(nameElementCommandsCreate);

        // check content
        ITUtilStructures.assertRead("",                   6);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    1);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   1);
        ITUtilNames.assertRead("", 4);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + PvNamesIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void validProcessVariableName() {
        // test if process variable name is valid

        ITUtilPvNames.assertValidProcessVariableName("CWL-CWSE05:WtrC-EC-003:property",       Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ::Property123412341234123412341234", Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ::Property123412341234",             Boolean.TRUE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010::Property()",                   Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010::Property",                     Boolean.TRUE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010:P:Property",                    Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010:EMR-FS:property",               Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010:EMR-FS:Property",               Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010:EMR-FS-001:property",           Boolean.FALSE);
        ITUtilPvNames.assertValidProcessVariableName("RFQ-010:EMR-FS-001:Property",           Boolean.TRUE);
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandConfirm;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.util.StructureCommand;
import org.openepics.names.util.StructureElementUtil;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test structures endpoints.
 * In particular, purpose is to test arrays of structures.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class StructuresMultipleIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupUuid = null;
    private static UUID systemUuid = null;
    private static UUID subsystemUuid = null;

    private static UUID disciplineUuid = null;
    private static UUID deviceGroupUuid = null;
    @SuppressWarnings("unused")
    private static UUID deviceTypeUuid = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Sg", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupUuid = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupUuid, "Sys", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemUuid = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemUuid, "Sub", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystemUuid = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Di", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineUuid = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineUuid, null, null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupUuid = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupUuid, "Dt", null, "description")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceTypeUuid = structureElements[0].getUuid();

        // check content
        ITUtilStructures.assertRead("",                   6);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    1);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   1);
        ITUtilNames.assertRead("", 3);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + StructuresMultipleIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void checkCreate() {
        // test conditions for create subsystem
        //     not create itself
        //
        // validate create
        //     type
        //     parent
        //     name
        //     mnemonic
        //     description

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        String value = null;

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "Sg",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgChc", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "Sg",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgChc", Boolean.TRUE);

        ITUtilStructures.assertExists(Type.SYSTEM, "Sys",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM, "SysChc", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM, "Sys",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM, "SysChc", Boolean.TRUE);

        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-Sub",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM, "Sys-SubChc",  Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-Sub",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM, "Sys-SubChc", Boolean.TRUE);

        ITUtilStructures.assertExists(Type.DISCIPLINE, "Di",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE, "DiChc", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE, "Di",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE, "DiChc", Boolean.TRUE);

        ITUtilStructures.assertExists(Type.DEVICEGROUP, "Grp",    Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpChc", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "Grp",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpChc", Boolean.FALSE);

        ITUtilStructures.assertExists(Type.DEVICETYPE, "Di-Dt",    Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICETYPE, "Di-DtChc", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE, "Di-Dt",    Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE, "Di-DtChc", Boolean.TRUE);

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgChc",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysChc", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubChc", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiChc",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",       null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtChc",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElementCommandsCreate[1].setType(null);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setType(Type.SYSTEMGROUP);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setType(Type.SUBSYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setType(Type.SYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.TRUE);

        structureElementCommandsCreate[1].setParent(null);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setParent(systemUuid);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setParent(subsystemUuid);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setParent(systemGroupUuid);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.TRUE);

        value = structureElementCommandsCreate[1].getDescription();
        structureElementCommandsCreate[1].setDescription(null);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.FALSE);
        structureElementCommandsCreate[1].setDescription(value);
        ITUtilStructures.assertValidate(structureElementCommandsCreate, StructureCommand.CREATE, Boolean.TRUE);
    }

    @Test
    void create() {
        // test create subsystem

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        StructureElement[] structureElements = null;

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgCa",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysCa", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubCa", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiCa",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",      null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtCa",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCa", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCa",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCa",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCa", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCa",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCa",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCa", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCa",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCa", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCa",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);
    }

    @Test
    void checkUpdate() {
        // test conditions for update subsystem
        //     not update
        //
        // note
        //     create in order to update
        //
        // validate update
        //     uuid
        //     type
        //     parent
        //     name
        //     mnemonic
        //     description

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        StructureElement[] structureElements = null;
        UUID uuid = null;
        String value = null;

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgCu",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysCu", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubCu", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiCu",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",      null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtCu",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCu",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCu",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCu", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCu",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCu",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCu",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCu",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCu",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCu", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCu",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCu",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCu",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCu",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCu",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCu", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCu",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCu",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCu",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCu",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCu",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCu", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCu",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCu",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCu",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);

        uuid = structureElements[1].getUuid();
        structureElementCommandsUpdate[1].setUuid(null);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setUuid(uuid);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandsUpdate[1].setType(null);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setType(Type.SYSTEMGROUP);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setType(Type.SUBSYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setType(Type.SYSTEM);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        structureElementCommandsUpdate[1].setParent(null);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setParent(systemUuid);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setParent(subsystemUuid);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setParent(systemGroupUuid);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.TRUE);

        value = structureElementCommandsUpdate[1].getDescription();
        structureElementCommandsUpdate[1].setDescription(null);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.FALSE);
        structureElementCommandsUpdate[1].setDescription(value);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate, StructureCommand.UPDATE, Boolean.TRUE);
    }

    @Test
    void update() {
        // test update subsystem
        //
        // note
        //     create in order to update

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        StructureElement[] structureElements = null;

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgUa",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysUa", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubUa", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiUa",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",      null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtUa",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgUa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysUa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiUa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtUa",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgUa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysUa",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiUa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtUa",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);

        structureElementCommandsUpdate[0].setDescription("0");
        structureElementCommandsUpdate[1].setDescription("1");
        structureElementCommandsUpdate[2].setDescription("2");
        structureElementCommandsUpdate[3].setDescription("3");
        structureElementCommandsUpdate[4].setDescription("4");
        structureElementCommandsUpdate[5].setDescription("5");

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgUa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysUa",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiUa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtUa",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgUa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysUa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiUa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtUa",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);

        structureElements = ITUtilStructures.assertUpdate(structureElementCommandsUpdate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgUa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysUa",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiUa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtUa",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgUa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysUa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubUa", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiUa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpUa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtUa",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);
    }

    @Test
    void checkDelete() {
        // test conditions for delete subsystem
        //     not delete
        //
        // note
        //     create in order to delete
        //
        // validate delete
        //     uuid
        //     type

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        StructureElement[] structureElements = null;
        UUID uuid = null;

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgCd",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysCd", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubCd", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiCd",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",      null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtCd",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCd",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCd",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCd", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCd",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCd",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCd",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCd",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCd",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCd", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCd",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCd",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCd",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgCd",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysCd",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubCd", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiCd",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpCd",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtCd",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgCd",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysCd",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubCd", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiCd",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpCd",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtCd",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);

        uuid = structureElementCommandsConfirm[1].getUuid();
        structureElementCommandsConfirm[1].setUuid(null);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE, Boolean.FALSE);
        structureElementCommandsConfirm[1].setUuid(uuid);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void delete() {
        // test delete subsystem
        //
        // note
        //     create in order to delete

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElementCommandUpdate[] structureElementCommandsUpdate = null;
        StructureElementCommandConfirm[] structureElementCommandsConfirm = null;
        StructureElementCommand[] structureElementCommands = null;
        StructureElement[] structureElements = null;

        structureElementCommands = new StructureElementCommand[] {
                new StructureElementCommand(null, Type.SYSTEMGROUP, null,            "SgDa",  null, "description"),
                new StructureElementCommand(null, Type.SYSTEM,      systemGroupUuid, "SysDa", null, "description"),
                new StructureElementCommand(null, Type.SUBSYSTEM,   systemUuid,      "SubDa", null, "description"),
                new StructureElementCommand(null, Type.DISCIPLINE,  null,            "DiDa",  null, "description"),
                new StructureElementCommand(null, Type.DEVICEGROUP, disciplineUuid,  "",      null, "description"),
                new StructureElementCommand(null, Type.DEVICETYPE,  deviceGroupUuid, "DtDa",  null, "description")
        };
        structureElementCommandsCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommands);
        structureElementCommandsUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommands);
        structureElementCommandsConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommands);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgDa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysDa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiDa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtDa",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgDa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysDa",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiDa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtDa",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);

        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        assertNotNull(structureElements);
        assertEquals(6, structureElements.length);

        structureElementCommandsUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElements);
        structureElementCommandsConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElements);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgDa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysDa",     Boolean.TRUE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiDa",      Boolean.TRUE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtDa",   Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgDa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysDa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiDa",      Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtDa",   Boolean.FALSE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.TRUE);

        ITUtilStructures.assertDelete(structureElementCommandsConfirm);

        ITUtilStructures.assertExists(Type.SYSTEMGROUP, "SgDa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SYSTEM,      "SysDa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DISCIPLINE,  "DiDa",      Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertExists(Type.DEVICETYPE,  "Di-DtDa",   Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEMGROUP, "SgDa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SYSTEM,      "SysDa",     Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.SUBSYSTEM,   "Sys-SubDa", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE,  "DiDa",      Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICEGROUP, "GrpDa",     Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DEVICETYPE,  "Di-DtDa",   Boolean.TRUE);

        ITUtilStructures.assertValidate(structureElementCommandsCreate,  StructureCommand.CREATE,  Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandsUpdate,  StructureCommand.UPDATE,  Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandsConfirm, StructureCommand.DELETE,  Boolean.FALSE);
    }

}

/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommand;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.util.NameCommand;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test names endpoints and rules for disciplines P&ID and Scientific in particular.
 * In practice, this class contains tests for instance index, en masse.
 * </p>
 *
 * @author Lars Johansson
 *
 * @see NamingConventionUtil
 */
@SuppressWarnings("unused")
@Testcontainers
class NamesInstanceIndexIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json
    //
    //     disciplines
    //         P&ID
    //         Scientific
    //     mnemonic path P&ID numeric
    //     see NamingConventionUtil

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010PRL = null;
    private static UUID subsystem010    = null;
    private static UUID subsystemN1U1   = null;

    private static UUID disciplineCryo  = null;
    private static UUID disciplineEMR   = null;
    private static UUID disciplineHVAC  = null;
    private static UUID disciplineProc  = null;
    private static UUID disciplineSC    = null;
    private static UUID disciplineVac   = null;
    private static UUID disciplineWtrC  = null;
    private static UUID disciplineBMD   = null;

    private static UUID deviceGroupCryo = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceGroupHVAC = null;
    private static UUID deviceGroupProc = null;
    private static UUID deviceGroupSC   = null;
    private static UUID deviceGroupVac  = null;
    private static UUID deviceGroupWtrC = null;
    private static UUID deviceGroupBMD  = null;

    private static UUID deviceType_Cryo_FS  = null;
    private static UUID deviceType_Cryo_IOC = null;
    private static UUID deviceType_Cryo_RFA = null;
    private static UUID deviceType_Cryo_TT  = null;

    private static UUID deviceType_EMR_FS   = null;
    private static UUID deviceType_EMR_IOC  = null;
    private static UUID deviceType_EMR_RFA  = null;
    private static UUID deviceType_EMR_TT   = null;

    private static UUID deviceType_HVAC_FS  = null;
    private static UUID deviceType_HVAC_IOC = null;
    private static UUID deviceType_HVAC_RFA = null;
    private static UUID deviceType_HVAC_TT  = null;

    private static UUID deviceType_Proc_FS  = null;
    private static UUID deviceType_Proc_IOC = null;
    private static UUID deviceType_Proc_RFA = null;
    private static UUID deviceType_Proc_TT  = null;

    private static UUID deviceType_SC_FS    = null;
    private static UUID deviceType_SC_IOC   = null;
    private static UUID deviceType_SC_RFA   = null;
    private static UUID deviceType_SC_TT    = null;

    private static UUID deviceType_Vac_FS   = null;
    private static UUID deviceType_Vac_IOC  = null;
    private static UUID deviceType_Vac_RFA  = null;
    private static UUID deviceType_Vac_TT   = null;

    private static UUID deviceType_WtrC_FS  = null;
    private static UUID deviceType_WtrC_IOC = null;
    private static UUID deviceType_WtrC_RFA = null;
    private static UUID deviceType_WtrC_TT  = null;

    private static UUID deviceType_BMD_FS   = null;
    private static UUID deviceType_BMD_IOC  = null;
    private static UUID deviceType_BMD_RFA  = null;
    private static UUID deviceType_BMD_TT   = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010PRL", null, "01 Phase Reference Line"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "N1U1",   null, "Power switch board 01. Electrical power cabinets")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010PRL = structureElements[0].getUuid();
        subsystem010    = structureElements[1].getUuid();
        subsystemN1U1   = structureElements[2].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Cryo", null, "Cryogenics"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR",  null, "Electromagnetic Resonators"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "HVAC", null, "Heating, Cooling and Air Conditioning"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Proc", null, "General Process Control"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "SC",   null, "Software Controllers"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Vac",  null, "Vacuum"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "WtrC", null, "Water Cooling"),
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "BMD",  null, "Beam Magnets and Deflectors")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineCryo = structureElements[0].getUuid();
        disciplineEMR  = structureElements[1].getUuid();
        disciplineHVAC = structureElements[2].getUuid();
        disciplineProc = structureElements[3].getUuid();
        disciplineSC   = structureElements[4].getUuid();
        disciplineVac  = structureElements[5].getUuid();
        disciplineWtrC = structureElements[6].getUuid();
        disciplineBMD  = structureElements[7].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineCryo, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR,  null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineHVAC, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineProc, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineSC,   null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineVac,  null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineWtrC, null, null, "empty"),
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineBMD,  null, null, "empty")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupCryo = structureElements[0].getUuid();
        deviceGroupEMR  = structureElements[1].getUuid();
        deviceGroupHVAC = structureElements[2].getUuid();
        deviceGroupProc = structureElements[3].getUuid();
        deviceGroupSC   = structureElements[4].getUuid();
        deviceGroupVac  = structureElements[5].getUuid();
        deviceGroupWtrC = structureElements[6].getUuid();
        deviceGroupBMD  = structureElements[7].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR,  "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupHVAC, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupProc, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupSC,   "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupVac,  "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupWtrC, "TT",  null, "Temperature Transmitter"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "IOC", null, "Input Output Controller"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupBMD,  "TT",  null, "Temperature Transmitter")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceType_Cryo_FS  = structureElements[0].getUuid();
        deviceType_Cryo_IOC = structureElements[1].getUuid();
        deviceType_Cryo_RFA = structureElements[2].getUuid();
        deviceType_Cryo_TT  = structureElements[3].getUuid();
        deviceType_EMR_FS   = structureElements[4].getUuid();
        deviceType_EMR_IOC  = structureElements[5].getUuid();
        deviceType_EMR_RFA  = structureElements[6].getUuid();
        deviceType_EMR_TT   = structureElements[7].getUuid();
        deviceType_HVAC_FS  = structureElements[8].getUuid();
        deviceType_HVAC_IOC = structureElements[9].getUuid();
        deviceType_HVAC_RFA = structureElements[10].getUuid();
        deviceType_HVAC_TT  = structureElements[11].getUuid();
        deviceType_Proc_FS  = structureElements[12].getUuid();
        deviceType_Proc_IOC = structureElements[13].getUuid();
        deviceType_Proc_RFA = structureElements[14].getUuid();
        deviceType_Proc_TT  = structureElements[15].getUuid();
        deviceType_SC_FS    = structureElements[16].getUuid();
        deviceType_SC_IOC   = structureElements[17].getUuid();
        deviceType_SC_RFA   = structureElements[18].getUuid();
        deviceType_SC_TT    = structureElements[19].getUuid();
        deviceType_Vac_FS   = structureElements[20].getUuid();
        deviceType_Vac_IOC  = structureElements[21].getUuid();
        deviceType_Vac_RFA  = structureElements[22].getUuid();
        deviceType_Vac_TT   = structureElements[23].getUuid();
        deviceType_WtrC_FS  = structureElements[24].getUuid();
        deviceType_WtrC_IOC = structureElements[25].getUuid();
        deviceType_WtrC_RFA = structureElements[26].getUuid();
        deviceType_WtrC_TT  = structureElements[27].getUuid();
        deviceType_BMD_FS   = structureElements[28].getUuid();
        deviceType_BMD_IOC  = structureElements[29].getUuid();
        deviceType_BMD_RFA  = structureElements[30].getUuid();
        deviceType_BMD_TT   = structureElements[31].getUuid();

        // check content
        ITUtilStructures.assertRead("",                  53);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    3);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   8);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  8);
        ITUtilStructures.assertRead("?type=DEVICETYPE",  32);
        ITUtilNames.assertRead("", 5);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + NamesInstanceIndexIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

  @Test
  void checkCreateInstanceIndex_Cryo() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline Cryo
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Cryo_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Cryo_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_EMR() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline EMR
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_EMR_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_EMR_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_HVAC() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline HVAC
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_HVAC_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_HVAC_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_Proc() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline Proc
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Proc_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Proc_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_SC() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline SC
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_SC_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_SC_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_Vac() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline Vac
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Vac_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_Vac_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_WtrC() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline WtrC
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_WtrC_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_WtrC_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

  @Test
  void checkCreateInstanceIndex_BMD() {
      // test conditions for create name
      //     not create itself
      //
      // note
      //     discipline BMD
      //     instance index

      NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate();
      nameElementCommandCreate.setParentSystemStructure(subsystem010PRL);
      nameElementCommandCreate.setDescription("description");

      // ----------------------------------------------------------------------------------------------------

      nameElementCommandCreate.setParentDeviceStructure(deviceType_BMD_IOC);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);

      nameElementCommandCreate.setParentDeviceStructure(deviceType_BMD_RFA);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "051",       Boolean.TRUE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00",        Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0000000",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "00000000",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000000000", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1234567",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "12345678",  Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123456789", Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "000ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123a",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ab",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abc",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123abcd",   Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123A",      Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123AB",     Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABC",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "123ABCD",   Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1",         Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "01",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "001",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0001",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "0110",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10",        Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "100",       Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "1000",      Boolean.TRUE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "10001",     Boolean.FALSE);

      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "",          Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, " ",         Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "Idx",       Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abcdef",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "abc123",    Boolean.FALSE);
      ITUtilNames.assertValidate(nameElementCommandCreate, NameCommand.CREATE, "a!",        Boolean.FALSE);
  }

}

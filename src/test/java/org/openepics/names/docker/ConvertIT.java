/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.response.ResponsePageNameElements;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test conversion of names and structures data from one format to another.
 * </p>
 *
 * @author Lars Johansson
 * @see NamesIT
 * @see NamesIT#readSearchHistoryDeleted()
 */
@Testcontainers
class ConvertIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     json2Excel
    //         response - no content (null) if response is not json
    //         any further test requires test to download excel
    //         test convert structures data in json, according to array of structure elements, to Excel file
    //     excel2json
    //         test convert names data in Excel file, according to template for structures, to json

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc  = null;
    private static UUID systemRFQ       = null;
    private static UUID subsystem010    = null;

    private static UUID disciplineEMR   = null;
    private static UUID deviceGroupEMR  = null;
    private static UUID deviceTypeFS    = null;

    @BeforeAll
    public static void initAll() {
        // setup
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;
        NameElementCommandCreate[] nameElementCommandsCreate = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010PRL", null, "01 Phase Reference Line"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010",    null, "RFQ-010"),
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "N1U1",   null, "Power switch board 01. Electrical power cabinets")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010 = structureElements[1].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "EMR", null, "Electromagnetic Resonators")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineEMR, null, null, "Control")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupEMR = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "FS",  null, "Flow Switch"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "RFA", null, "RF Antenna"),
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupEMR, "TT",  null, "Temperature Transmitter")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceTypeFS = structureElements[0].getUuid();

        nameElementCommandsCreate = new NameElementCommandCreate[] {
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "001", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "002", "description"),
                new NameElementCommandCreate(subsystem010, deviceTypeFS, "003", "description")
        };
        ITUtilNames.assertCreate(nameElementCommandsCreate);

        // check content
        ITUtilStructures.assertRead("",                  10);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    3);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   3);
        ITUtilNames.assertRead("", 8);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + ConvertIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void json2ExcelStructure() {
        ResponsePageStructureElements responsePage = ITUtilStructures.assertRead("/children/" + systemGroupAcc.toString(), 1);
        String response = ITUtilStructures.assertConvertJson2Excel(responsePage.getList().stream().toArray(StructureElement[]::new));

        assertNotNull(responsePage);
        assertNull(response);
    }

    @Test
    void json2ExcelStructures() {
        ResponsePageStructureElements responsePage = ITUtilStructures.assertRead("/children/" + systemRFQ.toString(), 3);
        String response = ITUtilStructures.assertConvertJson2Excel(responsePage.getList().stream().toArray(StructureElement[]::new));

        assertNotNull(responsePage);
        assertNull(response);
    }

    @Test
    void json2ExcelName() {
        ResponsePageNameElements responsePage = ITUtilNames.assertRead("?index=001", 1);
        String response = ITUtilNames.assertConvertJson2Excel(responsePage.getList().stream().toArray(NameElement[]::new));

        assertNotNull(responsePage);
        assertNull(response);
    }

    @Test
    void json2ExcelNames() {
        ResponsePageNameElements responsePage = ITUtilNames.assertRead("?index=00%", 3);
        String response = ITUtilNames.assertConvertJson2Excel(responsePage.getList().stream().toArray(NameElement[]::new));

        assertNotNull(responsePage);
        assertNull(response);
    }

    @Test
    void excel2JsonStructure() {
      String json = ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonStructureEmptyUuid() {
      String json = ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_empty_uuid.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonStructures() {
      String json = ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_systems.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonStructureStructureElement() {
      String json = ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_StructureElement.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonStructureDifferentColumnOrder() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_different_column_order.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureIncorrectOrdering() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_incorrect_ordering.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureIncorrectUuid() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_incorrect_uuid.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureIncorrectUuidParent() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_incorrect_uuid_parent.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureTooFewColumns() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_too_few_columns.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureTooManyColumns() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_too_many_columns.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureTooManyColumnsUuid() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/StructureElementCommand_create_system_too_many_columns_uuid.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureXlsx() {
        ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/xlsx.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureXlsxSheet() {
        ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/xlsx_sheet.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureText() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/text.txt"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonStructureTextExcel() {
      ITUtilStructures.assertConvertExcel2Json(new File("src/test/resources/data/templates/text.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonName() {
      String json = ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonNameEmptyUuid() {
      String json = ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_empty_uuid.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonNames() {
      String json = ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_names.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonNameNameElement() {
      String json = ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_NameElement.xlsx"));

      assertNotNull(json);
      assertTrue(json.length() > 0);
    }

    @Test
    void excel2JsonNameIncorrectUuid() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_incorrect_uuid.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameIncorrectUuidParentSystemStructure() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_incorrect_uuid_parentsystemstructure.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameIncorrectUuidParentDeviceStructure() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_incorrect_uuid_parentdevicestructure.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameDifferentColumnOrder() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_different_column_order.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameTooFewColumns() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_too_few_columns.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameTooManyColumns() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_too_many_columns.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameTooManyColumnsUuid() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/NameElementCommand_create_name_too_many_columns_uuid.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameXlsx() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/xlsx.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameXlsxSheet() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/xlsx_sheet.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameText() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/text.txt"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

    @Test
    void excel2JsonNameTextExcel() {
      ITUtilNames.assertConvertExcel2Json(new File("src/test/resources/data/templates/text.xlsx"), HttpURLConnection.HTTP_INTERNAL_ERROR);
    }

}

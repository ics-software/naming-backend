/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElement;
import org.openepics.names.rest.beans.element.NameElementCommand;
import org.openepics.names.rest.beans.element.NameElementCommandConfirm;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.NameElementCommandUpdate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.util.NameCommand;
import org.openepics.names.util.NameElementUtil;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test names endpoints.
 * In particular, purpose is to test arrays of names.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class NamesMultipleIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json
    //
    // names for each of
    //     system group                       + description
    //     system group + device type + index + description
    //     system                             + description
    //     system       + device type + index + description
    //     subsystem                          + description
    //     subsystem    + device type + index + description

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    private static UUID systemGroupAcc     = null;
    private static UUID systemRFQ          = null;
    private static UUID subsystem010PRL    = null;

    private static UUID disciplineCryo     = null;
    private static UUID deviceGroupCryo    = null;
    private static UUID deviceType_Cryo_FS = null;

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Acc", null, "Accelerator. The ESS Linear Accelerator")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemGroupAcc = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, systemGroupAcc, "RFQ", null, "Radio Frequency Quadrupole")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        systemRFQ = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, systemRFQ, "010PRL", null, "01 Phase Reference Line")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        subsystem010PRL = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Cryo", null, "Cryogenics")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        disciplineCryo = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, disciplineCryo, null, null, "empty")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceGroupCryo = structureElements[0].getUuid();

        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, deviceGroupCryo, "FS", null, "Flow Switch")
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        deviceType_Cryo_FS = structureElements[0].getUuid();

        // check content
        ITUtilStructures.assertRead("",                   6);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  1);
        ITUtilStructures.assertRead("?type=SYSTEM",       1);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",    1);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  1);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   1);
        ITUtilNames.assertRead("", 3);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + NamesMultipleIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void checkCreate() {
        // test conditions for create name
        //     not create itself
        //
        // note
        //     with and without index

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        UUID uuid = null;
        String value = null;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "011", "checkCreate 1.3.011"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "012", "checkCreate 1.3.012"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "013", "checkCreate 1.3.013"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "011", "checkCreate 2.3.011"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "012", "checkCreate 2.3.012"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "013", "checkCreate 2.3.013"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "011", "checkCreate 3.3.011"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "012", "checkCreate 3.3.012"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "013", "checkCreate 3.3.013")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertExists("RFQ:Cryo-FS-011", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-011", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        uuid = nameElementCommandsCreate[3].getParentSystemStructure();
        nameElementCommandsCreate[3].setParentSystemStructure(null);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        nameElementCommandsCreate[3].setParentSystemStructure(uuid);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);

        uuid = nameElementCommandsCreate[3].getParentDeviceStructure();
        nameElementCommandsCreate[3].setParentDeviceStructure(null);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        nameElementCommandsCreate[3].setParentDeviceStructure(uuid);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);

        value = nameElementCommandsCreate[3].getIndex();
        nameElementCommandsCreate[3].setIndex(null);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        nameElementCommandsCreate[3].setIndex(value);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);

        value = nameElementCommandsCreate[3].getDescription();
        nameElementCommandsCreate[3].setDescription(null);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        nameElementCommandsCreate[3].setDescription(value);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
    }

    @Test
    void create() {
        // test create name
        //
        // note
        //     with and without index

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        NameElement[] createdNameElements = null;
        int nbrNames = -1;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  null,               null,  "create 1.0.0"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "021", "create 1.3.021"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "022", "create 1.3.022"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "023", "create 1.3.023"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "021", "create 2.3.021"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "022", "create 2.3.022"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "023", "create 2.3.023"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "021", "create 3.3.021"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "022", "create 3.3.022"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "023", "create 3.3.023")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nameElementCommandsCreate = new NameElementCommandCreate[] {
                nameElementCommandsCreate[1],
                nameElementCommandsCreate[2],
                nameElementCommandsCreate[3],
                nameElementCommandsCreate[4],
                nameElementCommandsCreate[5],
                nameElementCommandsCreate[6],
                nameElementCommandsCreate[7],
                nameElementCommandsCreate[8],
                nameElementCommandsCreate[9]
        };

        ITUtilNames.assertExists("Acc:Cryo-FS-023", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("Acc:Cryo-FS-023", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nbrNames = ITUtilNames.assertRead("?deleted=false").getListSize();

        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        assertNotNull(createdNameElements);
        assertEquals(9, createdNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        nameElementCommandsUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElements);
        nameElementCommandsConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElements);

        ITUtilNames.assertExists("Acc:Cryo-FS-023", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("Acc:Cryo-FS-023", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void checkUpdate() {
        // test conditions for update name
        //     not update
        //
        // note
        //     create in order to update
        //     validate update

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        NameElement[] createdNameElements = null;
        int nbrNames = -1;
        UUID uuid = null;
        String value = null;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "031", "checkUpdate 1.3.031"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "032", "checkUpdate 1.3.032"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "033", "checkUpdate 1.3.033"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "031", "checkUpdate 2.3.031"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "032", "checkUpdate 2.3.032"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "033", "checkUpdate 2.3.033"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "031", "checkUpdate 3.3.031"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "032", "checkUpdate 3.3.032"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "033", "checkUpdate 3.3.033")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertExists("RFQ:Cryo-FS-031", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-031", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nbrNames = ITUtilNames.assertRead("?deleted=false").getListSize();

        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        assertNotNull(createdNameElements);
        assertEquals(9, createdNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        nameElementCommandsUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElements);
        nameElementCommandsConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElements);

        ITUtilNames.assertExists("RFQ:Cryo-FS-031", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-031", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);

        uuid = nameElementCommandsUpdate[3].getUuid();
        nameElementCommandsUpdate[3].setUuid(null);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        nameElementCommandsUpdate[3].setUuid(uuid);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);

        uuid = nameElementCommandsUpdate[3].getParentSystemStructure();
        nameElementCommandsUpdate[3].setParentSystemStructure(null);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        nameElementCommandsUpdate[3].setParentSystemStructure(uuid);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);

        uuid = nameElementCommandsUpdate[3].getParentDeviceStructure();
        nameElementCommandsUpdate[3].setParentDeviceStructure(null);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        nameElementCommandsUpdate[3].setParentDeviceStructure(uuid);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);

        value = nameElementCommandsUpdate[3].getIndex();
        nameElementCommandsUpdate[3].setIndex(null);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        nameElementCommandsUpdate[3].setIndex(value);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);

        value = nameElementCommandsUpdate[3].getDescription();
        nameElementCommandsUpdate[3].setDescription(null);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        nameElementCommandsUpdate[3].setDescription(value);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
    }

    @Test
    void update() {
        // test update name
        //
        // note
        //     create in order to update

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        NameElement[] createdNameElements = null;
        NameElement[] updatedNameElements = null;
        int nbrNames = -1;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "041", "update 1.3.041"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "042", "update 1.3.042"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "043", "update 1.3.043"),
                new NameElementCommand(null, systemRFQ,       null,               null,  "update 2.0.0"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "041", "update 2.3.041"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "042", "update 2.3.042"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "043", "update 2.3.043"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "041", "update 3.3.041"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "042", "update 3.3.042"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "043", "update 3.3.043")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nameElementCommands = new NameElementCommand[] {
                nameElementCommands[0],
                nameElementCommands[1],
                nameElementCommands[2],
                nameElementCommands[4],
                nameElementCommands[5],
                nameElementCommands[6],
                nameElementCommands[7],
                nameElementCommands[8],
                nameElementCommands[9]
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertExists("RFQ", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nbrNames = ITUtilNames.assertRead("?deleted=false").getListSize();

        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        assertNotNull(createdNameElements);
        assertEquals(9, createdNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        nameElementCommandsUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElements);
        nameElementCommandsConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElements);

        ITUtilNames.assertExists("RFQ", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);

        nameElementCommandsUpdate[0].setDescription("0");
        nameElementCommandsUpdate[1].setDescription("1");
        nameElementCommandsUpdate[2].setDescription("2");
        nameElementCommandsUpdate[3].setDescription("3");
        nameElementCommandsUpdate[4].setDescription("4");
        nameElementCommandsUpdate[5].setDescription("5");
        nameElementCommandsUpdate[6].setDescription("6");
        nameElementCommandsUpdate[7].setDescription("7");
        nameElementCommandsUpdate[8].setDescription("8");

        ITUtilNames.assertExists("RFQ", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);

        updatedNameElements = ITUtilNames.assertUpdate(nameElementCommandsUpdate);
        assertNotNull(updatedNameElements);
        assertEquals(9, updatedNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        ITUtilNames.assertExists("RFQ", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void checkDelete() {
        // test conditions for delete name
        //     not delete
        //
        // note
        //     create in order to delete
        //     validate delete
        //         uuid

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        NameElement[] createdNameElements = null;
        int nbrNames = -1;
        UUID uuid = null;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "051", "checkDelete 1.3.051"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "052", "checkDelete 1.3.052"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "053", "checkDelete 1.3.053"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "051", "checkDelete 2.3.051"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "052", "checkDelete 2.3.052"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "053", "checkDelete 2.3.053"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "051", "checkDelete 3.3.051"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "052", "checkDelete 3.3.052"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "053", "checkDelete 3.3.053")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertExists("RFQ:Cryo-FS-051", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-051", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nbrNames = ITUtilNames.assertRead("?deleted=false").getListSize();

        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        assertNotNull(createdNameElements);
        assertEquals(9, createdNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        nameElementCommandsCreate = NameElementUtil.convertElement2CommandCreate(createdNameElements);
        nameElementCommandsUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElements);
        nameElementCommandsConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElements);

        ITUtilNames.assertExists("RFQ:Cryo-FS-051", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-051", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);

        uuid = nameElementCommandsConfirm[3].getUuid();
        nameElementCommandsConfirm[3].setUuid(null);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);
        nameElementCommandsConfirm[3].setUuid(uuid);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);
    }

    @Test
    void delete() {
        // test delete name
        //
        // note
        //     create in order to delete
        //
        // names for each of
        //     system group                       + description
        //     system group + device type + index + description
        //     system                             + description
        //     system       + device type + index + description
        //     subsystem                          + description
        //     subsystem    + device type + index + description

        NameElementCommandCreate[] nameElementCommandsCreate = null;
        NameElementCommandUpdate[] nameElementCommandsUpdate = null;
        NameElementCommandConfirm[] nameElementCommandsConfirm = null;
        NameElementCommand[] nameElementCommands = null;
        NameElement[] createdNameElements = null;
        int nbrNames = -1;

        nameElementCommands = new NameElementCommand[] {
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "061", "delete 1.3.061"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "062", "delete 1.3.062"),
                new NameElementCommand(null, systemGroupAcc,  deviceType_Cryo_FS, "063", "delete 1.3.063"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "061", "delete 2.3.061"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "062", "delete 2.3.062"),
                new NameElementCommand(null, systemRFQ,       deviceType_Cryo_FS, "063", "delete 2.3.063"),
                new NameElementCommand(null, subsystem010PRL, null,               null,  "delete 3.0.0"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "061", "delete 3.3.061"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "062", "delete 3.3.062"),
                new NameElementCommand(null, subsystem010PRL, deviceType_Cryo_FS, "063", "delete 3.3.063")
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertValidate(nameElementCommandsCreate, NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate, NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nameElementCommands = new NameElementCommand[] {
                nameElementCommands[0],
                nameElementCommands[1],
                nameElementCommands[2],
                nameElementCommands[3],
                nameElementCommands[4],
                nameElementCommands[5],
                nameElementCommands[7],
                nameElementCommands[8],
                nameElementCommands[9]
        };
        nameElementCommandsCreate = NameElementUtil.convertCommand2CommandCreate(nameElementCommands);
        nameElementCommandsUpdate = NameElementUtil.convertCommand2CommandUpdate(nameElementCommands);
        nameElementCommandsConfirm = NameElementUtil.convertCommand2CommandConfirm(nameElementCommands);

        ITUtilNames.assertExists("RFQ:Cryo-FS-061", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-061", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);

        nbrNames = ITUtilNames.assertRead("?deleted=false").getListSize();

        createdNameElements = ITUtilNames.assertCreate(nameElementCommandsCreate);
        assertNotNull(createdNameElements);
        assertEquals(9, createdNameElements.length);

        assertEquals(nbrNames + 9, ITUtilNames.assertRead("?deleted=false").getListSize());

        nameElementCommandsCreate = NameElementUtil.convertElement2CommandCreate(createdNameElements);
        nameElementCommandsUpdate = NameElementUtil.convertElement2CommandUpdate(createdNameElements);
        nameElementCommandsConfirm = NameElementUtil.convertElement2CommandConfirm(createdNameElements);

        ITUtilNames.assertExists("RFQ:Cryo-FS-061", Boolean.TRUE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-061", Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.TRUE);

        ITUtilNames.assertDelete(nameElementCommandsConfirm);

        assertEquals(nbrNames, ITUtilNames.assertRead("?deleted=false").getListSize());

        ITUtilNames.assertExists("RFQ:Cryo-FS-061", Boolean.FALSE);
        ITUtilNames.assertIsValidToCreate("RFQ:Cryo-FS-061", Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsCreate,  NameCommand.CREATE, Boolean.TRUE);
        ITUtilNames.assertValidate(nameElementCommandsUpdate,  NameCommand.UPDATE, Boolean.FALSE);
        ITUtilNames.assertValidate(nameElementCommandsConfirm, NameCommand.DELETE, Boolean.FALSE);
    }

}

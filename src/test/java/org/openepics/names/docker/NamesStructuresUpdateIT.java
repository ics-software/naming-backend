/*
 * Copyright (C) 2022 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;
import org.openepics.names.rest.beans.element.NameElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElement;
import org.openepics.names.rest.beans.element.StructureElementCommandCreate;
import org.openepics.names.rest.beans.element.StructureElementCommandUpdate;
import org.openepics.names.rest.beans.response.ResponsePageStructureElements;
import org.openepics.names.util.StructureElementUtil;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests for Naming and PostgreSQL that make use of existing dockerization
 * with docker-compose-integrationtest.yml / Dockerfile.integrationtest.
 *
 * <p>
 * Purpose of this class is to test rules for names and structures.
 * In practice, this class contains tests for names and structures, before and after update of structures.
 * </p>
 *
 * @author Lars Johansson
 */
@Testcontainers
class NamesStructuresUpdateIT {

    // note
    //     code coverage available
    //     setup of content - init, each test - combination of operations - delete, get, patch, post, put
    //     request to server, response from server - status code, json
    //
    //     create update delete structures and ensure name is as expected
    //
    // setup
    //     system structure
    //         Sg
    //         Sys
    //         Sys-Sub
    //     system structure + device structure + index
    //         Sg:Di-Dt-Idx
    //         Sys:Di-Dt-Idx
    //         Sys-Sub:Di-Dt-Idx

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    // system group
    //     sg0
    private static UUID sg1 = null;
    private static UUID sg2 = null;

    // system
    private static UUID sys11, sys12, sys13 = null;
    private static UUID sys21, sys22, sys23 = null;

    // subsystem
    //     sub111, sub112, sub113
    //     sub121, sub122, sub123
    //     sub131, sub132, sub133
    //     sub211, sub212, sub213
    //     sub221, sub222, sub223
    //     sub231, sub232, sub233
    private static UUID sub132 = null;
    private static UUID sub231, sub232, sub233 = null;

    // discipline
    private static UUID di2 = null;

    // device group
    private static UUID dg21, dg22, dg23 = null;

    // device type
    private static UUID dt211, dt212, dt213 = null;
    private static UUID dt221, dt222, dt223 = null;
    private static UUID dt231, dt232, dt233 = null;

    // name
    //     n211, n212, n213
    //     n221, n222, n223
    //     n231, n232, n233

    @BeforeAll
    public static void initAll() {
        // init
        //     system structure - system group, system, subsystem
        //     device structure - discipline, device group, device type
        //     name
        // check content

        StructureElementCommandCreate[] structureElementCommandsCreate = null;
        StructureElement[] structureElements = null;
        NameElementCommandCreate[] nameElementCommandsCreate = null;

        String description = "description";

        // setup - system structure
        //     system group
        //         sg0
        //     +
        //     system group
        //         Sg1
        //     system
        //         Sys11, Sys12, Sys13
        //     subsystem
        //         Sub111, Sub112, Sub113
        //         Sub121, Sub122, Sub123
        //         Sub131, Sub132, Sub133
        //     name (system structure only)
        //         Sg1
        //         Sys11, Sys12, Sys13
        //         Sys11-Sub111, Sys11-Sub112, Sys11-Sub113
        //         Sys12-Sub121, Sys12-Sub122, Sys12-Sub123
        //         Sys13-Sub131, Sys13-Sub132, Sys13-Sub133
        //     +
        //     system group
        //         Sg2
        //     system
        //         Sys21, Sys22, Sys23
        //     subsystem
        //         Sub211, Sub212, Sub213
        //         Sub221, Sub222, Sub223
        //         Sub231, Sub232, Sub233
        //     name (system structure only)
        //         Sg2
        //         Sys21, Sys22, Sys23
        //         Sys21-Sub211, Sys21-Sub212, Sys21-Sub213
        //         Sys22-Sub221, Sys22-Sub222, Sys22-Sub223
        //         Sys23-Sub231, Sys23-Sub232, Sys23-Sub233

        // system group
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, null,  null, description),
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Sg1", null, description),
                new StructureElementCommandCreate(Type.SYSTEMGROUP, null, "Sg2", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        sg1 = structureElements[1].getUuid();
        sg2 = structureElements[2].getUuid();

        // system
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, sg1, "Sys11", null, description),
                new StructureElementCommandCreate(Type.SYSTEM, sg1, "Sys12", null, description),
                new StructureElementCommandCreate(Type.SYSTEM, sg1, "Sys13", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        sys11 = structureElements[0].getUuid();
        sys12 = structureElements[1].getUuid();
        sys13 = structureElements[2].getUuid();

        // subsystem
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys11, "Sub111", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys11, "Sub112", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys11, "Sub113", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys12, "Sub121", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys12, "Sub122", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys12, "Sub123", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys13, "Sub131", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys13, "Sub132", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys13, "Sub133", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        sub132 = structureElements[7].getUuid();

        // system
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SYSTEM, sg2, "Sys21", null, description),
                new StructureElementCommandCreate(Type.SYSTEM, sg2, "Sys22", null, description),
                new StructureElementCommandCreate(Type.SYSTEM, sg2, "Sys23", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        sys21 = structureElements[0].getUuid();
        sys22 = structureElements[1].getUuid();
        sys23 = structureElements[2].getUuid();

        // subsystem
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys21, "Sub211", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys21, "Sub212", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys21, "Sub213", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys22, "Sub221", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys22, "Sub222", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys22, "Sub223", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys23, "Sub231", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys23, "Sub232", null, description),
                new StructureElementCommandCreate(Type.SUBSYSTEM, sys23, "Sub233", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        sub231 = structureElements[6].getUuid();
        sub232 = structureElements[7].getUuid();
        sub233 = structureElements[8].getUuid();

        // check content
        ITUtilStructures.assertRead("",                  27);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  3);
        ITUtilStructures.assertRead("?type=SYSTEM",       6);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",   18);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   0);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  0);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   0);
        ITUtilNames.assertRead("", 26);

        // setup - device structure
        //     above
        //     +
        //     discipline
        //         Di2
        //     device group
        //         -, -, -
        //     device type
        //         Dt211, Dt212, Dt213
        //         Dt221, Dt222, Dt223
        //         Dt231, Dt232, Dt233

        // discipline
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DISCIPLINE, null, "Di2", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        di2 = structureElements[0].getUuid();

        // device group
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICEGROUP, di2, "", null, description),
                new StructureElementCommandCreate(Type.DEVICEGROUP, di2, "", null, description),
                new StructureElementCommandCreate(Type.DEVICEGROUP, di2, "", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        dg21 = structureElements[0].getUuid();
        dg22 = structureElements[1].getUuid();
        dg23 = structureElements[2].getUuid();

        // device type
        structureElementCommandsCreate = new StructureElementCommandCreate[] {
                new StructureElementCommandCreate(Type.DEVICETYPE, dg21, "Dt211", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg21, "Dt212", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg21, "Dt213", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg22, "Dt221", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg22, "Dt222", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg22, "Dt223", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg23, "Dt231", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg23, "Dt232", null, description),
                new StructureElementCommandCreate(Type.DEVICETYPE, dg23, "Dt233", null, description)
        };
        structureElements = ITUtilStructures.assertCreate(structureElementCommandsCreate);
        dt211 = structureElements[0].getUuid();
        dt212 = structureElements[1].getUuid();
        dt213 = structureElements[2].getUuid();
        dt221 = structureElements[3].getUuid();
        dt222 = structureElements[4].getUuid();
        dt223 = structureElements[5].getUuid();
        dt231 = structureElements[6].getUuid();
        dt232 = structureElements[7].getUuid();
        dt233 = structureElements[8].getUuid();

        // check content
        ITUtilStructures.assertRead("",                  40);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  3);
        ITUtilStructures.assertRead("?type=SYSTEM",       6);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",   18);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  3);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   9);
        ITUtilNames.assertRead("", 26);

        // setup - name
        //     above
        //     +
        //     name (system structure + device structure + index)
        //         Sg2:Di2-Dt211-211
        //         Sg2:Di2-Dt212-212
        //         Sg2:Di2-Dt213-213
        //         Sys22:Di2-Dt221-221
        //         Sys22:Di2-Dt222-222
        //         Sys22:Di2-Dt223-223
        //         Sys23-Sub231:Di2-Dt231-231
        //         Sys23-Sub232:Di2-Dt232-232
        //         Sys23-Sub233:Di2-Dt233-233

        nameElementCommandsCreate = new NameElementCommandCreate[] {
                new NameElementCommandCreate(sg2,    dt211, "211", description),
                new NameElementCommandCreate(sg2,    dt212, "212", description),
                new NameElementCommandCreate(sg2,    dt213, "213", description),
                new NameElementCommandCreate(sys22,  dt221, "221", description),
                new NameElementCommandCreate(sys22,  dt222, "222", description),
                new NameElementCommandCreate(sys22,  dt223, "223", description),
                new NameElementCommandCreate(sub231, dt231, "231", description),
                new NameElementCommandCreate(sub232, dt232, "232", description),
                new NameElementCommandCreate(sub233, dt233, "233", description)
        };
        ITUtilNames.assertCreate(nameElementCommandsCreate);

        // check content
        ITUtilStructures.assertRead("",                  40);
        ITUtilStructures.assertRead("?type=SYSTEMGROUP",  3);
        ITUtilStructures.assertRead("?type=SYSTEM",       6);
        ITUtilStructures.assertRead("?type=SUBSYSTEM",   18);
        ITUtilStructures.assertRead("?type=DISCIPLINE",   1);
        ITUtilStructures.assertRead("?type=DEVICEGROUP",  3);
        ITUtilStructures.assertRead("?type=DEVICETYPE",   9);
        ITUtilNames.assertRead("", 35);

        ITUtilNames.assertExists("Sg1",                        Boolean.TRUE);
        ITUtilNames.assertExists("Sys11",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys12",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys13",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub111",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub112",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub113",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub121",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub122",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub123",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub131",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub132",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub133",               Boolean.TRUE);

        ITUtilNames.assertExists("Sg2",                        Boolean.TRUE);
        ITUtilNames.assertExists("Sys21",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys22",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys23",                      Boolean.TRUE);
        ITUtilNames.assertExists("Sys21-Sub211",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys21-Sub212",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys21-Sub213",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys22-Sub221",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys22-Sub222",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys22-Sub223",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232",               Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub233",               Boolean.TRUE);

        ITUtilNames.assertExists("Sg2:Di2-Dt211-211",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt212-212",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt213-213",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt221-221",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt222-222",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt223-223",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231:Di2-Dt231-231", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232:Di2-Dt232-232", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub233:Di2-Dt233-233", Boolean.TRUE);
    }

    @AfterAll
    public static void extractJacocoReport() {
        // extract jacoco report from container file system
        ITUtil.extractJacocoReport(ENVIRONMENT,
                ITUtil.JACOCO_TARGET_PREFIX + NamesStructuresUpdateIT.class.getSimpleName() + ITUtil.JACOCO_TARGET_SUFFIX);
    }

    @Test
    void updateSystemStructure() {
        // test update system structure and ensure name is as expected
        //
        // note
        //     update subsystem
        //     update system
        //     update system group
        //
        // check
        //     read
        //     exists

        ResponsePageStructureElements response = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;

        ITUtilNames.assertExists("Sg1",            Boolean.TRUE);
        ITUtilNames.assertExists("Sys11",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys12",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys13",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub111",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub112",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub113",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub121",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub122",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub123",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub131",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub132",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub133",   Boolean.TRUE);

        int count = ITUtilNames.assertRead("") .getTotalCount().intValue();

        // ----------------------------------------------------------------------------------------------------
        // sub132
        response = ITUtilStructures.assertFind("/" + sub132);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertExists("Sys13-Su132",    Boolean.FALSE);

        structureElementCommandUpdate.setMnemonic("Su132");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg1",            Boolean.TRUE);
        ITUtilNames.assertExists("Sys11",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys12",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys13",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub111",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub112",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub113",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub121",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub122",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub123",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub131",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub132",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys13-Sub133",   Boolean.TRUE);

        ITUtilNames.assertExists("Sys13-Su132",    Boolean.TRUE);

        // ----------------------------------------------------------------------------------------------------
        // sys12
        response = ITUtilStructures.assertFind("/" + sys12);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertExists("Sy12",           Boolean.FALSE);
        ITUtilNames.assertExists("Sy12-Sub121",    Boolean.FALSE);
        ITUtilNames.assertExists("Sy12-Sub122",    Boolean.FALSE);
        ITUtilNames.assertExists("Sy12-Sub123",    Boolean.FALSE);

        structureElementCommandUpdate.setMnemonic("Sy12");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg1",            Boolean.TRUE);
        ITUtilNames.assertExists("Sys11",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys12",          Boolean.FALSE);
        ITUtilNames.assertExists("Sys13",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub111",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub112",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub113",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub121",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys12-Sub122",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys12-Sub123",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys13-Sub131",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub132",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys13-Sub133",   Boolean.TRUE);

        ITUtilNames.assertExists("Sy12",           Boolean.TRUE);
        ITUtilNames.assertExists("Sy12-Sub121",    Boolean.TRUE);
        ITUtilNames.assertExists("Sy12-Sub122",    Boolean.TRUE);
        ITUtilNames.assertExists("Sy12-Sub123",    Boolean.TRUE);

        // ----------------------------------------------------------------------------------------------------
        // sg1
        response = ITUtilStructures.assertFind("/" + sg1);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertExists("S1",             Boolean.FALSE);

        structureElementCommandUpdate.setMnemonic("S1");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg1",            Boolean.FALSE);
        ITUtilNames.assertExists("Sys11",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys12",          Boolean.FALSE);
        ITUtilNames.assertExists("Sys13",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub111",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub112",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys11-Sub113",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys12-Sub121",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys12-Sub122",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys12-Sub123",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys13-Sub131",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys13-Sub132",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys13-Sub133",   Boolean.TRUE);

        ITUtilNames.assertExists("S1",             Boolean.TRUE);
    }

    @Test
    void updateDeviceStructure() {
        // test update device structure and ensure name is as expected
        //
        // note
        //     update device type
        //     update device group
        //     update discipline
        //
        // check
        //     read
        //     exists

        ResponsePageStructureElements response = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;

        ITUtilNames.assertExists("Sg2:Di2-Dt211-211",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt212-212",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt213-213",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt221-221",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt222-222",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt223-223",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231:Di2-Dt231-231", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232:Di2-Dt232-232", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub233:Di2-Dt233-233", Boolean.TRUE);

        int count = ITUtilNames.assertRead("") .getTotalCount().intValue();

        // ----------------------------------------------------------------------------------------------------
        // dt232
        response = ITUtilStructures.assertFind("/" + dt232);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertExists("Sys23-Sub232:Di2-D232-232",  Boolean.FALSE);

        structureElementCommandUpdate.setMnemonic("D232");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg2:Di2-Dt211-211",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt212-212",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt213-213",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt221-221",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt222-222",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt223-223",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231:Di2-Dt231-231", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232:Di2-Dt232-232", Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub233:Di2-Dt233-233", Boolean.TRUE);

        ITUtilNames.assertExists("Sys23-Sub232:Di2-D232-232",  Boolean.TRUE);

        // ----------------------------------------------------------------------------------------------------
        // dg22
        response = ITUtilStructures.assertFind("/" + dg22);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg2:Di2-Dt211-211",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt212-212",          Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:Di2-Dt213-213",          Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt221-221",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt222-222",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:Di2-Dt223-223",        Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231:Di2-Dt231-231", Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232:Di2-D232-232",  Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub233:Di2-Dt233-233", Boolean.TRUE);

        // ----------------------------------------------------------------------------------------------------
        // di2
        response = ITUtilStructures.assertFind("/" + di2);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(response.getList().get(0));
        structureElementCommandUpdate.setDescription("asdf");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertExists("Sg2:D2-Dt211-211",           Boolean.FALSE);
        ITUtilNames.assertExists("Sg2:D2-Dt212-212",           Boolean.FALSE);
        ITUtilNames.assertExists("Sg2:D2-Dt213-213",           Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:D2-Dt221-221",         Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:D2-Dt222-222",         Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:D2-Dt223-223",         Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub231:D2-Dt231-231",  Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub232:D2-D232-232",   Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub233:D2-Dt233-233",  Boolean.FALSE);

        structureElementCommandUpdate.setMnemonic("D2");
        ITUtilStructures.assertUpdate(structureElementCommandUpdate);

        ITUtilNames.assertRead("", count);

        ITUtilNames.assertExists("Sg2:Di2-Dt211-211",          Boolean.FALSE);
        ITUtilNames.assertExists("Sg2:Di2-Dt212-212",          Boolean.FALSE);
        ITUtilNames.assertExists("Sg2:Di2-Dt213-213",          Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:Di2-Dt221-221",        Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:Di2-Dt222-222",        Boolean.FALSE);
        ITUtilNames.assertExists("Sys22:Di2-Dt223-223",        Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub231:Di2-Dt231-231", Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub232:Di2-Dt232-232", Boolean.FALSE);
        ITUtilNames.assertExists("Sys23-Sub233:Di2-Dt233-233", Boolean.FALSE);

        ITUtilNames.assertExists("Sg2:D2-Dt211-211",           Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:D2-Dt212-212",           Boolean.TRUE);
        ITUtilNames.assertExists("Sg2:D2-Dt213-213",           Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:D2-Dt221-221",         Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:D2-Dt222-222",         Boolean.TRUE);
        ITUtilNames.assertExists("Sys22:D2-Dt223-223",         Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub231:D2-Dt231-231",  Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub232:D2-D232-232",   Boolean.TRUE);
        ITUtilNames.assertExists("Sys23-Sub233:D2-Dt233-233",  Boolean.TRUE);
    }

}

/*
 * Copyright (C) 2024 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.docker;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.openepics.names.rest.beans.response.ResponseBoolean;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Utility class to help (Docker) integration tests for Naming and PostgreSQL.
 *
 * @author Lars Johansson
 */
public class ITUtilPvNames {

    static final ObjectMapper mapper = new ObjectMapper();

    /**
     * This class is not to be instantiated.
     */
    private ITUtilPvNames() {
        throw new IllegalStateException("Utility class");
    }

    // ----------------------------------------------------------------------------------------------------

    public static ResponseBoolean assertValidProcessVariableName(String name, Boolean expected) {
        try {
            String[] response = null;
            ResponseBoolean responseBoolean = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_PVNAMES + "/" + name + "/validity");
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            responseBoolean = mapper.readValue(response[1], ResponseBoolean.class);

            assertEquals(expected, responseBoolean.getValue());
            if (expected) {
                assertNull   (responseBoolean.getMessage());
            } else {
                assertNotNull(responseBoolean.getMessage());
            }

            return responseBoolean;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }

}

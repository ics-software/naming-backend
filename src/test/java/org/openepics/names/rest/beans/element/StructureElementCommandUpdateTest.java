/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for StructureElementCommandUpdate class.
 *
 * @author Lars Johansson
 *
 * @see StructureElementCommandUpdate
 */
class StructureElementCommandUpdateTest {

    @Test
    void equals() {
        StructureElementCommandUpdate se1 = null, se2 = null;

        se1 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");

        assertEquals(se1, se2);
    }

    @Test
    void equalsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandUpdate se1 = null, se2 = null;
        String json = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";

        try {
            se1 = mapper.readValue(json, StructureElementCommandUpdate.class);
            se2 = mapper.readValue(json, StructureElementCommandUpdate.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertEquals(se1, se2);
    }

    @Test
    void equalsNoArgs() {
        StructureElementCommandUpdate se1 = null, se2 = null;

        se1 = new StructureElementCommandUpdate();
        se2 = new StructureElementCommandUpdate();

        assertEquals(se1, se2);
    }

    @Test
    void notEquals() {
        StructureElementCommandUpdate se1 = null, se2 = null;

        se1 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SUBSYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA2", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 42, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description 2");
        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandUpdate se1 = null, se2 = null;
        String json  = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";
        String json2 = "{\"uuid\": \"b14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";

        try {
            se1 = mapper.readValue(json,  StructureElementCommandUpdate.class);
            se2 = mapper.readValue(json2, StructureElementCommandUpdate.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsNoArgs() {
        StructureElementCommandUpdate se1 = null;
        StructureElementCommandConfirm se2 = null;

        se1 = new StructureElementCommandUpdate();
        se2 = new StructureElementCommandConfirm();

        assertNotEquals(se1, se2);
    }

    @Test
    void hashCodeEquals() {
        StructureElementCommandUpdate se1 = null, se2 = null;

        se1 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");

        assertNotNull(se1);
        assertNotNull(se2);
        assertEquals(se1, se2);
        assertEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        StructureElementCommandUpdate se1 = null, se2 = null;

        se1 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandUpdate(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");

        assertNotNull(se1);
        assertNotNull(se2);
        assertNotEquals(se1, se2);
        assertNotEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void toString_() {
        StructureElementCommandUpdate se1 = null;

        se1 = new StructureElementCommandUpdate(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description");

        String str = se1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
    }

}

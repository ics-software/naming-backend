/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Status;
import org.openepics.names.rest.beans.Type;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for StructureElement class.
 *
 * @author Lars Johansson
 *
 * @see StructureElement
 */
class StructureElementTest {

    @Test
    void equals() {
        StructureElement se1 = null, se2 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");

        assertEquals(se1, se2);
    }

    @Test
    void equalsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElement se1 = null, se2 = null;
        String json = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"mnemonicPath\": \"AA1\", \"ordering\": \"41\", \"level\": \"2\", \"description\": \"description\", \"status\": \"APPROVED\", \"deleted\": false, \"when\": null, \"who\": \"test who\", \"comment\": \"comment\"};";

        try {
            se1 = mapper.readValue(json, StructureElement.class);
            se2 = mapper.readValue(json, StructureElement.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertEquals(se1, se2);
    }

    @Test
    void equalsNoArgs() {
        StructureElement se1 = null, se2 = null;

        se1 = new StructureElement();
        se2 = new StructureElement();

        assertEquals(se1, se2);
    }

    @Test
    void notEquals() {
        StructureElement se1 = null, se2 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SUBSYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA2", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 42, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description 2",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA2", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 3,
                Status.APPROVED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.REJECTED, false,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, true,
                date, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                null, "test who", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who 2", "comment");
        assertNotEquals(se1, se2);

        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment 2");
        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElement se1 = null, se2 = null;
        String json  = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"mnemonicPath\": \"AA1\", \"ordering\": \"41\", \"level\": \"2\", \"description\": \"description\", \"status\": \"APPROVED\", \"deleted\": false, \"when\": null, \"who\": \"test who\", \"comment\": \"comment\"};";
        String json2 = "{\"uuid\": \"b14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\", \"parent\": \"05d52f1c-391e-41e3-a48f-dc8f36f8329b\", \"mnemonic\": \"AA1\", \"mnemonicPath\": \"AA1\", \"ordering\": \"41\", \"level\": \"2\", \"description\": \"description\", \"status\": \"APPROVED\", \"deleted\": false, \"when\": null, \"who\": \"test who\", \"comment\": \"comment\"};";

        try {
            se1 = mapper.readValue(json,  StructureElement.class);
            se2 = mapper.readValue(json2, StructureElement.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsNoArgs() {
        StructureElement se1 = null;
        StructureElementCommand se2 = null;

        se1 = new StructureElement();
        se2 = new StructureElementCommand();

        assertNotEquals(se1, se2);
    }

    @Test
    void hashCodeEquals() {
        StructureElement se1 = null, se2 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        se2 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");

        assertNotNull(se1);
        assertNotNull(se2);
        assertEquals(se1, se2);
        assertEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        StructureElement se1 = null, se2 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");
        se2 = new StructureElement(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");

        assertNotNull(se1);
        assertNotNull(se2);
        assertNotEquals(se1, se2);
        assertNotEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void toString_() {
        StructureElement se1 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");

        String str = se1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
        assertTrue(str.contains("description"));
        assertTrue(str.contains("comment"));
    }

    @Test
    void toStringSimple() {
        StructureElement se1 = null;
        Date date = new Date();

        se1 = new StructureElement(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM, UUID.fromString("05d52f1c-391e-41e3-a48f-dc8f36f8329b"),
                "AA1", 41, "description",
                "AA1", 2,
                Status.APPROVED, false,
                date, "test who", "comment");

        String str = se1.toStringSimple();

        assertNotNull(str);
        assertTrue (str.contains("uuid"));
        assertFalse(str.contains("description"));
        assertFalse(str.contains("comment"));
    }

}

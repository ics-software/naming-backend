/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for StructureElementCommandConfirm class.
 *
 * @author Lars Johansson
 *
 * @see StructureElementCommandConfirm
 */
class StructureElementCommandConfirmTest {

    @Test
    void equals() {
        StructureElementCommandConfirm se1 = null, se2 = null;

        se1 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);
        se2 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);

        assertEquals(se1, se2);
    }

    @Test
    void equalsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandConfirm se1 = null, se2 = null;
        String json = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\"};";

        try {
            se1 = mapper.readValue(json, StructureElementCommandConfirm.class);
            se2 = mapper.readValue(json, StructureElementCommandConfirm.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertEquals(se1, se2);
    }

    @Test
    void equalsNoArgs() {
        StructureElementCommandConfirm se1 = null, se2 = null;

        se1 = new StructureElementCommandConfirm();
        se2 = new StructureElementCommandConfirm();

        assertEquals(se1, se2);
    }

    @Test
    void notEquals() {
        StructureElementCommandConfirm se1 = null, se2 = null;

        se1 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandConfirm(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SUBSYSTEM);
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandConfirm(
                null, Type.SYSTEM);
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), null);
        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandConfirm se1 = null, se2 = null;
        String json  = "{\"uuid\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\"};";
        String json2 = "{\"uuid\": \"b14a8565-de10-4026-97e3-ab129ffaba96\", \"type\": \"SYSTEM\"};";

        try {
            se1 = mapper.readValue(json,  StructureElementCommandConfirm.class);
            se2 = mapper.readValue(json2, StructureElementCommandConfirm.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsNoArgs() {
        StructureElementCommandConfirm se1 = null;
        StructureElement se2 = null;

        se1 = new StructureElementCommandConfirm();
        se2 = new StructureElement();

        assertNotEquals(se1, se2);
    }

    @Test
    void hashCodeEquals() {
        StructureElementCommandConfirm se1 = null, se2 = null;

        se1 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);
        se2 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);

        assertNotNull(se1);
        assertNotNull(se2);
        assertEquals(se1, se2);
        assertEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        StructureElementCommandConfirm se1 = null, se2 = null;

        se1 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);
        se2 = new StructureElementCommandConfirm(
                UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);

        assertNotNull(se1);
        assertNotNull(se2);
        assertNotEquals(se1, se2);
        assertNotEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void toString_() {
        StructureElementCommandConfirm se1 = null;

        se1 = new StructureElementCommandConfirm(
                UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"), Type.SYSTEM);

        String str = se1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
    }

}

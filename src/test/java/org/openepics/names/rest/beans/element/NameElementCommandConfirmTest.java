/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for NameElementCommandConfirm class.
 *
 * @author Lars Johansson
 *
 * @see NameElementCommandConfirm
 */
class NameElementCommandConfirmTest {

    @Test
    void equals() {
        NameElementCommandConfirm ne1 = null, ne2 = null;

        ne1 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));
        ne2 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));

        assertEquals(ne1, ne2);
    }

    @Test
    void equalsJson() {
        ObjectMapper mapper = new ObjectMapper();
        NameElementCommandConfirm ne1 = null, ne2 = null;
        String json = "{\"uuid\": \"45bdc415-cf5a-4650-b6dd-478540830c2a\"}";

        try {
            ne1 = mapper.readValue(json, NameElementCommandConfirm.class);
            ne2 = mapper.readValue(json, NameElementCommandConfirm.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertEquals(ne1, ne2);
    }

    @Test
    void equalsNoArgs() {
        NameElementCommandConfirm ne1 = null, ne2 = null;

        ne1 = new NameElementCommandConfirm();
        ne2 = new NameElementCommandConfirm();

        assertEquals(ne1, ne2);
    }

    @Test
    void notEquals() {
        NameElementCommandConfirm ne1 = null, ne2 = null;

        ne1 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));
        assertNotEquals(ne1, ne2);

        ne2 = new NameElementCommandConfirm(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"));
        assertNotEquals(ne1, ne2);

        ne2 = new NameElementCommandConfirm(
                null);
        assertNotEquals(ne1, ne2);
    }

    @Test
    void notEqualsJson() {
        ObjectMapper mapper = new ObjectMapper();
        NameElementCommandConfirm ne1 = null, ne2 = null;
        String json  = "{\"uuid\": \"45bdc415-cf5a-4650-b6dd-478540830c2a\"}";
        String json2 = "{\"uuid\": \"55bdc415-cf5a-4650-b6dd-478540830c2a\"}";

        try {
            ne1 = mapper.readValue(json,  NameElementCommandConfirm.class);
            ne2 = mapper.readValue(json2, NameElementCommandConfirm.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertNotEquals(ne1, ne2);
    }

    @Test
    void notEqualsNoArgs() {
        NameElementCommandConfirm ne1 = null;
        NameElement ne2 = null;

        ne1 = new NameElementCommandConfirm();
        ne2 = new NameElement();

        assertNotEquals(ne1, ne2);
    }

    @Test
    void hashCodeEquals() {
        NameElementCommandConfirm ne1 = null, ne2 = null;

        ne1 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));
        ne2 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));

        assertNotNull(ne1);
        assertNotNull(ne2);
        assertEquals(ne1, ne2);
        assertEquals(ne1.hashCode(), ne2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        NameElementCommandConfirm ne1 = null, ne2 = null;

        ne1 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));
        ne2 = new NameElementCommandConfirm(
                UUID.fromString("55bdc415-cf5a-4650-b6dd-478540830c2a"));

        assertNotNull(ne1);
        assertNotNull(ne2);
        assertNotEquals(ne1, ne2);
        assertNotEquals(ne1.hashCode(), ne2.hashCode());
    }

    @Test
    void toString_() {
        NameElementCommandConfirm ne1 = null;

        ne1 = new NameElementCommandConfirm(
                UUID.fromString("45bdc415-cf5a-4650-b6dd-478540830c2a"));

        String str = ne1.toString();

        assertNotNull(str);
        assertTrue(str.contains("uuid"));
    }

}

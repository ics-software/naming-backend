/*
 * Copyright (c) 2023 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package org.openepics.names.rest.beans.element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.openepics.names.rest.beans.Type;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit tests for StructureElementCommandCreate class.
 *
 * @author Lars Johansson
 *
 * @see StructureElementCommandCreate
 */
class StructureElementCommandCreateTest {

    @Test
    void equals() {
        StructureElementCommandCreate se1 = null, se2 = null;

        se1 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");

        assertEquals(se1, se2);
    }

    @Test
    void equalsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandCreate se1 = null, se2 = null;
        String json = "{\"type\": \"SYSTEM\", \"parent\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";

        try {
            se1 = mapper.readValue(json, StructureElementCommandCreate.class);
            se2 = mapper.readValue(json, StructureElementCommandCreate.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertEquals(se1, se2);
    }

    @Test
    void equalsNoArgs() {
        StructureElementCommandCreate se1 = null, se2 = null;

        se1 = new StructureElementCommandCreate();
        se2 = new StructureElementCommandCreate();

        assertEquals(se1, se2);
    }

    @Test
    void notEquals() {
        StructureElementCommandCreate se1 = null, se2 = null;

        se1 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandCreate(
                Type.SUBSYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA2", 41, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 42, "description");
        assertNotEquals(se1, se2);

        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description 2");
        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsJson() {
        ObjectMapper mapper = new ObjectMapper();
        StructureElementCommandCreate se1 = null, se2 = null;
        String json  = "{\"type\": \"SYSTEM\", \"parent\": \"a14a8565-de10-4026-97e3-ab129ffaba96\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";
        String json2 = "{\"type\": \"SYSTEM\", \"parent\": \"b14a8565-de10-4026-97e3-ab129ffaba96\", \"mnemonic\": \"AA1\", \"ordering\": \"41\", \"description\": \"description\"};";

        try {
            se1 = mapper.readValue(json,  StructureElementCommandCreate.class);
            se2 = mapper.readValue(json2, StructureElementCommandCreate.class);
        } catch (JsonMappingException e) {
            fail();
        } catch (JsonProcessingException e) {
            fail();
        }

        assertNotEquals(se1, se2);
    }

    @Test
    void notEqualsNoArgs() {
        StructureElementCommandCreate se1 = null;
        StructureElementCommandUpdate se2 = null;

        se1 = new StructureElementCommandCreate();
        se2 = new StructureElementCommandUpdate();

        assertNotEquals(se1, se2);
    }

    @Test
    void hashCodeEquals() {
        StructureElementCommandCreate se1 = null, se2 = null;

        se1 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");

        assertNotNull(se1);
        assertNotNull(se2);
        assertEquals(se1, se2);
        assertEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void hashCodeNotEquals() {
        StructureElementCommandCreate se1 = null, se2 = null;

        se1 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");
        se2 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("b14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");

        assertNotNull(se1);
        assertNotNull(se2);
        assertNotEquals(se1, se2);
        assertNotEquals(se1.hashCode(), se2.hashCode());
    }

    @Test
    void toString_() {
        StructureElementCommandCreate se1 = null;

        se1 = new StructureElementCommandCreate(
                Type.SYSTEM, UUID.fromString("a14a8565-de10-4026-97e3-ab129ffaba96"),
                "AA1", 41, "description");

        String str = se1.toString();

        assertNotNull(str);
        assertTrue(str.contains("parent"));
    }

}

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.24

-- Started on 2024-03-04 17:19:26 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 228 (class 1255 OID 16623)
-- Name: get_instance_index(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_instance_index(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    len int;
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            len = length(mnemonic_path);
            pos = strpos(mnemonic_path, '-');
            mnemonic_path = reverse(mnemonic_path);
            RETURN substr(mnemonic_path, len - pos + 2);
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;


--
-- TOC entry 214 (class 1255 OID 16622)
-- Name: get_mnemonic_path_device_structure(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_device_structure(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            mnemonic_path = substr(mnemonic_path, strpos(mnemonic_path, '-')+1);
            RETURN reverse(mnemonic_path);
        ELSIF nbr_delimiters = 1 then
            return mnemonic_path;
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;


--
-- TOC entry 215 (class 1255 OID 16626)
-- Name: get_mnemonic_path_devicegroup(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_devicegroup(devicegroup_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid into discipline_uuid from devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null then
        return discipline_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 231 (class 1255 OID 16627)
-- Name: get_mnemonic_path_devicetype(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_devicetype(devicetype_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    devicetype_mnemonic text;
    devicegroup_uuid text;
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into devicegroup_uuid, devicetype_mnemonic from devicetype where uuid = devicetype_uuid and latest = true;
    select parent_uuid, mnemonic into discipline_uuid from devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null and devicetype_mnemonic is not null then
        return concat(discipline_mnemonic, '-', devicetype_mnemonic);
    elsif devicetype_mnemonic is not null then
        return devicetype_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 230 (class 1255 OID 16625)
-- Name: get_mnemonic_path_subsystem(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_subsystem(subsystem_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    subsystem_mnemonic text;
    system_uuid text;
    system_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into system_uuid, subsystem_mnemonic from subsystem where uuid = subsystem_uuid and latest = true;
    select mnemonic into system_mnemonic from "system" where uuid = system_uuid and latest = true;

    if system_mnemonic is not null and subsystem_mnemonic is not null then
        return concat(system_mnemonic, '-', subsystem_mnemonic);
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 229 (class 1255 OID 16624)
-- Name: get_mnemonic_path_system(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_system(system_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    system_mnemonic text;
BEGIN
    select mnemonic into system_mnemonic from "system" where uuid = system_uuid and latest = true;

    if system_mnemonic is not null then
        return system_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 213 (class 1255 OID 16621)
-- Name: get_mnemonic_path_system_structure(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_system_structure(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    pos int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        RETURN substr(convention_name, 1, pos-1);
    END IF;
    RETURN convention_name;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16386)
-- Name: appinfo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.appinfo (
    id bigint NOT NULL,
    version integer,
    schemaversion integer NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 16389)
-- Name: appinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.appinfo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 186
-- Name: appinfo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.appinfo_id_seq OWNED BY public.appinfo.id;


--
-- TOC entry 187 (class 1259 OID 16391)
-- Name: device; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.device (
    id bigint NOT NULL,
    version integer,
    uuid character varying(255)
);


--
-- TOC entry 188 (class 1259 OID 16394)
-- Name: device_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 188
-- Name: device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.device_id_seq OWNED BY public.device.id;


--
-- TOC entry 203 (class 1259 OID 16518)
-- Name: devicegroup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicegroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 210 (class 1259 OID 16594)
-- Name: devicegroup_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicegroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 210
-- Name: devicegroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicegroup_id_seq OWNED BY public.devicegroup.id;


--
-- TOC entry 189 (class 1259 OID 16396)
-- Name: devicerevision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicerevision (
    id bigint NOT NULL,
    version integer,
    additionalinfo character varying(255),
    conventionname character varying(255),
    conventionnameeqclass character varying(255),
    deleted boolean NOT NULL,
    instanceindex character varying(255),
    requestdate timestamp without time zone,
    device_id bigint,
    devicetype_id bigint,
    requestedby_id bigint,
    section_id bigint,
    processorcomment character varying(255)
);


--
-- TOC entry 190 (class 1259 OID 16402)
-- Name: devicerevision_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicerevision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 190
-- Name: devicerevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicerevision_id_seq OWNED BY public.devicerevision.id;


--
-- TOC entry 204 (class 1259 OID 16524)
-- Name: devicetype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicetype (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 211 (class 1259 OID 16596)
-- Name: devicetype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 211
-- Name: devicetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicetype_id_seq OWNED BY public.devicetype.id;


--
-- TOC entry 202 (class 1259 OID 16512)
-- Name: discipline; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.discipline (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 209 (class 1259 OID 16592)
-- Name: discipline_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.discipline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 209
-- Name: discipline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.discipline_id_seq OWNED BY public.discipline.id;


--
-- TOC entry 205 (class 1259 OID 16530)
-- Name: name; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.name (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    systemgroup_uuid text,
    system_uuid text,
    subsystem_uuid text,
    devicetype_uuid text,
    instance_index text,
    convention_name text,
    convention_name_equivalence text,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 212 (class 1259 OID 16598)
-- Name: name_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 212
-- Name: name_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.name_id_seq OWNED BY public.name.id;


--
-- TOC entry 191 (class 1259 OID 16404)
-- Name: namepart; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.namepart (
    id bigint NOT NULL,
    version integer,
    nameparttype character varying(255),
    uuid character varying(255)
);


--
-- TOC entry 192 (class 1259 OID 16410)
-- Name: namepart_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.namepart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 192
-- Name: namepart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.namepart_id_seq OWNED BY public.namepart.id;


--
-- TOC entry 193 (class 1259 OID 16412)
-- Name: namepartrevision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.namepartrevision (
    id bigint NOT NULL,
    version integer,
    deleted boolean NOT NULL,
    description character varying(255),
    mnemonic character varying(255),
    mnemoniceqclass character varying(255),
    name character varying(255),
    processdate timestamp without time zone,
    processorcomment character varying(255),
    requestdate timestamp without time zone,
    requestercomment character varying(255),
    status character varying(255),
    namepart_id bigint,
    parent_id bigint,
    processedby_id bigint,
    requestedby_id bigint
);


--
-- TOC entry 194 (class 1259 OID 16418)
-- Name: namepartrevision_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.namepartrevision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 194
-- Name: namepartrevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.namepartrevision_id_seq OWNED BY public.namepartrevision.id;


--
-- TOC entry 201 (class 1259 OID 16506)
-- Name: subsystem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subsystem (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 208 (class 1259 OID 16590)
-- Name: subsystem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subsystem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 208
-- Name: subsystem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subsystem_id_seq OWNED BY public.subsystem.id;


--
-- TOC entry 200 (class 1259 OID 16500)
-- Name: system; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 207 (class 1259 OID 16588)
-- Name: system_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 207
-- Name: system_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_id_seq OWNED BY public.system.id;


--
-- TOC entry 199 (class 1259 OID 16494)
-- Name: systemgroup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.systemgroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 206 (class 1259 OID 16586)
-- Name: systemgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.systemgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 206
-- Name: systemgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.systemgroup_id_seq OWNED BY public.systemgroup.id;


--
-- TOC entry 195 (class 1259 OID 16420)
-- Name: useraccount; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.useraccount (
    id bigint NOT NULL,
    version integer,
    role character varying(255),
    username character varying(255)
);


--
-- TOC entry 196 (class 1259 OID 16426)
-- Name: useraccount_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.useraccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 196
-- Name: useraccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.useraccount_id_seq OWNED BY public.useraccount.id;


--
-- TOC entry 2101 (class 2604 OID 16428)
-- Name: appinfo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appinfo ALTER COLUMN id SET DEFAULT nextval('public.appinfo_id_seq'::regclass);


--
-- TOC entry 2102 (class 2604 OID 16429)
-- Name: device id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.device ALTER COLUMN id SET DEFAULT nextval('public.device_id_seq'::regclass);


--
-- TOC entry 2112 (class 2604 OID 16604)
-- Name: devicegroup id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicegroup ALTER COLUMN id SET DEFAULT nextval('public.devicegroup_id_seq'::regclass);


--
-- TOC entry 2103 (class 2604 OID 16430)
-- Name: devicerevision id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision ALTER COLUMN id SET DEFAULT nextval('public.devicerevision_id_seq'::regclass);


--
-- TOC entry 2113 (class 2604 OID 16605)
-- Name: devicetype id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicetype ALTER COLUMN id SET DEFAULT nextval('public.devicetype_id_seq'::regclass);


--
-- TOC entry 2111 (class 2604 OID 16603)
-- Name: discipline id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discipline ALTER COLUMN id SET DEFAULT nextval('public.discipline_id_seq'::regclass);


--
-- TOC entry 2114 (class 2604 OID 16606)
-- Name: name id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.name ALTER COLUMN id SET DEFAULT nextval('public.name_id_seq'::regclass);


--
-- TOC entry 2104 (class 2604 OID 16431)
-- Name: namepart id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepart ALTER COLUMN id SET DEFAULT nextval('public.namepart_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 16432)
-- Name: namepartrevision id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision ALTER COLUMN id SET DEFAULT nextval('public.namepartrevision_id_seq'::regclass);


--
-- TOC entry 2110 (class 2604 OID 16602)
-- Name: subsystem id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subsystem ALTER COLUMN id SET DEFAULT nextval('public.subsystem_id_seq'::regclass);


--
-- TOC entry 2109 (class 2604 OID 16601)
-- Name: system id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system ALTER COLUMN id SET DEFAULT nextval('public.system_id_seq'::regclass);


--
-- TOC entry 2108 (class 2604 OID 16600)
-- Name: systemgroup id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.systemgroup ALTER COLUMN id SET DEFAULT nextval('public.systemgroup_id_seq'::regclass);


--
-- TOC entry 2106 (class 2604 OID 16433)
-- Name: useraccount id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.useraccount ALTER COLUMN id SET DEFAULT nextval('public.useraccount_id_seq'::regclass);


--
-- TOC entry 2318 (class 0 OID 16386)
-- Dependencies: 185
-- Data for Name: appinfo; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.appinfo (id, version, schemaversion) FROM stdin;
1	1	1
\.


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 186
-- Name: appinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.appinfo_id_seq', 1, true);


--
-- TOC entry 2320 (class 0 OID 16391)
-- Dependencies: 187
-- Data for Name: device; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.device (id, version, uuid) FROM stdin;
\.


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 188
-- Name: device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.device_id_seq', 1, false);


--
-- TOC entry 2336 (class 0 OID 16518)
-- Dependencies: 203
-- Data for Name: devicegroup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicegroup (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
\.


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 210
-- Name: devicegroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicegroup_id_seq', 1, false);


--
-- TOC entry 2322 (class 0 OID 16396)
-- Dependencies: 189
-- Data for Name: devicerevision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicerevision (id, version, additionalinfo, conventionname, conventionnameeqclass, deleted, instanceindex, requestdate, device_id, devicetype_id, requestedby_id, section_id, processorcomment) FROM stdin;
\.


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 190
-- Name: devicerevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicerevision_id_seq', 1, false);


--
-- TOC entry 2337 (class 0 OID 16524)
-- Dependencies: 204
-- Data for Name: devicetype; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicetype (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
\.


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 211
-- Name: devicetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicetype_id_seq', 1, false);


--
-- TOC entry 2335 (class 0 OID 16512)
-- Dependencies: 202
-- Data for Name: discipline; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.discipline (id, version, uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
\.


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 209
-- Name: discipline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.discipline_id_seq', 1, false);


--
-- TOC entry 2338 (class 0 OID 16530)
-- Dependencies: 205
-- Data for Name: name; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.name (id, version, uuid, systemgroup_uuid, system_uuid, subsystem_uuid, devicetype_uuid, instance_index, convention_name, convention_name_equivalence, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	aa42ed64-4dd6-40c5-80fc-4b8381da414e	b3f5b914-9607-4df8-a701-553f4d238e1e	\N	\N	\N	\N	Sg	SG	System structure only	APPROVED	t	f	2024-03-04 16:18:38.976	test who	\N	\N	\N	\N
2	0	33db7396-ed60-4368-bb6d-481dca8cfee4	\N	25a231ac-cc79-43f0-a8aa-00d34b6262a8	\N	\N	\N	Sys	SYS	System structure only	APPROVED	t	f	2024-03-04 16:18:39.52	test who	\N	\N	\N	\N
3	0	c80cc24f-bdd9-4469-90fb-c28469ca301c	\N	\N	0ccfe162-7bf2-46f0-b670-910e9c7a0f99	\N	\N	Sys-AA1	SYS-AA1	System structure only	APPROVED	t	f	2024-03-04 16:18:39.858	test who	\N	\N	\N	\N
4	0	34fdad07-44c3-4d6e-9821-ea78e9d9950a	\N	\N	4fe32787-9e4e-48df-b2d4-413b6310ddd8	\N	\N	Sys-AA2	SYS-AA2	System structure only	APPROVED	t	f	2024-03-04 16:18:39.858	test who	\N	\N	\N	\N
5	0	eb39c613-f8cc-4e35-84be-a71eb6c4e371	\N	\N	921fd974-b02e-4535-9fa4-832e1f413a2f	\N	\N	Sys-AA3	SYS-AA3	System structure only	APPROVED	t	f	2024-03-04 16:18:39.858	test who	\N	\N	\N	\N
6	0	f6cd08cb-8383-404d-bf24-72280e2fdfb2	\N	\N	d8c8a289-3b50-4d45-bff0-0ba6933235fd	\N	\N	Sys-AA4	SYS-AA4	System structure only	APPROVED	t	f	2024-03-04 16:18:39.858	test who	\N	\N	\N	\N
7	0	4d8db96a-9a42-4901-9e56-5ae9f915a9fd	\N	\N	ca0d26d2-fcf0-4844-9509-9c60835857e1	\N	\N	Sys-AA5	SYS-AA5	System structure only	APPROVED	t	f	2024-03-04 16:18:39.858	test who	\N	\N	\N	\N
8	0	8bdd044f-17bd-4846-ba10-49c5c4bb3418	\N	\N	ed15e5ff-0641-4185-9fb0-f43702a14350	\N	\N	Sys-AB1	SYS-AB1	System structure only	APPROVED	t	f	2024-03-04 16:18:39.95	test who	\N	\N	\N	\N
9	0	067b4dd9-fd59-4786-8b65-58bb0177e3ce	\N	\N	8e158f4b-0ea5-4ca9-87ae-def88d72a647	\N	\N	Sys-AB2	SYS-AB2	System structure only	APPROVED	t	f	2024-03-04 16:18:39.95	test who	\N	\N	\N	\N
10	0	b47a43f0-8991-4fc4-a6d1-0296578e1bff	\N	\N	b8e9bbfc-e3c4-4eef-883e-0e2d0709ec03	\N	\N	Sys-AB3	SYS-AB3	System structure only	APPROVED	t	f	2024-03-04 16:18:39.95	test who	\N	\N	\N	\N
11	0	d84cce5c-a687-473f-b4bc-7512d4ac820f	\N	\N	d13ea971-d722-45ea-bbab-96c5e221a344	\N	\N	Sys-AB4	SYS-AB4	System structure only	APPROVED	t	f	2024-03-04 16:18:39.95	test who	\N	\N	\N	\N
12	0	23937a8b-39a0-4193-a436-df45ad9cef88	\N	\N	63d701ba-555b-41fd-9810-9d87454aa1fa	\N	\N	Sys-AB5	SYS-AB5	System structure only	APPROVED	t	f	2024-03-04 16:18:39.95	test who	\N	\N	\N	\N
18	0	809eec71-dd7c-4a26-a50c-85bf9c1ec6d3	\N	\N	9659c6e8-7d37-47d8-aaf6-eeb7ada222d1	\N	\N	Sys-AC1	SYS-AC1	System structure only	APPROVED	t	t	2024-03-04 16:18:40.261	test who	\N	\N	\N	\N
13	1	809eec71-dd7c-4a26-a50c-85bf9c1ec6d3	\N	\N	9659c6e8-7d37-47d8-aaf6-eeb7ada222d1	\N	\N	Sys-AC1	SYS-AC1	System structure only	APPROVED	f	f	2024-03-04 16:18:40.157	test who	\N	\N	\N	\N
19	0	e0bb293d-a1a4-4146-97ba-c5ca6e69cc48	\N	\N	0db3c127-d8f7-48ea-8af1-6433928f928b	\N	\N	Sys-AC2	SYS-AC2	System structure only	APPROVED	t	t	2024-03-04 16:18:40.272	test who	\N	\N	\N	\N
14	1	e0bb293d-a1a4-4146-97ba-c5ca6e69cc48	\N	\N	0db3c127-d8f7-48ea-8af1-6433928f928b	\N	\N	Sys-AC2	SYS-AC2	System structure only	APPROVED	f	f	2024-03-04 16:18:40.157	test who	\N	\N	\N	\N
20	0	4b013f5e-e25c-4936-8a37-e1c0ba0fb7b8	\N	\N	5d1164af-e155-433e-a8e6-1a60cbc315b8	\N	\N	Sys-AC3	SYS-AC3	System structure only	APPROVED	t	t	2024-03-04 16:18:40.282	test who	\N	\N	\N	\N
15	1	4b013f5e-e25c-4936-8a37-e1c0ba0fb7b8	\N	\N	5d1164af-e155-433e-a8e6-1a60cbc315b8	\N	\N	Sys-AC3	SYS-AC3	System structure only	APPROVED	f	f	2024-03-04 16:18:40.157	test who	\N	\N	\N	\N
21	0	665f4a7d-c678-4e01-9fce-3b3f5709a384	\N	\N	2542b027-513b-40bb-9be8-74fcb400459e	\N	\N	Sys-AC4	SYS-AC4	System structure only	APPROVED	t	t	2024-03-04 16:18:40.291	test who	\N	\N	\N	\N
16	1	665f4a7d-c678-4e01-9fce-3b3f5709a384	\N	\N	2542b027-513b-40bb-9be8-74fcb400459e	\N	\N	Sys-AC4	SYS-AC4	System structure only	APPROVED	f	f	2024-03-04 16:18:40.157	test who	\N	\N	\N	\N
22	0	96f51bac-74cf-49ca-a73f-9d4d411e2a1d	\N	\N	e1e4b087-c3d5-409f-9a5f-a9e9f84962fb	\N	\N	Sys-AC5	SYS-AC5	System structure only	APPROVED	t	t	2024-03-04 16:18:40.3	test who	\N	\N	\N	\N
17	1	96f51bac-74cf-49ca-a73f-9d4d411e2a1d	\N	\N	e1e4b087-c3d5-409f-9a5f-a9e9f84962fb	\N	\N	Sys-AC5	SYS-AC5	System structure only	APPROVED	f	f	2024-03-04 16:18:40.157	test who	\N	\N	\N	\N
28	0	91476425-714e-4ed4-bc93-d6a1087e4e61	\N	\N	2cb15615-72a4-48af-aa88-57fe20caaebe	\N	\N	Sys-AD1	SYS-AD1	System structure only	APPROVED	t	t	2024-03-04 16:18:40.48	test who	\N	\N	\N	\N
23	1	91476425-714e-4ed4-bc93-d6a1087e4e61	\N	\N	2cb15615-72a4-48af-aa88-57fe20caaebe	\N	\N	Sys-AD1	SYS-AD1	System structure only	APPROVED	f	f	2024-03-04 16:18:40.361	test who	\N	\N	\N	\N
29	0	e147bba9-4389-47d5-90d1-45672e947173	\N	\N	1c764e48-dbda-4d6a-baf0-013be384dce0	\N	\N	Sys-AD2	SYS-AD2	System structure only	APPROVED	t	t	2024-03-04 16:18:40.484	test who	\N	\N	\N	\N
24	1	e147bba9-4389-47d5-90d1-45672e947173	\N	\N	1c764e48-dbda-4d6a-baf0-013be384dce0	\N	\N	Sys-AD2	SYS-AD2	System structure only	APPROVED	f	f	2024-03-04 16:18:40.361	test who	\N	\N	\N	\N
30	0	0bfa9007-1399-418e-94b2-cff5185d09a3	\N	\N	700ff3d0-ec0f-41b0-9d0d-dfd4c8091b9d	\N	\N	Sys-AD3	SYS-AD3	System structure only	APPROVED	t	t	2024-03-04 16:18:40.488	test who	\N	\N	\N	\N
25	1	0bfa9007-1399-418e-94b2-cff5185d09a3	\N	\N	700ff3d0-ec0f-41b0-9d0d-dfd4c8091b9d	\N	\N	Sys-AD3	SYS-AD3	System structure only	APPROVED	f	f	2024-03-04 16:18:40.361	test who	\N	\N	\N	\N
31	0	7089a67c-5cad-40bb-8165-9fa7e7aa7eb4	\N	\N	ab167f84-7c67-4364-a6b7-42405d98cb69	\N	\N	Sys-AD4	SYS-AD4	System structure only	APPROVED	t	t	2024-03-04 16:18:40.491	test who	\N	\N	\N	\N
26	1	7089a67c-5cad-40bb-8165-9fa7e7aa7eb4	\N	\N	ab167f84-7c67-4364-a6b7-42405d98cb69	\N	\N	Sys-AD4	SYS-AD4	System structure only	APPROVED	f	f	2024-03-04 16:18:40.361	test who	\N	\N	\N	\N
32	0	25b75ada-e6c6-4255-9268-f940adfc1729	\N	\N	8f76876a-504e-4402-bd60-6bb4c60d4d29	\N	\N	Sys-AD5	SYS-AD5	System structure only	APPROVED	t	t	2024-03-04 16:18:40.496	test who	\N	\N	\N	\N
27	1	25b75ada-e6c6-4255-9268-f940adfc1729	\N	\N	8f76876a-504e-4402-bd60-6bb4c60d4d29	\N	\N	Sys-AD5	SYS-AD5	System structure only	APPROVED	f	f	2024-03-04 16:18:40.361	test who	\N	\N	\N	\N
33	0	dec7ecf7-b8a3-4bcd-a0bf-cce3def865c8	\N	\N	b8485a11-1c59-4ef5-876b-16a49512d457	\N	\N	Sys-AE1	SYS-AE1	System structure only	APPROVED	t	f	2024-03-04 16:18:40.556	test who	\N	\N	\N	\N
34	0	5644b3da-abfa-45ff-beb6-68c4f87aa237	\N	\N	30fbb919-376d-40d2-9bd2-3c9fd7b952da	\N	\N	Sys-AE2	SYS-AE2	System structure only	APPROVED	t	f	2024-03-04 16:18:40.556	test who	\N	\N	\N	\N
35	0	aaee4277-8fd0-46fc-b80d-bc87333a506d	\N	\N	89feee5b-5003-4dc6-a693-d0e2dd92112b	\N	\N	Sys-AE3	SYS-AE3	System structure only	APPROVED	t	f	2024-03-04 16:18:40.556	test who	\N	\N	\N	\N
36	0	8813dbcd-0454-45f3-ac5d-24559042218e	\N	\N	f4eaf55f-01bb-4a37-8d61-56f576444dd7	\N	\N	Sys-AE4	SYS-AE4	System structure only	APPROVED	t	f	2024-03-04 16:18:40.556	test who	\N	\N	\N	\N
37	0	1b7f2c10-5504-42cb-aaf4-2aa1230aafd8	\N	\N	87932c6c-909e-4165-a75d-2bc10db24415	\N	\N	Sys-AE5	SYS-AE5	System structure only	APPROVED	t	f	2024-03-04 16:18:40.556	test who	\N	\N	\N	\N
43	0	523d31e4-fda3-4830-bb6a-d043b1a8fed6	\N	\N	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	\N	\N	Sys-AF1	SYS-AF1	System structure only	APPROVED	t	t	2024-03-04 16:18:41.008	test who	\N	\N	\N	\N
38	1	523d31e4-fda3-4830-bb6a-d043b1a8fed6	\N	\N	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	\N	\N	Sys-AF1	SYS-AF1	System structure only	APPROVED	f	f	2024-03-04 16:18:40.822	test who	\N	\N	\N	\N
44	0	8c59fa83-4f97-4225-86d7-5c7efc73cf75	\N	\N	0eb86e59-0afc-4a49-a554-4a449d1cec3d	\N	\N	Sys-AF2	SYS-AF2	System structure only	APPROVED	t	t	2024-03-04 16:18:41.012	test who	\N	\N	\N	\N
39	1	8c59fa83-4f97-4225-86d7-5c7efc73cf75	\N	\N	0eb86e59-0afc-4a49-a554-4a449d1cec3d	\N	\N	Sys-AF2	SYS-AF2	System structure only	APPROVED	f	f	2024-03-04 16:18:40.822	test who	\N	\N	\N	\N
45	0	fe412faa-2226-4bf0-82fb-96ada804a96f	\N	\N	efe208d0-4e11-4c28-b84c-f0d589448ccb	\N	\N	Sys-AF3	SYS-AF3	System structure only	APPROVED	t	t	2024-03-04 16:18:41.016	test who	\N	\N	\N	\N
40	1	fe412faa-2226-4bf0-82fb-96ada804a96f	\N	\N	efe208d0-4e11-4c28-b84c-f0d589448ccb	\N	\N	Sys-AF3	SYS-AF3	System structure only	APPROVED	f	f	2024-03-04 16:18:40.822	test who	\N	\N	\N	\N
46	0	767d91ec-0979-4a5f-a470-1d1898c83258	\N	\N	6746256a-3cfb-4b04-8250-29805a1fa4a4	\N	\N	Sys-AF4	SYS-AF4	System structure only	APPROVED	t	t	2024-03-04 16:18:41.019	test who	\N	\N	\N	\N
41	1	767d91ec-0979-4a5f-a470-1d1898c83258	\N	\N	6746256a-3cfb-4b04-8250-29805a1fa4a4	\N	\N	Sys-AF4	SYS-AF4	System structure only	APPROVED	f	f	2024-03-04 16:18:40.822	test who	\N	\N	\N	\N
47	0	c9a66287-5c79-4bb2-98f3-60cb9eb902b9	\N	\N	d2bac9ef-4a4a-43c4-855a-ba22d1005798	\N	\N	Sys-AF5	SYS-AF5	System structure only	APPROVED	t	t	2024-03-04 16:18:41.022	test who	\N	\N	\N	\N
42	1	c9a66287-5c79-4bb2-98f3-60cb9eb902b9	\N	\N	d2bac9ef-4a4a-43c4-855a-ba22d1005798	\N	\N	Sys-AF5	SYS-AF5	System structure only	APPROVED	f	f	2024-03-04 16:18:40.822	test who	\N	\N	\N	\N
\.


--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 212
-- Name: name_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.name_id_seq', 47, true);


--
-- TOC entry 2324 (class 0 OID 16404)
-- Dependencies: 191
-- Data for Name: namepart; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.namepart (id, version, nameparttype, uuid) FROM stdin;
\.


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 192
-- Name: namepart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.namepart_id_seq', 1, false);


--
-- TOC entry 2326 (class 0 OID 16412)
-- Dependencies: 193
-- Data for Name: namepartrevision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.namepartrevision (id, version, deleted, description, mnemonic, mnemoniceqclass, name, processdate, processorcomment, requestdate, requestercomment, status, namepart_id, parent_id, processedby_id, requestedby_id) FROM stdin;
\.


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 194
-- Name: namepartrevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.namepartrevision_id_seq', 1, false);


--
-- TOC entry 2334 (class 0 OID 16506)
-- Dependencies: 201
-- Data for Name: subsystem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.subsystem (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	0ccfe162-7bf2-46f0-b670-910e9c7a0f99	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AA1	AA1	1	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.858	test who	\N
2	0	4fe32787-9e4e-48df-b2d4-413b6310ddd8	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AA2	AA2	1	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.858	test who	\N
3	0	921fd974-b02e-4535-9fa4-832e1f413a2f	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AA3	AA3	1	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.858	test who	\N
4	0	d8c8a289-3b50-4d45-bff0-0ba6933235fd	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AA4	AA4	1	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.858	test who	\N
5	0	ca0d26d2-fcf0-4844-9509-9c60835857e1	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AA5	AA5	1	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.858	test who	\N
11	0	ed15e5ff-0641-4185-9fb0-f43702a14350	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB1	AB1	2	some other description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.081	test who	\N
6	1	ed15e5ff-0641-4185-9fb0-f43702a14350	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB1	AB1	2	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:39.95	test who	\N
12	0	8e158f4b-0ea5-4ca9-87ae-def88d72a647	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB2	AB2	2	some other description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.081	test who	\N
7	1	8e158f4b-0ea5-4ca9-87ae-def88d72a647	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB2	AB2	2	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:39.95	test who	\N
13	0	b8e9bbfc-e3c4-4eef-883e-0e2d0709ec03	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB3	AB3	2	some other description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.081	test who	\N
8	1	b8e9bbfc-e3c4-4eef-883e-0e2d0709ec03	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB3	AB3	2	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:39.95	test who	\N
14	0	d13ea971-d722-45ea-bbab-96c5e221a344	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB4	AB4	2	some other description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.081	test who	\N
9	1	d13ea971-d722-45ea-bbab-96c5e221a344	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB4	AB4	2	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:39.95	test who	\N
15	0	63d701ba-555b-41fd-9810-9d87454aa1fa	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB5	AB5	2	some other description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.081	test who	\N
10	1	63d701ba-555b-41fd-9810-9d87454aa1fa	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AB5	AB5	2	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:39.95	test who	\N
21	0	9659c6e8-7d37-47d8-aaf6-eeb7ada222d1	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC1	AC1	3	description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.254	test who	\N
16	1	9659c6e8-7d37-47d8-aaf6-eeb7ada222d1	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC1	AC1	3	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.157	test who	\N
22	0	0db3c127-d8f7-48ea-8af1-6433928f928b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC2	AC2	3	description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.254	test who	\N
17	1	0db3c127-d8f7-48ea-8af1-6433928f928b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC2	AC2	3	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.157	test who	\N
23	0	5d1164af-e155-433e-a8e6-1a60cbc315b8	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC3	AC3	3	description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.254	test who	\N
18	1	5d1164af-e155-433e-a8e6-1a60cbc315b8	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC3	AC3	3	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.157	test who	\N
24	0	2542b027-513b-40bb-9be8-74fcb400459e	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC4	AC4	3	description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.254	test who	\N
19	1	2542b027-513b-40bb-9be8-74fcb400459e	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC4	AC4	3	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.157	test who	\N
25	0	e1e4b087-c3d5-409f-9a5f-a9e9f84962fb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC5	AC5	3	description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.254	test who	\N
20	1	e1e4b087-c3d5-409f-9a5f-a9e9f84962fb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AC5	AC5	3	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.157	test who	\N
26	1	2cb15615-72a4-48af-aa88-57fe20caaebe	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD1	AD1	4	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.361	test who	\N
27	1	1c764e48-dbda-4d6a-baf0-013be384dce0	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD2	AD2	4	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.361	test who	\N
28	1	700ff3d0-ec0f-41b0-9d0d-dfd4c8091b9d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD3	AD3	4	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.361	test who	\N
29	1	ab167f84-7c67-4364-a6b7-42405d98cb69	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD4	AD4	4	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.361	test who	\N
30	1	8f76876a-504e-4402-bd60-6bb4c60d4d29	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD5	AD5	4	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.361	test who	\N
36	0	2cb15615-72a4-48af-aa88-57fe20caaebe	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD1	AD1	4	some other description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.478	test who	\N
31	1	2cb15615-72a4-48af-aa88-57fe20caaebe	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD1	AD1	4	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.43	test who	\N
37	0	1c764e48-dbda-4d6a-baf0-013be384dce0	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD2	AD2	4	some other description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.478	test who	\N
32	1	1c764e48-dbda-4d6a-baf0-013be384dce0	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD2	AD2	4	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.43	test who	\N
38	0	700ff3d0-ec0f-41b0-9d0d-dfd4c8091b9d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD3	AD3	4	some other description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.478	test who	\N
33	1	700ff3d0-ec0f-41b0-9d0d-dfd4c8091b9d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD3	AD3	4	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.43	test who	\N
39	0	ab167f84-7c67-4364-a6b7-42405d98cb69	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD4	AD4	4	some other description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.478	test who	\N
34	1	ab167f84-7c67-4364-a6b7-42405d98cb69	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD4	AD4	4	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.43	test who	\N
40	0	8f76876a-504e-4402-bd60-6bb4c60d4d29	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD5	AD5	4	some other description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:40.478	test who	\N
35	1	8f76876a-504e-4402-bd60-6bb4c60d4d29	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AD5	AD5	4	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.43	test who	\N
41	1	b8485a11-1c59-4ef5-876b-16a49512d457	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE1	AE1	5	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.556	test who	\N
42	1	30fbb919-376d-40d2-9bd2-3c9fd7b952da	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE2	AE2	5	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.556	test who	\N
43	1	89feee5b-5003-4dc6-a693-d0e2dd92112b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE3	AE3	5	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.556	test who	\N
44	1	f4eaf55f-01bb-4a37-8d61-56f576444dd7	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE4	AE4	5	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.556	test who	\N
45	1	87932c6c-909e-4165-a75d-2bc10db24415	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE5	AE5	5	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.556	test who	\N
46	1	b8485a11-1c59-4ef5-876b-16a49512d457	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE1	AE1	5	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.633	test who	\N
47	1	30fbb919-376d-40d2-9bd2-3c9fd7b952da	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE2	AE2	5	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.633	test who	\N
48	1	89feee5b-5003-4dc6-a693-d0e2dd92112b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE3	AE3	5	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.633	test who	\N
49	1	f4eaf55f-01bb-4a37-8d61-56f576444dd7	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE4	AE4	5	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.633	test who	\N
50	1	87932c6c-909e-4165-a75d-2bc10db24415	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE5	AE5	5	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.633	test who	\N
56	0	b8485a11-1c59-4ef5-876b-16a49512d457	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE1	AE1	5	yet another description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.768	test who	\N
51	1	b8485a11-1c59-4ef5-876b-16a49512d457	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE1	AE1	5	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.698	test who	\N
57	0	30fbb919-376d-40d2-9bd2-3c9fd7b952da	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE2	AE2	5	yet another description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.768	test who	\N
52	1	30fbb919-376d-40d2-9bd2-3c9fd7b952da	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE2	AE2	5	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.698	test who	\N
58	0	89feee5b-5003-4dc6-a693-d0e2dd92112b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE3	AE3	5	yet another description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.768	test who	\N
53	1	89feee5b-5003-4dc6-a693-d0e2dd92112b	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE3	AE3	5	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.698	test who	\N
59	0	f4eaf55f-01bb-4a37-8d61-56f576444dd7	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE4	AE4	5	yet another description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.768	test who	\N
54	1	f4eaf55f-01bb-4a37-8d61-56f576444dd7	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE4	AE4	5	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.698	test who	\N
60	0	87932c6c-909e-4165-a75d-2bc10db24415	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE5	AE5	5	yet another description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:40.768	test who	\N
55	1	87932c6c-909e-4165-a75d-2bc10db24415	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AE5	AE5	5	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.698	test who	\N
61	1	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF1	AF1	6	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.822	test who	\N
62	1	0eb86e59-0afc-4a49-a554-4a449d1cec3d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF2	AF2	6	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.822	test who	\N
63	1	efe208d0-4e11-4c28-b84c-f0d589448ccb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF3	AF3	6	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.822	test who	\N
64	1	6746256a-3cfb-4b04-8250-29805a1fa4a4	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF4	AF4	6	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.822	test who	\N
65	1	d2bac9ef-4a4a-43c4-855a-ba22d1005798	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF5	AF5	6	description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.822	test who	\N
66	1	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF1	AF1	6	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.872	test who	\N
67	1	0eb86e59-0afc-4a49-a554-4a449d1cec3d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF2	AF2	6	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.872	test who	\N
68	1	efe208d0-4e11-4c28-b84c-f0d589448ccb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF3	AF3	6	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.872	test who	\N
69	1	6746256a-3cfb-4b04-8250-29805a1fa4a4	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF4	AF4	6	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.872	test who	\N
71	1	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF1	AF1	6	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.921	test who	\N
72	1	0eb86e59-0afc-4a49-a554-4a449d1cec3d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF2	AF2	6	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.921	test who	\N
73	1	efe208d0-4e11-4c28-b84c-f0d589448ccb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF3	AF3	6	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.921	test who	\N
74	1	6746256a-3cfb-4b04-8250-29805a1fa4a4	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF4	AF4	6	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.921	test who	\N
75	1	d2bac9ef-4a4a-43c4-855a-ba22d1005798	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF5	AF5	6	more description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.921	test who	\N
70	1	d2bac9ef-4a4a-43c4-855a-ba22d1005798	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF5	AF5	6	some other description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.872	test who	\N
81	0	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF1	AF1	6	yet another description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:41.006	test who	\N
76	1	9ce444c8-4dd9-4456-84fa-ff3f60a6dbf3	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF1	AF1	6	yet another description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.959	test who	\N
82	0	0eb86e59-0afc-4a49-a554-4a449d1cec3d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF2	AF2	6	yet another description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:41.006	test who	\N
77	1	0eb86e59-0afc-4a49-a554-4a449d1cec3d	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF2	AF2	6	yet another description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.959	test who	\N
83	0	efe208d0-4e11-4c28-b84c-f0d589448ccb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF3	AF3	6	yet another description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:41.006	test who	\N
78	1	efe208d0-4e11-4c28-b84c-f0d589448ccb	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF3	AF3	6	yet another description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.959	test who	\N
84	0	6746256a-3cfb-4b04-8250-29805a1fa4a4	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF4	AF4	6	yet another description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:41.006	test who	\N
79	1	6746256a-3cfb-4b04-8250-29805a1fa4a4	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF4	AF4	6	yet another description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.959	test who	\N
85	0	d2bac9ef-4a4a-43c4-855a-ba22d1005798	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF5	AF5	6	yet another description	APPROVED	t	t	\N	\N	\N	2024-03-04 16:18:41.006	test who	\N
80	1	d2bac9ef-4a4a-43c4-855a-ba22d1005798	25a231ac-cc79-43f0-a8aa-00d34b6262a8	AF5	AF5	6	yet another description	APPROVED	f	f	\N	\N	\N	2024-03-04 16:18:40.959	test who	\N
\.


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 208
-- Name: subsystem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.subsystem_id_seq', 85, true);


--
-- TOC entry 2333 (class 0 OID 16500)
-- Dependencies: 200
-- Data for Name: system; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.system (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	25a231ac-cc79-43f0-a8aa-00d34b6262a8	b3f5b914-9607-4df8-a701-553f4d238e1e	Sys	SYS	\N	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:39.52	test who	\N
\.


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 207
-- Name: system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_id_seq', 1, true);


--
-- TOC entry 2332 (class 0 OID 16494)
-- Dependencies: 199
-- Data for Name: systemgroup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.systemgroup (id, version, uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	b3f5b914-9607-4df8-a701-553f4d238e1e	Sg	SG	\N	description	APPROVED	t	f	\N	\N	\N	2024-03-04 16:18:38.976	test who	\N
\.


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 206
-- Name: systemgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.systemgroup_id_seq', 1, true);


--
-- TOC entry 2328 (class 0 OID 16420)
-- Dependencies: 195
-- Data for Name: useraccount; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.useraccount (id, version, role, username) FROM stdin;
\.


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 196
-- Name: useraccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.useraccount_id_seq', 1, false);


--
-- TOC entry 2116 (class 2606 OID 16435)
-- Name: appinfo appinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appinfo
    ADD CONSTRAINT appinfo_pkey PRIMARY KEY (id);


--
-- TOC entry 2118 (class 2606 OID 16437)
-- Name: device device_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);


--
-- TOC entry 2169 (class 2606 OID 16616)
-- Name: devicegroup devicegroup_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicegroup
    ADD CONSTRAINT devicegroup_pk PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 16439)
-- Name: devicerevision devicerevision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT devicerevision_pkey PRIMARY KEY (id);


--
-- TOC entry 2178 (class 2606 OID 16618)
-- Name: devicetype devicetype_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicetype
    ADD CONSTRAINT devicetype_pk PRIMARY KEY (id);


--
-- TOC entry 2160 (class 2606 OID 16614)
-- Name: discipline discipline_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discipline
    ADD CONSTRAINT discipline_pk PRIMARY KEY (id);


--
-- TOC entry 2187 (class 2606 OID 16620)
-- Name: name name_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.name
    ADD CONSTRAINT name_pk PRIMARY KEY (id);


--
-- TOC entry 2122 (class 2606 OID 16441)
-- Name: namepart namepart_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepart
    ADD CONSTRAINT namepart_pkey PRIMARY KEY (id);


--
-- TOC entry 2124 (class 2606 OID 16443)
-- Name: namepartrevision namepartrevision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT namepartrevision_pkey PRIMARY KEY (id);


--
-- TOC entry 2152 (class 2606 OID 16612)
-- Name: subsystem subsystem_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subsystem
    ADD CONSTRAINT subsystem_pk PRIMARY KEY (id);


--
-- TOC entry 2143 (class 2606 OID 16610)
-- Name: system system_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system
    ADD CONSTRAINT system_pk PRIMARY KEY (id);


--
-- TOC entry 2134 (class 2606 OID 16608)
-- Name: systemgroup systemgroup_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.systemgroup
    ADD CONSTRAINT systemgroup_pk PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 16445)
-- Name: useraccount useraccount_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.useraccount
    ADD CONSTRAINT useraccount_pkey PRIMARY KEY (id);


--
-- TOC entry 2163 (class 1259 OID 16563)
-- Name: devicegroup_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_deleted_idx ON public.devicegroup USING btree (deleted);


--
-- TOC entry 2164 (class 1259 OID 16558)
-- Name: devicegroup_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_id_idx ON public.devicegroup USING btree (id);


--
-- TOC entry 2165 (class 1259 OID 16583)
-- Name: devicegroup_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_latest_idx ON public.devicegroup USING btree (latest);


--
-- TOC entry 2166 (class 1259 OID 16561)
-- Name: devicegroup_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_mnemonic_idx ON public.devicegroup USING btree (mnemonic);


--
-- TOC entry 2167 (class 1259 OID 16560)
-- Name: devicegroup_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_parent_uuid_idx ON public.devicegroup USING btree (parent_uuid);


--
-- TOC entry 2170 (class 1259 OID 16562)
-- Name: devicegroup_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_status_idx ON public.devicegroup USING btree (status);


--
-- TOC entry 2171 (class 1259 OID 16559)
-- Name: devicegroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_uuid_idx ON public.devicegroup USING btree (uuid);


--
-- TOC entry 2172 (class 1259 OID 16569)
-- Name: devicetype_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_deleted_idx ON public.devicetype USING btree (deleted);


--
-- TOC entry 2173 (class 1259 OID 16564)
-- Name: devicetype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_id_idx ON public.devicetype USING btree (id);


--
-- TOC entry 2174 (class 1259 OID 16584)
-- Name: devicetype_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_latest_idx ON public.devicetype USING btree (latest);


--
-- TOC entry 2175 (class 1259 OID 16567)
-- Name: devicetype_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_mnemonic_idx ON public.devicetype USING btree (mnemonic);


--
-- TOC entry 2176 (class 1259 OID 16566)
-- Name: devicetype_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_parent_uuid_idx ON public.devicetype USING btree (parent_uuid);


--
-- TOC entry 2179 (class 1259 OID 16568)
-- Name: devicetype_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_status_idx ON public.devicetype USING btree (status);


--
-- TOC entry 2180 (class 1259 OID 16565)
-- Name: devicetype_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_uuid_idx ON public.devicetype USING btree (uuid);


--
-- TOC entry 2155 (class 1259 OID 16557)
-- Name: discipline_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_deleted_idx ON public.discipline USING btree (deleted);


--
-- TOC entry 2156 (class 1259 OID 16553)
-- Name: discipline_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_id_idx ON public.discipline USING btree (id);


--
-- TOC entry 2157 (class 1259 OID 16582)
-- Name: discipline_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_latest_idx ON public.discipline USING btree (latest);


--
-- TOC entry 2158 (class 1259 OID 16555)
-- Name: discipline_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_mnemonic_idx ON public.discipline USING btree (mnemonic);


--
-- TOC entry 2161 (class 1259 OID 16556)
-- Name: discipline_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_status_idx ON public.discipline USING btree (status);


--
-- TOC entry 2162 (class 1259 OID 16554)
-- Name: discipline_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_uuid_idx ON public.discipline USING btree (uuid);


--
-- TOC entry 2181 (class 1259 OID 16576)
-- Name: name_convention_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_convention_name_idx ON public.name USING btree (convention_name);


--
-- TOC entry 2182 (class 1259 OID 16578)
-- Name: name_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_deleted_idx ON public.name USING btree (deleted);


--
-- TOC entry 2183 (class 1259 OID 16575)
-- Name: name_devicetype_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_devicetype_uuid_idx ON public.name USING btree (devicetype_uuid);


--
-- TOC entry 2184 (class 1259 OID 16571)
-- Name: name_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_id_idx ON public.name USING btree (id);


--
-- TOC entry 2185 (class 1259 OID 16585)
-- Name: name_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_latest_idx ON public.name USING btree (latest);


--
-- TOC entry 2188 (class 1259 OID 16577)
-- Name: name_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_status_idx ON public.name USING btree (status);


--
-- TOC entry 2189 (class 1259 OID 16574)
-- Name: name_subsystem_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_subsystem_uuid_idx ON public.name USING btree (subsystem_uuid);


--
-- TOC entry 2190 (class 1259 OID 16573)
-- Name: name_system_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_system_uuid_idx ON public.name USING btree (system_uuid);


--
-- TOC entry 2191 (class 1259 OID 16572)
-- Name: name_systemgroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_systemgroup_uuid_idx ON public.name USING btree (systemgroup_uuid);


--
-- TOC entry 2192 (class 1259 OID 16570)
-- Name: name_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_uuid_idx ON public.name USING btree (uuid);


--
-- TOC entry 2146 (class 1259 OID 16552)
-- Name: subsystem_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_deleted_idx ON public.subsystem USING btree (deleted);


--
-- TOC entry 2147 (class 1259 OID 16547)
-- Name: subsystem_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_id_idx ON public.subsystem USING btree (id);


--
-- TOC entry 2148 (class 1259 OID 16581)
-- Name: subsystem_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_latest_idx ON public.subsystem USING btree (latest);


--
-- TOC entry 2149 (class 1259 OID 16550)
-- Name: subsystem_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_mnemonic_idx ON public.subsystem USING btree (mnemonic);


--
-- TOC entry 2150 (class 1259 OID 16549)
-- Name: subsystem_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_parent_uuid_idx ON public.subsystem USING btree (parent_uuid);


--
-- TOC entry 2153 (class 1259 OID 16551)
-- Name: subsystem_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_status_idx ON public.subsystem USING btree (status);


--
-- TOC entry 2154 (class 1259 OID 16548)
-- Name: subsystem_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_uuid_idx ON public.subsystem USING btree (uuid);


--
-- TOC entry 2137 (class 1259 OID 16546)
-- Name: system_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_deleted_idx ON public.system USING btree (deleted);


--
-- TOC entry 2138 (class 1259 OID 16541)
-- Name: system_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_id_idx ON public.system USING btree (id);


--
-- TOC entry 2139 (class 1259 OID 16580)
-- Name: system_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_latest_idx ON public.system USING btree (latest);


--
-- TOC entry 2140 (class 1259 OID 16544)
-- Name: system_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_mnemonic_idx ON public.system USING btree (mnemonic);


--
-- TOC entry 2141 (class 1259 OID 16543)
-- Name: system_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_parent_uuid_idx ON public.system USING btree (parent_uuid);


--
-- TOC entry 2144 (class 1259 OID 16545)
-- Name: system_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_status_idx ON public.system USING btree (status);


--
-- TOC entry 2145 (class 1259 OID 16542)
-- Name: system_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_uuid_idx ON public.system USING btree (uuid);


--
-- TOC entry 2129 (class 1259 OID 16540)
-- Name: systemgroup_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_deleted_idx ON public.systemgroup USING btree (deleted);


--
-- TOC entry 2130 (class 1259 OID 16536)
-- Name: systemgroup_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_id_idx ON public.systemgroup USING btree (id);


--
-- TOC entry 2131 (class 1259 OID 16579)
-- Name: systemgroup_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_latest_idx ON public.systemgroup USING btree (latest);


--
-- TOC entry 2132 (class 1259 OID 16538)
-- Name: systemgroup_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_mnemonic_idx ON public.systemgroup USING btree (mnemonic);


--
-- TOC entry 2135 (class 1259 OID 16539)
-- Name: systemgroup_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_status_idx ON public.systemgroup USING btree (status);


--
-- TOC entry 2136 (class 1259 OID 16537)
-- Name: systemgroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_uuid_idx ON public.systemgroup USING btree (uuid);


--
-- TOC entry 2197 (class 2606 OID 16446)
-- Name: namepartrevision fk_3f26vetemhujfdm9q74ecr2u5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_3f26vetemhujfdm9q74ecr2u5 FOREIGN KEY (namepart_id) REFERENCES public.namepart(id);


--
-- TOC entry 2193 (class 2606 OID 16451)
-- Name: devicerevision fk_4ucnoos7kd8s1gaqbpwm1xptq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_4ucnoos7kd8s1gaqbpwm1xptq FOREIGN KEY (requestedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2198 (class 2606 OID 16456)
-- Name: namepartrevision fk_9vomfk9x1jow27ifx6xc62c5x; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_9vomfk9x1jow27ifx6xc62c5x FOREIGN KEY (processedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2199 (class 2606 OID 16461)
-- Name: namepartrevision fk_9xs5oy86lf0j8ukpjokjipeke; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_9xs5oy86lf0j8ukpjokjipeke FOREIGN KEY (requestedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2194 (class 2606 OID 16466)
-- Name: devicerevision fk_d3ocbsb4tl4ttnusn98khq148; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_d3ocbsb4tl4ttnusn98khq148 FOREIGN KEY (devicetype_id) REFERENCES public.namepart(id);


--
-- TOC entry 2195 (class 2606 OID 16471)
-- Name: devicerevision fk_l7kklb4mxixjs27nsso6shone; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_l7kklb4mxixjs27nsso6shone FOREIGN KEY (section_id) REFERENCES public.namepart(id);


--
-- TOC entry 2196 (class 2606 OID 16476)
-- Name: devicerevision fk_l9r1givkfaiol5or2lnr324xp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_l9r1givkfaiol5or2lnr324xp FOREIGN KEY (device_id) REFERENCES public.device(id);


--
-- TOC entry 2200 (class 2606 OID 16481)
-- Name: namepartrevision fk_lufxqy46l9eiq55d445rbukag; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_lufxqy46l9eiq55d445rbukag FOREIGN KEY (parent_id) REFERENCES public.namepart(id);


-- Completed on 2024-03-04 17:19:26 CET

--
-- PostgreSQL database dump complete
--


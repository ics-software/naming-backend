--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.24

-- Started on 2024-03-04 17:17:29 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 228 (class 1255 OID 16623)
-- Name: get_instance_index(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_instance_index(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    len int;
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            len = length(mnemonic_path);
            pos = strpos(mnemonic_path, '-');
            mnemonic_path = reverse(mnemonic_path);
            RETURN substr(mnemonic_path, len - pos + 2);
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;


--
-- TOC entry 214 (class 1255 OID 16622)
-- Name: get_mnemonic_path_device_structure(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_device_structure(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    pos int;
    mnemonic_path text;
    nbr_delimiters int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        mnemonic_path = substr(convention_name, pos+1);
        nbr_delimiters = array_length(string_to_array(mnemonic_path, '-'), 1) - 1;
        IF nbr_delimiters = 2 then
            mnemonic_path = reverse(mnemonic_path);
            mnemonic_path = substr(mnemonic_path, strpos(mnemonic_path, '-')+1);
            RETURN reverse(mnemonic_path);
        ELSIF nbr_delimiters = 1 then
            return mnemonic_path;
        ELSE
            RETURN null;
        END IF;
    END IF;
    RETURN null;
END;
$$;


--
-- TOC entry 215 (class 1255 OID 16626)
-- Name: get_mnemonic_path_devicegroup(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_devicegroup(devicegroup_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid into discipline_uuid from devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null then
        return discipline_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 231 (class 1255 OID 16627)
-- Name: get_mnemonic_path_devicetype(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_devicetype(devicetype_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    devicetype_mnemonic text;
    devicegroup_uuid text;
    discipline_uuid text;
    discipline_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into devicegroup_uuid, devicetype_mnemonic from devicetype where uuid = devicetype_uuid and latest = true;
    select parent_uuid, mnemonic into discipline_uuid from devicegroup where uuid = devicegroup_uuid and latest = true;
    select mnemonic into discipline_mnemonic from discipline where uuid = discipline_uuid and latest = true;

    if discipline_mnemonic is not null and devicetype_mnemonic is not null then
        return concat(discipline_mnemonic, '-', devicetype_mnemonic);
    elsif devicetype_mnemonic is not null then
        return devicetype_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 230 (class 1255 OID 16625)
-- Name: get_mnemonic_path_subsystem(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_subsystem(subsystem_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    subsystem_mnemonic text;
    system_uuid text;
    system_mnemonic text;
BEGIN
    select parent_uuid, mnemonic into system_uuid, subsystem_mnemonic from subsystem where uuid = subsystem_uuid and latest = true;
    select mnemonic into system_mnemonic from "system" where uuid = system_uuid and latest = true;

    if system_mnemonic is not null and subsystem_mnemonic is not null then
        return concat(system_mnemonic, '-', subsystem_mnemonic);
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 229 (class 1255 OID 16624)
-- Name: get_mnemonic_path_system(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_system(system_uuid text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    system_mnemonic text;
BEGIN
    select mnemonic into system_mnemonic from "system" where uuid = system_uuid and latest = true;

    if system_mnemonic is not null then
        return system_mnemonic;
    else
        return null;
    end if;
END;
$$;


--
-- TOC entry 213 (class 1255 OID 16621)
-- Name: get_mnemonic_path_system_structure(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.get_mnemonic_path_system_structure(convention_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    pos int;
BEGIN
    pos = strpos(convention_name, ':');
    IF pos > 0  THEN
        RETURN substr(convention_name, 1, pos-1);
    END IF;
    RETURN convention_name;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16386)
-- Name: appinfo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.appinfo (
    id bigint NOT NULL,
    version integer,
    schemaversion integer NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 16389)
-- Name: appinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.appinfo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 186
-- Name: appinfo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.appinfo_id_seq OWNED BY public.appinfo.id;


--
-- TOC entry 187 (class 1259 OID 16391)
-- Name: device; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.device (
    id bigint NOT NULL,
    version integer,
    uuid character varying(255)
);


--
-- TOC entry 188 (class 1259 OID 16394)
-- Name: device_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 188
-- Name: device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.device_id_seq OWNED BY public.device.id;


--
-- TOC entry 203 (class 1259 OID 16518)
-- Name: devicegroup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicegroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 210 (class 1259 OID 16594)
-- Name: devicegroup_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicegroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 210
-- Name: devicegroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicegroup_id_seq OWNED BY public.devicegroup.id;


--
-- TOC entry 189 (class 1259 OID 16396)
-- Name: devicerevision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicerevision (
    id bigint NOT NULL,
    version integer,
    additionalinfo character varying(255),
    conventionname character varying(255),
    conventionnameeqclass character varying(255),
    deleted boolean NOT NULL,
    instanceindex character varying(255),
    requestdate timestamp without time zone,
    device_id bigint,
    devicetype_id bigint,
    requestedby_id bigint,
    section_id bigint,
    processorcomment character varying(255)
);


--
-- TOC entry 190 (class 1259 OID 16402)
-- Name: devicerevision_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicerevision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 190
-- Name: devicerevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicerevision_id_seq OWNED BY public.devicerevision.id;


--
-- TOC entry 204 (class 1259 OID 16524)
-- Name: devicetype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.devicetype (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 211 (class 1259 OID 16596)
-- Name: devicetype_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.devicetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 211
-- Name: devicetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.devicetype_id_seq OWNED BY public.devicetype.id;


--
-- TOC entry 202 (class 1259 OID 16512)
-- Name: discipline; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.discipline (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 209 (class 1259 OID 16592)
-- Name: discipline_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.discipline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 209
-- Name: discipline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.discipline_id_seq OWNED BY public.discipline.id;


--
-- TOC entry 205 (class 1259 OID 16530)
-- Name: name; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.name (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    systemgroup_uuid text,
    system_uuid text,
    subsystem_uuid text,
    devicetype_uuid text,
    instance_index text,
    convention_name text,
    convention_name_equivalence text,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 212 (class 1259 OID 16598)
-- Name: name_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.name_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 212
-- Name: name_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.name_id_seq OWNED BY public.name.id;


--
-- TOC entry 191 (class 1259 OID 16404)
-- Name: namepart; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.namepart (
    id bigint NOT NULL,
    version integer,
    nameparttype character varying(255),
    uuid character varying(255)
);


--
-- TOC entry 192 (class 1259 OID 16410)
-- Name: namepart_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.namepart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 192
-- Name: namepart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.namepart_id_seq OWNED BY public.namepart.id;


--
-- TOC entry 193 (class 1259 OID 16412)
-- Name: namepartrevision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.namepartrevision (
    id bigint NOT NULL,
    version integer,
    deleted boolean NOT NULL,
    description character varying(255),
    mnemonic character varying(255),
    mnemoniceqclass character varying(255),
    name character varying(255),
    processdate timestamp without time zone,
    processorcomment character varying(255),
    requestdate timestamp without time zone,
    requestercomment character varying(255),
    status character varying(255),
    namepart_id bigint,
    parent_id bigint,
    processedby_id bigint,
    requestedby_id bigint
);


--
-- TOC entry 194 (class 1259 OID 16418)
-- Name: namepartrevision_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.namepartrevision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 194
-- Name: namepartrevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.namepartrevision_id_seq OWNED BY public.namepartrevision.id;


--
-- TOC entry 201 (class 1259 OID 16506)
-- Name: subsystem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subsystem (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 208 (class 1259 OID 16590)
-- Name: subsystem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subsystem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 208
-- Name: subsystem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.subsystem_id_seq OWNED BY public.subsystem.id;


--
-- TOC entry 200 (class 1259 OID 16500)
-- Name: system; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    parent_uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 207 (class 1259 OID 16588)
-- Name: system_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 207
-- Name: system_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_id_seq OWNED BY public.system.id;


--
-- TOC entry 199 (class 1259 OID 16494)
-- Name: systemgroup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.systemgroup (
    id bigint NOT NULL,
    version integer,
    uuid text NOT NULL,
    mnemonic text,
    mnemonic_equivalence text,
    ordering integer,
    description text,
    status text,
    latest boolean NOT NULL,
    deleted boolean NOT NULL,
    requested timestamp without time zone,
    requested_by text,
    requested_comment text,
    processed timestamp without time zone,
    processed_by text,
    processed_comment text
);


--
-- TOC entry 206 (class 1259 OID 16586)
-- Name: systemgroup_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.systemgroup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 206
-- Name: systemgroup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.systemgroup_id_seq OWNED BY public.systemgroup.id;


--
-- TOC entry 195 (class 1259 OID 16420)
-- Name: useraccount; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.useraccount (
    id bigint NOT NULL,
    version integer,
    role character varying(255),
    username character varying(255)
);


--
-- TOC entry 196 (class 1259 OID 16426)
-- Name: useraccount_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.useraccount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 196
-- Name: useraccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.useraccount_id_seq OWNED BY public.useraccount.id;


--
-- TOC entry 2101 (class 2604 OID 16428)
-- Name: appinfo id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appinfo ALTER COLUMN id SET DEFAULT nextval('public.appinfo_id_seq'::regclass);


--
-- TOC entry 2102 (class 2604 OID 16429)
-- Name: device id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.device ALTER COLUMN id SET DEFAULT nextval('public.device_id_seq'::regclass);


--
-- TOC entry 2112 (class 2604 OID 16604)
-- Name: devicegroup id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicegroup ALTER COLUMN id SET DEFAULT nextval('public.devicegroup_id_seq'::regclass);


--
-- TOC entry 2103 (class 2604 OID 16430)
-- Name: devicerevision id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision ALTER COLUMN id SET DEFAULT nextval('public.devicerevision_id_seq'::regclass);


--
-- TOC entry 2113 (class 2604 OID 16605)
-- Name: devicetype id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicetype ALTER COLUMN id SET DEFAULT nextval('public.devicetype_id_seq'::regclass);


--
-- TOC entry 2111 (class 2604 OID 16603)
-- Name: discipline id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discipline ALTER COLUMN id SET DEFAULT nextval('public.discipline_id_seq'::regclass);


--
-- TOC entry 2114 (class 2604 OID 16606)
-- Name: name id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.name ALTER COLUMN id SET DEFAULT nextval('public.name_id_seq'::regclass);


--
-- TOC entry 2104 (class 2604 OID 16431)
-- Name: namepart id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepart ALTER COLUMN id SET DEFAULT nextval('public.namepart_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 16432)
-- Name: namepartrevision id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision ALTER COLUMN id SET DEFAULT nextval('public.namepartrevision_id_seq'::regclass);


--
-- TOC entry 2110 (class 2604 OID 16602)
-- Name: subsystem id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subsystem ALTER COLUMN id SET DEFAULT nextval('public.subsystem_id_seq'::regclass);


--
-- TOC entry 2109 (class 2604 OID 16601)
-- Name: system id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system ALTER COLUMN id SET DEFAULT nextval('public.system_id_seq'::regclass);


--
-- TOC entry 2108 (class 2604 OID 16600)
-- Name: systemgroup id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.systemgroup ALTER COLUMN id SET DEFAULT nextval('public.systemgroup_id_seq'::regclass);


--
-- TOC entry 2106 (class 2604 OID 16433)
-- Name: useraccount id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.useraccount ALTER COLUMN id SET DEFAULT nextval('public.useraccount_id_seq'::regclass);


--
-- TOC entry 2318 (class 0 OID 16386)
-- Dependencies: 185
-- Data for Name: appinfo; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.appinfo (id, version, schemaversion) FROM stdin;
1	1	1
\.


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 186
-- Name: appinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.appinfo_id_seq', 1, true);


--
-- TOC entry 2320 (class 0 OID 16391)
-- Dependencies: 187
-- Data for Name: device; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.device (id, version, uuid) FROM stdin;
\.


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 188
-- Name: device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.device_id_seq', 1, false);


--
-- TOC entry 2336 (class 0 OID 16518)
-- Dependencies: 203
-- Data for Name: devicegroup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicegroup (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	d64e3a84-1224-4352-af0a-e7d2b512aa94	5c27876e-8cb8-4576-b918-c984748cf996	\N	\N	\N	Control	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.173	test who	\N
\.


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 210
-- Name: devicegroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicegroup_id_seq', 1, true);


--
-- TOC entry 2322 (class 0 OID 16396)
-- Dependencies: 189
-- Data for Name: devicerevision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicerevision (id, version, additionalinfo, conventionname, conventionnameeqclass, deleted, instanceindex, requestdate, device_id, devicetype_id, requestedby_id, section_id, processorcomment) FROM stdin;
\.


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 190
-- Name: devicerevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicerevision_id_seq', 1, false);


--
-- TOC entry 2337 (class 0 OID 16524)
-- Dependencies: 204
-- Data for Name: devicetype; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.devicetype (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	d64e3a84-1224-4352-af0a-e7d2b512aa94	FS	FS	\N	Flow Switch	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.264	test who	\N
2	0	f91e2bef-c50c-45b9-8b51-051872993313	d64e3a84-1224-4352-af0a-e7d2b512aa94	RFA	RFA	\N	RF Antenna	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.264	test who	\N
3	0	fe0bce90-46fc-4d78-b47b-6679e29235bc	d64e3a84-1224-4352-af0a-e7d2b512aa94	TT	TT	\N	Temperature Transmitter	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.264	test who	\N
\.


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 211
-- Name: devicetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.devicetype_id_seq', 3, true);


--
-- TOC entry 2335 (class 0 OID 16512)
-- Dependencies: 202
-- Data for Name: discipline; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.discipline (id, version, uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	5c27876e-8cb8-4576-b918-c984748cf996	EMR	EMR	\N	Electromagnetic Resonators	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.124	test who	\N
\.


--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 209
-- Name: discipline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.discipline_id_seq', 1, true);


--
-- TOC entry 2338 (class 0 OID 16530)
-- Dependencies: 205
-- Data for Name: name; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.name (id, version, uuid, systemgroup_uuid, system_uuid, subsystem_uuid, devicetype_uuid, instance_index, convention_name, convention_name_equivalence, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	b6177d1f-fdc3-47f0-b35e-3e61ea8760be	9aae66a8-ac73-4c0c-b252-430249882403	\N	\N	\N	\N	Acc	ACC	System structure only	APPROVED	t	f	2024-03-04 16:14:50.452	test who	\N	\N	\N	\N
2	0	bce1e479-115e-4aa4-a641-e634af3a4f71	\N	e5c2b06e-7f3e-4adf-9c2d-c20c0f873ba9	\N	\N	\N	RFQ	RFQ	System structure only	APPROVED	t	f	2024-03-04 16:14:50.871	test who	\N	\N	\N	\N
3	0	e317123a-a00f-4009-873e-17212267ed20	\N	\N	de2b46ae-2459-4da4-92e1-d0dcff67e96f	\N	\N	RFQ-010PRL	RFQ-10PR1	System structure only	APPROVED	t	f	2024-03-04 16:14:51.015	test who	\N	\N	\N	\N
4	0	fdd97e12-7591-483b-ae54-a3fd83c5f86b	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	\N	\N	RFQ-010	RFQ-10	System structure only	APPROVED	t	f	2024-03-04 16:14:51.015	test who	\N	\N	\N	\N
5	0	1a0924f4-9b30-4064-89cd-7ce2c547595f	\N	\N	3c36d87f-3d2c-4a88-87f1-77614e20aa89	\N	\N	RFQ-N1U1	RFQ-N1U1	System structure only	APPROVED	t	f	2024-03-04 16:14:51.015	test who	\N	\N	\N	\N
7	0	0a6ac5c8-e177-4630-85a5-0b20c1eb1c08	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	002	RFQ-010:EMR-FS-002	RFQ-10:EMR-FS-2	description	APPROVED	t	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
8	0	7a39848e-7075-4327-8402-e3c80e92f8f6	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	003	RFQ-010:EMR-FS-003	RFQ-10:EMR-FS-3	description	APPROVED	t	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
9	0	311f25dd-8ab3-4482-9d83-0c00c0f09858	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	004	RFQ-010:EMR-FS-004	RFQ-10:EMR-FS-4	description	APPROVED	t	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
10	0	0b70edf6-e7e9-40d9-8983-01eb03b99076	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	005	RFQ-010:EMR-FS-005	RFQ-10:EMR-FS-5	description	APPROVED	t	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
11	0	4d40d460-8de3-4525-80e7-1ca28e984240	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	006	RFQ-010:EMR-FS-006	RFQ-10:EMR-FS-6	description	APPROVED	t	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
6	1	cb709c37-a1a2-4188-9acb-4692d603ad81	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	001	RFQ-010:EMR-FS-001	RFQ-10:EMR-FS-1	description	APPROVED	f	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
15	0	cb709c37-a1a2-4188-9acb-4692d603ad81	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	001	RFQ-010:EMR-FS-001	RFQ-10:EMR-FS-1	updated description again	APPROVED	t	f	2024-03-04 16:14:51.628	test who	\N	\N	\N	\N
14	1	cb709c37-a1a2-4188-9acb-4692d603ad81	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	001	RFQ-010:EMR-FS-001	RFQ-10:EMR-FS-1	updated description	APPROVED	f	f	2024-03-04 16:14:51.584	test who	\N	\N	\N	\N
16	0	57291cdc-41e9-4c9f-aeeb-520692ac3a57	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	007	RFQ-010:EMR-FS-007	RFQ-10:EMR-FS-7	description	APPROVED	t	t	2024-03-04 16:14:51.66	test who	\N	\N	\N	\N
12	1	57291cdc-41e9-4c9f-aeeb-520692ac3a57	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	007	RFQ-010:EMR-FS-007	RFQ-10:EMR-FS-7	description	APPROVED	f	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
17	0	22f8625f-7135-4503-8413-d3792c6a1215	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	008	RFQ-010:EMR-FS-008	RFQ-10:EMR-FS-8	description	APPROVED	t	t	2024-03-04 16:14:51.683	test who	\N	\N	\N	\N
13	1	22f8625f-7135-4503-8413-d3792c6a1215	\N	\N	40743524-3a06-4d8c-be81-5cfe8419e05f	e5b662f9-ca7e-4fb3-b083-30b0e6a38a9d	008	RFQ-010:EMR-FS-008	RFQ-10:EMR-FS-8	description	APPROVED	f	f	2024-03-04 16:14:51.512	test who	\N	\N	\N	\N
\.


--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 212
-- Name: name_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.name_id_seq', 17, true);


--
-- TOC entry 2324 (class 0 OID 16404)
-- Dependencies: 191
-- Data for Name: namepart; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.namepart (id, version, nameparttype, uuid) FROM stdin;
\.


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 192
-- Name: namepart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.namepart_id_seq', 1, false);


--
-- TOC entry 2326 (class 0 OID 16412)
-- Dependencies: 193
-- Data for Name: namepartrevision; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.namepartrevision (id, version, deleted, description, mnemonic, mnemoniceqclass, name, processdate, processorcomment, requestdate, requestercomment, status, namepart_id, parent_id, processedby_id, requestedby_id) FROM stdin;
\.


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 194
-- Name: namepartrevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.namepartrevision_id_seq', 1, false);


--
-- TOC entry 2334 (class 0 OID 16506)
-- Dependencies: 201
-- Data for Name: subsystem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.subsystem (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	de2b46ae-2459-4da4-92e1-d0dcff67e96f	e5c2b06e-7f3e-4adf-9c2d-c20c0f873ba9	010PRL	10PR1	\N	01 Phase Reference Line	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.015	test who	\N
2	0	40743524-3a06-4d8c-be81-5cfe8419e05f	e5c2b06e-7f3e-4adf-9c2d-c20c0f873ba9	010	10	\N	RFQ-010	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.015	test who	\N
3	0	3c36d87f-3d2c-4a88-87f1-77614e20aa89	e5c2b06e-7f3e-4adf-9c2d-c20c0f873ba9	N1U1	N1U1	\N	Power switch board 01. Electrical power cabinets	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:51.015	test who	\N
\.


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 208
-- Name: subsystem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.subsystem_id_seq', 3, true);


--
-- TOC entry 2333 (class 0 OID 16500)
-- Dependencies: 200
-- Data for Name: system; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.system (id, version, uuid, parent_uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	e5c2b06e-7f3e-4adf-9c2d-c20c0f873ba9	9aae66a8-ac73-4c0c-b252-430249882403	RFQ	RFQ	\N	Radio Frequency Quadrupole	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:50.871	test who	\N
\.


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 207
-- Name: system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.system_id_seq', 1, true);


--
-- TOC entry 2332 (class 0 OID 16494)
-- Dependencies: 199
-- Data for Name: systemgroup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.systemgroup (id, version, uuid, mnemonic, mnemonic_equivalence, ordering, description, status, latest, deleted, requested, requested_by, requested_comment, processed, processed_by, processed_comment) FROM stdin;
1	0	9aae66a8-ac73-4c0c-b252-430249882403	Acc	ACC	\N	Accelerator. The ESS Linear Accelerator	APPROVED	t	f	\N	\N	\N	2024-03-04 16:14:50.452	test who	\N
\.


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 206
-- Name: systemgroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.systemgroup_id_seq', 1, true);


--
-- TOC entry 2328 (class 0 OID 16420)
-- Dependencies: 195
-- Data for Name: useraccount; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.useraccount (id, version, role, username) FROM stdin;
\.


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 196
-- Name: useraccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.useraccount_id_seq', 1, false);


--
-- TOC entry 2116 (class 2606 OID 16435)
-- Name: appinfo appinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appinfo
    ADD CONSTRAINT appinfo_pkey PRIMARY KEY (id);


--
-- TOC entry 2118 (class 2606 OID 16437)
-- Name: device device_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);


--
-- TOC entry 2169 (class 2606 OID 16616)
-- Name: devicegroup devicegroup_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicegroup
    ADD CONSTRAINT devicegroup_pk PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 16439)
-- Name: devicerevision devicerevision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT devicerevision_pkey PRIMARY KEY (id);


--
-- TOC entry 2178 (class 2606 OID 16618)
-- Name: devicetype devicetype_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicetype
    ADD CONSTRAINT devicetype_pk PRIMARY KEY (id);


--
-- TOC entry 2160 (class 2606 OID 16614)
-- Name: discipline discipline_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discipline
    ADD CONSTRAINT discipline_pk PRIMARY KEY (id);


--
-- TOC entry 2187 (class 2606 OID 16620)
-- Name: name name_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.name
    ADD CONSTRAINT name_pk PRIMARY KEY (id);


--
-- TOC entry 2122 (class 2606 OID 16441)
-- Name: namepart namepart_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepart
    ADD CONSTRAINT namepart_pkey PRIMARY KEY (id);


--
-- TOC entry 2124 (class 2606 OID 16443)
-- Name: namepartrevision namepartrevision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT namepartrevision_pkey PRIMARY KEY (id);


--
-- TOC entry 2152 (class 2606 OID 16612)
-- Name: subsystem subsystem_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subsystem
    ADD CONSTRAINT subsystem_pk PRIMARY KEY (id);


--
-- TOC entry 2143 (class 2606 OID 16610)
-- Name: system system_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system
    ADD CONSTRAINT system_pk PRIMARY KEY (id);


--
-- TOC entry 2134 (class 2606 OID 16608)
-- Name: systemgroup systemgroup_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.systemgroup
    ADD CONSTRAINT systemgroup_pk PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 16445)
-- Name: useraccount useraccount_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.useraccount
    ADD CONSTRAINT useraccount_pkey PRIMARY KEY (id);


--
-- TOC entry 2163 (class 1259 OID 16563)
-- Name: devicegroup_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_deleted_idx ON public.devicegroup USING btree (deleted);


--
-- TOC entry 2164 (class 1259 OID 16558)
-- Name: devicegroup_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_id_idx ON public.devicegroup USING btree (id);


--
-- TOC entry 2165 (class 1259 OID 16583)
-- Name: devicegroup_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_latest_idx ON public.devicegroup USING btree (latest);


--
-- TOC entry 2166 (class 1259 OID 16561)
-- Name: devicegroup_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_mnemonic_idx ON public.devicegroup USING btree (mnemonic);


--
-- TOC entry 2167 (class 1259 OID 16560)
-- Name: devicegroup_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_parent_uuid_idx ON public.devicegroup USING btree (parent_uuid);


--
-- TOC entry 2170 (class 1259 OID 16562)
-- Name: devicegroup_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_status_idx ON public.devicegroup USING btree (status);


--
-- TOC entry 2171 (class 1259 OID 16559)
-- Name: devicegroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicegroup_uuid_idx ON public.devicegroup USING btree (uuid);


--
-- TOC entry 2172 (class 1259 OID 16569)
-- Name: devicetype_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_deleted_idx ON public.devicetype USING btree (deleted);


--
-- TOC entry 2173 (class 1259 OID 16564)
-- Name: devicetype_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_id_idx ON public.devicetype USING btree (id);


--
-- TOC entry 2174 (class 1259 OID 16584)
-- Name: devicetype_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_latest_idx ON public.devicetype USING btree (latest);


--
-- TOC entry 2175 (class 1259 OID 16567)
-- Name: devicetype_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_mnemonic_idx ON public.devicetype USING btree (mnemonic);


--
-- TOC entry 2176 (class 1259 OID 16566)
-- Name: devicetype_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_parent_uuid_idx ON public.devicetype USING btree (parent_uuid);


--
-- TOC entry 2179 (class 1259 OID 16568)
-- Name: devicetype_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_status_idx ON public.devicetype USING btree (status);


--
-- TOC entry 2180 (class 1259 OID 16565)
-- Name: devicetype_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX devicetype_uuid_idx ON public.devicetype USING btree (uuid);


--
-- TOC entry 2155 (class 1259 OID 16557)
-- Name: discipline_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_deleted_idx ON public.discipline USING btree (deleted);


--
-- TOC entry 2156 (class 1259 OID 16553)
-- Name: discipline_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_id_idx ON public.discipline USING btree (id);


--
-- TOC entry 2157 (class 1259 OID 16582)
-- Name: discipline_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_latest_idx ON public.discipline USING btree (latest);


--
-- TOC entry 2158 (class 1259 OID 16555)
-- Name: discipline_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_mnemonic_idx ON public.discipline USING btree (mnemonic);


--
-- TOC entry 2161 (class 1259 OID 16556)
-- Name: discipline_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_status_idx ON public.discipline USING btree (status);


--
-- TOC entry 2162 (class 1259 OID 16554)
-- Name: discipline_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX discipline_uuid_idx ON public.discipline USING btree (uuid);


--
-- TOC entry 2181 (class 1259 OID 16576)
-- Name: name_convention_name_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_convention_name_idx ON public.name USING btree (convention_name);


--
-- TOC entry 2182 (class 1259 OID 16578)
-- Name: name_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_deleted_idx ON public.name USING btree (deleted);


--
-- TOC entry 2183 (class 1259 OID 16575)
-- Name: name_devicetype_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_devicetype_uuid_idx ON public.name USING btree (devicetype_uuid);


--
-- TOC entry 2184 (class 1259 OID 16571)
-- Name: name_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_id_idx ON public.name USING btree (id);


--
-- TOC entry 2185 (class 1259 OID 16585)
-- Name: name_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_latest_idx ON public.name USING btree (latest);


--
-- TOC entry 2188 (class 1259 OID 16577)
-- Name: name_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_status_idx ON public.name USING btree (status);


--
-- TOC entry 2189 (class 1259 OID 16574)
-- Name: name_subsystem_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_subsystem_uuid_idx ON public.name USING btree (subsystem_uuid);


--
-- TOC entry 2190 (class 1259 OID 16573)
-- Name: name_system_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_system_uuid_idx ON public.name USING btree (system_uuid);


--
-- TOC entry 2191 (class 1259 OID 16572)
-- Name: name_systemgroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_systemgroup_uuid_idx ON public.name USING btree (systemgroup_uuid);


--
-- TOC entry 2192 (class 1259 OID 16570)
-- Name: name_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX name_uuid_idx ON public.name USING btree (uuid);


--
-- TOC entry 2146 (class 1259 OID 16552)
-- Name: subsystem_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_deleted_idx ON public.subsystem USING btree (deleted);


--
-- TOC entry 2147 (class 1259 OID 16547)
-- Name: subsystem_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_id_idx ON public.subsystem USING btree (id);


--
-- TOC entry 2148 (class 1259 OID 16581)
-- Name: subsystem_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_latest_idx ON public.subsystem USING btree (latest);


--
-- TOC entry 2149 (class 1259 OID 16550)
-- Name: subsystem_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_mnemonic_idx ON public.subsystem USING btree (mnemonic);


--
-- TOC entry 2150 (class 1259 OID 16549)
-- Name: subsystem_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_parent_uuid_idx ON public.subsystem USING btree (parent_uuid);


--
-- TOC entry 2153 (class 1259 OID 16551)
-- Name: subsystem_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_status_idx ON public.subsystem USING btree (status);


--
-- TOC entry 2154 (class 1259 OID 16548)
-- Name: subsystem_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsystem_uuid_idx ON public.subsystem USING btree (uuid);


--
-- TOC entry 2137 (class 1259 OID 16546)
-- Name: system_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_deleted_idx ON public.system USING btree (deleted);


--
-- TOC entry 2138 (class 1259 OID 16541)
-- Name: system_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_id_idx ON public.system USING btree (id);


--
-- TOC entry 2139 (class 1259 OID 16580)
-- Name: system_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_latest_idx ON public.system USING btree (latest);


--
-- TOC entry 2140 (class 1259 OID 16544)
-- Name: system_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_mnemonic_idx ON public.system USING btree (mnemonic);


--
-- TOC entry 2141 (class 1259 OID 16543)
-- Name: system_parent_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_parent_uuid_idx ON public.system USING btree (parent_uuid);


--
-- TOC entry 2144 (class 1259 OID 16545)
-- Name: system_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_status_idx ON public.system USING btree (status);


--
-- TOC entry 2145 (class 1259 OID 16542)
-- Name: system_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX system_uuid_idx ON public.system USING btree (uuid);


--
-- TOC entry 2129 (class 1259 OID 16540)
-- Name: systemgroup_deleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_deleted_idx ON public.systemgroup USING btree (deleted);


--
-- TOC entry 2130 (class 1259 OID 16536)
-- Name: systemgroup_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_id_idx ON public.systemgroup USING btree (id);


--
-- TOC entry 2131 (class 1259 OID 16579)
-- Name: systemgroup_latest_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_latest_idx ON public.systemgroup USING btree (latest);


--
-- TOC entry 2132 (class 1259 OID 16538)
-- Name: systemgroup_mnemonic_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_mnemonic_idx ON public.systemgroup USING btree (mnemonic);


--
-- TOC entry 2135 (class 1259 OID 16539)
-- Name: systemgroup_status_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_status_idx ON public.systemgroup USING btree (status);


--
-- TOC entry 2136 (class 1259 OID 16537)
-- Name: systemgroup_uuid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX systemgroup_uuid_idx ON public.systemgroup USING btree (uuid);


--
-- TOC entry 2197 (class 2606 OID 16446)
-- Name: namepartrevision fk_3f26vetemhujfdm9q74ecr2u5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_3f26vetemhujfdm9q74ecr2u5 FOREIGN KEY (namepart_id) REFERENCES public.namepart(id);


--
-- TOC entry 2193 (class 2606 OID 16451)
-- Name: devicerevision fk_4ucnoos7kd8s1gaqbpwm1xptq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_4ucnoos7kd8s1gaqbpwm1xptq FOREIGN KEY (requestedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2198 (class 2606 OID 16456)
-- Name: namepartrevision fk_9vomfk9x1jow27ifx6xc62c5x; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_9vomfk9x1jow27ifx6xc62c5x FOREIGN KEY (processedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2199 (class 2606 OID 16461)
-- Name: namepartrevision fk_9xs5oy86lf0j8ukpjokjipeke; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_9xs5oy86lf0j8ukpjokjipeke FOREIGN KEY (requestedby_id) REFERENCES public.useraccount(id);


--
-- TOC entry 2194 (class 2606 OID 16466)
-- Name: devicerevision fk_d3ocbsb4tl4ttnusn98khq148; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_d3ocbsb4tl4ttnusn98khq148 FOREIGN KEY (devicetype_id) REFERENCES public.namepart(id);


--
-- TOC entry 2195 (class 2606 OID 16471)
-- Name: devicerevision fk_l7kklb4mxixjs27nsso6shone; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_l7kklb4mxixjs27nsso6shone FOREIGN KEY (section_id) REFERENCES public.namepart(id);


--
-- TOC entry 2196 (class 2606 OID 16476)
-- Name: devicerevision fk_l9r1givkfaiol5or2lnr324xp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.devicerevision
    ADD CONSTRAINT fk_l9r1givkfaiol5or2lnr324xp FOREIGN KEY (device_id) REFERENCES public.device(id);


--
-- TOC entry 2200 (class 2606 OID 16481)
-- Name: namepartrevision fk_lufxqy46l9eiq55d445rbukag; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.namepartrevision
    ADD CONSTRAINT fk_lufxqy46l9eiq55d445rbukag FOREIGN KEY (parent_id) REFERENCES public.namepart(id);


-- Completed on 2024-03-04 17:17:29 CET

--
-- PostgreSQL database dump complete
--


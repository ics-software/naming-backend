README
----------

Folder contains backup of data for Naming database that may be useful for development and maintenance.

Content prepared by
- integration test class - method
      NamesIT                  readSearchHistoryDeleted
      StructuresSubsystemIT    readSearchStatusDeletedChildren
- setting debug point in methods at first statement in read & search section
- debug
- when test is halted, use database management tool to backup data

Backup done with DBeaver tool
    21.2.0    dump-discs_names_namesit.sql
    23.3.1    dump-discs_names_subsystemit.sql

Restore to be done with psql
    make sure database container is up and running
    e.g.
        psql --host=localhost --port=5432 --dbname=discs_names --username=discs_names < ......../naming-backend/src/test/resources/data/db/dump-discs_names_subsystemit.sql

DBeaver
  https://dbeaver.io/

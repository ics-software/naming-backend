### About

Describe ability to develop and run integration tests for Naming backend with Docker.

In other words, how to use `src/test/java` to test `src/main/java` with integration tests using Docker.

##### Background

Naming backend with Postgresql database together with the environment in which the applications run, is heavily relied on by other applications and environments. Outside interface is to Naming but Naming and Postgresql go together. Therefore, there is need to test Naming and Postgresql together.

It is possible to test Naming REST API by running Naming and Postgresql applications together as Docker containers and executing a series of requests and commands to test their behavior. This tutorial will show how it works and give examples.

##### Content

* [Prerequisites](#prerequisites)
* [Examples](#examples)
* [How it works - big picture](#how-it-works-big-picture)
* [How it works - in detail](#how-it-works-in-detail)
* [How to run](#how-to-run)
* [Reference](#reference)

### Prerequisites

##### Tools

* Docker - engine 18.06.0+ or later, compose 2.21.0 or later, compose file version 3.7 to be supported

##### Dependencies

* JUnit 5
* Testcontainers

##### Files

* folder `src/test/java` and package `org.phoebus.channelfinder.docker`
* [docker-compose-integrationtest.yml](../../../docker-compose-integrationtest.yml)
* [Dockerfile](../../../Dockerfile)

### Examples

##### Simple

[HealthcheckIT.java](../../../src/test/java/org/openepics/names/docker/HealthcheckIT.java)

```
@Test
void readHealthcheck()
```

Purpose
* verify that Naming is up and running

How
* Http request (GET) is run towards Naming base url and response code is verified to be 200

##### Medium

[StructuresDisciplineIT.java](../../../src/test/java/org/openepics/names/docker/StructuresDisciplineIT.java)

```
@Test
void create()
```

Purpose
* verify behavior for single discipline that include commands - exists, is valid to create, validate create, create, validate approve, approve

How
* a series of Http requests (GET) and curl commands (POST, PUT, PATCH, DELETE) are run towards the application to test behavior

##### Complex

[NamesIT.java](../../../src/test/java/org/openepics/names/docker/NamesIT.java)

```
@Test
void readSearchHistoryDeleted()
```

Purpose
* set up test fixture - structures - SystemGroup, System, Subsystem, Discipline, DeviceGroup, DeviceType
* verify behavior for names that includes commands - create, read, update, delete
* no need to tear down test fixture - test content designed to not interfere with other tests

How
* a series of Http requests (GET) and curl commands (POST, PUT, PATCH, DELETE) are run towards the application to test behavior

### How it works - big picture

##### Note

* Docker containers (Naming, Postgresql) are shared for tests within test class. Order in which tests are run is not known. Therefore, each test is to leave Naming, Postgresql in a clean state to not disturb other tests.
* Tests in `NamesIT` together with `StructuresDeviceGroupIT, StructuresDeviceTypeIT, StructuresDisciplineIT, StructuresSubsystemIT, StructuresSystemGroupIT, StructuresSystemIT` represent most of functionality for names and structures. Verification of all functionality require all tests.

### How it works - in detail

##### Anatomy of an integration test

```
@Testcontainers
class StructuresDisciplineIT {

    @Container
    public static final ComposeContainer ENVIRONMENT = ITUtil.defaultComposeContainers();

    @Test
    void create() {
        // test create discipline

        StructureElementCommandCreate structureElementCommandCreate = null;
        StructureElementCommandUpdate structureElementCommandUpdate = null;
        StructureElementCommandConfirm structureElementCommandConfirm = null;
        StructureElementCommand structureElementCommand = null;
        StructureElement structureElement = null;

        structureElementCommand = new StructureElementCommand(null, Type.DISCIPLINE, null, "Ca", null, "description");

        structureElementCommandCreate = StructureElementUtil.convertCommand2CommandCreate(structureElementCommand);
        structureElementCommandUpdate = StructureElementUtil.convertCommand2CommandUpdate(structureElementCommand);
        structureElementCommandConfirm = StructureElementUtil.convertCommand2CommandConfirm(structureElementCommand);

        ITUtilStructures.assertExists(Type.DISCIPLINE, "Ca", Boolean.FALSE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE, "Ca", Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.FALSE);

        // create
        structureElement = ITUtilStructures.assertCreate(structureElementCommandCreate);
        structureElementCommandUpdate = StructureElementUtil.convertElement2CommandUpdate(structureElement);
        structureElementCommandConfirm = StructureElementUtil.convertElement2CommandConfirm(structureElement);

        ITUtilStructures.assertExists(Type.DISCIPLINE, "Ca", Boolean.TRUE);
        ITUtilStructures.assertIsValidToCreate(Type.DISCIPLINE, "Ca", Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandCreate,  StructureCommand.CREATE, Boolean.FALSE);
        ITUtilStructures.assertValidate(structureElementCommandUpdate,  StructureCommand.UPDATE, Boolean.TRUE);
        ITUtilStructures.assertValidate(structureElementCommandConfirm, StructureCommand.DELETE, Boolean.TRUE);
        ITUtilStructures.assertCreate(structureElementCommandCreate, HttpURLConnection.HTTP_CONFLICT);
    }
```

##### What happens at runtime

The test environment is started with through test class annotated with `@Testcontainers` and constant `ENVIRONMENT` annotated with `@Container`. Containers are started (Ryuk, Naming, Postgresql). Then one time setup is run (method annotated with `@BeforeAll` if needed), after which individual tests are run (methods annotated with `@Test`). Note that one time tear down is not needed if test content is designed to not interfere with other tests (i.e. method annotated with `@AfterAll` if needed). Finally tasks are done and test class is closed.

Note the extensive use of test utility classes (in more detail below) in which are shared code for common tasks.

* authorization
* serialization and deserialization of properties, tags and channels
* Http requests (GET) and curl commands (POST, PUT, PATCH, DELETE) corresponding to endpoints in Naming REST API
* assert response

##### Examining `create`

1.  A GET request is made to Naming to check if Discipline exists
2.  A GET request is made to Naming to check if Discipline is valid to create
3.  A GET request is made to Naming to validate command to create Discipline
4.  A GET request is made to Naming to validate command to update Discipline
5.  A GET request is made to Naming to validate command to delete Discipline
6.  A POST request is made to Naming to create Discipline
7.  A GET request is made to Naming to check if Discipline exists
8.  A GET request is made to Naming to check if Discipline is valid to create
9.  A GET request is made to Naming to validate command to create Discipline
10. A GET request is made to Naming to validate command to update Discipline
11. A GET request is made to Naming to validate command to delete Discipline
12. A POST request is made to Naming to create Discipline


* 1, 7 - Request correspond to IStructures method

```
    @GetMapping(
            value = "/exists/{type}/{mnemonicPath}")
    public ResponseEntity<ResponseBoolean> existsStructure(
```

* 2, 8 - Request correspond to IStructures method

```
    @GetMapping(
            value = "/isValidToCreate/{type}/{mnemonicPath}",
            produces = {"application/json"})
    public ResponseEntity<ResponseBoolean> isValidToCreateStructure(
```

* 3, 9 - Request correspond to IStructures method

```
    @GetMapping(
            value = "/validateCreate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresCreate(
```

* 4, 10 - Request correspond to IStructures method

```
    @GetMapping(
            value = "/validateUpdate",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresUpdate(
```

* 5, 11 - Request correspond to IStructures method

```
    @GetMapping(
            value = "/validateDelete",
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<ResponseBooleanList> validateStructuresDelete(
```

* 6, 12 - Request correspond to IStructures method

```
    @PostMapping(
            produces = {"application/json"},
            consumes = {"application/json"})
    public ResponseEntity<List<StructureElement>> createStructures(
```

##### Test classes

See `src/test/java` and `org.openepics.names.docker`.

* files with suffix IT.java

##### Test utilities

See `src/test/java` and `org.openepics.names.docker`.

* files with prefix ITUtil

##### Test utilities - example

With the help of test utitilies, the tests themselves may be simplified and made more clear.

```
public class ITUtilNames {

    public static ResponsePageNameElements assertRead(String queryString) {
        return assertRead(queryString, -1, -1);
    }
    public static ResponsePageNameElements assertRead(String queryString, int expectedEqual) {
        return assertRead(queryString, expectedEqual, expectedEqual);
    }

    /**
     * Utility method to read name elements and assert number of items in list.
     *
     * @param queryString query string
     * @param expectedGreaterThanOrEqual (if non-negative number) greater than or equal to this number of items
     * @param expectedLessThanOrEqual (if non-negative number) less than or equal to this number of items
     * @return response page name elements
     */
    public static ResponsePageNameElements assertRead(String queryString, int expectedGreaterThanOrEqual, int expectedLessThanOrEqual) {
        try {
            String[] response = null;
            ResponsePageNameElements responsePageNameElements = null;

            response = ITUtil.doGetJson(ITUtil.HTTP_IP_PORT_NAMING_API_V1_NAMES + queryString);
            ITUtil.assertResponseLength2Code(response, HttpURLConnection.HTTP_OK);
            responsePageNameElements = mapper.readValue(response[1], ResponsePageNameElements.class);

            // expected number of items in list
            //     (if non-negative number)
            //     expectedGreaterThanOrEqual <= nbr of items <= expectedLessThanOrEqual
            if (expectedGreaterThanOrEqual >= 0) {
                assertTrue(responsePageNameElements.getListSize() >= expectedGreaterThanOrEqual);
            }
            if (expectedLessThanOrEqual >= 0) {
                assertTrue(responsePageNameElements.getListSize() <= expectedLessThanOrEqual);
            }

            // expected value for latest for items in list
            assertLatest(responsePageNameElements);

            return responsePageNameElements;
        } catch (IOException e) {
            fail();
        } catch (Exception e) {
            fail();
        }
        return null;
    }
```

Above methods can be used like shown below.

```
@Testcontainers
class NamesIT {

    @Test
    void readSearchHistoryDeleted() {
        NameElementCommandCreate nameElementCommandCreate = new NameElementCommandCreate(subsystem010, deviceTypeFS, "001", "description");

        ITUtilNames.assertCreate(nameElementCommandCreate);

        ITUtilNames.assertRead("?name=RFQ-010:EMR-FS-005",                               1);
        ITUtilNames.assertRead("?name=RFQ-010%",                                        10, -1);

        ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=true&page=0&pageSize=3",   3);
        ITUtilNames.assertRead("?index=00_&orderBy=WHEN&isAsc=false&page=2&pageSize=3",  2);
```

##### Note

* (Re) Build after change in `src/main/java` is needed in order for change to be tested as `Dockerfile` relies on built code.
* Configuration in folder `src/test/java` and package `org.openepics.names.docker`, e.g. urls and port numbers, is coupled to files `Dockerfile` and `docker-compose-integrationtest.yml` (beside `src/main/resources/application.properties`).
* Both positive and negative tests are important to ensure validation works as expected.

### How to run

See [How to run Integration test with Docker](INTEGRATIONTEST_DOCKER_RUN.md).

### Reference

##### Naming

* REST API - folder `src/main/java` and packages `org.openepics.names.rest.api.v1` and `org.openepics.names.rest.controller`

##### Testcontainers

* [Testcontainers](https://testcontainers.com/)

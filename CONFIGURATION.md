### Configuration

Configuration may be set using SpringBoot's configuration file (`application.properties`) and environment variables.

##### Environment variables

| Variable                            | Default                                     | Description                                                                                                            |
|-------------------------------------|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------|
| **Application**                     |                                             |                                                                                                                        |
|                                     |                                             |                                                                                                                        |
| NAMING_DATABASE_URL                 | jdbc:postgresql://postgres:5432/discs_names | JDBC URL for the database connection                                                                                   |
| NAMING_DATABASE_USERNAME            | discs_names                                 | user name for the database connection                                                                                  |
| NAMING_DATABASE_PASSWORD            | discs_names                                 | password for the database connection                                                                                   |
| NAMING_LOGGING_LEVEL                | INFO                                        | log level for application                                                                                              |
| NAMING_LOGGING_FILTER_INCLUDES      | org.openepics.names                         | comma-separated list of strings to filter stack trace for inclusion (ref. whitelist)                                   |
| NAMING_LOGGING_FILTER_EXCLUDES      | org.openepics.names.util.ExceptionUtil      | comma-separated list of strings to filter stack trace for exclusion (ref. blacklist)                                   |
| NAMING_LOGGING_STACKTRACE_LENGTH    | 20                                          | max number of stack trace elements to log, negative value will log entire stack trace                                  |
| NAMING_MAIL_ADMINISTRATOR           |                                             | comma-separated list of administrator email addresses                                                                  |
| NAMING_MAIL_NOTIFICATION            | false                                       | true/false if mail notications are to be sent                                                                          |
| NAMING_MAIL_NOTIFICATION_NAMES      |                                             | comma-separated list of email addresses to which to send notications about changes for names                           |
| NAMING_MAIL_NOTIFICATION_STRUCTURES |                                             | comma-separated list of email addresses to which to send notications about changes for structures                      |
| NAMING_NOTIFY_SCHEDULE              | 0 0 5 * * *                                 | schedule for notification job, cron expression with six or seven fields to describe individual details of the schedule |
| NAMING_SMTP_HOST                    | mail.esss.lu.se                             | DNS name of server that accepts connections for SMTP                                                                   |
| NAMING_SMTP_PORT                    | 587                                         | port to use for SMTP, 587 for SMTP Secure                                                                              |
| NAMING_SMTP_USERNAME                |                                             | username for SMTP server                                                                                               |
| NAMING_SMTP_PASSWORD                |                                             | password for SMTP server                                                                                               |
|                                     |                                             |                                                                                                                        |
| **Springdoc, Swagger**              |                                             |                                                                                                                        |
|                                     |                                             |                                                                                                                        |
| NAMING_SWAGGER_URL                  | http://localhost:8080/swagger-ui.html       | default url for Swagger UI                                                                                             |
| API_DOCS_PATH                       | /api-docs                                   | custom path of the OpenAPI documentation in JSON                                                                       |
| SWAGGER_UI_PATH                     | /swagger-ui.html                            | custom path of the swagger-ui documentation in HTML                                                                    |
|                                     |                                             |                                                                                                                        |
| **Security**                        |                                             |                                                                                                                        |
|                                     |                                             |                                                                                                                        |
| NAMING_SECURITY_ENABLED             | false                                       | enable security for application                                                                                        |
| AES_KEY                             | aes_secret_key                              | secret that is used to hash the tokens in the JWT token                                                                |
| JWT_SECRET                          | secret_password_to_hash_token               | secret that is used sign the JWT token                                                                                 |
| JWT_EXP_MIN                         | 240                                         | time interval until the JWT token is valid (in minutes)                                                                |
| RBAC_SERVER_ADDRESS                 |                                             | RBAC server address used for log in users if NAMING_SECURITY_ENABLED is enabled                                        |

To do - in particular at time of first set of releases

----------------------------------------------------------------------------------------------------

Content
    (a) Before first release
            1. verification      - static
            2. verification      - data & application
    (b) For first release
            1. preparation       - close & backup
            2. migration scripts - run
            3. verification      - data & application
            4. release           - release & deploy
            5. post              - open & use
    (c) For second release
            1. preparation       - close & backup
            2. remove code       - remove obsolete
            3. migration script  - make new, remove previous
            4. upgrade database  - to latest postgres
            5. remove obsolete database content
            6. flyway            - introduce
            7. release           - release & deploy
            8. post              - open & use

----------------------------------------------------------------------------------------------------

Note
    - First release
            migration of database and functionality
          ( migration script V6.2 - audit tables contain all )
    - Second release
            one migration script only
            upgrade Postgres database
            introduction of Flyway
          ( possibility to do first and second release together and deploy Naming after second release )
    ------------------------------------------------------------------------------------------------
    - Tests should be able to run at all times
            see src/test/resources/INTEGRATIONTEST_DOCKER_RUN.md
                To build and run all unit tests and integration tests (Docker) with code coverage.
    - Verification of migration with endpoints
            /verification - concerns verification of migration up to script V5 (pre audit)
    - Configuration not mentioned below
            additional configfuration may be required
    - Keep migration documentation until judged ok to remove
            keep docs/developer/naming_rest_api_migration.doc (.pdf) until some later time
            to be able to guide users of old Naming REST API to new Naming REST API
    - Respect integrations

----------------------------------------------------------------------------------------------------

(a) Before first release
        1. verification          - static
                1.1 migration scripts
                1.2 code
                1.3 documentation
        2. verification          - data & application
                2.1 use endpoints
                        /report
                            /report/about
                        /verification
                            /verification/migration_structures
                            /verification/migration_names
                            /verification/data_reachable
                            /verification/restapi_oldvsnew
                2.2 analyse database
                        example sql queries
                2.3 swagger
                2.4 other

----------------------------------------------------------------------------------------------------

(b) For first release
        1. preparation           - close & backup
                1.1 close Naming (old)
                1.2 backup database
        2. migration scripts     - run
                2.1 run migration script - src/main/resources/db/migration/V4__Schema_data_migration.sql
                2.2 run migration script - src/main/resources/db/migration/V5.2__Data_transformation_status_deleted.sql
                2.3 run migration script - src/main/resources/db/migration/V6.2__Schema_data_migration_audit.sql
                2.7 run migration script - src/main/resources/db/migration/V7__Remove_user_notification_cc_list.sql

                about
                        V4   - migrate to temporary tables, "work in progress"
                        V5.2 - delete entries that never have been active
                        V6.2 - migrate to permanent tables, audit tables with all entries
                        V7   - remove table as no longer used
        3. verification          - data & application
                3.1 use old Naming REST API
                3.2 use new Naming REST API
                        /report
                            /report/about
                        /verification
                            /verification/migration_structures
                            /verification/migration_names
                            /verification/data_reachable
                            /verification/restapi_oldvsnew
                        swagger

                        note
                                that if migration_structures fails, the difference in (# namepartrevision - # sum structures)
                                should match number of rows that were deleted in script V5.2
                3.2 analyse database
                        example sql queries
                3.3 other
        4. release               - release & deploy
        5. post                  - open & use
                4.1 open Naming (new)

----------------------------------------------------------------------------------------------------

(c) For second release
        1. preparation           - close & backup
                1.1 close Naming (new)
                1.2 backup database
        2. remove code       - remove obsolete
                2.1. remove code - old and wip (work-in-progress)
                        src/main/java
                            org.openepics.names.old.business
                            org.openepics.names.old.model
                            org.openepics.names.old.nameviews
                            org.openepics.names.repository.model.wip
                            org.openepics.names.repository.old
                            org.openepics.names.repository.wip
                            org.openepics.names.rest.beans.old
                            org.openepics.names.rest.controller.old
                            org.openepics.names.util.old
                            org.openepics.names.util.wip
                            ( org.openepics.names.old )
                            org.openepics.names.rest.controller/ReportController.java
                            org.openepics.names.rest.controller/VerificationController.java
                        src/test/java
                            org.openepics.names.repository.model.wip
                            org.openepics.names.docker.ReportIT.java
                            org.openepics.names.docker.VerificationIT.java

                        old - refers to code for old Naming tool                              - for reference and verification purposes
                        wip - refers to work-in-progress (temporary) code for new Naming tool - for migration and verification purposes
                2.2. update documentation ( related to removal of code )
                        src/test/resources
                            INTEGRATIONTEST_DOCKER_RUN.md
                        docs/developer
                            naming_architecture_code.docx
                            naming_architecture_code.pdf
        3. migration script  - make new, remove previous
                3.1 create with database management tool, e.g. DBeaver
                3.2 give name V1__*.sql
                3.3 update docker-compose-integrationtest.yml to use new migration script V1__*.sql instead of old migration scripts (V1 - V6)
        4. upgrade database  - to latest postgres
                4.1 decide postgres version to upgrade to, e.g. 16.2
                4.2 update docker compose files
                        docker-compose.yml
                        docker-compose-demo.yml
                        docker-compose-integrationtest.yml
        5. remove obsolete database content
                5.1 remove obsolete tables - tables no longer in use - see 6.2 - decide on flyway table
                        tables
                            remove
                                appinfo
                                device
                                devicerevision
                              ( flyway_schema_history )
                                namepart
                                namepartrevision
                                useraccount
                                wip_devicegroup
                                wip_devicetype
                                wip_discipline
                                wip_name
                                wip_subsystem
                                wip_system
                                wip_systemgroup
                            keep
                                audit_name
                                audit_structure
                                devicegroup
                                devicetype
                                discipline
                              ( flyway_schema_history )
                                name
                                subsystem
                                system
                                systemgroup
                        other
                            related to wip tables -- see also V4__Schema_data_migration.sql
                            indices
                            foreign keys
                            sequences
                            functions
        6. flyway            - introduce
                6.1 possibly use existing table flyway_schema_history or create a new table
                6.2 introduce flyway and make use of new migration script V1__*.sql - see 5.1 - decide on flyway table
                6.3 update integration tests accordingly
                        docker-compose-integrationtest.yml
                            postgres:
                              volumes:
                                - ./src/main/resources/db/migration/V1__*.sql:/docker-entrypoint-initdb.d/V1__*.sql
        7. release           - release & deploy
        8. post              - open & use
                8.1 open Naming (new)

----------------------------------------------------------------------------------------------------
